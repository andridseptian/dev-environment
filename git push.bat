@ECHO OFF

ECHO Commit and Push current workplace to ...
git config --get remote.origin.url

ECHO Press any key to Continue.
pause >nul

set /p mes="Enter Commit Message: "

ECHO push with commit message "%mes%"
git add . && git commit -m "%mes%" && git push

ECHO Press any key to Exit.

pause >nul