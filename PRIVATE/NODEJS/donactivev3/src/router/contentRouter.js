var express = require('express')
const { content } = require('../controller/webContentController')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))

exports.init = async function (app) {
    try {
        app.use('/api/content', router)
        router.post('/*', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)
                logger.info('request param: ', req.params)

                var response = "no response"
                switch (req.path) {
                    case 'create': response = await content.createContent(req, res); break;
                    case '/select/all': response = await content.getAllContent(req, res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }

                logger.info("response body: ", response)
                res.send(response)
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })
    } catch (error) {
        logger.error(error)
    }
}

exports.contentRouter = this