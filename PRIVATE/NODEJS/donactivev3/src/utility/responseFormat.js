exports.sendResponse = async (response, res, logger) => {
    if (response !== "" || response !== undefined) {
        res.send(response)
    } else {
        res.send(JSON.stringify({
            rc: "99",
            rm: "RESPONSE NOT FOUND"
        }))
    }

    logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
}

exports.dataWrap = async (data) => {
    if (!data.hasOwnProperty('rc')) {
        var response = {
            rc: '00',
            rm: data
        }
        return (response)
    } else {
        return data
    }
}

exports.responseWrap = async (rc, rm, data) => {
    var response = {
        rc: rc,
        rm: rm,
        data: data
    }
    return (response)
}

exports.errorWrap = async (logger, error) => {
    logger.error(error)
    return {
        rc: '99',
        rm: error.message,
        detail: error.original
    }
}

exports.isNull = async (data) => {
    if (data !== null && data !== undefined && data !== "") {
        return true
    } else {
        return false
    }
}

exports.rf = this