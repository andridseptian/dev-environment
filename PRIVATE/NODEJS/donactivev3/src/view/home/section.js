const { html } = require('@popeindustries/lit-html-server');

function sectionLayout(id, title, body, textcolor, background) {

    return html`
    <section class="page-section bg-${background} text-${textcolor} mb-0" id="${id}">
        <div class="container">
            <!-- tentang Section Heading-->
            <h2 class="page-section-heading text-center text-uppercase text-${textcolor}">${title}</h2>
            <!-- Icon Divider-->
            <div class="divider-custom divider-${textcolor}">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-heart"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <!-- tentang Section Content-->
            <div class="row">
                ${body}
            </div>
        </div>
    </section>
    `
}

class sectionBody {
    static about = function () {
        return html`
        <div class="col-lg-4 ml-auto">
            <p class="lead text-justify">Donactive Peduli Indonesia adalah yayasan amal yang bergerak dalam
                bidang agama, sosial, dan kemanusiaan. Berdasarkan namanya, Donactive terdiri dari
                dua suku kata
                yaitu Donative dan Active , yang selanjutnya diartikan sebagai suatu perkumpulan orang-orang
                yang aktif memberi donasi atau bantuan kepada orang-orang yang membutuhkan, baik
                bantuan dalam bentuk fisik maupun bantuan kemanusiaan lainnya.</p>
        </div>
        <div class="col-lg-4 mr-auto">
            <p class="lead text-justify">Harapan besar Yayasan ini adalah dapat menjadi pelopor dalam melakukan
                dan melaksanakan segala bentuk kegiatan yang berfokus pada peningkatan kualitas
                hidup dan
                mewujudkan kesejahteraan bagi masyarakat, menjadi wadah terpercaya bagi masyarakat Indonesia
                untuk menyalurkan dan memberikan donasi nya untuk selanjutnya disalurkan dalam
                bentuk kegiatan yang disusun oleh Donactive Peduli Indonesia.</p>
        </div>
        `
    }
    static why = function () {
        return html`
        <div class="col-lg-6 ml-auto">
            <p class="lead text-justify">Tujuan utama dari Donactive Peduli Indonesia adalah memberikan manfaat,
                bantuan, dan kebaikan sebesar-besarnya kepada masyarakat yang membutuhkan guna
                mewujudkan
                kesejahteraan di Indonesia juga menjadi wadah bagi para pemuda untuk menyalurkan ide, gagasan,
                dan dedikasinya didalam tiap kegiatan Donactive. selain itu para relawan yang
                tergabung mempunyai kesempatan untuk membentuk karakter, memperluas relasi, melatih diri
                berorganisasi dan tentunya menanamkan nilai-nilai Agama dan kepedulian sejak dini
                sehingga dari Donactive Peduli Indonesia inilah lahir generasi yang terampil, cerdas, dan
                berakhlak mulia..</p>
        </div>
        <div class="col-lg-4 mr-auto">
            <img class="img-fluid" src="/assets/img/portfolio/cabin.png" alt="" />
        </div>
        `
    }
    static donate = function () {
        return html`
        <div class="col-lg-6 ml-auto">
            <h5>Donasi terkumpul saat ini</h5>
            <hr>
            <h3>Data Belum Tersedia</h3>
            <hr>
            <p>Untuk ikut berpartisipasi dalam kegiatan kami, dapat melakukan donasi ke :
                No. Rekening Donactive Peduli Indonesia:
                BRI 0192-01-000857-56-1 a.n. Donactive Peduli Indonesia</p>
        </div>
        <div class="col-lg-4 mr-auto">
            <img class="img-fluid" src="/assets/img/portfolio/cabin.png" alt="" />
        </div>
        `
    }
    static contact = function () {
        return html`
        <div class="container">
            <h3 class="text-center">Kami bisa dihubungi melalui media berikut</h3>
            <hr>
            <div class="text-center">
                <div class="row justify-content-md-center mb-3">
                    <div class="col-sm-1 my-auto">
                        <i class="fab fa-2x fa-facebook"></i>
                    </div>
                    <div class="col-sm-4 my-auto">
                        <h6>Donactive Peduli Indonesia</h6>
                    </div>
                </div>
                <div class="row justify-content-md-center mb-3">
                    <div class="col-sm-1 my-auto">
                        <i class="fab fa-2x fa-instagram"></i>
                    </div>
                    <div class="col-sm-4 my-auto">
                        <h6>@donactive.id</h6>
                    </div>
                </div>
                <div class="row justify-content-md-center mb-3">
                    <div class="col-sm-1 my-auto">
                        <i class="fas fa-2x fa-envelope"></i>
                    </div>
                    <div class="col-sm-4 my-auto">
                        <h6>donactiveid@gmail.com</h6>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        `
    }
}

exports.sectionLayout = sectionLayout
exports.sectionBody = sectionBody

