const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');
const { appconfig } = require('../../app');

exports.databaseInitxxx = new Sequelize(appconfig.database, {
    logging: false,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: true
    }
}) // Example for postgres