const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { databaseInit } = require('../init')
const { getLogger } = require('log4js');
const logger = getLogger(require('path').basename(__filename))


var rf = require('../../utility/responseFormat');
var sequelize = databaseInit;

class WebContent extends Model { }
WebContent.init({
    id: { primaryKey: true, type: DataTypes.STRING },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    content: DataTypes.TEXT,
    status: DataTypes.STRING
}, { sequelize, modelName: 'webcontent' });

class WebContentStatus {
    static ACTIVE = "ACTIVE"
    static NOT_ACTIVE = "NOT_ACTIVE"
}

(async function () {
    await sequelize.sync({ alter: true });
})();

exports.WebContentStatus = WebContentStatus;

exports.create = async function (id, title, description, content) {
    try {
        const contetData = await WebContent.create({
            id: id,
            title: title,
            description: description,
            content: content,
            status: WebContentStatus.ACTIVE
        });
        return contetData;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.selectAll = async function () {
    try {
        const contetData = await WebContent.findAll();
        return contetData;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.selectById = async function (id) {
    try {
        const contetData = await WebContent.findOne({ where: { id: id } });
        return contetData;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.webContent = this
