const { renderToStream, html } = require('@popeindustries/lit-html-server')
const express = require('express')
const expressSession = require('express-session');
const cookieParser = require('cookie-parser');
const app = express()
const port = 3000

//view
const dashboard = require('./src/view/dashboard/layout')
const { dashboardContent } = require('./src/view/dashboard/content')
const login = require('./src/view/login/layout')
const { loginContent } = require('./src/view/login/content')
const regis = require('./src/view/register/layout')
const { registContent } = require('./src/view/register/content')

// Init DB
const userapp = require("./src/model/userapp")
const { userRouter } = require('./src/router/userRouter');
const { sequelizeSessionStore } = require('./src/database/donactiv_db');

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// sessions init using cookie parser and Express session
app.use(cookieParser());
app.use(expressSession({
    secret: 'donactiveadminsecret',
    store: sequelizeSessionStore,
    resave: false,
    saveUninitialized: false,
}));

app.use('/', express.static('public'))
app.use('/node_modules', express.static('node_modules'))

app.get('/dashboard', function (req, res) {
    renderToStream(dashboard.Layout(dashboardContent.home())).pipe(res)
})

app.get('/login', function (req, res) {
    renderToStream(login.Layout(loginContent.login())).pipe(res)
})

app.get('/regist', function (req, res) {
    renderToStream(regis.Layout(registContent.regist())).pipe(res)
})


userRouter.init(app)

app.listen(port, () => {
    console.log(`listening at http://localhost:${port}`)
})
