import { html, render } from "/node_modules/lit-html/lit-html.js?module"

$("#button-createaccount").on("click", function () {
    var settings = {
        "url": "/api/user/registration",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "username": $("#input-username").val(),
            "password": $("#input-password").val(),
            "email": $("#input-email").val(),
            "phone_number": null,
            "fullname": null
        }),
    };

    $.ajax(settings).done(function (response) {
        console.log(response)

        render(html`
        <p>Success, will be directed in 5 seconds</p>
        `, document.getElementById('content-body'))

        setTimeout(() => {
            window.location.href = response.data.url
        }, 5000);

    });
});