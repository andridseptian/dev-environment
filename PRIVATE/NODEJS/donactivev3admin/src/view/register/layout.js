const { html } = require("@popeindustries/lit-html-server");

function Layout(body) {
    return html`
    <html lang="en">
    
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
        <meta name="author" content="Łukasz Holeczek">
        <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
        <title>CoreUI Free Bootstrap Admin Template</title>
        <link href="css/style.css" rel="stylesheet">
    </head>
    
    <body class="c-app flex-row align-items-center">
        ${body}
        ${script()}
    </body>
    
    </html>
    `
} exports.Layout = Layout

function script() {
    return html`
    <!-- CoreUI and necessary plugins-->
    <script src="modules/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="modules/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <script src="node_modules/jquery/dist/jquery.js"></script>
    <script type="module" src="/js/register/script.js"></script>
    `
}