const { renderToStream, html } = require('@popeindustries/lit-html-server')

class dashboardContent {
    static home = async function () {
        return contentWrapper(html`
        <div class="row">
            ${smallcard("Akses", "3000", "text-white bg-primary")}
            ${smallcard("User", "12", "bg-warning")}
            ${smallcard("Artikel", "30", "bg-info")}
        </div>
        ${card("", "", html`
        <div class="mt-3 mb-5 text-center">
            <p>SELAMAT DATANG, DI DASHBOARD WEBSITE DONACTIVE</p>
        </div>
        `, "")}
        `)
    }

    static profile = async function () {

    }
} exports.dashboardContent = dashboardContent

function contentWrapper(body) {
    return html`
        <div class="c-wrapper c-fixed-components">
            ${header()}
            <div class="c-body">
                <main class="c-main">
                    <div class="container-fluid">
                        <div class="fade-in">
                            ${body}
                        </div>
                    </div>
                </main>
            </div>
        </div>
        `
}

function header() {
    return html`
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
            data-class="c-sidebar-show">
            <svg class="c-icon c-icon-lg">
                <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-menu"></use>
            </svg>
        </button><a class="c-header-brand d-lg-none" href="#">
            <svg width="118" height="46" alt="CoreUI Logo">
                <use xlink:href="assets/brand/coreui.svg#full"></use>
            </svg></a>
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
            data-class="c-sidebar-lg-show" responsive="true">
            <svg class="c-icon c-icon-lg">
                <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-menu"></use>
            </svg>
        </button>
        <ul class="c-header-nav d-md-down-none">
            <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Dashboard</a></li>
            <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Users</a></li>
            <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Settings</a></li>
        </ul>
        <ul class="c-header-nav ml-auto mr-4">
            <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#"
                    role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="c-avatar"><img class="c-avatar-img" src="assets/img/avatars/6.jpg" alt="user@email.com">
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right pt-0">
                    <div class="dropdown-divider"></div><a class="dropdown-item" href="#">
                        <svg class="c-icon mr-2">
                            <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-lock-locked"></use>
                        </svg> Lock Account</a><a class="dropdown-item" href="#">
                        <svg class="c-icon mr-2">
                            <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-account-logout"></use>
                        </svg> Logout</a>
                </div>
            </li>
        </ul>
        <div class="c-subheader px-3">
            <!-- Breadcrumb-->
            <ol class="breadcrumb border-0 m-0">
                <li class="breadcrumb-item">Home</li>
                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
                <!-- Breadcrumb Menu-->
            </ol>
        </div>
    </header>
    `
}

function card(title, description, body, footer) {
    return html`
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <div>
                    <h4 class="card-title mb-0">${title}</h4>
                    <div class="small text-muted">${description}</div>
                </div>
            </div>
            <div class="container">
                ${body}
            </div>
        </div>
        <div class="card-footer">
            ${footer}
        </div>
    </div>
    <!-- /.card-->
    `
}

function smallcard(key, value, style) {
    return html`
    <div class="col-sm-6 col-lg-3">
        <div class="card ${style}">
            <div class="card-body card-body pb-2 d-flex justify-content-between align-items-start">
                <div>
                    <div class="text-value-lg">${value}</div>
                    <div>${key}</div>
                </div>
            </div>
        </div>
    </div>
    `
}