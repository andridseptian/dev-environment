const { html } = require("@popeindustries/lit-html-server");

function Layout(body) {
    return html`
    <html lang="en">
    
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
        <meta name="author" content="Łukasz Holeczek">
        <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
        <title>CoreUI Free Bootstrap Admin Template</title>
        <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">
        <link rel="manifest" href="assets/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Main styles for this application-->
        <link href="css/style.css" rel="stylesheet">
        <link href="modules/@coreui/chartjs/dist/css/coreui-chartjs.css" rel="stylesheet">
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    </head>
    
    <body class="c-app">
        ${sidebar()}
        ${body}
        ${script()}
    </body>
    
    </html>
    `
}

function sidebar() {
    return html`
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
        <div class="c-sidebar-brand d-lg-down-none">
            <h5>Donactive Dashboard</h5>
        </div>
        <ul class="c-sidebar-nav">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="index.html">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-speedometer"></use>
                    </svg> Dashboard<span class="badge badge-info">NEW</span></a></li>
            <li class="c-sidebar-nav-title">Akun</li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="colors.html">
                    <i class="far fa-user mr-3"></i> Profil</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="typography.html">
                    <i class="far fa-check-square mr-3"></i> Konfirmasi</a></li>
    
            <li class="c-sidebar-nav-title">Components</li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a
                    class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="far fa-edit mr-3"></i> Konfigurasi </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="buttons/buttons.html"><span
                                class="c-sidebar-nav-icon"></span> Konten</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="buttons/button-group.html"><span
                                class="c-sidebar-nav-icon"></span> Donasi</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="buttons/dropdowns.html"><span
                                class="c-sidebar-nav-icon"></span> Kontak</a></li>
                </ul>
            </li>
    
            <li class="c-sidebar-nav-item mt-auto"><a class="c-sidebar-nav-link c-sidebar-nav-link-success" href="/"
                    target="_top">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-cloud-download"></use>
                    </svg> Direct to Home</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link c-sidebar-nav-link-danger" href="/" target="_top">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="modules/@coreui/icons/sprites/free.svg#cil-layers"></use>
                    </svg> Logout</a></li>
        </ul>
        <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
    </div>
    `
}

function script() {
    return html`
    <!-- CoreUI and necessary plugins-->
    <script src="/modules/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="/modules/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <!-- <script src="/modules/@coreui/chartjs/dist/js/coreui-chartjs.bundle.js"></script> -->
    <script src="/modules/@coreui/utils/dist/coreui-utils.js"></script>
    <!-- <script src="/js/main.js"></script> -->
    `
}

exports.Layout = Layout