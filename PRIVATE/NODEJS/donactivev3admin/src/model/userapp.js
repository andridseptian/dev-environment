const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');

const { log4js } = require('../utility/logger');
const { db_connection } = require('../database/donactiv_db');
const { rf } = require('../utility/responseFormat');
const logger = log4js.getLogger(require('path').basename(__filename))

const sequelize = db_connection;

class UserApp extends Model { }
UserApp.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    username: {
        type: DataTypes.STRING,
        unique: true
    },
    password: DataTypes.STRING,
    fullname: DataTypes.STRING,
    email: {
        type: DataTypes.STRING,
        unique: true
    },
    phone_number: {
        type: DataTypes.STRING,
        unique: true
    },
    profile_picture: DataTypes.STRING,
    status: DataTypes.STRING,
}, { sequelize, modelName: 'userapp' });

class status {
    static ACTIVE = "ACTIVE"
    static NOT_VERIFIED = "NOT_VERIFIED"
    static BLOCKED = "BLOCKED"
}

(async function () {
    logger.info(require('path').basename(__filename), " Initializing Database...");
    await sequelize.sync({ alter: true });
})();

exports.create = async function(username, password, email, phone_number, fullname) {
    try {
        var result = await UserApp.create({
            username: username,
            password: password,
            email: email,
            phone_number: phone_number,
            fullname: fullname,
            status: status.NOT_VERIFIED
        })
        return result
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByUsername = async function(username) {
    try {
        var result = await UserApp.findOne({where: {username:username}})
        return result
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByEmail = async function(email) {
    try {
        var result = await UserApp.findOne({where: {email:email}})
        return result
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByPhoneNumber = async function(phone_number) {
    try {
        var result = await UserApp.findOne({where: {phone_number:phone_number}})
        return result
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByUsernameAndPassword = async function(username, password) {
    try {
        var result = await UserApp.findOne({where: {username:username, password:password}})
        return result
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.userapp = this