const { log4js } = require('../utility/logger')
const logger = log4js.getLogger(require('path').basename(__filename))
const rf = require('../utility/responseFormat')
const df = require('../utility/dateFormat')
const md5 = require('md5')
const { userapp } = require('../model/userapp')
const { ERROR_MES, SUCCESS_MES } = require('../utility/constants')
const { renderToStream } = require('@popeindustries/lit-html-server')
const login = require('../../src/view/login/layout')
const { loginContent } = require('../../src/view/login/content')

class userController {
    static registration = async function (req, res) {
        try {
            var username = req.body.username
            var password = md5(req.body.password)
            var email = req.body.email
            var phone_number = req.body.phone_number
            var fullname = req.body.fullname

            if (rf.isNotNull(await userapp.findByUsername(username))) {
                return rf.responseWrap(ERROR_MES.ERR_USER_ALREADY_EXIST.RC, ERROR_MES.ERR_USER_ALREADY_EXIST.RM)
            }
            else if (rf.isNotNull(await userapp.findByEmail(email))) {
                return rf.responseWrap(ERROR_MES.ERR_EMAIL_ALREADY_EXIST.RC, ERROR_MES.ERR_EMAIL_ALREADY_EXIST.RM)
            }
            else {
                var userdata = await userapp.create(username, password, email, phone_number, fullname)
                var result = {
                    user: userdata.toJSON(),
                    url: "/login"
                }
                return rf.responseWrap(SUCCESS_MES.SUCCESS.RC, SUCCESS_MES.SUCCESS.RM, result)
            }
        } catch (error) {
            return rf.errorWrap(logger, error)
        }
    }

    static login = async function (req, res) {
        try {
            var username = req.body.username
            var password = md5(req.body.password)

            var result = await userapp.findByUsernameAndPassword(username, password)

            if (rf.isNull(result)) {
                return rf.responseWrap(ERROR_MES.ERR_USER_PASSWORD_NOT_FOUND.RC, ERROR_MES.ERR_USER_PASSWORD_NOT_FOUND.RM)
            } else {
                req.session.user = result.toJSON()
                return rf.responseWrap(SUCCESS_MES.SUCCESS.RC, SUCCESS_MES.SUCCESS.RM, req.session)
            }

        } catch (error) {
            return rf.errorWrap(logger, error)
        }
    }

    static logout = async function (req, res) {
        try {
            req.session.destroy(function (err) {
                // cannot access session here
                if (err) {
                    return rf.errorWrap(logger, err)
                }
            })
        } catch (error) {
            return rf.errorWrap(logger, error)
        }
    }
}

exports.userController = userController