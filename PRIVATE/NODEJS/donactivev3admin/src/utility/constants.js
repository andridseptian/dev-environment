class ERROR_MES {
    static ERR_USER_PASSWORD_NOT_FOUND = { RC: "89", RM: "Username dan Password tidak cocok" }
    static ERR_USER_ALREADY_EXIST = { RC: "88", RM: "Username sudah terdaftar" }
    static ERR_EMAIL_ALREADY_EXIST = { RC: "87", RM: "Email sudah terdaftar" }
    static ERR_PHONE_ALREADY_EXIST = { RC: "86", RM: "Nomor telepon sudah terdaftar" }
    static ERR_USER_NOT_FOUND = { RC: "86", RM: "Username tidak ditemukan" }

    static ERR_IE = { RC: "99", RM: "Terjadi kesalahan" }
    static ERR_PATH_NOT_FOUND = { RC: "98", RM: "Path tidak ditemukan" }
} exports.ERROR_MES = ERROR_MES

class SUCCESS_MES {
    static SUCCESS = { RC: "00", RM: "Sukses" }
} exports.SUCCESS_MES = SUCCESS_MES

