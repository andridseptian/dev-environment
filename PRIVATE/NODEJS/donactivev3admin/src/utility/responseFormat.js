exports.sendResponse = (response, res, logger) => {
    if (response !== "" || response !== undefined) {
        res.send(response)
    } else {
        res.send(JSON.stringify({
            rc: "99",
            rm: "RESPONSE NOT FOUND"
        }))
    }

    logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
}

exports.dataWrap = (data) => {
    if (!data.hasOwnProperty('rc')) {
        var response = {
            rc: '00',
            rm: data
        }
        return (response)
    } else {
        return data
    }
}

exports.responseWrap = (rc, rm, data) => {
    var response = {
        rc: rc,
        rm: rm,
    }

    if(this.isNotNull(data)){
        response.data = data
    }

    return (response)
}

exports.errorWrap = (logger, error) => {
    logger.error(error)
    return {
        rc: '99',
        rm: error.message,
        detail: error.original
    }
}

exports.isNotNull = (data) => {
    if (data !== null && data !== undefined && data !== "") {
        return true
    } else {
        return false
    }
}

exports.isNull = (data) => {
    if (data !== null && data !== undefined && data !== "") {
        return false
    } else {
        return true
    }
}

exports.rf = this