const { log4js } = require('../utility/logger')
const expressSession = require('express-session');
const SessionStore = require('connect-session-sequelize')(expressSession.Store);

var logger = function (params) {
    const log = log4js.getLogger(require('path').basename(__filename))
    log.info(params)
}

const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');
const db_connection = new Sequelize('postgres://postgres:postgres@localhost:5432/donactiv_db', {
    // logging: logger,
    logging: false,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: false
    }
}) // Example for postgres

db_connection.define("sessions", {
    sid: {
        type: Sequelize.STRING,
        primaryKey: true,
    },
    authid: Sequelize.STRING,
    expires: Sequelize.DATE,
    data: Sequelize.TEXT,
});

function extendDefaultFields(defaults, session) {
    return {
        data: defaults.data,
        expires: defaults.expires,
        authid: session.auth_id,
    };
}

const sequelizeSessionStore = new SessionStore({
    db: db_connection,
    table: "sessions",
    extendDefaultFields: extendDefaultFields,
});

exports.db_connection = db_connection
exports.sequelizeSessionStore = sequelizeSessionStore