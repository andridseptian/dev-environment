var express = require('express')
const { userController } = require('../controller/userController')
const { ERROR_MES } = require('../utility/constants')
var router = express.Router()
var { log4js } = require('../utility/logger')
const { rf } = require('../utility/responseFormat')
var logger = log4js.getLogger(require('path').basename(__filename))

exports.init = async function (app) {
    try {
        app.use('/api/user', router)
        router.post('/:service*', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)
                logger.info('request query: ', req.query)
                logger.info('request param: ', req.params)
                logger.info('param value: ', req.params[0].substring(1).split('/'))
                var response = rf.responseWrap(ERROR_MES.ERR_PATH_NOT_FOUND.RC, ERROR_MES.ERR_PATH_NOT_FOUND.RM)
                switch (req.params.service) {
                    case 'registration': response = (await userController.registration(req, res)); break;
                    case 'login': response = (await userController.login(req, res)); break;
                    case 'logout': response = (await userController.logout(req, res)); break;
                }
                logger.info("response: ", response)
                res.send(response)
            } catch (error) {
                logger.error(error)
                res.send(rf.responseWrap(ERROR_MES.ERR_IE.RC, ERROR_MES.ERR_IE.RM))
            }
        })
    } catch (error) {
        logger.error(error)
    }
}
exports.userRouter = this