var express = require('express')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))
const { Layout } = require('../view/home/layout')
const { home } = require('../view/home/home')
const { renderToStream } = require('@popeindustries/lit-html-server')

exports.init = async function (app) {
    try {
        app.use('/', router)
        router.get('/:page*', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request query: ', req.query)
                logger.info('request param: ', req.params)
                logger.info('param value: ', req.params[0].substring(1).split('/'))

                switch (req.params.page) {
                    case 'dashboard': renderToStream(Layout(home())).pipe(res); break;
                    case 'login': renderToStream(Layout(home())).pipe(res); break;
                    case 'register': renderToStream(Layout(home())).pipe(res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })
    } catch (error) {
        logger.error(error)
    }
}

exports.pageRouter = this