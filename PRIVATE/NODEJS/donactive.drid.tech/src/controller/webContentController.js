const { getLogger } = require("log4js")
const { webContent } = require("../database/model/content")
const { rf } = require("../utility/responseFormat")
var logger = getLogger(require('path').basename(__filename))

class content {
    static createContent = async function (req, res) {
        try {
            var id = req.body.id
            var title = req.body.title
            var description = req.body.description
            var content = req.body.content
            var result = await webContent.create(id, title, description, content)
            return rf.dataWrap(result)
        } catch (error) {
            return rf.errorWrap(logger, error)
        }
    }
    static getAllContent = async function (req, res) {
        try {
            var result = await webContent.selectAll()
            return rf.dataWrap(result)
        } catch (error) {
            return rf.errorWrap(logger, error)
        }
    }
}

exports.content = content