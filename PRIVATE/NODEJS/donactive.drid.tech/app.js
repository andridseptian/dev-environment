function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function start() {

  //#region check console arguments
  var args = {}
  for (let index = 0; index < process.argv.length; index++) {
    const val = process.argv[index];
    console.log(index + ': ' + val);
    var argument = val.split('=');
    if (argument[1]) args[argument[0]] = argument[1];
  }
  console.log(args);
  var appconfig;
  try {
    appconfig = require(args.configPath)
  } catch (error) {
    // console.error(error);
    try {
      if (args.dev) {
        appconfig = require('./config/app-config-dev.json')
        console.log("development mode");
      } else {
        appconfig = require('./config/app-config.json')
        console.log("production mode");
      }
    } catch (error) {
      console.log(`config file not found in '${args.configPath}' and './config/app-config.json', please specify your config file location using 'configPath' argument`)
      throw error;
    }
  }
  //#endregion
  exports.appconfig = appconfig

  const express = require('express')
  // const { contentRouter } = require('./src/router/contentRouter')
  const { pageRouter } = require('./src/router/pageRouter')
  const { log4js } = require('./src/utility/logger')
  var logger = log4js.getLogger(require('path').basename(__filename))
  logger.info("appconfig: " + JSON.stringify(appconfig))

  const app = express()
  var port = 3000


  // parse application/x-www-form-urlencoded
  app.use(express.urlencoded({ extended: true }))
  // parse application/json
  app.use(express.json())

  app.use('/', express.static('public/'))

  // app.get('/', function (req, res) {
  //   res.redirect("/page/home")
  // })

  pageRouter.init(app)
  // contentRouter.init(app)

  app.listen(port, () => {
    console.log(`listening at http://localhost:${port}`)
  })
} start()




