const { initModels, db } = require("../model/_initmodels")
const { RESP } = require("../utility/constants")
// const { log4js } = require("../utility/logger")
// const logger = log4js.getLogger(require('path').basename(__filename))
const models = initModels()


exports.getDonasi = async function (req, res) {
    try {
        console.log("--> Get All Donasi")
        var donasi = await models.donasi.findAll()
        // console.log(JSON.stringify(donasi))
        return donasi
    } catch (error) {
        console.error(error)
        return RESP.IE(error.message)
    }
}

exports.getDonasiCategory = async function (req, res) {
    try {
        console.log("--> Get Donasi category")
        var [donasi, raw] = await db.query("select d.category from donasi d group by d.category")
        // console.log(JSON.stringify(donasi))
        var category = []
        for (let index = 0; index < donasi.length; index++) {
            const element = donasi[index];
            category.push(element.category)
        }
        return category
    } catch (error) {
        console.error(error)
        return RESP.IE(error.message)
    }
}

exports.addDonasi = async function (req, res) {
    try {
        console.log("--> Add New Donasi")

        var category = req.body.category
        var amount = req.body.amount
        var description = req.body.description

        var donasi = await models.donasi.create({
            category: category,
            amount: amount,
            description: description
        })

        req.logActivity.username = req.session.user ? req.session.user : "developer"
        req.logActivity.description = (req.session.user ? req.session.user : "developer")
            + " menambahkan donasi di kategori " + category + " sejumlah Rp. " + amount
        // console.log(JSON.stringify(donasi))
        return RESP.SUCCESS(donasi)
    } catch (error) {
        console.error(error)
        return RESP.IE(error.message)
    }
}