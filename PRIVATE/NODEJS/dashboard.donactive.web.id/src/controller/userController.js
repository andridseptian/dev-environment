const { initModels } = require("../model/_initmodels")
const { RESP } = require("../utility/constants")
// const { log4js } = require("../utility/logger")
// const logger = log4js.getLogger(require('path').basename(__filename))
const models = initModels()

exports.userlogin = async function (req, res) {
    try {
        console.log("--> User Login")
        var user = req.query.user
        var password = req.query.password
        var userapp = await models.userapp.findOne({ where: { username: user, password: password } })
        console.log(userapp)
        if (userapp) {
            console.log(req.session)
            req.session.user = userapp.username
            req.logActivity.username = userapp.username
            req.logActivity.description = userapp.username + " melakukan login"
            return RESP.SUCCESS(userapp)
        } else {
            return RESP.NOTMATCH
        }
    } catch (error) {
        console.error(error)
        return RESP.IE(error.message)
    }
}

exports.userlogout = async function (req, res) {
    try {
        console.log("--> User Logout")
        req.session.destroy(function (err) {
            if (err) throw err
            // cannot access session here
        })
        return {
            redirect: true,
            url: "/login",
        }
    } catch (error) {
        console.error(error)
        return RESP.IE(error.message)
    }
}