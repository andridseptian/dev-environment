function responseBuilder(rc, rm, info) {
    return {
        rc: rc,
        rm: rm,
        data: info
    }
}

class RESP {
    static IE = function (info) { return responseBuilder("99", "INTERNAL SERVER ERROR", info) }
    static PATH_NOT_FOUND = responseBuilder("98", "PATH_NOT_FOUND")
    static NOT_AVAILABLE = responseBuilder("97", "NOT AVAILABLE")
    static FAILED = responseBuilder("97", "FAILED")

    static SUCCESS = responseBuilder("00", "SUCCESS")
    static SUCCESS = function (data) { return responseBuilder("00", "SUCCESS", data) }
    static NOTFOUND = responseBuilder("01", "NOTFOUND")
    static NOTMATCH = responseBuilder("02", "NOTMATCH")
    static MISSING = responseBuilder("03", "MISSING")
} exports.RESP = RESP