const { html } = require("@popeindustries/lit-html-server");

function sidebar() {
    return html`
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Menu</div>
                        <a class="nav-link" href="/">
                            <div class="sb-nav-link-icon"><i class="fa fa-home"></i></div>
                            Dashboard
                        </a>
                        <a class="nav-link" href="/donasi">
                            <div class="sb-nav-link-icon"><i class="fa fa-gift"></i></div>
                            Donasi
                        </a>
                        <a class="nav-link" href="/kegiatan">
                            <div class="sb-nav-link-icon"><i class="fa fa-tag"></i></div>
                            Kegiatan
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    `
} exports.sidebar = sidebar