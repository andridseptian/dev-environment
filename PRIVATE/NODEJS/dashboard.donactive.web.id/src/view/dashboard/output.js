const { comingsoon } = require("./content/comingsoon");
const { dashboard } = require("./content/dashboard");
const { donasi } = require("./content/donasi");
const { Layout } = require("./layout");
const { sidebar } = require("./sidebar");
const { topbar } = require("./topbar");

exports.dashboard = {
    layout: Layout,
    content: { 
        comingsoon: comingsoon,
        dashboard: dashboard,
        donasi: donasi
    }
}