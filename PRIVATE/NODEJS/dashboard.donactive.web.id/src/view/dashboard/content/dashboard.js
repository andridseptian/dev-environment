const { html } = require("@popeindustries/lit-html-server");
const { Op } = require("sequelize");
const { initModels } = require("../../../model/_initmodels");
const models = initModels()
const df = require('date-fns')



async function dashboard() {

    var donasiData = await models.donasi.findAll();

    var totalDonasi = 0
    for (let index = 0; index < donasiData.length; index++) {
        const element = donasiData[index];
        totalDonasi += element.amount
    }

    return html`
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Dashboard</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
                <div class="row justify-content-md-center mb-3">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4">
                            <div class="card-body">Jumlah Total Donasi</div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="/donasi">Rp. ${totalDonasi}</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive text-center">
                            <p>Selamat datang di Dashboard Website Donactive</p>
                            <p>Didashboard ini admin bisa melakukan pengaturan untuk website donactive</p>
                        </div>
                    </div>
                </div>
                <div class="card mb-4">
                <div class="card-header">
                        <i class="fas fa-calendar mr-1"></i>
                        TIMELINE
                    </div>
                    <div class="card-body">
                        ${getActivity()}
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Donactive 2020</div>
                </div>
            </div>
        </footer>
    </div>
    `
} exports.dashboard = dashboard

async function getActivity(){
    var activityData = await models.activity.findAll({where: {description: {[Op.ne]: null}}})
    activityData = JSON.stringify(activityData)
    activityData = JSON.parse(activityData)

    return html`
    <div>
        ${activityData.map(function(item){
            return (
                html`
                <div class="row justify-content-center">
                    <div class="col col-md-8 my-2 text-center">
                        <small>
                            <i class="fas fa-clock mr-1"></i>
                            ${df.formatDistanceToNowStrict(new Date(item.cr_time), {includeSeconds: true, addSuffix: true})}
                        </small> <br> 
                        <!-- <i class="fas fa-bell mr-1"></i> -->
                        ${item.description}
                        <hr>    
                    </div>
                </div>
                `
            )
        })}
    </div>
    
    `
}