const { html } = require("@popeindustries/lit-html-server");
const donasiData = require("../../../controller/donasiController")

async function donasi() {
    var dataCategory = await donasiData.getDonasiCategory()
    return html`
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Donasi</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Dashboard</li>
                    <li class="breadcrumb-item active">Donasi</li>
                </ol>
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        DATA DONASI
                    </div>
                    <div class="card-body">
                        <button id="add-donasi" type="button" class="btn btn-sm btn-primary float-right mt-3 mb-3 ml-3">Tambah Donasi</button>
                        <div class="btn-group float-right mt-3 mb-3 ml-3" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Kategori
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            ${dataCategory.map(item => html`
                                <a class="dropdown-item" href="#">${item}</a>
                            `)}
                            </div>
                        </div>
                        <div class="table-responsive">
                            ${tableDonasi()}
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Donactive 2020</div>
                </div>
            </div>
        </footer>
    </div>

    ${script()}
    `
} exports.donasi = donasi

function script(){
    return html`
    <script type="module" src="/js/dashboard/donasi.js"></script>
    `
}

async function tableDonasi() {

    var donasilist = await donasiData.getDonasi()

    donasilist = JSON.stringify(donasilist)
    donasilist = JSON.parse(donasilist)

    // console.log("list donasi: ",donasilist);

    return html`
        <div>
            <table class="table table-dark table-borderless rounded-lg">
                <thead>
                    <tr>
                        <th scope="col" style="width:100px"></th>
                        <th scope="col">ID</th>
                        <th scope="col">Waktu</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Bukti</th>
                    </tr>
                </thead>
                <tbody>
                    ${donasilist.map(function (item) {
                        return (
                            html`
                                <tr>
                                    <td scope="row">
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </td>
                                    <td>${item.id}</td>
                                    <td>${item.cr_time.replace("T", " ").replace("Z", " ").substring(0,16)}</td>
                                    <td>${item.category}</td>
                                    <td>${item.amount}</td>
                                    <td>${item.description}</td>
                                    <td>${item.proof ? item.proof : "tidak ada"}</td>
                                </tr>
                        `)
                    })}
                </tbody>
            </table>
        </div>
    `
}