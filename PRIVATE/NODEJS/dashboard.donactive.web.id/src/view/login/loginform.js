const { html } = require("@popeindustries/lit-html-server");

function loginform() {
    return html`
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header text-center">
                                        <small class="font-weight-light">Donactive Dashboard</small>
                                        <h3 class="font-weight-light">Login</h3>
                                    </div>
                                    <div class="card-body">
                                        <form>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Username / Email</label>
                                                <input id="login-user" class="form-control py-4" id="inputEmailAddress"
                                                    type="email" placeholder="Enter username / email address" />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputPassword">Password</label>
                                                <input id="login-password" class="form-control py-4" id="inputPassword"
                                                    type="password" placeholder="Enter password" />
                                            </div>
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <a id="login-submit" class="btn btn-block btn-primary">Login</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    `
} exports.loginform = loginform