const { Layout } = require("./layout");
const { loginform } = require("./loginform");

exports.login = {
    layout: Layout,
    loginform: loginform
}