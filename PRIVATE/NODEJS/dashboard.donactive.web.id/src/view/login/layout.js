const { html } = require('@popeindustries/lit-html-server');

function Layout(body) {
    return html`
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Donactive Peduli Indonesia | DASHBOARD LOGIN</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="../node_modules/@fortawesome/fontawesome-free/js/all.js"></script>
    </head>
    
    <body class="bg-primary">
    
        ${toast()}
        ${body}
        ${script()}
    </body>
    
    </html>
  `;
}

function script() {
    return html`
        <script src="../node_modules/jquery/dist/jquery.js"></script>
        <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/md5.js"></script>
        <script type="module" src="js/login/login.js"></script>
    `
}

function toast() {
    return html`
    <div class="position-fixed top-0 right-0 p-3" style="z-index: 5; right: 0; top: 0;">
        <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
            <div class="toast-header bg-primary">
                <!-- <img src="..." class="rounded mr-2" alt="..."> -->
                <strong class="mr-auto">Info</strong>
                <!-- <small>now</small> -->
                <button type="button" class="ml-2 mb-1 close ml-auto" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Hello, world! This is a toast message.
            </div>
        </div>
    </div>
    `
}

exports.Layout = Layout;