const express = require('express')
const router = express.Router()
// const { log4js } = require('../utility/logger')
// const logger = log4js.getLogger(require('path').basename(__filename))
const { RESP } = require('../utility/constants')
const { initModels } = require("../model/_initmodels")
const models = initModels()
const userController = require('../controller/userController')
const donasiController = require('../controller/donasiController')

exports.init = async function (app) {
    try {
        console.info("router initials")
        app.use("/service*", async function (req, res, next) {
            console.info("-->incoming req")
            console.info('request method : ', req.method)
            console.info('request path   : ', req.path)
            console.info('request query  : ', JSON.stringify(req.query))
            console.info('request body   : ', JSON.stringify(req.body))
            console.info('request param  : ', req.params)

            var logActivity = await models.activity.create({
                path: req.params["0"],
                request: JSON.stringify(req.method === "POST" ? req.body : req.query)
            })

            req.logActivity = logActivity

            var response = RESP.PATH_NOT_FOUND
            switch (req.params["0"]) {
                // User Path
                case '/user/login': response = await userController.userlogin(req, res); break;
                case '/user/logout': response = await userController.userlogout(req, res); break;
                case '/user/regist': response = RESP.NOT_AVAILABLE; break;
                case '/user/edit': response = RESP.NOT_AVAILABLE; break;

                // Donasi Path
                case '/donasi/get/all': response = await donasiController.getDonasi(req, res); break;
                case '/donasi/get/category': response = await donasiController.getDonasiCategory(req, res); break;
                case '/donasi/add': response = await donasiController.addDonasi(req, res); break;


            }
            // console.log("response: ", response.redirect);
            if (response.redirect) {
                res.redirect(response.url)
                console.info("<--outgoing res: redirect")
                console.info('request path   : ', req.path)
                console.info('request query  : ', JSON.stringify(req.query))
                console.info('request body   : ', JSON.stringify(req.body))
                console.info("redirect path  : ", response.url)

            } else {
                res.send(response)
                console.info("<--outgoing res")
                console.info('request path   : ', req.path)
                console.info('request query  : ', JSON.stringify(req.query))
                console.info('request body   : ', JSON.stringify(req.body))
                console.info('response body  : ', JSON.stringify(response))
                logActivity.response = JSON.stringify(response)
                logActivity.save()
            }
        })
    } catch (error) {
        console.error(error)
    }
}

exports.userRoutes = this