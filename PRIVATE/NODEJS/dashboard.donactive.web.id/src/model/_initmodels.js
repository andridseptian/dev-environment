// const { log4js } = require('../utility/logger')
// const logtemp = log4js.getLogger(require('path').basename(__filename))
// var logger = function (params) {
//     logtemp.info(params)
// }
const expressSession = require('express-session');
const SessionStore = require('connect-session-sequelize')(expressSession.Store);
const { Sequelize } = require('sequelize');
const config = require('../../config.json')
const db_connection = new Sequelize(config.database.db_name, config.database.username, config.database.password,{
    dialect: 'postgres',
    host: config.database.host,
    port: config.database.port,
    logging: false,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: false
    }
}) // Example for postgres

var _userapp = require("./userapp");
var _session = require("./session");
var _donasi = require("./donasi");
var _activity = require("./activity");



function initModels() {

    function extendDefaultFields(defaults) {
        return {
            data: defaults.data,
            expires: defaults.expires
        };
    }
    const sequelizeSessionStore = new SessionStore({
        db: db_connection,
        table: "session",
        extendDefaultFields: extendDefaultFields,
    });


    var userapp = _userapp(db_connection);
    var session = _session(db_connection);
    var donasi = _donasi(db_connection);
    var activity = _activity(db_connection);
    return {
        userapp,
        session,
        donasi,
        activity,
        sequelizeSessionStore
    };
}

module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
module.exports.db = db_connection;