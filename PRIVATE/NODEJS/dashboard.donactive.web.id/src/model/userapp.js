const Sequelize = require('sequelize');
module.exports = (sequelize) => {
    return userapp.init(sequelize);
}

class userapp extends Sequelize.Model {
    static init(sequelize) {
        super.init({
            id: {
                type: Sequelize.Sequelize.DataTypes.BIGINT,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            cr_time: {
                type: Sequelize.DataTypes.DATE,
                defaultValue: Sequelize.DataTypes.NOW
            },
            mod_time: {
                type: Sequelize.DataTypes.DATE,
                defaultValue: Sequelize.DataTypes.NOW,
                onUpdate: Sequelize.DataTypes.NOW
            },
            username: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            password: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
            fullname: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
            email: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
            generatedemail: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
            },
            phonenumber: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
            user_type: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
            status: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
        }, {
            sequelize,
            tableName: 'userapp',
            schema: 'public',
            timestamps: false,
            indexes: [
                {
                    name: "userapp_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]
                },
            ]
        });
        return userapp;
    }
}
