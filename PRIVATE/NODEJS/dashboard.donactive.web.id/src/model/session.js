const Sequelize = require('sequelize');
module.exports = (sequelize) => {
    return session.init(sequelize);
}

class session extends Sequelize.Model {
    static init(sequelize) {
        super.init({
            sid: {
                type: Sequelize.STRING,
                primaryKey: true,
            },
            expires: Sequelize.DATE,
            data: Sequelize.TEXT,
        }, {
            sequelize,
            tableName: 'session',
            schema: 'public',
            timestamps: false,
            indexes: [
                {
                    name: "session_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]
                },
            ]
        });
        return session;
    }
}