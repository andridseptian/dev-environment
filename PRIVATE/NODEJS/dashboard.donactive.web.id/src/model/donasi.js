const Sequelize = require('sequelize');
module.exports = (sequelize) => {
    return donasi.init(sequelize);
}

class donasi extends Sequelize.Model {
    static init(sequelize) {
        super.init({
            id: {
                type: Sequelize.BIGINT,
                primaryKey: true,
                autoIncrement: true
            },
            cr_time: {
                type: Sequelize.DataTypes.DATE,
                defaultValue: Sequelize.DataTypes.NOW
            },
            mod_time: {
                type: Sequelize.DataTypes.DATE,
                defaultValue: Sequelize.DataTypes.NOW,
                onUpdate: Sequelize.DataTypes.NOW
            },
            category: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
            },
            amount: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: false,
            },
            description: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
            },
            proof: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
            }
        }, {
            sequelize,
            tableName: 'donasi',
            schema: 'public',
            timestamps: false,
            indexes: [
                {
                    name: "donasi_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]
                },
            ]
        });
        return donasi;
    }
}