const Sequelize = require('sequelize');
module.exports = (sequelize) => {
    return activity.init(sequelize);
}

class activity extends Sequelize.Model {
    static init(sequelize) {
        super.init({
            id: {
                type: Sequelize.BIGINT,
                primaryKey: true,
                autoIncrement: true
            },
            cr_time: {
                type: Sequelize.DataTypes.DATE,
                defaultValue: Sequelize.DataTypes.NOW
            },
            path: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
            },
            request: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
            },
            response: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
            },
            username: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
            },
            description: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
            },
            additional: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
            }
        }, {
            sequelize,
            tableName: 'activity',
            schema: 'public',
            timestamps: false,
            indexes: [
                {
                    name: "activity_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]
                },
            ]
        });
        return activity;
    }
}