
const express = require('express')
// const { log4js } = require('./src/utility/logger')
// const logger = log4js.getLogger(require('path').basename(__filename))
const app = express()
const cookieParser = require('cookie-parser');
const expressSession = require('express-session');
const SessionStore = require('connect-session-sequelize')(expressSession.Store);
const port = 3000
const { renderToStream, html } = require('@popeindustries/lit-html-server')
const { dashboard } = require('./src/view/dashboard/output')
const { login } = require('./src/view/login/output')
const { userRoutes } = require('./src/routes/serviceRoutes');
const config = require('./config.json');
const { forwarding } = require('./forwarding');
const { db } = require('./src/model/_initmodels');

function extendDefaultFields(defaults) {
    return {
        data: defaults.data,
        expires: defaults.expires
    };
}
const sequelizeSessionStore = new SessionStore({
    db: db,
    table: "session",
    extendDefaultFields: extendDefaultFields,
});

async function start() {
    if (config.database) {
        console.log("Connecting to forwarding");
        await forwarding()
    }
    // parse application/x-www-form-urlencoded
    app.use(express.urlencoded({ extended: true }))
    // parse application/json
    app.use(express.json())

    app.use(cookieParser());
    app.use(expressSession({
        secret: 'secretkeepsecret',
        store: sequelizeSessionStore,
        resave: false,
        saveUninitialized: false,
    }));

    app.use('/', express.static('public/'))
    app.use('/node_modules', express.static('node_modules/'))

    // app.use(function (req, res, next) {
    //     logger.info("incoming req")
    //     logger.info('request path: ', req.path)
    //     logger.info('request query: ', req.query)
    //     logger.info('request param: ', req.params)
    //     next()
    // })

    app.get('/', function (req, res) {
        if (req.session.user) {
            renderToStream(dashboard.layout(dashboard.content.dashboard())).pipe(res)
        } else {
            res.redirect("/login")
        }
    })
    app.get('/donasi', function (req, res) {
        if (req.session.user) {
            renderToStream(dashboard.layout(dashboard.content.donasi())).pipe(res)
        } else {
            res.redirect("/login")
        }
    })
    app.get('/kegiatan', function (req, res) {
        if (req.session.user) {
            renderToStream(dashboard.layout(dashboard.content.comingsoon())).pipe(res)
        } else {
            res.redirect("/login")
        }
    })

    app.get('/login', function (req, res) {
        renderToStream(login.layout(login.loginform())).pipe(res)
    })

    userRoutes.init(app)


    // app.get('*', function (req, res) {
    //     logger.info("init 404")
    //     res.status(404).redirect('404.html');
    // });

    // app.use(function (req, res, next) {
    //     logger.info("outgoing res")
    //     logger.info('response: ', res)
    //     next()
    // })

    app.listen(port, () => {
        console.log(`listening at http://localhost:${port}`)
    })
}

start();








