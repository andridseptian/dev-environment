const { initModels } = require("./src/model/_initmodels");

(async function () {
    const model = initModels()
    await model.userapp.sync({ alter: true })
    await model.session.sync({ alter: true })
    await model.donasi.sync({ alter: true })
    await model.activity.sync({ alter: true })

    console.log("\n\n\nupdate completed");
})()