import { toastShow } from "./toast.js"

document.getElementById("login-submit").onclick = async function () {
    this.innerHTML = `
    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    Loading...
    `
    this.disabled = true

    var user = document.getElementById("login-user").value
    var password = md5(document.getElementById("login-password").value, null, null)
    // var password = CryptoJS.MD5(document.getElementById("login-password").value);

    var result = await new Promise(function (resolve, reject) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        fetch(`/service/user/login?user=${user}&password=${password}`, requestOptions)
            .then(response => response.text())
            .then(result => resolve(result))
            .catch(error => reject(error));
    })

    setTimeout(() => {
        console.log(result);
        if (JSON.parse(result).rc === "00") {
            window.location.href = "/"
        } else {
            toastShow(JSON.parse(result).rm)
            // alert(JSON.parse(result).rm)
        }
        this.innerHTML = `
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        Mengarahkan Ke Dashboard...
        `
    }, 1000);
}