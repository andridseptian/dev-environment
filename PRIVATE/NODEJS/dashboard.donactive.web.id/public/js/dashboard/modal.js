import { html, render } from '/node_modules/lit-html/lit-html.js';

export function setHeader(body) {
    render(body, document.getElementById("modal-header"))
}
export function setTitle() {
    render(body, document.getElementById("modal-title"))
}
export function setBody(body) {
    render(body, document.getElementById("modal-body"))

}
export function setFooter(body) {
    render(body, document.getElementById("modal-footer"))
}
export function show() {
    $('#modal-loading-container').modal('hide')
    $('#modal-container').modal('show')
}

export function hide() {
    $('#modal-container').modal('hide')
}

export function showConfirm(confirmBody, callback, arg1, arg2, arg3) {
    setHeader("Konfirmasi")
    setBody(confirmBody)
    setFooter(html`
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
        <button id="modal-confirm" type="button" class="btn btn-primary">Iya</button>
    `)
    document.getElementById("modal-confirm").onclick = function () {
        hide()
        callback(arg1, arg2, arg3)
    }
    show()
}

export function loadingShow(message) {
    render(message, document.getElementById("modal-loading-message"))
    $('#modal-loading-container').modal('show')
}

export function loadingClose(message) {
    $('#modal-loading-container').modal('hide')
}

