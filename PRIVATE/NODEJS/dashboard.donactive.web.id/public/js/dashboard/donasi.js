import { html, render } from '/node_modules/lit-html/lit-html.js';
import * as modal from '../../js/dashboard/modal.js'
import { toastShow } from '../../js/dashboard/toast.js';

document.getElementById("add-donasi").onclick = async function () {
    // alert("clicked")
    // toastShow("kategori dan jumlah harus diisi")

    modal.loadingShow("Memuat Form Donasi...")

    var settings = {
        "url": "/service/donasi/get/category",
        "method": "GET",
        "timeout": 0,
    };
    var dataCategory = await $.ajax(settings)

    modal.setBody(html`
    <div class="container">
        <form>
            <div class="form-group">
                <label>Kategori Donasi</label>
                <select id="donasi-kategori-list" class="form-control">
                    ${dataCategory.map(item => html`
                    <option value="${item}">${item}</option>
                    `)}
                    <option value="input-add"><i>Tambah Kategori...</i></option>
                </select>
                <input id="input-donasi-kategori-baru" type="text" class="form-control my-3" placeholder="Kategori Baru"
                    style="display:none">
            </div>
            <div class="form-group">
                <label>Jumlah</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Rp.</div>
                    </div>
                    <input id="input-donasi-jumlah" type="number" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input id="input-donasi-keterangan" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">Bukti Donasi</label>
                <input type="file" class="form-control-file" id="buktiDonasi">
                <small><i>upload bukti donasi jika ada</i></small>
                <img id="uploadedBukti" src="#" alt="bukti donasi" />
            </div>
        </form>
    </div>
    `)
    modal.setFooter(
        html`
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button id="input-donasi-submit" type="button" class="btn btn-primary">Input Donasi</button>
        `
    )
    modal.show()

    document.getElementById("donasi-kategori-list").onclick = function () {
        // alert(this.value)
        if (this.value === "input-add") {
            document.getElementById("input-donasi-kategori-baru").style.display = "block"
        } else {
            document.getElementById("input-donasi-kategori-baru").style.display = "none"

        }
    }

    document.getElementById("input-donasi-submit").onclick = function () {
        var category = $("#input-donasi-kategori").val()
        var amount = $("#input-donasi-jumlah").val()
        var description = $("#input-donasi-keterangan").val()
        console.log(category);
        console.log(amount);
        console.log(description);
        var raw = JSON.stringify({
            "category": category,
            "amount": amount,
            "description": description
        });
        if ($("#input-donasi-kategori").val() === "") {
            toastShow("kategori dan jumlah harus diisi")
        } else {
            modal.showConfirm(`Anda akan menambahkan Rp. ${$("#input-donasi-jumlah").val()} ke kategori ${$("#input-donasi-kategori").val()}`, inputDonasi, raw)
        }

    }

    function inputDonasi(raw) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        console.log(raw)

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("/service/donasi/add", requestOptions)
            .then(response => response.text())
            .then(result => toastShow("Berhasil Menambahkan Donasi"))
            .catch(error => toastShow(error));

        setTimeout(() => {
            window.location.reload()
        }, 1000);


    }
}