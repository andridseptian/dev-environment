(function () {
    AOS.init();
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("mainNav").style.top = "0";
        } else {
            document.getElementById("mainNav").style.top = "-65px";
        }
        prevScrollpos = currentScrollPos;
    }
})()