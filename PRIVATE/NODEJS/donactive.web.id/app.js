const express = require('express')
// const { contentRouter } = require('./src/router/contentRouter')
const { pageRouter } = require('./src/router/pageRouter')
const { log4js } = require('./src/utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))

const app = express()
var port = 3000

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))
// parse application/json
app.use(express.json())

app.use('/', express.static('public/'))

// app.get('/', function (req, res) {
//   res.redirect("/page/home")
// })

pageRouter.init(app)
// contentRouter.init(app)

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`)
})




