var path = require('path')
var date = require('./dateFormat')
var log4js = require('log4js')

log4js.configure({
    appenders: {
        out: { type: 'stdout' },
        app: { type: 'file', filename: path.join(__dirname, '../../log', `app-${date.getNowDate()}.log`) }
    },
    categories: {
        default: { appenders: ['out', 'app'], level: 'all' }
    }
});

const logger = log4js.getLogger();

function info(params) {
    logger.info(params)
}

exports.logger = logger
exports.log4js = log4js
exports.info = info