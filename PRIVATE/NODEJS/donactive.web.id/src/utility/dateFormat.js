exports.getNowDate = function () {
    var year = new Date().getFullYear()
    var month = (new Date().getMonth() + 1).toString().padStart(2, '0')
    var date = (new Date().getDate() + 1).toString().padStart(2, '0')
    return `${year}-${month}-${date}`
}

exports.getNowDateTimePlain = function () {
    var timestamp =
        (new Date().toISOString()
            .replace(/-/g, '')
            .replace(/:/g, '')
            .replace(/\./g, '')
            .replace(/T/g, '')
            .replace(/Z/g, '')
        )
    return timestamp
}

exports.getTimeID = () => {
    var year = new Date().getFullYear().toString().substring(2)
    var month = (new Date().getMonth() + 1).toString().padStart(2, "0")
    var date = (new Date().getDate()).toString().padStart(2, "0")
    var hour = (new Date().getHours()).toString().padStart(2, "0")
    var minutes = (new Date().getMinutes()).toString().padStart(2, "0")
    var seconds = (new Date().getSeconds()).toString().padStart(3, "0")
    var milliseconds = (new Date().getMilliseconds()).toString().padStart(3, "0")
    var result = year + month + date + hour + minutes + seconds + milliseconds
    return result;
}

exports.generateRandomNDigits = async (n) => {
    return await Math.floor(Math.random() * (9 * (Math.pow(10, n)))) + (Math.pow(10, n));
}