const { html } = require('@popeindustries/lit-html-server');
const { header } = require('./header');
const { navbar } = require('./navbar');
const { sectionLayout, sectionBody } = require('./section');

async function home() {
    return html`
        ${navbar()}
        ${header()}
        ${sectionLayout("tentang","Kenali Kami", sectionBody.about(), 'dark', 'light')}
        ${sectionLayout("kenapa","Kenapa Kami Ada", sectionBody.why(), 'light', 'primary')}
        ${sectionLayout("donasi","Donasi", sectionBody.donate(), 'dark', 'light')}
        ${sectionLayout("contact","Kontak Kami", sectionBody.contact(), 'light', 'dark')}
    `
}

exports.home = home