const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');
const db_connection = new Sequelize('postgres://postgres:4k535r00t@51.79.219.4:5432/lakupandai_simulator', {
    // logging: false,
    define: {
        //prevent sequelize from pluralizing table names 
        freezeTableName: true,
        timestamps: false
    }
}) // Example for postgres

// var initModels = require("./src/models/init-models");
var models = require("./src/models/init-models").initModels()

async function getall() {
   var agent = await models.agent.findAll();
   console.log(agent)
}

getall()

