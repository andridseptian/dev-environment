const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return agent.init(sequelize, DataTypes);
}

class agent extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      type: DataTypes.STRING(10),
      allowNull: false,
      primaryKey: true
    },
    account_mother_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    account_number: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    agent_type: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    dob: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    npwp: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone_number: {
      type: DataTypes.STRING(14),
      allowNull: true
    },
    saldo: {
      type: DataTypes.BIGINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'agent',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "agent_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return agent;
  }
}
