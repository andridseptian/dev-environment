var DataTypes = require("sequelize").DataTypes;
var Sequelize = require("sequelize").Sequelize;
var _SequelizeMeta = require("./SequelizeMeta");
var _agent = require("./agent");
var _dukcapil = require("./dukcapil");
var _log_activity = require("./log_activity");
var _nasabah = require("./nasabah");
var _product = require("./product");
var _sms = require("./sms");
const sequelize = new Sequelize('postgres://postgres:4k535r00t@51.79.219.4:5432/lakupandai_simulator', {
  // logging: false,
  define: {
    //prevent sequelize from pluralizing table names 
    freezeTableName: true,
    timestamps: false
  }
}) // Example for postgres

function initModels() {
  var SequelizeMeta = _SequelizeMeta(sequelize, DataTypes);
  var agent = _agent(sequelize, DataTypes);
  var dukcapil = _dukcapil(sequelize, DataTypes);
  var log_activity = _log_activity(sequelize, DataTypes);
  var nasabah = _nasabah(sequelize, DataTypes);
  var product = _product(sequelize, DataTypes);
  var sms = _sms(sequelize, DataTypes);


  return {
    SequelizeMeta,
    agent,
    dukcapil,
    log_activity,
    nasabah,
    product,
    sms,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
