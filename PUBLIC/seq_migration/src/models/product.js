const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return product.init(sequelize, DataTypes);
}

class product extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    additional_data: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    id_billing: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    product_code: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    product_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    transaction_amount: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    transaction_type: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    response_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    response_desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'product',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "product_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return product;
  }
}
