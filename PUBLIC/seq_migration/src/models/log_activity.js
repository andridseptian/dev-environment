const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return log_activity.init(sequelize, DataTypes);
}

class log_activity extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    dtm_created_msg: {
      type: DataTypes.DATE,
      allowNull: true
    },
    request: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    response: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'log_activity',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "log_activity_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return log_activity;
  }
}
