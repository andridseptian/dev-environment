const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return sms.init(sequelize, DataTypes);
}

class sms extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    cr_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_time: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    destination_number: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'sms',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "sms_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return sms;
  }
}
