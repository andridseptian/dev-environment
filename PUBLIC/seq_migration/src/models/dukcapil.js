const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return dukcapil.init(sequelize, DataTypes);
}

class dukcapil extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    nik: {
      type: DataTypes.STRING(16),
      allowNull: false,
      primaryKey: true
    },
    dob: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    mother_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'dukcapil',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "dukcapil_pkey",
        unique: true,
        fields: [
          { name: "nik" },
        ]
      },
    ]
  });
  return dukcapil;
  }
}
