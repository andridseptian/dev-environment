TRANSFER OFF US
# SERVICE
## Penambahan class baru ##
    -> FormatKonfirmasiInquiryTransferOffUs.java
    -> FormatInquiryTransferOffUs.java
    -> FormatInquiryTokenTransferOffUs.java
    -> GetInquiryDataTransferOffUs.java

## Penambahan alur trx-mgr ##

---> property side

<!--Transfer Nasabah Off Us -->
<property name="0200431100" value="CHECK_AGENT CHECK_NASABAH CHECK_STATUS_NASABAH_BANK CHECK_PRODUCT CHECK_TARGET INQUIRY_TRANSFER_ON_US SEND_ISO_RESPONSE LOG_SESSION" />
<property name="0200431200" value="CHECK_AGENT_ID_PIN0 CHECK_OTP_BY_ISO CHECK_PRODUCT GET_INQUIRY_DATA INQUIRY_OTP_TRANSFER_ON_US SEND_ISO_RESPONSE LOG_TRANSACTION LOG_SESSION" />
<property name="0200431300" value="CHECK_AGENT CHECK_PRODUCT CHECK_OTP INQUIRY_TOKEN_TRANSFER_ON_US SEND_ISO_RESPONSE LOG_SESSION" />
<property name="0200431400" value="CHECK_AGENT_ID_PIN0 GET_INQUIRY_DATA_TRANSFER_ON_US CEK_SALDO_AGENT_TRANSFER CHECK_PRODUCT TRX_TRANSFER_ON_US SEND_ISO_RESPONSE LOG_TRANSACTION LOG_SESSION" />

--> group side

<group name="INQUIRY_TRANSFER_OFF_US">
    <participant class="com.acs.lkpd.participant.format.FormatKonfirmasiInquiryTransferOnUs"/>
    <participant class="com.acs.lkpd.participant.process.GenerateOTPParticipant" />
</group>

<group name="INQUIRY_OTP_TRANSFER_OFF_US">
    <participant class="com.acs.lkpd.participant.process.CreateTransactionParticipant">
        <property name="trx_type" value="inquiry" />
        <property name="trx_description" value="Transfer Off Us" />
    </participant>
    <participant class="com.acs.lkpd.participant.format.FormatInquiryTransferOffUs"/>
    <participant class="com.acs.lkpd.participant.process.SendOTPParticipant">
        <property name="rm" value="RM_TOKEN_TRANSFER_ON_US" />
        <property name="sendSMS" value="true" />
    </participant>
</group>
