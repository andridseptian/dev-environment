package com.acs.ussd.entity.enums;

public enum TransactionStatus {
  SUCCESS("1"),
  FAILED("0"),
  PENDING("2");
  
  private String status;
  
  TransactionStatus(String status) {
    this.status = status;
  }
  
  public String toString() {
    return getStatus();
  }
  
  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
}
