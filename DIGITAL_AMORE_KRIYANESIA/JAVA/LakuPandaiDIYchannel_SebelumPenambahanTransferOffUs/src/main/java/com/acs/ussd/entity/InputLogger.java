package com.acs.ussd.entity;

import com.acs.ussd.spring.SpringImpl;
import org.json.JSONException;
import org.json.JSONObject;

public class InputLogger {
  private JSONObject json = new JSONObject();
  
  public InputLogger(Session session, String input) {
    if (session != null) {
      Menu m = SpringImpl.getMenuDao().findById(session.getLastMenuId());
      if (m != null)
        if (session.getFreeText() == null) {
          this.json.put(String.valueOf(m.getMenuName()), input);
        } else {
          this.json = new JSONObject(session.getFreeText());
          this.json.put(String.valueOf(m.getMenuName()), input);
        }  
      session.setUserInput(input);
      session.setFreeText(this.json.toString());
    } 
  }
  
  public JSONObject getJson() {
    return this.json;
  }
  
  public void setJson(JSONObject json) {
    this.json = json;
  }
  
  public String getValue(String key) {
    return (String)this.json.get(key);
  }
  
  public static void setSelectedMenu(Session session, Menu selectedMenu) {
    JSONObject json = new JSONObject(session.getFreeText());
    json.put("selected_menu", selectedMenu.getMenuName());
    session.setFreeText(json.toString());
  }
  
  public static JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
    JSONObject mergedJSON = new JSONObject();
    try {
      mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
      for (String crunchifyKey : JSONObject.getNames(json2))
        mergedJSON.put(crunchifyKey, json2.get(crunchifyKey)); 
    } catch (JSONException e) {
      throw new JSONException("JSON Exception" + e);
    } 
    return mergedJSON;
  }
}
