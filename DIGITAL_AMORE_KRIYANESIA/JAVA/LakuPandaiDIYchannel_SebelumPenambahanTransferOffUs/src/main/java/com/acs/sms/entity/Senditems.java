package com.acs.sms.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "sentitems")
public class Senditems implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "UpdatedInDB")
  private Date updatedInDB;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "InsertIntoDB")
  private Date insertIntoDB;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "SendingDateTime")
  private Date sendingDateTime;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DeliveryDateTime")
  private Date deliveryDateTime;
  
  @Column(name = "Text")
  @Type(type = "text")
  private String text;
  
  @Column(name = "DestinationNumber", length = 20)
  private String destinationNumber;
  
  @Column(name = "Coding")
  private String coding;
  
  @Column(name = "UDH")
  @Type(type = "text")
  private String udh;
  
  @Column(name = "Class")
  private Integer clazz;
  
  @Column(name = "TextDecoded")
  @Type(type = "text")
  private String textDecoded;
  
  @Column(name = "SenderID", length = 255)
  private String senderId;
  
  @Column(name = "SequencePosition")
  private Integer sequencePosition;
  
  @Column(name = "Status")
  private Status status;
  
  @Column(name = "StatusError")
  private Integer statusError;
  
  @Column(name = "TPMR")
  private Integer TPMR;
  
  @Column(name = "RelativeValidity")
  private Integer relativeValidity;
  
  @Column(name = "CreatorID")
  @Type(type = "text")
  private String creatorId;
  
  public enum Status {
    SendingOK, SendingOKNoReport, SendingError, DeliveryOK, DeliveryFailed, DeliveryPending, DeliveryUnknown, Error;
  }
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Date getUpdatedInDB() {
    return this.updatedInDB;
  }
  
  public void setUpdatedInDB(Date updatedInDB) {
    this.updatedInDB = updatedInDB;
  }
  
  public Date getInsertIntoDB() {
    return this.insertIntoDB;
  }
  
  public void setInsertIntoDB(Date insertIntoDB) {
    this.insertIntoDB = insertIntoDB;
  }
  
  public Date getSendingDateTime() {
    return this.sendingDateTime;
  }
  
  public void setSendingDateTime(Date sendingDateTime) {
    this.sendingDateTime = sendingDateTime;
  }
  
  public Date getDeliveryDateTime() {
    return this.deliveryDateTime;
  }
  
  public void setDeliveryDateTime(Date deliveryDateTime) {
    this.deliveryDateTime = deliveryDateTime;
  }
  
  public String getText() {
    return this.text;
  }
  
  public void setText(String text) {
    this.text = text;
  }
  
  public String getDestinationNumber() {
    return this.destinationNumber;
  }
  
  public void setDestinationNumber(String destinationNumber) {
    this.destinationNumber = destinationNumber;
  }
  
  public String getCoding() {
    return this.coding;
  }
  
  public void setCoding(String coding) {
    this.coding = coding;
  }
  
  public String getUdh() {
    return this.udh;
  }
  
  public void setUdh(String udh) {
    this.udh = udh;
  }
  
  public Integer getClazz() {
    return this.clazz;
  }
  
  public void setClazz(Integer clazz) {
    this.clazz = clazz;
  }
  
  public String getTextDecoded() {
    return this.textDecoded;
  }
  
  public void setTextDecoded(String textDecoded) {
    this.textDecoded = textDecoded;
  }
  
  public String getSenderId() {
    return this.senderId;
  }
  
  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }
  
  public Integer getSequencePosition() {
    return this.sequencePosition;
  }
  
  public void setSequencePosition(Integer sequencePosition) {
    this.sequencePosition = sequencePosition;
  }
  
  public Status getStatus() {
    return this.status;
  }
  
  public void setStatus(Status status) {
    this.status = status;
  }
  
  public Integer getStatusError() {
    return this.statusError;
  }
  
  public void setStatusError(Integer statusError) {
    this.statusError = statusError;
  }
  
  public Integer getTPMR() {
    return this.TPMR;
  }
  
  public void setTPMR(Integer TPMR) {
    this.TPMR = TPMR;
  }
  
  public Integer getRelativeValidity() {
    return this.relativeValidity;
  }
  
  public void setRelativeValidity(Integer relativeValidity) {
    this.relativeValidity = relativeValidity;
  }
  
  public String getCreatorId() {
    return this.creatorId;
  }
  
  public void setCreatorId(String creatorId) {
    this.creatorId = creatorId;
  }
}
