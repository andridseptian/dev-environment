package com.acs.mobile.participant;

import com.acs.ussd.entity.Registration;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.util.EncryptUtility;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class ParseRequest implements TransactionParticipant, Configurable {

    private Configuration cfg;

    Log log = Log.getLog("Q2", getClass().getName());

    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get("TRX");
        JSONObject message = new JSONObject(ctx.get("BODY_JSON").toString());
        if (message != null && !message.equals("")) {
            String action = message.getString("action");
            String msisdn_agen = (String) ctx.get("MSISDN");
            String msg = "";
            String pinAgen = "";
            String token = "";
            String fcm = "";
            String pc = "";
            String rc_sms = "";
            if (message.has("pin")) {
                String pass = message.getString("pin");
                EncryptUtility EU = null;
                try {
                    EU = new EncryptUtility("0404040404040404");
                } catch (Exception ex) {
                    Logger.getLogger(ParseRequest.class.getName()).log(Level.SEVERE, (String) null, ex);
                }
                String padding = "FFFFFFFFFFFFFFFF";
                pinAgen = EU.encrypt(StringUtils.substring(pass + padding, 0, 16)).toLowerCase();
            }

            ///* <editor-fold desc="agen nasabah"> */
            if (action.equals("login")) {
                pc = "900000";
                if (message.has("token")) {
                    fcm = message.getString("token");
                }
                String imei = message.getString("imei");
                msg = imei + "|" + pinAgen;
                rc_sms = "00";
            } else if (action.equals("check_nik")) {
                String nik = message.getString("nik");
                pc = "920000";
                msg = nik;
            } else if (action.equals("reset_pwd")) {
                if ("".equals(pinAgen) || pinAgen == null) {
                    message.getString("no_route");
                } else {
                    String imei = message.getString("imei");
                    pc = "930000";
                    msg = imei + "|" + pinAgen;
                }
            } else if (action.equals("generate_token_activate_account")) {
                Registration AR = new Registration();
                pc = "910000";
                String noKtp = message.getString("nik");
                String nama = message.getString("nama");
                String tgl_lahir = message.getString("tanggal");
                String msisdn_nasabah = message.getString("msisdn_nasabah");
                AR.setMsisdnNasabah(msisdn_nasabah);
                AR.setMsisdnAgen(msisdn_agen);
                AR.setDc("APP-LAKU-PANDAI");
                AR.setBankCode("114");
                AR.setNoIdentitas(noKtp);
                AR.setTanggalLahir(tgl_lahir);
                AR.setProvider("DATA");
                AR.setNama(nama);
                ctx.put("ACC_REGISTER", AR);
                msg = noKtp + "|" + nama + "|" + tgl_lahir + "|" + msisdn_nasabah;
                rc_sms = "01";
            } else if (action.equals("pendaftaran_rekening")) {
                pc = "940000";
                token = message.getString("token");
                msg = token + "|" + pinAgen;
            } else if (action.equals("resend_token")) {
                String msisdn_nasabah = message.getString("msisdn_nasabah");
                pc = "950000";
                msg = msisdn_nasabah;
            }
            /* </editor-fold> */

            ///* <editor-fold desc="terik_tunai"> */
            if (action.equals("request_tarik_tunai")) {
                pc = "210000";
                String nomorHp = message.getString("msisdn_nasabah");
                String amount = message.getString("nominal");
                msg = nomorHp + "|" + amount + "|" + pinAgen;
                rc_sms = "02";
            } else if (action.equals("send_token_tarik_tunai")) {
                pc = "220000";
                token = message.getString("token");
                msg = token;
                rc_sms = "03";
            } else if (action.equals("posting_tarik_tunai")) {
                pc = "230000";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "04";
            }
            /* </editor-fold> */

            ///* <editor-fold desc="setor_tunai"> */
            if (action.equals("request_setor_tunai")) {
                pc = "310000";
                String nomorHp = message.getString("msisdn_nasabah");
                String amount = message.getString("nominal");
                msg = nomorHp + "|" + amount;
                rc_sms = "05";
            } else if (action.equals("posting_setor_tunai")) {
                pc = "320000";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "06";
            } else if (action.equals("request_setor_tunai_non_bsa")) {
                pc = "340000";
                String nomorRek = message.getString("rek_nasabah");
                String amount = message.getString("nominal");
                msg = nomorRek + "|" + amount;
                rc_sms = "05";
            } else if (action.equals("posting_setor_tunai_non_bsa")) {
                pc = "350000";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "06";
            }
            /* </editor-fold> */

            ///* <editor-fold desc="transfer_on_us_lakupandai"> */
            if (action.equals("request_transfer_on_us_lakupandai")) {
                pc = "421100";
                String nomorHp = message.getString("msisdn_nasabah");
                String noRek = message.getString("msisdn_tujuan");
                String amount = message.getString("nominal");
                msg = nomorHp + "|" + noRek + "|" + amount;
            } else if (action.equals("request_transfer_on_us_pin_lakupandai")) {
                pc = "421200";
                token = message.getString("token");
                msg = pinAgen;
            } else if (action.equals("send_token_transfer_on_us_lakupandai")) {
                pc = "421300";
                token = message.getString("token");
                msg = token;
            } else if (action.equals("posting_transfer_on_us_lakupandai")) {
                pc = "421400";
                token = message.getString("token");
                msg = pinAgen;
            }
            /* </editor-fold> */

            ///* <editor-fold desc="tansfer_on_us">  */
            if (action.equals("request_transfer_on_us")) {
                pc = "411100";
                String nomorHp = message.getString("msisdn_nasabah");
                String noRek = message.getString("no_rekening");
                String amount = message.getString("nominal");
                msg = nomorHp + "|" + noRek + "|" + amount;
                rc_sms = "07";
            } else if (action.equals("request_transfer_on_us_pin")) {
                pc = "411200";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "08";
            } else if (action.equals("send_token_transfer_on_us")) {
                pc = "411300";
                token = message.getString("token");
                msg = token;
                rc_sms = "09";
            } else if (action.equals("posting_transfer_on_us")) {
                pc = "411400";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "10";
            }
            /* </editor-fold> */

            ///* <editor-fold desc="tansfer_off_us (antar bank)">  */
            if (action.equals("request_transfer_off_us")) {
                pc = "431100";
                String nomorHp = message.getString("msisdn_nasabah");
                String noRek = message.getString("no_rekening");
                String noBank = message.getString("id_bank");
                String amount = message.getString("nominal");
                msg = nomorHp + "|" + noBank + "|" + noRek + "|" + amount;
                rc_sms = "07";
            } else if (action.equals("request_transfer_off_us_pin")) {
                pc = "431200";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "08";
            } else if (action.equals("send_token_transfer_off_us")) {
                pc = "431300";
                token = message.getString("token");
                msg = token;
                rc_sms = "09";
            } else if (action.equals("posting_transfer_off_us")) {
                pc = "431400";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "10";
            }
            /* </editor-fold> */

            ///* <editor-fold desc="beli_pulsa"> */
            if (action.equals("request_beli_pulsa")) {
                pc = "511100";
                String nomorHpNas = message.getString("msisdn_nasabah");
                String nomorHpTuj = message.getString("msisdn_tujuan");
                String telco = message.getString("provider");
                String denom = message.getString("denom");
                String key = "BELI PULSA|";
                String data = nomorHpTuj + "|" + telco + "|" + denom + "|";
                String keyword = "|BELI " + nomorHpTuj + " " + denom + "|";
                msg = key + data + nomorHpNas + keyword + pinAgen;
                rc_sms = "12";
            } else if (action.equals("send_token_beli_pulsa")) {
                pc = "511200";
                token = message.getString("token");
                msg = token;
                rc_sms = "13";
            } else if (action.equals("posting_beli_pulsa")) {
                pc = "511300";
                token = message.getString("token");
                msg = pinAgen;
                rc_sms = "14";
            }
            /* </editor-fold> */

            ///* <editor-fold desc="menu"> */
            if (action.equals("last_trx")) {
                pc = "610000";
                String nomorHp = message.getString("msisdn_nasabah");
                msg = nomorHp + "|" + pinAgen;
                rc_sms = "15";
            } else if (action.equals("history_trx")) {
                pc = "620000";
                String startDate = message.getString("start_date");
                String endDate = message.getString("end_date");
                String page = message.getString("page");
                msg = startDate + "|" + endDate;
                ctx.put("sDate", startDate);
                ctx.put("eDate", endDate);
                ctx.put("page", page);
                rc_sms = "15";
            } else if (action.equals("notifikasi")) {
                String page = message.getString("page");
                ctx.put("page", page);
                rc_sms = "15";
            } else if (action.equals("notifikasi_mobile")) {
                String page = message.getString("page");
                ctx.put("page", page);
                rc_sms = "15";
            } else if (action.equals("detail_notifikasi_mobile")) {
                pc = "505000";
                String resi = message.getString("resi");
                ctx.put("resi", resi);
                rc_sms = "15";
                msg = resi;
            } else if (action.equals("list_bank")) {
                rc_sms = "15";
            } else if (action.equals("get_menu")) {
                pc = "630000";
                String tipe = message.getString("tipe");
                msg = tipe;
            }
            /* </editor-fold> */

            ///* <editor-fold desc="saldo"> */
            if (action.equals("saldo_agen")) {
                pc = "710000";
                msg = pinAgen;
                rc_sms = "16";
            } else if (action.equals("saldo_nasabah")) {
                pc = "720000";
                String nomorHp = message.getString("msisdn_nasabah");
                msg = nomorHp + "|" + pinAgen;
                rc_sms = "15";
            }
            /* </editor-fold> */

            ///* <editor-fold desc="payment"> */
            if (action.equals("inquiry_payment")) {
                String product_code = message.getString("product_code");
                String nomor_pelanggan = message.getString("id_pelanggan");
                msg = nomor_pelanggan;
                if (!message.has("hp_nasabah")) {
                    product_code = "5" + product_code.substring(1);
                } else {
                    msg = message.getString("hp_nasabah") + "|" + nomor_pelanggan;
                }
                pc = product_code;
            } else if (action.equals("inquiry_prepaid")) {
                String product_code = message.getString("product_code");
                String nomor_pelanggan = message.getString("id_pelanggan");
                String denom = message.getString("denom");
                msg = nomor_pelanggan + "|" + denom;
                if (!message.has("hp_nasabah")) {
                    product_code = "5" + product_code.substring(1);
                } else {
                    msg = message.getString("hp_nasabah") + "|" + nomor_pelanggan + "|" + denom;
                }
                pc = product_code;
            } else if (action.equals("posting_payment") || action.equals("posting_prepaid")) {
                String product_code = message.getString("product_code");
                String tipe = message.getString("tipe");
                token = message.getString("token");
                msg = pinAgen;
                if (tipe.equals("debit")) {
                    product_code = "7" + product_code.substring(1);
                } else {
                    product_code = "8" + product_code.substring(1);
                }
                pc = product_code;
            }
            /* </editor-fold> */

            if (action.equals("initialize")) {
                String info = message.getString("info");
                ctx.put("info", info);
            } else {
                message.getString("no_route");
                rc_sms = "99";
            }
            trx.setReqMessage(msg);
            ctx.put("pc", pc);
            ctx.put("token", token);
            ctx.put("fcm_key", fcm);
            ctx.put("rc_sms", rc_sms);
            return 1;
        }
        return 64;
    }

    public void commit(long l, Serializable srlzbl) {
    }

    public void abort(long l, Serializable srlzbl) {
    }

    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
