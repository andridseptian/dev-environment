package com.acs.ussd.participant;

import com.acs.ussd.entity.enums.Constant;
import java.io.IOException;
import java.io.Serializable;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;

public class SendISOResponse implements AbortParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    ISOSource source = (ISOSource)ctx.get(Constant.SRC);
    if (source == null || !source.isConnected())
      return 0; 
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    ISOSource source = (ISOSource)ctx.get(Constant.SRC);
    ISOMsg out = (ISOMsg)ctx.get(Constant.OUT);
    try {
      source.send(out);
    } catch (IOException ex) {
      this.log.error(ex);
    } catch (org.jpos.iso.ISOFilter.VetoException ex) {
      this.log.error(ex);
    } catch (ISOException isoe) {
      this.log.error(isoe);
    } 
  }
  
  public void abort(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    ISOSource source = (ISOSource)ctx.get(Constant.SRC);
    ISOMsg out = (ISOMsg)ctx.get(Constant.OUT);
    ISOMsg in = (ISOMsg)ctx.get(Constant.IN);
    String rc = ctx.getString(Constant.RC);
    try {
      if (out == null) {
        out = (ISOMsg)in.clone();
        out.setResponseMTI();
        if (rc == null) {
          out.set(Constant.ISO_FIELD_RC, Constant.RC_UNKNOWN_ERROR);
        } else {
          out.set(Constant.ISO_FIELD_RC, rc);
        } 
      } 
      if (!out.hasField(Constant.ISO_FIELD_RC))
        if (rc == null) {
          out.set(Constant.ISO_FIELD_RC, Constant.RC_UNKNOWN_ERROR);
        } else {
          out.set(Constant.ISO_FIELD_RC, rc);
        }  
      source.send(out);
    } catch (IOException ex) {
      this.log.error(ex);
    } catch (org.jpos.iso.ISOFilter.VetoException ex) {
      this.log.error(ex);
    } catch (ISOException isoe) {
      this.log.error(isoe);
    } 
  }
  
  public int prepareForAbort(long l, Serializable srlzbl) {
    return 1;
  }
}
