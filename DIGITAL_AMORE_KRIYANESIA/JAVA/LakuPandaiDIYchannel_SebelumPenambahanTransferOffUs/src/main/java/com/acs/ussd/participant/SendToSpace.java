package com.acs.ussd.participant;

import com.acs.ussd.entity.enums.Constant;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

public class SendToSpace implements TransactionParticipant, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    ISOMsg in = (ISOMsg)ctx.get(Constant.IN);
    Long timeout = Long.valueOf(this.cfg.getLong("timeout", 30000L));
    String spaceName = this.cfg.get("space_name", "jatim-space");
    try {
      String[] bitkey = ISOUtil.toStringArray(this.cfg.get("key_space"));
      String key = "";
      for (String bit : bitkey) {
        if (in.hasField(bit))
          key = key + in.getString(bit); 
      } 
      this.log.info("KEY SEND TO SPACE : " + key);
      Space sp = SpaceFactory.getSpace(spaceName);
      sp.out(key, ctx, timeout.longValue());
    } catch (ISOException isos) {
      this.log.error(isos);
    } 
  }
  
  public void abort(long l, Serializable srlzbl) {}
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
