package com.acs.ussd.web;

import com.acs.ussd.entity.Client;
import com.acs.ussd.entity.InputLogger;
import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.ParseXML;
import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.Session;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.MenuType;
import com.acs.ussd.parser.XMLParser;
import com.acs.ussd.spring.SpringImpl;
import com.dcms.iso.filter.XmlFormatter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jpos.q2.QBeanSupport;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONException;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class USSDController extends QBeanSupport {
  private Route ussdGetRoute;
  
  protected void initService() throws Exception {
    final int port = this.cfg.getInt("port", 8889);
    Spark.setPort(port);
    this.ussdGetRoute = new Route("/*") {
        public Object handle(Request request, Response response) {
          String path = request.pathInfo();
          RequestMapping req = SpringImpl.getRequestMappingDao().find(path);
          if (req != null) {
            switch (req.getProvider()) {
              case "TSEL":
                return USSDController.this.telkomselRequest(req, port, request, response);
              case "SAT":
                return USSDController.this.indosatRequest(req, request, response);
            } 
            return "provider not supported";
          } 
          USSDController.this.log.info("Get Request on : " + request.url() + "?" + request.queryString() + "\n Response : Request not found");
          return "request not found";
        }
      };
  }
  
  public String indosatRequest(RequestMapping req, Request request, Response response) {
    if (req != null) {
      String rsp = "";
      XMLParser xml = new SATParser(req);
      LogEvent le = new LogEvent();
      Session session = new Session();
      Client client = req.getClient();
      String msisdn = request.queryParams(req.getParameterMSISDN());
      String input = request.queryParams(req.getParameterInput());
      String messageid = request.queryParams(req.getParameterSession());
      if (msisdn != null && input != null && messageid != null) {
        if (msisdn.startsWith("0"))
          msisdn = "62" + msisdn.substring(1); 
        try {
          le.addMessage("Get Request on : " + request.url() + "?" + request.queryString());
          session = SpringImpl.getSessionDao().findSession(msisdn, client.getClientCode(), messageid, req.getProvider());
          InputLogger inputLogger = new InputLogger(session, input);
          Calendar cal = Calendar.getInstance();
          cal.setTime(new Date());
          if (session != null && session.isActive().booleanValue()) {
            session.setActive(Boolean.valueOf(session.getExpDate().after(new Date())));
            session.setClientReff(messageid);
            SpringImpl.getSessionDao().save(session, req);
          } 
          if (session == null || !session.isActive().booleanValue()) {
            session = new Session(msisdn, client.getClientCode(), req.getDefaultGroup(), req.getProvider(), messageid);
            List<Menu> menuUtama = SpringImpl.getMenuDao().findByClientAndGroup(client.getClientCode(), req.getDefaultGroup());
            session.setLastMenuId(Menu.getHeader(menuUtama).getId());
            rsp = xml.generateMenu(menuUtama);
            le.addMessage(("Send Response : " + rsp != null) ? (new XmlFormatter())
                .format(rsp) : rsp);
            return rsp;
          } 
          if (session.isActive().booleanValue()) {
            this.log.info("session active");
            Menu lastMenu = SpringImpl.getMenuDao().findById((session.getLastMenuId() == null) ? Long.valueOf(1L) : session.getLastMenuId());
            le.addMessage("LAST MENU : " + lastMenu.getId() + " | " + lastMenu.getDisplayName());
            rsp = generateResponse(req, inputLogger, lastMenu, session, xml);
            le.addMessage(("Send Response : " + rsp != null) ? (new XmlFormatter())
                .format(rsp) : rsp);
            return rsp;
          } 
          return xml.generateDefaultResponse();
        } catch (Exception ex) {
          this.log.error(ex);
          SpringImpl.getSessionDao().setInactive(session);
          return xml.generateDefaultResponse();
        } finally {
          this.log.info(le);
          SpringImpl.getSessionDao().save(session, req);
        } 
      } 
      return "parameter not supported";
    } 
    return request.pathInfo() + " not supported";
  }
  
  public String telkomselRequest(RequestMapping req, int port, Request request, Response response) {
    if (req != null) {
      String rsp = "";
      XMLParser xml = new TSELParser(req);
      Session session = new Session();
      Client client = req.getClient();
      String msisdn = request.queryParams(req.getParameterMSISDN());
      String input = request.queryParams(req.getParameterInput());
      String messageid = request.queryParams(req.getParameterSession());
      LogEvent le = new LogEvent();
      try {
        String[] array = input.split("\\*");
        int count = array.length;
        if (count > 2 && !"*141*500#".equals(input) && !"*806*5#".equals(input)) {
          this.log.info("Get longdial-request on : " + request.url() + "?" + request.queryString());
          String output = "";
          String hasil = "";
          String data = "";
          String host = "";
          host = "http://localhost:" + port + req.getPathInfo();
          for (int i = 2; i < count; i++) {
            if (i == count - 1)
              array[i] = array[i].substring(0, array[i].length() - 1); 
            data = "";
            data = data + "&msisdn=" + msisdn;
            data = data + "&command=" + array[i];
            data = data + "&umb_session_id=" + messageid;
            URL url = new URL(host + "?" + data);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            hasil = "";
            output = "";
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null)
              hasil = hasil + output; 
            conn.disconnect();
          } 
          return hasil;
        } 
      } catch (IOException ioe) {
        this.log.error("ERROR LONG DIAL " + req.getProvider() + " : " + ioe.getMessage());
      } 
      if (messageid == null)
        messageid = msisdn; 
      if (msisdn != null && input != null && messageid != null) {
        if (msisdn.startsWith("0"))
          msisdn = "62" + msisdn.substring(1); 
        try {
          le.addMessage("Get Request on : " + request.url() + "?" + request.queryString());
          session = SpringImpl.getSessionDao().findSession(msisdn, client.getClientCode(), messageid, req.getProvider());
          if (session != null && input.equals("0")) {
            session.setActive(Boolean.FALSE);
            SpringImpl.getSessionDao().save(session, req);
            return "reset accepted";
          } 
          InputLogger inputLogger = new InputLogger(session, input);
          if (session != null && session.isActive().booleanValue()) {
            session.setActive(Boolean.valueOf(session.getExpDate().after(new Date())));
            session.setClientReff(messageid);
            SpringImpl.getSessionDao().save(session, req);
          } 
          if (session == null || !session.isActive().booleanValue()) {
            session = new Session(msisdn, client.getClientCode(), req.getDefaultGroup(), req.getProvider(), messageid);
            List<Menu> menuUtama = SpringImpl.getMenuDao().findByClientAndGroup(client.getClientCode(), req.getDefaultGroup());
            session.setLastMenuId(Menu.getHeader(menuUtama).getId());
            rsp = xml.generateMenu(menuUtama);
            le.addMessage(("Send Response : " + rsp != null) ? (new XmlFormatter())
                .format(rsp) : rsp);
            return rsp;
          } 
          if (session.isActive().booleanValue()) {
            Menu lastMenu = SpringImpl.getMenuDao().findById((session.getLastMenuId() == null) ? 
                Menu.getHeader(SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), req.getDefaultGroup())).getId() : session
                .getLastMenuId());
            le.addMessage("LAST MENU : " + lastMenu.getId() + " | " + lastMenu.getDisplayName() + " | Group : " + lastMenu.getGroupMenu());
            rsp = generateResponse(req, inputLogger, lastMenu, session, xml);
            le.addMessage(("Send Response : " + rsp != null) ? (new XmlFormatter())
                .format(rsp) : rsp);
            return rsp;
          } 
          return xml.generateDefaultResponse();
        } catch (Exception ex) {
          this.log.error(ex);
          SpringImpl.getSessionDao().setInactive(session);
          return xml.generateDefaultResponse();
        } finally {
          this.log.info(le);
          SpringImpl.getSessionDao().save(session, req);
        } 
      } 
      return "Parameter salah / tidak lengkap";
    } 
    return request.pathInfo() + " tidak tersedia";
  }
  
  protected void startService() throws Exception {
    Spark.get(this.ussdGetRoute);
  }
  
  protected void stopService() throws Exception {}
  
  public String generateResponse(RequestMapping req, InputLogger inputLogger, Menu lastMenu, Session session, XMLParser xml) {
    Menu selectedMenu, childMenu, menuProvider, parentMenu;
    switch (lastMenu.getType()) {
      case MENU_HEADER:
      case MENU:
        selectedMenu = SpringImpl.getMenuDao().findByGroupAndIndex(req.getClient().getClientCode(), lastMenu.getGroupMenu(), session.getUserInput());
        if (selectedMenu != null) {
          Menu menu1, menu2;
          if (selectedMenu.getAddToSession() != null && selectedMenu.getAddToSession().contains("{")) {
            try {
              inputLogger.setJson(InputLogger.mergeJSONObjects(inputLogger.getJson(), new JSONObject(selectedMenu.getAddToSession())));
              session.setFreeText(inputLogger.getJson().toString());
              JSONObject jSONObject = inputLogger.getJson();
            } catch (JSONException json) {
              this.log.error("Can't add [" + selectedMenu.getAddToSession() + "] to session");
            } 
            try {
              if (inputLogger.getJson().has("split_value")) {
                inputLogger.getJson().put(inputLogger.getJson().getString("split_value"), inputLogger.getJson().getString(selectedMenu.getId().toString()));
                session.setFreeText(inputLogger.getJson().toString());
              } 
            } catch (JSONException json) {
              this.log.error("Can't add split value[" + lastMenu.getId() + "] to session");
            } 
            if (inputLogger.getJson().has("next")) {
              Menu select = SpringImpl.getMenuDao().findByKey(session.getClientCode(), inputLogger.getJson().getString("next"));
              if (inputLogger.getJson().has("display")) {
                select.setDisplayName(inputLogger.getJson().getString("display"));
                inputLogger.getJson().remove("display");
              } 
              inputLogger.getJson().remove("next");
              session.setFreeText(inputLogger.getJson().toString());
              return generateXML(req, inputLogger, select, session, xml);
            } 
          } 
          switch (selectedMenu.getType()) {
            case MENU_HEADER:
            case MENU:
              menu1 = SpringImpl.getMenuDao().findChild(req.getClient().getClientCode(), selectedMenu);
              return generateXML(req, inputLogger, menu1, session, xml);
            case SEND_VIEW_INFO_FORM:
            case SEND_VIEW_INPUT_FORM:
            case SEND_VIEW_INPUT_OR_INFO_BY_RC:
            case SEND_VIEW_INPUT_MULTIPLE_PAGE:
            case SEND_VIEW_INFO_MULTIPLE_PAGE:
            case CHECK_PREFIX:
              return generateXML(req, inputLogger, selectedMenu, session, xml);
            case BACK:
              menu2 = SpringImpl.getMenuDao().findParent(req.getClient().getClientCode(), selectedMenu);
              return generateXML(req, inputLogger, menu2, session, xml);
          } 
          SpringImpl.getSessionDao().setInactive(session);
          return xml.generateDefaultResponse();
        } 
        SpringImpl.getSessionDao().setInactive(session);
        return xml.generateDefaultResponse();
      case SEND_VIEW_INFO_FORM:
      case SEND_VIEW_INPUT_FORM:
      case SEND_VIEW_INPUT_OR_INFO_BY_RC:
      case INPUT:
        if (lastMenu.getAddToSession() != null && lastMenu.getAddToSession().contains("{")) {
          JSONObject jsonData = new JSONObject(lastMenu.getAddToSession());
          try {
            if (jsonData.has("min") && (jsonData.has("if_min") || jsonData.has("if_min_text"))) {
              int minLength = jsonData.getInt("min");
              jsonData.remove("min");
              if (session.getUserInput().length() < minLength) {
                Menu next = new Menu();
                if (jsonData.has("if_min")) {
                  next = SpringImpl.getMenuDao().findByKey(session.getClientCode(), jsonData.getString("if_min"));
                  jsonData.remove("if_min");
                } else {
                  next.setType(MenuType.INFO);
                  next.setDisplayName(jsonData.getString("if_min_text"));
                  jsonData.remove("if_min_text");
                } 
                if (next != null) {
                  session.setFreeText(jsonData.toString());
                  return generateXML(req, inputLogger, next, session, xml);
                } 
              } 
            } 
            if (jsonData.has("max") && (jsonData.has("if_max") || jsonData.has("if_max_text"))) {
              int maxLength = jsonData.getInt("max");
              jsonData.remove("max");
              if (session.getUserInput().length() > maxLength) {
                Menu next = new Menu();
                if (jsonData.has("if_max")) {
                  next = SpringImpl.getMenuDao().findByKey(session.getClientCode(), jsonData.getString("if_max"));
                  jsonData.remove("if_max");
                } else {
                  next.setType(MenuType.INFO);
                  next.setDisplayName(jsonData.getString("if_max_text"));
                  jsonData.remove("if_max_text");
                } 
                if (next != null) {
                  session.setFreeText(jsonData.toString());
                  return generateXML(req, inputLogger, next, session, xml);
                } 
              } 
            } 
          } catch (JSONException jse) {
            this.log.error("Can't load [" + lastMenu.getAddToSession() + "] to session, caused by " + jse.getMessage());
          } catch (Exception ex) {
            this.log.error("Can't load [" + lastMenu.getAddToSession() + "] to session, caused by " + ex.getMessage());
          } 
        } 
        childMenu = SpringImpl.getMenuDao().findChild(req.getClient().getClientCode(), lastMenu);
        return generateXML(req, inputLogger, childMenu, session, xml);
      case CHECK_PREFIX:
        menuProvider = checkPrefix(inputLogger, lastMenu, session);
        return generateXML(req, inputLogger, menuProvider, session, xml);
      case BACK:
        parentMenu = SpringImpl.getMenuDao().findParent(req.getClient().getClientCode(), lastMenu);
        return generateXML(req, inputLogger, parentMenu, session, xml);
    } 
    session.setLastMenuId(lastMenu.getId());
    SpringImpl.getSessionDao().setInactive(session);
    return xml.generateDefaultResponse();
  }
  
  private String generateXML(RequestMapping req, InputLogger inputLogger, Menu selectedMenu, Session session, XMLParser xml) {
    if (selectedMenu.getAddToSession() != null && selectedMenu.getAddToSession().startsWith("{"))
      try {
        JSONObject jsonData = new JSONObject(selectedMenu.getAddToSession());
        inputLogger.setJson(InputLogger.mergeJSONObjects(inputLogger.getJson(), new JSONObject(selectedMenu.getAddToSession())));
        session.setFreeText(inputLogger.getJson().toString());
        if (jsonData.has("next")) {
          Menu next = SpringImpl.getMenuDao().findByKey(session.getClientCode(), jsonData.getString("next"));
          if (next != null)
            selectedMenu = next; 
        } 
      } catch (JSONException json) {
        this.log.error("Can't load [" + selectedMenu.getAddToSession() + "] to session, check parameter (min/max must be integer) caused by " + json.getMessage());
      } catch (Exception ex) {
        this.log.error("Can't load [" + selectedMenu.getAddToSession() + "] to session, caused by " + ex.getMessage());
      }  
    try {
      String[] hasil;
      List<Menu> listMenu;
      Space sp;
      Context ctx;
      Trx trx;
      String processor, reffNumber, queueKey;
      Context rspCtx;
      Trx rspTrx;
      Menu parentMenu;
      switch (selectedMenu.getType()) {
        case MENU_HEADER:
        case MENU:
          hasil = selectedMenu.getDisplayName().split("[{}]");
          if (hasil.length > 1) {
            List<Menu> list = SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), selectedMenu.getGroupMenu());
            JSONObject jsonStep = new JSONObject(session.getFreeText());
            for (int i = 0; i < list.size(); i++) {
              this.log.info(((Menu)list.get(i)).getType());
              if (MenuType.MENU_HEADER.equals(((Menu)list.get(i)).getType())) {
                String[] hasil2 = ((Menu)list.get(i)).getDisplayName().split("[{}]");
                String message = "";
                for (String x : hasil2) {
                  if (jsonStep.has(x))
                    x = jsonStep.getString(x); 
                  message = message + x;
                } 
                ((Menu)list.get(i)).setDisplayName(message);
              } 
              this.log.info(((Menu)list.get(i)).getDisplayName());
            } 
            session.setLastMenuId(Menu.getHeader(list).getId());
            return xml.generateMenu(list);
          } 
          listMenu = SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), selectedMenu.getGroupMenu());
          session.setLastMenuId(Menu.getHeader(listMenu).getId());
          return xml.generateMenu(listMenu);
        case CHECK_PREFIX:
        case INPUT:
          session.setLastMenuId(selectedMenu.getId());
          return xml.generateInput(selectedMenu);
        case INFO:
          session.setLastMenuId(selectedMenu.getId());
          return xml.generateInfo(selectedMenu);
        case SEND_VIEW_INFO_FORM:
        case SEND_VIEW_INPUT_FORM:
        case SEND_VIEW_INPUT_OR_INFO_BY_RC:
        case SEND_VIEW_INPUT_MULTIPLE_PAGE:
        case SEND_VIEW_INFO_MULTIPLE_PAGE:
        case SEND_VIEW_INFO_MENU_FORM:
          session.setLastMenuId(selectedMenu.getId());
          InputLogger.setSelectedMenu(session, selectedMenu);
          sp = SpaceFactory.getSpace();
          ctx = new Context();
          trx = new Trx();
          trx.setMsisdn(session.getMsisdn());
          trx.setClient(req.getClient());
          trx.setFreeText(session.getFreeText());
          trx.setProvider(req.getProvider());
          trx.setReqMessage(selectedMenu.getTrxMessage());
          ctx.put("TRX", trx);
          ctx.put("SESSION", session);
          ctx.put(Constant.ORIGIN, req.getClient().getDisplayName());
          processor = trx.getClient().getName();
          trx.setRequestTime(new Date());
          reffNumber = trx.getStan();
          queueKey = trx.getMsisdn() + reffNumber;
          trx.setQueueKey(queueKey);
          this.log.info("TRX " + trx.getMsisdn() + "[" + trx.getStan() + "] process using : " + processor);
          sp.out(processor, ctx, this.cfg.getInt("timeout", 60000));
          this.log.info("Trying fetch from : " + queueKey);
          rspCtx = (Context)sp.in(Constant.RSP + queueKey, this.cfg.getInt("timeout", 60000));
          rspTrx = (Trx)rspCtx.get("TRX");
          selectedMenu.setDisplayName(parseToXML(rspTrx.getMsgContent(), req.getClient().getClientCode(), req.getProvider()));
          session.setLastMenuId(selectedMenu.getId());
          switch (selectedMenu.getType()) {
            case SEND_VIEW_INPUT_FORM:
              return xml.generateInput(selectedMenu);
            case SEND_VIEW_INFO_FORM:
              SpringImpl.getSessionDao().setInactive(session);
              return xml.generateInfo(selectedMenu);
            case SEND_VIEW_INPUT_OR_INFO_BY_RC:
              if (rspTrx.getClient().getClientCode().equals("114")) {
                if (rspTrx.getMsgContent().toLowerCase().contains("ditolak")) {
                  SpringImpl.getSessionDao().setInactive(session);
                  return xml.generateInfo(selectedMenu);
                } 
                return xml.generateInput(selectedMenu);
              } 
              if (rspTrx.getClientRc().equals(Constant.RC_APPROVED))
                return xml.generateInput(selectedMenu); 
              SpringImpl.getSessionDao().setInactive(session);
              return xml.generateInfo(selectedMenu);
            case SEND_VIEW_INFO_MENU_FORM:
              if (rspTrx.getClientRc().equals(Constant.RC_APPROVED)) {
                String[] splitDisplayRM = rspTrx.getMsgContent().split("#");
                List<Menu> listMenu1 = SpringImpl.getMenuDao().findByParentIdByLength(selectedMenu.getId(), req.getClient().getClientCode(), splitDisplayRM.length);
                for (int i = 0; i < splitDisplayRM.length; i++) {
                  ((Menu)listMenu1.get(i)).setDisplayName(((Menu)listMenu1.get(i)).getDisplayName().replace("$split" + i, parseToXML(splitDisplayRM[i], req.getClient().getClientCode(), req.getProvider())));
                  if (i > 1) {
                    inputLogger.setJson(InputLogger.mergeJSONObjects(inputLogger.getJson(), (new JSONObject()).put(((Menu)listMenu1.get(i)).getId().toString(), splitDisplayRM[i])));
                    session.setFreeText(inputLogger.getJson().toString());
                  } 
                } 
                session.setLastMenuId(Menu.getHeader(listMenu1).getId());
                SpringImpl.getSessionDao().save(session, req);
                return xml.generateMenu(listMenu1);
              } 
              SpringImpl.getSessionDao().setInactive(session);
              return xml.generateInfo(selectedMenu);
          } 
          session.setLastMenuId(selectedMenu.getId());
          SpringImpl.getSessionDao().setInactive(session);
          return xml.generateDefaultResponse();
        case BACK:
          parentMenu = SpringImpl.getMenuDao().findParent(req.getClient().getClientCode(), selectedMenu);
          session.setLastMenuId(parentMenu.getId());
          return xml.generateMenu(SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), parentMenu.getGroupMenu()));
      } 
      session.setLastMenuId(selectedMenu.getId());
      SpringImpl.getSessionDao().setInactive(session);
      return xml.generateDefaultResponse();
    } catch (Exception ex) {
      this.log.error(ex);
      SpringImpl.getSessionDao().setInactive(session);
      return xml.generateDefaultResponse();
    } 
  }
  
  private Menu checkPrefix(InputLogger inputLogger, Menu menu, Session session) {
    try {
      String nomorHandphone = inputLogger.getValue(menu.getMenuName());
      URI uri = (new URIBuilder(Constant.CHECK_PREFIX_IP)).setPort(Constant.CHECK_PREFIX_PORT).setPath("/prefix").setParameter("no", nomorHandphone).build();
      CloseableHttpClient httpClient = HttpClientBuilder.create().build();
      HttpGet httpGet = new HttpGet(uri);
      CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest)httpGet);
      StringWriter writer = new StringWriter();
      IOUtils.copy(closeableHttpResponse.getEntity().getContent(), writer);
      JSONObject rsp = new JSONObject(writer.toString());
      JSONObject parameter = new JSONObject(menu.getTrxMessage());
      if (nomorHandphone.length() < 8) {
        Menu menu1 = new Menu();
        menu1.setType(MenuType.INFO);
        menu1.setDisplayName("Panjang No HP tidak boleh kurang dari 8 Digit");
        return menu1;
      } 
      if (nomorHandphone.length() > 14) {
        Menu menu1 = new Menu();
        menu1.setType(MenuType.INFO);
        menu1.setDisplayName("Panjang No HP tidak boleh lebih dari 14 Digit");
        return menu1;
      } 
      if (rsp.has("product") && rsp.has("biller") && parameter.has("check")) {
        String check = parameter.getString("check");
        if (check != null && rsp.has(check) && parameter.has(rsp.getString(check))) {
          Menu targetMenu = SpringImpl.getMenuDao().findByKey(menu.getClientCode(), parameter.getString(rsp.getString(check)));
          JSONObject ft = new JSONObject(session.getFreeText());
          ft.put("product", rsp.getString("product"));
          session.setFreeText(ft.toString());
          return targetMenu;
        } 
        if (parameter.has("else"))
          return SpringImpl.getMenuDao().findByKey("0000", parameter.getString("else")); 
        return SpringImpl.getMenuDao().findById(Long.valueOf("9990"));
      } 
      Menu m = new Menu();
      m.setType(MenuType.INFO);
      m.setDisplayName("Product tidak tersedia");
      return m;
    } catch (IOException ex) {
      this.log.error(ex);
      return null;
    } catch (URISyntaxException ex) {
      this.log.error(ex);
      return null;
    } 
  }
  
  public static Map<String, String> splitQuery(String parameter) throws UnsupportedEncodingException {
    Map<String, String> query_pairs = new LinkedHashMap<>();
    String[] pairs = parameter.split("&");
    for (String pair : pairs) {
      int idx = pair.indexOf("=");
      query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
    } 
    return query_pairs;
  }
  
  public String parseToXML(String text, String bankCode, String provider) {
    String request = text;
    if (!request.isEmpty()) {
      List<ParseXML> listData = SpringImpl.getParseXMLDao().find(bankCode, provider);
      if (listData != null)
        for (ParseXML parseXML : listData)
          request = request.replace(StringEscapeUtils.unescapeJava(parseXML.getTargetContent()), StringEscapeUtils.unescapeJava(parseXML.getReplaceContent()));  
    } 
    return request;
  }
}
