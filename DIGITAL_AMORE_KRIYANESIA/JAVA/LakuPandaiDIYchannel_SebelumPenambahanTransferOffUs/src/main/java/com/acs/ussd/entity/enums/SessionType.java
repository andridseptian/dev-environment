package com.acs.ussd.entity.enums;

public enum SessionType {
  TIME_LIMIT, REFRESH_TIME_LIMIT, NO_LIMIT;
}
