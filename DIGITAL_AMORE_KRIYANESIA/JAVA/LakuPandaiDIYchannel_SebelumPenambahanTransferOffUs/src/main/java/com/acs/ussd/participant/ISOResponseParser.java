package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.TransactionStatus;
import java.io.Serializable;
import org.jpos.core.Configuration;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

public class ISOResponseParser implements TransactionParticipant {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    ISOMsg hasil = (ISOMsg)ctx.get(Constant.IN);
    try {
      String[] arrayCheck = ISOUtil.toStringArray(this.cfg.get("check"));
      for (String check : arrayCheck) {
        if (check.contains(":")) {
          String[] param = check.split(":");
          if (hasil != null && hasil.hasField(param[0])) {
            System.out.println("PARAM 0: " + param[0] + " ,1:" + param[1]);
            if (param[1].equals(hasil.getValue(param[0]))) {
              trx.setStatus(TransactionStatus.SUCCESS);
              trx.setClientRc(Constant.RC_APPROVED);
            } else {
              ctx.put("INFOTRX", param[0] + " not found in response");
              trx.setStatus(TransactionStatus.FAILED);
              trx.setClientRc(Constant.RC_MISSING_MANDATORY_PARAMETER);
              ctx.put("TRX", trx);
              return;
            } 
          } else {
            ctx.put("INFOTRX", arrayCheck.toString() + " not found in response");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_MISSING_MANDATORY_PARAMETER);
            ctx.put("TRX", trx);
            return;
          } 
        } 
      } 
    } catch (Exception ex) {
      this.log.info(ex);
      ctx.put("INFOTRX", "IN null");
      trx.setStatus(TransactionStatus.FAILED);
      trx.setClientRc(Constant.RC_INTERNAL_ERROR);
    } finally {
      ctx.put("TRX", trx);
    } 
  }
  
  public void abort(long l, Serializable srlzbl) {}
}
