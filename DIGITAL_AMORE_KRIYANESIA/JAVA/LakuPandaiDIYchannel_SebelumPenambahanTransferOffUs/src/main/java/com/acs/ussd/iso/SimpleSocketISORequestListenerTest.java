package com.acs.ussd.iso;

import com.acs.ussd.entity.enums.Constant;
import java.io.IOException;
import java.util.Arrays;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;

public class SimpleSocketISORequestListenerTest implements ISORequestListener, LogSource, Configurable {
  Configuration cfg;
  
  Log log;
  
  Logger logger;
  
  String realm;
  
  public boolean process(ISOSource isos, ISOMsg isomsg) {
    try {
      BaseChannel bc = (BaseChannel)isos;
      String originIp = bc.getSocket().getInetAddress().getHostAddress();
      if ("0800".equals(isomsg.getMTI())) {
        isomsg.setResponseMTI();
        isomsg.set(Constant.ISO_FIELD_RC, Constant.RC_APPROVED);
        isos.send(isomsg);
      } else if ("0810".equals(isomsg.getMTI())) {
        this.log.info("DROP 0810 REQUEST");
      } else if (isomsg.isResponse()) {
        this.log.info("DROP Late Response");
      } else {
        String txn = this.cfg.get("txnmgr");
        int[] mandatoryField = { 7, 11, 37 };
        if (isomsg.hasFields(mandatoryField)) {
          String mdn = isomsg.getString(61);
          String rrn = isomsg.getString(37);
          String queueKey = mdn + rrn;
          Context ctx = new Context();
          ctx.put(Constant.IN, isomsg);
          ctx.put(Constant.ORIGIN, this.cfg.get("origin"));
          ctx.put(Constant.SRC, isos);
          ctx.put(Constant.RSP, isomsg);
          Space sp = SpaceFactory.getSpace();
          sp.out("jatim-txnmgr", ctx, 30000L);
          if (isomsg.isRequest())
            isomsg.setResponseMTI(); 
          isomsg.set(Constant.ISO_FIELD_RC, Constant.RC_APPROVED);
          isos.send(isomsg);
          this.log.info("Send response to JATIM");
        } else {
          this.log.error("Missing Mandatory Parameter, must contain all these field : " + Arrays.toString(mandatoryField));
          if (isomsg.isRequest())
            isomsg.setResponseMTI(); 
          isomsg.set(Constant.ISO_FIELD_RC, Constant.RC_MISSING_MANDATORY_PARAMETER);
          isos.send(isomsg);
        } 
      } 
    } catch (ISOException isoe) {
      this.log.error(isoe);
    } catch (IOException ex) {
      this.log.error(ex);
    } 
    return true;
  }
  
  public void setLogger(Logger logger, String realm) {
    this.logger = logger;
    this.realm = realm;
  }
  
  public String getRealm() {
    return this.realm;
  }
  
  public Logger getLogger() {
    return this.logger;
  }
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
    this.log = Log.getLog("Q2", getClass().getName());
  }
}
