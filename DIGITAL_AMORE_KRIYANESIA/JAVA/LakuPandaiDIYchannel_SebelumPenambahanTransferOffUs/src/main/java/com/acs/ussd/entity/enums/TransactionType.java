package com.acs.ussd.entity.enums;

public enum TransactionType {
  ONE_STEP, TWO_STEP;
}
