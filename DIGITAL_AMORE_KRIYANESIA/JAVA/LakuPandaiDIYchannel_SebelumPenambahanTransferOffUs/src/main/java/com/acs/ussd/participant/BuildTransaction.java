package com.acs.ussd.participant;

import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.spring.SpringImpl;
import java.io.Serializable;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

public class BuildTransaction implements TransactionParticipant, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    if (trx == null && ctx
      .get("PROVIDER") != null && ctx
      .get("MSISDN") != null && ctx
      .get("SMS") != null && ctx
      .get("SESSION") != null) {
      String provider = ctx.getString("PROVIDER");
      String msisdn = ctx.getString("MSISDN");
      String sms = ctx.getString("SMS");
      String session = ctx.getString("SESSION");
      String clientCode = this.cfg.get("clientCode", "not-supported");
      trx = new Trx();
      RequestMapping req = SpringImpl.getRequestMappingDao().findByClientCode(clientCode);
      if (req != null) {
        trx.setRequestTime(new Date());
        trx.setQueueKey(msisdn + session);
        trx.setClient(req.getClient());
        trx.setMsisdn(msisdn);
        trx.setProvider(provider);
        trx.setProviderReff(ctx.getString("CHNNL"));
        trx.setReqMessage(sms);
        ctx.put("TRX", trx);
        ctx.put(Constant.ORIGIN, req.getClient().getDisplayName());
        return 1;
      } 
      trx.setMsisdn(msisdn);
      trx.setProvider(provider);
      trx.setReqMessage(sms);
      ctx.put("TRX", trx);
      ctx.put(Constant.ORIGIN, "not-supported");
      return 64;
    } 
    ctx.put("TRX", trx);
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {}
  
  public void abort(long l, Serializable srlzbl) {}
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
