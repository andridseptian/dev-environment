package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.util.Utility;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONException;
import org.json.JSONObject;

public class SendSMS implements TransactionParticipant, Configurable {
  Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    switch (trx.getProvider()) {
      case "SAT":
      case "TSEL":
      case "XL":
        return 1;
    } 
    return 0;
  }
  
  public void commit(long l, Serializable srlzbl) {
    String output = "";
    Space sp = SpaceFactory.getSpace();
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    LogEvent le = new LogEvent("");
    String telco = trx.getProvider();
    String msisdn = trx.getMsisdn();
    String sms = trx.getMsgContent();
    String transId = "UMB" + Utility.generateStan();
    String transDate = "0";
    String host = this.cfg.get("host");
    String bankCode = this.cfg.get("bankcode");
    String NoSID = this.cfg.get("noSID");
    Boolean sendSms = Boolean.TRUE;
    Boolean multipleSms = Boolean.FALSE;
    try {
      if (trx.getFreeText() != null) {
        JSONObject dataCheck = new JSONObject(trx.getFreeText());
        if (dataCheck.has("telco"))
          telco = dataCheck.getString("telco"); 
        if (dataCheck.has("host"))
          host = dataCheck.getString("host"); 
        if (dataCheck.has("bankcode"))
          bankCode = dataCheck.getString("bankcode"); 
        if (dataCheck.has("sid"))
          NoSID = dataCheck.getString("sid"); 
        if (dataCheck.has(telco.toLowerCase() + "_" + "sid"))
          NoSID = dataCheck.getString(telco.toLowerCase() + "_" + "sid"); 
        if (dataCheck.has("sms"))
          sendSms = Boolean.valueOf(dataCheck.getBoolean("sms")); 
        if (dataCheck.has(telco.toLowerCase() + "_" + "sms"))
          sendSms = Boolean.valueOf(dataCheck.getBoolean(telco.toLowerCase() + "_" + "sms")); 
        if (dataCheck.has("multiple_sms"))
          multipleSms = Boolean.valueOf(dataCheck.getBoolean("multiple_sms")); 
        if (dataCheck.has(telco.toLowerCase() + "_" + "multiple_sms"))
          multipleSms = Boolean.valueOf(dataCheck.getBoolean(telco.toLowerCase() + "_" + "multiple_sms")); 
      } 
    } catch (JSONException je) {
      this.log.error("JSONException :" + trx.getFreeText());
    } 
    if (sendSms.booleanValue() && !trx.getClientRc().equals(Constant.RC_MISSING_MANDATORY_PARAMETER) && !trx.getClientRc().equals(Constant.RC_TIME_OUT)) {
      this.log.info("Send SMS by request");
      try {
        this.log.info("Multiple SMS : " + multipleSms);
        if (multipleSms.booleanValue() && sms.length() > 159) {
          int totalSms = (int)Math.ceil(sms.length() / 160.0D);
          this.log.info("Send Total SMS :" + totalSms);
          this.log.info("Text : " + sms);
          String[] temp = sms.split("\n\n");
          this.log.info("Temp Length: " + temp.length);
          for (int loop = 0; loop < temp.length; loop++) {
            this.log.info("Loop : " + loop);
            String text = temp[loop];
            String req = "";
            try {
              req = "telco=" + URLEncoder.encode(telco, "UTF-8") + "&msisdn=" + URLEncoder.encode(msisdn, "UTF-8") + "&sms=" + URLEncoder.encode(text, "UTF-8") + "&bank_code=" + URLEncoder.encode(bankCode, "UTF-8") + "&trans_id=" + URLEncoder.encode(transId, "UTF-8") + "&trans_date=" + URLEncoder.encode(transDate, "UTF-8") + "&no_sid=" + URLEncoder.encode(NoSID, "UTF-8");
              URL url = new URL(host + "?" + req);
              HttpURLConnection conn = (HttpURLConnection)url.openConnection();
              conn.setRequestMethod("GET");
              conn.setReadTimeout(40000);
              conn.setConnectTimeout(40000);
              BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
              String hasil = "";
              while ((output = br.readLine()) != null)
                hasil = hasil + output; 
              this.log.info("Response : " + hasil);
            } catch (UnsupportedEncodingException ex) {
              this.log.info("Error Send SMS" + ex.getMessage());
            } 
          } 
        } else {
          String req = "telco=" + URLEncoder.encode(telco, "UTF-8") + "&msisdn=" + URLEncoder.encode(msisdn, "UTF-8") + "&sms=" + URLEncoder.encode(sms, "UTF-8") + "&bank_code=" + URLEncoder.encode(bankCode, "UTF-8") + "&trans_id=" + URLEncoder.encode(transId, "UTF-8") + "&trans_date=" + URLEncoder.encode(transDate, "UTF-8") + "&no_sid=" + URLEncoder.encode(NoSID, "UTF-8");
          URL url = new URL(host + "?" + req);
          HttpURLConnection conn = (HttpURLConnection)url.openConnection();
          conn.setRequestMethod("GET");
          conn.setReadTimeout(40000);
          conn.setConnectTimeout(40000);
          BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
          String hasil = "";
          while ((output = br.readLine()) != null) {
            System.out.println(output);
            hasil = hasil + output;
          } 
        } 
      } catch (UnsupportedEncodingException ex) {
        this.log.error(ex.getMessage());
      } catch (MalformedURLException ex) {
        this.log.error(ex.getMessage());
      } catch (IOException ex) {
        this.log.info("MASUK SINI ERROR");
        this.log.error(ex.getMessage());
      } finally {
        ctx.put("TRX", trx);
      } 
    } else {
      this.log.info("Not Send SMS by request");
    } 
  }
  
  public void abort(long l, Serializable srlzbl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
