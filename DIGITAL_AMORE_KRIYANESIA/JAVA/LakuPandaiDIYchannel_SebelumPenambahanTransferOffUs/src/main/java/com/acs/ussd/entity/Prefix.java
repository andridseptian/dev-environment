package com.acs.ussd.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "prefix")
public class Prefix implements Serializable {
  @Id
  @Column(name = "id")
  private Long id;
  
  @Column(name = "provider", length = 15)
  private String provider;
  
  @Column(name = "prefix", length = 10)
  private String prefix;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getProvider() {
    return this.provider;
  }
  
  public void setProvider(String provider) {
    this.provider = provider;
  }
  
  public String getPrefix() {
    return this.prefix;
  }
  
  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }
}
