package com.acs.ussd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "account_register")
public class Registration implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "account_register_seq")
  @SequenceGenerator(name = "account_register_seq", sequenceName = "account_register_seq")
  @Column(name = "id")
  private Long id;
  
  @Column(name = "msisdn_nasabah", length = 255)
  private String msisdnNasabah;
  
  @Column(name = "no_identitas", length = 255)
  private String noIdentitas;
  
  @Column(name = "nama", length = 255)
  private String nama;
  
  @Column(name = "kota_lahir", length = 255)
  private String kotaLahir;
  
  @Column(name = "tgl_lahir", length = 255)
  private String tanggalLahir;
  
  @Column(name = "jenis_kelamin", length = 255)
  private String jenisKelamin;
  
  @Column(name = "msisdn_agen", length = 255)
  private String msisdnAgen;
  
  @Column(name = "reff", length = 255)
  private String reff;
  
  @Column(name = "provider", length = 255)
  private String provider;
  
  @Column(name = "bank_code", length = 255)
  private String bankCode;
  
  @Column(name = "dc", length = 255)
  private String dc;
  
  @Column(name = "rc", length = 255)
  private String rc;
  
  @Column(name = "rm")
  @Type(type = "text")
  private String rm;
  
  @Column(name = "dtm_created", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm;
  
  @Column(name = "free_text")
  @Type(type = "text")
  private String freeText;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getMsisdnNasabah() {
    return this.msisdnNasabah;
  }
  
  public void setMsisdnNasabah(String msisdnNasabah) {
    this.msisdnNasabah = msisdnNasabah;
  }
  
  public String getNoIdentitas() {
    return this.noIdentitas;
  }
  
  public void setNoIdentitas(String noIdentitas) {
    this.noIdentitas = noIdentitas;
  }
  
  public String getNama() {
    return this.nama;
  }
  
  public void setNama(String nama) {
    this.nama = nama;
  }
  
  public String getKotaLahir() {
    return this.kotaLahir;
  }
  
  public void setKotaLahir(String kotaLahir) {
    this.kotaLahir = kotaLahir;
  }
  
  public String getTanggalLahir() {
    return this.tanggalLahir;
  }
  
  public void setTanggalLahir(String tanggalLahir) {
    this.tanggalLahir = tanggalLahir;
  }
  
  public String getJenisKelamin() {
    return this.jenisKelamin;
  }
  
  public void setJenisKelamin(String jenisKelamin) {
    this.jenisKelamin = jenisKelamin;
  }
  
  public String getMsisdnAgen() {
    return this.msisdnAgen;
  }
  
  public void setMsisdnAgen(String msisdnAgen) {
    this.msisdnAgen = msisdnAgen;
  }
  
  public String getReff() {
    return this.reff;
  }
  
  public void setReff(String reff) {
    this.reff = reff;
  }
  
  public String getProvider() {
    return this.provider;
  }
  
  public void setProvider(String provider) {
    this.provider = provider;
  }
  
  public String getBankCode() {
    return this.bankCode;
  }
  
  public void setBankCode(String bankCode) {
    this.bankCode = bankCode;
  }
  
  public String getDc() {
    return this.dc;
  }
  
  public void setDc(String dc) {
    this.dc = dc;
  }
  
  public String getRc() {
    return this.rc;
  }
  
  public void setRc(String rc) {
    this.rc = rc;
  }
  
  public String getRm() {
    return this.rm;
  }
  
  public void setRm(String rm) {
    this.rm = rm;
  }
  
  public Date getDtm() {
    return this.dtm;
  }
  
  public void setDtm(Date dtm) {
    this.dtm = dtm;
  }
  
  public String getFreeText() {
    return this.freeText;
  }
  
  public void setFreeText(String freeText) {
    this.freeText = freeText;
  }
}
