package com.acs.ussd.dao;

import com.acs.ussd.entity.RequestMapping;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

@Repository("requestMappingDao")
public class RequestMappingDao {
  @Resource(name = "sharedEntityManager")
  private EntityManager em;
  
  public RequestMapping find(String pathInfo) {
    try {
      if (pathInfo != null)
        return (RequestMapping)this.em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.pathInfo = :pathInfo")
          .setParameter("pathInfo", pathInfo)
          .setMaxResults(1)
          .getSingleResult(); 
      return null;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public RequestMapping findByClientCode(String clientCode) {
    try {
      if (clientCode != null)
        return (RequestMapping)this.em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.client.clientCode = :clientCode")
          
          .setParameter("clientCode", clientCode)
          .setMaxResults(1)
          .getSingleResult(); 
      return null;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public RequestMapping findByClientCodeUrl(String clientCode, String url) {
    try {
      if (clientCode != null)
        return (RequestMapping)this.em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.client.clientCode = :clientCode AND r.pathInfo = :path")
          .setParameter("clientCode", clientCode)
          .setParameter("path", url)
          .setMaxResults(1)
          .getSingleResult(); 
      return null;
    } catch (NoResultException nre) {
      return null;
    } 
  }
}
