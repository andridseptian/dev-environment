package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import java.io.Serializable;
import org.apache.commons.codec.binary.Base64;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;

public class XLConfSecurity implements TransactionParticipant {
  public int prepare(long l, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    String reqMessage = trx.getReqMessage();
    if (reqMessage.contains("conf#")) {
      String[] data = reqMessage.split("#");
      String encPin = new String(Base64.encodeBase64(data[1].getBytes()));
      trx.setReqMessage(data[0] + "#" + encPin);
    } 
    ctx.put("TRX", trx);
  }
  
  public void abort(long l, Serializable srlzbl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
