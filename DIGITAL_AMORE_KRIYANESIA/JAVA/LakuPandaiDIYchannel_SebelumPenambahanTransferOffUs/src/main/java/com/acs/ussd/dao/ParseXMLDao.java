package com.acs.ussd.dao;

import com.acs.ussd.entity.ParseXML;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

@Repository("parseXMLDao")
public class ParseXMLDao {
  @Resource(name = "sharedEntityManager")
  private EntityManager em;
  
  public List<ParseXML> find(String clientCode, String provider) {
    try {
      return this.em.createQuery("SELECT p FROM ParseXML as p WHERE (p.clientCode =:clientCode OR UPPER(p.clientCode) = 'ALL') AND (p.provider =:provider OR UPPER(p.provider) = 'ALL')")
        .setParameter("clientCode", clientCode.toUpperCase())
        .setParameter("provider", provider.toUpperCase())
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}
