package com.acs.ussd.channel;

import com.acs.util.Utility;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.q2.iso.QMUX;
import org.jpos.util.NameRegistrar;

public class AutoSignonASCIIChannel extends ASCIIChannel {
  public void connect() {
    try {
      super.connect();
      doLogOn();
    } catch (org.jpos.util.NameRegistrar.NotFoundException ex) {
      Logger.getLogger(AutoSignonASCIIChannel.class.getName()).log(Level.SEVERE, (String)null, (Throwable)ex);
    } catch (IOException ex) {
      ex.printStackTrace();
    } 
  }
  
  public void doLogOn() throws NameRegistrar.NotFoundException {
    try {
      ISOMsg r = createMsg();
      r.setMTI("0800");
      r.set(7, (new SimpleDateFormat("MMddHHmmss")).format(new Date()));
      r.set(11, Utility.generateStan());
      r.set(70, "001");
      if (isConnected()) {
        QMUX mux = (QMUX)NameRegistrar.get("mux." + getName() + "-mux");
        ISOMsg rcv = mux.request(r, 5000L);
        if (rcv != null)
          if (rcv.getString(39).equals("00")); 
      } 
    } catch (ISOException ex) {
      ex.printStackTrace();
    } 
  }
}
