package com.acs.ussd.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "client")
public class Client implements Serializable {
  @Id
  @Column(name = "client_code")
  private String clientCode;
  
  @Column(name = "name", length = 100)
  private String name;
  
  @Column(name = "display_name", length = 100)
  private String displayName;
  
  @Column(name = "pic")
  @Type(type = "text")
  private String pic;
  
  @Column(name = "contact_phone")
  @Type(type = "text")
  private String contactPhone;
  
  @Column(name = "info")
  @Type(type = "text")
  private String info;
  
  public String getClientCode() {
    return this.clientCode;
  }
  
  public void setClientCode(String clientCode) {
    this.clientCode = clientCode;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getPic() {
    return this.pic;
  }
  
  public void setPic(String pic) {
    this.pic = pic;
  }
  
  public String getContactPhone() {
    return this.contactPhone;
  }
  
  public void setContactPhone(String contactPhone) {
    this.contactPhone = contactPhone;
  }
  
  public String getInfo() {
    return this.info;
  }
  
  public void setInfo(String info) {
    this.info = info;
  }
  
  public String getDisplayName() {
    return this.displayName;
  }
  
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
