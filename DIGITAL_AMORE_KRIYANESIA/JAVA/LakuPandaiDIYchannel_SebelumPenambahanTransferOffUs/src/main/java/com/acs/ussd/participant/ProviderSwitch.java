package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;
import org.jpos.util.Log;

public class ProviderSwitch implements GroupSelector, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    String provider = trx.getProvider();
    ctx.put("GRP", provider);
    return 65;
  }
  
  public String select(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    String key = ctx.getString("GRP");
    String groups = this.cfg.get(key, "Not-Supported");
    return groups;
  }
  
  public void commit(long l, Serializable srlzbl) {}
  
  public void abort(long l, Serializable srlzbl) {}
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
