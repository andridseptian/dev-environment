package com.acs.ussd.dao;

import com.acs.ussd.entity.InboxGammu;
import com.acs.ussd.entity.Registration;
import com.acs.ussd.entity.SpamGammu;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("regDao")
@Transactional
public class RegistrationDao {
  @Resource(name = "sharedEntityManager")
  private EntityManager em1;
  
  public void save(Registration registration) {
    this.em1.persist(registration);
  }
  
  public void save(InboxGammu inboxGammu) {
    this.em1.persist(inboxGammu);
  }
  
  public void save(SpamGammu spamGammu) {
    this.em1.persist(spamGammu);
  }
}
