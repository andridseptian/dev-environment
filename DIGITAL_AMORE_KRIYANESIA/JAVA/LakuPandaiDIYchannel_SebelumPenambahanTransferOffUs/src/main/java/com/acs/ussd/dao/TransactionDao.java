package com.acs.ussd.dao;

import com.acs.ussd.entity.Trx;
import java.util.Date;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("transactionDao")
@Transactional
public class TransactionDao {
  Log log = Log.getLog("Q2", getClass().getName());
  
  LogEvent le;
  
  @Resource(name = "sharedEntityManager")
  private EntityManager em;
  
  @Transactional
  public void save(Trx trx) {
    try {
      this.em.persist(trx);
    } catch (Exception ex) {
      this.le = new LogEvent(getClass().getName());
      this.le.addMessage(("Cannot save : " + trx != null) ? trx.getMsisdn() : ("trx at " + new Date()));
      this.le.addMessage("===================");
      this.le.addMessage(ex);
      this.le.addMessage("========END========");
      this.log.error(this.le);
    } 
  }
}
