package com.dcms.iso.filter;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

public class XmlFormatter {
  public String format(String xml) {
    try {
      InputSource src = new InputSource(new StringReader(xml));
      Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
      Boolean keepDeclaration = Boolean.valueOf(xml.startsWith("<?xml"));
      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS impl = (DOMImplementationLS)registry.getDOMImplementation("LS");
      LSSerializer writer = impl.createLSSerializer();
      writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
      writer.getDomConfig().setParameter("xml-declaration", keepDeclaration);
      return writer.writeToString(document);
    } catch (Exception ex) {
      System.err.println(ex.getMessage());
      return xml;
    } 
  }
}
