package com.amore.las.tx.participant.webadmin.usermobile;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import com.amore.las.utility.Tripledes;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateUserMobile implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("msisdn") && bodyData.has("account_tipe") && bodyData.has("address") && bodyData
        .has("cabang_kd_kantor") && bodyData.has("email") && bodyData.has("info") && bodyData
        .has("primary_name") && bodyData.has("pin") && bodyData.has("id") && bodyData
        .has("status_pin") && bodyData.has("status") && bodyData.has("fail_login") && bodyData
        .has("status_reset_pin") && bodyData.has("deleted") && bodyData.has("reset_msisdn")) {
        Long id_user = Long.valueOf(bodyData.getLong("id"));
        String msisdn = bodyData.getString("msisdn");
        String account_tipe = bodyData.getString("account_tipe");
        String address = bodyData.getString("address");
        int cabang_kd_kantor = bodyData.getInt("cabang_kd_kantor");
        String email = bodyData.getString("email");
        String info = bodyData.getString("info");
        String name = bodyData.getString("primary_name");
        String pin = bodyData.getString("pin");
        Boolean status_pin = Boolean.valueOf(bodyData.getBoolean("status_pin"));
        int status = bodyData.getInt("status");
        int fail_login = bodyData.getInt("fail_login");
        Boolean status_reset_pin = Boolean.valueOf(bodyData.getBoolean("status_reset_pin"));
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        Boolean reset_msisdn = Boolean.valueOf(bodyData.getBoolean("reset_msisdn"));
        Tripledes tripledes = new Tripledes("L4SD1G1T4L4M0R3KR1Y4N3S14");
        MAgent mAgent = SpringInitializer.getmAgentDao().agentById(id_user);
        if (mAgent != null) {
          mAgent.setMsisdn(msisdn);
          mAgent.setAccount_tipe(account_tipe);
          mAgent.setAddress(address);
          mAgent.setCabang_kd_kantor(cabang_kd_kantor);
          mAgent.setEmail(email);
          mAgent.setInfo(info);
          mAgent.setName(name);
          mAgent.setPin(tripledes.encrypt(pin));
          mAgent.setStatus_pin(status_pin);
          mAgent.setStatus(status);
          mAgent.setUsr_updated(mUserWeb);
          mAgent.setReset_pin(status_reset_pin);
          mAgent.setDeleted(deleted);
          mAgent.setReset_msisdn(reset_msisdn);
          mAgent.setFail_login(fail_login);
          mAgent = SpringInitializer.getmAgentDao().saveOrUpdate(mAgent);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmi\\usermobile\UpdateUserMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */