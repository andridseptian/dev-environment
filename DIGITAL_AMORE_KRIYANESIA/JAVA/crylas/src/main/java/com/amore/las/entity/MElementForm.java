package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MElementForm")
@Table(name = "m_element_form")
public class MElementForm {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_element_form_seq")
  @SequenceGenerator(name = "id_element_form_seq", sequenceName = "id_element_form_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @ManyToOne
  @JoinColumn(name = "id_form", nullable = false)
  private MForm id_form;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "label", nullable = false)
  private String label;
  
  @Column(name = "type", nullable = false)
  private String type;
  
  @Column(name = "input_type")
  private String input_type;
  
  @Column(name = "length")
  private int length;
  
  @Type(type = "text")
  @Column(name = "value")
  private String value;
  
  @Type(type = "text")
  @Column(name = "additional")
  private String additional;
  
  @Column(name = "mandatory")
  private Boolean mandatory;
  
  @Type(type = "text")
  @Column(name = "master")
  private String master;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Column(name = "deleted")
  private Boolean deleted;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public MForm getId_form() {
    return this.id_form;
  }
  
  public void setId_form(MForm id_form) {
    this.id_form = id_form;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getLabel() {
    return this.label;
  }
  
  public void setLabel(String label) {
    this.label = label;
  }
  
  public String getType() {
    return this.type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getInput_type() {
    return this.input_type;
  }
  
  public void setInput_type(String input_type) {
    this.input_type = input_type;
  }
  
  public int getLength() {
    return this.length;
  }
  
  public void setLength(int length) {
    this.length = length;
  }
  
  public String getValue() {
    return this.value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  public String getAdditional() {
    return this.additional;
  }
  
  public void setAdditional(String additional) {
    this.additional = additional;
  }
  
  public String getMaster() {
    return this.master;
  }
  
  public void setMaster(String master) {
    this.master = master;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public Boolean getMandatory() {
    return this.mandatory;
  }
  
  public void setMandatory(Boolean mandatory) {
    this.mandatory = mandatory;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MElementForm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */