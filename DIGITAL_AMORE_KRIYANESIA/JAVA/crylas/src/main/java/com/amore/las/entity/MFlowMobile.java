package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MFlowMobile")
@Table(name = "m_flow_mobile")
public class MFlowMobile {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_flow_mobile_seq")
  @SequenceGenerator(name = "id_flow_mobile_seq", sequenceName = "id_flow_mobile_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "fromid", nullable = false)
  private int fromid;
  
  @ManyToOne
  @JoinColumn(name = "to_id")
  private MFlowMobile toid;
  
  @Column(name = "form", nullable = false)
  private Boolean form;
  
  @Column(name = "id_ref", nullable = false)
  private int id_ref;
  
  @Column(name = "account_type", nullable = false)
  private String account_type;
  
  @Type(type = "text")
  @Column(name = "decription")
  private String decription;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Column(name = "deleted")
  private Boolean deleted;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public int getFromid() {
    return this.fromid;
  }
  
  public void setFromid(int fromid) {
    this.fromid = fromid;
  }
  
  public MFlowMobile getToid() {
    return this.toid;
  }
  
  public void setToid(MFlowMobile toid) {
    this.toid = toid;
  }
  
  public Boolean getForm() {
    return this.form;
  }
  
  public void setForm(Boolean form) {
    this.form = form;
  }
  
  public int getId_ref() {
    return this.id_ref;
  }
  
  public void setId_ref(int id_ref) {
    this.id_ref = id_ref;
  }
  
  public String getAccount_type() {
    return this.account_type;
  }
  
  public void setAccount_type(String account_type) {
    this.account_type = account_type;
  }
  
  public String getDecription() {
    return this.decription;
  }
  
  public void setDecription(String decription) {
    this.decription = decription;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MFlowMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */