package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "MImageMob")
@Table(name = "m_image_mobile")
public class MImageMob {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_image_mob_seq")
  @SequenceGenerator(name = "id_image_mob_seq", sequenceName = "id_image_mob_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @ManyToOne
  @JoinColumn(name = "id_transaction_mob", nullable = false)
  private MTransactionMobile id_transaction_mob;
  
  @Column(name = "id_element_form", nullable = false)
  private int id_element_form;
  
  @Column(name = "latitude", nullable = false)
  private String latitude;
  
  @Column(name = "longitude", nullable = false)
  private String longitude;
  
  @Column(name = "link_url", nullable = false)
  private String link_url;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MAgent usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MAgent usr_updated;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public MTransactionMobile getId_transaction_mob() {
    return this.id_transaction_mob;
  }
  
  public void setId_transaction_mob(MTransactionMobile id_transaction_mob) {
    this.id_transaction_mob = id_transaction_mob;
  }
  
  public int getId_element_form() {
    return this.id_element_form;
  }
  
  public void setId_element_form(int id_element_form) {
    this.id_element_form = id_element_form;
  }
  
  public String getLink_url() {
    return this.link_url;
  }
  
  public void setLink_url(String link_url) {
    this.link_url = link_url;
  }
  
  public String getLatitude() {
    return this.latitude;
  }
  
  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }
  
  public String getLongitude() {
    return this.longitude;
  }
  
  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MAgent getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MAgent usr_created) {
    this.usr_created = usr_created;
  }
  
  public MAgent getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MAgent usr_updated) {
    this.usr_updated = usr_updated;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MImageMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */