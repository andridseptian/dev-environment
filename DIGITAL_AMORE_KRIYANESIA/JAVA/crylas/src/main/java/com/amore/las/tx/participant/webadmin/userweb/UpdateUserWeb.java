package com.amore.las.tx.participant.webadmin.userweb;

import com.amore.las.entity.MGroup;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateUserWeb implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("name") && bodyData.has("password") && bodyData.has("username") && bodyData
        .has("id_group") && bodyData.has("id") && bodyData.has("status") && bodyData
        .has("deleted") && bodyData.has("status_login")) {
        Long id_user = Long.valueOf(bodyData.getLong("id"));
        String name = bodyData.getString("name");
        String password = bodyData.getString("password");
        String username = bodyData.getString("username");
        Long id_group = Long.valueOf(bodyData.getLong("id_group"));
        int status = bodyData.getInt("status");
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        int status_login = bodyData.getInt("status_login");
        MUserWeb mUserWeb1 = SpringInitializer.getmUserWebDao().userById(id_user);
        if (mUserWeb1 != null) {
          MGroup mGroup = SpringInitializer.getmGroupDao().groupById(id_group);
          mUserWeb1.setId(id_user);
          mUserWeb1.setUsername(username);
          mUserWeb1.setName(name);
          mUserWeb1.setPassword(password);
          mUserWeb1.setStatus_login(status_login);
          mUserWeb1.setId_group(mGroup);
          mUserWeb1.setStatus(status);
          mUserWeb1.setDeleted(deleted);
          mUserWeb1 = SpringInitializer.getmUserWebDao().saveOrUpdate(mUserWeb1);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmi\\userweb\UpdateUserWeb.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */