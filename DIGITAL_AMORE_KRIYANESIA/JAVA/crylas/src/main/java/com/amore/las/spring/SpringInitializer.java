package com.amore.las.spring;

import com.amore.las.dao.MAgentDao;
import com.amore.las.dao.MAgentTakedDao;
import com.amore.las.dao.MBungaDao;
import com.amore.las.dao.MCabangDao;
import com.amore.las.dao.MElementFormDao;
import com.amore.las.dao.MElementFormMobileDao;
import com.amore.las.dao.MFlowDao;
import com.amore.las.dao.MFlowMobileDao;
import com.amore.las.dao.MFormDao;
import com.amore.las.dao.MFormMobileDao;
import com.amore.las.dao.MGroupDao;
import com.amore.las.dao.MImageMobDao;
import com.amore.las.dao.MKaryawanDao;
import com.amore.las.dao.MLogMobileActivityDao;
import com.amore.las.dao.MLogWebAdminActivityDao;
import com.amore.las.dao.MMenuMobileDao;
import com.amore.las.dao.MNotarisDao;
import com.amore.las.dao.MPermissionsDao;
import com.amore.las.dao.MPrivilegeFormsDao;
import com.amore.las.dao.MReponseDao;
import com.amore.las.dao.MRoleDao;
import com.amore.las.dao.MSettingAplikasiDao;
import com.amore.las.dao.MTransactionDao;
import com.amore.las.dao.MTransactionMobileDao;
import com.amore.las.dao.MUserWebDao;
import com.amore.las.dao.MWebConfigDao;
import javax.naming.ConfigurationException;
import org.jpos.util.Log;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SpringInitializer {
  Log log = Log.getLog("Q2", getClass().getName());
  
  private static MLogMobileActivityDao mLogMobileActivityDao;
  
  private static MLogWebAdminActivityDao mLogWebAdminActivityDao;
  
  private static MReponseDao mResponseDao;
  
  private static MAgentDao mAgentDao;
  
  private static MUserWebDao mUserWebDao;
  
  private static MElementFormDao mElementFormDao;
  
  private static MFlowDao mFlowDao;
  
  private static MFormDao mFormDao;
  
  private static MGroupDao mGroupDao;
  
  private static MPermissionsDao mPermissionsDao;
  
  private static MPrivilegeFormsDao mPrivilegeFormsDao;
  
  private static MRoleDao mRoleDao;
  
  private static MTransactionDao mTransactionDao;
  
  private static MWebConfigDao mWebConfigDao;
  
  private static MElementFormMobileDao mElementFormMobileDao;
  
  private static MFlowMobileDao mFlowMobileDao;
  
  private static MMenuMobileDao mMenuMobileDao;
  
  private static MFormMobileDao mFormMobileDao;
  
  private static MTransactionMobileDao mTransactionMobileDao;
  
  private static MImageMobDao mImageMobDao;
  
  private static MAgentTakedDao mAgentTakedDao;
  
  private static MSettingAplikasiDao mSettingAplikasiDao;
  
  private static MCabangDao mCabangDao;
  
  private static MBungaDao mBungaDao;
  
  private static MNotarisDao mNotarisDao;
  
  private static MKaryawanDao mKaryawanDao;
  
  public static MLogMobileActivityDao getmLogMobileActivityDao() {
    return mLogMobileActivityDao;
  }
  
  public static void setmLogMobileActivityDao(MLogMobileActivityDao mLogMobileActivityDao) {
    SpringInitializer.mLogMobileActivityDao = mLogMobileActivityDao;
  }
  
  public static MLogWebAdminActivityDao getmLogWebAdminActivityDao() {
    return mLogWebAdminActivityDao;
  }
  
  public static void setmLogWebAdminActivityDao(MLogWebAdminActivityDao mLogWebAdminActivityDao) {
    SpringInitializer.mLogWebAdminActivityDao = mLogWebAdminActivityDao;
  }
  
  public static MReponseDao getmResponseDao() {
    return mResponseDao;
  }
  
  public static void setmResponseDao(MReponseDao mResponseDao) {
    SpringInitializer.mResponseDao = mResponseDao;
  }
  
  public static MAgentDao getmAgentDao() {
    return mAgentDao;
  }
  
  public static void setmAgentDao(MAgentDao mAgentDao) {
    SpringInitializer.mAgentDao = mAgentDao;
  }
  
  public static MUserWebDao getmUserWebDao() {
    return mUserWebDao;
  }
  
  public static void setmUserWebDao(MUserWebDao mUserWebDao) {
    SpringInitializer.mUserWebDao = mUserWebDao;
  }
  
  public static MElementFormDao getmElementFormDao() {
    return mElementFormDao;
  }
  
  public static void setmElementFormDao(MElementFormDao mElementFormDao) {
    SpringInitializer.mElementFormDao = mElementFormDao;
  }
  
  public static MFlowDao getmFlowDao() {
    return mFlowDao;
  }
  
  public static void setmFlowDao(MFlowDao mFlowDao) {
    SpringInitializer.mFlowDao = mFlowDao;
  }
  
  public static MFormDao getmFormDao() {
    return mFormDao;
  }
  
  public static void setmFormDao(MFormDao mFormDao) {
    SpringInitializer.mFormDao = mFormDao;
  }
  
  public static MGroupDao getmGroupDao() {
    return mGroupDao;
  }
  
  public static void setmGroupDao(MGroupDao mGroupDao) {
    SpringInitializer.mGroupDao = mGroupDao;
  }
  
  public static MPermissionsDao getmPermissionsDao() {
    return mPermissionsDao;
  }
  
  public static void setmPermissionsDao(MPermissionsDao mPermissionsDao) {
    SpringInitializer.mPermissionsDao = mPermissionsDao;
  }
  
  public static MPrivilegeFormsDao getmPrivilegeFormsDao() {
    return mPrivilegeFormsDao;
  }
  
  public static void setmPrivilegeFormsDao(MPrivilegeFormsDao mPrivilegeFormsDao) {
    SpringInitializer.mPrivilegeFormsDao = mPrivilegeFormsDao;
  }
  
  public static MRoleDao getmRoleDao() {
    return mRoleDao;
  }
  
  public static void setmRoleDao(MRoleDao mRoleDao) {
    SpringInitializer.mRoleDao = mRoleDao;
  }
  
  public static MTransactionDao getmTransactionDao() {
    return mTransactionDao;
  }
  
  public static void setmTransactionDao(MTransactionDao mTransactionDao) {
    SpringInitializer.mTransactionDao = mTransactionDao;
  }
  
  public Log getLog() {
    return this.log;
  }
  
  public void setLog(Log log) {
    this.log = log;
  }
  
  public static MWebConfigDao getmWebConfigDao() {
    return mWebConfigDao;
  }
  
  public static void setmWebConfigDao(MWebConfigDao mWebConfigDao) {
    SpringInitializer.mWebConfigDao = mWebConfigDao;
  }
  
  public static MElementFormMobileDao getmElementFormMobileDao() {
    return mElementFormMobileDao;
  }
  
  public static void setmElementFormMobileDao(MElementFormMobileDao mElementFormMobileDao) {
    SpringInitializer.mElementFormMobileDao = mElementFormMobileDao;
  }
  
  public static MFlowMobileDao getmFlowMobileDao() {
    return mFlowMobileDao;
  }
  
  public static void setmFlowMobileDao(MFlowMobileDao mFlowMobileDao) {
    SpringInitializer.mFlowMobileDao = mFlowMobileDao;
  }
  
  public static MMenuMobileDao getmMenuMobileDao() {
    return mMenuMobileDao;
  }
  
  public static void setmMenuMobileDao(MMenuMobileDao mMenuMobileDao) {
    SpringInitializer.mMenuMobileDao = mMenuMobileDao;
  }
  
  public static MFormMobileDao getmFormMobileDao() {
    return mFormMobileDao;
  }
  
  public static void setmFormMobileDao(MFormMobileDao mFormMobileDao) {
    SpringInitializer.mFormMobileDao = mFormMobileDao;
  }
  
  public static MTransactionMobileDao getmTransactionMobileDao() {
    return mTransactionMobileDao;
  }
  
  public static void setmTransactionMobileDao(MTransactionMobileDao mTransactionMobileDao) {
    SpringInitializer.mTransactionMobileDao = mTransactionMobileDao;
  }
  
  public static MImageMobDao getmImageMobDao() {
    return mImageMobDao;
  }
  
  public static void setmImageMobDao(MImageMobDao mImageMobDao) {
    SpringInitializer.mImageMobDao = mImageMobDao;
  }
  
  public static MAgentTakedDao getmAgentTakedDao() {
    return mAgentTakedDao;
  }
  
  public static void setmAgentTakedDao(MAgentTakedDao mAgentTakedDao) {
    SpringInitializer.mAgentTakedDao = mAgentTakedDao;
  }
  
  public static MSettingAplikasiDao getmSettingAplikasiDao() {
    return mSettingAplikasiDao;
  }
  
  public static void setmSettingAplikasiDao(MSettingAplikasiDao mSettingAplikasiDao) {
    SpringInitializer.mSettingAplikasiDao = mSettingAplikasiDao;
  }
  
  public static MCabangDao getmCabangDao() {
    return mCabangDao;
  }
  
  public static void setmCabangDao(MCabangDao mCabangDao) {
    SpringInitializer.mCabangDao = mCabangDao;
  }
  
  public static MBungaDao getmBungaDao() {
    return mBungaDao;
  }
  
  public static void setmBungaDao(MBungaDao mBungaDao) {
    SpringInitializer.mBungaDao = mBungaDao;
  }
  
  public static MNotarisDao getmNotarisDao() {
    return mNotarisDao;
  }
  
  public static void setmNotarisDao(MNotarisDao mNotarisDao) {
    SpringInitializer.mNotarisDao = mNotarisDao;
  }
  
  public static MKaryawanDao getmKaryawanDao() {
    return mKaryawanDao;
  }
  
  public static void setmKaryawanDao(MKaryawanDao mKaryawanDao) {
    SpringInitializer.mKaryawanDao = mKaryawanDao;
  }
  
  public void initService() throws ConfigurationException {
    FileSystemXmlApplicationContext fileSystemXmlApplicationContext = new FileSystemXmlApplicationContext("ApplicationContext.xml");
    setmLogMobileActivityDao((MLogMobileActivityDao)fileSystemXmlApplicationContext.getBean("MLogMobileActivityDao", MLogMobileActivityDao.class));
    setmLogWebAdminActivityDao((MLogWebAdminActivityDao)fileSystemXmlApplicationContext.getBean("MLogWebAdminActivityDao", MLogWebAdminActivityDao.class));
    setmResponseDao((MReponseDao)fileSystemXmlApplicationContext.getBean("MReponseDao", MReponseDao.class));
    setmAgentDao((MAgentDao)fileSystemXmlApplicationContext.getBean("MAgentDao", MAgentDao.class));
    setmUserWebDao((MUserWebDao)fileSystemXmlApplicationContext.getBean("MUserWebDao", MUserWebDao.class));
    setmElementFormDao((MElementFormDao)fileSystemXmlApplicationContext.getBean("MElementFormDao", MElementFormDao.class));
    setmFlowDao((MFlowDao)fileSystemXmlApplicationContext.getBean("MFlowDao", MFlowDao.class));
    setmFormDao((MFormDao)fileSystemXmlApplicationContext.getBean("MFormDao", MFormDao.class));
    setmGroupDao((MGroupDao)fileSystemXmlApplicationContext.getBean("MGroupDao", MGroupDao.class));
    setmPermissionsDao((MPermissionsDao)fileSystemXmlApplicationContext.getBean("MPermissionsDao", MPermissionsDao.class));
    setmPrivilegeFormsDao((MPrivilegeFormsDao)fileSystemXmlApplicationContext.getBean("MPrivilegeFormsDao", MPrivilegeFormsDao.class));
    setmRoleDao((MRoleDao)fileSystemXmlApplicationContext.getBean("MRoleDao", MRoleDao.class));
    setmTransactionDao((MTransactionDao)fileSystemXmlApplicationContext.getBean("MTransactionDao", MTransactionDao.class));
    setmWebConfigDao((MWebConfigDao)fileSystemXmlApplicationContext.getBean("MWebConfigDao", MWebConfigDao.class));
    setmElementFormMobileDao((MElementFormMobileDao)fileSystemXmlApplicationContext.getBean("MElementFormMobileDao", MElementFormMobileDao.class));
    setmFlowMobileDao((MFlowMobileDao)fileSystemXmlApplicationContext.getBean("MFlowMobileDao", MFlowMobileDao.class));
    setmMenuMobileDao((MMenuMobileDao)fileSystemXmlApplicationContext.getBean("MMenuMobileDao", MMenuMobileDao.class));
    setmFormMobileDao((MFormMobileDao)fileSystemXmlApplicationContext.getBean("MFormMobileDao", MFormMobileDao.class));
    setmTransactionMobileDao((MTransactionMobileDao)fileSystemXmlApplicationContext.getBean("MTransactionMobileDao", MTransactionMobileDao.class));
    setmImageMobDao((MImageMobDao)fileSystemXmlApplicationContext.getBean("MImageMobDao", MImageMobDao.class));
    setmAgentTakedDao((MAgentTakedDao)fileSystemXmlApplicationContext.getBean("MAgentTakedDao", MAgentTakedDao.class));
    setmSettingAplikasiDao((MSettingAplikasiDao)fileSystemXmlApplicationContext.getBean("MSettingAplikasiDao", MSettingAplikasiDao.class));
    setmCabangDao((MCabangDao)fileSystemXmlApplicationContext.getBean("MCabangDao", MCabangDao.class));
    setmBungaDao((MBungaDao)fileSystemXmlApplicationContext.getBean("MBungaDao", MBungaDao.class));
    setmNotarisDao((MNotarisDao)fileSystemXmlApplicationContext.getBean("MNotarisDao", MNotarisDao.class));
    setmKaryawanDao((MKaryawanDao)fileSystemXmlApplicationContext.getBean("MKaryawanDao", MKaryawanDao.class));
    this.log.info("Init DB has successfull");
    this.log.info("Service Started");
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\spring\SpringInitializer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */