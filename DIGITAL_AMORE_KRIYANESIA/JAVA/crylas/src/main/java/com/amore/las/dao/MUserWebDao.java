package com.amore.las.dao;

import com.amore.las.entity.MUserWeb;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MUserWebDao")
@Transactional
public class MUserWebDao extends Dao {
  public MUserWeb saveOrUpdate(MUserWeb mUserWeb) {
    Date date = new Date();
    if (mUserWeb.getId() == null) {
      mUserWeb.setDtm_created(date);
      this.em.persist(mUserWeb);
    } else {
      mUserWeb.setDtm_updated(date);
      this.em.merge(mUserWeb);
    } 
    return mUserWeb;
  }
  
  public MUserWeb userByUsername(String username) {
    try {
      MUserWeb muw = (MUserWeb)this.em.createQuery("SELECT muw FROM MUserWeb muw WHERE muw.username = :username and muw.deleted = false").setParameter("username", username).setMaxResults(1).getSingleResult();
      return muw;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MUserWeb userById(Long id) {
    try {
      MUserWeb muw = (MUserWeb)this.em.createQuery("SELECT muw FROM MUserWeb muw WHERE muw.id = :id and muw.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return muw;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MUserWeb> allUser() {
    try {
      return this.em.createQuery("SELECT muw FROM MUserWeb muw where muw.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MUserWeb> userParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT muw FROM MUserWeb muw where muw.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MUserWeb> userParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT muw FROM MUserWeb muw where muw.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MUserWebDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */