package com.amore.las.dao;

import com.amore.las.entity.MNotaris;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MNotarisDao")
@Transactional
public class MNotarisDao extends Dao {
  public MNotaris saveOrUpdate(MNotaris mNotaris) {
    Date date = new Date();
    if (mNotaris.getId() == null) {
      mNotaris.setDtm_created(date);
      this.em.persist(mNotaris);
    } else {
      mNotaris.setDtm_updated(date);
      this.em.merge(mNotaris);
    } 
    return mNotaris;
  }
  
  public MNotaris byId(Long id) {
    try {
      MNotaris ma = (MNotaris)this.em.createQuery("SELECT ma FROM MNotaris ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MNotaris byName(String name) {
    try {
      MNotaris ma = (MNotaris)this.em.createQuery("SELECT ma FROM MNotaris ma WHERE ma.name = :name and ma.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MNotaris> byPrice(int price) {
    try {
      return this.em.createQuery("SELECT ma FROM MNotaris ma where ma.price = :price and ma.deleted = false")
        .setParameter("price", Integer.valueOf(price))
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MNotaris> getAllNotaris() {
    try {
      return this.em.createQuery("SELECT ma FROM MNotaris ma where ma.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MNotaris> notarisParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT ma FROM MNotaris ma where ma.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MNotaris> notarisParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT ma FROM MNotaris ma where ma.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MNotarisDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */