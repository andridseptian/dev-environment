package com.amore.las.utility;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Random;
import org.jpos.iso.ISOUtil;
import org.jpos.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class Utility {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public static JSONObject merge(JSONObject... jsonObjects) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    for (JSONObject temp : jsonObjects) {
      Iterator<String> keys = temp.keys();
      while (keys.hasNext()) {
        String key = keys.next();
        jsonObject.put(key, temp.get(key));
      } 
    } 
    return jsonObject;
  }
  
  public static String getStan() {
    String s = String.valueOf(Math.abs((new Random()).nextLong()));
    return s.substring(s.length() - 6);
  }
  
  public static String getTrxId() {
    String s = ISOUtil.getRandomDigits(new Random(), 30, 9);
    return s;
  }
  
  public static String getReffNumber() {
    String s = ISOUtil.getRandomDigits(new Random(), 30, 9);
    return s;
  }
  
  public static String getRrn() {
    String s = ISOUtil.getRandomDigits(new Random(), 12, 9);
    return s;
  }
  
  public static String getOtp() {
    String s = String.valueOf(Math.abs((new Random()).nextLong()));
    return s.substring(s.length() - 6);
  }
  
  public static String replaceMsisdn(String msisdn) {
    StringBuilder sb = new StringBuilder();
    if (msisdn.startsWith("0")) {
      sb.append("62");
      sb.append(msisdn.substring(1));
    } else if (msisdn.startsWith("620")) {
      sb.append("62");
      sb.append(msisdn.substring(3));
    } else if (msisdn.startsWith("+62")) {
      sb.append(msisdn.substring(1));
    } else {
      sb.append(msisdn);
    } 
    return sb.toString();
  }
  
  public static String replaceMsisdnToCore(String msisdn) {
    StringBuilder sb = new StringBuilder();
    sb.append("0");
    sb.append(msisdn.substring(2));
    return sb.toString();
  }
  
  public static String formatAmount(double val) {
    String ret = "";
    DecimalFormat decFrm = new DecimalFormat();
    decFrm.applyPattern("###,###,###,###,###,###");
    ret = decFrm.format(val).replace(',', '.');
    return ret;
  }
  
  public static String generateDigitPin() {
    int digit1 = getRandomIntegerBetweenRange(1, 9);
    int digit2 = getRandomIntegerBetweenRange(digit1, 9);
    int digit3 = getRandomIntegerBetweenRange(digit2, 9);
    int digit4 = getRandomIntegerBetweenRange(1, 9);
    int digit5 = getRandomIntegerBetweenRange(1, 9);
    int digit6 = getRandomIntegerBetweenRange(1, 9);
    for (int i = 0; i < 6; i++) {
      if (digit1 == digit2) {
        digit2 = getRandomIntegerBetweenRange(digit1, 9);
      } else if (digit3 == digit4) {
        digit3 = getRandomIntegerBetweenRange(1, 6);
      } else if (digit5 == digit6) {
        digit4 = getRandomIntegerBetweenRange(digit3, 9);
      } else if (digit4 == digit5) {
        digit6 = getRandomIntegerBetweenRange(1, 8);
      } else {
        break;
      } 
    } 
    String random = "" + digit1 + "" + digit2 + "" + digit3 + "" + digit4 + "" + digit5 + "" + digit6;
    return random;
  }
  
  public static int getRandomIntegerBetweenRange(int min, int max) {
    int x = (int)(Math.random() * (max - min + 1)) + min;
    return x;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\la\\utility\Utility.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */