package com.amore.las.dao;

import com.amore.las.entity.MFlow;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MFlowDao")
@Transactional
public class MFlowDao extends Dao {
  public MFlow saveOrUpdate(MFlow mFlow) {
    Date date = new Date();
    if (mFlow.getId() == null) {
      mFlow.setDtm_created(date);
      this.em.persist(mFlow);
    } else {
      mFlow.setDtm_updated(date);
      this.em.merge(mFlow);
    } 
    return mFlow;
  }
  
  public MFlow flowById(Long id) {
    try {
      MFlow mf = (MFlow)this.em.createQuery("SELECT mf FROM MFlow mf WHERE mf.id = :id and mf.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mf;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MFlow flowByName(String name) {
    try {
      MFlow mf = (MFlow)this.em.createQuery("SELECT mf FROM MFlow mf WHERE mf.name = :name and mf.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return mf;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlow> allFlow() {
    try {
      return this.em.createQuery("SELECT mf FROM MFlow mf where mf.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlow> flowParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mf FROM MFlow mf where mf.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlow> flowParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mf FROM MFlow mf where mf.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MFlowDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */