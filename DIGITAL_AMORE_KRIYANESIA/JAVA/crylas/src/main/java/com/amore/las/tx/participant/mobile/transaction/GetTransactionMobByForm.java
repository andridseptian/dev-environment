package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MImageMob;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MSettingAplikasi;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionMobile;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetTransactionMobByForm implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      int id_flow = bodyData.getInt("id_form");
      MTransaction mt = SpringInitializer.getmTransactionDao().transactionById(Long.valueOf(bodyData.getLong("trx_web")));
      MTransactionMobile mTransaction = SpringInitializer.getmTransactionMobileDao().transactionByForm(mAgent, id_flow, mt);
      MSettingAplikasi mSettingAplikasi = SpringInitializer.getmSettingAplikasiDao().byName("image mobile");
      if (mTransaction != null) {
        JSONArray logData = new JSONArray(mTransaction.getData_value());
        List<MImageMob> mImageMob = SpringInitializer.getmImageMobDao().byIdTrxMob(mTransaction);
        if (mImageMob.size() > 0) {
          int a = mImageMob.size();
          int length = logData.length();
          bodyData = new JSONObject();
          bodyData.put("id", mTransaction.getId());
          JSONObject img = null;
          for (int i = 0; i < a; i++) {
            for (int j = 0; j < length; j++) {
              img = logData.getJSONObject(i);
              if (img.getString("type").equals("image_upload"))
                img.put("value", mSettingAplikasi.getUrl().toString() + ((MImageMob)mImageMob.get(i)).getLink_url()); 
            } 
          } 
          bodyData.put("data", logData);
        } else {
          bodyData = new JSONObject();
          bodyData.put("id", mTransaction.getId());
          bodyData.put("data", logData);
        } 
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\GetTransactionMobByForm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */