package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetTransactionWeb1 implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      List<Object[]> list_trx = SpringInitializer.getmTransactionDao().ListTrxByDepartemen(mAgent.getAccount_tipe());
      int length = list_trx.size();
      if (length > 0) {
        JSONArray data = new JSONArray();
        for (int b = 0; b < length; b++) {
          JSONObject logData = new JSONObject();
          Long idx = Long.valueOf(Long.parseLong(((Object[])list_trx.get(b))[0].toString()));
          Long iddx = Long.valueOf(Long.parseLong(((Object[])list_trx.get(b))[1].toString()));
          MTransactionDetail mTransactionDetail = SpringInitializer.getmTransactionDao().getTransactionDetailFirstDept(iddx);
          JSONArray dt = new JSONArray(mTransactionDetail.getData_value().toString());
          int lengthDt = dt.length();
          String cabang = null, nama = null, flapon = null, produk = null;
          String alamat = null;
          String rtrw = null;
          String kelurahan = null;
          String kecamatan = null;
          String kota = null;
          String provinsi = null;
          String kodepos = null;
          for (int i = 0; i < lengthDt; i++) {
            JSONObject jso = dt.getJSONObject(i);
            if (jso.getString("view_title").equals("Cabang")) {
              cabang = jso.getString("value");
            } else if (jso.getString("view_title").equals("Nama Lengkap")) {
              nama = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kredit Mohon")) {
              flapon = jso.getString("value");
            } else if (jso.getString("view_title").equals("Jenis Kredit")) {
              produk = jso.getString("value");
            } else if (jso.getString("view_title").equals("Alamat Sekarang")) {
              alamat = jso.getString("value");
            } else if (jso.getString("view_title").equals("RT / RW Sekarang")) {
              rtrw = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kelurahan Sekarang")) {
              kelurahan = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kecamatan Sekarang")) {
              kecamatan = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kota Sekarang")) {
              kota = jso.getString("value");
            } else if (jso.getString("view_title").equals("Provinsi Sekarang")) {
              provinsi = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kode Pos Sekarang")) {
              kodepos = jso.getString("value");
            } 
          } 
          logData.put("id", iddx);
          logData.put("cabang", cabang);
          logData.put("nama", nama);
          logData.put("flapon", flapon);
          logData.put("produk", produk);
          String alamat_lengkap = alamat + " RT/RW. " + rtrw + " Kelurahan " + kelurahan + " Kecamatan " + kecamatan + " Kota " + kota + " " + provinsi + " " + kodepos;
          logData.put("alamat", alamat_lengkap);
          data.put(logData);
        } 
        bodyData.put("data", data);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\GetTransactionWeb1.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */