package com.amore.las.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "MMenuMobile")
@Table(name = "m_menu_mobile")
public class MMenuMobile {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_menu_mobile_seq")
  @SequenceGenerator(name = "id_menu_mobile_seq", sequenceName = "id_menu_mobile_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "view_title", nullable = false)
  private String view_title;
  
  @Column(name = "main", nullable = false)
  private Boolean main;
  
  @Column(name = "tipe")
  private String tipe;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "id_menu_mobile", cascade = {CascadeType.ALL})
  private List<MFormMobile> form_mobile;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getView_title() {
    return this.view_title;
  }
  
  public void setView_title(String view_title) {
    this.view_title = view_title;
  }
  
  public Boolean getMain() {
    return this.main;
  }
  
  public void setMain(Boolean main) {
    this.main = main;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public List<MFormMobile> getForm_mobile() {
    return this.form_mobile;
  }
  
  public void setForm_mobile(List<MFormMobile> form_mobile) {
    this.form_mobile = form_mobile;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public String getTipe() {
    return this.tipe;
  }
  
  public void setTipe(String tipe) {
    this.tipe = tipe;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MMenuMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */