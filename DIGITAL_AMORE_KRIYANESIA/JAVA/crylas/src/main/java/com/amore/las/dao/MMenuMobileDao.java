package com.amore.las.dao;

import com.amore.las.entity.MMenuMobile;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MMenuMobileDao")
@Transactional
public class MMenuMobileDao extends Dao {
  public MMenuMobile saveOrUpdate(MMenuMobile mMenuMobile) {
    Date date = new Date();
    if (mMenuMobile.getId() == null) {
      mMenuMobile.setDtm_created(date);
      this.em.persist(mMenuMobile);
    } else {
      mMenuMobile.setDtm_updated(date);
      this.em.merge(mMenuMobile);
    } 
    return mMenuMobile;
  }
  
  public MMenuMobile byId(Long id) {
    try {
      MMenuMobile mm = (MMenuMobile)this.em.createQuery("SELECT mm FROM MMenuMobile mm WHERE mm.id = :id and mm.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MMenuMobile byTitle(String title) {
    try {
      MMenuMobile mm = (MMenuMobile)this.em.createQuery("SELECT mm FROM MMenuMobile mm WHERE mm.view_title = :title and mm.deleted = false").setParameter("title", title).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MMenuMobile> allForm() {
    try {
      return this.em.createQuery("SELECT mf FROM MMenuMobile mf WHERE mf.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MMenuMobile> menuParam(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT me FROM MMenuMobile me where me.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MMenuMobile> menuParamCount(String query) {
    try {
      return this.em.createQuery("SELECT me FROM MMenuMobile me where me.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MMenuMobileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */