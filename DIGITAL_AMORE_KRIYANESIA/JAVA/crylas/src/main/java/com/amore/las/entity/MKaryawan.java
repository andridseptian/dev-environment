package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "MKaryawan")
@Table(name = "m_karyawan")
public class MKaryawan {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_karyawan_seq")
  @SequenceGenerator(name = "id_karyawan_seq", sequenceName = "id_karyawan_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "position")
  private String position;
  
  @Column(name = "cabang")
  private String cabang;
  
  @Column(name = "nik_karyawan")
  private String nik_karyawan;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getPosition() {
    return this.position;
  }
  
  public void setPosition(String position) {
    this.position = position;
  }
  
  public String getCabang() {
    return this.cabang;
  }
  
  public void setCabang(String cabang) {
    this.cabang = cabang;
  }
  
  public String getNik_karyawan() {
    return this.nik_karyawan;
  }
  
  public void setNik_karyawan(String nik_karyawan) {
    this.nik_karyawan = nik_karyawan;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MKaryawan.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */