package com.amore.las.dao;

import com.amore.las.entity.MLogWebAdminActivity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MLogWebAdminActivityDao")
@Transactional
public class MLogWebAdminActivityDao extends Dao {
  public MLogWebAdminActivity saveOrUpdate(MLogWebAdminActivity mLogWebAdminActivity) {
    if (mLogWebAdminActivity.getId() == null) {
      this.em.persist(mLogWebAdminActivity);
    } else {
      this.em.merge(mLogWebAdminActivity);
    } 
    return mLogWebAdminActivity;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MLogWebAdminActivityDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */