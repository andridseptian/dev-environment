package com.amore.las.tx.participant.webadmin.formmob;

import com.amore.las.entity.MFormMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateFormMob implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("id") && bodyData.has("description") && bodyData.has("header") && bodyData
        .has("name") && bodyData.has("id_menu") && bodyData.has("mandatory") && bodyData
        .has("deleted") && bodyData.has("position")) {
        Long id_form = Long.valueOf(bodyData.getLong("id"));
        String desc = bodyData.getString("description");
        String header = bodyData.getString("header");
        String name = bodyData.getString("name");
        Long id_menu = Long.valueOf(bodyData.getLong("id_menu"));
        Boolean mandatory = Boolean.valueOf(bodyData.getBoolean("mandatory"));
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        String position = bodyData.getString("position");
        int pos = 0;
        if (bodyData.getString("position").equals("-") || bodyData.getString("position").equals("")) {
          pos = 0;
        } else {
          pos = Integer.parseInt(position);
        } 
        MFormMobile mFormz = SpringInitializer.getmFormMobileDao().byId(id_form);
        if (mFormz != null) {
          mFormz.setDecription(desc);
          mFormz.setHeader(header);
          mFormz.setName(name);
          mFormz.setId_menu_mobile(SpringInitializer.getmMenuMobileDao().byId(id_menu));
          mFormz.setMandatory(mandatory);
          mFormz.setUsr_updated(mUserWeb);
          mFormz.setPosition(pos);
          mFormz.setDeleted(deleted);
          SpringInitializer.getmFormMobileDao().saveOrUpdate(mFormz);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("FA");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\formmob\UpdateFormMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */