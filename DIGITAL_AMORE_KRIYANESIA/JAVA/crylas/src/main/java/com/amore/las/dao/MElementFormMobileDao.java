package com.amore.las.dao;

import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MFormMobile;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MElementFormMobileDao")
@Transactional
public class MElementFormMobileDao extends Dao {
  public MElementFormMobile saveOrUpdate(MElementFormMobile mElementFormMobile) {
    Date date = new Date();
    if (mElementFormMobile.getId() == null) {
      mElementFormMobile.setDtm_created(date);
      this.em.persist(mElementFormMobile);
    } else {
      mElementFormMobile.setDtm_updated(date);
      this.em.merge(mElementFormMobile);
    } 
    return mElementFormMobile;
  }
  
  public MElementFormMobile byId(Long id) {
    try {
      MElementFormMobile mm = (MElementFormMobile)this.em.createQuery("SELECT mm FROM MElementFormMobile mm WHERE mm.id = :id and mm.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MElementFormMobile byLabel(String label) {
    try {
      MElementFormMobile mm = (MElementFormMobile)this.em.createQuery("SELECT mm FROM MElementFormMobile mm WHERE mm.label = :label and mm.deleted = false").setParameter("label", label).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementFormMobile> byIdForm(MFormMobile mFormMobile) {
    try {
      return this.em.createQuery("SELECT mm FROM MElementFormMobile mm WHERE mm.id_form_mobile = :mFormMobile and mm.deleted = false order by mm.position asc")
        .setParameter("mFormMobile", mFormMobile)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementFormMobile> allElement() {
    try {
      return this.em.createQuery("SELECT mm FROM MElementFormMobile mm WHERE mm.deleted = false order by mm.position asc")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementFormMobile> elementParam(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT me FROM MElementFormMobile me where me.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementFormMobile> elementParamCount(String query) {
    try {
      return this.em.createQuery("SELECT me FROM MElementFormMobile me where me.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MElementFormMobileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */