package com.amore.las.dao;

import com.amore.las.entity.MPermissions;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MPermissionsDao")
@Transactional
public class MPermissionsDao extends Dao {
  public MPermissions saveOrUpdate(MPermissions mPermissions) {
    Date date = new Date();
    if (mPermissions.getId() == null) {
      mPermissions.setDtm_created(date);
      this.em.persist(mPermissions);
    } else {
      mPermissions.setDtm_updated(date);
      this.em.merge(mPermissions);
    } 
    return mPermissions;
  }
  
  public void remove(Long id) {
    MPermissions mPermissions = (MPermissions)this.em.find(MPermissions.class, id);
    this.em.remove(mPermissions);
  }
  
  public MPermissions permissionById(Long id) {
    try {
      MPermissions mp = (MPermissions)this.em.createQuery("SELECT mp FROM MPermissions mp WHERE mp.id = :id and mp.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mp;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MPermissions permissionByName(String name) {
    try {
      MPermissions mp = (MPermissions)this.em.createQuery("SELECT mp FROM MPermissions mp WHERE mp.name = :name and mp.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return mp;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MPermissions> allPermissions() {
    try {
      return this.em.createQuery("SELECT mp FROM MPermissions mp where mp.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MPermissions> permissionParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mp FROM MPermissions mp where mp.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MPermissions> permissionParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mp FROM MPermissions mp where mp.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MPermissionsDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */