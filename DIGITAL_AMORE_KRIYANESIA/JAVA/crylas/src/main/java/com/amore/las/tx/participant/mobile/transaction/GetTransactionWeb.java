package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetTransactionWeb implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    String cabang = null, nama = null, flapon = null, produk = null;
    String alamat = null;
    String rtrw = null;
    String kelurahan = null;
    String kecamatan = null;
    String kota = null;
    String provinsi = null;
    String kodepos = null;
    Long id_head = null;
    String alamat_lengkap = null;
    try {
      String surveyor = "all";
      List<MTransaction> list_head = SpringInitializer.getmTransactionDao().mySurveyTransactionWebMob(mAgent.getAccount_tipe(), surveyor);
      if (list_head.size() > 0) {
        JSONArray data = new JSONArray();
        for (MTransaction mTransaction : list_head) {
          JSONObject logData = new JSONObject();
          List<MTransactionDetail> list_trx = SpringInitializer.getmTransactionDao().transactionDetailByIdHead(mTransaction);
          if (list_trx.size() > 0) {
            for (MTransactionDetail mTransactionDetail : list_trx) {
              JSONArray jarr = new JSONArray(mTransactionDetail.getData_value());
              int length = jarr.length();
              for (int i = 0; i < length; i++) {
                JSONObject jso = jarr.getJSONObject(i);
                if (jso.getString("name").equalsIgnoreCase("Cabang")) {
                  cabang = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("Nama")) {
                  nama = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("plafon")) {
                  flapon = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("JenisKredit")) {
                  produk = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("AlamatSekarang")) {
                  alamat = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("RTRWSekarang")) {
                  rtrw = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("KelurahanSekarang")) {
                  kelurahan = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("KecamatanSekarang")) {
                  kecamatan = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("KotaSekarang")) {
                  kota = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("ProvinsiSekarang")) {
                  provinsi = jso.getString("value");
                } else if (jso.getString("name").equalsIgnoreCase("KodePosSekarang")) {
                  kodepos = jso.getString("value");
                } 
              } 
              id_head = mTransactionDetail.getId_head().getId();
            } 
          } else {
            this.rc = SpringInitializer.getmResponseDao().getRc("20");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } 
          logData.put("id", id_head);
          logData.put("cabang", cabang);
          logData.put("nama", nama);
          logData.put("flapon", flapon);
          logData.put("produk", produk);
          alamat_lengkap = alamat + " RT/RW. " + rtrw + " Kelurahan " + kelurahan + " Kecamatan " + kecamatan + " Kota " + kota + " " + provinsi + " " + kodepos;
          logData.put("alamat", alamat_lengkap);
          data.put(logData);
        } 
        bodyData.put("data", data);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\GetTransactionWeb.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */