package com.amore.las.utility;

import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class TestEncryptDec implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    try {
      String pin = bodyData.getString("pin");
      Tripledes tripledes = new Tripledes("L4SD1G1T4L4M0R3KR1Y4N3S14");
      JSONObject data = new JSONObject();
      data.put("name", tripledes.encrypt(pin));
      bodyData.put("data", data);
      this.rc = SpringInitializer.getmResponseDao().getRc("00");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
    } catch (Exception e) {
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
    } 
    ctx.put("RESPONSE", this.wsResponse);
    ctx.put("RC", this.rc);
    ctx.put("STATUS", "0");
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\la\\utility\TestEncryptDec.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */