package com.amore.las.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MFormMobile")
@Table(name = "m_form_mobile")
public class MFormMobile {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_form_mobile_seq")
  @SequenceGenerator(name = "id_form_mobile_seq", sequenceName = "id_form_mobile_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @ManyToOne
  @JoinColumn(name = "id_menu_mobile")
  private MMenuMobile id_menu_mobile;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "header", nullable = false)
  private String header;
  
  @Column(name = "link_url", nullable = false)
  private String link_url;
  
  @Column(name = "mandatory")
  private Boolean mandatory;
  
  @Type(type = "text")
  @Column(name = "decription")
  private String decription;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Column(name = "position")
  private int position;
  
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "id_form_mobile", cascade = {CascadeType.ALL})
  private List<MElementFormMobile> element_form_mobile;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getHeader() {
    return this.header;
  }
  
  public void setHeader(String header) {
    this.header = header;
  }
  
  public String getDecription() {
    return this.decription;
  }
  
  public void setDecription(String decription) {
    this.decription = decription;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public List<MElementFormMobile> getElement_form_mobile() {
    return this.element_form_mobile;
  }
  
  public void setElement_form_mobile(List<MElementFormMobile> element_form_mobile) {
    this.element_form_mobile = element_form_mobile;
  }
  
  public MMenuMobile getId_menu_mobile() {
    return this.id_menu_mobile;
  }
  
  public void setId_menu_mobile(MMenuMobile id_menu_mobile) {
    this.id_menu_mobile = id_menu_mobile;
  }
  
  public String getLink_url() {
    return this.link_url;
  }
  
  public void setLink_url(String link_url) {
    this.link_url = link_url;
  }
  
  public Boolean getMandatory() {
    return this.mandatory;
  }
  
  public void setMandatory(Boolean mandatory) {
    this.mandatory = mandatory;
  }
  
  public int getPosition() {
    return this.position;
  }
  
  public void setPosition(int position) {
    this.position = position;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MFormMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */