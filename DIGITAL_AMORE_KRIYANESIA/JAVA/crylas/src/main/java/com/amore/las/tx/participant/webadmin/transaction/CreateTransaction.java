package com.amore.las.tx.participant.webadmin.transaction;

import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CreateTransaction implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("status_transaction") && bodyData.has("surveyor") && bodyData.has("status_survey") && bodyData
        .has("id_trx") && bodyData.has("data_value") && bodyData.has("position") && bodyData
        .has("departemen")) {
        int status_transaction = bodyData.getInt("status_transaction");
        String surveyor = bodyData.getString("surveyor");
        String status_survey = bodyData.getString("status_survey");
        String id_trx = bodyData.getString("id_trx");
        String data_value = bodyData.getString("data_value");
        String position = bodyData.getString("position");
        String departemen = bodyData.getString("departemen");
        MTransaction mTransaction = null;
        if (id_trx.equals("") || id_trx.equals(null) || id_trx.equals("-")) {
          mTransaction = new MTransaction();
          mTransaction.setStatus_transaction(status_transaction);
          mTransaction.setUsr_created(mUserWeb);
          mTransaction.setDeleted(Boolean.valueOf(false));
          mTransaction = SpringInitializer.getmTransactionDao().saveOrUpdate(mTransaction);
        } else {
          Long idx = Long.valueOf(Long.parseLong(id_trx));
          mTransaction = SpringInitializer.getmTransactionDao().transactionById(idx);
        } 
        MTransactionDetail mTransactionDetail = new MTransactionDetail();
        mTransactionDetail.setId_head(mTransaction);
        mTransactionDetail.setPosition(position);
        mTransactionDetail.setData_value(data_value);
        mTransactionDetail.setDepartemen(departemen);
        mTransactionDetail.setSurveyor(surveyor);
        mTransactionDetail.setStatus_survey(status_survey);
        mTransactionDetail.setUsr_created(mUserWeb);
        mTransactionDetail.setDeleted(Boolean.valueOf(false));
        mTransactionDetail = SpringInitializer.getmTransactionDao().saveOrUpdateDetail(mTransactionDetail);
        bodyData.put("id_trx", mTransactionDetail.getId_head().getId());
        bodyData.put("status_survey", mTransactionDetail.getStatus_survey());
        bodyData.put("surveyor", mTransactionDetail.getSurveyor());
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\transaction\CreateTransaction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */