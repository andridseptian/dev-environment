package com.amore.las.dao;

import com.amore.las.entity.MRole;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MRoleDao")
@Transactional
public class MRoleDao extends Dao {
  public MRole saveOrUpdate(MRole mRole) {
    Date date = new Date();
    if (mRole.getId() == null) {
      mRole.setDtm_created(date);
      this.em.persist(mRole);
    } else {
      mRole.setDtm_updated(date);
      this.em.merge(mRole);
    } 
    return mRole;
  }
  
  public void remove(Long id) {
    MRole mPermissions = (MRole)this.em.find(MRole.class, id);
    this.em.remove(mPermissions);
  }
  
  public MRole roleById(Long id) {
    try {
      MRole mr = (MRole)this.em.createQuery("SELECT mr FROM MRole mr WHERE mr.id = :id and mr.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mr;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MRole> allRole() {
    try {
      return this.em.createQuery("SELECT mr FROM MRole mr where mr.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MRole> roleParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mr FROM MRole mr where mr.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MRole> roleParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mr FROM MRole mr where mr.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MRoleDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */