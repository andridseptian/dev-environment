package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MTransactionMobile")
@Table(name = "m_transaction_mobile")
public class MTransactionMobile {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_transaction_mob_seq")
  @SequenceGenerator(name = "id_transaction_mob_seq", sequenceName = "id_transaction_mob_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "status_transaction", nullable = false)
  private int status_transaction;
  
  @Column(name = "id_form", nullable = false)
  private int id_form;
  
  @Type(type = "text")
  @Column(name = "data_value")
  private String data_value;
  
  @ManyToOne
  @JoinColumn(name = "id_head_trx", nullable = false)
  private MTransaction id_head_trx;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MAgent usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MAgent usr_updated;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public int getStatus_transaction() {
    return this.status_transaction;
  }
  
  public void setStatus_transaction(int status_transaction) {
    this.status_transaction = status_transaction;
  }
  
  public int getId_form() {
    return this.id_form;
  }
  
  public void setId_form(int id_form) {
    this.id_form = id_form;
  }
  
  public String getData_value() {
    return this.data_value;
  }
  
  public void setData_value(String data_value) {
    this.data_value = data_value;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MAgent getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MAgent usr_created) {
    this.usr_created = usr_created;
  }
  
  public MAgent getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MAgent usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public MTransaction getId_head_trx() {
    return this.id_head_trx;
  }
  
  public void setId_head_trx(MTransaction id_head_trx) {
    this.id_head_trx = id_head_trx;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MTransactionMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */