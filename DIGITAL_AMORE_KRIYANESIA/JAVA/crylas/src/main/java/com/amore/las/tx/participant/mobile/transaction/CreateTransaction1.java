package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MImageMob;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.misc.BASE64Decoder;

public class CreateTransaction1 implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  JSONObject data = new JSONObject();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    JSONObject bodyImage = (JSONObject)ctx.get("IMAGE");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss.SSS");
    Long id_trx_web = Long.valueOf(bodyData.getLong("id_trx"));
    JSONArray data = new JSONArray(bodyData.get("data").toString());
    String id_agent = mAgent.getId().toString();
    try {
      int length = data.length();
      MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(id_trx_web);
      for (int a = 0; a < length; a++) {
        JSONObject logData = data.getJSONObject(a);
        JSONArray dt = new JSONArray(logData.get("form").toString());
        int lg = dt.length();
        JSONObject im = null;
        for (int b = 0; b < lg; b++) {
          im = dt.getJSONObject(b);
          if (im.getString("type").equalsIgnoreCase("image_upload"))
            try {
              dt.getJSONObject(b).put("value", "");
            } catch (Exception e) {
              this.log.info("Failed replace image upload");
            }  
        } 
        try {
          JSONArray getImg = new JSONArray(logData.get("form").toString());
          int length2 = getImg.length();
          JSONArray img = null;
          for (int i = 0; i < length2; i++) {
            img = getImg.getJSONArray(i);
            int k = img.length();
            for (int j = 0; j < k; j++) {
              JSONObject jso = img.getJSONObject(i);
              if (jso.getString("type").equalsIgnoreCase("image_upload")) {
                BASE64Decoder decoder = new BASE64Decoder();
                String value = jso.getString("value");
                if (!value.equals("-") || !value.equals("")) {
                  Date date = new Date();
                  String now = sdf.format(date);
                  String file_name = id_agent + "-" + logData.getInt("id_form") + "-" + now + ".jpg";
                  value.replaceAll("\n", "");
                  value.replaceAll("\\\\", "");
                  byte[] imageByte = decoder.decodeBuffer(value);
                  File outputfile = new File("/opt/crylas/webadmin/mobile/img/" + file_name);
                  String url = outputfile.toString().replace("/opt/crylas/webadmin", "");
                  outputfile.createNewFile();
                  FileOutputStream stream = new FileOutputStream(outputfile);
                  stream.write(imageByte);
                  stream.flush();
                  stream.close();
                  MImageMob mImageMob = new MImageMob();
                  mImageMob.setDeleted(Boolean.valueOf(false));
                  mImageMob.setId_element_form(jso.getInt("id"));
                  mImageMob.setLink_url(url);
                  mImageMob.setLatitude(jso.getString("latitude"));
                  mImageMob.setLongitude(jso.getString("longitude"));
                  mImageMob.setUsr_created(mAgent);
                  mImageMob = SpringInitializer.getmImageMobDao().saveOrUpdate(mImageMob);
                } 
              } 
            } 
          } 
        } catch (Exception e) {
          this.rc = SpringInitializer.getmResponseDao().getRc("22");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      StringWriter stringWriter = new StringWriter();
      PrintWriter printWriter = new PrintWriter(stringWriter);
      e.printStackTrace(printWriter);
      this.log.error(stringWriter.toString());
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\CreateTransaction1.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */