package com.amore.las.tx.participant.webadmin.master;

import com.amore.las.entity.MNotaris;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateNotaris implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("nama") && bodyData.has("harga") && bodyData.has("id") && bodyData.has("deleted")) {
        Long id_notaris = Long.valueOf(bodyData.getLong("id"));
        String name = bodyData.getString("nama");
        Long price = Long.valueOf(bodyData.getLong("harga"));
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        MNotaris mNotaris = SpringInitializer.getmNotarisDao().byId(id_notaris);
        if (mNotaris != null) {
          MNotaris mNotaris1 = SpringInitializer.getmNotarisDao().byName(name);
          if (mNotaris1 == null || mNotaris1.getName().equals(mNotaris.getName())) {
            mNotaris.setName(name);
            mNotaris.setPrice(price);
            mNotaris.setUsr_updated(mUserWeb);
            mNotaris.setDeleted(deleted);
            mNotaris = SpringInitializer.getmNotarisDao().saveOrUpdate(mNotaris);
            this.rc = SpringInitializer.getmResponseDao().getRc("00");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } else {
            this.rc = SpringInitializer.getmResponseDao().getRc("16");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } 
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\master\UpdateNotaris.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */