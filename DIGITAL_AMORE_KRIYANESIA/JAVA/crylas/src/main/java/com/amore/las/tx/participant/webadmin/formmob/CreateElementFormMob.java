package com.amore.las.tx.participant.webadmin.formmob;

import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CreateElementFormMob implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("additional") && bodyData.has("input_type") && bodyData.has("label") && bodyData
        .has("length") && bodyData.has("id_form_mobile") && bodyData.has("type") && bodyData
        .has("name") && bodyData.has("position")) {
        String additional = bodyData.getString("additional");
        String input_type = bodyData.getString("input_type");
        String label = bodyData.getString("label");
        int length = bodyData.getInt("length");
        Long id_form = Long.valueOf(bodyData.getLong("id_form_mobile"));
        String type = bodyData.getString("type");
        String name = bodyData.getString("name");
        String position = bodyData.getString("position");
        int pos = 0;
        if (bodyData.getString("position").equals("-") || bodyData.getString("position").equals("")) {
          pos = 0;
        } else {
          pos = Integer.parseInt(position);
        } 
        MElementFormMobile newMElementFormMobile = new MElementFormMobile();
        newMElementFormMobile.setAdditional(additional);
        newMElementFormMobile.setInput_type(input_type);
        newMElementFormMobile.setLabel(label);
        newMElementFormMobile.setLength(length);
        newMElementFormMobile.setId_form_mobile(SpringInitializer.getmFormMobileDao().byId(id_form));
        newMElementFormMobile.setType(type);
        newMElementFormMobile.setName(name);
        newMElementFormMobile.setUsr_created(mUserWeb);
        newMElementFormMobile.setPosition(pos);
        newMElementFormMobile.setDeleted(Boolean.valueOf(false));
        newMElementFormMobile = SpringInitializer.getmElementFormMobileDao().saveOrUpdate(newMElementFormMobile);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\formmob\CreateElementFormMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */