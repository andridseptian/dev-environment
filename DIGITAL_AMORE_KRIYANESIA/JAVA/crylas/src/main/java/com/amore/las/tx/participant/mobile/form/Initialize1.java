package com.amore.las.tx.participant.mobile.form;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MFlowMobile;
import com.amore.las.entity.MFormMobile;
import com.amore.las.entity.MMenuMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class Initialize1 implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    JSONObject logData = new JSONObject();
    try {
      List<MFlowMobile> mFlowMobile = SpringInitializer.getmFlowMobileDao().byAccountType(mAgent.getAccount_tipe());
      if (mFlowMobile.size() > 0) {
        JSONArray data_menu = new JSONArray();
        JSONArray form = new JSONArray();
        for (MFlowMobile mFlowMobile1 : mFlowMobile) {
          JSONObject dataFlow = new JSONObject();
          dataFlow.put("id", mFlowMobile1.getId());
          dataFlow.put("to", mFlowMobile1.getToid());
          dataFlow.put("from", mFlowMobile1.getFromid());
          Integer idref = Integer.valueOf(mFlowMobile1.getId_ref());
          Long id_ref = Long.valueOf(idref.longValue());
          JSONArray listMenu = new JSONArray();
          if (!mFlowMobile1.getForm().booleanValue() && !mFlowMobile1.getForm().booleanValue()) {
            MMenuMobile mMenuMobile = SpringInitializer.getmMenuMobileDao().byId(id_ref);
            if (mMenuMobile != null) {
              dataFlow.put("main", mMenuMobile.getMain());
              dataFlow.put("view_title", mMenuMobile.getView_title());
              dataFlow.put("tipe", mMenuMobile.getTipe());
              MMenuMobile mMenuMobilezz = SpringInitializer.getmMenuMobileDao().byId(mMenuMobile.getId());
              List<MFormMobile> mFormMobile = SpringInitializer.getmFormMobileDao().byIdMenu(mMenuMobilezz);
              if (mFormMobile.size() > 0) {
                for (MFormMobile mFormMobile1 : mFormMobile) {
                  Long idd = mFormMobile1.getId();
                  int iddd = idd.intValue();
                  MFlowMobile getToForm = SpringInitializer.getmFlowMobileDao().byTypeForm(iddd);
                  JSONArray element = new JSONArray();
                  JSONObject dataListMenu = new JSONObject();
                  JSONObject dataForm = new JSONObject();
                  dataListMenu.put("header", mFormMobile1.getHeader());
                  dataListMenu.put("description", mFormMobile1.getDecription());
                  dataListMenu.put("mandatory", mFormMobile1.getMandatory());
                  dataForm.put("id", getToForm.getId());
                  dataForm.put("view_title", mFormMobile1.getHeader());
                  dataForm.put("url", mFormMobile1.getLink_url());
                  dataForm.put("mandatory", mFormMobile1.getMandatory());
                  dataListMenu.put("to", getToForm.getToid().getId());
                  dataListMenu.put("jenis", getToForm.getToid().getForm());
                  dataForm.put("to", getToForm.getToid().getId());
                  dataForm.put("from", getToForm.getFromid());
                  listMenu.put(dataListMenu);
                  dataFlow.put("list_menu", listMenu);
                  form.put(dataForm);
                  List<MElementFormMobile> mElementFormMobile = SpringInitializer.getmElementFormMobileDao().byIdForm(mFormMobile1);
                  for (MElementFormMobile mElementFormMobile1 : mElementFormMobile) {
                    JSONObject data_element = new JSONObject();
                    data_element.put("id", mElementFormMobile1.getId());
                    data_element.put("input_type", mElementFormMobile1.getInput_type());
                    data_element.put("label", mElementFormMobile1.getLabel());
                    data_element.put("length", mElementFormMobile1.getLength());
                    data_element.put("type", mElementFormMobile1.getType());
                    data_element.put("options", mElementFormMobile1.getAdditional());
                    element.put(data_element);
                  } 
                  dataForm.put("element_form", element);
                } 
              } else {
                this.log.info("Data Form Mobile Tidak Ditemukan");
              } 
            } else {
              this.log.info("Data Menu Mobile Tidak Ditemukan");
            } 
          } else {
            MMenuMobile mMenuMobilez = SpringInitializer.getmMenuMobileDao().byId(id_ref);
            List<MFormMobile> mFormMobile = SpringInitializer.getmFormMobileDao().byIdMenu(mMenuMobilez);
            if (mFormMobile.size() > 0) {
              for (MFormMobile mFormMobile1 : mFormMobile) {
                JSONObject dataListMenu = new JSONObject();
                dataListMenu.put("header", mFormMobile1.getHeader());
                dataListMenu.put("description", mFormMobile1.getDecription());
                dataListMenu.put("mandatory", mFormMobile1.getMandatory());
                Long idd = mFormMobile1.getId();
                int iddd = idd.intValue();
                MFlowMobile getToForm = SpringInitializer.getmFlowMobileDao().byTypeForm(iddd);
                dataListMenu.put("to", getToForm.getToid().getId());
                dataListMenu.put("jenis", getToForm.getToid().getForm());
                listMenu.put(dataListMenu);
                dataFlow.put("list_menu", listMenu);
              } 
            } else {
              this.log.info("Data Form Mobile Tidak Ditemukan");
            } 
          } 
          data_menu.put(dataFlow);
          logData.put("menu", data_menu);
          logData.put("form", form);
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
      bodyData.put("data", logData);
      this.rc = SpringInitializer.getmResponseDao().getRc("00");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\form\Initialize1.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */