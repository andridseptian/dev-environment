package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MAgentTaked")
@Table(name = "m_agent_taked")
public class MAgentTaked {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_agent_taked_seq")
  @SequenceGenerator(name = "id_agent_taked_seq", sequenceName = "id_agent_taked_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @ManyToOne
  @JoinColumn(name = "id_head_trx", nullable = false)
  private MTransaction id_head_trx;
  
  @Column(name = "status")
  private int status;
  
  @Column(name = "account_tipe")
  private String account_tipe;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MAgent usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MAgent usr_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_web_updated")
  private MUserWeb usr__web_updated;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Type(type = "text")
  @Column(name = "additional")
  private String additional;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public int getStatus() {
    return this.status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  public String getAccount_tipe() {
    return this.account_tipe;
  }
  
  public void setAccount_tipe(String account_tipe) {
    this.account_tipe = account_tipe;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MAgent getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MAgent usr_created) {
    this.usr_created = usr_created;
  }
  
  public MAgent getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MAgent usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public MTransaction getId_head_trx() {
    return this.id_head_trx;
  }
  
  public void setId_head_trx(MTransaction id_head_trx) {
    this.id_head_trx = id_head_trx;
  }
  
  public String getAdditional() {
    return this.additional;
  }
  
  public void setAdditional(String additional) {
    this.additional = additional;
  }
  
  public MUserWeb getUsr__web_updated() {
    return this.usr__web_updated;
  }
  
  public void setUsr__web_updated(MUserWeb usr__web_updated) {
    this.usr__web_updated = usr__web_updated;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MAgentTaked.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */