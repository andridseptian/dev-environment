package com.amore.las.dao;

import com.amore.las.entity.MResponse;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MReponseDao")
@Transactional
public class MReponseDao extends Dao {
  public MResponse getRc(String rc) {
    try {
      return (MResponse)this.em.createQuery("SELECT mrc FROM MResponse mrc WHERE mrc.rc = :rc")
        .setParameter("rc", rc)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MReponseDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */