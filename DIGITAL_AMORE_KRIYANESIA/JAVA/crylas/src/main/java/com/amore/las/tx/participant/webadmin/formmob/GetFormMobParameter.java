package com.amore.las.tx.participant.webadmin.formmob;

import com.amore.las.entity.MFormMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetFormMobParameter implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      int page = bodyData.getInt("page");
      int pageSize = bodyData.getInt("pageSize");
      int offset = pageSize * page;
      String query = "";
      String orderby = bodyData.getString("orderBy");
      String orderdirection = bodyData.getString("orderDirection");
      JSONArray filter = bodyData.getJSONArray("filters");
      int length = filter.length();
      if (length > 0) {
        for (int i = 0; i < length; i++) {
          JSONObject data = filter.getJSONObject(i);
          query = query + "and me." + data.getString("column") + " " + data.getString("operator") + " '" + data.get("value").toString() + "' ";
        } 
        List<MFormMobile> getCount = SpringInitializer.getmFormMobileDao().formParamCount(query);
        int count = getCount.size();
        query = query + "ORDER BY " + orderby + " " + orderdirection + "";
        List<MFormMobile> mFormMobiles = SpringInitializer.getmFormMobileDao().formParam(query, pageSize, offset);
        if (mFormMobiles != null) {
          JSONArray data = new JSONArray();
          for (MFormMobile mFormMobile : mFormMobiles) {
            JSONObject logData = new JSONObject();
            String pos = null;
            if (mFormMobile.getPosition() == 0) {
              pos = "-";
            } else {
              pos = String.valueOf(mFormMobile.getPosition());
            } 
            logData.put("id", mFormMobile.getId());
            logData.put("description", mFormMobile.getDecription());
            logData.put("header", mFormMobile.getHeader());
            logData.put("name", mFormMobile.getName());
            logData.put("menu", mFormMobile.getId_menu_mobile().getName());
            logData.put("mandatory", mFormMobile.getMandatory());
            logData.put("position", pos);
            data.put(logData);
          } 
          bodyData = new JSONObject();
          bodyData.put("logData", data);
          bodyData.put("totalCount", count);
          bodyData.put("page", page);
          bodyData.put("pageSize", pageSize);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("21");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\formmob\GetFormMobParameter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */