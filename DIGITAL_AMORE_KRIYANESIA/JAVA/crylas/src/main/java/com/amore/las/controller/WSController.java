package com.amore.las.controller;

import com.amore.http.HttpServletEntity;
import com.amore.las.entity.MLogMobileActivity;
import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import com.amore.las.utility.Tripledes;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@CrossOrigin(origins = {"*"})
@RestController
public class WSController {

    Log log = Log.getLog("Q2", getClass().getName());

    private ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();

    private MResponse mapRc = new MResponse();

    Context ctx = new Context();

    @RequestMapping(value = {"/crylas/ws_dev/{service_type}"}, method = {RequestMethod.POST})
    public String allProcess(@RequestPart(name = "images") String images, @RequestPart(name = "body") String body, @PathVariable("service_type") String path, HttpServletRequest request, MultipartHttpServletRequest multipartHttpServletRequest) throws ParseException {
        HttpServletEntity httpServletEntity = new HttpServletEntity(request);
        if (httpServletEntity.getAuthorize().length() == 24) {
            MLogMobileActivity mActivity = new MLogMobileActivity();
            mActivity.setRemoteAddr(httpServletEntity.getRemoteAddr());
            mActivity.setRequestUrl(httpServletEntity.getRequestUrl());
            mActivity.setRouterPath(path);
            mActivity.setRequestBody(body);
            mActivity.setRequestTime(new Date());
            mActivity.setToken(httpServletEntity.getAuthorize());
            mActivity = SpringInitializer.getmLogMobileActivityDao().saveOrUpdate(mActivity);
            Tripledes tDes = new Tripledes(mActivity.getToken());
            this.log.info("Incoming from : " + mActivity.getRemoteAddr());
            this.log.info("Incoming Request Url : " + mActivity.getRequestUrl());
            this.log.info("Incoming Body: " + mActivity.getRequestBody());
            JSONObject reqBody = new JSONObject(mActivity.getRequestBody());
            JSONObject reqImages = new JSONObject(images);
            try {
                Space sp = SpaceFactory.getSpace();
                this.ctx.put("PATH", mActivity.getRouterPath());
                this.ctx.put("REQUEST_BODY", reqBody);
                this.ctx.put("REQUEST", mActivity);
                this.ctx.put("IMAGE", reqImages);
                this.ctx.put("TRXMGR_MOB", "las-mob-trx");
                this.ctx.put("TIME_OUT", Integer.valueOf(60000));
                sp.out("las-mob-trx", this.ctx, 60000L);
                Context response = (Context) sp.in(mActivity.getId(), 60000L);
                this.log.info("Insert request " + mActivity.getId() + " to space[" + "las-mob-trx" + "], timeout trx : " + '');
                this.log.info("Waiting " + mActivity.getId() + " from space");
                this.log.info("Get " + mActivity.getId() + " from space");
                if (response != null) {
                    mActivity.setStatus(response.getString("STATUS"));
                    this.mapRc = (MResponse) response.get("RC");
                    this.wsResponse = (ResponseWebServiceContainer) response.get("RESPONSE");
                } else {
                    mActivity.setStatus("1");
                    this.mapRc = SpringInitializer.getmResponseDao().getRc("50");
                    this.wsResponse = new ResponseWebServiceContainer(this.mapRc.getRc(), this.mapRc.getRm(), reqBody);
                }
            } catch (Exception ex) {
                this.log.error(ExceptionUtils.getStackTrace(ex));
                mActivity.setStatus("1");
                this.mapRc = SpringInitializer.getmResponseDao().getRc("99");
                this.wsResponse = new ResponseWebServiceContainer(this.mapRc.getRc(), this.mapRc.getRm(), reqBody);
            }
            mActivity.setResponseTime(new Date());
            mActivity.setResponse(tDes.encrypt(this.wsResponse.jsonToString()));
            mActivity.setResponseCode(this.mapRc.getRc());
            mActivity.setResponseMessage(this.mapRc.getRm());
            this.log.info("Outgoing to : " + request.getRemoteAddr());
            this.log.info("Outgoing Body: " + this.wsResponse.jsonToString());
            SpringInitializer.getmLogMobileActivityDao().saveOrUpdate(mActivity);
            return this.wsResponse.jsonToString();
        }
        this.mapRc = SpringInitializer.getmResponseDao().getRc("98");
        this.wsResponse = new ResponseWebServiceContainer(this.mapRc.getRc(), this.mapRc.getRm());
        return this.wsResponse.jsonToString();
    }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\controller\WSController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */
