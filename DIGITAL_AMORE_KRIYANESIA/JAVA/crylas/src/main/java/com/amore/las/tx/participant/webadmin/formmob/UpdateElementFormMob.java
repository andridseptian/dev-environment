package com.amore.las.tx.participant.webadmin.formmob;

import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateElementFormMob implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("id") && bodyData.has("additional") && bodyData.has("input_type") && bodyData
        .has("label") && bodyData.has("length") && bodyData.has("id_form") && bodyData
        .has("type") && bodyData.has("deleted") && bodyData.has("name") && bodyData
        .has("position")) {
        Long id_el = Long.valueOf(bodyData.getLong("id"));
        String additional = bodyData.getString("additional");
        String input_type = bodyData.getString("input_type");
        String label = bodyData.getString("label");
        int length = bodyData.getInt("length");
        Long id_form = Long.valueOf(bodyData.getLong("id_form"));
        String type = bodyData.getString("type");
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        String name = bodyData.getString("name");
        String position = bodyData.getString("position");
        int pos = 0;
        if (bodyData.getString("position").equals("-") || bodyData.getString("position").equals("")) {
          pos = 0;
        } else {
          pos = Integer.parseInt(position);
        } 
        MElementFormMobile mElementFormMobilez = SpringInitializer.getmElementFormMobileDao().byId(id_el);
        if (mElementFormMobilez != null) {
          mElementFormMobilez.setAdditional(additional);
          mElementFormMobilez.setInput_type(input_type);
          mElementFormMobilez.setLabel(label);
          mElementFormMobilez.setLength(length);
          mElementFormMobilez.setId_form_mobile(SpringInitializer.getmFormMobileDao().byId(id_form));
          mElementFormMobilez.setType(type);
          mElementFormMobilez.setUsr_updated(mUserWeb);
          mElementFormMobilez.setName(name);
          mElementFormMobilez.setDeleted(deleted);
          mElementFormMobilez.setPosition(pos);
          mElementFormMobilez = SpringInitializer.getmElementFormMobileDao().saveOrUpdate(mElementFormMobilez);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\formmob\UpdateElementFormMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */