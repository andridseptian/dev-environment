package com.amore.las.tx.participant.webadmin.transaction;

import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public class UploadFile implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    String id_form = (String)ctx.get("ID_FORM");
    String extension = (String)ctx.get("EXTENSION");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    MultipartFile[] file_images = (MultipartFile[])ctx.get("IMAGE");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss.SSS");
    try {
      StringBuilder sb = new StringBuilder();
      if (file_images.length > 0) {
        int i = 1;
        String file_name = "";
        String getfile = "";
        for (MultipartFile file : file_images) {
          Date date = new Date();
          String now = sdf.format(date);
          file_name = mUserWeb.getId() + "-" + id_form + "-" + now + "." + extension;
          File convFile = null;
          if (extension.equals("xls") || extension.equals("xlsx")) {
            convFile = new File("/opt/crylas/webadmin/xls/" + file_name);
          } else if (extension.equals("doc") || extension.equals("docx")) {
            convFile = new File("/opt/crylas/webadmin/doc/" + file_name);
          } else if (extension.equals("pdf")) {
            convFile = new File("/opt/crylas/webadmin/pdf/" + file_name);
          } else if (extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png")) {
            convFile = new File("/opt/crylas/webadmin/img/" + file_name);
          } else {
            convFile = new File("/opt/crylas/webadmin/" + extension);
            Boolean result = Boolean.valueOf(false);
            if (!convFile.exists()) {
              try {
                convFile.mkdir();
                result = Boolean.valueOf(true);
              } catch (Exception e) {
                this.log.info("Failed Create New Directory");
              } 
              if (result.booleanValue()) {
                this.log.info("Directory " + convFile.toString() + " Has Been Created.");
                convFile = new File("/opt/crylas/webadmin/" + extension + "/" + file_name);
              } 
            } else {
              convFile = new File("/opt/crylas/webadmin/" + extension + "/" + file_name);
            } 
          } 
          getfile = convFile.toString();
          convFile.createNewFile();
          FileOutputStream stream = new FileOutputStream(convFile);
          stream.write(file.getBytes());
          stream.flush();
          stream.close();
          sb.append(file_name);
          if (i < file_images.length)
            sb.append(";"); 
          i++;
        } 
        bodyData.put("id_form", id_form);
        bodyData.put("file_location", "172.16.100.13" + getfile);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\transaction\UploadFile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */