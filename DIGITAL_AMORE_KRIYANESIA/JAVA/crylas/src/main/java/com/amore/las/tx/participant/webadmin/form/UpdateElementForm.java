package com.amore.las.tx.participant.webadmin.form;

import com.amore.las.entity.MElementForm;
import com.amore.las.entity.MForm;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateElementForm implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("id") && bodyData.has("id_form") && bodyData.has("name") && bodyData.has("label") && bodyData
        .has("type") && bodyData.has("value") && bodyData.has("additional") && bodyData
        .has("mandatory") && bodyData.has("master") && bodyData.has("deleted")) {
        Long id_element_form = Long.valueOf(bodyData.getLong("id"));
        Long id_form = Long.valueOf(bodyData.getLong("id_form"));
        String name = bodyData.getString("name");
        String label = bodyData.getString("label");
        String type = bodyData.getString("type");
        String value = bodyData.getString("value");
        String additional = bodyData.getString("additional");
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        Boolean mandatory = Boolean.valueOf(bodyData.getBoolean("mandatory"));
        String master = bodyData.getString("master");
        MElementForm mElementForm = SpringInitializer.getmElementFormDao().elementById(id_element_form);
        if (mElementForm != null) {
          MForm mForm = SpringInitializer.getmFormDao().formById(id_form);
          if (mForm != null) {
            mElementForm.setId(id_element_form);
            mElementForm.setId_form(mForm);
            mElementForm.setName(name);
            mElementForm.setLabel(label);
            mElementForm.setType(type);
            mElementForm.setValue(value);
            mElementForm.setAdditional(additional);
            mElementForm.setUsr_updated(mUserWeb);
            mElementForm.setMaster(master);
            mElementForm.setDeleted(deleted);
            mElementForm.setMandatory(mandatory);
            mElementForm = SpringInitializer.getmElementFormDao().saveOrUpdate(mElementForm);
            this.rc = SpringInitializer.getmResponseDao().getRc("00");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } else {
            this.rc = SpringInitializer.getmResponseDao().getRc("20");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } 
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\form\UpdateElementForm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */