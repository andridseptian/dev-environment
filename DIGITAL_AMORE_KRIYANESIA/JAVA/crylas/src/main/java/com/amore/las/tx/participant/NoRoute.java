package com.amore.las.tx.participant;

import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class NoRoute implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject reqData = (JSONObject)ctx.get("REQUEST_BODY");
    MResponse rc = SpringInitializer.getmResponseDao().getRc("99");
    ResponseWebServiceContainer responseWebServiceContainer = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), reqData);
    ctx.put("RESPONSE", responseWebServiceContainer);
    ctx.put("RC", rc);
    ctx.put("STATUS", "1");
  }
  
  public void abort(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject reqData = (JSONObject)ctx.get("REQUEST_BODY");
    MResponse rc = SpringInitializer.getmResponseDao().getRc("99");
    ResponseWebServiceContainer responseWebServiceContainer = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), reqData);
    ctx.put("RESPONSE", responseWebServiceContainer);
    ctx.put("RC", rc);
    ctx.put("STATUS", "1");
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\NoRoute.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */