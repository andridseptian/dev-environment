package com.amore.las.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity(name = "MTransaction")
@Table(name = "m_transaction")
public class MTransaction {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_transaction_seq")
  @SequenceGenerator(name = "id_transaction_seq", sequenceName = "id_transaction_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "status_transaction", nullable = false)
  private int status_transaction;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "id_head", cascade = {CascadeType.ALL})
  private List<MTransactionDetail> transaction_detail;
  
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "id_head_trx", cascade = {CascadeType.ALL})
  private List<MAgentTaked> agent_taked;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public int getStatus_transaction() {
    return this.status_transaction;
  }
  
  public void setStatus_transaction(int status_transaction) {
    this.status_transaction = status_transaction;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public List<MTransactionDetail> getTransaction_detail() {
    return this.transaction_detail;
  }
  
  public void setTransaction_detail(List<MTransactionDetail> transaction_detail) {
    this.transaction_detail = transaction_detail;
  }
  
  public List<MAgentTaked> getAgent_taked() {
    return this.agent_taked;
  }
  
  public void setAgent_taked(List<MAgentTaked> agent_taked) {
    this.agent_taked = agent_taked;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MTransaction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */