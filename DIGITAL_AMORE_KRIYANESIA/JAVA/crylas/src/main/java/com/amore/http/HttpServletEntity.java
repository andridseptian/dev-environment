package com.amore.http;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

public class HttpServletEntity {
  private final String requestUrl;
  
  private final String headers;
  
  private final String remoteAddr;
  
  private final String remoteHost;
  
  private final String authorize;
  
  private final String uri;
  
  public HttpServletEntity(HttpServletRequest httpReq) {
    this.requestUrl = httpReq.getRequestURL().toString();
    this.remoteAddr = httpReq.getRemoteAddr();
    this.remoteHost = httpReq.getRemoteHost();
    this.headers = getHeadersInfo(httpReq).toString();
    this.authorize = httpReq.getHeader("x-api-key");
    this.uri = httpReq.getRequestURI();
  }
  
  public String getRequestUrl() {
    return this.requestUrl;
  }
  
  public String getRemoteHost() {
    return this.remoteHost;
  }
  
  public String getHeaders() {
    return this.headers;
  }
  
  public String getRemoteAddr() {
    return this.remoteAddr;
  }
  
  public String getAuthorize() {
    return this.authorize;
  }
  
  public String getUri() {
    return this.uri;
  }
  
  private Map<String, String> getHeadersInfo(HttpServletRequest request) {
    Map<String, String> map = new HashMap<>();
    Enumeration<String> headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String key = headerNames.nextElement();
      String value = request.getHeader(key);
      map.put(key, value);
    } 
    return map;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\http\HttpServletEntity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */