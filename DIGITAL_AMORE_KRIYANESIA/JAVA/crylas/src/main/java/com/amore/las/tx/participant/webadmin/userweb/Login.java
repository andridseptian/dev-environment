package com.amore.las.tx.participant.webadmin.userweb;

import com.amore.las.entity.MGroup;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class Login implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    try {
      String password = bodyData.getString("password");
      MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
      MGroup mGroup = mUserWeb.getId_group();
      JSONObject data = new JSONObject();
      if (password.equals(mUserWeb.getPassword())) {
        data.put("name", mUserWeb.getName());
        data.put("group", mGroup.getName());
        data.put("status_user", "ACTIVE");
        mUserWeb.setLast_login(new Date());
        mUserWeb.setStatus_login(1);
        SpringInitializer.getmUserWebDao().saveOrUpdate(mUserWeb);
        data.put("status_login", mUserWeb.getStatus_login());
        bodyData.put("data", data);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("12");
        data.put("name", mUserWeb.getName());
        data.put("group", mGroup.getName());
        data.put("status_user", "ACTIVE");
        bodyData.put("data", data);
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      } 
    } catch (Exception e) {
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
    } 
    ctx.put("RESPONSE", this.wsResponse);
    ctx.put("RC", this.rc);
    ctx.put("STATUS", "0");
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmi\\userweb\Login.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */