package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MAgentTaked;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CancelAgentTaked implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      Long id_survey = Long.valueOf(bodyData.getLong("id_survey"));
      MAgentTaked mAgentTaked = SpringInitializer.getmAgentTakedDao().byIdTrxAndAccountTipe(SpringInitializer.getmTransactionDao().transactionById(id_survey), mAgent.getAccount_tipe());
      if (mAgentTaked != null) {
        mAgentTaked.setDeleted(Boolean.valueOf(true));
        SpringInitializer.getmAgentTakedDao().saveOrUpdate(mAgentTaked);
        List<MTransactionDetail> lstDetail = mAgentTaked.getId_head_trx().getTransaction_detail();
        int length = lstDetail.size();
        if (length > 0) {
          int index = 0;
          for (int i = 0; i < length; i++) {
            index++;
            if (((MTransactionDetail)lstDetail.get(i)).getSurveyor().equals(mAgent.getId().toString()))
              break; 
          } 
          MTransactionDetail mTransactionDetail = mAgentTaked.getId_head_trx().getTransaction_detail().get(index - 1);
          mTransactionDetail.setStatus_survey("-");
          mTransactionDetail.setSurveyor("all");
          SpringInitializer.getmTransactionDao().saveOrUpdateDetail(mTransactionDetail);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\CancelAgentTaked.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */