package com.amore.las.tx.participant.webadmin.formmob;

import com.amore.las.entity.MFlowMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CreateFlowMob implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("account_type") && bodyData.has("description") && bodyData.has("form") && bodyData
        .has("fromid") && bodyData.has("id_ref")) {
        String account_type = bodyData.getString("account_type");
        String desc = bodyData.getString("description");
        Boolean form = Boolean.valueOf(bodyData.getBoolean("form"));
        int fromid = bodyData.getInt("fromid");
        int id_ref = bodyData.getInt("id_ref");
        MFlowMobile newmFlowMobile = new MFlowMobile();
        newmFlowMobile.setAccount_type(account_type);
        newmFlowMobile.setDecription(desc);
        newmFlowMobile.setForm(form);
        newmFlowMobile.setFromid(fromid);
        newmFlowMobile.setId_ref(id_ref);
        newmFlowMobile.setUsr_created(mUserWeb);
        newmFlowMobile.setDeleted(Boolean.valueOf(false));
        newmFlowMobile = SpringInitializer.getmFlowMobileDao().saveOrUpdate(newmFlowMobile);
        if (newmFlowMobile.getForm().equals(Boolean.valueOf(true))) {
          newmFlowMobile.setToid(newmFlowMobile);
          SpringInitializer.getmFlowMobileDao().saveOrUpdate(newmFlowMobile);
        } else {
          newmFlowMobile.setToid(null);
          SpringInitializer.getmFlowMobileDao().saveOrUpdate(newmFlowMobile);
        } 
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\formmob\CreateFlowMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */