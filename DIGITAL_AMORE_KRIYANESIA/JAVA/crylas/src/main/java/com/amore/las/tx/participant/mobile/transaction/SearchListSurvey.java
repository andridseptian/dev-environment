package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class SearchListSurvey implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      List<Object[]> list_trx = SpringInitializer.getmTransactionDao().SearchListTrxByDepartemen(bodyData.getString("param"));
      if (list_trx.size() > 0) {
        JSONArray data = new JSONArray();
        int sz = list_trx.size();
        for (int i = 0; i < sz; i++) {
          Long idx = Long.valueOf(Long.parseLong((String)((Object[])list_trx.get(i))[0]));
          MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(idx);
          MTransactionDetail mTransactionDetail = SpringInitializer.getmTransactionDao().getTransactionDetailFirst(mTransaction);
          JSONObject logData = new JSONObject();
          logData.put("id", mTransaction.getId());
          JSONArray data_trx = new JSONArray(mTransactionDetail.getData_value().toString());
          int length = data_trx.length();
          String cabang = null, nama = null, flapon = null, produk = null;
          String alamat = null;
          String rtrw = null;
          String kelurahan = null;
          String kecamatan = null;
          String kota = null;
          String provinsi = null;
          String kodepos = null;
          for (int j = 0; j < length; j++) {
            JSONObject jso = data_trx.getJSONObject(j);
            if (jso.getString("view_title").equals("Cabang")) {
              cabang = jso.getString("value");
            } else if (jso.getString("view_title").equals("Nama Lengkap")) {
              nama = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kredit Mohon")) {
              flapon = jso.getString("value");
            } else if (jso.getString("view_title").equals("Jenis Kredit")) {
              produk = jso.getString("value");
            } else if (jso.getString("view_title").equals("Alamat Sekarang")) {
              alamat = jso.getString("value");
            } else if (jso.getString("view_title").equals("RT / RW Sekarang")) {
              rtrw = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kelurahan Sekarang")) {
              kelurahan = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kecamatan Sekarang")) {
              kecamatan = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kota Sekarang")) {
              kota = jso.getString("value");
            } else if (jso.getString("view_title").equals("Provinsi Sekarang")) {
              provinsi = jso.getString("value");
            } else if (jso.getString("view_title").equals("Kode Pos Sekarang")) {
              kodepos = jso.getString("value");
            } 
          } 
          logData.put("cabang", cabang);
          logData.put("nama", nama);
          logData.put("flapon", flapon);
          logData.put("produk", produk);
          String alamat_lengkap = alamat + " RT/RW. " + rtrw + " Kelurahan " + kelurahan + " Kecamatan " + kecamatan + " Kota " + kota + " " + provinsi + " " + kodepos;
          logData.put("alamat", alamat_lengkap);
          data.put(logData);
        } 
        bodyData.put("data", data);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\SearchListSurvey.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */