package com.amore.las.dao;

import com.amore.las.entity.MCabang;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MCabangDao")
@Transactional
public class MCabangDao extends Dao {
  public MCabang saveOrUpdate(MCabang mCabang) {
    Date date = new Date();
    if (mCabang.getId() == null) {
      mCabang.setDtm_created(date);
      this.em.persist(mCabang);
    } else {
      mCabang.setDtm_updated(date);
      this.em.merge(mCabang);
    } 
    return mCabang;
  }
  
  public MCabang byId(Long id) {
    try {
      MCabang ma = (MCabang)this.em.createQuery("SELECT ma FROM MCabang ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MCabang byName(String name) {
    try {
      MCabang ma = (MCabang)this.em.createQuery("SELECT ma FROM MCabang ma WHERE ma.name = :name and ma.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MCabang> getAllCabang() {
    try {
      return this.em.createQuery("SELECT ma FROM MCabang ma where ma.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MCabang> cabangParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT ma FROM MCabang ma where ma.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MCabang> cabangParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT ma FROM MCabang ma where ma.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MCabangDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */