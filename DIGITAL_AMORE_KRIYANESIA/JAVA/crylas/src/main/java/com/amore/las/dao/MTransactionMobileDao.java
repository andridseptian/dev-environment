package com.amore.las.dao;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionMobile;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MTransactionMobileDao")
@Transactional
public class MTransactionMobileDao extends Dao {
  public MTransactionMobile saveOrUpdate(MTransactionMobile mTransactionMobile) {
    Date date = new Date();
    if (mTransactionMobile.getId() == null) {
      mTransactionMobile.setDtm_created(date);
      this.em.persist(mTransactionMobile);
    } else {
      mTransactionMobile.setDtm_updated(date);
      this.em.merge(mTransactionMobile);
    } 
    return mTransactionMobile;
  }
  
  public List<MTransactionMobile> allTrxMobByIdWeb(MTransaction id) {
    try {
      return this.em.createQuery("SELECT ma FROM MTransactionMobile ma WHERE ma.id_head_trx = :id and ma.deleted = false")
        .setParameter("id", id)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MTransactionMobile byId(Long id) {
    try {
      MTransactionMobile ma = (MTransactionMobile)this.em.createQuery("SELECT ma FROM MTransactionMobile ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MTransactionMobile transactionByForm(MAgent mAgent, int id_form, MTransaction mTransaction) {
    try {
      MTransactionMobile mtm = (MTransactionMobile)this.em.createQuery("SELECT mt FROM MTransactionMobile mt where mt.deleted = false and mt.id_form = :id_form and mt.usr_created = :agent and mt.id_head_trx = :trx_web").setParameter("agent", mAgent).setParameter("trx_web", mTransaction).setParameter("id_form", Integer.valueOf(id_form)).getSingleResult();
      return mtm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MTransactionMobileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */