package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MForm;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetDetailSurvey implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(Long.valueOf(bodyData.getLong("id")));
      if (mTransaction != null) {
        List<MTransactionDetail> list_trx = SpringInitializer.getmTransactionDao().transactionDetailByIdHead(mTransaction);
        if (list_trx.size() > 0) {
          List<Long> idf = new ArrayList();
          for (MTransactionDetail mTransactionDetail : list_trx) {
            JSONArray jarr = new JSONArray(mTransactionDetail.getData_value());
            int length = jarr.length();
            for (int i = 0; i < length; i++) {
              JSONObject jso = jarr.getJSONObject(i);
              idf.add(Long.valueOf(jso.getLong("id_form")));
            } 
          } 
          List list2 = new ArrayList(new HashSet(idf));
          if (list2.size() > 0) {
            JSONArray jssjsjs = new JSONArray();
            for (Object lg : list2) {
              JSONObject logData = new JSONObject();
              MForm mForm = SpringInitializer.getmFormDao().formById(Long.valueOf(lg.toString()));
              for (MTransactionDetail mTransactionDetail : list_trx) {
                JSONArray logData1 = new JSONArray();
                JSONArray jarr = new JSONArray(mTransactionDetail.getData_value());
                int length = jarr.length();
                for (int j = 0; j < length; j++) {
                  JSONObject jso = jarr.getJSONObject(j);
                  JSONObject logData2 = new JSONObject();
                  if (jso.getLong("id_form") == mForm.getId().longValue()) {
                    logData2.put("view_title", jso.getString("view_title"));
                    logData2.put("value", jso.getString("value"));
                    logData1.put(logData2);
                    logData.put("detail", logData1);
                  } 
                } 
                logData.put("form", mForm.getName());
              } 
              jssjsjs.put(logData);
            } 
            bodyData.put("data", jssjsjs);
          } else {
            this.rc = SpringInitializer.getmResponseDao().getRc("20");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } 
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\GetDetailSurvey.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */