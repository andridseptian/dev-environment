package com.amore.las.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "MSettingAplikasi")
@Table(name = "m_setting_aplikasi")
public class MSettingAplikasi {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_setting_aplikasi_seq")
  @SequenceGenerator(name = "id_setting_aplikasi_seq", sequenceName = "id_setting_aplikasi_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "url", nullable = false)
  private String url;
  
  @Column(name = "description", nullable = false)
  private String description;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getUrl() {
    return this.url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MSettingAplikasi.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */