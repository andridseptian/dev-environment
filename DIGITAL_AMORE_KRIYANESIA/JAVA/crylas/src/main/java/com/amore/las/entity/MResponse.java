package com.amore.las.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "MResponse")
@Table(name = "m_rc")
public class MResponse {
  @Id
  @Column(name = "response_code", nullable = false)
  private String rc;
  
  @Column(name = "response_message", nullable = false)
  private String rm;
  
  public String getRc() {
    return this.rc;
  }
  
  public void setRc(String rc) {
    this.rc = rc;
  }
  
  public String getRm() {
    return this.rm;
  }
  
  public void setRm(String rm) {
    this.rm = rm;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */