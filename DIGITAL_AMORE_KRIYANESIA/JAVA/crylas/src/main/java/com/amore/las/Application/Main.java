package com.amore.las.Application;

import com.amore.las.spring.SpringInitializer;
import javax.naming.ConfigurationException;
import org.jpos.q2.Q2;
import org.jpos.util.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.amore.las.controller"})
@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class})
public class Main {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
    try {
      Q2 q2 = new Q2("deploy");
      q2.start();
      SpringInitializer springInitializer = new SpringInitializer();
      springInitializer.initService();
    } catch (ConfigurationException e) {
      e.printStackTrace();
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\Application\Main.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */