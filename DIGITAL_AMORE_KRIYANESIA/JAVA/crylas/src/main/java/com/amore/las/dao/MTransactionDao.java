package com.amore.las.dao;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.jpos.util.Log;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MTransactionDao")
@Transactional
public class MTransactionDao extends Dao {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public MTransaction saveOrUpdate(MTransaction mTransaction) {
    Date date = new Date();
    if (mTransaction.getId() == null) {
      mTransaction.setDtm_created(date);
      this.em.persist(mTransaction);
    } else {
      mTransaction.setDtm_updated(date);
      this.em.merge(mTransaction);
    } 
    return mTransaction;
  }
  
  public void remove(Long id) {
    MTransaction mPermissions = (MTransaction)this.em.find(MTransaction.class, id);
    this.em.remove(mPermissions);
  }
  
  public MTransaction transactionById(Long id) {
    try {
      MTransaction mt = (MTransaction)this.em.createQuery("SELECT mt FROM MTransaction mt WHERE mt.id = :id and mt.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mt;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransaction> mySurveyTransactionWebMob(String departemen, String surveyor) {
    try {
      return this.em.createQuery("SELECT mt FROM MTransaction mt INNER JOIN mt.transaction_detail md where mt.deleted = false and md.surveyor = :surveyor and md.departemen = :departemen and mt.id NOT IN (SELECT ma.id_head_trx from MAgentTaked ma WHERE ma.account_tipe = :departemen and ma.deleted = false) GROUP BY mt.id")
        .setParameter("surveyor", surveyor)
        .setParameter("departemen", departemen)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransaction> onGoingTransactionWebMob(MAgent agent) {
    try {
      return this.em.createQuery("SELECT mt FROM MTransaction mt INNER JOIN mt.agent_taked ma where mt.deleted = false and ma.deleted = false and ma.usr_created = :agent and ma.status IN (1, 3)")
        .setParameter("agent", agent)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransaction> allTransaction() {
    try {
      return this.em.createQuery("SELECT mt FROM MTransaction mt where mt.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransaction> transactionParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mt FROM MTransaction mt where mt.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransaction> transactionParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mt FROM MTransaction mt where mt.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Object[]> ListTrxByDepartemen(String departemen) {
    try {
      return this.em.createNativeQuery("SELECT mt.id as id, md.id as idd, md.surveyor, md.departemen FROM m_transaction mt inner join m_transaction_detail md on mt.id = md.id_head where lower(md.departemen) = lower(:departemen) and surveyor = 'all'")
        .setParameter("departemen", departemen)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Object[]> SearchListTrxByDepartemen(String param) {
    try {
      return this.em.createNativeQuery("SELECT mt.id FROM m_transaction mt inner join m_transaction_detail md on mt.id = md.id_head where lower(md.departemen) = :departemen and surveyor = 'all' and md.data_value ILIKE '%" + param + "%'")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Object[]> allTransactionNotInSrc(String param, String account_tipe) {
    try {
      return this.em.createNativeQuery("SELECT mt.id, mt.data_value FROM m_transaction mt where mt.id not in (select ma.id_trx_web from m_agent_taked ma where ma.account_tipe = :account_tipe) and mt.data_value ILIKE '%" + param + "%'")
        .setParameter("account_tipe", account_tipe)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MTransactionDetail saveOrUpdateDetail(MTransactionDetail mTransactionDetail) {
    Date date = new Date();
    if (mTransactionDetail.getId() == null) {
      mTransactionDetail.setDtm_created(date);
      this.em.persist(mTransactionDetail);
    } else {
      mTransactionDetail.setDtm_updated(date);
      this.em.merge(mTransactionDetail);
    } 
    return mTransactionDetail;
  }
  
  public List<Object[]> trxDetailByIdHead(MTransaction mTransaction) {
    try {
      return this.em.createNativeQuery("SELECT mt.id, mt.status_transaction, md.data_value FROM m_transaction mt inner join m_transaction_detail md on mt.id = md.id where md.position = 'fpk' ")
        .setParameter("id_fpk", mTransaction)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransactionDetail> transactionDetailByIdHead(MTransaction mTransaction) {
    try {
      return this.em.createQuery("SELECT mt FROM MTransactionDetail mt where mt.id_head = :mTransaction order by mt.id asc")
        .setParameter("mTransaction", mTransaction)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MTransactionDetail transactionDetailById(Long id) {
    try {
      MTransactionDetail mt = (MTransactionDetail)this.em.createQuery("SELECT mt FROM MTransactionDetail mt WHERE mt.id = :id and mt.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mt;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MTransactionDetail getTransactionDetailFirst(MTransaction mTransaction) {
    try {
      return (MTransactionDetail)this.em.createQuery("SELECT mt FROM MTransactionDetail mt where mt.deleted = false and mt.dtm_created = (SELECT MIN(t.dtm_created) from MTransactionDetail t where t.id_head = :mTransaction)")
        .setParameter("mTransaction", mTransaction)
        .setMaxResults(1)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MTransactionDetail getTransactionDetailFirstDept(Long iddx) {
    try {
      return (MTransactionDetail)this.em.createQuery("SELECT mt FROM MTransactionDetail mt where mt.deleted = false and mt.dtm_created = (SELECT MIN(t.dtm_created) from MTransactionDetail t where t.id = :iddx)")
        .setParameter("iddx", iddx)
        .setMaxResults(1)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public int getTransactionDetailSurvey(MTransactionDetail mTransaction, String surveyor) {
    try {
      return this.em.createQuery("UPDATE MTransactionDetail md SET md.surveyor = :surveyor WHERE md.id = :mTransaction")
        .setParameter("mTransaction", mTransaction)
        .setParameter("surveyor", surveyor)
        .executeUpdate();
    } catch (Exception nre) {
      return 0;
    } 
  }
  
  public List<MTransactionDetail> getTransactionDetail(MTransaction mTransaction) {
    try {
      return this.em.createQuery("SELECT mt FROM MTransactionDetail mt where mt.deleted = false and mt.id_head = :mTransaction")
        .setParameter("mTransaction", mTransaction)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MTransactionDetail> getAllTransactionDetail() {
    try {
      return this.em.createQuery("SELECT mt FROM MTransactionDetail mt where mt.deleted = false order by mt.id asc")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Object[]> getTransactionDetailParam(String query, int limit, int offset) {
    try {
      return this.em.createNativeQuery("SELECT m.id_head, m.id FROM m_transaction_detail m INNER JOIN (SELECT mtd.id_head as hd, MAX(mtd.dtm_created) AS dtt FROM m_transaction_detail mtd GROUP BY hd) dt ON m.id_head = dt.hd AND m.dtm_created = dt.dtt where " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Object[]> getTransactionDetaiParameterCount(String query) {
    try {
      return this.em.createNativeQuery("SELECT m.id_head, m.id FROM m_transaction_detail m INNER JOIN (SELECT mtd.id_head as hd, MAX(mtd.dtm_created) AS dtt FROM m_transaction_detail mtd GROUP BY hd) dt ON m.id_head = dt.hd AND m.dtm_created = dt.dtt where " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MTransactionDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */