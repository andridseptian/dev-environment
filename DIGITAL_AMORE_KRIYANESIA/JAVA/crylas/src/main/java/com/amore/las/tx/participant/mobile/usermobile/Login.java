package com.amore.las.tx.participant.mobile.usermobile;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class Login implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    try {
      String pin = bodyData.getString("pin");
      String fcm_token = bodyData.getString("token");
      String imei = bodyData.getString("imei");
      MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
      JSONObject data = new JSONObject();
      data.put("name", mAgent.getName());
      data.put("account_type", mAgent.getAccount_tipe());
      data.put("status_reset_pin", mAgent.getReset_pin());
      data.put("status_reset_msisdn", mAgent.getReset_msisdn());
      data.put("status_approval", mAgent.getStatus_approval());
      data.put("status_pin", mAgent.getStatus_pin());
      data.put("fail_login", mAgent.getFail_login());
      if (mAgent.getStatus_pin().booleanValue()) {
        if (pin.equals(mAgent.getPin())) {
          data.put("status_user", "ACTIVE");
          bodyData.put("data", data);
          mAgent.setFail_login(0);
          mAgent.setLast_login(new Date());
          mAgent.setFcm_token(fcm_token);
          mAgent.setImei(imei);
          SpringInitializer.getmAgentDao().saveOrUpdate(mAgent);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("12");
          data.put("status_user", "ACTIVE");
          bodyData.put("data", data);
          if (mAgent.getFail_login() <= 3) {
            mAgent.setFail_login(mAgent.getFail_login() + 1);
            SpringInitializer.getmAgentDao().saveOrUpdate(mAgent);
          } else {
            mAgent.setStatus_pin(Boolean.valueOf(false));
            SpringInitializer.getmAgentDao().saveOrUpdate(mAgent);
          } 
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        } 
      } else {
        data.put("status_user", "ACTIVE");
        bodyData.put("data", data);
        this.rc = SpringInitializer.getmResponseDao().getRc("17");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      } 
    } catch (Exception e) {
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
    } 
    ctx.put("RESPONSE", this.wsResponse);
    ctx.put("RC", this.rc);
    ctx.put("STATUS", "0");
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobil\\usermobile\Login.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */