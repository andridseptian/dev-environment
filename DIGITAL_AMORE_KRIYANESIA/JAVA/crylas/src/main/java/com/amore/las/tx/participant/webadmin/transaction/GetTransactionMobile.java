package com.amore.las.tx.participant.webadmin.transaction;

import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionMobile;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetTransactionMobile implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      Long idx = Long.valueOf(bodyData.getLong("id"));
      MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(idx);
      List<MTransactionMobile> mTransactionMobiles = SpringInitializer.getmTransactionMobileDao().allTrxMobByIdWeb(mTransaction);
      int sz = mTransactionMobiles.size();
      int dtval = 0;
      if (sz > 0) {
        JSONArray dt = new JSONArray();
        for (MTransactionMobile mTransactionMobile : mTransactionMobiles) {
          JSONArray jarr = new JSONArray(mTransactionMobile.getData_value());
          dtval = jarr.length();
          for (int i = 0; i < dtval; i++) {
            JSONObject data = jarr.getJSONObject(i);
            JSONObject jso = new JSONObject();
            MElementFormMobile mElementFormMobile = SpringInitializer.getmElementFormMobileDao().byId(Long.valueOf(data.getLong("id")));
            data.put("name", mElementFormMobile.getName());
            if (data.has("value")) {
              jso.put("name", data.getString("name"));
              jso.put("value", data.getString("value"));
            } else {
              jso.put("name", data.getString("name"));
              jso.put("value", "");
            } 
            dt.put(jso);
          } 
        } 
        bodyData.put("data", dt);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\transaction\GetTransactionMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */