package com.amore.las.dao;

import com.amore.las.entity.MImageMob;
import com.amore.las.entity.MTransactionMobile;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MImageMobDao")
@Transactional
public class MImageMobDao extends Dao {
  Date date = new Date();
  
  public MImageMob saveOrUpdate(MImageMob mImageMob) {
    if (mImageMob.getId() == null) {
      mImageMob.setDtm_created(this.date);
      this.em.persist(mImageMob);
    } else {
      mImageMob.setDtm_updated(this.date);
      this.em.merge(mImageMob);
    } 
    return mImageMob;
  }
  
  public MImageMob byId(Long id) {
    try {
      MImageMob ma = (MImageMob)this.em.createQuery("SELECT ma FROM MImageMob ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MImageMob> byIdTrxMob(MTransactionMobile id_trxmob) {
    try {
      return this.em.createQuery("SELECT ma FROM MImageMob ma WHERE ma.id_transaction_mob = :id_transaction_mob and ma.deleted = false")
        .setParameter("id_transaction_mob", id_trxmob)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MImageMob remove(MImageMob mImageMob) {
    this.em.remove(this.em.merge(mImageMob));
    return mImageMob;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MImageMobDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */