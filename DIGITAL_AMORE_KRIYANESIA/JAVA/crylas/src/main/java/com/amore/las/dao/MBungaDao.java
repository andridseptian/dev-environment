package com.amore.las.dao;

import com.amore.las.entity.MBunga;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MBungaDao")
@Transactional
public class MBungaDao extends Dao {
  public MBunga saveOrUpdate(MBunga mBunga) {
    Date date = new Date();
    if (mBunga.getId() == null) {
      mBunga.setDtm_created(date);
      this.em.persist(mBunga);
    } else {
      mBunga.setDtm_updated(date);
      this.em.merge(mBunga);
    } 
    return mBunga;
  }
  
  public MBunga byId(Long id) {
    try {
      MBunga ma = (MBunga)this.em.createQuery("SELECT ma FROM MBunga ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MBunga byValue(int value) {
    try {
      MBunga ma = (MBunga)this.em.createQuery("SELECT ma FROM MBunga ma WHERE ma.value = :value and ma.deleted = false").setParameter("value", Integer.valueOf(value)).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MBunga> getAllBunga() {
    try {
      return this.em.createQuery("SELECT ma FROM MBunga ma where ma.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MBunga> bungaParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT ma FROM MBunga ma where ma.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MBunga> bungaParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT ma FROM MBunga ma where ma.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MBungaDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */