package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MTransactionDetail")
@Table(name = "m_transaction_detail")
public class MTransactionDetail {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_transaction_detail_seq")
  @SequenceGenerator(name = "id_transaction_detail_seq", sequenceName = "id_transaction_detail_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Type(type = "text")
  @Column(name = "data_value", nullable = false)
  private String data_value;
  
  @Column(name = "position", nullable = false)
  private String position;
  
  @Column(name = "surveyor")
  private String surveyor;
  
  @Column(name = "status_survey")
  private String status_survey;
  
  @Column(name = "departemen", nullable = false)
  private String departemen;
  
  @ManyToOne
  @JoinColumn(name = "id_head")
  private MTransaction id_head;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getData_value() {
    return this.data_value;
  }
  
  public void setData_value(String data_value) {
    this.data_value = data_value;
  }
  
  public String getPosition() {
    return this.position;
  }
  
  public void setPosition(String position) {
    this.position = position;
  }
  
  public String getSurveyor() {
    return this.surveyor;
  }
  
  public void setSurveyor(String surveyor) {
    this.surveyor = surveyor;
  }
  
  public String getStatus_survey() {
    return this.status_survey;
  }
  
  public void setStatus_survey(String status_survey) {
    this.status_survey = status_survey;
  }
  
  public String getDepartemen() {
    return this.departemen;
  }
  
  public void setDepartemen(String departemen) {
    this.departemen = departemen;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public MTransaction getId_head() {
    return this.id_head;
  }
  
  public void setId_head(MTransaction id_head) {
    this.id_head = id_head;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MTransactionDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */