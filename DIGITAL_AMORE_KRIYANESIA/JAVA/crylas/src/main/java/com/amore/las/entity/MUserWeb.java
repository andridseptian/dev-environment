package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "MUserWeb")
@Table(name = "m_user_web")
public class MUserWeb {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_user_web_seq")
  @SequenceGenerator(name = "id_user_web_seq", sequenceName = "id_user_web_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "username", nullable = false)
  private String username;
  
  @Column(name = "password", nullable = false)
  private String password;
  
  @Column(name = "status", nullable = false)
  private int status;
  
  @Column(name = "status_login", nullable = false)
  private int status_login;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @ManyToOne
  @JoinColumn(name = "id_group", nullable = false)
  private MGroup id_group;
  
  @Column(name = "last_login")
  @Temporal(TemporalType.TIMESTAMP)
  private Date last_login;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getUsername() {
    return this.username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }
  
  public String getPassword() {
    return this.password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
  public int getStatus() {
    return this.status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  public int getStatus_login() {
    return this.status_login;
  }
  
  public void setStatus_login(int status_login) {
    this.status_login = status_login;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public MGroup getId_group() {
    return this.id_group;
  }
  
  public void setId_group(MGroup id_group) {
    this.id_group = id_group;
  }
  
  public Date getLast_login() {
    return this.last_login;
  }
  
  public void setLast_login(Date last_login) {
    this.last_login = last_login;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MUserWeb.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */