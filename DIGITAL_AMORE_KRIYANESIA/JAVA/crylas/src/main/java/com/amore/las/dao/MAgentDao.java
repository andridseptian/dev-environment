package com.amore.las.dao;

import com.amore.las.entity.MAgent;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MAgentDao")
@Transactional
public class MAgentDao extends Dao {
  public MAgent saveOrUpdate(MAgent mAgent) {
    Date date = new Date();
    if (mAgent.getId() == null) {
      mAgent.setDtm_created(date);
      this.em.persist(mAgent);
    } else {
      mAgent.setDtm_updated(date);
      this.em.merge(mAgent);
    } 
    return mAgent;
  }
  
  public MAgent agentByMsisdn(String msisdn) {
    try {
      MAgent ma = (MAgent)this.em.createQuery("SELECT ma FROM MAgent ma WHERE ma.msisdn = :msisdn and ma.deleted = false").setParameter("msisdn", msisdn).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MAgent checkMsisdn(String msisdn) {
    try {
      Boolean deleted = Boolean.valueOf(false);
      MAgent ma = (MAgent)this.em.createQuery("SELECT ma FROM MAgent ma WHERE ma.msisdn = :msisdn and ma.deleted = :deleted").setParameter("msisdn", msisdn).setParameter("deleted", deleted).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MAgent agentById(Long id) {
    try {
      MAgent ma = (MAgent)this.em.createQuery("SELECT ma FROM MAgent ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MAgent> allAgent() {
    try {
      return this.em.createQuery("SELECT ma FROM MAgent ma where ma.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MAgent> agentParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT ma FROM MAgent ma where ma.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MAgent> agentParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT ma FROM MAgent ma where ma.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MAgentDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */