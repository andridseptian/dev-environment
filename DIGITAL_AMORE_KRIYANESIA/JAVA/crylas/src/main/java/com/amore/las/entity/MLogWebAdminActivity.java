package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MLogWebAdminActivity")
@Table(name = "m_log_webadmin_activity")
public class MLogWebAdminActivity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "log_webadmin_activity_id_seq")
  @SequenceGenerator(name = "log_webadmin_activity_id_seq", sequenceName = "log_webadmin_activity_id_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "remote_addr", nullable = false)
  private String remoteAddr;
  
  @Column(name = "request_url", nullable = false)
  private String requestUrl;
  
  @Column(name = "router_path", nullable = false)
  private String routerPath;
  
  @Column(name = "request_body", nullable = false)
  @Type(type = "text")
  private String requestBody;
  
  @Column(name = "response")
  @Type(type = "text")
  private String response;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "request_time")
  private Date requestTime;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "response_time")
  private Date responseTime;
  
  @Column(name = "response_code")
  private String responseCode;
  
  @Column(name = "response_message")
  private String responseMessage;
  
  @Column(name = "status")
  private String status;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getRemoteAddr() {
    return this.remoteAddr;
  }
  
  public void setRemoteAddr(String remoteAddr) {
    this.remoteAddr = remoteAddr;
  }
  
  public String getRequestUrl() {
    return this.requestUrl;
  }
  
  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }
  
  public String getRouterPath() {
    return this.routerPath;
  }
  
  public void setRouterPath(String routerPath) {
    this.routerPath = routerPath;
  }
  
  public String getRequestBody() {
    return this.requestBody;
  }
  
  public void setRequestBody(String requestBody) {
    this.requestBody = requestBody;
  }
  
  public String getResponse() {
    return this.response;
  }
  
  public void setResponse(String response) {
    this.response = response;
  }
  
  public Date getRequestTime() {
    return this.requestTime;
  }
  
  public void setRequestTime(Date requestTime) {
    this.requestTime = requestTime;
  }
  
  public Date getResponseTime() {
    return this.responseTime;
  }
  
  public void setResponseTime(Date responseTime) {
    this.responseTime = responseTime;
  }
  
  public String getResponseCode() {
    return this.responseCode;
  }
  
  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }
  
  public String getResponseMessage() {
    return this.responseMessage;
  }
  
  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }
  
  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MLogWebAdminActivity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */