package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MAgentTaked;
import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MFlowMobile;
import com.amore.las.entity.MFormMobile;
import com.amore.las.entity.MMenuMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class CreateAgentTaked implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      JSONArray logSurvey = bodyData.getJSONArray("surveys");
      int lengthS = logSurvey.length();
      if (lengthS > 0) {
        try {
          JSONArray jarr = new JSONArray();
          for (int a = 0; a < lengthS; a++) {
            JSONObject jso = new JSONObject();
            JSONObject survey = logSurvey.getJSONObject(a);
            Long id_trx_web = Long.valueOf(survey.getLong("id_survey"));
            MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(id_trx_web);
            MAgentTaked mAgentTaked = new MAgentTaked();
            mAgentTaked.setDeleted(Boolean.valueOf(false));
            mAgentTaked.setId_head_trx(mTransaction);
            mAgentTaked.setStatus(0);
            mAgentTaked.setUsr_created(mAgent);
            mAgentTaked.setAccount_tipe(mAgent.getAccount_tipe());
            mAgentTaked = SpringInitializer.getmAgentTakedDao().saveOrUpdate(mAgentTaked);
            jso.put("id_survey", id_trx_web);
            List<MTransactionDetail> list_trx = SpringInitializer.getmTransactionDao().transactionDetailByIdHead(mTransaction);
            JSONArray data = new JSONArray();
            List<MFlowMobile> mFlowMobile = SpringInitializer.getmFlowMobileDao().byAccountType(mAgent.getAccount_tipe());
            for (MFlowMobile mFlowMobile1 : mFlowMobile) {
              Integer idref = Integer.valueOf(mFlowMobile1.getId_ref());
              Long id_ref = Long.valueOf(idref.longValue());
              MMenuMobile mMenuMobile = SpringInitializer.getmMenuMobileDao().byId(id_ref);
              if (mMenuMobile != null) {
                MMenuMobile mMenuMobilezz = SpringInitializer.getmMenuMobileDao().byId(mMenuMobile.getId());
                List<MFormMobile> mFormMobile = SpringInitializer.getmFormMobileDao().byIdMenu(mMenuMobilezz);
                if (mFormMobile.size() > 0) {
                  for (MFormMobile mFormMobile1 : mFormMobile) {
                    Long idd = mFormMobile1.getId();
                    int iddd = idd.intValue();
                    MFlowMobile getToForm = SpringInitializer.getmFlowMobileDao().byTypeForm(iddd);
                    JSONArray element = new JSONArray();
                    JSONObject dataForm = new JSONObject();
                    dataForm.put("id", getToForm.getId());
                    dataForm.put("view_title", mFormMobile1.getHeader());
                    dataForm.put("url", mFormMobile1.getLink_url());
                    dataForm.put("mandatory", mFormMobile1.getMandatory());
                    dataForm.put("to", getToForm.getToid().getId());
                    dataForm.put("from", getToForm.getFromid());
                    data.put(dataForm);
                    List<MElementFormMobile> mElementFormMobile = SpringInitializer.getmElementFormMobileDao().byIdForm(mFormMobile1);
                    for (MElementFormMobile mElementFormMobile1 : mElementFormMobile) {
                      JSONObject data_element = new JSONObject();
                      String value = null;
                      for (MTransactionDetail mTransactionDetail : list_trx) {
                        JSONArray dt = new JSONArray(mTransactionDetail.getData_value());
                        int lengthDt = dt.length();
                        for (int i = 0; i < lengthDt; i++) {
                          JSONObject jsoo = dt.getJSONObject(i);
                          if (mElementFormMobile1.getName().equalsIgnoreCase(jsoo.getString("name")))
                            value = jsoo.getString("value"); 
                        } 
                      } 
                      data_element.put("value", value);
                      data_element.put("id", mElementFormMobile1.getId());
                      data_element.put("input_type", mElementFormMobile1.getInput_type());
                      data_element.put("label", mElementFormMobile1.getLabel());
                      data_element.put("length", mElementFormMobile1.getLength());
                      data_element.put("type", mElementFormMobile1.getType());
                      data_element.put("options", mElementFormMobile1.getAdditional());
                      element.put(data_element);
                    } 
                    dataForm.put("element_form", element);
                  } 
                  continue;
                } 
                this.log.info("Data Form Mobile Tidak Ditemukan");
                continue;
              } 
              this.log.info("Data Menu Mobile Tidak Ditemukan");
            } 
            jso.put("form", data);
            jarr.put(jso);
          } 
          bodyData = new JSONObject();
          bodyData.put("surveys", jarr);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } catch (Exception e) {
          this.log.info(ExceptionUtils.getStackTrace(e));
          this.rc = SpringInitializer.getmResponseDao().getRc("18");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\CreateAgentTaked.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */