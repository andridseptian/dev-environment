package com.amore.las.dao;

import com.amore.las.entity.MPrivilegeForms;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MPrivilegeFormsDao")
@Transactional
public class MPrivilegeFormsDao extends Dao {
  public MPrivilegeForms saveOrUpdate(MPrivilegeForms mPrivilegeForms) {
    Date date = new Date();
    if (mPrivilegeForms.getId() == null) {
      mPrivilegeForms.setDtm_created(date);
      this.em.persist(mPrivilegeForms);
    } else {
      mPrivilegeForms.setDtm_updated(date);
      this.em.merge(mPrivilegeForms);
    } 
    return mPrivilegeForms;
  }
  
  public void remove(Long id) {
    MPrivilegeForms mPermissions = (MPrivilegeForms)this.em.find(MPrivilegeForms.class, id);
    this.em.remove(mPermissions);
  }
  
  public MPrivilegeForms prvilegeById(Long id) {
    try {
      MPrivilegeForms mp = (MPrivilegeForms)this.em.createQuery("SELECT mp FROM MPrivilegeForms mp WHERE mp.id = :id and mp.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mp;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MPrivilegeForms> allPrivilege() {
    try {
      return this.em.createQuery("SELECT mp FROM MPrivilegeForms mp where mp.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MPrivilegeForms> privilegeParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mp FROM MPrivilegeForms mp where mp.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MPrivilegeForms> privilegeParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mp FROM MPrivilegeForms mp where mp.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MPrivilegeFormsDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */