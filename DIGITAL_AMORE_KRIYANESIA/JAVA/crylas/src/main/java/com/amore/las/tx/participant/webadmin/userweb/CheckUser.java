package com.amore.las.tx.participant.webadmin.userweb;

import com.amore.las.entity.MGroup;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CheckUser implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  JSONObject data = new JSONObject();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    try {
      String username = bodyData.getString("id_username");
      MUserWeb mUserWeb = SpringInitializer.getmUserWebDao().userByUsername(username);
      if (mUserWeb != null) {
        if (mUserWeb.getStatus() == 1) {
          ctx.put("USER_DETAIL", mUserWeb);
          return 65;
        } 
        MGroup mGroup = mUserWeb.getId_group();
        switch (mUserWeb.getStatus()) {
          case 0:
            this.rc = SpringInitializer.getmResponseDao().getRc("10");
            this.log.info("RM : " + this.rc.getRm());
            this.data.put("name", mUserWeb.getName());
            this.data.put("group", mGroup.getName());
            this.data.put("status_user", "INACTIVE");
            bodyData.put("data", this.data);
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
            return 64;
          case 2:
            this.rc = SpringInitializer.getmResponseDao().getRc("10");
            this.data.put("name", mUserWeb.getName());
            this.data.put("group", mGroup.getName());
            this.data.put("status_user", "BLOCKED");
            bodyData.put("data", this.data);
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
            return 64;
        } 
        this.rc = SpringInitializer.getmResponseDao().getRc("10");
        this.data.put("name", mUserWeb.getName());
        this.data.put("group", mGroup.getName());
        this.data.put("status_user", "INACTIVE");
        bodyData.put("data", this.data);
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
        return 64;
      } 
      this.rc = SpringInitializer.getmResponseDao().getRc("11");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
      return 64;
    } catch (Exception e) {
      this.wsResponse = new ResponseWebServiceContainer();
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
      return 64;
    } 
  }
  
  public void commit(long id, Serializable srlzbl) {}
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmi\\userweb\CheckUser.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */