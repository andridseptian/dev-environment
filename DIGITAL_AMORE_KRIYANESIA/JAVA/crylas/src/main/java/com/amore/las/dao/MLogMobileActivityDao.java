package com.amore.las.dao;

import com.amore.las.entity.MLogMobileActivity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MLogMobileActivityDao")
@Transactional
public class MLogMobileActivityDao extends Dao {
  public MLogMobileActivity saveOrUpdate(MLogMobileActivity mLogMobileActivity) {
    if (mLogMobileActivity.getId() == null) {
      this.em.persist(mLogMobileActivity);
    } else {
      this.em.merge(mLogMobileActivity);
    } 
    return mLogMobileActivity;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MLogMobileActivityDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */