package com.amore.las.tx.participant.webadmin.form;

import com.amore.las.entity.MElementForm;
import com.amore.las.entity.MForm;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CreateElementForm implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("id_form") && bodyData.has("name") && bodyData.has("label") && bodyData
        .has("type") && bodyData.has("value") && bodyData.has("additional") && bodyData
        .has("mandatory") && bodyData.has("master")) {
        MForm mForm = SpringInitializer.getmFormDao().formById(Long.valueOf(bodyData.getLong("id_form")));
        if (mForm != null) {
          MElementForm mElementForm = new MElementForm();
          mElementForm.setId_form(mForm);
          mElementForm.setName(bodyData.getString("name"));
          mElementForm.setLabel(bodyData.getString("label"));
          mElementForm.setType(bodyData.getString("type"));
          mElementForm.setValue(bodyData.getString("value"));
          mElementForm.setAdditional(bodyData.getString("additional"));
          mElementForm.setMandatory(Boolean.valueOf(bodyData.getBoolean("mandatory")));
          mElementForm.setMaster(bodyData.getString("master"));
          mElementForm.setUsr_created(mUserWeb);
          mElementForm.setDeleted(Boolean.valueOf(false));
          mElementForm = SpringInitializer.getmElementFormDao().saveOrUpdate(mElementForm);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\form\CreateElementForm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */