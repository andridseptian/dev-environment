package com.amore.las.dao;

import com.amore.las.entity.MFlowMobile;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MFlowMobileDao")
@Transactional
public class MFlowMobileDao extends Dao {
  public MFlowMobile saveOrUpdate(MFlowMobile mFlowMobile) {
    Date date = new Date();
    if (mFlowMobile.getId() == null) {
      mFlowMobile.setDtm_created(date);
      this.em.persist(mFlowMobile);
    } else {
      mFlowMobile.setDtm_updated(date);
      this.em.merge(mFlowMobile);
    } 
    return mFlowMobile;
  }
  
  public MFlowMobile byId(Long id) {
    try {
      MFlowMobile mm = (MFlowMobile)this.em.createQuery("SELECT mm FROM MFlowMobile mm WHERE mm.id = :id and mm.deleted = false order by mm.id asc").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlowMobile> byAccountType(String account_type) {
    try {
      return this.em.createQuery("SELECT mm FROM MFlowMobile mm WHERE mm.account_type = :account_type and mm.form = false and mm.deleted = false")
        .setParameter("account_type", account_type)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MFlowMobile byTypeMenu(String id_ref) {
    try {
      MFlowMobile mm = (MFlowMobile)this.em.createQuery("SELECT mm FROM MFlowMobile mm WHERE mm.form = false and mm.id_ref = :id_ref and mm.deleted = false").setParameter("id_ref", id_ref).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MFlowMobile byTypeForm(int id_ref) {
    try {
      MFlowMobile mm = (MFlowMobile)this.em.createQuery("SELECT mm FROM MFlowMobile mm WHERE mm.form = true and mm.id_ref = :id_ref and mm.deleted = false").setParameter("id_ref", Integer.valueOf(id_ref)).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MFlowMobile firstFlow(String account_type) {
    try {
      MFlowMobile mm = (MFlowMobile)this.em.createQuery("SELECT mm FROM MFlowMobile mm WHERE mm.form = false and mm.account_type = :account_type and fromid = 0 and mm.deleted = false").setParameter("account_type", account_type).setMaxResults(1).getSingleResult();
      return mm;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlowMobile> allFlow() {
    try {
      return this.em.createQuery("SELECT mm FROM MFlowMobile mm WHERE mm.deleted = false order by mm.id asc")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlowMobile> flowParam(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT me FROM MFlowMobile me where me.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFlowMobile> flowParamCount(String query) {
    try {
      return this.em.createQuery("SELECT me FROM MFlowMobile me where me.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MFlowMobileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */