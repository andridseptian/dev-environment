package com.amore.las.tx.participant;

import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;
import org.jpos.util.Log;

public class ControllerSwitch implements GroupSelector, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public String select(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    return this.cfg.get(ctx.getString("GROUP"), "NoRoute");
  }
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    String path = ctx.getString("PATH");
    if (path == null || "".equals(path)) {
      ctx.put("GROUP", "NoRoute");
    } else {
      ctx.put("GROUP", path);
    } 
    return 65;
  }
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
  
  public void commit(long id, Serializable context) {}
  
  public void abort(long id, Serializable context) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\ControllerSwitch.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */