package com.amore.las.dao;

import com.amore.las.entity.MSettingAplikasi;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MSettingAplikasiDao")
@Transactional
public class MSettingAplikasiDao extends Dao {
  public MSettingAplikasi saveOrUpdate(MSettingAplikasi mSettingAplikasi) {
    if (mSettingAplikasi.getId() == null) {
      this.em.persist(mSettingAplikasi);
    } else {
      this.em.merge(mSettingAplikasi);
    } 
    return mSettingAplikasi;
  }
  
  public MSettingAplikasi byId(Long id) {
    try {
      MSettingAplikasi mr = (MSettingAplikasi)this.em.createQuery("SELECT mr FROM MSettingAplikasi mr WHERE mr.id = :id").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mr;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MSettingAplikasi byName(String name) {
    try {
      MSettingAplikasi mr = (MSettingAplikasi)this.em.createQuery("SELECT mr FROM MSettingAplikasi mr WHERE mr.name = :name").setParameter("name", name).setMaxResults(1).getSingleResult();
      return mr;
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MSettingAplikasiDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */