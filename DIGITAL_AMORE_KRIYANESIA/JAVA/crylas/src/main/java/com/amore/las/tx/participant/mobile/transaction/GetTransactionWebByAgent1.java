package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class GetTransactionWebByAgent1 implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\GetTransactionWebByAgent1.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */