package com.amore.las.dao;

import com.amore.las.entity.MKaryawan;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MKaryawanDao")
@Transactional
public class MKaryawanDao extends Dao {
  public MKaryawan saveOrUpdate(MKaryawan mKaryawan) {
    Date date = new Date();
    if (mKaryawan.getId() == null) {
      mKaryawan.setDtm_created(date);
      this.em.persist(mKaryawan);
    } else {
      mKaryawan.setDtm_updated(date);
      this.em.merge(mKaryawan);
    } 
    return mKaryawan;
  }
  
  public MKaryawan byId(Long id) {
    try {
      MKaryawan ma = (MKaryawan)this.em.createQuery("SELECT ma FROM MKaryawan ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MKaryawan byName(String name) {
    try {
      MKaryawan ma = (MKaryawan)this.em.createQuery("SELECT ma FROM MKaryawan ma WHERE ma.name = :name and ma.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MKaryawan byNik(String nik) {
    try {
      MKaryawan ma = (MKaryawan)this.em.createQuery("SELECT ma FROM MKaryawan ma WHERE ma.nik_karyawan = :nik_karyawan and ma.deleted = false").setParameter("nik_karyawan", nik).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MKaryawan> byPrice(int price) {
    try {
      return this.em.createQuery("SELECT ma FROM MKaryawan ma where ma.price = :price and ma.deleted = false")
        .setParameter("price", Integer.valueOf(price))
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MKaryawan> getAllKaryawan() {
    try {
      return this.em.createQuery("SELECT ma FROM MKaryawan ma where ma.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MKaryawan> karyawanParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT ma FROM MKaryawan ma where ma.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MKaryawan> karyawanParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT ma FROM MKaryawan ma where ma.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MKaryawanDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */