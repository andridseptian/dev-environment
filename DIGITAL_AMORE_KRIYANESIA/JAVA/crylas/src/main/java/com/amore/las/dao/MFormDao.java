package com.amore.las.dao;

import com.amore.las.entity.MForm;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MFormDao")
@Transactional
public class MFormDao extends Dao {
  public MForm saveOrUpdate(MForm mForm) {
    Date date = new Date();
    if (mForm.getId() == null) {
      mForm.setDtm_created(date);
      this.em.persist(mForm);
    } else {
      mForm.setDtm_updated(date);
      this.em.merge(mForm);
    } 
    return mForm;
  }
  
  public void remove(Long id) {
    MForm mForm = (MForm)this.em.find(MForm.class, id);
    this.em.remove(mForm);
  }
  
  public MForm formByName(String name) {
    try {
      MForm mf = (MForm)this.em.createQuery("SELECT mf FROM MForm mf WHERE mf.name = :name").setParameter("name", name).setMaxResults(1).getSingleResult();
      return mf;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MForm formById(Long id) {
    try {
      MForm mf = (MForm)this.em.createQuery("SELECT mf FROM MForm mf WHERE mf.id = :id and mf.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mf;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MForm> allForm() {
    try {
      return this.em.createQuery("SELECT mf FROM MForm mf where mf.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MForm> formParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mf FROM MForm mf where mf.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MForm> formParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mf FROM MForm mf where mf.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MForm> allFormAndroid() {
    try {
      return this.em.createQuery("SELECT mf FROM MForm mf where mf.deleted = false and mf.android = true")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MFormDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */