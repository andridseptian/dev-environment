package com.amore.las.tx.participant.reqnik;

import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.Constants;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class ReqNik implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      String username = mUserWeb.getUsername();
      String nik = bodyData.getString("nik");
      String address = bodyData.getString("address");
      JSONObject data = new JSONObject();
      data.put("username", username);
      data.put("nik", nik);
      data.put("address", address);
      String http = Constants.IP_REQ_NIK;
      CloseableHttpClient httpClient = HttpClientBuilder.create().build();
      HttpPost httpPost = new HttpPost(http);
      StringEntity se = new StringEntity(data.toString());
      this.log.info("DATA  :  " + se);
      httpPost.setEntity((HttpEntity)se);
      httpPost.setHeader("Accept", "application/json");
      httpPost.setHeader("Content-Type", "application/json");
      httpPost.setHeader("xapix", Constants.API_KEY);
      httpPost.setHeader("xincomingx", Constants.INCOMING);
      CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest)httpPost);
      if (closeableHttpResponse != null) {
        BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
        StringBuilder respMsg = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null)
          respMsg.append(line); 
        JSONObject json = new JSONObject(respMsg.toString());
        if (json != null) {
          String tempRC = json.getString("rc");
          String tempRM = json.getString("rm");
          bodyData = new JSONObject();
          if (tempRC.equalsIgnoreCase("00")) {
            if (json.has("NIK") && !json.isNull("NIK"))
              bodyData.put("NIK", json.getLong("NIK")); 
            if (json.has("NO_KK") && !json.isNull("NO_KK"))
              bodyData.put("NO_KK", json.getLong("NO_KK")); 
            if (json.has("NAMA_LGKP") && !json.isNull("NAMA_LGKP"))
              bodyData.put("NAMA_LGKP", json.getString("NAMA_LGKP")); 
            if (json.has("TMPT_LHR") && !json.isNull("TMPT_LHR"))
              bodyData.put("TMPT_LHR", json.getString("TMPT_LHR")); 
            if (json.has("TGL_LHR") && !json.isNull("TGL_LHR")) {
              String tglLahir = json.getString("TGL_LHR");
              String[] arr = tglLahir.split("-");
              tglLahir = arr[2] + "-" + arr[1] + "-" + arr[0];
              bodyData.put("TGL_LHR", tglLahir);
            } 
            if (json.has("JENIS_KLMIN") && !json.isNull("JENIS_KLMIN"))
              bodyData.put("JENIS_KLMIN", json.getString("JENIS_KLMIN")); 
            if (json.has("ALAMAT") && !json.isNull("ALAMAT"))
              bodyData.put("ALAMAT", json.getString("ALAMAT")); 
            if (json.has("NO_RT") && !json.isNull("NO_RT"))
              bodyData.put("NO_RT", json.getInt("NO_RT")); 
            if (json.has("NO_RW") && !json.isNull("NO_RW"))
              bodyData.put("NO_RW", json.getInt("NO_RW")); 
            if (json.has("KEL_NAME") && !json.isNull("KEL_NAME"))
              bodyData.put("KEL_NAME", json.getString("KEL_NAME")); 
            if (json.has("KEC_NAME") && !json.isNull("KEC_NAME"))
              bodyData.put("KEC_NAME", json.getString("KEC_NAME")); 
            if (json.has("KAB_NAME") && !json.isNull("KAB_NAME"))
              bodyData.put("KAB_NAME", json.getString("KAB_NAME")); 
            if (json.has("PROP_NAME") && !json.isNull("PROP_NAME"))
              bodyData.put("PROP_NAME", json.getString("PROP_NAME")); 
            if (json.has("KODE_POS") && !json.isNull("KODE_POS"))
              bodyData.put("KODE_POS", json.getString("KODE_POS")); 
            if (json.has("AGAMA") && !json.isNull("AGAMA"))
              bodyData.put("AGAMA", json.getString("AGAMA")); 
            if (json.has("STATUS_KAWIN") && !json.isNull("STATUS_KAWIN"))
              bodyData.put("STATUS_KAWIN", json.getString("STATUS_KAWIN")); 
            if (json.has("JENIS_PKRJN") && !json.isNull("JENIS_PKRJN"))
              bodyData.put("JENIS_PKRJN", json.getString("JENIS_PKRJN")); 
            if (json.has("NAMA_LGKP_IBU") && !json.isNull("NAMA_LGKP_IBU"))
              bodyData.put("NAMA_LGKP_IBU", json.getString("NAMA_LGKP_IBU")); 
            this.rc = SpringInitializer.getmResponseDao().getRc("00");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } else {
            this.wsResponse = new ResponseWebServiceContainer(tempRC, tempRM, bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } 
        } else {
          this.log.info("+++++++++");
        } 
      } else {
        this.log.info("+++++++++");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\reqnik\ReqNik.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */