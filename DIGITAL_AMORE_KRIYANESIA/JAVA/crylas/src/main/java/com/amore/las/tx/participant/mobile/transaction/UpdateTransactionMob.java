package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MImageMob;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionMobile;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.misc.BASE64Decoder;

public class UpdateTransactionMob implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss.SSS");
    String id_agent = mAgent.getId().toString();
    try {
      Long id_transaction = Long.valueOf(bodyData.getLong("id"));
      int id_form = bodyData.getInt("id_form");
      Long id_trx = Long.valueOf(bodyData.getLong("trx_web"));
      String dt = bodyData.get("data").toString();
      MTransactionMobile mTransactionMobile = SpringInitializer.getmTransactionMobileDao().byId(id_transaction);
      if (mTransactionMobile != null) {
        JSONArray data = new JSONArray(dt);
        int length = data.length();
        JSONObject jso = null;
        for (int i = 0; i < length; i++) {
          jso = data.getJSONObject(i);
          if (jso.getString("type").equalsIgnoreCase("image_upload"))
            try {
              data.getJSONObject(i).put("value", "");
            } catch (Exception e) {
              this.log.info("Failed replace image upload");
            }  
        } 
        mTransactionMobile.setData_value(data.toString());
        List<MImageMob> mImageMobb = SpringInitializer.getmImageMobDao().byIdTrxMob(mTransactionMobile);
        int sz = mImageMobb.size();
        if (sz > 0) {
          for (int j = 0; j < sz; j++) {
            String url = ((MImageMob)mImageMobb.get(j)).getLink_url();
            File file = new File(url);
            file.delete();
            MImageMob mm = mImageMobb.get(j);
            SpringInitializer.getmImageMobDao().remove(mm);
          } 
        } else {
          this.log.info("Image Not Found");
        } 
      } else {
        MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(id_trx);
        JSONArray data = new JSONArray(dt);
        int length = data.length();
        JSONObject jso = null;
        for (int i = 0; i < length; i++) {
          jso = data.getJSONObject(i);
          if (jso.getString("type").equalsIgnoreCase("image_upload"))
            try {
              data.getJSONObject(i).put("value", "");
            } catch (Exception e) {
              this.log.info("Failed replace image upload");
            }  
        } 
        mTransactionMobile = new MTransactionMobile();
        mTransactionMobile.setDeleted(Boolean.valueOf(false));
        mTransactionMobile.setUsr_created(mAgent);
        mTransactionMobile.setStatus_transaction(1);
        mTransactionMobile.setId_form(id_form);
        mTransactionMobile.setId_head_trx(mTransaction);
        mTransactionMobile.setData_value(data.toString());
        SpringInitializer.getmTransactionMobileDao().saveOrUpdate(mTransactionMobile);
      } 
      try {
        JSONArray getImg = new JSONArray(dt);
        int length2 = getImg.length();
        JSONObject img = null;
        for (int i = 0; i < length2; i++) {
          img = getImg.getJSONObject(i);
          if (img.getString("type").equalsIgnoreCase("image_upload")) {
            BASE64Decoder decoder = new BASE64Decoder();
            String value = img.getString("value");
            if (!value.equals("-") || !value.equals("")) {
              Date date = new Date();
              String now = sdf.format(date);
              String file_name = id_agent + "-" + mTransactionMobile.getId_form() + "-" + now + ".png";
              value.replaceAll("\n", "");
              value.replaceAll("\\\\", "");
              byte[] imageByte = decoder.decodeBuffer(value);
              File outputfile = new File("/opt/crylas/webadmin/mobile/img/" + file_name);
              String url = outputfile.toString().replace("/opt/crylas/webadmin", "");
              outputfile.createNewFile();
              FileOutputStream stream = new FileOutputStream(outputfile);
              stream.write(imageByte);
              stream.flush();
              stream.close();
              MImageMob mImageMob = new MImageMob();
              mImageMob.setDeleted(Boolean.valueOf(false));
              mImageMob.setId_element_form(img.getInt("id"));
              mImageMob.setLink_url(url);
              mImageMob.setLatitude(img.getString("latitude"));
              mImageMob.setLongitude(img.getString("longitude"));
              mImageMob.setId_transaction_mob(mTransactionMobile);
              mImageMob.setUsr_created(mAgent);
              mImageMob = SpringInitializer.getmImageMobDao().saveOrUpdate(mImageMob);
            } 
          } 
        } 
      } catch (Exception e) {
        this.rc = SpringInitializer.getmResponseDao().getRc("22");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
      SpringInitializer.getmTransactionMobileDao().saveOrUpdate(mTransactionMobile);
      this.rc = SpringInitializer.getmResponseDao().getRc("00");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\UpdateTransactionMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */