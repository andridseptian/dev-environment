package com.amore.las.tx.participant.mobile.form;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MAgentTaked;
import com.amore.las.entity.MElementFormMobile;
import com.amore.las.entity.MFlowMobile;
import com.amore.las.entity.MFormMobile;
import com.amore.las.entity.MMenuMobile;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class Initialize implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    JSONObject logData = new JSONObject();
    String cabang = null, nama = null, flapon = null, produk = null;
    String alamat = null;
    String rtrw = null;
    String kelurahan = null;
    String kecamatan = null;
    String kota = null;
    String provinsi = null;
    String kodepos = null;
    String alamat_lengkap = null;
    try {
      List<MFlowMobile> mFlowMobile = SpringInitializer.getmFlowMobileDao().byAccountType(mAgent.getAccount_tipe());
      if (mFlowMobile.size() > 0) {
        JSONArray data_menu = new JSONArray();
        JSONArray form = new JSONArray();
        JSONArray agent_taked = new JSONArray();
        JSONArray survey_data = new JSONArray();
        for (MFlowMobile mFlowMobile1 : mFlowMobile) {
          JSONObject dataFlow = new JSONObject();
          dataFlow.put("id", mFlowMobile1.getId());
          dataFlow.put("to", mFlowMobile1.getToid());
          dataFlow.put("from", mFlowMobile1.getFromid());
          Integer idref = Integer.valueOf(mFlowMobile1.getId_ref());
          Long id_ref = Long.valueOf(idref.longValue());
          JSONArray listMenu = new JSONArray();
          if (!mFlowMobile1.getForm().booleanValue() && !mFlowMobile1.getForm().booleanValue()) {
            MMenuMobile mMenuMobile = SpringInitializer.getmMenuMobileDao().byId(id_ref);
            if (mMenuMobile != null) {
              dataFlow.put("main", mMenuMobile.getMain());
              dataFlow.put("view_title", mMenuMobile.getView_title());
              dataFlow.put("tipe", mMenuMobile.getTipe());
              MMenuMobile mMenuMobilezz = SpringInitializer.getmMenuMobileDao().byId(mMenuMobile.getId());
              List<MFormMobile> mFormMobile = SpringInitializer.getmFormMobileDao().byIdMenu(mMenuMobilezz);
              if (mFormMobile.size() > 0) {
                for (MFormMobile mFormMobile1 : mFormMobile) {
                  Long idd = mFormMobile1.getId();
                  int iddd = idd.intValue();
                  MFlowMobile getToForm = SpringInitializer.getmFlowMobileDao().byTypeForm(iddd);
                  JSONArray element = new JSONArray();
                  JSONObject dataListMenu = new JSONObject();
                  JSONObject dataForm = new JSONObject();
                  dataListMenu.put("header", mFormMobile1.getHeader());
                  dataListMenu.put("description", mFormMobile1.getDecription());
                  dataListMenu.put("mandatory", mFormMobile1.getMandatory());
                  dataForm.put("id", getToForm.getId());
                  dataForm.put("view_title", mFormMobile1.getHeader());
                  dataForm.put("url", mFormMobile1.getLink_url());
                  dataForm.put("mandatory", mFormMobile1.getMandatory());
                  dataListMenu.put("to", getToForm.getToid().getId());
                  dataListMenu.put("jenis", getToForm.getToid().getForm());
                  dataForm.put("to", getToForm.getToid().getId());
                  dataForm.put("from", getToForm.getFromid());
                  listMenu.put(dataListMenu);
                  dataFlow.put("list_menu", listMenu);
                  form.put(dataForm);
                  List<MElementFormMobile> mElementFormMobile = SpringInitializer.getmElementFormMobileDao().byIdForm(mFormMobile1);
                  for (MElementFormMobile mElementFormMobile1 : mElementFormMobile) {
                    JSONObject data_element = new JSONObject();
                    data_element.put("id", mElementFormMobile1.getId());
                    data_element.put("input_type", mElementFormMobile1.getInput_type());
                    data_element.put("label", mElementFormMobile1.getLabel());
                    data_element.put("length", mElementFormMobile1.getLength());
                    data_element.put("type", mElementFormMobile1.getType());
                    data_element.put("options", mElementFormMobile1.getAdditional());
                    element.put(data_element);
                  } 
                  dataForm.put("element_form", element);
                } 
              } else {
                this.log.info("Data Form Mobile Tidak Ditemukan");
              } 
            } else {
              this.log.info("Data Menu Mobile Tidak Ditemukan");
            } 
          } else {
            MMenuMobile mMenuMobilez = SpringInitializer.getmMenuMobileDao().byId(id_ref);
            List<MFormMobile> mFormMobile = SpringInitializer.getmFormMobileDao().byIdMenu(mMenuMobilez);
            if (mFormMobile.size() > 0) {
              for (MFormMobile mFormMobile1 : mFormMobile) {
                JSONObject dataListMenu = new JSONObject();
                dataListMenu.put("header", mFormMobile1.getHeader());
                dataListMenu.put("description", mFormMobile1.getDecription());
                dataListMenu.put("mandatory", mFormMobile1.getMandatory());
                Long idd = mFormMobile1.getId();
                int iddd = idd.intValue();
                MFlowMobile getToForm = SpringInitializer.getmFlowMobileDao().byTypeForm(iddd);
                dataListMenu.put("to", getToForm.getToid().getId());
                dataListMenu.put("jenis", getToForm.getToid().getForm());
                listMenu.put(dataListMenu);
                dataFlow.put("list_menu", listMenu);
              } 
            } else {
              this.log.info("Data Form Mobile Tidak Ditemukan");
            } 
          } 
          data_menu.put(dataFlow);
          logData.put("menu", data_menu);
          logData.put("form", form);
        } 
        List<MAgentTaked> mat = SpringInitializer.getmAgentTakedDao().byUser(mAgent);
        int lgmat = mat.size();
        if (lgmat > 0) {
          for (MAgentTaked mAgentTaked : mat) {
            JSONArray data = new JSONArray();
            JSONObject logDataSurvey = new JSONObject();
            List<MTransactionDetail> list_trx = SpringInitializer.getmTransactionDao().transactionDetailByIdHead(mAgentTaked.getId_head_trx());
            int lengthTrxDetail = list_trx.size();
            if (lengthTrxDetail > 0) {
              for (MTransactionDetail mTransactionDetail : list_trx) {
                JSONArray dt = new JSONArray(mTransactionDetail.getData_value());
                int lengthDt = dt.length();
                for (int i = 0; i < lengthDt; i++) {
                  JSONObject jso = dt.getJSONObject(i);
                  if (jso.getString("name").equalsIgnoreCase("Cabang")) {
                    cabang = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("Nama")) {
                    nama = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("plafon")) {
                    flapon = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("JenisKredit")) {
                    produk = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("AlamatSekarang")) {
                    alamat = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("RTRWSekarang")) {
                    rtrw = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("KelurahanSekarang")) {
                    kelurahan = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("KecamatanSekarang")) {
                    kecamatan = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("KotaSekarang")) {
                    kota = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("ProvinsiSekarang")) {
                    provinsi = jso.getString("value");
                  } else if (jso.getString("name").equalsIgnoreCase("KodePosSekarang")) {
                    kodepos = jso.getString("value");
                  } 
                } 
                alamat_lengkap = alamat + " RT/RW. " + rtrw + " Kelurahan " + kelurahan + " Kecamatan " + kecamatan + " Kota " + kota + " " + provinsi + " " + kodepos;
              } 
              JSONObject jsd = new JSONObject();
              jsd.put("id", mAgentTaked.getId_head_trx().getId());
              jsd.put("cabang", cabang);
              jsd.put("nama", nama);
              jsd.put("flapon", flapon);
              jsd.put("produk", produk);
              jsd.put("alamat", alamat_lengkap);
              agent_taked.put(jsd);
            } else {
              this.log.info("Data detail not found");
            } 
            List<MFlowMobile> mf = SpringInitializer.getmFlowMobileDao().byAccountType(mAgent.getAccount_tipe());
            for (MFlowMobile mf1 : mf) {
              Integer idref = Integer.valueOf(mf1.getId_ref());
              Long id_ref = Long.valueOf(idref.longValue());
              MMenuMobile mMenuMobile = SpringInitializer.getmMenuMobileDao().byId(id_ref);
              if (mMenuMobile != null) {
                MMenuMobile mMenuMobilezz = SpringInitializer.getmMenuMobileDao().byId(mMenuMobile.getId());
                List<MFormMobile> mFormMobile = SpringInitializer.getmFormMobileDao().byIdMenu(mMenuMobilezz);
                if (mFormMobile.size() > 0) {
                  for (MFormMobile mFormMobile1 : mFormMobile) {
                    Long idd = mFormMobile1.getId();
                    int iddd = idd.intValue();
                    MFlowMobile getToForm = SpringInitializer.getmFlowMobileDao().byTypeForm(iddd);
                    JSONArray element = new JSONArray();
                    JSONObject dataForm = new JSONObject();
                    dataForm.put("id", getToForm.getId());
                    dataForm.put("view_title", mFormMobile1.getHeader());
                    dataForm.put("url", mFormMobile1.getLink_url());
                    dataForm.put("mandatory", mFormMobile1.getMandatory());
                    dataForm.put("to", getToForm.getToid().getId());
                    dataForm.put("from", getToForm.getFromid());
                    data.put(dataForm);
                    List<MElementFormMobile> mElementFormMobile = SpringInitializer.getmElementFormMobileDao().byIdForm(mFormMobile1);
                    for (MElementFormMobile mElementFormMobile1 : mElementFormMobile) {
                      JSONObject data_element = new JSONObject();
                      String value = null;
                      for (MTransactionDetail mTransactionDetail : list_trx) {
                        JSONArray dt = new JSONArray(mTransactionDetail.getData_value());
                        int lengthDt = dt.length();
                        for (int i = 0; i < lengthDt; i++) {
                          JSONObject jso = dt.getJSONObject(i);
                          if (mElementFormMobile1.getName().equalsIgnoreCase(jso.getString("name")))
                            value = jso.getString("value"); 
                        } 
                      } 
                      data_element.put("value", value);
                      data_element.put("id", mElementFormMobile1.getId());
                      data_element.put("input_type", mElementFormMobile1.getInput_type());
                      data_element.put("label", mElementFormMobile1.getLabel());
                      data_element.put("length", mElementFormMobile1.getLength());
                      data_element.put("type", mElementFormMobile1.getType());
                      data_element.put("options", mElementFormMobile1.getAdditional());
                      element.put(data_element);
                    } 
                    dataForm.put("element_form", element);
                  } 
                  continue;
                } 
                this.log.info("Data Form Mobile Tidak Ditemukan");
                continue;
              } 
              this.log.info("Data Menu Mobile Tidak Ditemukan");
            } 
            logDataSurvey.put("form", data);
            logDataSurvey.put("id_survey", mAgentTaked.getId_head_trx().getId());
            survey_data.put(logDataSurvey);
          } 
        } else {
          this.log.info("Data Agent Taked Tidak Ditemukan");
        } 
        logData.put("taked_survey", agent_taked);
        logData.put("survey_data", survey_data);
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
      bodyData.put("data", logData);
      this.rc = SpringInitializer.getmResponseDao().getRc("00");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\form\Initialize.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */