package com.amore.las.dao;

import com.amore.las.entity.MWebConfig;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MWebConfigDao")
@Transactional
public class MWebConfigDao extends Dao {
  Date date = new Date();
  
  public MWebConfig saveOrUpdate(MWebConfig mWebConfig) {
    if (mWebConfig.getId() == null) {
      mWebConfig.setDtm_created(this.date);
      this.em.persist(mWebConfig);
    } else {
      mWebConfig.setDtm_updated(this.date);
      this.em.merge(mWebConfig);
    } 
    return mWebConfig;
  }
  
  public MWebConfig webConfigById(Long id) {
    try {
      MWebConfig mw = (MWebConfig)this.em.createQuery("SELECT mw FROM MWebConfig mw WHERE mw.id = :id and mw.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mw;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MWebConfig webConfigByName(String name) {
    try {
      MWebConfig mw = (MWebConfig)this.em.createQuery("SELECT mw FROM MWebConfig mw WHERE mw.name_config = :name and mw.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return mw;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MWebConfig> allWebConfig() {
    try {
      return this.em.createQuery("SELECT mw FROM MWebConfig mw where mw.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MWebConfig> webConfigParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mw FROM MWebConfig mw where mw.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MWebConfig> webConfigParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mw FROM MWebConfig mw where mw.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MWebConfigDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */