package com.amore.las.utility;

import org.jpos.util.Log;

public class Constants {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public static String IP_REQ_NIK = "http://172.16.100.13:9961/xnik/reqnik";
  
  public static String API_KEY = "12345678";
  
  public static String INCOMING = "LAS";
  
  public static final String TOKEN = "987654321098765432109876";
  
  public static final String DATABASE_SCHEMA = "las";
  
  public static final String HTTP_SERVLET_ENTITY_AUTH = "x-api-key";
  
  public class REQ_NIK {
    public static final String NIK = "NIK";
    
    public static final String NOKK = "NO_KK";
    
    public static final String NAMA = "NAMA_LGKP";
    
    public static final String TEMPATLAHIR = "TMPT_LHR";
    
    public static final String TGLLAHIR = "TGL_LHR";
    
    public static final String JENISKELAMIN = "JENIS_KLMIN";
    
    public static final String ALAMAT = "ALAMAT";
    
    public static final String NORT = "NO_RT";
    
    public static final String NORW = "NO_RW";
    
    public static final String KELURAHAN = "KEL_NAME";
    
    public static final String KECAMATAN = "KEC_NAME";
    
    public static final String KOTA = "KAB_NAME";
    
    public static final String PROPINSI = "PROP_NAME";
    
    public static final String AGAMA = "AGAMA";
    
    public static final String STATUS = "STATUS_KAWIN";
    
    public static final String PEKERJAAN = "JENIS_PKRJN";
    
    public static final String NAMAIBU = "NAMA_LGKP_IBU";
    
    public static final String NOKOT = "NO_KAB";
    
    public static final String NOPROP = "NO_PROP";
    
    public static final String NOKEC = "NO_KEC";
    
    public static final String NOKEL = "NO_KEL";
    
    public static final String ZIPCODE = "KODE_POS";
  }
  
  public class SETTING {
    public static final String TRIDES_PIN_ENCRYPT = "L4SD1G1T4L4M0R3KR1Y4N3S14";
    
    public class SETTING_VALUE {
      public static final int TIME_OUT = 60000;
      
      public static final String TRXMGR_MOB = "las-mob-trx";
      
      public static final String TRXMGR_WEBADMIN = "las-webadmin-trx";
      
      public static final String TRXMGR_UPLOAD = "upload-trx";
      
      public static final String TRXMGR_UPLOAD_MOB = "upload-mob";
      
      public static final String CORE_URL = "";
    }
    
    public class SETTING_DESC {
      public static final String TIME_OUT = "TIME_OUT";
      
      public static final String TRXMGR_MOB = "TRXMGR_MOB";
      
      public static final String TRXMGR_WEBADMIN = "TRXMGR_WEBADMIN";
      
      public static final String TRXMGR_UPLOAD = "TRXMGR_UPLOAD";
      
      public static final String TRXMGR_UPLOAD_MOB = "TRXMGR_UPLOAD_MOB";
      
      public static final String CORE_URL = "CORE_URL";
    }
  }
  
  public class WS {
    public static final String REQUEST_BODY = "REQUEST_BODY";
    
    public static final String REQUEST = "REQUEST";
    
    public static final String RESPONSE = "RESPONSE";
    
    public static final String IMAGE = "IMAGE";
    
    public static final String KEY = "KEY";
    
    public static final String PATH = "PATH";
    
    public static final String MLOG = "MLOG";
    
    public static final String ABORTED_RESPONSE = "Internal Error";
    
    public class STATUS {
      public static final String SUCCESS = "0";
      
      public static final String FAILED = "1";
      
      public class APPROVAL {
        public static final int APPROVE = 1;
        
        public static final int WAITING_FOR_APPROVE = 2;
        
        public static final int REJECT = 3;
      }
      
      public class TRANSACTION {
        public static final int ON_GOING = 1;
        
        public static final int ACCEPTED = 2;
        
        public static final int DECLINE = 3;
      }
      
      public class AGENT_TAKED {
        public static final int TAKED = 0;
        
        public static final int FILLED = 1;
        
        public static final int ACCEPTED = 2;
        
        public static final int REJECTED = 3;
      }
    }
    
    public class REQUEST {
      public static final String SPECIFIC_SURVEY = "0";
      
      public static final String FILLED_SURVEY = "1";
    }
    
    public class RC {
      public static final String SUCCESS = "00";
      
      public static final String USER_AUTH_FAILED = "01";
      
      public static final String USER_NOT_FOUND = "11";
      
      public static final String PIN_AUTH_FAILED = "12";
      
      public static final String PIN_MISMATCH = "13";
      
      public static final String MSISDN_ALREADY_EXIST = "14";
      
      public static final String USER_NOT_ACTIVE = "15";
      
      public static final String DATA_NOT_FOUND = "20";
      
      public static final String UNREGISTERED_ACCOUNT_NUMBER = "23";
      
      public static final String TIMEOUT = "50";
      
      public static final String OTP_FAILED = "97";
      
      public static final String AUTH_FAILED = "98";
      
      public static final String SERVICE_PATH_NOT_FOUND = "99";
      
      public static final String INTERNAL_ERROR = "IE";
      
      public static final String FAILED = "FA";
      
      public static final String PIN_BLOCKED = "17";
      
      public static final String FAILED_SAVE_DATA = "18";
      
      public static final String FAILED_SAVE_IMAGE = "22";
    }
  }
  
  public class WA {
    public static final String REQUEST_BODY = "REQUEST_BODY";
    
    public static final String REQUEST = "REQUEST";
    
    public static final String RESPONSE = "RESPONSE";
    
    public static final String IMAGE = "IMAGE";
    
    public static final String EXTENSION = "EXTENSION";
    
    public static final String ID_FORM = "ID_FORM";
    
    public static final String PATH = "PATH";
    
    public static final String MLOG = "MLOG";
    
    public static final String ABORTED_RESPONSE = "ABORTED_RESPONSE";
    
    public class STATUS {
      public static final String SUCCESS = "0";
      
      public static final String FAILED = "1";
      
      public class TRANSACTION {
        public static final int ON_GOING = 1;
        
        public static final int ACCEPTED = 2;
        
        public static final int DECLINE = 3;
      }
      
      public class APPROVAL {
        public static final int APPROVE = 1;
        
        public static final int WAITING_FOR_APPROVE = 2;
        
        public static final int REJECT = 3;
      }
      
      public class LOGIN {
        public static final int ACTIVE = 1;
        
        public static final int NONACTIVE = 0;
      }
    }
    
    public class RC {
      public static final String SUCCESS = "00";
      
      public static final String USER_AUTH_FAILED = "10";
      
      public static final String USER_NOT_FOUND = "11";
      
      public static final String PIN_AUTH_FAILED = "12";
      
      public static final String PIN_MISMATCH = "13";
      
      public static final String MSISDN_ALREADY_EXIST = "14";
      
      public static final String DATA_ALREADY_EXIST = "16";
      
      public static final String USER_NOT_ACTIVE = "15";
      
      public static final String DATA_NOT_FOUND = "20";
      
      public static final String DATA_INVALID = "21";
      
      public static final String TIMEOUT = "50";
      
      public static final String OTP_FAILED = "97";
      
      public static final String AUTH_FAILED = "98";
      
      public static final String SERVICE_PATH_NOT_FOUND = "99";
      
      public static final String INTERNAL_ERROR = "IE";
      
      public static final String FAILED = "FA";
    }
  }
  
  public class TRX {
    public static final String STATUS = "STATUS";
    
    public static final String RC = "RC";
    
    public static final String USER_DETAIL = "USER_DETAIL";
  }
  
  public class CONTROLLER {
    public static final String GROUP = "GROUP";
  }
  
  public static class DATA {
    public static final String USER = "USER";
  }
  
  public class ACCOUNT {
    public class STATUS_USER {
      public static final int INACTIVE = 0;
      
      public static final int ACTIVE = 1;
      
      public static final int BLOCKED = 2;
    }
    
    public class USER_DESC_STATUS {
      public static final String INACTIVE = "INACTIVE";
      
      public static final String ACTIVE = "ACTIVE";
      
      public static final String BLOCKED = "BLOCKED";
    }
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\la\\utility\Constants.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */