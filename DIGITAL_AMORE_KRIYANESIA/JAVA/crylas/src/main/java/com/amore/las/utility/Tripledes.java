package com.amore.las.utility;

import java.io.IOException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.jpos.util.Log;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Tripledes {
  Log log = Log.getLog("Q2", getClass().getName());
  
  private DESedeKeySpec desKeySpec;
  
  private IvParameterSpec ivSpec;
  
  public Tripledes(String key) {
    try {
      byte[] keyBytes = key.getBytes();
      this.desKeySpec = new DESedeKeySpec(keyBytes);
      this.ivSpec = new IvParameterSpec(key.substring(0, 8).getBytes());
    } catch (Exception e) {
      e.printStackTrace();
    } 
  }
  
  public String encrypt(String origData) {
    try {
      byte[] ori = origData.getBytes();
      SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
      SecretKey key = factory.generateSecret(this.desKeySpec);
      Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
      cipher.init(1, key, this.ivSpec);
      byte[] crypted = cipher.doFinal(ori);
      return base64Encode(crypted);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    } 
  }
  
  public String decrypt(String crypted) {
    try {
      byte[] crypt = base64Decode(crypted);
      SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
      SecretKey key = factory.generateSecret(this.desKeySpec);
      Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
      cipher.init(2, key, this.ivSpec);
      return new String(cipher.doFinal(crypt));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    } 
  }
  
  private static String base64Encode(byte[] data) {
    BASE64Encoder encoder = new BASE64Encoder();
    return encoder.encode(data);
  }
  
  private static byte[] base64Decode(String data) throws IOException {
    BASE64Decoder decoder = new BASE64Decoder();
    return decoder.decodeBuffer(data);
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\la\\utility\Tripledes.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */