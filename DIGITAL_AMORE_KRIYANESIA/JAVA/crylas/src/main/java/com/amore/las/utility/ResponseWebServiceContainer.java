package com.amore.las.utility;

import org.json.JSONObject;

public class ResponseWebServiceContainer {
  public final String RC = "rc";
  
  public final String RM = "rm";
  
  private JSONObject json;
  
  public ResponseWebServiceContainer() {}
  
  public ResponseWebServiceContainer(JSONObject jObjectResponse) {
    this.json = jObjectResponse;
  }
  
  public ResponseWebServiceContainer(String rc, String desc) {
    this.json = (new JSONObject()).put("rc", rc).put("rm", desc);
  }
  
  public ResponseWebServiceContainer(String rc, String desc, JSONObject another) {
    this.json = Utility.merge(new JSONObject[] { (new JSONObject()).put("rc", rc).put("rm", desc), another });
  }
  
  public JSONObject toJSONObject() {
    return this.json;
  }
  
  public String jsonToString() {
    return this.json.toString();
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\la\\utility\ResponseWebServiceContainer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */