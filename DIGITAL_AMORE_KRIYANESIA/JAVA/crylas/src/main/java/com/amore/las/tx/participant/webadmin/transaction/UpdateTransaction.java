package com.amore.las.tx.participant.webadmin.transaction;

import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateTransaction implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("status_transaction") && bodyData.has("surveyor") && bodyData.has("status_survey") && bodyData
        .has("id_trx") && bodyData.has("data_value") && bodyData.has("position") && bodyData
        .has("departemen") && bodyData.has("id_detail_detail") && bodyData.has("deleted_head") && bodyData
        .has("deleted_detail")) {
        String id_trx_detail = bodyData.getString("id_detail_detail");
        int status_transaction = bodyData.getInt("status_transaction");
        String surveyor = bodyData.getString("surveyor");
        String status_survey = bodyData.getString("status_survey");
        Long id_trx = Long.valueOf(bodyData.getLong("id_trx"));
        String data_value = bodyData.getString("data_value");
        String position = bodyData.getString("position");
        String departemen = bodyData.getString("departemen");
        Boolean deleted_head = Boolean.valueOf(bodyData.getBoolean("deleted_head"));
        Boolean deleted_detail = Boolean.valueOf(bodyData.getBoolean("deleted_detail"));
        MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(id_trx);
        if (mTransaction != null) {
          if (id_trx_detail.equals("") || id_trx_detail.equals(null) || id_trx_detail.equals("-")) {
            mTransaction.setStatus_transaction(status_transaction);
            mTransaction.setUsr_updated(mUserWeb);
            mTransaction.setDeleted(deleted_head);
            mTransaction = SpringInitializer.getmTransactionDao().saveOrUpdate(mTransaction);
            this.rc = SpringInitializer.getmResponseDao().getRc("00");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } else {
            Long idx = Long.valueOf(Long.parseLong(id_trx_detail));
            mTransaction.setStatus_transaction(status_transaction);
            mTransaction.setUsr_updated(mUserWeb);
            mTransaction.setDeleted(deleted_head);
            mTransaction = SpringInitializer.getmTransactionDao().saveOrUpdate(mTransaction);
            MTransactionDetail mTransactionDetail = SpringInitializer.getmTransactionDao().transactionDetailById(idx);
            if (mTransactionDetail != null) {
              mTransactionDetail.setId_head(mTransaction);
              mTransactionDetail.setPosition(position);
              mTransactionDetail.setData_value(data_value);
              mTransactionDetail.setDepartemen(departemen);
              mTransactionDetail.setSurveyor(surveyor);
              mTransactionDetail.setStatus_survey(status_survey);
              mTransactionDetail.setUsr_created(mUserWeb);
              mTransactionDetail.setDeleted(deleted_detail);
              SpringInitializer.getmTransactionDao().saveOrUpdateDetail(mTransactionDetail);
              this.rc = SpringInitializer.getmResponseDao().getRc("00");
              this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
              ctx.put("RESPONSE", this.wsResponse);
              ctx.put("RC", this.rc);
              ctx.put("STATUS", "0");
            } else {
              this.rc = SpringInitializer.getmResponseDao().getRc("20");
              this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
              ctx.put("RESPONSE", this.wsResponse);
              ctx.put("RC", this.rc);
              ctx.put("STATUS", "0");
            } 
          } 
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\transaction\UpdateTransaction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */