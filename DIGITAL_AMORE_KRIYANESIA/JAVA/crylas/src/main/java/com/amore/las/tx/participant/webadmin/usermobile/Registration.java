package com.amore.las.tx.participant.webadmin.usermobile;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import com.amore.las.utility.Tripledes;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class Registration implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("msisdn") && bodyData.has("account_tipe") && bodyData.has("address") && bodyData
        .has("cabang_kd_kantor") && bodyData.has("email") && bodyData.has("info") && bodyData
        .has("primary_name") && bodyData.has("pin")) {
        String msisdn = bodyData.getString("msisdn");
        String account_tipe = bodyData.getString("account_tipe");
        String address = bodyData.getString("address");
        int cabang_kd_kantor = bodyData.getInt("cabang_kd_kantor");
        String email = bodyData.getString("email");
        String info = bodyData.getString("info");
        String name = bodyData.getString("primary_name");
        String pin = bodyData.getString("pin");
        Tripledes tripledes = new Tripledes("L4SD1G1T4L4M0R3KR1Y4N3S14");
        MAgent mAgent = SpringInitializer.getmAgentDao().agentByMsisdn(msisdn);
        if (mAgent == null) {
          MAgent newMAgent = new MAgent();
          newMAgent.setMsisdn(msisdn);
          newMAgent.setAccount_tipe(account_tipe);
          newMAgent.setAddress(address);
          newMAgent.setCabang_kd_kantor(cabang_kd_kantor);
          newMAgent.setEmail(email);
          newMAgent.setImei("-");
          newMAgent.setInfo(info);
          newMAgent.setName(name);
          newMAgent.setNpwp("-");
          newMAgent.setPin(tripledes.encrypt(pin));
          newMAgent.setStatus_approval(0);
          newMAgent.setStatus_pin(Boolean.valueOf(true));
          newMAgent.setReset_pin(Boolean.valueOf(false));
          newMAgent.setStatus(1);
          newMAgent.setFail_login(0);
          newMAgent.setDeleted(Boolean.valueOf(false));
          newMAgent.setReset_msisdn(Boolean.valueOf(false));
          newMAgent.setUsr_created(mUserWeb);
          newMAgent = SpringInitializer.getmAgentDao().saveOrUpdate(newMAgent);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("14");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmi\\usermobile\Registration.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */