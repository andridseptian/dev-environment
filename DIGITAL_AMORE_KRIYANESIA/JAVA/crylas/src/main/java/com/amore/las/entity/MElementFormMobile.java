package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MElementFormMobile")
@Table(name = "m_element_form_mobile")
public class MElementFormMobile {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_element_form_mobile_seq")
  @SequenceGenerator(name = "id_element_form_mobile_seq", sequenceName = "id_element_form_mobile_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @ManyToOne
  @JoinColumn(name = "id_form_mobile", nullable = false)
  private MFormMobile id_form_mobile;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "label", nullable = false)
  private String label;
  
  @Column(name = "type", nullable = false)
  private String type;
  
  @Column(name = "input_type")
  private String input_type;
  
  @Column(name = "length")
  private int length;
  
  @Type(type = "text")
  @Column(name = "value")
  private String value;
  
  @Type(type = "text")
  @Column(name = "additional")
  private String additional;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Column(name = "deleted")
  private Boolean deleted;
  
  @Column(name = "position")
  private int position;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public MFormMobile getId_form_mobile() {
    return this.id_form_mobile;
  }
  
  public void setId_form_mobile(MFormMobile id_form_mobile) {
    this.id_form_mobile = id_form_mobile;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getLabel() {
    return this.label;
  }
  
  public void setLabel(String label) {
    this.label = label;
  }
  
  public String getType() {
    return this.type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getInput_type() {
    return this.input_type;
  }
  
  public void setInput_type(String input_type) {
    this.input_type = input_type;
  }
  
  public int getLength() {
    return this.length;
  }
  
  public void setLength(int length) {
    this.length = length;
  }
  
  public String getValue() {
    return this.value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  public String getAdditional() {
    return this.additional;
  }
  
  public void setAdditional(String additional) {
    this.additional = additional;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
  
  public int getPosition() {
    return this.position;
  }
  
  public void setPosition(int position) {
    this.position = position;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MElementFormMobile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */