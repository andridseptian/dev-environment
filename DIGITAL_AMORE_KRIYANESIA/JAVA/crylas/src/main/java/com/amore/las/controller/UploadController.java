package com.amore.las.controller;

import com.amore.http.HttpServletEntity;
import com.amore.las.entity.MLogWebAdminActivity;
import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = {"*"})
@RestController
public class UploadController {

    Log log = Log.getLog("Q2", getClass().getName());

    private ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();

    private MResponse mapRc = new MResponse();

    Context ctx = new Context();

    @RequestMapping(value = {"/crylas/upload/{service_type}"}, method = {RequestMethod.POST})
    public String getRequestMultipartAds(@RequestPart(name = "images", required = true) MultipartFile[] images, @RequestPart(name = "body") String body, @RequestPart(name = "id_form") String id_form, @RequestPart(name = "extension") String extension, @PathVariable("service_type") String path, HttpServletRequest request) {
        HttpServletEntity httpServletEntity = new HttpServletEntity(request);
        MLogWebAdminActivity mActivity = new MLogWebAdminActivity();
        mActivity.setRemoteAddr(httpServletEntity.getRemoteAddr());
        mActivity.setRequestUrl(httpServletEntity.getRequestUrl());
        mActivity.setRequestBody(body);
        mActivity.setRouterPath(path);
        mActivity.setRequestTime(new Date());
        mActivity = SpringInitializer.getmLogWebAdminActivityDao().saveOrUpdate(mActivity);
        this.log.info("Incoming from : " + mActivity.getRemoteAddr());
        this.log.info("Incoming Request Url : " + mActivity.getRequestUrl());
        this.log.info("Incoming Body: " + mActivity.getRequestBody());
        JSONObject reqBody = new JSONObject(mActivity.getRequestBody());
        try {
            Space sp = SpaceFactory.getSpace();
            this.ctx.put("PATH", mActivity.getRouterPath());
            this.ctx.put("REQUEST_BODY", reqBody);
            this.ctx.put("REQUEST", mActivity);
            this.ctx.put("IMAGE", images);
            this.ctx.put("EXTENSION", extension);
            this.ctx.put("ID_FORM", id_form);
            this.ctx.put("TRXMGR_UPLOAD", "upload-trx");
            this.ctx.put("TIME_OUT", Integer.valueOf(60000));
            sp.out("upload-trx", this.ctx, 60000L);
            Context response = (Context) sp.in(mActivity.getId(), 60000L);
            this.log.info("Insert request " + mActivity.getId() + " to space[" + "las-webadmin-trx" + "], timeout trx : " + '');
            this.log.info("Waiting " + mActivity.getId() + " from space");
            this.log.info("Get " + mActivity.getId() + " from space");
            if (response != null) {
                mActivity.setStatus(response.getString("STATUS"));
                this.mapRc = (MResponse) response.get("RC");
                this.wsResponse = (ResponseWebServiceContainer) response.get("RESPONSE");
            } else {
                mActivity.setStatus("1");
                this.mapRc = SpringInitializer.getmResponseDao().getRc("50");
                this.wsResponse = new ResponseWebServiceContainer(this.mapRc.getRc(), this.mapRc.getRm(), reqBody);
            }
        } catch (Exception ex) {
            this.log.error(ExceptionUtils.getStackTrace(ex));
            this.mapRc = SpringInitializer.getmResponseDao().getRc("99");
            this.wsResponse = new ResponseWebServiceContainer(this.mapRc.getRc(), this.mapRc.getRm(), reqBody);
        }
        mActivity.setResponseTime(new Date());
        mActivity.setResponse(this.wsResponse.jsonToString());
        mActivity.setResponseCode(this.mapRc.getRc());
        mActivity.setResponseMessage(this.mapRc.getRm());
        this.log.info("Outgoing to : " + request.getRemoteAddr());
        this.log.info("Outgoing Body: " + this.wsResponse.jsonToString());
        SpringInitializer.getmLogWebAdminActivityDao().saveOrUpdate(mActivity);
        return mActivity.getResponse();
    }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\controller\UploadController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */
