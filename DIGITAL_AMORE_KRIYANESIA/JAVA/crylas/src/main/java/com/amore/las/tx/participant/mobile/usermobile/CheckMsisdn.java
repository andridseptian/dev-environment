package com.amore.las.tx.participant.mobile.usermobile;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class CheckMsisdn implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  JSONObject data = new JSONObject();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    try {
      String msisdn = bodyData.getString("msisdn");
      MAgent mAgent = SpringInitializer.getmAgentDao().checkMsisdn(msisdn);
      if (mAgent != null) {
        this.data.put("name", mAgent.getName());
        if (mAgent.getStatus() == 1) {
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.data.put("status_user", "ACTIVE");
          bodyData.put("data", this.data);
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        } else {
          switch (mAgent.getStatus()) {
            case 0:
              this.rc = SpringInitializer.getmResponseDao().getRc("01");
              this.data.put("status_user", "INACTIVE");
              bodyData.put("data", this.data);
              this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
              break;
            case 2:
              this.rc = SpringInitializer.getmResponseDao().getRc("01");
              this.data.put("status_user", "BLOCKED");
              bodyData.put("data", this.data);
              this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
              break;
            default:
              this.rc = SpringInitializer.getmResponseDao().getRc("01");
              this.data.put("status_user", "INACTIVE");
              bodyData.put("data", this.data);
              this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
              break;
          } 
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("11");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      } 
    } catch (Exception e) {
      this.wsResponse = new ResponseWebServiceContainer();
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
    } 
    ctx.put("RESPONSE", this.wsResponse);
    ctx.put("RC", this.rc);
    ctx.put("STATUS", "0");
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobil\\usermobile\CheckMsisdn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */