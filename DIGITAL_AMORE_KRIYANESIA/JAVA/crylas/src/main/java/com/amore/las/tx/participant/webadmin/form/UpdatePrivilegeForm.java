package com.amore.las.tx.participant.webadmin.form;

import com.amore.las.entity.MForm;
import com.amore.las.entity.MPermissions;
import com.amore.las.entity.MPrivilegeForms;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdatePrivilegeForm implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("id") && bodyData.has("id_form") && bodyData.has("id_permission") && bodyData.has("read") && bodyData
        .has("write") && bodyData.has("deleted")) {
        Long id_privilege_form = Long.valueOf(bodyData.getLong("id"));
        Long id_form = Long.valueOf(bodyData.getLong("id_form"));
        Long id_permission = Long.valueOf(bodyData.getLong("id_permission"));
        Boolean read = Boolean.valueOf(bodyData.getBoolean("read"));
        Boolean write = Boolean.valueOf(bodyData.getBoolean("write"));
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        MPrivilegeForms mPrivilegeForms = SpringInitializer.getmPrivilegeFormsDao().prvilegeById(id_privilege_form);
        if (mPrivilegeForms != null) {
          MForm mForm = SpringInitializer.getmFormDao().formById(id_form);
          MPermissions mPermissions = SpringInitializer.getmPermissionsDao().permissionById(id_permission);
          mPrivilegeForms.setId(id_privilege_form);
          mPrivilegeForms.setId_form(mForm);
          mPrivilegeForms.setId_permission(mPermissions);
          mPrivilegeForms.setRead(read);
          mPrivilegeForms.setWrite(write);
          mPrivilegeForms.setUsr_updated(mUserWeb);
          mPrivilegeForms.setDeleted(deleted);
          mPrivilegeForms = SpringInitializer.getmPrivilegeFormsDao().saveOrUpdate(mPrivilegeForms);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\form\UpdatePrivilegeForm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */