package com.amore.las.tx.participant.webadmin.transaction;

import com.amore.las.entity.MResponse;
import com.amore.las.entity.MTransaction;
import com.amore.las.entity.MTransactionDetail;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetTransactionDetail implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    Long id_trx = Long.valueOf(bodyData.getLong("id_trx"));
    try {
      MTransaction mTransaction = SpringInitializer.getmTransactionDao().transactionById(id_trx);
      if (mTransaction != null) {
        List<MTransactionDetail> mTransactionDetails = SpringInitializer.getmTransactionDao().getTransactionDetail(mTransaction);
        int length = mTransactionDetails.size();
        if (length > 0) {
          JSONArray data = new JSONArray();
          for (MTransactionDetail mTransactionDetail : mTransactionDetails) {
            JSONObject logData = new JSONObject();
            logData.put("id", mTransactionDetail.getId());
            logData.put("data_value", mTransactionDetail.getData_value());
            logData.put("departemen", mTransactionDetail.getDepartemen());
            logData.put("position", mTransactionDetail.getPosition());
            logData.put("status_survey", mTransactionDetail.getStatus_survey());
            logData.put("surveyor", mTransactionDetail.getSurveyor());
            logData.put("id_head", mTransactionDetail.getId_head().getId());
            logData.put("created", mTransactionDetail.getDtm_created());
            logData.put("last_update", mTransactionDetail.getDtm_updated());
            data.put(logData);
          } 
          bodyData.put("logData", data);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("20");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\transaction\GetTransactionDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */