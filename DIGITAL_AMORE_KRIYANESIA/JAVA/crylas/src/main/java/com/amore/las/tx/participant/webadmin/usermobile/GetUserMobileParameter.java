package com.amore.las.tx.participant.webadmin.usermobile;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetUserMobileParameter implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      int page = bodyData.getInt("page");
      int pageSize = bodyData.getInt("pageSize");
      int offset = pageSize * page;
      String orderby = bodyData.getString("orderBy");
      String orderdirection = bodyData.getString("orderDirection");
      String query = "";
      JSONArray filter = bodyData.getJSONArray("filters");
      int length = filter.length();
      if (length > 0) {
        for (int i = 0; i < length; i++) {
          JSONObject data = filter.getJSONObject(i);
          query = query + "and ma." + data.getString("column") + " " + data.getString("operator") + " '" + data.getString("value") + "' ";
        } 
        List<MAgent> getCount = SpringInitializer.getmAgentDao().agentParameterCount(query);
        int count = getCount.size();
        query = query + "ORDER BY " + orderby + " " + orderdirection + "";
        List<MAgent> mAgent = SpringInitializer.getmAgentDao().agentParameter(query, pageSize, offset);
        if (mAgent != null) {
          JSONArray data = new JSONArray();
          for (MAgent mAgent1 : mAgent) {
            JSONObject logData = new JSONObject();
            logData.put("id", mAgent1.getId());
            logData.put("account_tipe", mAgent1.getAccount_tipe());
            logData.put("address", mAgent1.getAddress());
            logData.put("cabang_kd_kantor", mAgent1.getCabang_kd_kantor());
            logData.put("email", mAgent1.getEmail());
            logData.put("imei", mAgent1.getImei());
            logData.put("info", mAgent1.getInfo());
            logData.put("name", mAgent1.getName());
            logData.put("msisdn", mAgent1.getMsisdn());
            logData.put("pin", mAgent1.getPin());
            logData.put("status_pin", mAgent1.getStatus_pin());
            logData.put("fail_login", mAgent1.getFail_login());
            logData.put("last_login", mAgent1.getLast_login());
            if (mAgent1.getStatus() == 1) {
              logData.put("status", "ACTIVE");
            } else if (mAgent1.getStatus() == 0) {
              logData.put("status", "INACTIVE");
            } else {
              logData.put("status", "BLOCKED");
            } 
            data.put(logData);
          } 
          bodyData = new JSONObject();
          bodyData.put("logData", data);
          bodyData.put("totalCount", count);
          bodyData.put("page", page);
          bodyData.put("pageSize", pageSize);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("21");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmi\\usermobile\GetUserMobileParameter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */