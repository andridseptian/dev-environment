package com.amore.las.dao;

import com.amore.las.entity.MFormMobile;
import com.amore.las.entity.MMenuMobile;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MFormMobileDao")
@Transactional
public class MFormMobileDao extends Dao {
  public MFormMobile saveOrUpdate(MFormMobile mFormMobile) {
    Date date = new Date();
    if (mFormMobile.getId() == null) {
      mFormMobile.setDtm_created(date);
      this.em.persist(mFormMobile);
    } else {
      mFormMobile.setDtm_updated(date);
      this.em.merge(mFormMobile);
    } 
    return mFormMobile;
  }
  
  public MFormMobile byId(Long id) {
    try {
      return (MFormMobile)this.em.createQuery("SELECT mf FROM MFormMobile mf WHERE mf.id = :id and mf.deleted = false")
        .setParameter("id", id)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFormMobile> byIdMenu(MMenuMobile mMenuMobile) {
    try {
      return this.em.createQuery("SELECT mf FROM MFormMobile mf WHERE mf.id_menu_mobile = :mMenuMobile and mf.deleted = false order by mf.position asc")
        .setParameter("mMenuMobile", mMenuMobile)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFormMobile> byAccountType(String account_type) {
    try {
      return this.em.createNativeQuery("SELECT mf.id, mf.link_url, mf.header FROM m_form_mobile mf inner join m_menu_mobile mm on mm.id = mf.id_menu_mobile WHERE mm.account_type = :account_type and mf.deleted = false and mm.deleted = false")
        .setParameter("account_type", account_type)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MFormMobile byHeader(String header) {
    try {
      return (MFormMobile)this.em.createQuery("SELECT mf FROM MFormMobile mf WHERE mf.header = :header and mf.deleted = false")
        .setParameter("header", header)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFormMobile> allForm() {
    try {
      return this.em.createQuery("SELECT mf FROM MFormMobile mf WHERE mf.deleted = false ORDER BY mf.position")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFormMobile> formParam(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT me FROM MFormMobile me where me.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MFormMobile> formParamCount(String query) {
    try {
      return this.em.createQuery("SELECT me FROM MFormMobile me where me.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MFormMobileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */