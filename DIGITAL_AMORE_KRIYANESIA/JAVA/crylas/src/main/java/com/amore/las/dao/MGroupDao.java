package com.amore.las.dao;

import com.amore.las.entity.MGroup;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MGroupDao")
@Transactional
public class MGroupDao extends Dao {
  public MGroup saveOrUpdate(MGroup mGroup) {
    Date date = new Date();
    if (mGroup.getId() == null) {
      mGroup.setDtm_created(date);
      this.em.persist(mGroup);
    } else {
      mGroup.setDtm_updated(date);
      this.em.merge(mGroup);
    } 
    return mGroup;
  }
  
  public void remove(Long id) {
    MGroup mGroup = (MGroup)this.em.find(MGroup.class, id);
    this.em.remove(mGroup);
  }
  
  public MGroup groupById(Long id) {
    try {
      MGroup mg = (MGroup)this.em.createQuery("SELECT mg FROM MGroup mg WHERE mg.id = :id and mg.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return mg;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MGroup groupByName(String name) {
    try {
      MGroup mg = (MGroup)this.em.createQuery("SELECT mg FROM MGroup mg WHERE mg.name = :name and mg.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return mg;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MGroup> allGroup() {
    try {
      return this.em.createQuery("SELECT mg FROM MGroup mg where mg.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MGroup> groupParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT mg FROM MGroup mg where mg.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MGroup> groupParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT mg FROM MGroup mg where mg.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MGroupDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */