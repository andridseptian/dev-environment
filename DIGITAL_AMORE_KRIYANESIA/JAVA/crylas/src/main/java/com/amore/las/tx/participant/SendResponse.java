package com.amore.las.tx.participant;

import com.amore.las.entity.MLogMobileActivity;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;

public class SendResponse implements AbortParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepareForAbort(long id, Serializable context) {
    return 1;
  }
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    MLogMobileActivity activity = (MLogMobileActivity)ctx.get("REQUEST");
    ResponseWebServiceContainer responseWebServiceContainer = (ResponseWebServiceContainer)ctx.get("RESPONSE");
    Space space = SpaceFactory.getSpace();
    this.log.info("Request Id " + activity.getId());
    this.log.info("Response " + responseWebServiceContainer.jsonToString());
    space.out(activity.getId(), ctx, 60000L);
  }
  
  public void abort(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    MLogMobileActivity activity = (MLogMobileActivity)ctx.get("REQUEST");
    ResponseWebServiceContainer responseWebServiceContainer = (ResponseWebServiceContainer)ctx.get("RESPONSE");
    Space space = SpaceFactory.getSpace();
    this.log.info("Request Id " + activity.getId());
    this.log.info("Response " + responseWebServiceContainer.jsonToString());
    space.out(activity.getId(), ctx, 60000L);
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\SendResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */