package com.amore.las.dao;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MAgentTaked;
import com.amore.las.entity.MTransaction;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MAgentTakedDao")
@Transactional
public class MAgentTakedDao extends Dao {
  public MAgentTaked saveOrUpdate(MAgentTaked mAgent) {
    Date date = new Date();
    if (mAgent.getId() == null) {
      mAgent.setDtm_created(date);
      this.em.persist(mAgent);
    } else {
      mAgent.setDtm_updated(date);
      this.em.merge(mAgent);
    } 
    return mAgent;
  }
  
  public MAgentTaked takedbyId(Long id) {
    try {
      MAgentTaked ma = (MAgentTaked)this.em.createQuery("SELECT ma FROM MAgentTaked ma WHERE ma.id = :id and ma.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MAgentTaked byIdTrxAndAccountTipe(MTransaction id, String account_tipe) {
    try {
      MAgentTaked ma = (MAgentTaked)this.em.createQuery("SELECT ma FROM MAgentTaked ma WHERE ma.id_head_trx = :id and ma.account_tipe = :account_tipe and ma.deleted = false").setParameter("id", id).setParameter("account_tipe", account_tipe).setMaxResults(1).getSingleResult();
      return ma;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MAgentTaked> takedbyIdTrx(MTransaction mTransaction) {
    try {
      return this.em.createQuery("SELECT ma FROM MAgentTaked ma WHERE ma.id_trx_web = :mTransaction and ma.deleted = false")
        .setParameter("mTransaction", mTransaction)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MAgentTaked> byUser(MAgent mAgent) {
    try {
      return this.em.createQuery("SELECT ma FROM MAgentTaked ma inner join ma.id_head_trx md WHERE ma.usr_created = :mAgent and ma.deleted = false and ma.status = :status")
        .setParameter("mAgent", mAgent)
        .setParameter("status", Integer.valueOf(0))
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MAgentTaked remove(MAgentTaked mAgentTaked) {
    this.em.remove(this.em.merge(mAgentTaked));
    return mAgentTaked;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MAgentTakedDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */