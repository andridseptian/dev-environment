package com.amore.las.utility;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class HTTPQueryHost {
  public static String httprequest(String url, String body) {
    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
      HttpPost request = new HttpPost(url);
      StringEntity params = new StringEntity(body);
      request.addHeader("content-type", "application/json");
      request.setEntity((HttpEntity)params);
      CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest)request);
      String json = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");
      return json;
    } catch (IOException ex) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\la\\utility\HTTPQueryHost.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */