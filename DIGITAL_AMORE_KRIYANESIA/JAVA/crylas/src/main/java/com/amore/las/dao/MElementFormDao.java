package com.amore.las.dao;

import com.amore.las.entity.MElementForm;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("MElementFormDao")
@Transactional
public class MElementFormDao extends Dao {
  public MElementForm saveOrUpdate(MElementForm mElementForm) {
    Date date = new Date();
    if (mElementForm.getId() == null) {
      mElementForm.setDtm_created(date);
      this.em.persist(mElementForm);
    } else {
      mElementForm.setDtm_updated(date);
      this.em.merge(mElementForm);
    } 
    return mElementForm;
  }
  
  public MElementForm elementById(Long id) {
    try {
      MElementForm me = (MElementForm)this.em.createQuery("SELECT me FROM MElementForm me WHERE me.id = :id and me.deleted = false").setParameter("id", id).setMaxResults(1).getSingleResult();
      return me;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public MElementForm elementByName(String name) {
    try {
      MElementForm me = (MElementForm)this.em.createQuery("SELECT me FROM MElementForm me WHERE me.name = :name and me.deleted = false").setParameter("name", name).setMaxResults(1).getSingleResult();
      return me;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementForm> allElement() {
    try {
      return this.em.createQuery("SELECT me FROM MElementForm me where me.deleted = false")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementForm> elementParameter(String query, int limit, int offset) {
    try {
      return this.em.createQuery("SELECT me FROM MElementForm me where me.deleted = false " + query + "")
        .setMaxResults(limit)
        .setFirstResult(offset)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<MElementForm> elementParameterCount(String query) {
    try {
      return this.em.createQuery("SELECT me FROM MElementForm me where me.deleted = false " + query + "")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\dao\MElementFormDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */