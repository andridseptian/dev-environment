package com.amore.las.tx.participant.webadmin.rules;

import com.amore.las.entity.MPermissions;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetPermissionParameter implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      int page = bodyData.getInt("page");
      int pageSize = bodyData.getInt("pageSize");
      int offset = pageSize * page;
      String orderby = bodyData.getString("orderBy");
      String orderdirection = bodyData.getString("orderDirection");
      String query = "";
      JSONArray filter = bodyData.getJSONArray("filters");
      int length = filter.length();
      if (length > 0) {
        for (int i = 0; i < length; i++) {
          JSONObject data = filter.getJSONObject(i);
          query = query + "and mp." + data.getString("column") + " " + data.getString("operator") + " '" + data.getString("value") + "' ";
        } 
        List<MPermissions> getCount = SpringInitializer.getmPermissionsDao().permissionParameterCount(query);
        int count = getCount.size();
        query = query + "ORDER BY " + orderby + " " + orderdirection + "";
        List<MPermissions> mPermissions = SpringInitializer.getmPermissionsDao().permissionParameter(query, pageSize, offset);
        if (mPermissions != null) {
          JSONArray data = new JSONArray();
          for (MPermissions mPermissions1 : mPermissions) {
            JSONObject logData = new JSONObject();
            logData.put("id", mPermissions1.getId());
            logData.put("name", mPermissions1.getName());
            logData.put("preference_code", mPermissions1.getPreference_code());
            logData.put("definition", mPermissions1.getDefinition());
            data.put(logData);
          } 
          bodyData = new JSONObject();
          bodyData.put("logData", data);
          bodyData.put("totalCount", count);
          bodyData.put("page", page);
          bodyData.put("pageSize", pageSize);
          this.rc = SpringInitializer.getmResponseDao().getRc("00");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("21");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\rules\GetPermissionParameter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */