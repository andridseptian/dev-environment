package com.amore.las.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity(name = "MAgent")
@Table(name = "m_agent")
public class MAgent {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_agent_seq")
  @SequenceGenerator(name = "id_agent_seq", sequenceName = "id_agent_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "address")
  private String address;
  
  @Column(name = "imei")
  private String imei;
  
  @Column(name = "info")
  private String info;
  
  @Column(name = "msisdn", nullable = false)
  private String msisdn;
  
  @Column(name = "name", nullable = false)
  private String name;
  
  @Column(name = "pin", nullable = false)
  private String pin;
  
  @Column(name = "status_pin")
  private Boolean status_pin;
  
  @Column(name = "status", nullable = false)
  private int status;
  
  @Column(name = "cabang_kd_kantor", nullable = false)
  private int cabang_kd_kantor;
  
  @Column(name = "fail_login", nullable = false)
  private int fail_login;
  
  @Column(name = "reset_pin")
  private Boolean reset_pin;
  
  @Column(name = "status_approval")
  private int status_approval;
  
  @Column(name = "account_tipe", nullable = false)
  private String account_tipe;
  
  @Column(name = "npwp", nullable = false)
  private String npwp;
  
  @Type(type = "text")
  @Column(name = "fcm_token")
  private String fcm_token;
  
  @Column(name = "reset_msisdn")
  private Boolean reset_msisdn;
  
  @Column(name = "email", nullable = false)
  private String email;
  
  @Column(name = "last_login")
  @Temporal(TemporalType.TIMESTAMP)
  private Date last_login;
  
  @Column(name = "dtm_created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_created;
  
  @Column(name = "dtm_updated")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm_updated;
  
  @ManyToOne
  @JoinColumn(name = "usr_created", nullable = false)
  private MUserWeb usr_created;
  
  @ManyToOne
  @JoinColumn(name = "usr_updated")
  private MUserWeb usr_updated;
  
  @Type(type = "text")
  @Column(name = "additional_data_1")
  private String additional_data_1;
  
  @Type(type = "text")
  @Column(name = "additional_data_2")
  private String additional_data_2;
  
  @Column(name = "deleted", nullable = false)
  private Boolean deleted;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getAddress() {
    return this.address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  public String getImei() {
    return this.imei;
  }
  
  public void setImei(String imei) {
    this.imei = imei;
  }
  
  public String getInfo() {
    return this.info;
  }
  
  public void setInfo(String info) {
    this.info = info;
  }
  
  public String getMsisdn() {
    return this.msisdn;
  }
  
  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getPin() {
    return this.pin;
  }
  
  public void setPin(String pin) {
    this.pin = pin;
  }
  
  public Boolean getStatus_pin() {
    return this.status_pin;
  }
  
  public void setStatus_pin(Boolean status_pin) {
    this.status_pin = status_pin;
  }
  
  public int getStatus() {
    return this.status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  public int getCabang_kd_kantor() {
    return this.cabang_kd_kantor;
  }
  
  public void setCabang_kd_kantor(int cabang_kd_kantor) {
    this.cabang_kd_kantor = cabang_kd_kantor;
  }
  
  public int getFail_login() {
    return this.fail_login;
  }
  
  public void setFail_login(int fail_login) {
    this.fail_login = fail_login;
  }
  
  public Boolean getReset_pin() {
    return this.reset_pin;
  }
  
  public void setReset_pin(Boolean reset_pin) {
    this.reset_pin = reset_pin;
  }
  
  public int getStatus_approval() {
    return this.status_approval;
  }
  
  public void setStatus_approval(int status_approval) {
    this.status_approval = status_approval;
  }
  
  public String getAccount_tipe() {
    return this.account_tipe;
  }
  
  public void setAccount_tipe(String account_tipe) {
    this.account_tipe = account_tipe;
  }
  
  public String getNpwp() {
    return this.npwp;
  }
  
  public void setNpwp(String npwp) {
    this.npwp = npwp;
  }
  
  public String getFcm_token() {
    return this.fcm_token;
  }
  
  public void setFcm_token(String fcm_token) {
    this.fcm_token = fcm_token;
  }
  
  public Boolean getReset_msisdn() {
    return this.reset_msisdn;
  }
  
  public void setReset_msisdn(Boolean reset_msisdn) {
    this.reset_msisdn = reset_msisdn;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public Date getLast_login() {
    return this.last_login;
  }
  
  public void setLast_login(Date last_login) {
    this.last_login = last_login;
  }
  
  public Date getDtm_created() {
    return this.dtm_created;
  }
  
  public void setDtm_created(Date dtm_created) {
    this.dtm_created = dtm_created;
  }
  
  public Date getDtm_updated() {
    return this.dtm_updated;
  }
  
  public void setDtm_updated(Date dtm_updated) {
    this.dtm_updated = dtm_updated;
  }
  
  public MUserWeb getUsr_created() {
    return this.usr_created;
  }
  
  public void setUsr_created(MUserWeb usr_created) {
    this.usr_created = usr_created;
  }
  
  public MUserWeb getUsr_updated() {
    return this.usr_updated;
  }
  
  public void setUsr_updated(MUserWeb usr_updated) {
    this.usr_updated = usr_updated;
  }
  
  public String getAdditional_data_1() {
    return this.additional_data_1;
  }
  
  public void setAdditional_data_1(String additional_data_1) {
    this.additional_data_1 = additional_data_1;
  }
  
  public String getAdditional_data_2() {
    return this.additional_data_2;
  }
  
  public void setAdditional_data_2(String additional_data_2) {
    this.additional_data_2 = additional_data_2;
  }
  
  public Boolean getDeleted() {
    return this.deleted;
  }
  
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\entity\MAgent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */