package com.amore.las.tx.participant.webadmin.master;

import com.amore.las.entity.MKaryawan;
import com.amore.las.entity.MResponse;
import com.amore.las.entity.MUserWeb;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

public class UpdateKaryawan implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MUserWeb mUserWeb = (MUserWeb)ctx.get("USER_DETAIL");
    try {
      if (bodyData.has("nama") && bodyData.has("jabatan") && bodyData.has("cabang") && bodyData
        .has("nik_karyawan") && bodyData.has("id") && bodyData.has("deleted")) {
        Long id_karyawan = Long.valueOf(bodyData.getLong("id"));
        String name = bodyData.getString("nama");
        String position = bodyData.getString("jabatan");
        String cabang = bodyData.getString("cabang");
        String nik_karyawan = bodyData.getString("nik_karyawan");
        Boolean deleted = Boolean.valueOf(bodyData.getBoolean("deleted"));
        MKaryawan mKaryawan = SpringInitializer.getmKaryawanDao().byId(id_karyawan);
        if (mKaryawan != null) {
          MKaryawan mKaryawan1 = SpringInitializer.getmKaryawanDao().byName(name);
          if (mKaryawan1 == null || mKaryawan1.getName().equals(mKaryawan.getName())) {
            mKaryawan.setName(name);
            mKaryawan.setPosition(position);
            mKaryawan.setCabang(cabang);
            mKaryawan.setNik_karyawan(nik_karyawan);
            mKaryawan.setUsr_updated(mUserWeb);
            mKaryawan.setDeleted(deleted);
            mKaryawan = SpringInitializer.getmKaryawanDao().saveOrUpdate(mKaryawan);
            this.rc = SpringInitializer.getmResponseDao().getRc("00");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } else {
            this.rc = SpringInitializer.getmResponseDao().getRc("16");
            this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
            ctx.put("RESPONSE", this.wsResponse);
            ctx.put("RC", this.rc);
            ctx.put("STATUS", "0");
          } 
        } else {
          this.rc = SpringInitializer.getmResponseDao().getRc("20");
          this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
          ctx.put("RESPONSE", this.wsResponse);
          ctx.put("RC", this.rc);
          ctx.put("STATUS", "0");
        } 
      } else {
        this.rc = SpringInitializer.getmResponseDao().getRc("FA");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.log.info(ExceptionUtils.getStackTrace(e));
      this.rc = SpringInitializer.getmResponseDao().getRc("IE");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\webadmin\master\UpdateKaryawan.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */