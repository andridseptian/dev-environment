package com.amore.las.tx.participant.mobile.transaction;

import com.amore.las.entity.MAgent;
import com.amore.las.entity.MImageMob;
import com.amore.las.entity.MResponse;
import com.amore.las.spring.SpringInitializer;
import com.amore.las.utility.ResponseWebServiceContainer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public class UploadImageMob implements TransactionParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
  
  JSONObject data = new JSONObject();
  
  MResponse rc = new MResponse();
  
  public int prepare(long id, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long id, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    JSONObject bodyData = (JSONObject)ctx.get("REQUEST_BODY");
    MultipartFile bodyImage = (MultipartFile)ctx.get("IMAGE");
    MAgent mAgent = (MAgent)ctx.get("USER_DETAIL");
    try {
      StringBuilder sb = new StringBuilder();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss.SSS");
      String url = null;
      Date date = new Date();
      String now = sdf.format(date);
      String file_name = mAgent.getId() + "-" + bodyData.getString("id_form") + "-" + now + ".jpg";
      File outputfile = new File(file_name);
      url = outputfile.toString().replace("/opt/crylas/webadmin", "");
      outputfile.createNewFile();
      FileOutputStream stream = new FileOutputStream(outputfile);
      stream.write(bodyImage.getBytes());
      stream.flush();
      stream.close();
      sb.append(file_name);
      try {
        MImageMob mImageMob = new MImageMob();
        mImageMob.setDeleted(Boolean.valueOf(false));
        mImageMob.setId_element_form(Integer.parseInt(bodyData.getString("id_element_form")));
        mImageMob.setLink_url(url);
        mImageMob.setLatitude(bodyData.getString("latitude"));
        mImageMob.setLongitude(bodyData.getString("longitude"));
        mImageMob.setId_transaction_mob(SpringInitializer.getmTransactionMobileDao().byId(Long.valueOf(bodyData.getString("id_form"))));
        mImageMob.setUsr_created(mAgent);
        mImageMob = SpringInitializer.getmImageMobDao().saveOrUpdate(mImageMob);
        this.rc = SpringInitializer.getmResponseDao().getRc("00");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } catch (Exception e) {
        this.rc = SpringInitializer.getmResponseDao().getRc("22");
        this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
        ctx.put("RESPONSE", this.wsResponse);
        ctx.put("RC", this.rc);
        ctx.put("STATUS", "0");
      } 
    } catch (Exception e) {
      this.rc = SpringInitializer.getmResponseDao().getRc("22");
      this.wsResponse = new ResponseWebServiceContainer(this.rc.getRc(), this.rc.getRm(), bodyData);
      ctx.put("RESPONSE", this.wsResponse);
      ctx.put("RC", this.rc);
      ctx.put("STATUS", "0");
    } 
  }
  
  public void abort(long id, Serializable srlzbl) {}
}


/* Location:              H:\USER\Documents\MobaXterm\slash\RemoteFiles\68202_4_2\MIDWARELAS-1.0.jar!\com\amore\las\tx\participant\mobile\transaction\UploadImageMob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */