/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.dao;

import com.dak.lpcoresim.model.LogActivity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "LogActivityDao")
@Transactional
public class LogActivityDao extends Dao {

    public LogActivity saveOrUpdate(LogActivity logact) {
        if (logact.getId() == null) {
            em.persist(logact);
            System.out.println("persist / saved");
        } else {
            em.merge(logact);
            System.out.println("Merge / updated");
        }
        return logact;
    }
}
