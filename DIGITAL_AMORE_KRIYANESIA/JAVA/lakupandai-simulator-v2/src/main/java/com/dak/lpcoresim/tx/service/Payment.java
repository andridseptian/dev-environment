/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service;

import com.dak.lpcoresim.model.Agent;
import com.dak.lpcoresim.model.Nasabah;
import com.dak.lpcoresim.model.Product;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class Payment implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());
    LogEvent le = new LogEvent();

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);
        LogEvent le = new LogEvent();
        le.addMessage("--Payment Product--");

        try {
            le.addMessage("REQUEST INPUT: \n" + input);

            String sourceAccountNumber = input.getString("sourceAccountNumber");
            String productCode = 0 + input.getString("productCode").substring(1);
            String idBilling = input.getString("idBilling");
            String transactionType = input.getString("transactionType");
            String fee = input.getString("fee");
            String agentFee = input.getString("agentFee");
            String tax = input.getString("tax");
            String accountType = input.getString("accountType");
            String transactionAmount = input.getString("transactionAmount");

            Random rnd = new Random();
            int n = 100000 + rnd.nextInt(900000);
//            String pattern = "yyyyMMddHHmmss";
            String pattern = "ssHHmmMMddyyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String jobIdInquiry = n + simpleDateFormat.format(new Date());
            String rrn = simpleDateFormat.format(new Date());

            String Output = "";
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMsg = factory.createMessage();
            soapMsg.getSOAPHeader().detachNode();
            SOAPPart part = soapMsg.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody bodySoap = envelope.getBody();

            SOAPBodyElement element = bodySoap.addBodyElement(envelope.createName("AgentInquiryResponse", "ns1", "urn:AgentInquiry"));
            SOAPElement el = element.addChildElement("ServiceResponse");

            Product product = null;
            String additionalData = null;
//            if (productCode.equals("010002")
//                    || productCode.equals("013002")
//                    || productCode.equals("011002")
//                    || productCode.equals("011003")
//                    || productCode.equals("010006")) {

            le.addMessage("searching for product...");
            le.addMessage("idBilling: " + idBilling + " transactionType: " + transactionType + " productCode: " + productCode);
            if (transactionType.equals("3")) {
                le.addMessage("transaction equals 3");
                le.addMessage("findAgentByIdBillingAndTransactionTypeAndProductCode");
                product = SpringIn.getProductDao().findAgentByProductCodeAndTransactionTypeAndBilling(productCode, transactionType, idBilling);
                if (product != null) {
                    additionalData = product.getAdditionalData()
                            .replace("[MSISDN]", idBilling)
                            .replace("[AMOUNT]", transactionAmount)
                            .replace("[REF]", rrn);
                } else {
                    le.addMessage("findAgentByProductCodeAndTransactionType");
                    product = SpringIn.getProductDao().findAgentByProductCodeAndTransactionType(productCode, transactionType);
                    if (product != null) {
                        additionalData = product.getAdditionalData()
                                .replace("[MSISDN]", idBilling)
                                .replace("[AMOUNT]", transactionAmount)
                                .replace("[REF]", rrn);
                    } else {
                        le.addMessage("product not found");
                    }
                }
            } else {
                le.addMessage("transaction equals else");
                le.addMessage("findAgentByIdBillingAndTransactionTypeAndProductCode");
                product = SpringIn.getProductDao().findAgentByIdBillingAndTransactionTypeAndProductCode(idBilling, transactionType, productCode);
                if (product != null) {
                    additionalData = product.getAdditionalData();
                    transactionAmount = product.getTransactionAmount();
                } else {
                    le.addMessage("findAgentByProductCodeAndTransactionType");
                    product = SpringIn.getProductDao().findAgentByProductCodeAndTransactionType(idBilling, transactionType);
                    if (product != null) {
                        additionalData = product.getAdditionalData();
                        transactionAmount = product.getTransactionAmount();
                    } else {
                        le.addMessage("product not found");
                    }
                }
            }
            if (product != null) {
                le.addMessage("product not null");
                if (accountType.equals("A")) {
                    Agent agent = SpringIn.getAgentDao().findAgentByAccountNumber(sourceAccountNumber);
                    agent.setSaldo(agent.getSaldo() - Long.parseLong(product.getTransactionAmount() + "00"));
                    SpringIn.getAgentDao().saveOrUpdate(agent);
                } else {
                    Nasabah nasabah = SpringIn.getNasabahDao().findNasabahByAccountNumber(sourceAccountNumber);

                    nasabah.setSaldo(nasabah.getSaldo() - Long.parseLong(product.getTransactionAmount() + "00"));
                    SpringIn.getNasabahDao().saveOrUpdate(nasabah);
                }
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(product.getResponseCode());
                el.addChildElement("responseDesc").addTextNode(product.getResponseDesc());
                el.addChildElement("sourceAccountNumber").addTextNode(sourceAccountNumber);
                el.addChildElement("productCode").addTextNode(productCode);
                el.addChildElement("idBilling").addTextNode(idBilling);
                el.addChildElement("transactionAmount").addTextNode(transactionAmount);
                el.addChildElement("fee").addTextNode(fee);
                el.addChildElement("agentFee").addTextNode(agentFee);
                el.addChildElement("tax").addTextNode(tax);
                el.addChildElement("additionalData").addTextNode(additionalData);
                el.addChildElement("transactionType").addTextNode(transactionType);
                el.addChildElement("accountType").addTextNode(accountType);
                el.addChildElement("jobIdInquiry").addTextNode(jobIdInquiry);
                el.addChildElement("rrn").addTextNode(rrn);
            } else {
                le.addMessage("product is null");
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.GAGAL);
                el.addChildElement("responseDesc").addTextNode("TAGIHAN TIDAK ADA");
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMsg.writeTo(out);
            String strMsg = new String(out.toByteArray());
            Output = strMsg;

            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }

        log.info(le);
        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
