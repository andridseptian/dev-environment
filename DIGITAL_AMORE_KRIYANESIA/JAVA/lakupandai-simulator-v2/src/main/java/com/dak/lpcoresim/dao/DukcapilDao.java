/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.dao;

import com.dak.lpcoresim.model.Dukcapil;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "DukcapilDao")
@Transactional
public class DukcapilDao extends Dao {
    
    public Dukcapil findDukcapilByNik(String nik){
        try {
            Dukcapil userdat = (Dukcapil) em.createQuery("SELECT a FROM Dukcapil a WHERE a.Nik = :nik")
                    .setParameter("nik", nik)
                    .getSingleResult();
            return userdat;
        } catch (NoResultException e) {
            return null;
        }
    }
}
