/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author Andri D Septian
 */
@Entity(name = "Product")
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_seq")
    @SequenceGenerator(name = "id_seq", sequenceName = "id_seq")
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name = "id_billing", nullable = false)
    private String IdBilling;

    @Column(name = "product_code", nullable = false)
    private String ProductCode;

    @Column(name = "product_name")
    private String ProductName;

    @Column(name = "transaction_amount")
    private String TransactionAmount;

    @Column(name = "additional_data")
    @Type(type = "text")
    private String AdditionalData;

    @Column(name = "transaction_type")
    private String TransactionType;
    
    @Column(name = "response_code")
    private String ResponseCode;
    
    @Column(name = "response_desc")
    private String ResponseDesc;

    public String getIdBilling() {
        return IdBilling;
    }

    public void setIdBilling(String IdBilling) {
        this.IdBilling = IdBilling;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String ProductCode) {
        this.ProductCode = ProductCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public String getTransactionAmount() {
        return TransactionAmount;
    }

    public void setTransactionAmount(String TransactionAmount) {
        this.TransactionAmount = TransactionAmount;
    }

    public String getAdditionalData() {
        return AdditionalData;
    }

    public void setAdditionalData(String AdditionalData) {
        this.AdditionalData = AdditionalData;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String TransactionType) {
        this.TransactionType = TransactionType;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String ResponseCode) {
        this.ResponseCode = ResponseCode;
    }

    public String getResponseDesc() {
        return ResponseDesc;
    }

    public void setResponseDesc(String ResponseDesc) {
        this.ResponseDesc = ResponseDesc;
    }
    
    

}
