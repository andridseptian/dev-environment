/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.dao;

import com.dak.lpcoresim.model.Agent;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "AgentDao")
@Transactional
public class AgentDao extends Dao {

    public Agent saveOrUpdate(Agent agent) {
        if (agent.getId() == null) {
            em.persist(agent);
            System.out.println("persist / saved");
        } else {
            em.merge(agent);
            System.out.println("Merge / updated");
        }
        return agent;
    }
    
    public Agent findAgentById(String id){
        try {
            Agent userdat = (Agent) em.createQuery("SELECT a FROM Agent a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return userdat;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Agent findAgentByPhoneNumber(String PhoneNumber){
        try {
            Agent agent = (Agent) em.createQuery("SELECT a FROM Agent a WHERE a.phoneNumber = :phoneNumber")
                    .setParameter("phoneNumber", PhoneNumber)
                    .getSingleResult();
            return agent;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Agent findAgentByAccountNumber(String AccountNumber){
        try {
            Agent agent = (Agent) em.createQuery("SELECT a FROM Agent a WHERE a.accountNumber = :AccountNumber")
                    .setParameter("AccountNumber", AccountNumber)
                    .getSingleResult();
            return agent;
        } catch (NoResultException e) {
            return null;
        }
    }
}
