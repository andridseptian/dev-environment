/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.dao;

import com.dak.lpcoresim.model.Nasabah;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "NasabahDao")
@Transactional
public class NasabahDao extends Dao {
    public Nasabah saveOrUpdate(Nasabah nasabah) {
        if (nasabah.getId() == null) {
            em.persist(nasabah);
            System.out.println("persist / saved");
        } else {
            em.merge(nasabah);
            System.out.println("Merge / updated");
        }
        return nasabah;
    }
    
    public Nasabah findNasabahById(String id){
        try {
            Nasabah userdat = (Nasabah) em.createQuery("SELECT a FROM Nasabah a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return userdat;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Nasabah findNasabahByNik(String nik){
        try {
            Nasabah userdat = (Nasabah) em.createQuery("SELECT a FROM Nasabah a WHERE a.nik = :nik")
                    .setParameter("nik", nik)
                    .getSingleResult();
            return userdat;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Nasabah findNasabahByPhoneNumber(String PhoneNumber){
        try {
            Nasabah nasabah = (Nasabah) em.createQuery("SELECT a FROM Nasabah a WHERE a.phoneNumber = :phoneNumber")
                    .setParameter("phoneNumber", PhoneNumber)
                    .getSingleResult();
            return nasabah;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Nasabah findNasabahByAccountNumber(String AccountNumber){
        try {
            Nasabah nasabah = (Nasabah) em.createQuery("SELECT a FROM Nasabah a WHERE a.accountNumber = :AccountNumber")
                    .setParameter("AccountNumber", AccountNumber)
                    .getSingleResult();
            return nasabah;
        } catch (NoResultException e) {
            return null;
        }
    }
}
