/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service;

import com.dak.lpcoresim.model.Agent;
import com.dak.lpcoresim.model.Nasabah;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class BalanceInquiry implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);

        try {
            log.info("REQUEST INPUT: \n" + input);

            String Output = "";
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMsg = factory.createMessage();
            soapMsg.getSOAPHeader().detachNode();
            SOAPPart part = soapMsg.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody bodySoap = envelope.getBody();

            SOAPBodyElement element = bodySoap.addBodyElement(envelope.createName("BalanceInquiryResponse", "ns1", "urn:BalanceInquiry"));
            SOAPElement el = element.addChildElement("ServiceResponse");

            Agent agent = SpringIn.getAgentDao().findAgentByAccountNumber(input.getString(Constants.INPUT.PHONE_NUMBER));
            if (agent == null) {
                agent = SpringIn.getAgentDao().findAgentByPhoneNumber(input.getString(Constants.INPUT.PHONE_NUMBER));
            }
            Nasabah nasabah = SpringIn.getNasabahDao().findNasabahByPhoneNumber(input.getString(Constants.INPUT.PHONE_NUMBER));
            if (nasabah == null) {
                log.info("nasabah null");
                nasabah = SpringIn.getNasabahDao().findNasabahByAccountNumber(input.getString(Constants.INPUT.PHONE_NUMBER));
            }

            if (agent != null) {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
                el.addChildElement("phoneNumber").addTextNode(agent.getPhoneNumber());
                el.addChildElement("accountName").addTextNode(agent.getName());
                el.addChildElement("balance").addTextNode(String.valueOf(agent.getSaldo()));

            } else if (nasabah != null) {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
                el.addChildElement("phoneNumber").addTextNode(nasabah.getPhoneNumber());
                el.addChildElement("accountName").addTextNode(nasabah.getName());
                el.addChildElement("balance").addTextNode(String.valueOf(nasabah.getSaldo()));
            } else {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.GAGAL);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.GAGAL);
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMsg.writeTo(out);
            String strMsg = new String(out.toByteArray());
            Output = strMsg;

            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);

        } catch (SOAPException e) {
            log.error(e);
            e.printStackTrace();
        } catch (IOException ex) {
            log.error(ex);
            ex.printStackTrace();
        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
