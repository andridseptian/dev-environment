/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service;

import com.dak.lpcoresim.model.Agent;
import com.dak.lpcoresim.model.Nasabah;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Random;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author dakandri
 */
public class AccountTransaction implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);

        try {
            log.info("REQUEST INPUT: \n" + input);

            String type = input.getString("type");
            String sourceAccountNumber = input.getString("sourceAccountNumber");
            String destinationAccountNumber = input.getString("destinationAccountNumber");
            String descriptionTransaction = input.getString("descriptionTransaction");
            String InputreferenceNumber = input.getString("referenceNumber");
            Long transactionAmount = Long.parseLong(input.getString("transactionAmount"));

            Random rnd = new Random();
            int n = 100000 + rnd.nextInt(900000);
            String referenceNumber = String.valueOf(n);
            String destinationAccountName = "";

            String Output = "";
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMsg = factory.createMessage();
            soapMsg.getSOAPHeader().detachNode();
            SOAPPart part = soapMsg.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody bodySoap = envelope.getBody();

            SOAPBodyElement element = bodySoap.addBodyElement(envelope.createName("AccountTransactionResponse", "ns1", "urn:AccountTransaction"));
            SOAPElement el = element.addChildElement("ServiceResponse");

            if (type.equals("3")) { // Setor
                Agent agent = SpringIn.getAgentDao().findAgentByAccountNumber(sourceAccountNumber);
                Nasabah nasabah = SpringIn.getNasabahDao().findNasabahByPhoneNumber(destinationAccountNumber);

                log.info("Setor tunai :\n"
                        + agent.getName() + ">>>" + nasabah.getName()
                        + "\nJumlah: " + transactionAmount);

                log.info("Saldo Agent : " + agent.getSaldo()
                        + "\nSaldo Nasabah : " + nasabah.getSaldo());
                agent.setSaldo(agent.getSaldo() - transactionAmount);
                nasabah.setSaldo(nasabah.getSaldo() + transactionAmount);

                SpringIn.getAgentDao().saveOrUpdate(agent);
                SpringIn.getNasabahDao().saveOrUpdate(nasabah);
            } else if (type.equals("2")) { // Tarik
                Agent agent = SpringIn.getAgentDao().findAgentByAccountNumber(destinationAccountNumber);
                Nasabah nasabah = SpringIn.getNasabahDao().findNasabahByPhoneNumber(sourceAccountNumber);

                log.info("Tarik tunai :\n"
                        + agent.getName() + "<<<" + nasabah.getName()
                        + "\nJumlah: " + transactionAmount);

                log.info("Saldo Agent : " + agent.getSaldo()
                        + "\nSaldo Nasabah : " + nasabah.getSaldo());

                agent.setSaldo(agent.getSaldo() + transactionAmount);
                nasabah.setSaldo(nasabah.getSaldo() - transactionAmount);

                SpringIn.getAgentDao().saveOrUpdate(agent);
                SpringIn.getNasabahDao().saveOrUpdate(nasabah);
            } else {
                if (destinationAccountNumber.startsWith("62")) { //Nasabah Lakupandai ke Nasabah Lakupandai
                    Nasabah nasabah = SpringIn.getNasabahDao().findNasabahByPhoneNumber(sourceAccountNumber);
                    Nasabah nasabahDes = SpringIn.getNasabahDao().findNasabahByPhoneNumber(destinationAccountNumber);

                    log.info("Transfer ke Nasabah Lakupandai :\n"
                            + nasabah.getName() + ">>>" + nasabahDes.getName()
                            + "\nJumlah: " + transactionAmount);

                    log.info("Saldo Nasbah Sumber : " + nasabah.getSaldo()
                            + "\nSaldo Nasabah Tujuan : " + nasabahDes.getSaldo());

                    destinationAccountName = nasabahDes.getName();
                    
                    nasabah.setSaldo(nasabah.getSaldo() - transactionAmount);
                    nasabahDes.setSaldo(nasabahDes.getSaldo() + transactionAmount);

                    SpringIn.getNasabahDao().saveOrUpdate(nasabah);
                    SpringIn.getNasabahDao().saveOrUpdate(nasabahDes);

                } else { //Nasabah Lakupandai ke Agen BPD DIY
                    Nasabah nasabah = SpringIn.getNasabahDao().findNasabahByPhoneNumber(sourceAccountNumber);
                    Agent agentDes = SpringIn.getAgentDao().findAgentByAccountNumber(destinationAccountNumber);

                    log.info("Transfer ke Agen BPD DIY :\n"
                            + nasabah.getName() + ">>>" + agentDes.getName()
                            + "\nJumlah: " + transactionAmount);

                    log.info("Saldo Nasbah Sumber : " + nasabah.getSaldo()
                            + "\nSaldo Agen Tujuan : " + agentDes.getSaldo());
                    
                    destinationAccountName = agentDes.getName();

                    nasabah.setSaldo(nasabah.getSaldo() - transactionAmount);
                    agentDes.setSaldo(agentDes.getSaldo() + transactionAmount);

                    SpringIn.getNasabahDao().saveOrUpdate(nasabah);
                    SpringIn.getAgentDao().saveOrUpdate(agentDes);
                }
            }

            if (!type.equals("1")) {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
                el.addChildElement("sourceAccountNumber").addTextNode(sourceAccountNumber);
                el.addChildElement("destinationAccountNumber").addTextNode(destinationAccountNumber);
                el.addChildElement("chargeAmount").addTextNode(String.valueOf(transactionAmount));
                el.addChildElement("agentFee").addTextNode("0");
                el.addChildElement("tax").addTextNode("0");
                el.addChildElement("referenceNumber").addTextNode(InputreferenceNumber);
                el.addChildElement("descriptionTransaction").addTextNode(descriptionTransaction);
                el.addChildElement("type").addTextNode(type);
                el.addChildElement("rrn").addTextNode(referenceNumber);
            } else {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
                el.addChildElement("sourceAccountNumber").addTextNode(sourceAccountNumber);
                el.addChildElement("destinationAccountName").addTextNode(destinationAccountName);
                el.addChildElement("destinationAccountNumber").addTextNode(destinationAccountNumber);
                el.addChildElement("transactionAmount").addTextNode(String.valueOf(transactionAmount));
                el.addChildElement("chargeAmount").addTextNode(String.valueOf(0));
                el.addChildElement("agentFee").addTextNode("0");
                el.addChildElement("tax").addTextNode("0");
                el.addChildElement("referenceNumber").addTextNode(InputreferenceNumber);
                el.addChildElement("descriptionTransaction").addTextNode(descriptionTransaction);
                el.addChildElement("type").addTextNode(type);
                el.addChildElement("rrn").addTextNode(referenceNumber);
            }

//                            <channelId>LP</channelId>
//                <responseCode>00</responseCode>
//                <responseDesc>SUKSES</responseDesc>
//                <sourceAccountNumber>6281241738116</sourceAccountNumber>
//                <destinationAccountName>RISYA MAULANA</destinationAccountName>
//                <destinationAccountNumber>6285708855970</destinationAccountNumber>
//                <transactionAmount>10000</transactionAmount>
//                <chargeAmount>0</chargeAmount>
//                <agentFee>0</agentFee>
//                <tax>0</tax>
//                <referenceNumber>191010055013</referenceNumber>
//                <descriptionTransaction>inquiryAccount</descriptionTransaction>
//                <type>1</type>
//                <rrn>091829719037</rrn>
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMsg.writeTo(out);
            String strMsg = new String(out.toByteArray());
            Output = strMsg;

            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);

        } catch (Exception e) {
            log.error(e);
        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
