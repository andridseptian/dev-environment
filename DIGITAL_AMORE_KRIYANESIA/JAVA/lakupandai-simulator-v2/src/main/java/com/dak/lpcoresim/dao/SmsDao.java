/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.dao;

import com.dak.lpcoresim.model.Sms;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "SmsDao")
@Transactional
public class SmsDao extends Dao {

    public Sms saveOrUpdate(Sms sms) {
        if (sms.getId() == null) {
            em.persist(sms);
            System.out.println("persist / saved");
        } else {
            em.merge(sms);
            System.out.println("Merge / updated");
        }
        return sms;
    }
}
