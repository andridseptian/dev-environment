/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.controller;

import com.dak.lpcoresim.model.LogActivity;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.Date;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.json.JSONException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Andri D Septian
 */
@RestController
public class MainController {

    Log log = Log.getLog("Q2", getClass().getName());
    JSONObject jso = new JSONObject();

    @RequestMapping(value = "/{service_type}", method = RequestMethod.POST)
    public String allProcess(@RequestBody(required = false) String body, @PathVariable("service_type") String path,
            HttpServletRequest request) throws ParseException {

        try {
            Context ctx = new Context();
            Space space = SpaceFactory.getSpace();

            LogActivity logActivity = new LogActivity();
            logActivity.setDtm_created_msg(new Date());
            logActivity.setRequest(body);
            logActivity.setResponse("Failed");
            SpringIn.getLogActivityDao().saveOrUpdate(logActivity);

            body = URLDecoder.decode(body);
            log.info("REQUEST BODY: \n" + body);

            Document xmlbody = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new ByteArrayInputStream(body.getBytes("UTF-8"))));
            xmlbody.getDocumentElement().normalize();

            JSONObject input = new JSONObject();
            NodeList nodeList = xmlbody.getElementsByTagName("*");

            if (body.contains("<smsbc>")) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element element = (Element) nodeList.item(i);
                    if (i >= 3) {
                        input.put(element.getNodeName(), element.getTextContent());
                    }
                }
            } else {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element element = (Element) nodeList.item(i);
                    if (i == 2) {
                        input.put("nsl", element.getNodeName());
                    }
                    if (i > 3) {
                        input.put(element.getNodeName(), element.getTextContent());
                    }
//                    if(element.getNodeName().equals("productCode")){
//                        path = element.getNodeName();
//                    }
                }
            }

            ctx.put(Constants.REQ_BODY, input);
            ctx.put(Constants.PATH, path);
            ctx.put(Constants.LOG, logActivity);
            
            System.out.println("Ini path : " + path);

            space.out("lpcoresim-trxmgr", ctx, 60000);
            Context ctx_response = (Context) space.in(logActivity.getId(), 60000);

            return (String) ctx_response.getString(Constants.BODY_RESPONSE);
        } catch (IOException | ParserConfigurationException | JSONException | DOMException | SAXException e) {
            log.error(e);
            e.printStackTrace();
            return null;
        }

    }

}
