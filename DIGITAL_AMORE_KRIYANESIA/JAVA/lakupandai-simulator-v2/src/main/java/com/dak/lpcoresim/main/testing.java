/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.soap.SOAPException;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class testing {

    public static void main(String[] args) throws MalformedURLException, IOException, SOAPException {

        String message = "Telkomsel Prepaid 22/12/20 13:38 Resi:- ke 0811666601005 nominal Rp.15.000 adm Rp.1.500 Voucher SN: 20201222135807787056 (RC:05) TRANSAKSI TIME OUT";

        String destinationNumber = "082196740580";

        JSONObject sendIMS = new JSONObject();
        String sendMessage = "Send SMS To [" + destinationNumber + "]\n" + message;
        sendIMS.put("destination", "082196740580");
        sendIMS.put("message", sendMessage.replace("OTP", "rahasia"));

        sendPOST(sendIMS.toString());

    }

    public static void sendPOST(String param) throws IOException {
        try {
            System.out.println("send Param: \n" + param);
            URL obj = new URL("http://139.99.30.176:9050/smsbulk");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "text/json; charset=utf-8");
            con.setRequestProperty("x-api-key", "889");
            con.setRequestProperty("user-agent", "modex");
            con.setRequestProperty("authorization", "modex123Test");

            // For POST only - START
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(param.getBytes());
            os.flush();
            os.close();
            // For POST only - END

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                System.out.println("Response IMS: " + response.toString());
            } else {
                System.out.println("POST request not worked " + con.getResponseMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
