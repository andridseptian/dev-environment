/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.main;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Andri D Septian
 */
public class test_XMLConvert {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String reqParam = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n"
                + "<SOAP-ENV:Envelope\n"
                + "    xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"\n"
                + "    xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"\n"
                + "    xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AgentRegistration/\"\n"
                + "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                + "    <SOAP-ENV:Body>\n"
                + "        <ns1:AgentLogin>\n"
                + "            <param0 xsi:type=\"SOAP-ENC:Struct\">\n"
                + "                <channelId xsi:type=\"xsd:string\">LP</channelId>\n"
                + "                <userId xsi:type=\"xsd:string\">mahen</userId>\n"
                + "                <pin xsi:type=\"xsd:string\">83bda7a0e613cb71</pin>\n"
                + "                <servicePassword xsi:type=\"xsd:string\">R4h4s1a</servicePassword>\n"
                + "                <type xsi:type=\"xsd:string\">2</type>\n"
                + "            </param0>\n"
                + "        </ns1:AgentLogin>\n"
                + "    </SOAP-ENV:Body>\n"
                + "</SOAP-ENV:Envelope>";

        reqParam = "%3C%3Fxml+version=%221.0%22+encoding%3D%22UTF-8%22%3F%3E%3CSOAP-ENV%3AEnvelope+xmlns%3ASOAP-ENV%3D%22http%3A%2F%2Fschemas.xmlsoap.org%2Fsoap%2Fenvelope%2F%22+xmlns%3Ans1%3D%22https%3A%2F%2Flpproxy.bpddiy.int%3A4646%2FAgentRegistration%2F%22+xmlns%3Axsd%3D%22http%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema%22+xmlns%3Axsi%3D%22http%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema-instance%22+xmlns%3ASOAP-ENC%3D%22http%3A%2F%2Fschemas.xmlsoap.org%2Fsoap%2Fencoding%2F%22+%3E%3CSOAP-ENV%3ABody%3E%3Cns1%3AAgentLogin%3E%3Cparam0+xsi%3Atype%3D%22SOAP-ENC%3AStruct%22%3E%3CchannelId+xsi%3Atype%3D%22xsd%3Astring%22%3ELP%3C%2FchannelId%3E%3CuserId+xsi%3Atype%3D%22xsd%3Astring%22%3Emahen%3C%2FuserId%3E%3Cpin+xsi%3Atype%3D%22xsd%3Astring%22%3E83bda7a0e613cb71%3C%2Fpin%3E%3CservicePassword+xsi%3Atype%3D%22xsd%3Astring%22%3ER4h4s1a%3C%2FservicePassword%3E%3Ctype+xsi%3Atype%3D%22xsd%3Astring%22%3E2%3C%2Ftype%3E%3C%2Fparam0%3E%3C%2Fns1%3AAgentLogin%3E%3C%2FSOAP-ENV%3ABody%3E%3C%2FSOAP-ENV%3AEnvelope%3E";
        
        reqParam = URLDecoder.decode(reqParam);
        System.out.println(reqParam);
        Document xmlbody = null;
        try {
            xmlbody = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new ByteArrayInputStream(reqParam.getBytes("UTF-8"))));
        } catch (Exception e) {
            e.printStackTrace();
        }

        xmlbody.getDocumentElement().normalize();

        JSONObject input = new JSONObject();
        NodeList nodeList = xmlbody.getElementsByTagName("*");

        for (int i = 0; i < nodeList.getLength(); i++) {
            // Get element
            Element element = (Element) nodeList.item(i);
            if (i == 2) {
                input.put("nsl", element.getNodeName());
            }
            if (i > 3) {
                input.put(element.getNodeName(), element.getTextContent());
            }
        }

        System.out.println(input);

    }

}
