/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service;

import com.dak.lpcoresim.model.Agent;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class AgentRegistration implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);

        try {
            log.info("REQUEST INPUT: \n" + input);
            String Output = "";
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMsg = factory.createMessage();
            soapMsg.getSOAPHeader().detachNode();
            SOAPPart part = soapMsg.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody bodySoap = envelope.getBody();

            SOAPBodyElement element = bodySoap.addBodyElement(envelope.createName("AgentInquiryResponse", "ns1", "urn:AgentInquiry"));
            SOAPElement el = element.addChildElement("ServiceResponse");

            Agent agent = SpringIn.getAgentDao().findAgentByAccountNumber(input.getString(Constants.INPUT.ACCOUNT_NUMBER));
            if (agent != null) {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
            } else {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.NOT_FOUND);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.NOT_FOUND);
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMsg.writeTo(out);
            String strMsg = new String(out.toByteArray());
            Output = strMsg;

            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            String Output = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n"
                    + "<SOAP-ENV:Envelope\n"
                    + "    xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"urn:AgentRegistration\">\n"
                    + "    <SOAP-ENV:Body>\n"
                    + "        <ns1:AgentRegistrationResponse>\n"
                    + "            <ServiceResponse>\n"
                    + "                <channelId>LP</channelId>\n"
                    + "                <responseCode>99</responseCode>\n"
                    + "                <responseDesc>INTERNAL SERVER ERROR</responseDesc>\n"
                    + "            </ServiceResponse>\n"
                    + "        </ns1:AgentRegistrationResponse>\n"
                    + "    </SOAP-ENV:Body>\n"
                    + "</SOAP-ENV:Envelope>";
            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);
        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
