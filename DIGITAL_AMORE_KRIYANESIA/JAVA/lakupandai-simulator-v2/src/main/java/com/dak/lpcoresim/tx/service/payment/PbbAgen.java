/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service.payment;

import com.dak.lpcoresim.utility.Constants;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class PbbAgen implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);
        
        try {
            log.info("REQUEST INPUT: \n" + input);
            
            String sourceAccountNumber = input.getString("sourceAccountNumber");
            String productCode = input.getString("productCode");
            String idBilling = input.getString("idBilling");
            String transactionType = input.getString("transactionType");
            String fee = input.getString("fee");
            String agentFee = input.getString("agentFee");
            String tax = input.getString("tax");
            String accountType = input.getString("accountType");
            
            String Output = "";
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMsg = factory.createMessage();
            soapMsg.getSOAPHeader().detachNode();
            SOAPPart part = soapMsg.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody bodySoap = envelope.getBody();

            SOAPBodyElement element = bodySoap.addBodyElement(envelope.createName("PaymentResponse", "ns1", "urn:Payment"));
            SOAPElement el = element.addChildElement("ServiceResponse");

            el.addChildElement("channelId").addTextNode("LP");
            el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
            el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
            el.addChildElement("sourceAccountNumber").addTextNode(sourceAccountNumber);
            el.addChildElement("productCode").addTextNode(productCode);
            el.addChildElement("idBilling").addTextNode(idBilling);
            el.addChildElement("transactionAmount").addTextNode("");
            el.addChildElement("fee").addTextNode(fee);
            el.addChildElement("agentFee").addTextNode(agentFee);
            el.addChildElement("tax").addTextNode(tax);
            el.addChildElement("additionalData").addTextNode("{\"nop\":\"340305000400701030\",\"tahun\":\"2019\",\"nama\":\"SIYAT\",\"kelurahan\":\"NGEPOSSARI\",\"tagihan\":17664}");
            el.addChildElement("transactionType").addTextNode(transactionType);
            el.addChildElement("accountType").addTextNode(accountType);
            el.addChildElement("jobIdInquiry").addTextNode("20190812162424707038");
            el.addChildElement("rrn").addTextNode("162424707038");
            
//                <channelId xsi:type="xsd:string">ACSLP</channelId>
//                <servicePassword xsi:type="xsd:string">7D1E9DF8C27D62A2F4AE5B50C38FB536</servicePassword>
//                <userId xsi:type="xsd:string">randy4674</userId>
//                <pin xsi:type="xsd:string">1f4d4a03f2723b68</pin>
//                <sourceAccountNumber xsi:type="xsd:number">001211030554</sourceAccountNumber>
//                <productCode xsi:type="xsd:number">050121</productCode>
//                <idBilling xsi:type="xsd:number">3403050004007010302019</idBilling>
//                <transactionAmount xsi:type="xsd:number">Null</transactionAmount>
//                <fee xsi:type="xsd:number">Null</fee>
//                <agentFee xsi:type="xsd:number">Null</agentFee>
//                <tax xsi:type="xsd:number">Null</tax>
//                <additionalData xsi:type="xsd:text">Null</additionalData>
//                <transactionType xsi:type="xsd:text">1</transactionType>
//                <accountType xsi:type="xsd:string">A</accountType>
//                <jobIdInquiry xsi:type="xsd:number">Null</jobIdInquiry>
//                <stan xsi:type="xsd:string">326485</stan>

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMsg.writeTo(out);
            String strMsg = new String(out.toByteArray());
            Output = strMsg;

            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);

        } catch (Exception e) {

        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
