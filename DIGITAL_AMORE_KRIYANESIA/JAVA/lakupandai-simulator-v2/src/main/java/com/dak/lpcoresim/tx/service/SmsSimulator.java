/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service;

import com.dak.lpcoresim.model.Sms;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import static javafx.css.StyleOrigin.USER_AGENT;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class SmsSimulator implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);

        try {
            log.info("REQUEST INPUT: \n" + input);

            String destinationNumber = input.getString("destinationNumber");
            String message = input.getString("message");
            String dateTime = "1126160455";
            String pertnerId = input.getString("partnerId");
            String pertnerName = input.getString("partnerName");
            String pertnerPassword = input.getString("password");

            Sms sms = new Sms();
            sms.setCrTime(new Date());
            sms.setDestinationNumber(destinationNumber);
            sms.setMessage(message);
            sms.setDateTime(dateTime);
            SpringIn.getSmsDao().saveOrUpdate(sms);

            String smsResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                    + "<smsbc>\n"
                    + "    <response>\n"
                    + "        <datetime>" + dateTime + "</datetime>\n"
                    + "        <rrn>1122019080915345646518822679855</rrn>\n"
                    + "        <partnerId>" + pertnerId + "</partnerId>\n"
                    + "        <partnerName>" + pertnerName + "</partnerName>\n"
                    + "        <destinationNumber>" + destinationNumber + "</destinationNumber>\n"
                    + "        <message>" + message + "</message>\n"
                    + "        <rc>1</rc>\n"
                    + "        <rm>Success</rm>\n"
                    + "    </response>\n"
                    + "</smsbc>";

            try {
                JSONObject sendIMS = new JSONObject();
                String sendMessage = "Send SMS To [" + destinationNumber + "]\n" + message;
                sendIMS.put("destination", "082196740580");
                sendIMS.put("message", sendMessage.replace("OTP", "rahasia"));
//                sendPOST(sendIMS.toString());
                
                log.info("Message to IMS: " + sendMessage);
            } catch (Exception e) {
                log.error(e);
            }

            ctx.put(Constants.BODY_RESPONSE, smsResponse);
            log.info("BODY RESPONSE: \n" + smsResponse);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void sendPOST(String param) throws IOException {
        try {
            log.info("send Param: \n"+param);
            URL obj = new URL("http://139.99.30.176:9050/smsbulk");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "text/json; charset=utf-8");
            con.setRequestProperty("x-api-key", "889");
            con.setRequestProperty("user-agent", "modex");
            con.setRequestProperty("authorization", "modex123Test");

            // For POST only - START
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(param.getBytes());
            os.flush();
            os.close();
            // For POST only - END

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                log.info("Response IMS: " + response.toString());
            } else {
                log.info("POST request not worked " + con.getResponseMessage());
            }
        } catch (Exception e) {
            log.error(e);
        }

    }
}
