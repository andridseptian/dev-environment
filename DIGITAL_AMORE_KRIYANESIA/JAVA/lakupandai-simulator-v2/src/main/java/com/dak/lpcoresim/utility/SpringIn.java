/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.utility;

import com.dak.lpcoresim.dao.AgentDao;
import com.dak.lpcoresim.dao.DukcapilDao;
import com.dak.lpcoresim.dao.LogActivityDao;
import com.dak.lpcoresim.dao.NasabahDao;
import com.dak.lpcoresim.dao.ProductDao;
import com.dak.lpcoresim.dao.SmsDao;
import javax.naming.ConfigurationException;
import org.jpos.util.Log;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Andri D Septian
 */
public class SpringIn {

    Log log = Log.getLog("Q2", getClass().getName());

    public static LogActivityDao logActivityDao;
    public static AgentDao agentDao;
    public static NasabahDao nasabahDao;
    public static SmsDao smsDao;
    public static DukcapilDao dukcapilDao;
    public static ProductDao productDao;

    public static LogActivityDao getLogActivityDao() {
        return logActivityDao;
    }

    public static void setLogActivityDao(LogActivityDao logActivityDao) {
        SpringIn.logActivityDao = logActivityDao;
    }

    public static AgentDao getAgentDao() {
        return agentDao;
    }

    public static void setAgentDao(AgentDao agentDao) {
        SpringIn.agentDao = agentDao;
    }

    public static NasabahDao getNasabahDao() {
        return nasabahDao;
    }

    public static void setNasabahDao(NasabahDao nasabahDao) {
        SpringIn.nasabahDao = nasabahDao;
    }

    public static SmsDao getSmsDao() {
        return smsDao;
    }

    public static void setSmsDao(SmsDao smsDao) {
        SpringIn.smsDao = smsDao;
    }

    public static DukcapilDao getDukcapilDao() {
        return dukcapilDao;
    }

    public static void setDukcapilDao(DukcapilDao dukcapilDao) {
        SpringIn.dukcapilDao = dukcapilDao;
    }

    public static ProductDao getProductDao() {
        return productDao;
    }

    public static void setProductDao(ProductDao productDao) {
        SpringIn.productDao = productDao;
    }

    public void initService() throws ConfigurationException {

//        ApplicationContext context = new FileSystemXmlApplicationContext("/src/main/resources/ApplicationContext.xml");
        ApplicationContext context = new FileSystemXmlApplicationContext("ApplicationContext.xml");

        setLogActivityDao(context.getBean("LogActivityDao", LogActivityDao.class));
        setAgentDao(context.getBean("AgentDao", AgentDao.class));
        setNasabahDao(context.getBean("NasabahDao", NasabahDao.class));
        setSmsDao(context.getBean("SmsDao", SmsDao.class));
        setDukcapilDao(context.getBean("DukcapilDao", DukcapilDao.class));
        setProductDao(context.getBean("ProductDao", ProductDao.class));

        log.info("Init DB has Started");
        log.info("Service Started");
    }
}
