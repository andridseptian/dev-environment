/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.participant;

import com.dak.lpcoresim.utility.Constants;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class NoRoute implements TransactionParticipant {
    
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }
    
    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        JSONObject output = new JSONObject();
        output.put("rc","99");
        output.put("rm","Path tidak ditemukan");
        ctx.put(Constants.BODY_RESPONSE, output);
        
    }
    
    @Override
    public void abort(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.put(Constants.BODY_RESPONSE, "Path tidak tersedia");
        
    }
}
