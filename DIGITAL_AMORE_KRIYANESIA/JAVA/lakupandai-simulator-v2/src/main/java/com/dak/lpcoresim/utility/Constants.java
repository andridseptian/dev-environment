/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.utility;

/**
 *
 * @author Andri D Septian
 */
public class Constants {

    public static final String REQ_BODY = "REQ_BODY";
    public static final String BODY_RESPONSE = "BODY_RESPONSE";
    public static final String PATH = "PATH";
    public static final String LOG = "LOG";
    public static final String ABORTED_RESPONSE = "ABORTED_RESPONSE";
    public static final String STATUS = "STATUS";

    public static class TRANSACTIONAL_MANAGER {

        public static final String GROUP = "GROUP";
    }

    public static class RESPONSE {

        public static class CODE {

            public static final String SUKSES = "00";
            public static final String GAGAL = "99";
            public static final String NOT_FOUND = "92";
            
        }

        public static class MESSAGE {

            public static final String SUKSES = "SUKSES";
            public static final String GAGAL = "GAGAL";
            public static final String NOT_FOUND = "DATA TIDAK DITEMUKAN";
        }
    }

    public static class INPUT {

        public static final String USERID = "userId";
        public static final String PIN = "pin";
        public static final String SERVICE_PASSWORD = "servicePassword";
        public static final String TYPE = "type";
        public static final String CHANNELID = "channelId";
        public static final String PHONE_NUMBER = "phoneNumber";
        public static final String ACCOUNT_NUMBER = "accountNumber";
        public static final String NIK = "nik";
    }

}
