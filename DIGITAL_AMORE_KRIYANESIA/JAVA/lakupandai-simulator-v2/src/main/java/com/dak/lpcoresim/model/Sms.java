/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author Andri D Septian
 */
@Entity(name = "Sms")
@Table(name = "sms")
public class Sms {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_sms_seq")
    @SequenceGenerator(name = "id_sms_seq", sequenceName = "id_sms_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "cr_time", nullable = true)
    private Date CrTime;

    @Column(name = "date_time", nullable = false)
    private String DateTime;

    @Column(name = "destination_number", nullable = false)
    private String DestinationNumber;

    @Column(name = "message", nullable = false)
    @Type(type = "text")
    private String Message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCrTime() {
        return CrTime;
    }

    public void setCrTime(Date CrTime) {
        this.CrTime = CrTime;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String DateTime) {
        this.DateTime = DateTime;
    }

    public String getDestinationNumber() {
        return DestinationNumber;
    }

    public void setDestinationNumber(String DestinationNumber) {
        this.DestinationNumber = DestinationNumber;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

}
