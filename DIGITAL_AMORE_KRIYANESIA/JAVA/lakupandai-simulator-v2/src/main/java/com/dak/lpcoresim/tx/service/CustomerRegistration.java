/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.service;

import com.dak.lpcoresim.model.Dukcapil;
import com.dak.lpcoresim.model.Nasabah;
import com.dak.lpcoresim.utility.Constants;
import com.dak.lpcoresim.utility.SpringIn;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Random;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class CustomerRegistration implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject input = (JSONObject) ctx.get(Constants.REQ_BODY);

        try {
            log.info("REQUEST INPUT: \n" + input);

            String Output = "";
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMsg = factory.createMessage();
            soapMsg.getSOAPHeader().detachNode();
            SOAPPart part = soapMsg.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody bodySoap = envelope.getBody();

            SOAPBodyElement element = bodySoap.addBodyElement(envelope.createName("CustomerRegistration", "ns1", "urn:CustomerRegistration"));
            SOAPElement el = element.addChildElement("ServiceResponse");

            String phoneNumber = input.getString("phoneNumber");
            String nik = input.getString("nik");
            String accountType = input.getString("accountType");
            long Saldo = 100000000;

            boolean accnumber = false;
            String accountNumber = "";
            while (!accnumber) {
                Random rnd = new Random();
                int n = 100 + rnd.nextInt(900);
                accountNumber = "00226" + n + phoneNumber.substring(phoneNumber.length() - 4);
                Nasabah nascek = SpringIn.getNasabahDao().findNasabahByAccountNumber(accountNumber);
                if(nascek != null){
                    accnumber = true;
                }
            }

            Dukcapil dukcapil = SpringIn.getDukcapilDao().findDukcapilByNik(nik);

            log.info("dob : " + dukcapil.getDob());

            Nasabah nasabah = new Nasabah();
            nasabah.setNik(nik);
//            nasabah.setAccountNumber("00226200" + phoneNumber.substring(phoneNumber.length() - 4));
            nasabah.setAccountNumber(accountNumber);
            nasabah.setAddress(dukcapil.getAddress());
            nasabah.setPhoneNumber(phoneNumber);
            nasabah.setName(dukcapil.getName());
            nasabah.setAccountMotherName(dukcapil.getMotherName());
            nasabah.setDob(dukcapil.getDob());
            nasabah.setAgentType(accountType);
            nasabah.setSaldo(Saldo);
            SpringIn.getNasabahDao().saveOrUpdate(nasabah);

            if (dukcapil != null) {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.SUKSES);
                el.addChildElement("responseDesc").addTextNode(Constants.RESPONSE.MESSAGE.SUKSES);
                el.addChildElement("accountNumber").addTextNode(nasabah.getAccountNumber());
                el.addChildElement("accountType").addTextNode(nasabah.getAgentType());
            } else {
                el.addChildElement("channelId").addTextNode("LP");
                el.addChildElement("responseCode").addTextNode(Constants.RESPONSE.CODE.GAGAL);
                el.addChildElement("responseDesc").addTextNode("DATA TIDAK DITEMUKAN");
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMsg.writeTo(out);
            String strMsg = new String(out.toByteArray());
            Output = strMsg;

            ctx.put(Constants.BODY_RESPONSE, Output);
            log.info("BODY RESPONSE: \n" + Output);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
