/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.tx.participant;

import com.dak.lpcoresim.model.LogActivity;
import com.dak.lpcoresim.utility.Constants;
import java.io.Serializable;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;

/**
 *
 * @author Andri D Septian
 */
public class SendResponse implements AbortParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepareForAbort(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        LogActivity logAct = (LogActivity) ctx.get(Constants.LOG);

        log.info("masuk send response");
        Space space = SpaceFactory.getSpace();
        space.out(logAct.getId(), ctx, 60000);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        LogActivity logAct = (LogActivity) ctx.get(Constants.LOG);

        System.out.println("masuk send response");
        Space space = SpaceFactory.getSpace();
        space.out(logAct.getId(), ctx, 60000);
    }
}
