/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Andri D Septian
 */
@Entity(name = "Dukcapil")
@Table(name = "dukcapil")
public class Dukcapil {

    @Id
    @Column(name = "nik", length = 16, nullable = false)
    private String Nik;

    @Column(name = "name", length = 50)
    private String Name;

    @Column(name = "dob")
    private String Dob;

    @Column(name = "mother_name")
    private String MotherName;
    
    @Column(name = "address")
    private String Address;

    public String getNik() {
        return Nik;
    }

    public void setNik(String Nik) {
        this.Nik = Nik;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String Dob) {
        this.Dob = Dob;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String MotherName) {
        this.MotherName = MotherName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    
    

}
