/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.lpcoresim.dao;

import com.dak.lpcoresim.model.Product;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "ProductDao")
@Transactional
public class ProductDao extends Dao {

    public Product findAgentByIdBilling(String IdBilling) {
        try {
            Product product = (Product) em.createQuery("SELECT a FROM Product a WHERE a.IdBilling = :idbilling")
                    .setParameter("idbilling", IdBilling)
                    .getSingleResult();
            return product;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Product findAgentByIdBillingAndProductCode(String IdBilling, String ProductCode) {
        try {
            Product product = (Product) em.createQuery("SELECT a FROM Product a WHERE a.IdBilling = :idbilling AND a.ProductCode = :product_code")
                    .setParameter("idbilling", IdBilling)
                    .setParameter("product_code", ProductCode)
                    .getSingleResult();
            return product;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Product findAgentByIdBillingAndTransactionTypeAndProductCode(String IdBilling, String transactionType, String ProductCode) {
        try {
            Product product = (Product) em.createQuery("SELECT a FROM Product a WHERE a.IdBilling = :idbilling AND a.TransactionType = :transactionType AND a.ProductCode = :productCode")
                    .setParameter("idbilling", IdBilling)
                    .setParameter("transactionType", transactionType)
                    .setParameter("productCode", ProductCode)
                    .getSingleResult();
            return product;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Product findAgentByProductCodeAndTransactionType(String ProductCode, String transactionType) {
        try {
            Product product = (Product) em.createQuery("SELECT a FROM Product a WHERE a.ProductCode = :ProductCode AND a.TransactionType = :transactionType")
                    .setParameter("ProductCode", ProductCode)
                    .setParameter("transactionType", transactionType)
                    .getSingleResult();
            return product;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Product findAgentByProductCodeAndTransactionTypeAndBilling(String ProductCode, String transactionType, String billing) {
        try {
            Product product = (Product) em.createQuery("SELECT a FROM Product a WHERE a.ProductCode = :ProductCode AND a.TransactionType = :transactionType AND a.IdBilling = :idbilling")
                    .setParameter("ProductCode", ProductCode)
                    .setParameter("transactionType", transactionType)
                    .setParameter("idbilling", billing)
                    .getSingleResult();
            return product;
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
