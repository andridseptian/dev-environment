package com.acs.util;

import java.util.Random;

/**
 *
 * @author Erwin
 */
public class StringRandomUtil {
    
    private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final int RANDOM_STRING_LENGTH = 10;

    public String generateRandomString(){
        StringBuilder randStr = new StringBuilder();
        for(int i=0; i<RANDOM_STRING_LENGTH; i++){
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }
    
    public String generateRandomString(String charList, int length){
        StringBuilder randStr = new StringBuilder();
        for(int i=0; i<length; i++){
            int number = getRandomNumber();
            char ch = charList.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }
    
    public String generateRandomString(int length){
        StringBuilder randStr = new StringBuilder();
        for(int i=0; i<length; i++){
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }
    
    public static String generateUniqueRandomString(int length) {
        StringRandomUtil random = new StringRandomUtil();
        String generatedString = "";
        Boolean flag = false;
        for (int i = 0; i < 10; i++) {
            generatedString = random.generateRandomString(length);
            if (generatedString.matches(".*\\d.*")) {
                flag = true;
                break;
            } else {
                flag = false;
            }
        }

        if (!flag) {
            generatedString = "1asd45fg67";
        }
        
        return generatedString;
    }
     
    /**
     * This method generates random numbers
     * @return int
     */
    private int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }
    
}
