/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.iso;

import bsh.Interpreter;
import java.util.Arrays;
import java.util.HashSet;
import org.jpos.bsh.BSHRequestListener;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;

/**
 *
 * @author erwin
 */
public class CustomBSHListener extends BSHRequestListener {

    protected static final String MTI_MACRO = "$mti";
    protected HashSet whitelist;
    protected String[] bshSource;
    Configuration cfg;

    public CustomBSHListener() {
        super();
    }

    /**
     * @param cfg
     * <ul>
     * <li>whitelist - supported message types (example: "1100,1220")
     * <li>source - BSH script(s) to run (can be more than one)
     * </ul>
     */
    public void setConfiguration(Configuration cfg)
            throws ConfigurationException {
        this.cfg = cfg;
        bshSource = cfg.getAll("source");
        String[] mti = cfg.get("whitelist", "*").split(",");
        whitelist = new HashSet(Arrays.asList(mti));
    }

    public boolean process(ISOSource source, ISOMsg m) {
        try {
            String mti = m.getMTI();
            if (!whitelist.contains(mti) && !whitelist.contains("*")) {
                mti = "unsupported";
            }

            for (int i = 0; i < bshSource.length; i++) {
                try {
                    Interpreter bsh = new Interpreter();
                    bsh.set("source", source);
                    bsh.set("message", m);
                    bsh.set("log", this);
                    bsh.set("cfg", cfg);

                    int idx = bshSource[i].indexOf(MTI_MACRO);
                    String script;

                    if (idx >= 0) {
                        // replace $mti with the actual value in script file name
                        script = bshSource[i].substring(0, idx) + mti
                                + bshSource[i].substring(idx + MTI_MACRO.length());
                    } else {
                        script = bshSource[i];
                    }

                    bsh.source(script);
                } catch (Exception e) {
                    //warn(e);
                    //return false;
                }
            }
        } catch (Exception e) {
            //warn(e);
            return false;
        }
        return true;
    }

}
