/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.iso.channel;

import com.acs.util.Utility;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

/**
 *
 * @author Erwin
 */
public class AutoLogonASCIIChannel extends ASCIIChannel {

    boolean autoLogOn = true;
    String de7Format = "MMddHHmmss";
    String nmic = "001";
    String mti = "0800";
    LogEvent evt;

    public AutoLogonASCIIChannel() {
        super();
    }

    @Override
    public void connect() {
        evt = new LogEvent(this, "autologon-ascii-channel-connect");
        try {
            super.connect();
            if (autoLogOn) {
                doLogOn();
            }
        } catch (IOException ex) {
            evt.addMessage(ex.getMessage());
        } catch (ISOException isoe) {
            evt.addMessage(isoe.getMessage());
        } finally {
            Logger.log(evt);
        }
    }

    public void doLogOn() throws IOException, ISOException {
        ISOMsg r = createMsg();
        r.setMTI(mti);
        r.set(7, new SimpleDateFormat(de7Format).format(new Date()));
        r.set(11, Utility.generateStan());
        r.set(70, nmic);
        if (this.isConnected()) {
            synchronized (serverOutLock) {
                this.send(r);
            }
            synchronized (serverInLock) {
                ISOMsg receive;
                receive = this.receive();
                if (receive == null) {
                    evt.addMessage("Log On Timeout");
                } else {
                    evt.addMessage("Log On Success");
                }
            }
        }
    }

}
