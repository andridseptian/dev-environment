/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.mail;

import java.util.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Erwin
 */
public class MailSender {

    public static void sendMailToSingleRecipient(String smtpHost, String mailUser, String mailPassword, String sender, String to, String subject, String mailContent) throws MessagingException {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", smtpHost);
        properties.setProperty("mail.user", mailUser);
        properties.setProperty("mail.password", mailPassword);
        Session session = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(sender));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        message.setText(mailContent);
        Transport.send(message);
    }
    
    public static void sendMailToSingleRecipient(Properties properties, String sender, String to, String subject, String mailContent) throws AddressException, MessagingException {
        Session session = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(sender));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        message.setText(mailContent);
        Transport.send(message);
    }

    public static void sendMailToMultipleRecipient(String smtpHost, String mailUser, String mailPassword, String sender, String to, String cc, String bcc, String subject, String mailContent) throws MessagingException {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", smtpHost);
        properties.setProperty("mail.user", mailUser);
        properties.setProperty("mail.password", mailPassword);
        Session session = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(sender));
        if (to != null) {
            message.addRecipients(Message.RecipientType.TO, to);
        }
        if (cc != null) {
            message.addRecipients(Message.RecipientType.CC, cc);
        }
        if (bcc != null) {
            message.addRecipients(Message.RecipientType.BCC, bcc);
        }
        message.setSubject(subject);
        message.setText(mailContent);
        Transport.send(message);
    }

}
