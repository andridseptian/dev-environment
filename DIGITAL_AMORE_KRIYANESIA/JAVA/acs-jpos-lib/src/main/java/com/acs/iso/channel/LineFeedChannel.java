package com.acs.iso.channel;

import com.acs.util.Utility;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.BaseChannel;
import static org.jpos.iso.ISOChannel.RX;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

/**
 *
 * @author Erwin
 */
public class LineFeedChannel extends BaseChannel {

    LogEvent evt;

    public LineFeedChannel() {
        super();
    }

    @Override
    public void connect() {
        evt = new LogEvent(this, "connect");
        try {
            super.connect();
        } catch (IOException ex) {
            evt.addMessage(ex.getMessage());
            Logger.log(evt);
        }
    }

    @Override
    public void sendKeepAlive() throws IOException {}

    @Override
    public ISOMsg receive() throws IOException, ISOException {
        ISOMsg m = new ISOMsg();
        int c, len = 1;
        m.setPackager(packager);
        m.setSource(this);
        evt = new LogEvent(this, "receive");
        try {
            synchronized (serverIn) {
                c = serverIn.read();
                if (c == -1) {
                    throw new EOFException("connection closed");
                }
                byte[] b = new byte[1];
                b[0] = (byte) c;

                // Wait for packets until timeout
                while ((c = serverIn.available()) > 0) {
                    b = new byte[c];
                    if (serverIn.read(b) != c) {
                        throw new EOFException("connection closed");
                    }
                    len += c;
                }
                int lengthOfInputStream = len - 1;
                byte[] buf = new byte[lengthOfInputStream];
                System.arraycopy(b, 0, buf, 0, lengthOfInputStream);
                byte[] zeroHeader = (byte[]) "0".getBytes("ISO8859_1");
                byte[] resultArr = new byte[lengthOfInputStream + 1];
                System.arraycopy(zeroHeader, 0, resultArr, 0, 1);
                System.arraycopy(buf, 0, resultArr, 1, lengthOfInputStream - 1);
//                Logger.log(new LogEvent(this, "Input Stream: " + new String(resultArr)));
                m.unpack(resultArr);
            }
            m.setDirection(ISOMsg.INCOMING);
            m = applyIncomingFilters(m, evt);
            m.setDirection(ISOMsg.INCOMING);
            evt.addMessage(m);
            cnt[RX]++;
            setChanged();
            notifyObservers(m);
        } catch (ISOException e) {
            evt.addMessage(e);
            throw e;
        } catch (EOFException e) {
            evt.addMessage("<peer-disconnect/>");
            throw e;
        } catch (InterruptedIOException e) {
            evt.addMessage("<io-timeout/>");
            throw e;
        } catch (IOException e) {
            if (usable) {
                evt.addMessage(e);
            }
            throw e;
        } catch (Exception e) {
            evt.addMessage(e);
            throw new ISOException("unexpected exception", e);
        }
        Logger.log(evt);
        return m;
    }

    @Override
    protected void sendMessageTrailler(ISOMsg m, byte[] b) throws IOException {
        serverOut.write("\n".getBytes("ISO8859_1"));
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        super.setConfiguration(cfg);
    }

}
