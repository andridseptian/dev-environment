package com.acs.http;

import com.acs.http.channel.HttpChannelAdaptor;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author Erwin
 */
public class HttpSender {

    public static Log log = Log.getLog("Q2", "HTTP-Sender");
    private static final int PENDING = 2;
    private static final int SUCCESS = 1;
    private static final int FAILED = 0;

    public static HttpChannelMsg request(String destination, HttpChannelMsg req, String additionalUrl, String method, int milisTimeoutConnection) throws NameRegistrar.NotFoundException, NoSuchAlgorithmException {
        HttpChannelAdaptor httpClient = (HttpChannelAdaptor) NameRegistrar.get(destination);
        req.setContentType(httpClient.getContentType());
        if (additionalUrl == null) {
            req.setAdditionalUrl("/");
        } else {
            req.setAdditionalUrl(additionalUrl);
        }
        HttpChannelMsg rspMsg = null;
        try {
            if ("post".equals(method)) {
                rspMsg = httpClient.sendPost(req, milisTimeoutConnection);
            } else {
                rspMsg = httpClient.sendGet(req, milisTimeoutConnection);
            }
        } catch (IOException ex) {
            log.error("Cannot close HttpClient");
        } catch (KeyManagementException ex) {
            log.error("Key Management Exception");
        }

        if (rspMsg == null) {
            rspMsg = req;
            rspMsg.setTxStatusCode(PENDING);
            log.error("No response from module '" + destination + "'");
            return rspMsg;
        } else if (rspMsg.getHttpStatusCode() >= 300) {
            rspMsg.setTxStatusCode(FAILED);
            log.error("Receive HTTP Status Code '" + rspMsg.getHttpStatusCode() + "' from module '" + destination + "'");
            return rspMsg;
        } else {
            rspMsg.setTxStatusCode(SUCCESS);
            return rspMsg;
        }
    }

    public static HttpChannelMsg request(HttpChannelAdaptor httpClient, HttpChannelMsg req, String additionalUrl, String method, int milisTimeoutConnection) throws NameRegistrar.NotFoundException, NoSuchAlgorithmException {
        req.setContentType(httpClient.getContentType());
        if (additionalUrl == null) {
            req.setAdditionalUrl("/");
        } else {
            req.setAdditionalUrl(additionalUrl);
        }
        HttpChannelMsg rspMsg = null;
        try {
            if ("post".equals(method)) {
                rspMsg = httpClient.sendPost(req, milisTimeoutConnection);
            } else {
                rspMsg = httpClient.sendGet(req, milisTimeoutConnection);
            }
        } catch (IOException ex) {
            log.error("Cannot close HttpClient");
        } catch (KeyManagementException ex) {
            log.error("Key Management Exception");
        }
        if (rspMsg == null) {
            rspMsg = req;
            rspMsg.setTxStatusCode(PENDING);
            log.error("No response from module '" + httpClient.getName() + "'");
            return rspMsg;
        } else if (rspMsg.getHttpStatusCode() >= 300) {
            rspMsg.setTxStatusCode(FAILED);
            log.error("Receive HTTP Status Code '" + rspMsg.getHttpStatusCode() + "' from module '" + httpClient.getName() + "'");
            return rspMsg;
        } else {
            rspMsg.setTxStatusCode(SUCCESS);
            return rspMsg;
        }
    }

}
