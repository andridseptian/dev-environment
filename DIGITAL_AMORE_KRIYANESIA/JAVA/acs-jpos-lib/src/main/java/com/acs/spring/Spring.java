/*
 * Copyright (c) 2004 jPOS.org
 *
 * See terms of license at http://jpos.org/license.html
 *
 */
package com.acs.spring;

import org.jpos.core.ConfigurationException;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * @author Anthony Schexnaildre
 * @version $Revision: 2066 $ $Date: 2004-11-23 10:21:01 -0800 (Tue, 23 Nov 2004) $
 * @jmx:mbean description="Spring QBean" extends="org.jpos.q2.QBeanSupportMBean"
 */
public class Spring extends QBeanSupport implements SpringMBean {

    
    protected ApplicationContext context;
    protected String[] configFiles;

    @Override
    public void initService () throws ConfigurationException {
        configFiles = cfg.getAll( "config" );
        if ( configFiles.length < 1 )
            throw new ConfigurationException ("config property not specified");
        
        //context = new GenericXmlApplicationContext( configFiles );
        context = new FileSystemXmlApplicationContext( configFiles );
        //context = new ClassPathXmlApplicationContext(configFiles);
    }

    @Override
    public void startService () {
        NameRegistrar.register(getName(), context);
        //NameRegistrar.register( getName(), this );
        
    }

    @Override
    public void stopService () {
        NameRegistrar.unregister(getName());
        //NameRegistrar.unregister( getName() );
        
    }

    /**
     * Returns the Spring ApplicationContext
     * @return 
     */
    public ApplicationContext getContext () {
	    return context;
    }

    /**
     * @return 
     * @jmx:managed-attribute description="Configuration Files"
     */
    public String[] getConfig () {
        return configFiles;
    }

}

