/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.smpp;

import org.jpos.q2.QBeanSupportMBean;

/**
 *
 * @author EliteBook
 */
public interface SmppMBean extends QBeanSupportMBean {

    void setPort(int port);

    int getPort();
    
    void setHost(String host);
    
    String getHost();
    
    void setUsername(String username);
    
    String getUsername();
    
    void setPassword(String password);
    
    String getPassword();
    
    void setReconnect(int reconnect);
    
    int getReconnect();
    
    void setBindingType(String bindingType);
    
    String getbindingType();
    
}
