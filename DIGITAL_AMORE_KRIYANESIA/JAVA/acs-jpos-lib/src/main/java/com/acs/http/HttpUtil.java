package com.acs.http;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Erwin
 */
public class HttpUtil {

    static final Logger log = LoggerFactory.getLogger(HttpUtil.class);

    public static HttpResponse sendAndRcvHttpDelete(String url, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        // for handling Https
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpDelete httpDelete = new HttpDelete(url);
        httpDelete.setConfig(requestConfig);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        log.info("Send HTTP Delete request to '" + url + "'");
        HttpResponse response = httpClient.execute(httpDelete);
        log.info("Receive HTTP Response from '" + url + "' with Status Code : " + response.getStatusLine().getStatusCode());
        return response;
    }

    public static HttpResponse sendAndRcvHttpPutPlainText(String url, String contentRequest, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        // for handling Https
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPut httpPut = new HttpPut(url);
        httpPut.setConfig(requestConfig);
        StringEntity se = new StringEntity(contentRequest);
        se.setContentType("text/plain");
        httpPut.setEntity(se);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        //log.info("Send HTTP Put request to '" + url + "'");
        HttpResponse response = httpClient.execute(httpPut);
        //log.info("Receive HTTP Response from '" + url + "' with Status Code : " + response.getStatusLine().getStatusCode());
        return response;
    }

    public static HttpResponse sendAndRcvHttpPostPlainText(String url, String contentRequest, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        // for handling Https
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        StringEntity se = new StringEntity(contentRequest);
        se.setContentType("text/plain");
        httpPost.setEntity(se);
//        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public static HttpResponse sendAndRcvHttpPostJsonApplicationText(String url, String contentRequest, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }

            }
        };
        SSLContext sslcontext = SSLContext.getInstance("SSL");
        sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
        X509HostnameVerifier allX509HostsValid = new X509HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }

            @Override
            public void verify(String string, SSLSocket ssls) throws IOException {
                
            }

            @Override
            public void verify(String string, X509Certificate xc) throws SSLException {
                
            }

            @Override
            public void verify(String string, String[] strings, String[] strings1) throws SSLException {
                
            }
        };
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, allX509HostsValid);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        StringEntity se = new StringEntity(contentRequest);
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public static HttpResponse sendAndRcvHttpPostForm(String url, List<NameValuePair> nameValuePairs, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws UnsupportedEncodingException, IOException, NoSuchAlgorithmException, KeyManagementException {
        // for handling Https
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        //CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public static HttpResponse sendAndRcvHttpGet(String url, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        // for handling Https
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);
        //CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpGet);
        return response;
    }

    public static String getStringHttpContent(HttpResponse response) throws IllegalStateException, IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(response.getEntity().getContent(), writer);
        String content = writer.toString();
        return content;
    }

    public static String getStringHttpContent(HttpEntity entity) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(entity.getContent(), writer);
        String content = writer.toString();
        return content;
    }

}
