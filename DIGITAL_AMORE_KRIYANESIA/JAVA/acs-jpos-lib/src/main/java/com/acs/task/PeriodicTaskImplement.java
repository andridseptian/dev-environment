/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.task;

import bsh.BshClassManager;
import bsh.Interpreter;
import java.io.File;
import java.io.FileInputStream;
import org.jdom.Element;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.q2.Q2;
import org.jpos.util.Log;

/**
 *
 * @author dhacker
 */
public class PeriodicTaskImplement 
        implements Runnable, Configurable
{
    Log log;
    Q2 server;
    Interpreter bsh;
    Configuration cfg;
    Element element;
    String script;

    public PeriodicTaskImplement() {
        super();
    }

    /**
     * Execute script for certain period
     *
     */
    @Override
    public void run() {
        try {
            bsh.eval(script);
        } catch (Exception e) {
            log.error(e);
        }
    }

    /**
     * Read script file for scheduled period
     * @param server running Q2 server
     */
    public void setServer(Q2 server) {
        this.server = server;
        try {
            bsh = new Interpreter ();
            BshClassManager bcm = bsh.getClassManager();
            bcm.setClassPath(server.getLoader().getURLs());
            bcm.setClassLoader(server.getLoader());
            script = "";
            String scriptfile = cfg.get("script");
            File f = new File(scriptfile);
            if (f.exists()) {
                FileInputStream fis = new FileInputStream(f);
                int r = 0;
                while ((r = fis.read()) != -1) {
                    char c = (char)r;
                    script += c;
                }
                fis.close();
            } 
            else
            {
                log.error("file " + f.getAbsolutePath() + " not found");
            }
            bsh.set("qbean", this);
            bsh.set("log", log);            
        } catch (Exception e) {
            log.error(e);
        }
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        log = Log.getLog ("Q2", getClass().getName());
        this.cfg = cfg;
    }
}
