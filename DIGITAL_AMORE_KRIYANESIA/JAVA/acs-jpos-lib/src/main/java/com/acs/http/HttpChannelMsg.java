/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.http;

/**
 *
 * @author EliteBook
 */
public class HttpChannelMsg {
    
    private int httpStatusCode;
    private int txStatusCode;
    private String content;
    private String contentType;
    private String additionalUrl;
    private String urlParam;

    /**
     * @return the httpStatusCode
     */
    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    /**
     * @param httpStatusCode the httpStatusCode to set
     */
    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the urlParam
     */
    public String getUrlParam() {
        return urlParam;
    }

    /**
     * @param urlParam the urlParam to set
     */
    public void setUrlParam(String urlParam) {
        this.urlParam = urlParam;
    }

    /**
     * @return the additionalUrl
     */
    public String getAdditionalUrl() {
        return additionalUrl;
    }

    /**
     * @param additionalUrl the additionalUrl to set
     */
    public void setAdditionalUrl(String additionalUrl) {
        this.additionalUrl = additionalUrl;
    }

    /**
     * @return the txStatusCode
     */
    public int getTxStatusCode() {
        return txStatusCode;
    }

    /**
     * @param txStatusCode the txStatusCode to set
     */
    public void setTxStatusCode(int txStatusCode) {
        this.txStatusCode = txStatusCode;
    }
    
}
