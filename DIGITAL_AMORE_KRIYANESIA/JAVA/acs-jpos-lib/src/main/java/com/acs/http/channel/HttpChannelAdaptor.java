package com.acs.http.channel;

import com.acs.http.HttpChannelMsg;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.naming.ConfigurationException;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.jdom.Element;
import org.jpos.q2.QBeanSupport;
import org.jpos.space.Space;
import org.jpos.util.Logger;
import org.jpos.util.NameRegistrar;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


/**
 *
 * @author EliteBook
 */
public class HttpChannelAdaptor extends QBeanSupport {
    Space<String, Object> sp;
    String in, out;
    long delay;
    int maxConnections;
    int maxConnectAttempts;
    String protocol;
    private String url;
    private String contentType;
    protected String realm = null;
    protected String originalRealm = null;
    private boolean msgLog;

    public HttpChannelAdaptor() {
    }

    @Override
    public void setRealm(String realm) {
        this.realm = realm;
    }

    @Override
    public String getRealm() {
        return realm;
    }

    @Override
    public void initService() {
        try {
            log.info("HTTP Post Channel Client Adaptor " + getName() + " is starting");
            initChannel();
        } catch (ConfigurationException ex) {
            log.error(ex);
        }
    }

    @Override
    public void startService() {
        NameRegistrar.register(getName(), this);
        log.info("HTTP Channel Client Adaptor " + getName() + " is started");
    }

    @Override
    public void stopService() {
        log.info("HTTP Channel Client Adaptor " + getName() + " is stopping");
        //httpClient.getConnectionManager().shutdown();
    }

    @Override
    public void destroyService() {
        log.info("HTTP Channel Client Adaptor " + getName() + " is stopped");
        NameRegistrar.unregister(getName());
    }

    public void initChannel() throws ConfigurationException {
        Element persist = getPersist();
        Element e = persist.getChild("channel");
        if (e == null) {
            throw new ConfigurationException("channel element missing");
        }
        String logFlag = (getProperty(getProperties("channel"), "msglog"));
        if ("true".equalsIgnoreCase(logFlag)) {
            setMsgLog(true);
        } else {
            setMsgLog(false);
        }
        setUrl(getProperty(getProperties("channel"), "url"));
        realm = this.getClass().getName() + "/" + getUrl();
        setContentType(getProperty(getProperties("channel"), "content-type"));
    }

    public HttpChannelMsg sendGet(HttpChannelMsg httpReq, int milisTimeoutConnection) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        //SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(SSLContext.getInstance("TLS"), SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        //CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        try {
            RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(milisTimeoutConnection).setConnectTimeout(milisTimeoutConnection).setSocketTimeout(milisTimeoutConnection).build();
            HttpGet httpGet = new HttpGet(getUrl() + httpReq.getAdditionalUrl() + "?" + httpReq.getUrlParam());
            httpGet.setConfig(requestConfig);
            httpGet.addHeader("User-Agent", "ACS/HTTP-Adaptor");
            HttpResponse response = httpClient.execute(httpGet);
            Logger.log(getLog().createLogEvent("send to " + httpGet.getURI().getHost() + httpGet.getURI().getPath(), httpGet.getRequestLine().getUri()));

            HttpChannelMsg rsp = new HttpChannelMsg();
            rsp.setHttpStatusCode(response.getStatusLine().getStatusCode());
            StringWriter writer = new StringWriter();
            IOUtils.copy(response.getEntity().getContent(), writer);
            rsp.setContent(writer.toString());
            Logger.log(getLog().createLogEvent("receive from " + httpGet.getURI().getHost() + httpGet.getURI().getPath(), rsp.getContent()));
            httpClient.close();
            return rsp;
        } catch (IOException io) {
            httpClient.close();
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            io.printStackTrace(printWriter);
            String trace = stringWriter.toString();
            Logger.log(getLog().createLogEvent("IOException", trace));
            return null;
        }
    }

    public HttpChannelMsg sendPost(HttpChannelMsg httpReq, int milisTimeoutConnection) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        //CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        try {
            RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(milisTimeoutConnection).setConnectTimeout(milisTimeoutConnection).setSocketTimeout(milisTimeoutConnection).build();
            StringEntity se = new StringEntity(httpReq.getContent());
            se.setContentType(httpReq.getContentType());
            HttpPost httpPost = new HttpPost(getUrl() + httpReq.getAdditionalUrl());
            httpPost.setConfig(requestConfig);
            httpPost.setHeader("User-Agent", "ACS/HTTP-Adaptor");
            httpPost.setEntity(se);
            Logger.log(getLog().createLogEvent("send to " + httpPost.getURI().getHost() + httpPost.getURI().getPath(), httpReq.getContent()));

            HttpResponse response = httpClient.execute(httpPost);
            HttpChannelMsg rsp = new HttpChannelMsg();
            rsp.setHttpStatusCode(response.getStatusLine().getStatusCode());

            StringWriter writer = new StringWriter();
            IOUtils.copy(response.getEntity().getContent(), writer);
            rsp.setContent(writer.toString());
            Logger.log(getLog().createLogEvent("receive from " + httpPost.getURI().getHost() + httpPost.getURI().getPath(), rsp.getContent()));
            httpClient.close();
            return rsp;
        } catch (IOException io) {
            httpClient.close();
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            io.printStackTrace(printWriter);
            String trace = stringWriter.toString();
            Logger.log(getLog().createLogEvent("IOException", trace));
            return null;
        }
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the msgLog
     */
    public boolean isMsgLog() {
        return msgLog;
    }

    /**
     * @param msgLog the msgLog to set
     */
    public void setMsgLog(boolean msgLog) {
        this.msgLog = msgLog;
    }
}
