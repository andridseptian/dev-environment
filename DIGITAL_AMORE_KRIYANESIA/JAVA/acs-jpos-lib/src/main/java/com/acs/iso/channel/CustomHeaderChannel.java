package com.acs.iso.channel;

import com.acs.util.Utility;
import java.io.IOException;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

/**
 *
 * @author Erwin
 */
public class CustomHeaderChannel extends BaseChannel implements Configurable {

    private Configuration cfg;
    private String customHeader;

    public CustomHeaderChannel() {
        super();
    }

    @Override
    public void connect() {
        LogEvent evt = new LogEvent(this, "connect");
        try {
            super.connect();
        } catch (IOException ex) {
            evt.addMessage("Unable to Connect");
        } finally {
            Logger.log(evt);
        }
    }

    @Override
    public void disconnect() {
        LogEvent evt = new LogEvent(this, "disconnect");
        try {
            super.disconnect();
        } catch (IOException ex) {
            evt.addMessage(ex);
        }
    }

    @Override
    public void send(ISOMsg m) throws IOException, ISOException {
        LogEvent evt = new LogEvent(this, "send");
        m.setPackager(getDynamicPackager(m));
        String msg = new String(m.pack());
        msg = customHeader + msg;
        evt.addMessage(msg);
        evt.addMessage(m);
        String ml = Utility.str2bcd(msg.length());
        byte[] dl = ml.getBytes("ISO8859_1");
        byte[] dm = msg.getBytes("ISO8859_1");
        byte[] dt = new byte[dl.length + dm.length];
        System.arraycopy(dl, 0, dt, 0, dl.length);
        System.arraycopy(dm, 0, dt, dl.length, dm.length);
        serverOut.write(dt);
        serverOut.flush();
        setChanged();
        org.jpos.util.Logger.log(evt);
    }

    @Override
    public ISOMsg receive() throws IOException, ISOException {
        LogEvent evt = new LogEvent(this, "receive");
        ISOMsg m = createMsg();
        m.setPackager(getDynamicPackager(m));
        m.setSource(this);
        synchronized (serverInLock) {
            int len = getMessageLength();
            int hLen = getHeaderLength();
            byte[] b = new byte[2];
            int ctr = this.serverIn.read(b);
            if (len > 0) {
                if (ctr > 0) {
                    int l = Utility.bcd2str(b);
                    byte[] d = new byte[l];
                    this.serverIn.read(d);
                    String msg = new String(d).substring(7);
                    evt.addMessage(new String(d));
                    m.unpack(msg.getBytes());
                    evt.addMessage(m);
                    org.jpos.util.Logger.log(evt);
                }
            }
        }
        return m;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
        customHeader = cfg.get("custom-header", "");
    }

}
