package com.acs.iso.channel;

import com.acs.util.Utility;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.iso.ChannelAdaptor;

/**
 *
 * @author Erwin
 */
public class AutoNetmanSenderChannelAdaptor extends ChannelAdaptor {

    Timer timer;
    TimerTask task;
    boolean run = false;
    boolean signOn = false;

    @Override
    public void startService() {
        super.startService();
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                doNetman();
            }
        };
        timer.schedule(task, new Date(new Date().getTime() + 5000), cfg.getLong("echo-delay", 30000));
    }

    @Override
    public void stopService() {
        super.stopService();
        if (timer != null) {
            timer.cancel();
        }
        if (task != null) {
            task.cancel();
        }
        signOn = false;
    }

    public void doNetman() {
        if (!run) {
            run = true;
            try {
                String header = cfg.get("netman-header", "");
                String pc = cfg.get("netman-pc", "");
                String tid = cfg.get("netman-tid", "");
                String nii = cfg.get("netman-nii", "");
                ISOMsg r = new ISOMsg();
                if(!header.equalsIgnoreCase("")) {
                    r.setHeader(ISOUtil.hex2byte(header));
                }
                r.setMTI("0800");
                if(!pc.equalsIgnoreCase("")) {
                    r.set(3, pc);
                }
                r.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
                r.set(11, Utility.generateStan());
                if(!nii.equalsIgnoreCase("")) {
                    r.set(24, nii);
                }
                if(!tid.equalsIgnoreCase("")) {
                    r.set(41, tid);
                }
                if (!signOn) {
                    r.set(70, "001");
                    if (this.isConnected()) {
                        this.send(r);
                        signOn = true;
                    } else {
                        log.error("Not connected to host, Sign On / Handshake Failed");
                        signOn = false;
                    }
                } else {
                    r.set(70, "301");
                    this.send(r);
                }
            } catch (ISOException ex) {
                log.error(ex.getMessage());
            } finally {
                run = false;
            }
        } else {
            log.error("Echo Task is still running");
        }
    }

}
