// Copyright (c) Micro Pay Nusantara 2010
package com.acs.smpp;

import ie.omk.smpp.Connection;
import ie.omk.smpp.event.SMPPEventAdapter;
import ie.omk.smpp.message.BindResp;
import ie.omk.smpp.message.SMPPProtocolException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Element;
import org.jpos.core.Configurable;
import org.jpos.core.ConfigurationException;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.QFactory;
import org.jpos.util.Loggeable;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author Erwin
 */
public class Smpp extends QBeanSupport implements SmppMBean, Configurable, Loggeable, Runnable {

    private Connection conn;
    Thread thisThread;
    private int port, reconnect;
    private String host, bindingType, username, password;
    private boolean bind = true;

    public Smpp() {
        super();
    }

    public Smpp(int port, String host, String username, String password) {
        super();
        this.port = port;
        this.host = host;
        this.username = username;
        this.password = password;
    }

    @Override
    public void initService() {
        NameRegistrar.register(getName(), this);
    }

    @Override
    public void run() {
//        while (!exit) {
        Thread.currentThread().setName("smpp-connector");
        while (running()) {
            try {
                if (conn == null || conn.getState() == 0) {
                    connect();
                }
            } catch (SMPPProtocolException ex) {
                log.error(ex);
            }
        }
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException ie) {
                log.error(ie);
            }
        }
//        }
    }

    private void connect() {
        try {
            conn = new Connection(host, port, true);
            conn.autoAckLink(true);
            conn.autoAckMessages(true);
            addHandlers();
        } catch (UnknownHostException uhe) {
            setBind(false);
            log.error(uhe);
        }
        boolean retry = false;
        //while (!retry) {
        try {
            int binding = Connection.RECEIVER;
            if ("transceiver".equals(bindingType)) {
                binding = Connection.TRANSCEIVER;
            } else if ("transmitter".equals(bindingType)) {
                binding = Connection.TRANSMITTER;
            } else if ("receiver".equals(bindingType)) {
                binding = Connection.RECEIVER;
            }
            BindResp r = conn.bind(binding, username, password, "SMPP");
            setBind(true);
            retry = true;
        } catch (IOException ioe) {
            setBind(false);
            try {
                log.info("trying to rebinding to " + host + " " + port + " in " + reconnect);
                Thread.sleep(reconnect);
            } catch (InterruptedException ie) {
                log.error(ie);
            }
        } catch (NullPointerException npe) {
            setBind(false);
            try {
                log.error(npe);
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Smpp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //}
    }

    @Override
    public void dump(PrintStream stream, String string) {
    }

    public Connection getConnection() {
        return conn;
    }

    public void initServer() throws ConfigurationException {
        if (port == 0) {
            throw new ConfigurationException("Port value not set");
        }
        if (host == null) {
            throw new ConfigurationException("Host is null");
        }
        if (username == null) {
            throw new ConfigurationException("Username value not set");
        }
        if (password == null) {
            throw new ConfigurationException("Password is null");
        }
        if (bindingType == null) {
            throw new ConfigurationException("Binding Type is not set");
        }
        if (reconnect == 0) {
            throw new ConfigurationException("Reconnect is not set");
        }
        thisThread = new Thread(this);
        thisThread.start();
    }

    @Override
    public void startService() throws ConfigurationException {
        initServer();
    }

    @Override
    public void stopService() {
        setBind(false);
//        unsetShortname();
        new Thread("SMPPServer-shutdown") {
            @Override
            public void run() {
                try {
                    shutdownServer();
                } catch (IOException ex) {
                    log.error(ex);
                }
            }
        }.start();

    }

    private void shutdownServer() throws IOException {
        setBind(false);
        if (conn != null && conn.isBound()) {
            conn.unbind();
//            conn.force_unbind();
//            conn.closeLink();
        }
        if (thisThread != null) {
//            thisThread.interrupt();
            thisThread.stop();
        }
    }

    @Override
    public void destroyService() {
        setBind(false);
        NameRegistrar.unregister(this.getName());
    }

    /**
     * @param reconnect
     * @jmx:managed-attribute description="Server port"
     */
    @Override
    public synchronized void setReconnect(int reconnect) {
        this.reconnect = reconnect;
        setAttr(getAttrs(), "reconnect", Integer.valueOf(reconnect));
        setModified(true);
    }

    /**
     * @return 
     * @jmx:managed-attribute description="Server port"
     */
    @Override
    public int getReconnect() {
        return reconnect;
    }

    /**
     * @param port
     * @jmx:managed-attribute description="Server port"
     */
    @Override
    public synchronized void setPort(int port) {
        this.port = port;
        setAttr(getAttrs(), "port", Integer.valueOf(port));
        setModified(true);
    }

    /**
     * @return @jmx:managed-attribute description="Server port"
     */
    @Override
    public int getPort() {
        return port;
    }

    /**
     * @param host
     * @jmx:managed-attribute description="Server host"
     */
    @Override
    public synchronized void setHost(String host) {
        this.host = host;
        setAttr(getAttrs(), "host", host);
        setModified(true);
    }

    /**
     * @return @jmx:managed-attribute description="Server host"
     */
    @Override
    public String getHost() {
        return host;
    }

    /**
     * @param username
     * @jmx:managed-attribute description="Server username"
     */
    @Override
    public synchronized void setUsername(String username) {
        this.username = username;
        setAttr(getAttrs(), "username", username);
        setModified(true);
    }

    /**
     * @return @jmx:managed-attribute description="Server username"
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * @param password
     * @jmx:managed-attribute description="Server password"
     */
    @Override
    public synchronized void setPassword(String password) {
        this.password = password;
        setAttr(getAttrs(), "password", password);
        setModified(true);
    }

    /**
     * @return @jmx:managed-attribute description="Server password"
     */
    @Override
    public String getPassword() {
        return password;
    }

    private void addHandlers() {
        QFactory factory = getFactory();
        Iterator iter = getPersist().getChildren("smpp-handler").iterator();
        while (iter.hasNext()) {
            try {
                Element l = (Element) iter.next();
                SMPPEventAdapter listener = (SMPPEventAdapter) factory.newInstance(l.getAttributeValue("class"));
//                ConnectionObserver listener = (ConnectionObserver) factory.newInstance(l.getAttributeValue("class"));
                factory.setLogger(listener, l);
                factory.setConfiguration(listener, l);
                conn.addObserver(listener);
            } catch (ConfigurationException ex) {
                log.error(ex);
            }
        }
    }

    @Override
    public void setBindingType(String bindingType) {
        this.bindingType = bindingType;
        setAttr(getAttrs(), "bindingType", bindingType);
        setModified(true);
    }

    @Override
    public String getbindingType() {
        return bindingType;
    }

    /**
     * @return the bind
     */
    public boolean isBind() {
        return bind;
    }

    /**
     * @param bind the bind to set
     */
    public void setBind(boolean bind) {
        this.bind = bind;
    }
}
