/*
 * jPOS Project [http://jpos.org]
 * Copyright (C) 2000-2010 Alejandro P. Revilla
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.acs.http.channel;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.naming.ConfigurationException;
import javax.net.ssl.*;
import org.jdom.Element;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.QBeanSupport;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.util.Logger;
import org.jpos.util.NameRegistrar;

/**
 * HttpChannelAdaptor connects and disconnects a channel for every message
 * exchange.
 * 
 * <p>Example qbean:</p>
 * &lt;client class="org.jpos.q2.iso.HttpChannelAdaptor" logger="Q2" name="channel-adaptor"&gt;<br>
 *   &lt;channel ...<br>
 *     ...<br>
 *     ...<br>
 *   &lt;/channel&gt;<br>
 *   &lt;space&gt;space&lt;/space&gt;<br>
 *   &lt;max-connections&gt;5&lt;/max-connections&gt;<br>
 *   &lt;max-connect-attempts&gt;15&lt;/max-connect-attempts&gt;<br>
 *   &lt;in&gt;send&lt;/in&gt;<br>
 *   &lt;out&gt;receive&lt;/out&gt;<br>
 * &lt;/client&gt;<br>
 *
 *
 */
public class HttpChannelAdaptorX extends QBeanSupport
    implements HttpChannelAdaptorMBean
{
    Space<String,Object> sp;
    String in, out;
    long delay;
    int maxConnections;
    int maxConnectAttempts;
    String protocol;
    
    public HttpChannelAdaptorX () {
        super ();
    }

    //@SuppressWarnings("unchecked")
    private Space<String,Object> grabSpace (Element e) {
        return (Space<String,Object>) SpaceFactory.getSpace (e != null ? e.getText() : "");
    }

    /**
     * Initialize HttpChannel Adaptor.
     * initialize parameter are
     * space
     * input
     * output
     * delay
     * maximum-connection
     *
     */
    public void initAdaptor() {
        Element persist = getPersist ();
        sp = grabSpace (persist.getChild ("space"));
        in = persist.getChildTextTrim ("in");
        out = persist.getChildTextTrim ("out");
        delay = 5000;
        String s = persist.getChildTextTrim ("max-connections");
        maxConnections = (s!=null) ? Integer.parseInt(s) : 1;  // reasonable default
        s = persist.getChildTextTrim ("max-connect-attempts");
        maxConnectAttempts = (s!=null) ? Integer.parseInt(s) : 15;  // reasonable default
    }

    /**
     * Starting HttpChannelAdaptor service.
     */
    @Override
    public void startService () {
        try {
            initAdaptor();
            for (int i=0; i<maxConnections; i++) {
                Worker w = new Worker(i);
                w.initChannel();
                (new Thread(w)).start();
            }
            NameRegistrar.register (getName(), this);           
        } catch (Exception e) {
            getLog().warn ("error starting service", e);
        }
    }
    /**
     * Stopping HttpChannelAdaptor service.
     */
    @Override
    public void stopService () {
        try {
            for (int i=0; i<maxConnections; i++) {
                sp.out(in, new Object());
            }
        } catch (Exception e) {
            getLog().warn ("error stopping service", e);
        }
    }

    /**
     * Destroy service from Q2.
     */
    @Override
    public void destroyService () {
        NameRegistrar.unregister (getName ());
        NameRegistrar.unregister ("channel." + getName ());
    }

    /**
     * Queue a message to be transmitted by this adaptor
     * @param m message to send
     */
    public void send (HttpMsg m) {
        sp.out (in, m);
    }

    /**
     * Queue a message to be transmitted by this adaptor
     * @param m message to send
     * @param timeout in millis
     */
    public void send (HttpMsg m, long timeout) {
        sp.out (in, m, timeout);
    }

    /**
     * Receive HttpMsg response message.
     */
    public HttpMsg receive () {
        return (HttpMsg) sp.in (out);
    }

    /**
     * Receive message
     * @param timeout time to wait for an incoming message
     */
    public HttpMsg receive (long timeout) {
        return (HttpMsg) sp.in (out, timeout);
    }

    public class Worker implements Runnable {
        HttpURLConnection urlc;
        String urlStr;
        String method;
        int id;
        int timeout;

        public Worker (int i) {
            super ();
            id = i;
        }

        /**
        * Pipes everything from the reader to the writer via a buffer
        */
        private void pipe(Reader reader, Writer writer) throws IOException
        {
            char[] buf = new char[2048];
            int read = 0;
            while ((read = reader.read(buf)) >= 0) {
                writer.write(buf, 0, read);
            }
            writer.flush();
        }
        
        @Override
        public void run () {
            Thread.currentThread().setName ("channel-worker-" + id);
            //int[] handbackFields = cfg.getInts ("handback-field");
            while (running ()){
                try {
                    Object o = sp.in (in, timeout);                    
                    if (o instanceof HttpMsg) {
                        String dump;
                        HttpMsg msg = (HttpMsg)o;                        
                        String httpurl = (msg.getProtocol() == null ? protocol : msg.getProtocol()) + "://" + urlStr + (msg.getContextPath() == null ? "" : msg.getContextPath());                        
                        if ("post".equals(msg.getMethod())) {                            
                            URL url = new URL(httpurl);
                            urlc = (HttpURLConnection) url.openConnection();
                            urlc.setDoOutput(true);
                            //urlc.setUseCaches(false);
                            //urlc.setAllowUserInteraction(false);
                            urlc.setDoInput(true);                                                        
                            urlc.setRequestMethod("POST");
                            //timeout
                            urlc.setConnectTimeout(timeout);
                            urlc.setReadTimeout(timeout);
                            //set header
                            for (int i = 0; i < msg.getHeader().size(); i++) {
                                String h[] = (String[]) msg.getHeader().get(i);
                                urlc.setRequestProperty(h[0], h[1]);
                            }
                            OutputStream out = null;
                            InputStream in = null;
                            try {
                                //send
                                //getLog().info("send", );
                                String indent = "    ";
                                dump = "";
                                dump += httpurl + "\n";
                                dump += indent + msg.getRequest();
                                Logger.log(getLog().createLogEvent("send", dump));
                                out = urlc.getOutputStream();
                                OutputStreamWriter writer = new OutputStreamWriter(out);
                                StringReader strreader = new StringReader(msg.getRequest());
                                pipe(strreader, writer);
                                writer.close();
                                                        
                                //receive
                                in = urlc.getInputStream();
                                Reader reader = new InputStreamReader(in);
                                StringWriter output = new StringWriter();
                                pipe(reader, output);
                                msg.setStatusCode(urlc.getResponseCode());
                                msg.setResponse(output.toString());
                                reader.close();
                                dump = "";
                                dump += httpurl + "\n";
                                dump += indent + msg.getResponse();
                                Logger.log(getLog().createLogEvent("receive", msg.getResponse()));
                            } catch (IOException e) {
                                getLog().warn ("channel-worker-"+id, e.getMessage ());                                
                            } finally {
                                if (in != null)
                                    in.close();
                                if (out != null)
                                    out.close();
                            }
                        } else if ("get".equals(msg.getMethod())) {
                            if (msg.getRequest() != null && msg.getRequest().length () > 0) {
                                httpurl += "?" + msg.getRequest();
                            }
                            URL url = new URL(httpurl);
                            Logger.log(getLog().createLogEvent("send", httpurl));
                            urlc = (HttpURLConnection)url.openConnection ();
                            urlc.setDoInput(true);
                            urlc.setRequestMethod("GET");
                            //timeout
                            urlc.setConnectTimeout(timeout);
                            urlc.setReadTimeout(timeout);
                            //set header
                            for (int i = 0; i < msg.getHeader().size(); i++) {
                                String h[] = (String[]) msg.getHeader().get(i);
                                urlc.setRequestProperty(h[0], h[1]);
                            }
                            InputStream in = null;
                            try {                                
                                //receive
                                in = urlc.getInputStream();
                                BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                                StringBuffer sb = new StringBuffer();
                                String line;
                                while ((line = rd.readLine()) != null) {
                                    sb.append(line);
                                }
                                rd.close();
                                msg.setStatusCode(urlc.getResponseCode());
                                msg.setResponse(sb.toString());
                                Logger.log(getLog().createLogEvent("receive", msg.getResponse()));
                            } catch (IOException e) {                                
                                getLog().warn ("channel-worker-"+id, e.getMessage ());                                
                            } finally {
                                if (in != null)
                                    in.close();
                            }
                        }
                        sp.out(out, msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getLog().warn ("channel-worker-"+id, e.getMessage ());
                    ISOUtil.sleep (1000);
                } finally {
                    try {
                        
                    } catch (Exception e) {
                        //getLog().warn ("channel-worker-"+id, e.getMessage ());
                    }
                }
            }
        }
        
        public void initChannel() throws ConfigurationException {
            Element persist = getPersist ();
            Element e = persist.getChild ("channel");
            if (e == null)
                throw new ConfigurationException ("channel element missing");
            protocol = getProperty (getProperties ("channel"), "protocol");
            String host = getProperty (getProperties ("channel"), "host");
            int port = 0;
            try {
                port = Integer.parseInt (
                    getProperty (getProperties ("channel"), "port")
                );
            } catch (NumberFormatException ex) {
                getLog().error (ex);
            }
            timeout = 0;
            try {
                timeout = Integer.parseInt (
                    getProperty (getProperties ("channel"), "timeout")
                );
            } catch (NumberFormatException ex) {
                getLog().error (ex);
            }
            String contextpath = getProperty (getProperties ("channel"), "contextpath");
            method = getProperty (getProperties ("channel"), "method");
            urlStr = host + (port == 0 ? "" : ":" + port) + contextpath;
            if ("https".equals(protocol)) {
                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                    @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    @Override
                        public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    @Override
                        public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
                };
                // Install the all-trusting trust manager
                try {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                        @Override
                        public boolean verify(String host, SSLSession ssls) {
                            return true;
                        }
                   });
                } catch (Exception ex) {
                   getLog().error (ex);
                }
            }            
        }
        
    }

    @Override
    public synchronized void setInQueue (String in) {
        String old = this.in;
        this.in = in;
        if (old != null)
            sp.out (old, new Object());

        getPersist().getChild("in").setText (in);
        setModified (true);
    }

    @Override
    public String getInQueue () {
        return in;
    }

    @Override
    public synchronized void setOutQueue (String out) {
        this.out = out;
        getPersist().getChild("out").setText (out);
        setModified (true);
    }

    @Override
    public String getOutQueue () {
        return out;
    }

    @Override
    public synchronized void setHost (String host) {
        setProperty (getProperties ("channel"), "host", host);
        setModified (true);
    }
    @Override
    public String getHost () {
        return getProperty (getProperties ("channel"), "host");
    }

    @Override
    public synchronized void setPort (int port) {
        setProperty (
            getProperties ("channel"), "port", Integer.toString (port)
        );
        setModified (true);
    }
    @Override
    public int getPort () {
        int port = 0;
        try {
            port = Integer.parseInt (
                getProperty (getProperties ("channel"), "port")
            );
        } catch (NumberFormatException e) {
            getLog().error (e);
        }
        return port;
    }

    @Override
    public synchronized void setContextPath (String path) {
        setProperty (getProperties ("channel"), "contextpath", path);
        setModified (true);
    }
    /**
     * Get context
     *
     * @return contextPath
     */
    @Override
    public String getContextPath () {
        return getProperty (getProperties ("channel"), "contextpath");
    }

    
    /*
    public synchronized void setSocketFactory (String sFac) {
        setProperty(getProperties("channel"), "socketFactory", sFac);
        setModified(true);
    }
    public String getSocketFactory() {
        return getProperty(getProperties ("channel"), "socketFactory");
    }*/
}