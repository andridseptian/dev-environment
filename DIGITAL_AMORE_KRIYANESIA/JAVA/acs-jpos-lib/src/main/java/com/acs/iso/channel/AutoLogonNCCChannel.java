/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.iso.channel;

import com.acs.util.Utility;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.NCCChannel;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

/**
 *
 * @author EliteBook
 */
public class AutoLogonNCCChannel extends NCCChannel {

    boolean autoLogOn = true;
    String de7Format = "MMddHHmmss";
    String nmic = "001";
    String mti = "0800";
    LogEvent evt;

    public AutoLogonNCCChannel() {
        super();
    }

    @Override
    public void connect() {
        evt = new LogEvent(this, "autologon-ncc-channel");
        try {
            super.connect();
            if (autoLogOn) {
                doLogOn();
            }
        } catch (IOException ex) {
            evt.addMessage(ex.getMessage());
        } catch (ISOException isoe) {
            evt.addMessage(isoe.getMessage());
        } finally {
            Logger.log(evt);
        }
    }

    public void doLogOn() throws ISOException, IOException {
        ISOMsg r = createMsg();
        r.setMTI(mti);
        r.set(7, new SimpleDateFormat(de7Format).format(new Date()));
        r.set(11, Utility.generateStan());
        r.set(70, nmic);
        if (this.isConnected()) {
            synchronized (serverOutLock) {
                this.send(r);
            }
            synchronized (serverInLock) {
                ISOMsg receive;
                receive = this.receive();
                if (receive == null) {
                    evt.addMessage("Log On Timeout");
                } else {
                    evt.addMessage("Log On Success");
                }
            }
        }
    }

}
