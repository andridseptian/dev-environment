package com.acs.nodectrl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.eclipse.jetty.http.HttpStatus;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOServer;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.iso.ChannelAdaptor;
import org.jpos.q2.iso.QServer;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

/**
 *
 * @author Erwin
 */
public class NodeCtrl extends QBeanSupport {

    @Override
    public void initService() {
        Spark.setPort(cfg.getInt("port", 4567));
        NameRegistrar.register(getName(), this);
    }

    @Override
    public void startService() {
        get(new Route("/node") {

            @Override
            public Object handle(Request request, Response response) {
                try {
                    Thread.sleep(3500);
                } catch (InterruptedException ex) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    log.error(stringWriter.toString());
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                }
                JSONObject json = new JSONObject();
                JSONArray fileList = new JSONArray();
                File folder = new File("deploy");
                File[] listOfFiles = folder.listFiles();
                SAXBuilder builder = new SAXBuilder();
                for (File file : listOfFiles) {
                    JSONObject jo = new JSONObject();
                    jo.put("running", true);
                    jo.put("connected", false);
                    if (file.isFile()) {
                        String filename = file.getName();
                        jo.put("filename", filename);
                        try {
                            Document doc = builder.build("deploy/" + file.getName());
                            Element rootElement = doc.getRootElement();
                            String name = rootElement.getAttributeValue("name", rootElement.getName());
                            String description = rootElement.getAttributeValue("desc", "");
                            if (!name.contains("-mux")
                                    && !name.equalsIgnoreCase("Q2")
                                    && !name.equalsIgnoreCase("spring")
                                    && !name.equalsIgnoreCase("logback-config")
                                    && !name.contains("spring")
                                    && !name.equalsIgnoreCase("nodectrl")) {
                                jo.put("name", name);
                                jo.put("desc", description);
                                Object o = NameRegistrar.getIfExists(name);
                                if (o != null) {
                                    jo.put("running", true);
                                    if (o instanceof ChannelAdaptor) {
                                        ChannelAdaptor ca = (ChannelAdaptor) o;
                                        ISOChannel channel = (ISOChannel) NameRegistrar.getIfExists("channel." + ca.getName());
                                        if (channel.isConnected()) {
                                            jo.put("connected", true);
                                        }
                                    } else if (o instanceof QServer) {
                                        QServer qs = (QServer) o;
                                        ISOServer server = (ISOServer) NameRegistrar.getIfExists("server." + qs.getName());
                                        ISOChannel channel = server.getLastConnectedISOChannel();
                                        if (channel != null && channel.isConnected()) {
                                            jo.put("connected", true);
                                        }
                                    }
                                } else {
                                    jo.put("running", false);
                                }
                                fileList.put(jo);
                            }
                        } catch (JDOMException ex) {
                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            ex.printStackTrace(printWriter);
                            log.error(stringWriter.toString());
                        } catch (IOException ex) {
                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            ex.printStackTrace(printWriter);
                            log.error(stringWriter.toString());
                        } catch (Exception ex) {
                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            ex.printStackTrace(printWriter);
                            log.error(stringWriter.toString());
                        }
                    }
                }
                json.put("fileList", fileList);
                response.status(HttpStatus.OK_200);
                return json.toString();
            }
        });

        get(new Route("/node/view") {

            @Override
            public Object handle(Request request, Response response) {
                String filename = request.queryParams("filename");
                try {
                    byte[] encoded = Files.readAllBytes(Paths.get("deploy/" + filename));
                    response.status(HttpStatus.OK_200);
                    return new String(encoded, "UTF-8");
                } catch (IOException ex) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    log.error(stringWriter.toString());
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "File not found";
                }
            }
        });

        put(new Route("/node/update/:filename") {

            @Override
            public Object handle(Request request, Response response) {
                String filename = request.params("filename");
                String content = request.body();
                if (new File("deploy/" + filename).exists()) {
                    try {
                        String msg = content;
                        Files.write(Paths.get("deploy/" + filename), msg.getBytes());
                        response.status(HttpStatus.OK_200);
                        return "Update file : " + filename + " Success";
                    } catch (IOException ex) {
                        log.error(ex.getMessage());
                        response.status(HttpStatus.PRECONDITION_FAILED_412);
                        return "Failed to update file";
                    }
                } else {
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "File not found";
                }
            }
        });

        post(new Route("/node/save/:filename") {

            @Override
            public Object handle(Request request, Response response) {
                String filename = request.params("filename");
                String content = request.body();
                if (new File("deploy/" + filename).exists()) {
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "File already exist";
                } else {
                    try {
                        String msg = content;
                        Files.write(Paths.get("deploy/" + filename), msg.getBytes());
                        response.status(HttpStatus.OK_200);
                        return "Save file : " + filename + " Success";
                    } catch (IOException ex) {
                        StringWriter stringWriter = new StringWriter();
                        PrintWriter printWriter = new PrintWriter(stringWriter);
                        ex.printStackTrace(printWriter);
                        log.error(stringWriter.toString());
                        response.status(HttpStatus.PRECONDITION_FAILED_412);
                        return "Failed to update file";
                    }
                }
            }
        });

        post(new Route("/node/stop") {

            @Override
            public Object handle(Request request, Response response) {
                String filename = request.queryParams("filename");
                try {
                    if (new File("deploy/" + filename).exists()) {
                        byte[] encoded = Files.readAllBytes(Paths.get("deploy/" + filename));
                        Files.deleteIfExists(Paths.get("deploy/" + filename));
                        Files.write(Paths.get("deploy/" + filename + ".OFF"), encoded);
                        response.status(HttpStatus.OK_200);
                        return "Stopping : " + filename + " Success";
                    } else {
                        response.status(HttpStatus.PRECONDITION_FAILED_412);
                        return "File " + filename + " not exists!!";
                    }
                } catch (IOException ex) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    log.error(stringWriter.toString());
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "Failed to update file";
                } catch (Exception ex) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    log.error(stringWriter.toString());
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "Failed to update file";
                }
            }
        });

        post(new Route("/node/start") {

            @Override
            public Object handle(Request request, Response response) {
                String filename = request.queryParams("filename");
                File oldFile = new File("deploy/" + filename);
                File newFile = new File("deploy/" + filename.replace(".OFF", "").replace(".off", ""));
                if (oldFile.exists()) {
                    if (oldFile.renameTo(newFile)) {
                        response.status(HttpStatus.OK_200);
                        return "Start " + filename.replace(".OFF", "").replace(".off", "") + " Success";
                    } else {
                        response.status(HttpStatus.PRECONDITION_FAILED_412);
                        return "Start " + filename.replace(".OFF", "").replace(".off", "") + " Failed";
                    }
                } else {
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "File Not Found";
                }
            }
        });

        delete(new Route("/node/delete/:filename") {

            @Override
            public Object handle(Request request, Response response) {
                String filename = request.params("filename");
                try {
                    if (Files.deleteIfExists(Paths.get("deploy/" + filename))) {
                        response.status(HttpStatus.OK_200);
                        return "File " + filename + " successfully deleted!!";
                    } else {
                        response.status(HttpStatus.PRECONDITION_FAILED_412);
                        return "File " + filename + " not exists!!";
                    }
                } catch (IOException ex) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    log.error(stringWriter.toString());
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "Failed to update file";
                } catch (Exception ex) {
                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    log.error(stringWriter.toString());
                    response.status(HttpStatus.PRECONDITION_FAILED_412);
                    return "Failed to update file";
                }
            }
        });
    }

    @Override
    public void destroyService() {
        if (NameRegistrar.getIfExists(getName()) != null) {
            NameRegistrar.unregister(getName());
        }
    }

}
