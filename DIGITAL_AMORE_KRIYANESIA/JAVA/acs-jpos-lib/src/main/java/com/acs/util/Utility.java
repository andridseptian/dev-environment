/*
 * Utility.java
 *
 * Created on August 12, 2008, 1:43 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.acs.util;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;

/**
 *
 * @author dhacker
 */
public class Utility {

    static ArrayList modules = new ArrayList();
    static final String Encoding = "ISO8859_1";
    static final int PadLeft = 0;
    static final int PadRight = 1;

    /**
     * Convert value to amount format, "##,###,###,###".
     *
     * @param val
     * @return Formatted amount.
     */
    public static String formatAmount(double val) {
        String ret = "";
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("##,###,###,###");
        ret = decFrm.format(val).replace(',', '.');
        return ret;
    }

    public static String stringAmount(double val) {
        String ret;
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("#");
        ret = decFrm.format(val);
        return ret;
    }
    
    public static String stringAmount(BigDecimal val) {
        val.setScale(0, BigDecimal.ROUND_DOWN);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(0);
        df.setGroupingUsed(false);
        return df.format(val);
    }

    public static String zeropad(String i, int len) {
        try {
            return ISOUtil.zeropad(i, len);
        } catch (ISOException isoe) {
            return i.substring(0, len);
        }
    }

    /**
     * Get settle date from given format. Settle date is H + 1
     *
     * @param format
     * @return Settled from given parameter.
     */
    public static String getReconDate(String format) {
        String ret = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        long t = System.currentTimeMillis();
        try {
            {
                //add 1 day
                t += (24 * 60 * 60 * 1000);
                Date dateadd = new Date(t);
                cal.setTime(dateadd);
                ret = df.format(dateadd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static String getDate(String format) {
        String ret = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        long t = System.currentTimeMillis();
        try {
            Date dateadd = new Date(t);
            cal.setTime(dateadd);
            ret = df.format(dateadd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
    
    public static Date getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    /**
     * Get current date time, return value format is yyyyMMddHHmmss
     *
     * @return Current date time String, return value format is yyyyMMddHHmmss
     */
    public static String getCurrentDateTime() {
        String datetime;
        SimpleDateFormat df;
        Date now = new Date();
        df = new SimpleDateFormat("yyyyMMddHHmmss");
        datetime = df.format(now);
        return datetime;
    }

    /**
     * Get formatted date from String s.
     *
     * @param s
     * @param pattern
     * @return Formatted date from String s
     */
    public static Date getDateFromStr(String s, String pattern) {
        Date datetime = null;
        SimpleDateFormat df;
        try {
            df = new SimpleDateFormat(pattern);
            //df.p
            datetime = df.parse(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datetime;
    }

    /**
     * Get string formatted date from Date value.
     *
     * @param dt
     * @param pattern
     * @return Formatted date base on pattern parameter.
     */
    public static String getStrFromDate(Date dt, String pattern) {
        String ret = null;
        SimpleDateFormat df;
        try {
            df = new SimpleDateFormat(pattern);
            ret = df.format(dt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Get settle date from given date. Settle date is H + 1
     *
     * @param date
     * @return Settle date from given parameter.
     */
    public static String getSettleDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String ret = "";

        if (date == null) {
            return ret;
        }

        if (date.equals("00000000")) {
            return ret;
        }

        try {
            df.parse(date);
            ret = date;
        } catch (Exception e) {
            e.printStackTrace();
            ret = "";
        }
        return ret;
    }

    /**
     * Convert format date from fromFormat to toFormat
     *
     * @param date
     * @param fromFormat
     * @param toFormat
     * @return String
     */
    public static String formatDate(String date, String fromFormat, String toFormat) {
        SimpleDateFormat src = new SimpleDateFormat();
        SimpleDateFormat dst = new SimpleDateFormat();

        if (date == null || date.trim().length() == 0) {
            return "";
        }

        if (fromFormat == null || fromFormat.trim().length() == 0 || toFormat == null || toFormat.trim().length() == 0) {
            return null;
        }
        try {
            src.applyPattern(fromFormat);
            dst.applyPattern(toFormat);
            String result = dst.format(src.parse(date));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get MD5 value of string s
     *
     * @param s
     * @return ret
     */
    public static String hash(String s, String algorithm) {
        String ret = "";
        try {
            MessageDigest msg = MessageDigest.getInstance(algorithm);
            msg.update(s.getBytes());
            byte[] hash = msg.digest();
            ret = ISOUtil.hexString(hash);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    /**
     * Count character ch, in String s
     *
     * @param s
     * @param ch
     * @return amount of character in String s
     */
    public static int charCount(String s, String ch) {
        int ret = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (ch.equalsIgnoreCase(s.substring(i, i + 1))) {
                ret++;
            }
        }
        return ret;
    }

    /*
     * public static String convertThBl2BlTh(String data) { String ret =
     * (data.substring(4, 6) + data.substring(0, 4)); return ret; }
     *
     * public static String convertBlTh2ThBl(String data) { String ret =
     * (data.substring(2, 6) + data.substring(0, 2)); return ret; }
     */
    /**
     * Generate random unique ID.
     *
     * @return random generate UUID.
     */
    public static String getUUID() {
        UUID gen = UUID.randomUUID();
        String uuid = gen.toString();
        uuid = uuid.replace("-", "");
        uuid = uuid.toUpperCase();
        return uuid;
    }

    public static String generateRrn() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
        String dateString = sdf.format(new Date());
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return dateString + s.substring(s.length() - 6);
    }

    public static String generateEdcRrn() throws ISOException {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return ISOUtil.zeropad(s.substring(s.length() - 6), 12);
    }

    public static String generateStan() {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return s.substring(s.length() - 6);
    }

    public static String generateRandomNumber(int length) {
        UUID gen = UUID.randomUUID();
        long randomNumber = Math.abs(gen.getLeastSignificantBits());
        String stringRep = String.valueOf(randomNumber);
        String reff1 = stringRep.substring(0, length);
        return reff1;
    }

    public static byte[] decrypt(String path, String plaintext) throws
            IllegalBlockSizeException,
            BadPaddingException,
            NoSuchPaddingException,
            InvalidKeySpecException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(path));
        byte[] rawkey = new byte[(int) path.length()];
        in.readFully(rawkey);
        in.close();

        DESedeKeySpec keyspec = new DESedeKeySpec(rawkey);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey key = keyfactory.generateSecret(keyspec);
        Cipher cipher = Cipher.getInstance("DES");
        byte[] ciphertext = cipher.doFinal(plaintext.getBytes());
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plaindecrypt = cipher.doFinal(ciphertext);
        return plaindecrypt;
    }

    /**
     * Hold decrypt until finish time.
     *
     * @param finish
     */
    public static void waitUntil(int finish) {
        int c = 0;
        while (true) {
            if (c < finish) {
                ISOUtil.sleep(1000);
                c++;
                continue;
            } else {
                break;
            }
        }
    }

    public static byte[] hex2Byte(String h) {
        if (h.length() % 2 != 0) {
            h = "0" + h;
        }

        int l = h.length() / 2;

        byte[] r = new byte[l];

        int i = 0;
        int j = 0;
        for (int k = h.length(); i < k; j++) {
            r[j] = Short.valueOf(h.substring(i, i + 2), 16).byteValue();

            i += 2;
        }

        return r;
    }

    public static String hex2String(String h) {
        return new String(hex2Byte(h));
    }

    public static String byte2Hex(byte[] b) {
        StringBuffer sbuf = new StringBuffer();

        int i = 0;
        for (int n = b.length; i < n; i++) {
            byte hiByte = (byte) ((b[i] & 0xF0) >> 4);
            byte loByte = (byte) (b[i] & 0xF);

            sbuf.append(Character.forDigit(hiByte, 16));
            sbuf.append(Character.forDigit(loByte, 16));
        }

        return sbuf.toString();
    }

    public static String String2Hex(String s) {
        return byte2Hex(s.getBytes());
    }

    public static String str2bcd(int argInt) {
        try {
            ByteArrayOutputStream o = new ByteArrayOutputStream();
            o.write(argInt >> 8);
            o.write(argInt);

            return o.toString("ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "0";
    }

    public static int bcd2str(String argInt) {
        try {
            byte[] b = argInt.getBytes("ISO8859_1");

            return (b[0] & 0xFF) << 8 | b[1] & 0xFF;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static int bcd2str(byte[] argInt) {
        try {
            return (argInt[0] & 0xFF) << 8 | argInt[1] & 0xFF;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String pad(String m, int pos, String with, int total) {
        if (m.length() < total) {
            StringBuffer s = new StringBuffer(m);

            while (s.length() < total) {
                if (pos == 0) {
                    s.insert(0, with);
                } else {
                    if (pos != 1) {
                        continue;
                    }
                    s.append(with);
                }
            }

            return s.toString();
        }

        if (pos == 0) {
            return m.substring(m.length() - total);
        }
        if (pos == 1) {
            return m.substring(0, total);
        }

        return m.substring(0, total);
    }

    public static String pad(String m, String pos, String with, int total) {
        if (m.length() < total) {
            StringBuffer s = new StringBuffer(m);

            while (s.length() < total) {
                if ("L".equals(pos)) {
                    s.insert(0, with);
                } else if ("R".equals(pos)) {
                    s.append(with);
                }
            }
            return s.toString();
        }

        if ("L".equals(pos)) {
            return m.substring(m.length() - total);
        }
        if ("R".equals(pos)) {
            return m.substring(0, total);
        }

        return m.substring(0, total);
    }

    public static String decode7bit(String src) {
        String result = null;
        String temp = null;
        byte left = 0;

        if ((src != null) && (src.length() % 2 == 0)) {
            result = "";
            int[] b = new int[src.length() / 2];
            temp = src + "0";

            int i = 0;
            int j = 0;
            for (int k = 0; i < temp.length() - 2; j++) {
                b[j] = Integer.parseInt(temp.substring(i, i + 2), 16);

                k = j % 7;
                byte srcAscii = (byte) (b[j] << k & 0x7F | left);
                result = result + (char) srcAscii;
                left = (byte) (b[j] >>> 7 - k);

                if (k == 6) {
                    result = result + (char) left;
                    left = 0;
                }
                if (j == src.length() / 2) {
                    result = result + (char) left;
                }
                i += 2;
            }
        }

        return result.trim();
    }
    
    public static Boolean compareDate(Date date1, Date date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddmmyy");
        String dateString1 = sdf.format(date1);
        
        String dateString2 = sdf.format(date2);
        if(dateString1.equals(dateString2)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public static boolean isValidValue(Pattern pattern, String value) {
        return pattern.matcher(value).matches();
    }
    
    
}
