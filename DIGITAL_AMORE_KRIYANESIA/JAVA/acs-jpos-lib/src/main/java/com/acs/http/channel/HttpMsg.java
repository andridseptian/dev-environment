/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.http.channel;

import java.util.ArrayList;

/**
 *
 * @author dhacker
 */
public class HttpMsg {
    String protocol;
    String contextpath;
    String method;
    String req;
    String rsp;
    int statusCode;
    ArrayList headers;

    public HttpMsg(String protocol, String method) {
        this.protocol = protocol;
        this.method = method;
        this.headers = new ArrayList();
    }

    public HttpMsg() {
        this.headers = new ArrayList();
    }

    public String getMethod() {
        return this.method;
    }
    public void setMethod(String method) {
        this.method = method;
    }

    public String getProtocol() {
        return this.protocol;
    }
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getRequest() {
        return this.req;
    }
    public void setRequest(String msg) {
        this.req = msg;
    }

    public String getResponse() {
        return this.rsp;
    }
    public void setResponse(String msg) {
        this.rsp = msg;
    }

    public int getStatusCode() {
        return this.statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setContextPath(String contextpath) {
        this.contextpath = contextpath;
    }
    public String getContextPath() {
        return this.contextpath;
    }
        
    public void addHeader(String key, String value) {
        headers.add(new String[] {key, value});
    }
    public ArrayList getHeader() {
        return this.headers;
    }
    public void setHeader(ArrayList headers) {
        this.headers = headers;
    }

}
