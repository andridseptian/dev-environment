$(document).ready(function () {
    $("#img-logo").attr("src", "./img/icon/login/login-logo.gif");
    setLoading("Memuat Komponen");
    setTimeout(() => {
        $("#modal-loading").modal('hide');
        $("#view-pages").show();
        javaConnector.getLembagaData();
        javaConnector.getLoginData();
        javaConnector.getTerminalData();
        downloadConnector.checkUpdate();

        $("#pane-registration").hide();

        buttonListener.init();

        try {
            $("#img-logo").attr("src", "./img/icon/login/login-logo.gif");
            // $("#img-logo").show(1000);
            $("#pane-login").show();

            $(window).resize(function () {
                if ($(window).width() <= 800) {
                    $('#side-nav').hide(500, 'swing');
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $('#side-nav').show(500, 'swing');
                    $("#menu-panes").css("padding-left", "350px");
                }
            });

        } catch (error) {
            alert(error);
        }
    }, 5000);

});

function setAlert(message) {
    try {
        $("#modal-loading").modal('hide');
        $("#alert-title").html("Informasi")
        $("#alert-body").html(message);
        $("#alert-footer").html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>`);
        $("#modal-alert").modal('toggle');
    } catch (error) {
        alert(error);
    }
}

function setLoading(message) {
    try {
        $("#loading-subtitle").html(message);
        $("#modal-loading").modal('toggle');
    } catch (error) {
        alert(error);
    }
}

class activate {
    static showActivate() {
        $("#modal-activate").modal('toggle');
    }

    static saveActivate() {
        $("#modal-activate").modal('hide');
        setAlert('fungsi belum tersedia');
    }
}


var jsConnector = {
    set: function (result) {

    }
};

function getJsConnector() {
    return jsConnector;
};

