class billing {
    static show() {
        try {

            var reqBody = JSON.stringify({
                bpr: globalBankId
            });

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://103.28.148.203:5050/test/extension_inquiry_bpr",
                "method": "POST",
                "headers": {
                    "content-type": "application/json"
                },
                "processData": false,
                "data": reqBody
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                // alert(response);

                var res = JSON.parse(response);
                if (res.rm.tagihan != undefined) {
                    // alert(JSON.stringify(res.rm.tagihan));

                    var dataArray = [];
                    res.rm.tagihan.forEach(element => {
                        dataArray.push({
                            Total: "Rp " + element.Total,
                            Date: element.Date.substring(0, 11)
                        });
                    });

                    setTimeout(() => {
                        var tableBilling = $('#billing-table').DataTable({
                            data: dataArray,
                            autoWidth: false,
                            columns: [
                                { "data": "Total", "title": "Tagihan", "width": "300px" },
                                { "data": "Date", "title": "Tanggal", "width": "300px" },

                            ],
                            initComplete: function (settings, json) {
                                $('#billing-detail').html(`
                                    <h4>Total Bayar: Rp ${res.rm.amount}</h4>
                                    <p>Due Date: ${res.rm.due_date}</p>
                                    <p>Invoice Terakhir: ${res.rm.invoice}</p>
                                    <h3>Kode Bayar: ${res.rm.payment_code}</h3>

                                    <small>*Anda dapat melakukan pencetakan invoice anda melalui wesite SIP</small>
                                `);
                            }
                        });
                    }, 500);
                } else {
                    // alert(JSON.stringify(res.rm));
                    $('#billing-detail').html(`
                        <h3>${res.rm.rm}</h3>
                        <h4>Due Date: ${res.rm.due_date.substring(0, 11)}</h4>
                    `);
                }
            });
        } catch (error) {
            alert(error);
        }
    }
}