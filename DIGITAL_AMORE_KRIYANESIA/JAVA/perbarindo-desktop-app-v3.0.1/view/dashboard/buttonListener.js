class buttonListener {
    static menuButtonListener() {

        $("#menu-fold").click(function () {
            if ($(window).width() > 800) {
                if ($("#side-nav").is(":visible")) {
                    $("#side-nav").toggle("fast", "swing");
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $("#side-nav").toggle("fast", "swing");
                    $("#menu-panes").css("padding-left", "350px");
                }
            } else {
                $("#side-nav").toggle("fast", "swing");
                $("#menu-panes").css("padding-left", "50px");
            }
        });

        $("#a-dashboard").click(function () {
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-dashboard").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-webportal").click(function () {

            $("#page-dashboard").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-webportal").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-returndata").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-returndata").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-manuser").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $("#page-manterminal").hide();

            $("#page-manuser").show("fast");

            setTimeout(() => {
                javaConnector.sendRequestFromJS('getuserlist', 'null');
            }, 500);

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-manterminal").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $("#page-manuser").hide();

            $("#page-manterminal").show("fast");

            setTimeout(() => {
                javaConnector.sendRequestFromJS('getserialnumber', 'null');
            }, 500);

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-offlinektp").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-billing").hide();

            $("#page-offlinektp").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-billing").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();

            $("#page-billing").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#footer-logout").click(function () {
            try {
                setLoading('Membersihkan data....');
                // alert('logout');
                javaConnector.sendRequestFromJS('logout', JSON.stringify({
                    username: globalOperatorUserName
                }));
                setTimeout(() => {
                    window.open('login.html');
                }, 5000);
            } catch (error) {
                alert(error);
            }
        });
    }

    static portalButtonListener() {
        portal.buttonListener();
    }

    static returnDataButtonListener() {
        $("#return-inputdata").click(function () {
            returndata.setReturnDataRange();
            javaConnector.getReturnData();
            showInputReturnData();
        });

        $("#return-saveexcel").click(function () {
            javaConnector.saveReturnDataToExcel(JSON.stringify(returnDataContainer));
        });

        var parseReturnMonthAndYear = function () {
            var year = $("#return-year").val();
            var month = $("#return-month").val();

            var monthParse = [
                ["Januari", "01"],
                ["Februari", "02"],
                ["Maret", "03"],
                ["April", "04"],
                ["Mei", "05"],
                ["Juni", "06"],
                ["Juli", "07"],
                ["Agustus", "08"],
                ["September", "09"],
                ["Oktober", "10"],
                ["November", "11"],
                ["Desember", "12"],
            ]

            var monthValue = "false";

            monthParse.forEach(element => {
                if (element[0] == month) {
                    monthValue = element[1];
                }
            });

            return {
                year: year,
                month: monthValue
            }
        }

        $("#returndata-input").click(function () {
            try {
                var data = [
                    $("#returndata-input-date").val(),
                    $("#returndata-input-user").val(),
                    $("#returndata-input-nik").val(),
                    $("#returndata-input-cif").val(),
                    `<button id="returndata-button" type="button" class="btn btn-danger btn-sm">delete</button>`
                ];

                returnDataContainer.push(data);

                var data = parseReturnMonthAndYear();
                javaConnector.saveReturnData(data.year, data.month, JSON.stringify(returnDataContainer));

                $('#return-table').DataTable().destroy();

                $('#return-table').DataTable({
                    data: returnDataContainer
                });

            } catch (error) {
                alert(error);
            }
        });

        function getReturnDataByMonthAndYear() {
            try {
                var year = $("#return-year").val();
                var month = $("#return-month").val();

                var monthParse = [
                    ["Januari", "01"],
                    ["Februari", "02"],
                    ["Maret", "03"],
                    ["April", "04"],
                    ["Mei", "05"],
                    ["Juni", "06"],
                    ["Juli", "07"],
                    ["Agustus", "08"],
                    ["September", "09"],
                    ["Oktober", "10"],
                    ["November", "11"],
                    ["Desember", "12"],
                ]

                var monthValue = "false";

                monthParse.forEach(element => {
                    if (element[0] == month) {
                        monthValue = element[1];
                    }
                });

                // alert(year + " " + monthValue);

                $("#return-table").html("tidak ada data");

                var data = parseReturnMonthAndYear();

                javaConnector.getReturnDataByMonthAndYear(data.month, data.year);
            } catch (error) {
                alert(error);
            }
        }

        $("#return-getdatabymonth").click(function () {
            getReturnDataByMonthAndYear();
        })

        $("#return-year").change(function () {
            getReturnDataByMonthAndYear();
        });

        $("#return-month").change(function () {
            getReturnDataByMonthAndYear();
        });

        $("#return-table").on('click', '.btn', function () {
            try {
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                var cell1 = tr.find('td').eq(1).html();
                var cell2 = tr.find('td').eq(2).html();
                var cell3 = tr.find('td').eq(3).html();
                var cell4 = tr.find('td').eq(4).html();

                var output = `["${cell0}","${cell1}","${cell2}","${cell3}",${JSON.stringify(cell4)}]`;

                var temp = JSON.stringify(returnDataContainer)
                    .replace(output, "")
                    .replace(',,', ',')
                    .replace(',]', ']')
                    .replace('[,', '[');

                returnDataContainer = JSON.parse(temp);

                var data = parseReturnMonthAndYear();

                javaConnector.saveReturnData(data.year, data.month, JSON.stringify(returnDataContainer));

                $('#return-table').DataTable().destroy();

                $('#return-table').DataTable({
                    data: returnDataContainer
                });
            } catch (error) {
                alert(error);
            }
        });

        $("#return-openportal").click(function () {
            try {
                portalConnector.openWebPortalDatabalikan();
            } catch (error) {
                alert(error);
            }
        });
    }

    static connectionButtonListener() {
        $("#connection-continue").click(function () {
            setAlert('silahkan lanjutkan koneksi anda');
            counterTick = -1;
            portalConnector.continueVPN();
        });

        $("#connection-disconnect").click(function () {
            setAlert('koneksi anda akan diputuskan');
            counterTick = -1;
            portalConnector.disconnectingFromVPN();
        });
    }

    static disclaimerButtonListener() {
        $("#generate-disclaimer").click(function () {
            try {
                $("#modal-disclaimer").modal('toggle');

                $("#checked-by").html(globalOperatorFullname);

                $("#disclaimer-input-nama").change(function () {
                    $("#approved-by").html(this.value);
                });
            } catch (error) {
                alert(error);
            }
        });

        $("#disclaimer-cb-alamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-alamat").prop('disabled', false);
            } else {
                $("#disclaimer-input-alamat").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-detailalamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-rtrw").prop('disabled', false);
                $("#disclaimer-input-keldesa").prop('disabled', false);
                $("#disclaimer-input-kec").prop('disabled', false);
            } else {
                $("#disclaimer-input-rtrw").prop('disabled', true);
                $("#disclaimer-input-keldesa").prop('disabled', true);
                $("#disclaimer-input-kec").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-status").click(function () {
            if (this.checked) {
                $("#disclaimer-input-kerja").prop('disabled', false);
                $("#disclaimer-input-kawin").prop('disabled', false);
                $("#disclaimer-input-warga").prop('disabled', false);
            } else {
                $("#disclaimer-input-kerja").prop('disabled', true);
                $("#disclaimer-input-kawin").prop('disabled', true);
                $("#disclaimer-input-warga").prop('disabled', true);
            }
        });

        $("#disclaimer-footer").html(`
            <button id="disclaimer-disagree" type="button" class="btn btn-danger" 
                data-dismiss="modal">Tidak Setuju</button>
            <button id="disclaimer-portal-agree" type="button" class="btn btn-primary">Setuju</button>
        `)

        $("#disclaimer-portal-agree").click(function () {

            if (
                $("#disclaimer-input-nik").val() != "" &&
                $("#disclaimer-input-nama").val() != "" &&
                $("#disclaimer-input-tanggal").val() != "" &&
                $("#disclaimer-input-tempat").val() != ""
            ) {

                $("#modal-disclaimer").modal('toggle');

                var data = {
                    nik: $("#disclaimer-input-nik").val(),
                    nama: $("#disclaimer-input-nama").val(),
                    tanggal: $("#disclaimer-input-tanggal").val(),
                    tempat: $("#disclaimer-input-tempat").val(),
                    kelamin: $("#disclaimer-input-kelamin").val(),
                    alamat: $("#disclaimer-input-alamat").val(),
                    rtrw: $("#disclaimer-input-rtrw").val(),
                    keldesa: $("#disclaimer-input-keldesa").val(),
                    kec: $("#disclaimer-input-kec").val(),
                    kawin: $("#disclaimer-input-kawin").val(),
                    kerja: $("#disclaimer-input-kerja").val(),
                    warga: $("#disclaimer-input-warga").val()
                }

                javaConnector.createDiscalimerToPDF(JSON.stringify(data));
            } else {
                setAlert('form nik,nama dan tempat tanggal lahir tidak boleh kosong');
            }
        });
    }

    static OfflineKTPButtonListener() {
        $("#offline-scan").click(() => {
            javaConnector.ktpInitialize();
            setLoading('Mengakses alat scan ktp, pastikan alat telah terhubung....');
            $("#img-loading").attr("src", "./img/assets/scanner/plug-device.gif");
            // setTimeout(() => {
            //     $("#modal-loading").modal('hide');
            //     setTimeout(() => {
            //         setLoading('Memeriksa ktp anda....');
            //         javaConnector.scanKTP();
            //     }, 500);
            // }, 5000);
        });

        $("#offline-conf").click(() => {
            setTimeout(() => {
                $("#modal-offlineconf").modal('toggle');

                $("#offlineconf-sync").click(() => {
                    javaConnector.sendRequestFromJS('setdeviceid', JSON.stringify({
                        pcid: $("#offlineconf-input-pcid").val(),
                        conf: $("#offlineconf-input-conf").val()
                    }));;
                })
            }, 300);
        })

        $("#offline-disclaimer").click(() => {
            try {
                $("#modal-disclaimer").modal('toggle');

                $("#checked-by").html(globalOperatorFullname);

                $("#disclaimer-input-nama").change(function () {
                    $("#approved-by").html(this.value);
                });

                if (jsKTP != "") {

                    //#region jsKTP Body
                    /* 
                        var ktp = [
                        ["NIK", `: ${jsKTP.nik}`],
                        ["Nama", `: ${jsKTP.nama}`],
                        ["Tempat/Tgl Lahir", `: ${jsKTP.ttl}`],
                        ["Jenis Kelamin", `: ${jsKTP.kelamin}`],
                        ["Alamat", `: ${jsKTP.alamat}`],
                        ["RT/RW", `: ${jsKTP.rtrw}`],
                        ["Kel/Desa", `: ${jsKTP.kel} / ${jsKTP.kota}`],
                        ["Kecamatan", `: ${jsKTP.kec}`],
                        ["Agama", `: ${jsKTP.agama}`],
                        ["Status Perkawinan", `: ${jsKTP.status}`],
                        ["Pekerjaan", `: ${jsKTP.pekerjaan}`],
                        ["Kewarganegaraan", `: ${jsKTP.warga}`]
                        ];
                    */
                    //#endregion

                    try {
                        var ttl = jsKTP.ttl.split(",");
                    } catch (error) { }

                    $("#approved-by").html(jsKTP.nama);

                    $("#disclaimer-input-nik").val(jsKTP.nik);
                    $("#disclaimer-input-nama").val(jsKTP.nama);
                    $("#disclaimer-input-tanggal").val(ttl[1]);
                    $("#disclaimer-input-tempat").val(ttl[0]);
                    $("#disclaimer-input-kelamin").val(jsKTP.kelamin);
                    $("#disclaimer-input-alamat").val(jsKTP.alamat);
                    $("#disclaimer-input-rtrw").val(jsKTP.rtrw);
                    $("#disclaimer-input-keldesa").val(jsKTP.kel);
                    $("#disclaimer-input-kec").val(jsKTP.kec);
                    $("#disclaimer-input-kawin").val(jsKTP.status);
                    $("#disclaimer-input-kerja").val(jsKTP.pekerjaan);
                    $("#disclaimer-input-warga").val(jsKTP.warga);

                };

            } catch (error) {
                alert(error);
            }
        });

        $("#disclaimer-cb-alamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-alamat").prop('disabled', false);
            } else {
                $("#disclaimer-input-alamat").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-detailalamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-rtrw").prop('disabled', false);
                $("#disclaimer-input-keldesa").prop('disabled', false);
                $("#disclaimer-input-kec").prop('disabled', false);
            } else {
                $("#disclaimer-input-rtrw").prop('disabled', true);
                $("#disclaimer-input-keldesa").prop('disabled', true);
                $("#disclaimer-input-kec").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-status").click(function () {
            if (this.checked) {
                $("#disclaimer-input-kerja").prop('disabled', false);
                $("#disclaimer-input-kawin").prop('disabled', false);
                $("#disclaimer-input-warga").prop('disabled', false);
            } else {
                $("#disclaimer-input-kerja").prop('disabled', true);
                $("#disclaimer-input-kawin").prop('disabled', true);
                $("#disclaimer-input-warga").prop('disabled', true);
            }
        });

        $("#disclaimer-footer").html(`
            <button id="disclaimer-disagree" type="button" class="btn btn-danger" 
                data-dismiss="modal">Tidak Setuju</button>
            <button id="disclaimer-offline-agree" type="button" class="btn btn-primary">Setuju</button>
        `)

        $("#disclaimer-offline-agree").click(function () {
            if (
                $("#disclaimer-input-nik").val() != "" &&
                $("#disclaimer-input-nama").val() != "" &&
                $("#disclaimer-input-tanggal").val() != "" &&
                $("#disclaimer-input-tempat").val() != ""
            ) {
                $("#modal-disclaimer").modal('toggle');
                var data = {
                    nik: $("#disclaimer-input-nik").val(),
                    nama: $("#disclaimer-input-nama").val(),
                    tanggal: $("#disclaimer-input-tanggal").val(),
                    tempat: $("#disclaimer-input-tempat").val(),
                    kelamin: $("#disclaimer-input-kelamin").val(),
                    alamat: $("#disclaimer-input-alamat").val(),
                    rtrw: $("#disclaimer-input-rtrw").val(),
                    keldesa: $("#disclaimer-input-keldesa").val(),
                    kec: $("#disclaimer-input-kec").val(),
                    kawin: $("#disclaimer-input-kawin").val(),
                    kerja: $("#disclaimer-input-kerja").val(),
                    warga: $("#disclaimer-input-warga").val()
                }
                javaConnector.createDiscalimerToPDF(JSON.stringify(data));
            } else {
                setAlert('form nik,nama dan tempat tanggal lahir tidak boleh kosong');
            }
        });

        $("#dukcapil-sync").click(function () {
            $("#modal-offlinesync").modal('toggle');
        });
    }
}