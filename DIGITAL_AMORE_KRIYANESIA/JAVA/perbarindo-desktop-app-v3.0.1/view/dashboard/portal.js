class portal {

    static buttonListener() {
        $("#portal-openapp").click(function () {
            try {
                setLoading('Menghubungkan ke jaringan bersama');
                portalConnector.openWebPortalCheckNikViaVPN();
            } catch (error) {
                alert(error);
            }
        });

        $("#portal-openbrowser").click(function () {
            try {
                setLoading('Menghubungkan ke jaringan bersama');
                portalConnector.openWebPortalCheckNik();
            } catch (error) {
                alert(error);
            }
        });
    }

    static connectingToVPN() {
        javaConnector.connectingToVPN('browser');
    }

    static connectedPortalApplied() {

    }

    static connectedToVPN() {
        function checkConnection() {
            portalConnector.checkVPNConnection();
            if (counterTick != -1) {
                setTimeout(checkConnection, 5000);
            }
        } checkConnection()
    }

    static vpnNotConnected() {
        setAlert('Terputus dari jaringan bersama');
        counterTick = -1;
        $('#modal-connection').modal('hide');
        portalConnector.cancelVPN();
    }

    static disconnectFromVPN() {
        javaConnector.disconnectingFromVPN();
    }

    static showScheduledTimeOut() {

    }
}