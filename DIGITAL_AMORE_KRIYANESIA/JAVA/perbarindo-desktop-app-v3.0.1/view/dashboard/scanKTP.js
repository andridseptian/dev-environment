class scanKTP {
    static setKTPResult(result) {
        var jsKTP = JSON.parse(result);

        $('offline-result').html(result);
    }

    static scanningKTP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Memeriksa data ktp, letakkan ktp pada alat pembaca ktp dan tunggu hingga proses selesai...');
            $("#img-loading").attr("src", "./img/assets/scanner/put-card.gif");
            javaConnector.scanKTP();
        }, 500);
    }

    static scanningFingerPrint() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Memeriksa data sidik jari, Letakkan jari telunjuk yang akan di scan pada alat pembaca sidik jari');
            $("#img-loading").attr("src", "./img/assets/scanner/put-finger.gif");
            // javaConnector.scanKTP();
        }, 500);
    }

    static failedReadKTP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Gagal, mengulang pembacaan');
            scanningFingerPrint()
        }, 2000);
    }

    static failedReadFP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Sidik jari tidak cocok, mengulang pembacaan');
            scanningFingerPrint()
        }, 1000);
    }
}