class manuser {
    static getUserList(data) {
        try {
            var arrData = JSON.parse(data);
            /* 
            <tr>
                <th>username</th>
                <th>Nama</th>
                <th>No HP</th>
                <th>Email</th>
                <th>Kadaluarsa Password</th>
                <th>Status</th>
                <th> - </th>
            </tr>
            */
            var html = ``;
            arrData.forEach(element => {
                html += `<tr class="row-of-data">`

                html += `<td class="userid">${element.user_id}</td>`
                html += `<td class="fullname">${element.fullname}</td>`
                html += `<td class="phone">${element.no_hp}</td>`
                html += `<td class="email">${element.email}</td>`
                html += `<td class="expired">${element.expired_password}</td>`
                if (element.status == 0) {
                    html += `<td class="status">tidak aktif</td>`
                    html += `<td class="action">
                                <button type="button" class="man-activate btn btn-success btn-sm">aktifkan</button>
                            </td>`
                } else if (element.status == 1) {
                    html += `<td class="status">aktif</td>`
                    html += `<td class="action">
                                <button type="button" class="man-block btn btn-danger btn-sm">non-aktifkan</button>
                            </td>`
                } else if (element.status == 2) {
                    html += `<td class="status">terblokir</td>`
                    html += `<td class="action">
                                <button type="button" class="man-activate btn btn-warning btn-sm">aktifkan</button>
                            </td>`
                }
                html += `</tr>`
            });

            $('#manuser-body').html(html);
            $('#manuser-table').DataTable();

            $('.man-activate').click(function () {
                setLoading('Mengaktifkan user');
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                javaConnector.sendRequestFromJS('activateuser', cell0);
                setTimeout(() => {
                    javaConnector.sendRequestFromJS('getuserlist', 'null');
                }, 1000);

                // alert(cell0);
            });

            $('.man-block').click(function () {
                setLoading('Menonaktifkan User');
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                javaConnector.sendRequestFromJS('deactivateuser', cell0);
                setTimeout(() => {
                    javaConnector.sendRequestFromJS('getuserlist', 'null');
                }, 1000);
                // alert(cell0);
            });

        } catch (error) {
            alert(error);
        }
    }

    static hidemenu() {
        $('#a-manuser').hide();
    }
}