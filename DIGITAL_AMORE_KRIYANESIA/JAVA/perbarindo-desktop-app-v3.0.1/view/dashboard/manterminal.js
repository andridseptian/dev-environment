class manterminal {
    static setTerminalList(data) {
        try {
            var arrData = JSON.parse(data);
            /* 
                "bank_id"
                "serial_number"
                "mac"
                "status"
            */
            var html = ``;
            arrData.forEach(element => {
                html += `<tr class="row-of-data">`

                html += `<td class="serial_number">${element.serial_number}</td>`
                if (element.mac == undefined) {
                    html += `<td class="mac"> </td>`
                    html += `<td class="status">tidak aktif</td>`
                    html += `<td class="hostname"> </td>`
                    html += `<td class="processorid"> </td>`
                    html += `<td class="processorname"> </td>`
                    html += `<td class="action"> </td>`
                } else {
                    html += `<td class="mac">${element.mac}</td>`
                    html += `<td class="status">aktif</td>`
                    html += `<td class="hostname">${element.terminal.host_name}</td>`
                    html += `<td class="processorid">${element.terminal.processor_id}</td>`
                    html += `<td class="processorname">${element.terminal.processor_name}</td>`
                    html += `<td class="action">
                                <button type="button" class="manterminal-resetserial btn btn-danger btn-sm">reset</button>
                            </td>`
                }
                html += `</tr>`
            });

            $('#manterminal-body').html(html);
            $('#manterminal-table').DataTable();


            $('.manterminal-resetserial').click(function () {
                setLoading('Mereset Terminal');
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                javaConnector.sendRequestFromJS('resetserialnumber', cell0);
                setTimeout(() => {
                    javaConnector.sendRequestFromJS('getserialnumber', 'null');
                }, 1000);
            })

        } catch (error) {
            alert(error)
        }
    }

    static generateSerialNumber() {
        setLoading('Generate Serial Number');
        setTimeout(() => {
            javaConnector.sendRequestFromJS('generateserialnumber', 'null');
        }, 1000);
    }

    static refreshGenerateSerial() {
        setAlert('Sukses Generates Serial Number');
        setTimeout(() => {
            javaConnector.sendRequestFromJS('getserialnumber', 'null');
        }, 1000);
    }

    static hidemenu() {
        $('#a-manterminal').hide();
    }
}