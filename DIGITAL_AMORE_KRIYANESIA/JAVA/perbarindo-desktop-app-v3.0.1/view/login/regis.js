class regis {
    static submit() {
        try {
            if (
                $('#regis-fullname').val() != "" &&
                $('#regis-username').val() != "" &&
                $('#regis-nik').val() != "" &&
                $('#regis-email').val() != "" &&
                $('#regis-phonenumber').val() != "" &&
                $('#regis-password').val() != "" &&
                $('#regis-confpassword').val() != ""
            ) {
                setLoading("sedang memeriksa data anda");

                var password = $("#regis-password").val();
                var confpassword = $("#regis-confpassword").val();

                if (password == confpassword) {
                    var data = {
                        username: $('#regis-username').val(),
                        fullname: $('#regis-fullname').val(),
                        email: $('#regis-email').val(),
                        nik: $('#regis-nik').val(),
                        phonenumber: $('#regis-phonenumber').val(),
                        password: $('#regis-password').val()
                    };

                    var reqBody = JSON.stringify({
                        bank_id: $('#config-bankid').val()
                    })

                    const settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "http://117.54.12.171:61003/sb/checkAdminAvailability",
                        "method": "POST",
                        "headers": {
                            "content-type": "application/json"
                        },
                        "processData": false,
                        "data": reqBody
                    };

                    $.ajax(settings).done(function (response) {
                        setTimeout(() => {
                            var resBody = JSON.parse(response);
                            if (resBody.rc == "04") {
                                $('#modal-loading').modal('hide');
                                $('#modal-adminconfirm').modal();

                                $('#adminconfirm-yes').click(() => {
                                    setLoading("mendaftarkan user");
                                    data.privillage = 1;
                                    javaConnector.sendRequestFromJS('registration', JSON.stringify(data));
                                });

                                $('#adminconfirm-no').click(() => {
                                    setLoading("mendaftarkan user");
                                    data.privillage = 0;
                                    javaConnector.sendRequestFromJS('registration', JSON.stringify(data));
                                });
                            } else {
                                data.privillage = 0;
                                javaConnector.sendRequestFromJS('registration', JSON.stringify(data));
                            }
                        }, 2000);
                    });
                } else {
                    setAlert('Konfirmasi password anda belum sesuai, coba sekali lagi');
                }
            } else {
                setAlert('Form data harus diisi semua, tidak boleh ada yg kosong');
            }
        } catch (error) {
            alert(error);
        }
    }

    static response() {
        $("#modal-loading").modal('hide');
        $("#pane-registration").hide();
        $("#pane-login").show("fast", "swing");
    }

    static onSucceed(message) {
        setAlert(message);
        $("#modal-loading").modal('hide');
        $("#pane-registration").hide();
        $("#pane-login").show("fast", "swing");
    }
}