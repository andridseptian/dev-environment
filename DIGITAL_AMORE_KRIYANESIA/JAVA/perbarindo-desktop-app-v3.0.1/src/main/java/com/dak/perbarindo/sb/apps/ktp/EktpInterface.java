/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.ktp;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

/**
 *
 * @author ACS-Ilham
 */
public interface EktpInterface extends Library {

    EktpInterface INSTANCE = (EktpInterface) Native.loadLibrary(Ektp.dllPath, EktpInterface.class);

    int EktpInitDevice(PointerByReference phContext);

    int EktpReadPsamUid(Pointer hContext, byte[] uidData);

    int EktpSetConfig(Pointer hContext, String pcid, String conf);

    int EktpReadCard(Pointer hContext, byte[] ktpData, byte[] fpData);

    int EktpReleaseDevice(Pointer hContext);

    int CaptureFingerPrint(byte[] imageData, IntByReference nfiqScore);

    int MatchFingerPrint(String imageStr, int imageSize,
            String ktpFpStr, int ktpFpStrSize,
            IntByReference score, IntByReference matched);
}
