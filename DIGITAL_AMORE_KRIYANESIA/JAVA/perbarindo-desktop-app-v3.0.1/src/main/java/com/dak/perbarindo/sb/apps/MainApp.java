package com.dak.perbarindo.sb.apps;

import com.dak.perbarindo.sb.apps.utility.VpnConnection;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class MainApp extends Application {

    private static final Logger log = LogManager.getLogger(MainApp.class);

    public static final boolean developement = true;
    
    public static final JSONObject buildData = new JSONObject()
            .put("buildname", "elephant")
            .put("buildversion", (developement ? "3.0.1-Beta Debugging" : "3.0.1"))
            .put("buildnumber", "build.43");

    public static final JSONObject IP = new JSONObject()
            .put("gateway", "sb3.srv.co.id")
            .put("vpn", "sb3.srv.co.id")
            .put("cloud", "103.28.148.203");

    public static boolean isDevelopement() {
        return developement;
    }

    public static JSONObject getBuildData() {
        return buildData;
    }

    public static JSONObject getIP() {
        return IP;
    }

    @Override
    public void start(Stage stage) throws Exception {

        VpnConnection.disconnectVPN();
        
        log.info("Starting App...");
        log.info("\n/***\n"
                + " *      ___ _             _             ___               _        _    _ _   _          ____\n"
                + " *     / __| |_  __ _ _ _(_)_ _  __ _  | _ ) __ _ _ _  __| |_ __ _(_)__| | |_| |_   __ _|__ /\n"
                + " *     \\__ \\ ' \\/ _` | '_| | ' \\/ _` | | _ \\/ _` | ' \\/ _` \\ V  V / / _` |  _| ' \\  \\ V /|_ \\\n"
                + " *     |___/_||_\\__,_|_| |_|_||_\\__, | |___/\\__,_|_||_\\__,_|\\_/\\_/|_\\__,_|\\__|_||_|  \\_/|___/\n"
                + " *                              |___/                                                        \n"
                + " */");

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("BPR Sharing Bandiwdth EKTP v3");
        stage.getIcons().add(new Image("/icon/Logo.png"));
        stage.initStyle(StageStyle.DECORATED);
        stage.setMinWidth(800);
        stage.setMinHeight(600);
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(event -> {
            try {
                log.info("prepare for closing stage");
                VpnConnection.disconnectVPN();
                Thread.sleep(1000);
                log.info("attempt close");
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
