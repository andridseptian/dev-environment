/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.ktp;

/**
 *
 * @author dak-alex
 */
public class EktpReaderData {
    public static String ktpStr = null;
    public static String ktpFpStr = null;

    public static String getKtpStr() {
        return ktpStr;
    }

    public static void setKtpStr(String ktpStr) {
        EktpReaderData.ktpStr = ktpStr;
    }

    public static String getKtpFpStr() {
        return ktpFpStr;
    }

    public static void setKtpFpStr(String ktpFpStr) {
        EktpReaderData.ktpFpStr = ktpFpStr;
    }
    
    
}
