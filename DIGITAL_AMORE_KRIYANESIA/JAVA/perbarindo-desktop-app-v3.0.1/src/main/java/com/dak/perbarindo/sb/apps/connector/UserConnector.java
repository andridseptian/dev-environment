/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.connector;

import com.dak.perbarindo.sb.apps.controller.MainController;
import com.dak.perbarindo.sb.apps.request.RequestData;
import javafx.application.Platform;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author drid
 */
public class UserConnector {

    private static final Logger log = LogManager.getLogger(UserConnector.class);
    private WebView webView;
    private MainController mc;
    private JSObject jsConnector;
    private JSObject billingJsConnector;

    public UserConnector(MainController mainController) {
        this.mc = mainController;
        this.webView = mc.webView;
    }

    public void sendRequestFromJS(String path, String body) {
        mc.sleep(1000);
        new Thread(() -> {
            mc.sleep(500);
            try {
                JSONObject response = RequestData.send(path, body);
                if (!response.has("fail")) {
                    if (response.has("success")) {
                        log.info(path + " has been success");
                        Platform.runLater(() -> {
                            webView.getEngine().executeScript(response.getString("success"));
                        });
                    } else if (response.has("execute")) {
                        log.info(path + " has been success to execute");
                        Platform.runLater(() -> {
                            if (response.getString("parent").equals("regis")) {
                                jsConnector = (JSObject) webView.getEngine().executeScript("getRegisConnector()");
                            }
                            jsConnector.call(response.getString("execute"), response.getJSONObject("data"));
                        });
                    } else {
                        mc.sendAlertToJS(response.getString("rm"));
                    }
                } else {
                    mc.sendAlertToJS(response.getString("fail"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                mc.sendAlertToJS("Gagal memproses request tampilan, " + e.getMessage());
            }
        }).start();
    }

    public void checkBilling(String bank_id) {
        new Thread(() -> {
            try {
                JSONObject reqBody = new JSONObject().put("bpr", bank_id);

                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, reqBody.toString());
                Request request = new Request.Builder()
                        .url("http://103.28.148.203:5050/test/extension_inquiry_bpr")
                        .post(body)
                        .addHeader("Content-Type", "application/json")
                        .build();

                Response response = client.newCall(request).execute();

                Platform.runLater(() -> {
                    try {
                        billingJsConnector = (JSObject) webView.getEngine().executeScript("getBillingJsConnector()");
                        billingJsConnector.call("resultBilling", response.body().string());
                    } catch (Exception e) {
                        log.error(new String(), e);
                    }
                });

            } catch (Exception e) {
                log.error(new String(), e);
            }
        }).start();
    }

}
