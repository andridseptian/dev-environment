/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.utility;

import com.dak.perbarindo.sb.apps.connector.JavaConnector;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dak-alex
 */
public class Tools {
    
    private static final Logger log = LogManager.getLogger(Tools.class);

    public static String crLogId() {

        LocalDateTime myDateObj = LocalDateTime.now();
        System.out.println("Before formatting: " + myDateObj);
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("mmss");
        String formattedDate = myDateObj.format(myFormatObj);
        System.out.println("After formatting: " + formattedDate);
        String id = String.valueOf(getRandomInt(100, 999))  + formattedDate;
        System.out.println("Id: " + id);
        return id;
    }

    public static int getRandomInt(int min, int max) {
        int x = (int) ((Math.random() * ((max - min) + 1)) + min);
        return x;
    }
    
    public static boolean writeFile(String filelocation, String filename, String data) {
        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            log.info("write DATA -");
            log.info(data);

            write.write(data);
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }
    
    public static String readFile(String filelocation, String filename) {
        try {
            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }
}
