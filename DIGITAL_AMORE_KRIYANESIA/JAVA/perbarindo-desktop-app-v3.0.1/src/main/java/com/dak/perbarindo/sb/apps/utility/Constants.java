/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.utility;

import com.dak.perbarindo.sb.apps.MainApp;

/**
 *
 * @author andrids
 */
public class Constants {

    public class path {

        public class name {

            // desc: User Manajement Services
            /* <editor-fold> */
            public static final String LOGIN = "login";
            public static final String LOGOUT = "logout";
            public static final String REGISTRATION = "registration";
            public static final String GET_USER_LIST = "getuserlist";
            public static final String ACTIVATE_USER = "activateuser";
            public static final String DEACTIVATE_USER = "deactivateuser";
            public static final String ACTIVATE_TERMINAL = "activateterminal";
            public static final String SET_DEVICE_ID = "setdeviceid";
            public static final String RESET_PASSWORD_BY_ADMIN = "resetpasswordbyadmin";
            public static final String REQUEST_RESET_PASSWORD_BY_USER = "requestresetpasswordbyuser";
            public static final String RESET_PASSWORD_BY_USER = "resetpasswordbyuser";
            public static final String CHECK_ADMIN_AVAILABILITY = "checkadmin";
            /* END </editor-fold> */
            // desc: Manajemen terminal
            /* <editor-fold> */
            public static final String GET_SERIAL_NUMBER = "getserialnumber";
            public static final String GENERATE_SERIAL_NUMBER = "generateserialnumber";
            public static final String RESET_SERIAL_NUMBER = "resetserialnumber";
            /* END </editor-fold> */

            // desc: VPN Services
            /* <editor-fold> */
            public static final String GET_VPN = "getvpn";
            /* END </editor-fold> */

            // desc:
            /* <editor-fold> */
            public static final String BILLING_NOTIFICATION = "billingnotification";
            /* END </editor-fold> */
        }

        public class url {
//            public static final String IP = "http://117.54.12.173:4419/dukcapil";

            // desc: User Manajement Service
            /* <editor-fold> */
//            public static final String IP_USER = "http://103.28.148.203:61003";
            public static final String IP_USER = "http://117.54.12.171:61003";
            public static final String LOGIN = IP_USER + "/sb/login";
            public static final String LOGOUT = IP_USER + "/sb/logout";
            public static final String REGISTRATION = IP_USER + "/sb/createNewUser";
            public static final String GET_USER_LIST = IP_USER + "/sb/getAllUserByBankId";
            public static final String ACTIVATE_USER = IP_USER + "/sb/activateUser";
            public static final String DEACTIVATE_USER = IP_USER + "/sb/deactivateUser";
            public static final String ACTIVATE_TERMINAL = IP_USER + "/";
            public static final String SET_DEVICE_ID = IP_USER + "/sb/saveDevice";
            public static final String RESET_PASSWORD_BY_ADMIN = IP_USER + "/sb/resetPasswordByAdmin";
            public static final String REQUEST_RESET_PASSWORD_BY_USER = IP_USER + "/sb/requestResetPasswordByUser";
            public static final String RESET_PASSWORD_BY_USER = IP_USER + "/sb/changePassword";
            public static final String CHECK_ADMIN_AVAILABILITY = IP_USER + "/sb/checkAdminAvailability";
            /* END </editor-fold> */

            // desc: Manajemen terminal
            /* <editor-fold> */
//            public static final String IP_TERMINAL = "http://103.28.148.203:61004";
            public static final String IP_TERMINAL = "http://117.54.12.171:61004";
            public static final String GET_SERIAL_NUMBER = IP_TERMINAL + "/sb/getSNbyBankID";
            public static final String GENERATE_SERIAL_NUMBER = IP_TERMINAL + "/sb/generateSerialNumber";
            public static final String RESET_SERIAL_NUMBER = IP_TERMINAL + "/sb/resetSN";
            /* END </editor-fold> */

            // desc: VPN services
            /* <editor-fold> */
            public static final String GET_VPN = "http://vpn.srv.co.id:9980/sbw/vpn/v1/create_vpn";
            /* END </editor-fold> */

            // desc: Billing Services
            /* <editor-fold> */
            public static final String IP_BILLING = "http://103.28.148.203:5050";
            public static final String BILLING_NOTIFICATION = IP_BILLING + "/test/extension_inquiry_bpr";
            /* END </editor-fold> */

        }
    }

    public static String IP_APP_CREATEVPN = "http://crvpn.srv.co.id:9144/dukcapil";
//    public static String IP_APP = "http://srv.co.id:9144/dukcapil";
//    public static String IP_APP = "http://192.168.100.81:9144/dukcapil";
    public static String IP_APP = "http://sb.srv.co.id:9144/dukcapil";
    public static String ENCRYPT = "010203040506070809" + "0A0B0C";
    public static final String TDES_KEY = "P3RB@R1ND0D1G1T4L4M0R3KR1Y4N3S14";
    public static final String TDES_KEY_APP = "APLIKASISHARINGBANDWIDTHENKRIPSI";

    ///////////////Put Data///////////////
    public static final String INPUT_NIK = "nik";
    public static final String INPUT_USERID = "user_id";
    public static final String INPUT_USERNAME = "username";
    public static final String INPUT_USERAPP = "id";
    public static final String INPUT_ID = "id";
    public static final String INPUT_ID_USERAPP = "id_userapp";
    public static final String INPUT_PASSWORD = "password";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String INPUT_IPUSER = "IP_USER";
    public static final String INPUT_MACADDRESS = "MAC_ADDRESS";
    public static final String INPUT_OTP = "OTP";
    public static final String INPUT_REGISTRATION_CODE = "registrationCode";
    public static final String INPUT_BANKID = "bankId";
    public static final String INPUT_DEVICEID = "deviceId";
    public static final String INPUT_PCID = "pcid";
    public static final String INPUT_CONF = "conf";
    public static final String INPUT_STARTDATE = "startDate";
    public static final String INPUT_ENDDATE = "endDate";
    public static final String INPUT_DATA = "data";
    public static final String INPUT_URL = "url";
    public static final String INPUT_JUMLAH = "jumlah";
    public static final String INPUT_IP = "ip";
    public static final String INPUT_ID_LEMBAGA = "id_lembaga";
    public static final String INPUT_NAMA_LEMBAGA = "nama_lembaga";
    public static final String INPUT_TOKEN = "token";
    public static final String INPUT_PARAMETER = "parameter";
    public static final String INPUT_BYQUESTION = "byquestion";
    public static final String INPUT_SERIALNUM = "serial_number";

    public class DATA_CREATE_USER {

        public static final String USERNAME = "username";
        public static final String NAME = "name";
        public static final String PASSWORD = "password";
        public static final String EMAIL = "email";
        public static final String NOHP = "no_hp";
        public static final String NIK = "nik";
        public static final String NIP = "nip";
        public static final String WAKTUWILAYAH = "waktu_bagian";
        public static final String MAC = "MAC_ADDRESS";
        public static final String BANKID = "bankId";
    }

    public class DATA_VPN {

        public static final String VPNhost = "vpnhost";
        public static final String VPNname = "vpnname";
        public static final String VPNusername = "vpnusername";
        public static final String VPNpassword = "vpnpassword";
    }
}
