package com.dak.perbarindo.sb.apps.data;

import static com.dak.perbarindo.sb.apps.data.Device.deviceData;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dak-alex
 */
public class KtpOnline {

    private static final Logger log = LogManager.getLogger(KtpOnline.class);

    public static String userid = null;
    public static String password = null;
    public static String url = null;

    public static void init() {
        String data = readFile();
        if (data != null) {
            JSONObject dataKTPConf = new JSONObject(data);
            userid = dataKTPConf.getString("userid");
            password = dataKTPConf.getString("password");
            url = dataKTPConf.getString("url");
        }
    }

    public static String getUserid() {
        return userid;
    }

    public static void setUserid(String userid) {
        KtpOnline.userid = userid;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        KtpOnline.password = password;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        KtpOnline.url = url;
    }

    public static String readFile() {
        try {
            String filelocation = System.getProperty("user.dir") + "\\config";
            String filename = "\\ktponline.conf";

            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
//            sendAlertToJS("Gagal membaca file, " + e.getMessage());
            return null;
        }
    }

    public static boolean writeFile() {
        String filelocation = System.getProperty("user.dir") + "\\config";
        String filename = "\\ktponline.conf";
        try {

            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            JSONObject data = new JSONObject()
                    .put("userid", getUserid())
                    .put("password", getPassword())
                    .put("url", getUrl());

            log.info("write DATA -");
            log.info(data.toString());
            log.info("location: " + filelocation + filename);

            write.write(data.toString());
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }

}
