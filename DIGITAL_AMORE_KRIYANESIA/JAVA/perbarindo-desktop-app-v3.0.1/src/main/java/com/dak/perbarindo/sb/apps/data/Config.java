/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.data;

import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

/**
 *
 * @author ROG
 */
public class Config {

    public static void getConfig() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://103.28.148.203:61006/configuration/get/sshconfig")
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();

        JSONObject jsonRes = new JSONObject(response.body().string());

        JSONObject sshConfig = jsonRes.getJSONObject("ssh");
        JSONObject forwardConfig = jsonRes.getJSONObject("forward");

        portforward.sshHost = sshConfig.getString("ssh_host");
        portforward.sshUser = sshConfig.getString("ssh_user");
        portforward.sshPassword = sshConfig.getString("ssh_pass");
        portforward.sshPort = Integer.parseInt(sshConfig.getString("ssh_port"));

        portforward.webPortalNikHost = forwardConfig.getJSONObject("web_portal_nik_host").getString("ip");
        portforward.webPortalDatabalikanHost = forwardConfig.getJSONObject("web_databalikan_host").getString("ip");

        /* 
            {
                "ssh": {
                    "ssh_host": "117.54.12.173",
                    "ssh_user": "root",
                    "ssh_pass": "qawsed1477",
                    "ssh_port": "7819"
                },
                "forward": {
                    "web_portal_nik_host": {
                        "ip": "172.16.160.52",
                        "path": "/webportal/login"
                    },
                    "web_databalikan_host": {
                        "ip": "172.16.160.52",
                        "path": "/databalikan/login"
                    }
                }
            }
         */
    }

    public static class vpn {

        public static String host = "";

        public static void getConfig() throws IOException {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            Request request = new Request.Builder()
                    .url("http://103.28.148.203:61006/configuration/get/vpnconfig")
                    .method("GET", null)
                    .build();
            Response response = client.newCall(request).execute();

            JSONObject jsonRes = new JSONObject(response.body().string());
            vpn.host = jsonRes.getString("vpn");
        }

        public static String getHost() {
            return host;
        }

        public static void setHost(String host) {
            vpn.host = host;
        }

    }

    public static class portforward {

        public static String sshHost = "";
        public static String sshUser = "";
        public static String sshPassword = "";
        public static int sshPort = 0;

        public static String webPortalNikHost = "";
        public static String webPortalDatabalikanHost = "";

        public portforward() throws IOException {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            Request request = new Request.Builder()
                    .url("http://103.28.148.203:61006/configuration/get/sshconfig")
                    .method("GET", null)
                    .build();
            Response response = client.newCall(request).execute();

            JSONObject jsonRes = new JSONObject(response.body().string());

            JSONObject sshConfig = jsonRes.getJSONObject("ssh");
            JSONObject forwardConfig = jsonRes.getJSONObject("forward");

            portforward.sshHost = sshConfig.getString("ssh_host");
            portforward.sshUser = sshConfig.getString("ssh_user");
            portforward.sshPassword = sshConfig.getString("ssh_pass");
            portforward.sshPort = Integer.parseInt(sshConfig.getString("ssh_port"));

            portforward.webPortalNikHost = forwardConfig.getJSONObject("web_portal_nik_host").getString("ip");
            portforward.webPortalDatabalikanHost = forwardConfig.getJSONObject("web_databalikan_host").getString("ip");
        }

        public static void getConfig() throws IOException {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            Request request = new Request.Builder()
                    .url("http://103.28.148.203:61006/configuration/get/sshconfig")
                    .method("GET", null)
                    .build();
            Response response = client.newCall(request).execute();

            JSONObject jsonRes = new JSONObject(response.body().string());

            JSONObject sshConfig = jsonRes.getJSONObject("ssh");
            JSONObject forwardConfig = jsonRes.getJSONObject("forward");

            portforward.sshHost = sshConfig.getString("ssh_host");
            portforward.sshUser = sshConfig.getString("ssh_user");
            portforward.sshPassword = sshConfig.getString("ssh_pass");
            portforward.sshPort = Integer.parseInt(sshConfig.getString("ssh_port"));

            portforward.webPortalNikHost = forwardConfig.getJSONObject("web_portal_nik_host").getString("ip");
            portforward.webPortalDatabalikanHost = forwardConfig.getJSONObject("web_databalikan_host").getString("ip");
        }

        public static String getSshHost() {
            return sshHost;
        }

        public static void setSshHost(String sshHost) {
            portforward.sshHost = sshHost;
        }

        public static String getSshUser() {
            return sshUser;
        }

        public static void setSshUser(String sshUser) {
            portforward.sshUser = sshUser;
        }

        public static String getSshPassword() {
            return sshPassword;
        }

        public static void setSshPassword(String sshPassword) {
            portforward.sshPassword = sshPassword;
        }

        public static int getSshPort() {
            return sshPort;
        }

        public static void setSshPort(int sshPort) {
            portforward.sshPort = sshPort;
        }

        public static String getWebPortalNikHost() {
            return webPortalNikHost;
        }

        public static void setWebPortalNikHost(String webPortalNikHost) {
            portforward.webPortalNikHost = webPortalNikHost;
        }

        public static String getWebPortalDatabalikanHost() {
            return webPortalDatabalikanHost;
        }

        public static void setWebPortalDatabalikanHost(String webPortalDatabalikanHost) {
            portforward.webPortalDatabalikanHost = webPortalDatabalikanHost;
        }

    }
}
