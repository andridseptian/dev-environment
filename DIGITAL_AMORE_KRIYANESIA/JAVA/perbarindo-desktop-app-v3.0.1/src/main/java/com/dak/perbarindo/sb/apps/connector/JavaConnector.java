/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.connector;

import com.dak.perbarindo.sb.apps.MainApp;
import com.dak.perbarindo.sb.apps.controller.MainController;
import com.dak.perbarindo.sb.apps.data.Device;
import com.dak.perbarindo.sb.apps.data.Lembaga;
import com.dak.perbarindo.sb.apps.data.Terminal;
import com.dak.perbarindo.sb.apps.data.UserOperator;
import com.dak.perbarindo.sb.apps.ktp.Ektp;
import com.dak.perbarindo.sb.apps.ktp.EktpInterface;
import com.dak.perbarindo.sb.apps.request.RequestData;
import com.dak.perbarindo.sb.apps.utility.VpnConnection;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.sun.jna.ptr.IntByReference;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class JavaConnector {

    private static final Logger log = LogManager.getLogger(JavaConnector.class);

    private WebView webView;
    private JSObject jsConnector;

    private TimerTask countingConnection;
    private Timer countingTimer;

    private String urlDukcapil;
    private String portDukcapil;

    private String urlDatabalikan;
    private String portDatabalikan;

    private JSch jsch = new JSch();
    private Session session;
    private int connectionStep = 0;

    public JavaConnector(WebView webView) {
        this.webView = webView;
        try {
            jsConnector = (JSObject) webView.getEngine().executeScript("getJsConnector()");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public void logFromJS(String message) {
        try {
            log.info(message);
        } catch (Exception e) {
            log.error(e);
            sendAlertToJS(e.getMessage());
            e.printStackTrace();
        }
    }

    public void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendRequestFromJS(String path, String body) {
        sleep(1000);
        new Thread(() -> {
            sleep(500);
            try {
                JSONObject response = RequestData.send(path, body);
                if (!response.has("fail")) {
                    if (response.has("success")) {
                        log.info(path + " has been success");
                        Platform.runLater(() -> {
                            webView.getEngine().executeScript(response.getString("success"));
                        });
                    } else {
                        sendAlertToJS(response.getString("rm"));
                    }
                } else {
                    sendAlertToJS(response.getString("fail"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                sendAlertToJS("Gagal memproses request tampilan, " + e.getMessage());
            }
        }).start();
    }

    public void checkLauncherUpdate() {
        log.info("check launcher update");
    }

    public void sendAlertToJS(String message) {
        new Thread(() -> {
            Platform.runLater(() -> {
                String function = "setAlert('[message]')";
                function = function.replace("[message]", message);
                webView.getEngine().executeScript(function);
                log.info("execute command - " + function);
            });
        }).start();
    }

    public void getTerminalData() {
        log.info("save Terminal Data -");
        new Thread(() -> {
            Terminal.init();
            if (Terminal.getMacaddress() != null) {
                JSONObject data = MainApp.buildData
                        .put("macaddress", Terminal.getMacaddress())
                        .put("procid", Terminal.getProcessorid())
                        .put("procname", Terminal.getProcessorname())
                        .put("uuid", Terminal.getUuid());

                Platform.runLater(() -> {
                    String function = "data.setMacAddress(`[message]`)";
                    function = function.replace("[message]", data.toString());
                    webView.getEngine().executeScript(function);
                    log.info("execute command - " + function);
                });
            } else {
                sendAlertToJS("Wah, aplikasi gagal menemukan macaddress.  Pastikan komputermu tidak terhubung ke jaringan private dan restart aplikasimu");
            }
        }).start();
    }

    public void saveLembagaData(String data) {
        log.info("save Lembaga Data -");
        new Thread(() -> {
            try {
                JSONObject dataIn = new JSONObject(data);
                Lembaga.setBankID(dataIn.getString("bankid"));
                Lembaga.setLembagaID(dataIn.getString("lembagaid"));
                Lembaga.setLembagaName(dataIn.getString("lembaganame"));
                Lembaga.setToken(dataIn.getString("token"));
                if (!Lembaga.writeFile()) {
                    sendAlertToJS("Gagal menyimpan data. pastikan anda running aplikasi sebagai administrator");
                } else {
                    sendAlertToJS("Data tersimpan");
                }
            } catch (Exception e) {
                log.error(e);
                e.printStackTrace();
                sendAlertToJS("Gagal menyimpan data. " + e.getMessage());
            }
        }).start();
    }

    public void getLembagaData() {
        log.info("get Lembaga Data -");
        new Thread(() -> {
            String data = Lembaga.readFile();
            if (data != null) {
                Platform.runLater(() -> {
                    String function = "data.setLembaga(`[message]`)";
                    function = function.replace("[message]", data.toString());
                    webView.getEngine().executeScript(function);
                    log.info("execute command - " + function);
                });
            } else {
                log.info("not found");
                sendAlertToJS("Sepertinya anda belum menyimpan data pengturan lembaga, silahkan isi terlebih dahulu form pengaturan lembaga");
            }
        }).start();
    }

    public void getLoginData() {
        log.info("get LOGIN DATA -");
        new Thread(() -> {
            String data = UserOperator.readFile();
            if (data != null) {
                Platform.runLater(() -> {
                    String function = "data.setLoginData(`[message]`)";
                    function = function.replace("[message]", data.toString());
                    webView.getEngine().executeScript(function);
                    log.info("execute command - " + function);
                });
            }
        }).start();

    }

    public void getDashboardData() {
        log.info("get DASHBOARD DATA -");
        new Thread(() -> {
            try {
                JSONObject data = new JSONObject();
                Device.Init();
                data.put("username", UserOperator.getUsername());
                data.put("fullname", UserOperator.getFullname());
                data.put("bprname", UserOperator.getBPRName());
                data.put("macaddress", Terminal.getMacaddress());
                data.put("procid", Terminal.getProcessorid());
                data.put("procname", Terminal.getProcessorname());
                data.put("uuid", Terminal.getUuid());
                data.put("host", Terminal.getHostname());
                data.put("bankid", Lembaga.getBankID());
                data.put("lembagaid", Lembaga.getLembagaID());
                data.put("privilege", UserOperator.getPrivilege());
                data.put("pcid", Device.getPcid());
                data.put("conf", Device.getConf());
                data.put("deviceid", Device.getDeviceId());

                Platform.runLater(() -> {
                    String function = "data.setInitialData(`[message]`)";
                    function = function.replace("[message]", data.toString());
                    webView.getEngine().executeScript(function);
                    log.info("execute command - " + function);
                });
            } catch (Exception e) {
                log.error(e);
                MainController.setAlertOnTop("Gagal mendapatkan data dashboard, " + e.getMessage(), Alert.AlertType.ERROR);
            }
        }).start();
    }

    public void saveReturnData(String year, String month, String data) {
        log.info("save Return Data -");
        new Thread(() -> {
            try {
                log.info("save RETURN DATA -");
                LocalDate currentdate = LocalDate.now();
                log.info(data);
//                String filename = currentdate.getYear() + "-" + String.format("%02d", currentdate.getMonthValue()) + "-return" + ".data";
                String filename = year + "-" + month + "-return" + ".data";
                writeFile(System.getProperty("user.dir") + "\\return", "\\" + filename, data);
            } catch (Exception e) {
                log.error(e);
                MainController.setAlertOnTop("Gagal mendapatkan data dashboard, " + e.getMessage(), Alert.AlertType.ERROR);
            }
        }).start();
    }

    public void getReturnData() {
        log.info("get RETURN DATA -");
        new Thread(() -> {
            try {
                LocalDate currentdate = LocalDate.now();
                String filename = currentdate.getYear() + "-" + String.format("%02d", currentdate.getMonthValue()) + "-return" + ".data";
                String data = readFile(System.getProperty("user.dir") + "\\return", "\\" + filename);
                log.info(data);
                if (data != null) {
                    Platform.runLater(() -> {
                        jsConnector.call("setReturn", data);
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void getReturnDataByMonthAndYear(String month, String year) {
        log.info("get RETURN DATA -");
        new Thread(() -> {
            try {
                String filename = year + "-" + month + "-return" + ".data";
                String data = readFile(System.getProperty("user.dir") + "\\return", "\\" + filename);
                log.info(data);
                if (data != null) {
                    Platform.runLater(() -> {
                        jsConnector.call("setReturn", data);
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void saveReturnDataToExcel(String data) {
        sleep(1000);
        log.info("save RETURN DATA TO EXCEL -");
        log.info(data);

        new Thread(() -> {

        }).start();

        try {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Save File");
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Microsoft Excel .xlsx", "*.xlsx");
            FileChooser.ExtensionFilter extFilter2 = new FileChooser.ExtensionFilter("Microsoft Excel .xls", "*.xls");
            chooser.getExtensionFilters().add(extFilter);
            chooser.getExtensionFilters().add(extFilter2);
            File file = chooser.showSaveDialog(new Stage());

            String location = file.getAbsolutePath();

            log.info("location saved: ");
            log.info(location);

            log.info("test");
            XSSFWorkbook workbook = new XSSFWorkbook();
            log.info("test");
            XSSFSheet sheet = workbook.createSheet("Data Balikan");

            log.info("initialize workbook");

            int widthUnitsCol1 = 20 * 256;
            sheet.setColumnWidth(1, widthUnitsCol1);

            int widthUnitsCol2 = 20 * 256;
            sheet.setColumnWidth(2, widthUnitsCol2);

            int widthUnitsCol3 = 30 * 256;
            sheet.setColumnWidth(3, widthUnitsCol3);

            int widthUnitsCol4 = 20 * 256;
            sheet.setColumnWidth(4, widthUnitsCol4);

            int widthUnitsCol5 = 30 * 256;
            sheet.setColumnWidth(5, widthUnitsCol5);

            int widthUnitsCol6 = 30 * 256;
            sheet.setColumnWidth(6, widthUnitsCol6);

            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setBold(true);
            style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            style.setFont(font);

            Row tittle = sheet.createRow(0);
            tittle.createCell(0).setCellValue("NO");
            tittle.getCell(0).setCellStyle(style);

            tittle.createCell(1).setCellValue("WAKTU");
            tittle.getCell(1).setCellStyle(style);

            tittle.createCell(2).setCellValue("USER");
            tittle.getCell(2).setCellStyle(style);

            tittle.createCell(3).setCellValue("NIK");
            tittle.getCell(3).setCellStyle(style);

            tittle.createCell(4).setCellValue("CIF");
            tittle.getCell(4).setCellStyle(style);

            JSONArray jArray = new JSONArray(data);

            log.info("data array: " + jArray.toString());

            Object[][] out = new Object[jArray.length()][4];

            if (jArray != null) {
                for (int i = 0; i < jArray.length(); i++) {

                    String time = jArray.getJSONArray(i).getString(0);
                    String user = jArray.getJSONArray(i).getString(1);
                    String nik = jArray.getJSONArray(i).getString(2);
                    String cif = jArray.getJSONArray(i).getString(3);

                    out[i][0] = time;
                    out[i][1] = user;
                    out[i][2] = nik;
                    out[i][3] = cif;
                }
            } else {
                log.info("array null");
            }

            int rowCount = 0;
            int cellnumber = 0;

            for (Object[] aBook : out) {
                Row row = sheet.createRow(++rowCount);
                row.createCell(0).setCellValue(++cellnumber);

                int columnCount = 0;

                for (Object field : aBook) {
                    Cell cell = row.createCell(++columnCount);

                    log.info("cell: " + field.toString());

                    if (field instanceof String) {
                        cell.setCellValue((String) field);
                    } else if (field instanceof Integer) {
                        cell.setCellValue((Integer) field);
                    }
                }
            }
            try (FileOutputStream outputStream = new FileOutputStream(file.getPath())) {
                workbook.write(outputStream);

                sendAlertToJS("data tersimpan di " + file.getPath().replace("\\", "\\\\"));
                log.info("data saved");
            } catch (Exception ex) {
                ex.printStackTrace();
                sendAlertToJS("Gagal menyimpan data ke excel, " + ex.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            sendAlertToJS("Gagal memproses data ke excel, " + e.getMessage());
        }
    }

    public void createDiscalimerToPDF(String data) {
        log.info("create Discalimer To PDF --");
        sleep(1000);

        try {
            log.info("--> createDiscalimerToPDF");
            log.info("body - " + data);
            JSONObject jsData = new JSONObject(data);

            File file = File.createTempFile("disclaimer", ".html");
            System.out.println(file);

            FileChooser chooser = new FileChooser();
            chooser.setTitle("Save File");
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF Documents .pdf", "*.pdf");
            chooser.getExtensionFilters().add(extFilter);

            String readconfig = readFile(System.getProperty("user.dir"), "\\config\\temp.conf");
            JSONObject tempConf = new JSONObject();

            if (readconfig != null) {
                tempConf = new JSONObject(readconfig);
                File latestLocation = new File(tempConf.getString("latest_location_save_disclaimer"));
                if (latestLocation.exists()) {
                    chooser.setInitialDirectory(latestLocation);
                }
            }

            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            System.out.println(date);

            chooser.setInitialFileName(jsData.getString("nama") + "-" + date + ".pdf");

            File filePdf = chooser.showSaveDialog(new Stage());
            String location = filePdf.getAbsolutePath();
            tempConf.put("latest_location_save_disclaimer", filePdf.getParentFile().getAbsolutePath());
            log.info("path: " + filePdf.getParentFile().getAbsolutePath());
            writeFile(System.getProperty("user.dir"), "\\config\\temp.conf", tempConf.toString());

            new Thread(() -> {
                try {
                    String header = "<head>\n"
                            + "    <title>Disclaimer</title>\n"
                            + "    <style>\n"
                            + "        h1 {\n"
                            + "            text-align: center;\n"
                            + "        }\n"
                            + "        h2,\n"
                            + "        p {\n"
                            + "            padding-left: 50px;\n"
                            + "            padding-right: 50px;\n"
                            + "        }\n"
                            + "    </style>\n"
                            + "</head>";

                    String table = "<tr>\n"
                            + "        <td>\n"
                            + "             &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "             &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "        </td>"
                            + "        <td>NIK</td>\n"
                            + "        <td>: [nik]</td>\n"
                            + "    </tr>";

                    table += "  <tr>\n"
                            + "      <td></td>\n"
                            + "      <td>Nama</td>\n"
                            + "      <td>: [nama]</td>\n"
                            + " </tr>";

                    table += "  <tr>\n"
                            + "      <td></td>\n"
                            + "      <td>Tempat, Tanggal Lahir</td>\n"
                            + "      <td>: [tempat], [tanggal]</td>\n"
                            + " </tr>";

                    if (!jsData.getString("alamat").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>Alamat</td>\n"
                                + "      <td>: " + jsData.getString("alamat") + "</td>\n"
                                + " </tr>";
                    }

                    if (!jsData.getString("rtrw").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>RT/RW</td>\n"
                                + "      <td>: " + jsData.getString("rtrw") + "</td>\n"
                                + " </tr>";
                    }

                    if (!jsData.getString("keldesa").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>Kel/Desa</td>\n"
                                + "      <td>: " + jsData.getString("keldesa") + "</td>\n"
                                + " </tr>";
                    }

                    if (!jsData.getString("kec").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>Kecamatan</td>\n"
                                + "      <td>: " + jsData.getString("kec") + "</td>\n"
                                + " </tr>";
                    }

                    if (!jsData.getString("kawin").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>Status Perkawinan</td>\n"
                                + "      <td>: " + jsData.getString("kawin") + "</td>\n"
                                + " </tr>";
                    }

                    if (!jsData.getString("kerja").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>Status Pekerjaan</td>\n"
                                + "      <td>: " + jsData.getString("kerja") + "</td>\n"
                                + " </tr>";
                    }

                    if (!jsData.getString("warga").equals("")) {
                        table += "  <tr>\n"
                                + "      <td></td>\n"
                                + "      <td>Kewarganegaraan</td>\n"
                                + "      <td>: " + jsData.getString("warga") + "</td>\n"
                                + " </tr>";
                    }

                    String body = "<body>\n"
                            + "    <div>\n"
                            + "        <h1>Surat Pernyataan</h1>\n"
                            + "        <h2>Pernyataan:</h2>\n"
                            + "        <p>\n"
                            + "            <table style=\"margin-left: 100px\"> [table] </table>\n"
                            + "        </p>\n"
                            + "        <p>\n"
                            + "            Dengan ini saya menyatakan dan menyetujui untuk dilakukan pengecekan NIK,\n"
                            + "            secara online yang terhubung dengan sistem Ditjen Dukcapil, maupun secara offline (alat baca\n"
                            + "            card reader),\n"
                            + "            dan hasil pengecekannya akan dikelola sebagaimana mestinya oleh BPR/BPRS [bprname]\n"
                            + "        </p>\n"
                            + "        <table>\n"
                            + "            <tr>\n"
                            + "                <td>\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                </td>\n"
                            + "                <td>Diperiksa oleh</td>\n"
                            + "                <td>\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                    &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;\n"
                            + "                </td>\n"
                            + "                <td>Disetujui oleh</td>\n"
                            + "            </tr>\n"
                            + "            <tr><td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td></tr>\n"
                            + "            <tr><td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td></tr>\n"
                            + "            <tr><td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td></tr>\n"
                            + "            <tr><td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td></tr>\n"
                            + "            <tr>\n"
                            + "                <td></td>\n"
                            + "                <td>[operatorname]</td>\n"
                            + "                <td></td>\n"
                            + "                <td>[nasabahname]</td>\n"
                            + "            </tr>\n"
                            + "        </table>"
                            + "    </div>\n"
                            + "</body>";

                    body = body.replace("[table]", table);
                    String value = "<html>" + header + body + "</html>";
                    value = value.replace("[nik]", jsData.getString("nik"))
                            .replace("[nama]", jsData.getString("nama"))
                            .replace("[tanggal]", jsData.getString("tanggal"))
                            .replace("[tempat]", jsData.getString("tempat"))
                            .replace("[bprname]", UserOperator.getBPRName())
                            .replace("[operatorname]", UserOperator.getFullname())
                            .replace("[nasabahname]", jsData.getString("nama"));

                    // writes few lines
                    try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                        bw.write(value);
                    }

                    // read from a temporary file
                    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            System.out.println(line);
                        }
                    }

                    log.info(file.getPath());
                    generatePDFFromHTML(file, location);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendAlertToJS("Gagal menyimpan file ke pdf, " + e.getMessage());
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
            sendAlertToJS("Gagal menemukan lokasi penyimpanan pdf, " + e.getMessage());
        }
    }

    public String readFile(String filelocation, String filename) {
        try {
            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
//            sendAlertToJS("Gagal membaca file, " + e.getMessage());
            return null;
        }
    }

    public boolean writeFile(String filelocation, String filename, String data) {
        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            sendAlertToJS("Gagal menulis data, " + e.getMessage());
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            log.info("write DATA -");
            log.info(data);

            write.write(data);
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            sendAlertToJS("Gagal menyimpan data, " + e.getMessage());
            return false;
        }
    }

    private void generatePDFFromHTML(File file, String location) {
        try {
            String readconfig = readFile(System.getProperty("user.dir"), "\\config\\temp.conf");

            if (readconfig != null) {
                JSONObject tempConf = new JSONObject();
                tempConf.put("latest_location_save_disclaimer", location);
            }

            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(location));
            document.open();

            XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream(file));
            document.close();
            sendAlertToJS("Dokumen tersimpan di " + location.replace("\\", "\\\\"));
        } catch (Exception e) {
            e.printStackTrace();
            sendAlertToJS("Gagal membuat file PDF, " + e.getMessage());
        }
    }
}
