/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.ktp;

/**
 *
 * @author ACS-Ilham
 */
public class DataKTP {

    private String NIK;
    private String namaLengkap;
    private String noKK;
    private String tempatLahir;
    private String tglLahir;
    private String jenisKelamin;
    private String alamat;
    private String noRT;
    private String noRW;
    private String kelurahan;
    private String noKelurahan;
    private String kecamatan;
    private String noKecamatan;
    private String kabupaten;
    private String noKabupaten;
    private String propinsi;
    private String noPropinsi;
    private String kodePos;
    private String agama;
    private String jenisPekerjaan;
    private String namaIbu;
    private String status;

    public DataKTP() {

    }

    public DataKTP(String NIK, String namaLengkap, String noKK, String tempatLahir, String tglLahir, String jenisKelamin, String alamat, String noRT, String noRW, String kelurahan, String noKelurahan, String kecamatan, String noKecamatan, String kabupaten, String noKabupaten, String propinsi, String noPropinsi, String kodePos, String agama, String jenisPekerjaan, String namaIbu, String status) {
        this.NIK = NIK;
        this.namaLengkap = namaLengkap;
        this.noKK = noKK;
        this.tempatLahir = tempatLahir;
        this.tglLahir = tglLahir;
        this.jenisKelamin = jenisKelamin;
        this.alamat = alamat;
        this.noRT = noRT;
        this.noRW = noRW;
        this.kelurahan = kelurahan;
        this.noKelurahan = noKelurahan;
        this.kecamatan = kecamatan;
        this.noKecamatan = noKecamatan;
        this.kabupaten = kabupaten;
        this.noKabupaten = noKabupaten;
        this.propinsi = propinsi;
        this.noPropinsi = noPropinsi;
        this.kodePos = kodePos;
        this.agama = agama;
        this.jenisPekerjaan = jenisPekerjaan;
        this.namaIbu = namaIbu;
        this.status = status;
    }

    public String getNIK() {
        return NIK;
    }

    public void setNIK(String NIK) {
        this.NIK = NIK;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNoKK() {
        return noKK;
    }

    public void setNoKK(String noKK) {
        this.noKK = noKK;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoRT() {
        return noRT;
    }

    public void setNoRT(String noRT) {
        this.noRT = noRT;
    }

    public String getNoRW() {
        return noRW;
    }

    public void setNoRW(String noRW) {
        this.noRW = noRW;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getNoKelurahan() {
        return noKelurahan;
    }

    public void setNoKelurahan(String noKelurahan) {
        this.noKelurahan = noKelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getNoKecamatan() {
        return noKecamatan;
    }

    public void setNoKecamatan(String noKecamatan) {
        this.noKecamatan = noKecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getNoKabupaten() {
        return noKabupaten;
    }

    public void setNoKabupaten(String noKabupaten) {
        this.noKabupaten = noKabupaten;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getNoPropinsi() {
        return noPropinsi;
    }

    public void setNoPropinsi(String noPropinsi) {
        this.noPropinsi = noPropinsi;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getJenisPekerjaan() {
        return jenisPekerjaan;
    }

    public void setJenisPekerjaan(String jenisPekerjaan) {
        this.jenisPekerjaan = jenisPekerjaan;
    }

    public String getNamaIbu() {
        return namaIbu;
    }

    public void setNamaIbu(String namaIbu) {
        this.namaIbu = namaIbu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

}
