/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.ktp;

import com.dak.perbarindo.sb.apps.MainApp;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author ACS-Ilham
 */
public class Ektp {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Ektp.class);

    public static String dllPath = "";
    public static EktpInterface myInterface;
    public static Pointer hContext;
//    public static String pcid;
//    public static String conf;

    //SDK
    public static void ExtractJar() {
        System.out.println("Extract Jar");
        String libFullName = "EktpSdk";
        if (System.getProperty("os.name").contains("Windows")) {
            logger.info("windows");
            if (System.getenv("ProgramFiles(x86)") != null) {
                libFullName = libFullName + "_x64.dll";
                logger.info("Load Lib : " + libFullName);
            } else {
                libFullName = libFullName + "_x86.dll";
                logger.info("Load Lib : " + libFullName);
            }
        } else {
            logger.info("not windows");
            if (System.getProperty("os.arch").indexOf("64") != -1) {
                libFullName = libFullName + "_x64.so";
                logger.info("Load Lib : " + libFullName);
            } else {
                libFullName = libFullName + "_x86.so";
                logger.info("Load Lib : " + libFullName);
            }
        }
        
        if(MainApp.developement){
            libFullName = "EktpSdk_x64.dll";
        } else {
            libFullName = "EktpSdk_x86.dll";
        }

        String filepath = System.getProperty("user.dir") + File.separator + libFullName;
        logger.info("filepath: " + filepath);
        dllPath = filepath;
        InputStream in = null;
        BufferedInputStream reader = null;
        FileOutputStream writer = null;
        File extractedLibFile = new File(filepath);

        if (!extractedLibFile.exists()) {
            try {
                logger.info("notfound: " + filepath);
                in = Ektp.class.getResourceAsStream(libFullName);
                if (in == null) {
                    in = Ektp.class.getResourceAsStream(libFullName);
                }
                Ektp.class.getResource(libFullName);
                reader = new BufferedInputStream(in);
                writer = new FileOutputStream(extractedLibFile);
                byte[] buffer = new byte[1024];
                while (reader.read(buffer) > 0) {
                    writer.write(buffer);
                    buffer = new byte[1024];
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.info(e);
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.info(e);
                }
            }
        }
    }

    public static int InitEktp(String pcid, String conf) {
        try {
            int ret = -1;
            PointerByReference phContext = new PointerByReference();
            ret = myInterface.EktpInitDevice(phContext);
            hContext = phContext.getValue();

            if (hContext == null || ret != 0) {
                return -1;
            }
//
//        String fileName = "Config.txt";
//        File file = new File(fileName);
//        try {
//            TripleDES tdes = new TripleDES(Constant.ENCRYPT);
//            if (file.exists()) {
//                FileReader fr = new FileReader(file);
//                BufferedReader br = new BufferedReader(fr);
//                pcid = tdes.decrypt(br.readLine());
//                conf = tdes.decrypt(br.readLine());
//                br.close();
//            } else {
//                pcid = "";
//                conf = "";
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

            ret = myInterface.EktpSetConfig(hContext, pcid, conf);

            if (ret != 0) {
                return -2;
            }

            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static byte[] hexStrToBytes(String hexStr) {
        if (hexStr == null || hexStr.trim().equals("")) {
            return new byte[0];
        }

        byte[] bytes = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            byte hegh = (byte) (Character.digit(hexStr.charAt(i * 2), 16) & 0xff);
            byte low = (byte) (Character.digit(hexStr.charAt(i * 2 + 1), 16) & 0xff);
            bytes[i] = (byte) (hegh << 4 | low);
        }

        return bytes;

    }

    public static int getRealLength(byte[] a) {
        int i = 0;
        for (; i < a.length; i++) {
            if (a[i] == '\0') {
                break;
            }
        }
        return i;
    }
}
