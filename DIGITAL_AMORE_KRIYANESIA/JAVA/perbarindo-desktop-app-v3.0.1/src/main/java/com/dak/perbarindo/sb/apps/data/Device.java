/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author dak-alex
 */
public class Device {

    private static final Logger log = LogManager.getLogger(Device.class);

    public static String pcid = null;
    public static String conf = null;
    public static String deviceId = null;
    public static String bankid = null;

    public static JSONObject deviceData = null;

    public static void Init() {
        try {
            JSONObject device = new JSONObject(readFile());
            if (device != null) {
                setPcid(device.getString("pcid"));
                setConf(device.getString("conf"));
                setBankid(device.getString("bankid"));
                setDeviceId(device.getString("deviceid"));
                setDeviceData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readFile() {
        try {
            String filelocation = System.getProperty("user.dir") + "\\config";
            String filename = "\\device.conf";

            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
//            sendAlertToJS("Gagal membaca file, " + e.getMessage());
            return null;
        }
    }

    public static boolean writeFile() {
        String filelocation = System.getProperty("user.dir") + "\\config";
        String filename = "\\device.conf";
        try {

            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            setDeviceData();
            log.info("write DATA -");
            log.info(deviceData.toString());
            log.info("location: " + filelocation + filename);

            write.write(deviceData.toString());
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }

    public static String getPcid() {
        return pcid;
    }

    public static void setPcid(String pcid) {
        Device.pcid = pcid;
    }

    public static String getConf() {
        return conf;
    }

    public static void setConf(String conf) {
        Device.conf = conf;
    }

    public static String getDeviceId() {
        return deviceId;
    }

    public static void setDeviceId(String deviceId) {
        Device.deviceId = deviceId;
    }

    public static JSONObject getDeviceData() {
        return deviceData;
    }

    public static void setDeviceData() {
        JSONObject data = new JSONObject()
                .put("bankid", getBankid())
                .put("deviceid", getDeviceId())
                .put("conf", getConf())
                .put("pcid", getPcid());

        Device.deviceData = data;
    }

    public static String getBankid() {
        return bankid;
    }

    public static void setBankid(String bankid) {
        Device.bankid = bankid;
    }

}
