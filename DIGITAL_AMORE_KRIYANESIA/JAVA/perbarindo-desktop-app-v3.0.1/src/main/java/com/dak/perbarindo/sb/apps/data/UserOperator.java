/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class UserOperator {

    private static final Logger log = LogManager.getLogger(Lembaga.class);

    private static String filelocation = System.getProperty("user.dir") + "\\config";
    private static String filename = "\\useroperator.conf";

    private static JSONObject jsUserData;

    private static String Username = null;
    private static String Password = null;
    private static String Fullname = null;
    private static String Email = null;
    private static String NIK = null;
    private static String PhoneNumber = null;
    private static String ExpiredPassword = null;
    private static String BPRName = null;
    private static String Privilege = null;
    private static String DueDate = null;

    public static String readFile() {
        try {
            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (FileNotFoundException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    public static boolean writeFile() {

        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            JSONObject data = new JSONObject();
            data.put("username", Username)
                    .put("password", Password)
                    .put("fullname", Fullname)
                    .put("nik", NIK)
                    .put("bprname", BPRName);

            log.info("write CONFIG -");
            log.info(data.toString());

            write.write(data.toString());
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }

    public static String getFilelocation() {
        return filelocation;
    }

    public static void setFilelocation(String filelocation) {
        UserOperator.filelocation = filelocation;
    }

    public static String getFilename() {
        return filename;
    }

    public static void setFilename(String filename) {
        UserOperator.filename = filename;
    }

    public static String getUsername() {
        return Username;
    }

    public static void setUsername(String Username) {
        UserOperator.Username = Username;
    }

    public static String getPassword() {
        return Password;
    }

    public static void setPassword(String Password) {
        UserOperator.Password = Password;
    }

    public static String getFullname() {
        return Fullname;
    }

    public static void setFullname(String Fullname) {
        UserOperator.Fullname = Fullname;
    }

    public static String getEmail() {
        return Email;
    }

    public static void setEmail(String Email) {
        UserOperator.Email = Email;
    }

    public static String getNIK() {
        return NIK;
    }

    public static void setNIK(String NIK) {
        UserOperator.NIK = NIK;
    }

    public static String getPhoneNumber() {
        return PhoneNumber;
    }

    public static void setPhoneNumber(String PhoneNumber) {
        UserOperator.PhoneNumber = PhoneNumber;
    }

    public static String getExpiredPassword() {
        return ExpiredPassword;
    }

    public static void setExpiredPassword(String ExpiredPassword) {
        UserOperator.ExpiredPassword = ExpiredPassword;
    }

    public static String getBPRName() {
        return BPRName;
    }

    public static void setBPRName(String BPRName) {
        UserOperator.BPRName = BPRName;
    }

    public static JSONObject getJsUserData() {
        setJsUserData(new JSONObject()
                .put("username", Username)
                .put("password", Password)
                .put("fullname", Fullname)
                .put("email", Email)
                .put("nik", NIK)
                .put("nik", PhoneNumber)
                .put("nik", ExpiredPassword)
                .put("nik", BPRName)
        );
        return jsUserData;
    }

    public static void setJsUserData(JSONObject jsUserData) {
        UserOperator.jsUserData = jsUserData;
    }

    public static String getPrivilege() {
        return Privilege;
    }

    public static void setPrivilege(String Privilege) {
        UserOperator.Privilege = Privilege;
    }

    public static String getDueDate() {
        return DueDate;
    }

    public static void setDueDate(String DueDate) {
        UserOperator.DueDate = DueDate;
    }

}
