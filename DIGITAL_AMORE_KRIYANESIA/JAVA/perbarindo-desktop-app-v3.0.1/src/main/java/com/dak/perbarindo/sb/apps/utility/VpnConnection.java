/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.utility;

import com.dak.perbarindo.sb.apps.MainApp;
import com.dak.perbarindo.sb.apps.controller.MainController;
import com.dak.perbarindo.sb.apps.data.Config;
import com.dak.perbarindo.sb.apps.request.RequestData;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javafx.scene.control.Alert;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class VpnConnection {
    
    private static final Logger log = LogManager.getLogger(VpnConnection.class);
    
    private static ProcessBuilder processBuilder = new ProcessBuilder();
    private static Process process;
    
    private static String vpnconfiglocation = System.getProperty("user.dir") + "\\config";
    private static String vpnconfigname = "\\sb3vpn.pbk";
    private static String vpnname = "sb3vpn";
//    private static String vpnhost = "vpn.srv.co.id";
    private static String vpnhost = "117.54.12.173";
    private static String vpnusername = "";
    private static String vpnpassword = "";
    
    private static String vpnconfig = ""
            + "[[vpnname]]\n"
            + "MEDIA=rastapi\n"
            + "Port=VPN3-0\n"
            + "Device=WAN Miniport (PPTP)\n"
            + "DEVICE=vpn\n"
            + "PhoneNumber=[vpnhost]";
    
    public static boolean connectVPN() throws IOException {
        if (!readFile()) {
            if (!writeFile()) {
                return false;
            }
        }
        
        Config.vpn.getConfig();
        vpnhost = Config.vpn.host;
        
        JSONObject response = RequestData.send(Constants.path.name.GET_VPN, null);
        
        if (response != null) {
            
            if (!response.has("rc") || !response.getString("rc").equals("00")) {
                MainController.setAlert("Gagal mendapatkan data vpn, " + response.getString("rc"), Alert.AlertType.ERROR);
                return false;
            }
            
            JSONObject data = null;
            
            try {
                data = response.getJSONArray("data").getJSONObject(0);
            } catch (Exception e) {
                log.error(new String(), e);
            }
            
            if (data == null) {
                data = response.getJSONObject("data");
            }
            
            vpnusername = data.getString("username");
            vpnpassword = data.getString("password");
            
            long startTime = System.currentTimeMillis();
            try {
                
                String commandValue = "rasdial \"" + vpnname + "\" \"" + vpnusername + "\" \"" + vpnpassword + "\"  /phonebook:\"" + (vpnconfiglocation + vpnconfigname) + "\"";
                
                if (MainApp.developement) {
                    log.info("command: " + commandValue);
                }
                
                processBuilder.command("cmd.exe", "/c", commandValue);
                log.info("connect VPN -");
                log.info(vpnconfiglocation + vpnconfigname);
                process = processBuilder.start();
                BufferedReader reader
                        = new BufferedReader(new InputStreamReader(process.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line = null;
                while (process.isAlive()) {
                    if ((line = reader.readLine()) != null) {
                        builder.append(line);
                        builder.append(System.getProperty("line.separator"));
                        log.info(line);
                        if (line.contains("failed") | line.contains("Remote Access error")) {
                            log.info("Failed: " + line);
                            MainController.setAlert(line, Alert.AlertType.ERROR);
                            return false;
                        }
                    }
                }
            } catch (Exception e) {
                log.info("Failed to Connect");
                log.error(e);
                e.printStackTrace();
                return false;
            }
            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
            log.info("Duration to Connect " + elapsedTime / 1000 + " Seconds");
            return true;
            
        } else {
            MainController.setAlert("Gagal mendapatkan data vpn, koneksi tidak mendapatkan response", Alert.AlertType.ERROR);
            return false;
        }
    }
    
    public static boolean disconnectVPN() {
        try {
            processBuilder.command("cmd.exe", "/c", "rasdial /DISCONNECT");
            log.info("disconnect VPN -");
            process = processBuilder.start();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while (process.isAlive()) {
                if ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.getProperty("line.separator"));
                    log.info(line);
                    if (line.contains("failed") | line.contains("Remote Access error")) {
                        log.info("Failed: " + line);
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            log.info("Failed to disconnect");
            log.error(e);
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public static boolean checkConnection() {
        try {
            processBuilder.command("cmd.exe", "/c", "rasdial");
            log.info("disconnect VPN -");
            process = processBuilder.start();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while (process.isAlive()) {
                if ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.getProperty("line.separator"));
                    log.info(line);
                    if (line.contains("No connections")) {
                        log.info("vpn not connected");
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            log.info("Failed to disconnect");
            log.error(e);
            e.printStackTrace();
            return false;
        }
        log.info("vpn connected");
        return true;
    }
    
    public static boolean readFile() {
        try {
            try {
                Config.vpn.getConfig();
                vpnhost = Config.vpn.host;
            } catch (Exception e) {
                log.error(new String(), e);
            }
            
            File myObj = new File(vpnconfiglocation + vpnconfigname);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
                log.info(data);
            }
            if (!data.contains(vpnhost)) {
                return false;
            }
            myReader.close();
            return true;
        } catch (FileNotFoundException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }
    
    public static boolean writeFile() {
        
        try {
            File dir = new File(vpnconfiglocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }
            
            File file = new File(vpnconfiglocation + vpnconfigname);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
        
        try {
            FileWriter write = new FileWriter(vpnconfiglocation + vpnconfigname);
            String configvpn = vpnconfig.replace("[vpnname]", vpnname)
                    .replace("[vpnhost]", vpnhost);
            write.write(configvpn);
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }
}
