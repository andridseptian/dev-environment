package com.dak.perbarindo.sb.apps.controller;

import com.dak.perbarindo.sb.apps.MainApp;
import com.dak.perbarindo.sb.apps.connector.DownloadConnector;
import com.dak.perbarindo.sb.apps.connector.JavaConnector;
import com.dak.perbarindo.sb.apps.connector.PortalConnector;
import com.dak.perbarindo.sb.apps.connector.ScanConnector;
import com.dak.perbarindo.sb.apps.connector.UserConnector;
import com.dak.perbarindo.sb.apps.data.Terminal;
import java.io.File;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ResourceBundle;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Scanner;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainController implements Initializable {

    private static final Logger log = LogManager.getLogger(MainController.class);

    private JavaConnector javaConnector;
    private PortalConnector portalConnector;
    private UserConnector userConnector;
    private DownloadConnector downloadConnector;
    private ScanConnector scanConnector;

    @FXML
    private Label lbInfo;
    @FXML
    private VBox parentVbox;

    public static WebView webView = new WebView();
    @FXML
    private AnchorPane apaneroot;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        log.info("initialize main controller");
        Instant t1 = Instant.now();

//        installCertificateVerificationBypassTools();
        webView.setContextMenuEnabled(false);
        webView.setFontSmoothingType(FontSmoothingType.GRAY);

//        webView.setPrefSize(2000, 2000);
        AnchorPane.setTopAnchor(webView, 0D);
        AnchorPane.setBottomAnchor(webView, 0D);
        AnchorPane.setLeftAnchor(webView, 0D);
        AnchorPane.setRightAnchor(webView, 0D);

        apaneroot.getChildren().add(apaneroot.getChildren().size(), webView);

        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

        webView.getEngine().getLoadWorker().stateProperty().addListener(
                new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                log.info("oldValue: " + oldValue);
                log.info("newValue: " + newValue);

                if (newValue == Worker.State.SUCCEEDED) {
                    webView.setVisible(true);
                    javaConnector = new JavaConnector(webView);
                    JSObject window = (JSObject) webView.getEngine().executeScript("window");
                    window.setMember("javaConnector", javaConnector);
                    userConnector = new UserConnector(MainController.this);
                    window.setMember("userConnector", userConnector);
                    downloadConnector = new DownloadConnector(MainController.this);
                    window.setMember("downloadConnector", downloadConnector);
                    portalConnector = new PortalConnector(MainController.this);
                    window.setMember("portalConnector", portalConnector);
                    scanConnector = new ScanConnector(MainController.this);
                    window.setMember("scanConnector", scanConnector);

                } else if (newValue == Worker.State.FAILED) {
                    setAlertOnTopAndExit("Gagal memuat tampilan, aplikasi akan ditutup, pastikan anda sudah menginstall aplikasi dengan benar");
                } else {
                    webView.setVisible(false);
                }
            }
        });

        webView.getEngine().setOnAlert(new EventHandler<WebEvent<String>>() {
            @Override
            public void handle(WebEvent<String> event) {
                log.info(event.getData());

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText(null);
                alert.setContentText(event.getData());
                alert.showAndWait();
            }
        });

        try {
            Terminal.init();
            String viewLocation = "";
            if (MainApp.isDevelopement()) {
                viewLocation = "file:///" + System.getProperty("user.dir").replace("\\", "/") + "/src/main/resources/view/login.html";
            } else {
                viewLocation = getClass().getResource("/view/login.html").toURI().toString();
            }
            webView.getEngine().load(viewLocation);
        } catch (Exception e) {
            e.printStackTrace();
            setAlertOnTopAndExit("Gagal memuat tampilan, " + e.getMessage());
        }

        log.info("initialize done");
        Instant t2 = Instant.now();
        long performance = ChronoUnit.MILLIS.between(t1, t2);
        log.info("performance: " + performance);
    }

    public static void setAlert(String message, Alert.AlertType type) {
        Platform.runLater(() -> {
            Alert alert = new Alert(type);
            alert.setTitle(null);
            alert.initStyle(StageStyle.UTILITY);
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();
        });
    }

    public static void setAlertOnTop(String message, Alert.AlertType type) {
        Platform.runLater(() -> {
            Alert alert = new Alert(type);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();
        });
    }

    public static void setAlertOnTopAndExit(String message, Alert.AlertType type) {
        Platform.runLater(() -> {
            Alert alert = new Alert(type);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();

            Platform.exit();
            System.exit(0);
        });
    }

    public static void setAlertOnTopAndExit(String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();

            System.exit(0);
            Platform.exit();
        });
    }

    public void sendAlertToJS(String message) {
        new Thread(() -> {
            Platform.runLater(() -> {
                try {
                    String function = "setAlert('[message]')";
                    function = function.replace("[message]", message);
                    webView.getEngine().executeScript(function);
                    log.info("execute command - " + function);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendAlertToJS(e.getMessage());
                    log.error(new String(), e);
                }
            });
        }).start();
    }

    public void executeScript(String script) {
        Platform.runLater(() -> {
            webView.getEngine().executeScript(script);
        });
    }

    public void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean writeFile(String filename, String filelocation, String data) {
        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            sendAlertToJS("Gagal menulis data, " + e.getMessage());
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            log.info("write DATA -");
            log.info(data);

            write.write(data);
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            sendAlertToJS("Gagal menyimpan data, " + e.getMessage());
            return false;
        }
    }

    public String readFile(String filename, String path) {
        try {
            File myObj = new File(path + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    private void installCertificateVerificationBypassTools() {
        this.installCustomTrustManager();
        this.installCustomHostNameverifier();

    }

    private void installCustomTrustManager() {
        System.out.println("Installing custom trust manager");
        try {
            TrustManager[] nonDiscriminantTrustManager = new TrustManager[]{new X509TrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    // ignore client trust check
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    // ignore server trust check
                }
            }};
            final SSLContext ret = SSLContext.getInstance("SSL");
            ret.init(null, nonDiscriminantTrustManager, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(ret.getSocketFactory());
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {

        }
    }

    private void installCustomHostNameverifier() {
        System.out.println("Installing custom hostname verifier");
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String string, SSLSession ssls) {
                System.out.println("Verifying connection...");
                if (ssls.getProtocol().contains("https")) {
                    System.out.println("https session allowed.");
                }
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

    @FXML
    private void rootKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F5) {
            webView.getEngine().reload();
        }

        if (event.getCode() == KeyCode.F6) {
            webView.getEngine().executeScript("history.back()");
        }

        if (event.getCode() == KeyCode.BACK_SPACE) {

        }
    }

}
