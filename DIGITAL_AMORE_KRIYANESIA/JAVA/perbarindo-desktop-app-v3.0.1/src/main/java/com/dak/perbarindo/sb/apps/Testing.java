/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps;

import com.dak.perbarindo.sb.apps.ktp.Ektp;
import com.dak.perbarindo.sb.apps.ktp.EktpInterface;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.sun.jna.ptr.IntByReference;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class Testing {

    static int count = 0;

    public static void main(String[] args) {
//        nullString();

        System.out.println(System.getProperty("os.name"));

        
    }

    public static void nullString() {
        String variable = null;
        System.out.println(strIfNull(variable, "VARIABLE-NOT-FOUND").replace("-", ""));
    }

    public static String strIfNull(String data, String ifNull) {
        return ((data == null) ? ifNull : data);
    }

    public static void sendEmail() {
        JSONArray dataArr = new JSONArray("[\n"
                + "    {\n"
                + "        \"Timestamp\": 44110.75716494213,\n"
                + "        \"Bank ID\": 2788,\n"
                + "        \"Nama BPR\": \"PT. BPR NUSAMBA GENTENG\",\n"
                + "        \"Email Aktif\": \"nusamba_genteng@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085755536618\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44110.75805444444,\n"
                + "        \"Bank ID\": 3024,\n"
                + "        \"Nama BPR\": \"PT BPR Bahtera Masyarakat Jabar\",\n"
                + "        \"Email Aktif\": \"bprbm.jabar@bankbahtera.co.id\",\n"
                + "        \"Wa Aktif\": \"081573817169\",\n"
                + "        \"__EMPTY\": \"bank id salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44110.76444741898,\n"
                + "        \"Bank ID\": 168,\n"
                + "        \"Nama BPR\": \"Pt\",\n"
                + "        \"Email Aktif\": \"PT.BPR DANA MULTI GUNA\",\n"
                + "        \"Wa Aktif\": \"089504323272\",\n"
                + "        \"__EMPTY\": \"salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44110.78223643519,\n"
                + "        \"Bank ID\": 90,\n"
                + "        \"Nama BPR\": \"PT.  BPR Bintang Dana Persada\",\n"
                + "        \"Email Aktif\": \"Krisman.sai@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081377966619\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44110.79042896991,\n"
                + "        \"Bank ID\": \"071279\",\n"
                + "        \"Nama BPR\": \"PT. BPR Darmawan Adhiguna Lestari\",\n"
                + "        \"Email Aktif\": \"jkssbma@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"081517335500\",\n"
                + "        \"__EMPTY\": \"salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44110.7951880787,\n"
                + "        \"Bank ID\": 2453,\n"
                + "        \"Nama BPR\": \"PT. BPR NUSANTARA BONA PASOGIT 22\",\n"
                + "        \"Email Aktif\": \"bpr_nbp22@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085358081225\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.25530887731,\n"
                + "        \"Bank ID\": 1585,\n"
                + "        \"Nama BPR\": \"BPR WIRADHANA PUTRAMAS\",\n"
                + "        \"Email Aktif\": \"bpr_wp@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085231162575\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.26402884259,\n"
                + "        \"Bank ID\": 2037,\n"
                + "        \"Nama BPR\": \"PD.BPR Bank Buleleng 45\",\n"
                + "        \"Email Aktif\": \"buleleng45@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"087759943862\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.27211171296,\n"
                + "        \"Bank ID\": \"071279\",\n"
                + "        \"Nama BPR\": \"PT. BPR DARMAWAN ADHIGUNA LESTARI\",\n"
                + "        \"Email Aktif\": \"jkssbma@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"081517335500\",\n"
                + "        \"__EMPTY\": \"salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.287080486116,\n"
                + "        \"Bank ID\": 3318,\n"
                + "        \"Nama BPR\": \"BPR Pundi Dana Mandiri\",\n"
                + "        \"Email Aktif\": \"pundidanamandiri@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"082282247302\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.28812863426,\n"
                + "        \"Bank ID\": 2945,\n"
                + "        \"Nama BPR\": \"BPR Karya Guna Mandiri\",\n"
                + "        \"Email Aktif\": \"rizqi537@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085721221226\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.29587751157,\n"
                + "        \"Bank ID\": 2090,\n"
                + "        \"Nama BPR\": \"PT. BPR TAPA\",\n"
                + "        \"Email Aktif\": \"bprtapa@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085792613967\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.31226402777,\n"
                + "        \"Bank ID\": \"04500000000333\",\n"
                + "        \"Nama BPR\": \"PT.BPR KARYA SARI SEDANA\",\n"
                + "        \"Email Aktif\": \"bprkaryasari@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"087754629173\",\n"
                + "        \"__EMPTY\": \"salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.31723407407,\n"
                + "        \"Bank ID\": 2132,\n"
                + "        \"Nama BPR\": \"PT. BPR SARI SEDANA\",\n"
                + "        \"Email Aktif\": \"ptbprsarisedana@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"08970306467\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.31756253472,\n"
                + "        \"Bank ID\": 333,\n"
                + "        \"Nama BPR\": \"PT.BPR KARYA SARI SEDANA\",\n"
                + "        \"Email Aktif\": \"bprkaryasari@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"087754629173\",\n"
                + "        \"__EMPTY_2\": \"#ERROR!\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.31816362268,\n"
                + "        \"Bank ID\": \"04000000001190\",\n"
                + "        \"Nama BPR\": \"PT. BPR DANAPERMATA LESTARI \",\n"
                + "        \"Email Aktif\": \"bprdpl@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085346019858\",\n"
                + "        \"__EMPTY\": \"salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.32414203703,\n"
                + "        \"Bank ID\": 2453,\n"
                + "        \"Nama BPR\": \"PT. BPR NUSANTARA BONA PASOGIT 22\",\n"
                + "        \"Email Aktif\": \"bpr_nbp22@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085358081225\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.325211979165,\n"
                + "        \"Bank ID\": 1951,\n"
                + "        \"Nama BPR\": \"PT. BPR GRAHA LESTARI\",\n"
                + "        \"Email Aktif\": \"laluyudidarmaprayana@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081907341006\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.33213559027,\n"
                + "        \"Bank ID\": 2251,\n"
                + "        \"Nama BPR\": \"PT.BPR TERABINA SERAYA MULIA\",\n"
                + "        \"Email Aktif\": \"bprterabinaseraya@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"081270330444\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.33271887731,\n"
                + "        \"Bank ID\": 5077,\n"
                + "        \"Nama BPR\": \"PT. BPRS MITRA HARMONI KOTA MALANG\",\n"
                + "        \"Email Aktif\": \"mhm.syariah1@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082132630013\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.334069930555,\n"
                + "        \"Bank ID\": 172,\n"
                + "        \"Nama BPR\": \"BPR PRIMA RIAU SENTOSA\",\n"
                + "        \"Email Aktif\": \"ardisonputra27@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085274523231\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.33575391203,\n"
                + "        \"Bank ID\": \"0171174\",\n"
                + "        \"Nama BPR\": \"BPR MITRA CEMAWIS MANDIRI\",\n"
                + "        \"Email Aktif\": \"bpr_cemawis@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"081357796047\",\n"
                + "        \"__EMPTY\": \"salah\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.341060833336,\n"
                + "        \"Bank ID\": 20,\n"
                + "        \"Nama BPR\": \"PT. BPR CHANDRA MUKTIARTHA\",\n"
                + "        \"Email Aktif\": \"bprcma@bprcma.co.id\",\n"
                + "        \"Wa Aktif\": \"081232426446\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.341662060186,\n"
                + "        \"Bank ID\": 1585,\n"
                + "        \"Nama BPR\": \"BPR WIRADHANA PUTRAMAS\",\n"
                + "        \"Email Aktif\": \"bpr_wp@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085231162575\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.342296319446,\n"
                + "        \"Bank ID\": 92,\n"
                + "        \"Nama BPR\": \"BPR MUSI ARTHA SURYA\",\n"
                + "        \"Email Aktif\": \"bprmas_plbg@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"08127341275\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.342590555556,\n"
                + "        \"Bank ID\": 1751,\n"
                + "        \"Nama BPR\": \"BPR RUDO INDOBANK\",\n"
                + "        \"Email Aktif\": \"krisna@rudoindobank.com\",\n"
                + "        \"Wa Aktif\": \"085727957528\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34310537037,\n"
                + "        \"Bank ID\": 2607,\n"
                + "        \"Nama BPR\": \"PT. BPR ARTHA PAMENANG\",\n"
                + "        \"Email Aktif\": \"bpr_artha_pamenang@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085736604419\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34321685185,\n"
                + "        \"Bank ID\": 508,\n"
                + "        \"Nama BPR\": \"BPR KLATEN SEJAHTERA\",\n"
                + "        \"Email Aktif\": \"klaten.sejahtera@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085743699991\",\n"
                + "        \"__EMPTY_1\": \"DONE\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34401936343,\n"
                + "        \"Bank ID\": \"071279\",\n"
                + "        \"Nama BPR\": \"PT. BPR DARMAWAN ADHIGUNA LESTARI\",\n"
                + "        \"Email Aktif\": \"JKSSBMA@YAHOO.COM\",\n"
                + "        \"Wa Aktif\": \"081517335500\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34514358796,\n"
                + "        \"Bank ID\": 2954,\n"
                + "        \"Nama BPR\": \"PT. BPR GUNADHANA MITRASEMBADA\",\n"
                + "        \"Email Aktif\": \"bprgmn@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"081321749787\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34540608796,\n"
                + "        \"Bank ID\": 2693,\n"
                + "        \"Nama BPR\": \"BPR ANUGERAHDHARMA YUWANA BONDOWOSO\",\n"
                + "        \"Email Aktif\": \"ady.bondowoso1@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082223409822\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.346212233795,\n"
                + "        \"Bank ID\": 493,\n"
                + "        \"Nama BPR\": \"BPR GUNUNG MAS\",\n"
                + "        \"Email Aktif\": \"pt.bpr_gm@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"08562935202\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34736193287,\n"
                + "        \"Bank ID\": 2753,\n"
                + "        \"Nama BPR\": \"PT. BPR Kota Pasuruan\",\n"
                + "        \"Email Aktif\": \"bpr_kota@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"087754531919\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.34736858796,\n"
                + "        \"Bank ID\": 1163,\n"
                + "        \"Nama BPR\": \"PT. BPR NUSANTARA BONA PASOGIT 25\",\n"
                + "        \"Email Aktif\": \"bpr.nbp25@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"081269672573\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.350478692126,\n"
                + "        \"Bank ID\": 3758,\n"
                + "        \"Nama BPR\": \"PT. BPR BATANGHARI\",\n"
                + "        \"Email Aktif\": \"pt.bpr_batanghari@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"082377302304\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.35055674768,\n"
                + "        \"Bank ID\": 3317,\n"
                + "        \"Nama BPR\": \"PT. BPR ASIA SEJAHTERA\",\n"
                + "        \"Email Aktif\": \"bpr_asiasejahtera@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"083184572912\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.35119915509,\n"
                + "        \"Bank ID\": \"071172\",\n"
                + "        \"Nama BPR\": \"BPR BATU ARTOREJO\",\n"
                + "        \"Email Aktif\": \"bprartorejo@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"08192523830\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.35429827546,\n"
                + "        \"Bank ID\": 3200000001926,\n"
                + "        \"Nama BPR\": \"PT. BPR DANAMAS SIMPAN PINJAM\",\n"
                + "        \"Email Aktif\": \"bprdanamas@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082390870846\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.354908912035,\n"
                + "        \"Bank ID\": 1797,\n"
                + "        \"Nama BPR\": \"PT BPR BANK GUNA DAYA\",\n"
                + "        \"Email Aktif\": \"gunadaya123@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082335212233\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.35582633102,\n"
                + "        \"Bank ID\": 1701,\n"
                + "        \"Nama BPR\": \"PT BPR ADIL JAYA ARTHA\",\n"
                + "        \"Email Aktif\": \"adiljayaartha@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"081290351305\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.357325081015,\n"
                + "        \"Bank ID\": 2809,\n"
                + "        \"Nama BPR\": \"BPR PRIMA KREDIT UTAMA\",\n"
                + "        \"Email Aktif\": \"info@bprprimaku.com\",\n"
                + "        \"Wa Aktif\": \"087859168686\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.35837105324,\n"
                + "        \"Bank ID\": 835,\n"
                + "        \"Nama BPR\": \"PT. BPR KARYA UTAMA JABAR\",\n"
                + "        \"Email Aktif\": \"bprku.pusat@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085295351243\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.36071033565,\n"
                + "        \"Bank ID\": \"03000000002668\",\n"
                + "        \"Nama BPR\": \"PT BPR CARUBAN INDAH\",\n"
                + "        \"Email Aktif\": \"purnamawati80@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085335075350\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.36071015046,\n"
                + "        \"Bank ID\": 425,\n"
                + "        \"Nama BPR\": \"PT BPR ARTHAYASA AGENG\",\n"
                + "        \"Email Aktif\": \"agengbank@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085740308216\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.3630247338,\n"
                + "        \"Bank ID\": \"0410000010154\",\n"
                + "        \"Nama BPR\": \"PD. BPR Kabupaten Bulungan\",\n"
                + "        \"Email Aktif\": \"Rizalbprbulungan@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085390930001\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.36368232639,\n"
                + "        \"Bank ID\": 2421,\n"
                + "        \"Nama BPR\": \"BPRS Amanah Bangsa\",\n"
                + "        \"Email Aktif\": \"bprs.amanahbangsa@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082272686833\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.364732013884,\n"
                + "        \"Bank ID\": 1329,\n"
                + "        \"Nama BPR\": \"BPR Agrimakmur Lestari\",\n"
                + "        \"Email Aktif\": \"agrimakmurlestari@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"081226111842\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.36597961806,\n"
                + "        \"Bank ID\": 1730,\n"
                + "        \"Nama BPR\": \"BPR Gunung Kinibalu\",\n"
                + "        \"Email Aktif\": \"ptbprgk@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082216657660\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.36787003472,\n"
                + "        \"Bank ID\": 1951,\n"
                + "        \"Nama BPR\": \"BPR GRAHA LESTARI\",\n"
                + "        \"Email Aktif\": \"laluyudidarmaprayana@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081907341006\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.374654687505,\n"
                + "        \"Bank ID\": 2788,\n"
                + "        \"Nama BPR\": \"PT. BPR NUSAMBA GENTENG\",\n"
                + "        \"Email Aktif\": \"nusamba_genteng@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085755536618\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.37963054398,\n"
                + "        \"Bank ID\": 2012,\n"
                + "        \"Nama BPR\": \"PT BPR NAGA\",\n"
                + "        \"Email Aktif\": \"bprnaga@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081803646459\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.38261261574,\n"
                + "        \"Bank ID\": 3786,\n"
                + "        \"Nama BPR\": \"PT BPR DANA PRIMA MANDIRI\",\n"
                + "        \"Email Aktif\": \"bprdpm001@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085264956823\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.3842453588,\n"
                + "        \"Bank ID\": 1758,\n"
                + "        \"Nama BPR\": \"PT BPR WELERI JAYAPERSADA\",\n"
                + "        \"Email Aktif\": \"bpr.welerijayapersada@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081904800673\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.38429800926,\n"
                + "        \"Bank ID\": 70275,\n"
                + "        \"Nama BPR\": \"PD.BPR ROKAN HILIR\",\n"
                + "        \"Email Aktif\": \"bank_rohil@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085376007403\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.38437020833,\n"
                + "        \"Bank ID\": \"02600000000789\",\n"
                + "        \"Nama BPR\": \"PT. BPR TRISURYA TATA ARTHA\",\n"
                + "        \"Email Aktif\": \"bprtrisuryatataartha@ymail.com\",\n"
                + "        \"Wa Aktif\": \"0895607431050\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.386513819445,\n"
                + "        \"Bank ID\": \"072168\",\n"
                + "        \"Nama BPR\": \"PD.BPR BANK PASAR KAB LUMAJANG\",\n"
                + "        \"Email Aktif\": \"bankpasarlumajang@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085258033979\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.386920578705,\n"
                + "        \"Bank ID\": 20,\n"
                + "        \"Nama BPR\": \"PT. BPR CHANDRA MUKTIARTHA\",\n"
                + "        \"Email Aktif\": \"bprcma@bprcma.co.id\",\n"
                + "        \"Wa Aktif\": \"081232426446\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.38815143518,\n"
                + "        \"Bank ID\": 244,\n"
                + "        \"Nama BPR\": \"BPR DHANA SEMESTA\",\n"
                + "        \"Email Aktif\": \"Info@bprdhanasemesta@gmail.com\",\n"
                + "        \"Wa Aktif\": \"087838427162\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.38822878472,\n"
                + "        \"Bank ID\": 2248,\n"
                + "        \"Nama BPR\": \"PD. BPR ROKAN HILIR\",\n"
                + "        \"Email Aktif\": \"bank_rohil@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085376007403\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.388515810184,\n"
                + "        \"Bank ID\": \"070178\",\n"
                + "        \"Nama BPR\": \"KBPR ARTA KENCANA\",\n"
                + "        \"Email Aktif\": \"artakencana.bpr@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"081232502893\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.39275777778,\n"
                + "        \"Bank ID\": \"02400000000784\",\n"
                + "        \"Nama BPR\": \"BPR TAPEUNA DANA\",\n"
                + "        \"Email Aktif\": \"tapeunadana@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085275004637\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.392794224535,\n"
                + "        \"Bank ID\": 2611,\n"
                + "        \"Nama BPR\": \"PT. BPR ARTHA SAMUDERA INDONESIA\",\n"
                + "        \"Email Aktif\": \"saputrad22@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085155255770\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.39595810186,\n"
                + "        \"Bank ID\": 1124,\n"
                + "        \"Nama BPR\": \"PT BPR PRIMADANA ABADI \",\n"
                + "        \"Email Aktif\": \"bpr-primadana@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085267133779\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.39751548611,\n"
                + "        \"Bank ID\": 1130,\n"
                + "        \"Nama BPR\": \"BPR Tri Gunung Selatan\",\n"
                + "        \"Email Aktif\": \"bpr_tgs@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"082282390000\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.39913344907,\n"
                + "        \"Bank ID\": 2248,\n"
                + "        \"Nama BPR\": \"PD. BPR ROKAN HILIR\",\n"
                + "        \"Email Aktif\": \"bank_rohil@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085376007403\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.39939416667,\n"
                + "        \"Bank ID\": 5077,\n"
                + "        \"Nama BPR\": \"PT BPRS MITRA HARMONI KOTA MALANG\",\n"
                + "        \"Email Aktif\": \"mhm.syariah1@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082132630013\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40076924769,\n"
                + "        \"Bank ID\": 150,\n"
                + "        \"Nama BPR\": \"PT BPR BEPEDE KUTAI SEJAHTERA\",\n"
                + "        \"Email Aktif\": \"kantorpusat@bprbepedeks.co.id\",\n"
                + "        \"Wa Aktif\": \"081348641212\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40096915509,\n"
                + "        \"Bank ID\": 449,\n"
                + "        \"Nama BPR\": \"PT BPR BKK KARANGMALANG\",\n"
                + "        \"Email Aktif\": \"bprkarangmalang@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"08562500705\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40232108797,\n"
                + "        \"Bank ID\": 5160,\n"
                + "        \"Nama BPR\": \"PT BPR DANAFAST\",\n"
                + "        \"Email Aktif\": \"bprdanafastkaltara@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085245136707\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40420024305,\n"
                + "        \"Bank ID\": 244,\n"
                + "        \"Nama BPR\": \"BPR DHANA SEMESTA\",\n"
                + "        \"Email Aktif\": \"Info@bprdhanasemesta.co.id\",\n"
                + "        \"Wa Aktif\": \"087838427162\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40618039352,\n"
                + "        \"Bank ID\": 1821,\n"
                + "        \"Nama BPR\": \"PT BPR SINARGUNA SEJAHTERA\",\n"
                + "        \"Email Aktif\": \"angelmn612@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081229077065\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40670553241,\n"
                + "        \"Bank ID\": \"03200000001918\",\n"
                + "        \"Nama BPR\": \"PT BPR DANA BINTAN SEJAHTERA\",\n"
                + "        \"Email Aktif\": \"bpr_dbs@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085278150146\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40692060185,\n"
                + "        \"Bank ID\": 425,\n"
                + "        \"Nama BPR\": \"PT BPR ARTHAYASA AGENG\",\n"
                + "        \"Email Aktif\": \"agengbank@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085740308216\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40737793982,\n"
                + "        \"Bank ID\": \"02500000001010\",\n"
                + "        \"Nama BPR\": \"PT. BPR RAP GANDA\",\n"
                + "        \"Email Aktif\": \"rapganda_pusat@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085230629303\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.4075302662,\n"
                + "        \"Bank ID\": \"03200000001918\",\n"
                + "        \"Nama BPR\": \"PT .BPR DANA BINTAN SEJAHTERA\",\n"
                + "        \"Email Aktif\": \"bpr_dbs@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085278150146\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.40753251157,\n"
                + "        \"Bank ID\": 2460,\n"
                + "        \"Nama BPR\": \"BPR Nusantara Bona Pasogit 6\",\n"
                + "        \"Email Aktif\": \"bpr_nbp06@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"082360354304\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.4097621875,\n"
                + "        \"Bank ID\": 429,\n"
                + "        \"Nama BPR\": \"PT BPR BANK KLATEN (PERSERODA)\",\n"
                + "        \"Email Aktif\": \"bankklaten@ymail.com\",\n"
                + "        \"Wa Aktif\": \"081388887005\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.41076590278,\n"
                + "        \"Bank ID\": \"071279\",\n"
                + "        \"Nama BPR\": \"PT. BPR DARMAWAN ADHIGUNA LESTARI\",\n"
                + "        \"Email Aktif\": \"jkssbma@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"081517335500\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.411718206014,\n"
                + "        \"Bank ID\": 92,\n"
                + "        \"Nama BPR\": \"PT. BPR Musi Artha Surya\",\n"
                + "        \"Email Aktif\": \"bprmas_plbg@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085373261696\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.41709325231,\n"
                + "        \"Bank ID\": 158,\n"
                + "        \"Nama BPR\": \"PT.  BPR BUNGAMAYAMG AGROLOKA\",\n"
                + "        \"Email Aktif\": \"bpr.buma@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"081271015537\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.42005152778,\n"
                + "        \"Bank ID\": 1124,\n"
                + "        \"Nama BPR\": \"PT BPR PRIMA DANA ABADI \",\n"
                + "        \"Email Aktif\": \"bpr_primadana\",\n"
                + "        \"Wa Aktif\": \"085267133779\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.42048901621,\n"
                + "        \"Bank ID\": 2788,\n"
                + "        \"Nama BPR\": \"PT. BPR NUSAMBA GENTENG\",\n"
                + "        \"Email Aktif\": \"nsbgenteng@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085755536618\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.42287052084,\n"
                + "        \"Bank ID\": 2607,\n"
                + "        \"Nama BPR\": \"PT. BPR ARTHA PAMENANG\",\n"
                + "        \"Email Aktif\": \"bpr_artha_pamenang@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085736604419\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.42469733796,\n"
                + "        \"Bank ID\": 2607,\n"
                + "        \"Nama BPR\": \"PT. BPR ARTHA PAMENANG Cab JOMBANG\",\n"
                + "        \"Email Aktif\": \"bprap.jombang@gmail.com\",\n"
                + "        \"Wa Aktif\": \"082233453311\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.425847719904,\n"
                + "        \"Bank ID\": \"02400000003072\",\n"
                + "        \"Nama BPR\": \"PT. BPR Anugerah Artasentosa Prima\",\n"
                + "        \"Email Aktif\": \"bpranugerah@gmail.com\",\n"
                + "        \"Wa Aktif\": \"0811171760\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.4273118287,\n"
                + "        \"Bank ID\": 1013,\n"
                + "        \"Nama BPR\": \"BPR INGERTAD BANGUN UTAMA\",\n"
                + "        \"Email Aktif\": \"bpr_ingertad@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"085393137128\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.430902939814,\n"
                + "        \"Bank ID\": 2887,\n"
                + "        \"Nama BPR\": \"PT BPR TRIKARYA WARANUGRAHA\",\n"
                + "        \"Email Aktif\": \"trikarya_bpr@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"085655593666\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.43113369213,\n"
                + "        \"Bank ID\": 1000,\n"
                + "        \"Nama BPR\": \"PT.BPRS HIKMAH WAKILAH\",\n"
                + "        \"Email Aktif\": \"bprshw@gmail.com\",\n"
                + "        \"Wa Aktif\": \"08126901365\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.43732085648,\n"
                + "        \"Bank ID\": 172,\n"
                + "        \"Nama BPR\": \"BPR PRIMA RIAU SENTOSA\",\n"
                + "        \"Email Aktif\": \"ardisonputra27@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085274523231\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.44557418981,\n"
                + "        \"Bank ID\": 2800000000498,\n"
                + "        \"Nama BPR\": \"PT BPR INTAN SURYA\",\n"
                + "        \"Email Aktif\": \"intan_surya_bpr@yahoo.co.id\",\n"
                + "        \"Wa Aktif\": \"0811272187\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.447789432874,\n"
                + "        \"Bank ID\": \"02800000000413\",\n"
                + "        \"Nama BPR\": \"PT BPR ARTA MAS SURAKARTA\",\n"
                + "        \"Email Aktif\": \"bprartamassurakarta@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081393607100\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.45093337963,\n"
                + "        \"Bank ID\": 413,\n"
                + "        \"Nama BPR\": \"PT BPR ARTA MAS SURAKARTA\",\n"
                + "        \"Email Aktif\": \"bprartamassurakarta@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081393607100\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.45118880787,\n"
                + "        \"Bank ID\": \"02800000000470\",\n"
                + "        \"Nama BPR\": \"BPR CATUR ARTHA JAYA\",\n"
                + "        \"Email Aktif\": \"achmadnoorsyarifudin@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085740565068\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.45274255787,\n"
                + "        \"Bank ID\": 2800000011863,\n"
                + "        \"Nama BPR\": \"Perumda BPR Bank Brebes\",\n"
                + "        \"Email Aktif\": \"bankbrebes@gmail.com\",\n"
                + "        \"Wa Aktif\": \"089672735424\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.470653275464,\n"
                + "        \"Bank ID\": 15295,\n"
                + "        \"Nama BPR\": \"PD.BPR BANK PASAR KABUPATEN LUMAJANG\",\n"
                + "        \"Email Aktif\": \"akhmadfauzi1981@gmail.com\",\n"
                + "        \"Wa Aktif\": \"085258033979\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.47857229167,\n"
                + "        \"Bank ID\": 600691,\n"
                + "        \"Nama BPR\": \"PT BPR CITA DEWI\",\n"
                + "        \"Email Aktif\": \"bprcitadewi@gmail.com\",\n"
                + "        \"Wa Aktif\": \"081802547009\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"Timestamp\": 44111.47888846065,\n"
                + "        \"Bank ID\": 601854,\n"
                + "        \"Nama BPR\": \"PT BPR MITRA BALI MANDIRI\",\n"
                + "        \"Email Aktif\": \"bpr.mitramandiri@yahoo.com\",\n"
                + "        \"Wa Aktif\": \"082237443611\"\n"
                + "    }\n"
                + "]"
        );

        for (int x = 0; x < dataArr.length(); x++) {
//            System.out.println(dataArr.getJSONObject(x).toString());
            OkHttpClient client = new OkHttpClient();

            // desc:
            /* <editor-fold> */
            /**
             * {
             * "bank_id" : "2499", "bank_name": "PT Developer", "email":
             * "andridseptian@gmail.com" } *
             */
            /* END </editor-fold> */
            JSONObject requestBody = new JSONObject()
                    .put("bank_id", String.valueOf(dataArr.getJSONObject(x).get("Bank ID")))
                    .put("bank_name", dataArr.getJSONObject(x).get("Nama BPR"))
                    .put("email", dataArr.getJSONObject(x).get("Email Aktif"));

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\n\t\"bank_id\": \"508\"\n}");
            Request request = new Request.Builder()
                    .url("http://117.54.12.173:9980/vpn/broadcast")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                System.out.println(response.body().string());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void testingParsingVersion() {
        String currentVersion = "3.0.0.0";
        String databaseVersion = "3.0.1.0";

        String[] tempCurrentVersion = currentVersion.split("\\.");
        String[] tempDatabaseVersion = databaseVersion.split("\\.");

        for (int i = 0; i < tempDatabaseVersion.length; i++) {
            System.out.print(tempCurrentVersion[i] + ":" + tempDatabaseVersion[i]);

            if (Integer.parseInt(tempCurrentVersion[i])
                    < Integer.parseInt(tempDatabaseVersion[i])) {
                System.out.print(" - Outdated");
            }

            System.out.println("");
        }

    }

    public static void testingparsingtanggal() {
        try {
            String code = "20191112204222650"; //2019 11 12 20:42:22.650
            String pattern = "yyyyMMddHH:mm:sss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            Date parseDate = simpleDateFormat.parse(code);

            System.out.println("date: " + parseDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDiscalimerToPDF(String data) {

        try {

            File file = File.createTempFile("disclaimer", ".html");
            System.out.println(file);

            new Thread(() -> {
                try {
                    InputStream is = new FileInputStream("D:\\User\\Documents\\NetBeansProjects\\sharing-bandwidth-apps-v3\\view\\temp\\disclaimer.html");

                    // read from a temporary file
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            System.out.println(line);
                        }
                    }

                    generatePDFFromHTML(file, "bungkus.pdf");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void generatePDFFromHTML(File file, String location) throws FileNotFoundException, DocumentException, IOException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document,
                new FileOutputStream(location));
        document.open();

        XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream("D:\\User\\Documents\\NetBeansProjects\\sharing-bandwidth-apps-v3\\view\\temp\\disclaimer.html"));
        document.close();
    }

    public static void ktpTesting() {
        System.out.println("Testing KTP");

        String pcid = "2017BB12151000000000000000020651";
        String conf = "15C6FF254E8CE0B12AE6DA563B4B99E11E3A744687C8A19D833C8162E5062624";

        // Initialize KTP
        String ktpStr, ktpFpStr;
        Ektp.ExtractJar();
        Ektp.myInterface = EktpInterface.INSTANCE;
        Ektp.InitEktp(pcid, conf);

        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Letakkan KTP");
        String userName = myObj.nextLine();  // Read user input

        System.out.println(getDeviceId());

        int ret = -1;
        byte[] ktpData = new byte[65536];
        byte[] fpData = new byte[2048];
        ret = Ektp.myInterface.EktpReadCard(Ektp.hContext, ktpData, fpData);

        if (ret != 0) {
            System.out.println("EKTP tidak ditemukan");
        } else {
            ktpStr = new String(ktpData);
            int tempLen = Ektp.getRealLength(ktpData);
            ktpStr = String.copyValueOf(ktpStr.toCharArray(), 0, tempLen);

            ktpFpStr = new String(fpData);
            int ktpFpStrSize = Ektp.getRealLength(fpData);
            ktpFpStr = String.copyValueOf(ktpFpStr.toCharArray(), 0, ktpFpStrSize);

            doReadFpOff(ktpStr, ktpFpStr);
        }
    }

    public static void doReadFpOff(String ktpStr, String ktpFpStr) {

        if (Ektp.hContext == null) {
            System.out.println("Context Null");
            return;
        }
        if (ktpFpStr == null || ktpFpStr.length() == 0) {
            System.out.println("KTP tidak terscan");
            return;
        }

//        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Letakkan Jari Telunjuk");
//        String userName = myObj.nextLine();  // Read user input

//        java.util.Timer timer = new java.util.Timer();
//        timer.schedule(task, 10 * 1000);
        int ret = -1;
        IntByReference score = new IntByReference(-1);
        byte[] imageData = new byte[240001];

        int count = 0;
        while (ret != 0) {
            System.out.println("count: " + count++);
            System.out.println("ret: " + ret);
            ret = Ektp.myInterface.CaptureFingerPrint(imageData, score);
        }

        if (ret != 0) {
            System.out.println("data tidak ada " + ret);
            return;
        }
        String imageStr = new String(imageData);

        IntByReference matched = new IntByReference(0);
        ret = Ektp.myInterface.MatchFingerPrint(imageStr, 240000, ktpFpStr, ktpFpStr.length(), score, matched);
        if (ret != 0) {
            System.out.println("data tidak ada");
            return;
        }
        int match = matched.getValue();
        if (match == 1) {
            System.out.println("match == 1");
            System.out.println("Sidik jari cocok");
            showEktpDataOff(ktpStr);

        } else {
            System.out.println("match didn't");
            doReadFpOff(ktpStr, ktpFpStr);
        }
    }

    public static String getDeviceId() {
        if (Ektp.hContext == null) {

        }
        int ret = -1;
        byte[] uidData = new byte[20];
        ret = Ektp.myInterface.EktpReadPsamUid(Ektp.hContext, uidData);
        if (ret != 0) {

        }

        String psamUid = new String(uidData);
        int tempLen = Ektp.getRealLength(uidData);
        psamUid = String.copyValueOf(psamUid.toCharArray(), 0, tempLen);

        return psamUid;
    }

    private static void showEktpDataOff(String ktpStr) {
        try {
            String strList[] = ktpStr.split(";");
//            tfDataNIKOff.setText(strList[1]);
//            tfDataAlamatOff.setText(strList[2]);
//            tfDataRTRWOff.setText(strList[3] + "/" + strList[4]);
//            tfDataTTLOff.setText(strList[5] + ", " + strList[15]);
//            tfDataKecamatanOff.setText(strList[6]);
//            tfDataKelurahanOff.setText(strList[7]);
//            tfDataKotaOff.setText(strList[8]);
//            tfDataJenisKelaminOff.setText(strList[9]);
//            tfDataAgamaOff.setText(strList[11]);
//            tfDataStatusOff.setText(strList[12]);
//            tfDataPekerjaanOff.setText(strList[13]);
//            tfDataNamaOff.setText(strList[14]);
//            tfDataPropinsiOff.setText(strList[16]);
//            tfDataGoldarOff.setText(strList[10]);
//            tfDataKewarganegaraanOff.setText(strList[20]);
//            tfDataMasaBerlakuOff.setText(strList[17]);

            for (int i = 0; i <= strList.length; i++) {
                System.out.println(strList[i]);
            }

//            byte photoBytes[] = hexStrToBytes(strList[0]);
//            InputStream inputStream = new ByteArrayInputStream(photoBytes, 0, photoBytes.length);
//            BufferedImage photoBuffer = ImageIO.read(inputStream);
//            Image photoImage = SwingFXUtils.toFXImage(photoBuffer, null);
//            tempPhotoOff = photoImage;
//            ImageView ivPhotoImage = new ImageView(photoImage);
//            ivPhotoImage.setFitHeight(186);
//            ivPhotoImage.setFitWidth(156);
//            spFotoKTPOff.getChildren().add(ivPhotoImage);
//
//            byte signatureBytes[] = hexStrToBytes(strList[strList.length - 1]);
//            BufferedImage signBuffer = new BufferedImage(168, 44, BufferedImage.TYPE_BYTE_BINARY);
//            byte[] signArray = ((DataBufferByte) signBuffer.getRaster().getDataBuffer()).getData();
//            System.arraycopy(signatureBytes, 0, signArray, 0, signatureBytes.length);
//            Image signImage = SwingFXUtils.toFXImage(signBuffer, null);
//            tempSignOff = signImage;
//            ImageView ivSignImage = new ImageView(signImage);
//            ivSignImage.setFitHeight(66);
//            ivSignImage.setFitWidth(169);
//            spFotoTTDOff.getChildren().add(ivSignImage);
        } catch (Exception ex) {

        }
    }

    private static class forwarding {

        static String host = "103.28.148.203";
        static String user = "root";
        static String password = "Qawsed#1477";
        static int port = 7789;

        static int tunnelLocalPort = 80;
        static String tunnelRemoteHost = "172.16.50.7";
        static int tunnelRemotePort = 8080;
        static Session ses;

        private static void start() {
            try {
                System.out.println("Forwarding port --");
                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                JSch jsch = new JSch();
                ses = jsch.getSession(user, host, port);
                ses.setPassword(password);
                ses.setConfig(config);
                ses.connect();
                ses.setPortForwardingL(tunnelLocalPort, tunnelRemoteHost, tunnelRemotePort);
                System.out.println("Connected");
                System.out.println("Port Forwarded.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private static void stop() {
            ses.disconnect();
        }
    }
}
