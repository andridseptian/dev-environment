/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.connector;

import com.dak.perbarindo.sb.apps.controller.MainController;
import com.dak.perbarindo.sb.apps.utility.Download;
import java.io.File;
import javafx.scene.control.Alert;
import net.lingala.zip4j.core.ZipFile;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author ROG
 */
public class DownloadConnector {

    private static final Logger log = LogManager.getLogger(DownloadConnector.class);

    MainController mc;

    public DownloadConnector(MainController mc) {
        this.mc = mc;
    }

    public void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkUpdate() {
        log.info("--check update--");
        sleep(1000);
        new Thread(() -> {
            sleep(1000);
            try {
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, "{\n\t\"type\": \"partial\"\n}");
                Request request = new Request.Builder()
                        .url("http://103.28.148.203:61005/sb/check_version")
                        .post(body)
                        .addHeader("Content-Type", "application/json")
                        .build();
                Response response = client.newCall(request).execute();
                JSONObject respBody = new JSONObject(response.body().string());
                log.info(respBody.toString());
                if (respBody.has("rc")) {
                    if (respBody.getString("rc").equals("00")) {
                        JSONObject data = respBody.getJSONObject("data");
                        log.info(data);
                        String partialVersion = mc.readFile("partial.conf", System.getProperty("user.dir") + "\\config\\");

                        log.info("partial: " + partialVersion + " version: " + data.getString("version_code"));
                        if (partialVersion == null || !partialVersion.equals(data.getString("version_code"))) {
                            sleep(1000);
                            mc.executeScript("checkUpdate.waitingForUpdate()");

                            String FILE_NAME = "\\partial.zip";
                            String PATH = System.getProperty("user.dir");

                            Download down = new Download();
                            down.get("partial.zip",
                                    data.getString("link"));
                            if (downloadInstall(FILE_NAME, PATH)) {
                                mc.writeFile("partial.conf", System.getProperty("user.dir") + "\\config\\", data.getString("version_code"));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.info(new String(), e);
            }
        }).start();
    }

    public boolean downloadInstall(String FILE_NAME, String PATH) {
        try {
            log.info("unpacking");
            log.info(PATH + FILE_NAME);
            log.info("-->" + PATH);
            ZipFile zipFile = new ZipFile(PATH + FILE_NAME);
            zipFile.extractAll(PATH);
            Thread.sleep(3000);
            File downloaded = new File(PATH + FILE_NAME);
            if (downloaded.delete()) {
                log.info("Deleted the file: " + downloaded.getName());
                mc.executeScript("checkUpdate.updateCompleted()");
                return true;
            } else {
                log.info("Failed to delete the file.");
                mc.sendAlertToJS("Gagal memasang update");
                return false;
            }
        } catch (Exception e) {
            log.error(new String(), e);
            mc.sendAlertToJS("Gagal memasang update, " + e.getMessage());
            return false;
        }
    }
}
