/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.request.service;

import com.dak.perbarindo.sb.apps.request.RequestData;
import com.dak.perbarindo.sb.apps.request.SendRequest;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author ROG
 */
public class UserReq {

    private static final Logger log = LogManager.getLogger(UserReq.class);

    public static JSONObject check_admin(String url, String body) {
        try {

            log.info("check_admin");
            JSONObject response = SendRequest.post(url, body);

            log.info("response: ");
            log.info(response);

            if (response != null && response.has("rc")) {
                return RequestData.res.execute("regis", "registeringUser", response);
            } else {
                return RequestData.res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }
        } catch (Exception e) {
            log.error(new String(), e);
            return RequestData.res.fail("Gagal mendapatkan data serial number", e.getMessage());
        }
    }
}
