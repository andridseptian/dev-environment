/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.request;

import com.dak.perbarindo.sb.apps.MainApp;
import com.dak.perbarindo.sb.apps.data.Terminal;
import com.dak.perbarindo.sb.apps.utility.Constants;
import com.dak.perbarindo.sb.apps.utility.TripleDES;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class SendRequest {

    private static final Logger log = LogManager.getLogger(SendRequest.class);

    protected static boolean development = MainApp.developement;

    public static JSONObject post(String path, String reqBody) {
        try {
            TripleDES td = new TripleDES(Constants.ENCRYPT);

            if (development) {
                log.info("send POST - ");
                log.info(path);
                log.info(reqBody);
            } else {
                log.info("send POST - ");
                log.info(td.encrypt(path));
                log.info(td.encrypt(reqBody));
            }
            OkHttpClient client = new OkHttpClient();

            TripleDES tdes = new TripleDES(Constants.ENCRYPT);

            JSONObject jsReq = new JSONObject(reqBody);

//            jsReq.put(Constants.INPUT_PARAMETER, Terminal.getSerial());
//            jsReq.put("terminal", Terminal.getTerminalData());
            log.info(td.encrypt(jsReq.toString()));

            MediaType mediaType = MediaType.parse("text/plain");
//            RequestBody body = RequestBody.create(mediaType, tdes.encrypt(jsReq.toString()));
            RequestBody body = RequestBody.create(mediaType, jsReq.toString());
            Request request = new Request.Builder()
                    .url(path)
                    .post(body)
                    .build();

            Response response = client.newCall(request).execute();

//            String resBody = tdes.decrypt(response.body().string());
            String resBody = response.body().string();

            if (development) {
                log.info("response POST -");
                log.info(resBody);
            } else {
                log.info("response POST -");
                log.info(td.encrypt(resBody));
            }

            return new JSONObject(resBody);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject postOld(String path, String reqBody) {

        try {
            TripleDES td = new TripleDES(Constants.ENCRYPT);

            if (development) {
                log.info("send POST - ");
                log.info(path);
                log.info(reqBody);
            } else {
                log.info("send POST - ");
                log.info(td.encrypt(path));
                log.info(td.encrypt(reqBody));
            }

            forwarding.start();
            OkHttpClient client = new OkHttpClient();
            TripleDES tdes = new TripleDES(Constants.ENCRYPT);
            JSONObject jsReq = new JSONObject(reqBody);

            jsReq.put(Constants.INPUT_PARAMETER, Terminal.getSerial());
            log.info(td.encrypt(jsReq.toString()));

            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, tdes.encrypt(jsReq.toString()));
            Request request = new Request.Builder()
                    .url(path)
                    .post(body)
                    .build();

            Response response = client.newCall(request).execute();

            String resBody = tdes.decrypt(response.body().string());
//            String resBody = response.body().string();

            if (development) {
                log.info("response POST -");
                log.info(resBody);
            } else {
                log.info("response POST -");
                log.info(td.encrypt(resBody));
            }

            forwarding.stop();
            return new JSONObject(resBody);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject postPublic(String path, String reqBody) {

        try {
            TripleDES td = new TripleDES(Constants.ENCRYPT);
            if (development) {
                log.info("send POST - ");
                log.info(path);
                log.info(reqBody);
            } else {
                log.info("send POST - ");
                log.info(td.encrypt(path));
                log.info(td.encrypt(reqBody));
            }

            OkHttpClient client = new OkHttpClient();

            TripleDES tdes = new TripleDES(Constants.ENCRYPT);

            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, tdes.encrypt(new JSONObject(reqBody)
                    .put(Constants.INPUT_PARAMETER,
                            Terminal.getSerial()).toString()));
            Request request = new Request.Builder()
                    .url(path)
                    .post(body)
                    .addHeader("accept", "application/json")
                    .addHeader("apikey", "P3RB@R1ND0D1G1T4L4M0R3KR1Y4N3S14")
                    .build();

            Response response = client.newCall(request).execute();

            String resBody = tdes.decrypt(response.body().string());

            if (development) {
                log.info("response POST -");
                log.info(resBody);
            } else {
                log.info("response POST -");
                log.info(td.encrypt(resBody));
            }

            return new JSONObject(resBody);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject postPublicWOEncrypt(String path, String reqBody) {

        try {
            TripleDES td = new TripleDES(Constants.ENCRYPT);
            if (development) {
                log.info("send POST - ");
                log.info(path);
                log.info(reqBody);
            } else {
                log.info("send POST - ");
                log.info(td.encrypt(path));
                log.info(td.encrypt(reqBody));
            }

            OkHttpClient client = new OkHttpClient();

            TripleDES tdes = new TripleDES(Constants.ENCRYPT);

            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, reqBody);
            Request request = new Request.Builder()
                    .url(path)
                    .post(body)
                    .addHeader("accept", "application/json")
                    .addHeader("apikey", "P3RB@R1ND0D1G1T4L4M0R3KR1Y4N3S14")
                    .build();

            Response response = client.newCall(request).execute();

            String resBody = response.body().string();

            if (development) {
                log.info("response POST -");
                log.info(resBody);
            } else {
                log.info("response POST -");
                log.info(td.encrypt(resBody));
            }

            return new JSONObject(resBody);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    private static class forwarding {

        static String host = "vpn.srv.co.id";
        static String user = "root";
        static String password = "Qawsed#59989";
        static int port = 7879;

        static int tunnelLocalPort = 9080;
        static String tunnelRemoteHost = "192.168.100.18";
        static int tunnelRemotePort = 7879;
        static Session ses;

        private static void start() {
            try {
                log.info("Forwarding port --");
                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                JSch jsch = new JSch();
                ses = jsch.getSession(user, host, port);
                ses.setPassword(password);
                ses.setConfig(config);
                ses.connect();
                ses.setPortForwardingL(tunnelLocalPort, tunnelRemoteHost, tunnelRemotePort);
                log.info("Connected");
                log.info("Port Forwarded.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private static void stop() {
            ses.disconnect();
        }
    }

}
