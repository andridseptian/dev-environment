/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.connector;

import com.dak.perbarindo.sb.apps.controller.MainController;
import com.dak.perbarindo.sb.apps.data.Config;
import com.dak.perbarindo.sb.apps.utility.VpnConnection;
import com.jcraft.jsch.JSch;
import java.io.File;
import java.util.Date;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import com.jcraft.jsch.Session;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.Random;
import java.util.Timer;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author drid
 */
public class PortalConnector {
    
    private static final Logger log = LogManager.getLogger(PortalConnector.class);
    
    private WebView webView;
    private MainController mc;
    
    private TimerTask countingConnection;
    private Timer countingTimer;
    private long connectionDelay = 10 * 60 * 1000L;
    
    private JSch jsch = new JSch();
    private Session session;
    private int connectionStep = 0;
    
    public PortalConnector(MainController mainController) {
        mc = mainController;
        this.webView = mc.webView;
    }
    
    public void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void openWebPortalCheckNik() {
        log.info("--openWebPortalCheckNik--");
        new Thread(() -> {
            sleep(1000);
            try {
                Config.portforward.getConfig();
                int port = 7700 + new Random().nextInt(99);
                int retry = 0;
                while (!portForwarding(port, Config.portforward.webPortalNikHost, 80)) {
                    retry++;
                    log.info("attempt retry: " + retry);
                    port = 7700 + new Random().nextInt(99);
                }
                
                sleep(5000);
                String portalUrl = "http://localhost:" + port + "/webportal";
                log.info("port forwarded: " + portalUrl);
                mc.sendAlertToJS("Hai, web portal anda sedang dimuat, jangan lupa untuk mengisi form data balikan setelah anda melakukan pengecekan pada Web Portal Dukcapil. Jika browser tidak terbuka otomatis, akses portal anda di link berikut: "
                        + "<br><br><b>" + portalUrl + "</b>");
                sleep(1000);
                mc.executeScript("showReturnDataPage()");
                Desktop.getDesktop().browse(new URI(portalUrl));
                
            } catch (Exception e) {
                log.info("catch error");
                e.printStackTrace();
                mc.sendAlertToJS("Gagal membuka web portal check nik, " + e.getMessage());
                log.error(new String(), e);
            }
        }).start();
        
    }
    
    public void openWebPortalDatabalikan() {
        log.info("--openWebPortalDatabalikan--");
        new Thread(() -> {
            sleep(1000);
            try {
                Config.portforward.getConfig();
                int port = 8800 + new Random().nextInt(99);
                int retry = 0;
                while (!portForwarding(port, Config.portforward.webPortalDatabalikanHost, 80)) {
                    retry++;
                    log.info("attempt retry: " + retry);
                    port = 8800 + new Random().nextInt(99);
                }
                log.info("port forwarded");
                sleep(5000);
                String portalUrl = "http://localhost:" + port + "/databalikan";
                log.info("port forwarded: " + portalUrl);
                mc.sendAlertToJS("Hai, web portal anda sedang dimuat, Jika browser tidak terbuka secara otomatis, akses portal anda dilink berikut: <br><br>"
                        + "<b>" + portalUrl + "</b>");
                mc.executeScript("showReturnDataPage()");
                Desktop.getDesktop().browse(new URI("http://localhost:" + port + "/databalikan"));
            } catch (Exception e) {
                e.printStackTrace();
                log.error(new String(), e);
                mc.sendAlertToJS("Gagal membuka web portal databalikan, " + e.getMessage());
            }
        }).start();
    }
    
    public void openWebPortalCheckNikViaVPN() {
        log.info("--openWebPortalCheckNikViaVPN--");
        new Thread(() -> {
            sleep(1000);
            try {
                if (VpnConnection.connectVPN()) {
                    Platform.runLater(() -> {
                        mc.executeScript("showReturnDataPage()");
                        mc.executeScript("portal.connectedToVPN()");
                        mc.sendAlertToJS("Hai, web portal anda sedang dimuat, jangan lupa untuk mengisi form data balikan setelah anda melakukan pengecekan pada Web Portal Dukcapil");
                    });
                    
                    try {
                        log.info("open app -");
                        File webportal = new File(System.getProperty("user.dir") + "\\webportal\\web-portal.exe");
                        Desktop.getDesktop().open(webportal);
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error(new String(), e);
                        mc.sendAlertToJS("Gagal melakukan koneksi ke VPN, " + e.getMessage());
                    }
                    
                    if (countingTimer != null) {
                        log.info("terminate old sessions");
                        countingTimer.cancel();
                    }
                    countingConnection = new TimerTask() {
                        public void run() {
                            MainController.setAlertOnTop("Cek Notifikasi Anda", Alert.AlertType.WARNING);
                            Platform.runLater(() -> {
                                String function = "showConnectionAlert()";
                                webView.getEngine().executeScript(function);
                                log.info("execute command - " + function);
                            });
                            
                            countingTimer.cancel();
                        }
                    };
                    countingTimer = new Timer("Timer");
                    countingTimer.schedule(countingConnection, connectionDelay);
                } else {
                    mc.sendAlertToJS("Gagal menghubungkan terminal kejaringan bersama, coba untuk menggunakan fitur web portal melalui web browser");
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error(new String(), e);
                mc.sendAlertToJS("Gagal membuka web portal via aplikasi, silahkan coba untuk mengakses web portal memelalui browser. INFO: " + e.getMessage());
            }
        }).start();
    }
    
    public void continueVPN() {
        log.info("--continueVPN--");
        sleep(1000);
        new Thread(() -> {
            sleep(1000);
            try {
                if (VpnConnection.connectVPN()) {
                    if (countingTimer != null) {
                        countingTimer.cancel();
                    }
                    countingConnection = new TimerTask() {
                        public void run() {
                            System.out.println("Task performed on: " + new Date() + "n"
                                    + "Thread's name: " + Thread.currentThread().getName());
                            MainController.setAlertOnTop("Cek Notifikasi Anda", Alert.AlertType.WARNING);
                            Platform.runLater(() -> {
                                String function = "showConnectionAlert()";
                                webView.getEngine().executeScript(function);
                                log.info("execute command - " + function);
                            });
                            countingTimer.cancel();
                        }
                    };
                    countingTimer = new Timer("Timer");
                    countingTimer.schedule(countingConnection, connectionDelay);
                }
            } catch (Exception e) {
                log.error(new String(), e);
            }
            
        }).start();
    }
    
    public void cancelVPN() {
        log.info("--cancelVPN--");
        new Thread(() -> {
            if (countingTimer != null) {
                countingTimer.cancel();
            }
        }).start();
    }
    
    public void disconnectingFromVPN() {
        log.info("disconnecting to VPN -");
        sleep(1000);
        new Thread(() -> {
            sleep(1000);
            if (VpnConnection.disconnectVPN()) {
                log.info("disconnected");
            } else {
                log.warn("failed to disconnected");
                mc.sendAlertToJS("Gagal memutuskan koneksi vpn");
            }
        }).start();
    }
    
    public void checkVPNConnection() {
        log.info("--checkVPNConnection--");
        new Thread(() -> {
            if (!VpnConnection.checkConnection()) {
                mc.executeScript("portal.vpnNotConnected()");
            }
        }).start();
    }
    
    private boolean portForwarding(int tunnelLocalPort, String tunnelHost, int tunnelRemotePort) throws IOException {
        log.info("--Port forwarding--");
        try {
            Config.portforward.getConfig();
            String host = Config.portforward.sshHost;
            String user = Config.portforward.sshUser;
            String pass = Config.portforward.sshPassword;
            int port = Config.portforward.sshPort;
            
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            
            if (session == null) {
                session = jsch.getSession(user, host, port);
                session.setPassword(pass);
                session.setConfig(config);
                session.connect();
            }
            
            session.setPortForwardingL(tunnelLocalPort, tunnelHost, tunnelRemotePort);
            log.info("forwarded: " + session.getPortForwardingL().length);
            
            return true;
            
        } catch (Exception e) {
            e.printStackTrace();
            log.error(new String(), e);
            return false;
        }
    }
}
