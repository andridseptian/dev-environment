/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.connector;

import com.dak.perbarindo.sb.apps.controller.MainController;
import com.dak.perbarindo.sb.apps.data.KtpOnline;
import com.dak.perbarindo.sb.apps.ktp.Ektp;
import com.dak.perbarindo.sb.apps.ktp.EktpInterface;
import com.sun.jna.ptr.IntByReference;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author dak-alex
 */
public class ScanConnector {

    private static final Logger log = LogManager.getLogger(ScanConnector.class);

    private WebView webView;
    private MainController mc;
    private JSObject jsConnector;

    private JSObject scanConnector;

    public ScanConnector(MainController mc) {
        this.mc = mc;
        webView = mc.webView;
        jsConnector = (JSObject) webView.getEngine().executeScript("getJsConnector()");
        scanConnector = (JSObject) webView.getEngine().executeScript("getScanConnector()");
    }

    public void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ktpInitialize() {
        // Initialize KTP
        sleep(1000);
        new Thread(() -> {
            try {
                log.info("Testing KTP");
                sleep(1000);
                String filelocation = System.getProperty("user.dir") + "\\config";
                String filename = "\\device.conf";
                JSONObject deviceConf = new JSONObject(readFile(filelocation, filename));
                if (deviceConf != null) {
                    String pcid = deviceConf.getString("pcid");
                    String conf = deviceConf.getString("conf");

                    // String pcid = "2017BB12151000000000000000020651";
                    // String conf = "15C6FF254E8CE0B12AE6DA563B4B99E11E3A744687C8A19D833C8162E5062624";
                    String ktpStr, ktpFpStr;
                    Ektp.ExtractJar();
                    Ektp.myInterface = EktpInterface.INSTANCE;
                    Ektp.InitEktp(pcid, conf);
                    Thread.sleep(2000);

                    int count = 0;
                    while (getDeviceId().equals("")) {
                        System.out.println("wait" + count);
                        count++;
                        if (count > 1000) {
                            mc.sendAlertToJS("Alat pemindai KTP tidak ditemukan, pastikan anda telah menghubungkan alat dengan terminal anda, dan pastikan anda telah menginstall driver sebelumnya");
                            return;
                        }
                    }
                    Platform.runLater(() -> {
                        String function = "scanKTP.scanningKTP()";
                        webView.getEngine().executeScript(function);
                        log.info("execute command - " + function);
                    });
                } else {
                    mc.sendAlertToJS("Konfigurasi alat tidak ditemukan, sinkronisasikan alat terlebih dahulu");
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage() == null) {
                    mc.sendAlertToJS("Konfigurasi alat tidak ditemukan, sinkronisasikan alat terlebih dahulu");
                } else {
                    mc.sendAlertToJS("Gagal menemukan scanner, " + e.getMessage());
                }
            }
        }).start();
    }

    public void scanKTP() {
        sleep(1000);
        new Thread(() -> {
            log.info("scan KTP Offline");
            try {

                String ktpStr, ktpFpStr;
                int ret = -1;

                byte[] ktpData = new byte[65536];
                byte[] fpData = new byte[2048];

                getDeviceId();
                ret = Ektp.myInterface.EktpReadCard(Ektp.hContext, ktpData, fpData);

                int count = 0;

                long delay = 2000L;

                while (ret != 0) {
//                    Timer waitTimer;
//                    waitTimer = new Timer("Timer");
//                    TimerTask waitScan = new TimerTask() {
//                        public void run() {
//                            Platform.runLater(() -> {
//                                scanning = true;
//                                String function = "scanKTP.scanningProcess()";
//                                webView.getEngine().executeScript(function);
//                                log.info("execute command - " + function);
//                                waitTimer.cancel();
//                            });
//                        }
//                    };
//                    waitTimer.schedule(waitScan, delay);
                    System.out.println("wait scan " + count++ + " context:" + Ektp.hContext.hashCode());
                    ret = Ektp.myInterface.EktpReadCard(Ektp.hContext, ktpData, fpData);

                    sleep(500);

                    if (count > 60) {
                        mc.sendAlertToJS("Gagal melakukan scan pada KTP, E-KTP tidak terbaca.  Pastikan anda telah mengisi PCID dan CONF dengan benar sesuai dengan device yang anda gunakan.  Pastikan juga kartu yang anda scan memiliki Chip E-KTP yang aktif");
                        return;
                    }
                }

                if (ret != 0) {
                    System.out.println("EKTP tidak ditemukan");
                    return;
                } else {
                    ktpStr = new String(ktpData);
                    int tempLen = Ektp.getRealLength(ktpData);
                    ktpStr = String.copyValueOf(ktpStr.toCharArray(), 0, tempLen);

                    ktpFpStr = new String(fpData);
                    int ktpFpStrSize = Ektp.getRealLength(fpData);
                    ktpFpStr = String.copyValueOf(ktpFpStr.toCharArray(), 0, ktpFpStrSize);
                    String result = showEktpDataOff(ktpStr).toString();
                    new Thread(() -> {
                        Platform.runLater(() -> {
                            String function = "scanKTP.scanningFingerPrint()";
                            webView.getEngine().executeScript(function);
                            log.info("execute command - " + function);
                        });
                    }).start();

                    System.out.println("do read fingerprint");
                    doReadFpOff(ktpStr, ktpFpStr);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                mc.sendAlertToJS("Gagal membaca KTP dan Sidik Jari, " + e.getMessage());
            }
        }).start();

    }

    public void doReadFpOff(String ktpStr, String ktpFpStr) {
        sleep(1000);

        if (Ektp.hContext == null) {
            System.out.println("Context Null");
            return;
        }

        if (ktpFpStr == null || ktpFpStr.length() == 0) {
            System.out.println("KTP tidak terscan");
            return;
        }

        System.out.println("Letakkan Jari Telunjuk");
        int ret = -1;
        IntByReference score = new IntByReference(-1);
        byte[] imageData = new byte[240001];

        boolean fingerCaptured = false;

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //code
                mc.sendAlertToJS("Sidik jari tidak ditemukan, Pastikan anda meletakkan jari telunjuk anda pada sensor sidik jari di alat scanner");
                Ektp.myInterface.EktpReleaseDevice(Ektp.hContext);
            }
        }, 30 * 1000);

//        setTimeout(() -> {
//            if (!fingerCaptured) {
//                mc.sendAlertToJS("Sidik jari tidak ditemukan, Pastikan anda meletakkan jari telunjuk anda pada sensor sidik jari di alat scanner");
//                Ektp.myInterface.EktpReleaseDevice(Ektp.hContext);
//                return;
//            }
//        }, 10 * 1000);
        ret = Ektp.myInterface.CaptureFingerPrint(imageData, score);

        if (ret != 0) {
            System.out.println("data tidak ada " + ret);
            return;
        }
        String imageStr = new String(imageData);

        IntByReference matched = new IntByReference(0);

        ret = Ektp.myInterface.MatchFingerPrint(imageStr, 240000, ktpFpStr, ktpFpStr.length(), score, matched);
        if (ret != 0) {
            System.out.println("data tidak ada");
            return;
        }
        int match = matched.getValue();
        if (match == 1) {
            timer.cancel();
            timer.purge();
            Platform.runLater(() -> {
                log.info("call setOfflineKTPResult");
                jsConnector.call("setOfflineKTPResult", showEktpDataOff(ktpStr));
            });
        } else {
            timer.cancel();
            timer.purge();
            Platform.runLater(() -> {
                mc.sendAlertToJS("Sidik jari tidak cocok, pastikan anda menggunakan jari telunjuk, pastikan panel sidik jari tidak berdebu atau tidak terhalangi benda lain, kemudian lakukan scan kembali");
            });
        }
    }

    private JSONObject showEktpDataOff(String ktpStr) {
        try {
            String strList[] = ktpStr.split(";");

            JSONObject jsEktp = new JSONObject();
            jsEktp.put("nik", strList[1]);
            jsEktp.put("alamat", strList[2]);
            jsEktp.put("rtrw", strList[3] + "/" + strList[4]);
            jsEktp.put("ttl", strList[5] + "," + strList[15]);
            jsEktp.put("kec", strList[6]);
            jsEktp.put("kel", strList[7]);
            jsEktp.put("kota", strList[8]);
            jsEktp.put("kelamin", strList[9]);
            jsEktp.put("agama", strList[11]);
            jsEktp.put("status", strList[12]);
            jsEktp.put("pekerjaan", strList[13]);
            jsEktp.put("nama", strList[14]);
            jsEktp.put("provinsi", strList[16]);
            jsEktp.put("goldar", strList[10]);
            jsEktp.put("warga", strList[20]);
            jsEktp.put("berlaku", strList[17]);
            jsEktp.put("photo", strList[0]);
            jsEktp.put("signature", strList[strList.length - 1]);

            return jsEktp;

        } catch (Exception ex) {
            ex.printStackTrace();
            mc.sendAlertToJS("Gagal menampilkan data KTP, " + ex.getMessage());
            return null;
        }
    }

    public String getDeviceId() {
        System.out.println("Get Device ID");
        if (Ektp.hContext == null) {

        }
        int ret = -1;
        byte[] uidData = new byte[20];
        ret = Ektp.myInterface.EktpReadPsamUid(Ektp.hContext, uidData);
        if (ret != 0) {

        }
        String psamUid = new String(uidData);
        int tempLen = Ektp.getRealLength(uidData);
        psamUid = String.copyValueOf(psamUid.toCharArray(), 0, tempLen);
        return psamUid;
    }

    public void checkNikDukcapil(String path, String nik, String user_id, String password, String IP_USER) {
        log.info("--checkNikDukcapil--");
        new Thread(() -> {
            try {
                JSONObject reqBody = new JSONObject()
                        .put("nik", nik)
                        .put("password", password)
                        .put("user_id", user_id)
                        .put("IP_USER", IP_USER)
                        .put("path", path);

                log.info("Req Body: " + reqBody.toString());

                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, reqBody.toString());
                Request request = new Request.Builder()
                        .url("http://117.54.12.171:61003/sb/dukcapil/forwarder")
                        .post(body)
                        .addHeader("Content-Type", "application/json")
                        .build();

                Response response = client.newCall(request).execute();

                String scanResult = response.body().string();

                log.info("response: " + scanResult);

                Platform.runLater(() -> {
                    log.info("call setOfflineKTPResult");
                    scanConnector.call("setOnlineScanResult", scanResult);
                });

//                mc.sendAlertToJS(response.body().string());
            } catch (Exception e) {
                log.error(new String(), e);
                mc.sendAlertToJS("Gagal melakukan pengecekan NIK Dukcapil, " + e.getMessage());
            }

        }).start();
    }

    public void saveOnlineScanConfig(String url, String userid, String password) {
        log.info("--saveOnlineScanConfig--");
        new Thread(() -> {
            KtpOnline.setUrl(url);
            KtpOnline.setUserid(userid);
            KtpOnline.setPassword(password);
            if (KtpOnline.writeFile()) {
                mc.sendAlertToJS("Konfigurasi Cek KTP Online berhasi disimpan");
            } else {
                mc.sendAlertToJS("Gagal menyimpan konfigurasi Cek KTP Online");
            }
        }).start();
    }

    public void getOnlineKtpData() {
        new Thread(() -> {
            KtpOnline.init();
            if (KtpOnline.getUserid() != null) {
                Platform.runLater(() -> {
                    JSONObject data = new JSONObject()
                            .put("userid", KtpOnline.getUserid())
                            .put("password", KtpOnline.getPassword())
                            .put("url", KtpOnline.getUrl());
                    log.info("call setOnlineKtpConfig");
                    scanConnector.call("setOnlineKtpConfig", data.toString());
                });
            }
        }).start();
    }

    public String readFile(String filelocation, String filename) {
        try {
            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            myReader.close();
            return data;
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
//            sendAlertToJS("Gagal membaca file, " + e.getMessage());
            return null;
        }
    }

    public boolean writeFile(String filelocation, String filename, String data) {
        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (Exception e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            mc.sendAlertToJS("Gagal menulis data, " + e.getMessage());
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            log.info("write DATA -");
            log.info(data);

            write.write(data);
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            mc.sendAlertToJS("Gagal menyimpan data, " + e.getMessage());
            return false;
        }
    }

    public void setTimeout(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}
