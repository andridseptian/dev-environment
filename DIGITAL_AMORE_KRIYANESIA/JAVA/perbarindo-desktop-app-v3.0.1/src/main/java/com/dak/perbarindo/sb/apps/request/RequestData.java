/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.request;

import com.dak.perbarindo.sb.apps.data.Device;
import com.dak.perbarindo.sb.apps.data.Lembaga;
import com.dak.perbarindo.sb.apps.data.Terminal;
import com.dak.perbarindo.sb.apps.data.UserOperator;
import com.dak.perbarindo.sb.apps.ktp.Ektp;
import com.dak.perbarindo.sb.apps.ktp.EktpInterface;
import com.dak.perbarindo.sb.apps.request.service.UserReq;
import com.dak.perbarindo.sb.apps.utility.Constants;
import com.dak.perbarindo.sb.apps.utility.Tools;
import com.dak.perbarindo.sb.apps.utility.TripleDES;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class RequestData {

    private static final Logger log = LogManager.getLogger(RequestData.class);

    public static JSONObject send(String path, String body) {

        log.info("send PATH -");
        log.info(path);
        log.info(body);

        switch (path) {
            case Constants.path.name.GET_VPN:
//                return getVpn(Constants.path.url.GET_VPN);
                return getVpn("http://117.54.12.173:9980/vpn/findBy_BankId_macaddress");
            case Constants.path.name.LOGIN:
                return login(Constants.path.url.LOGIN, body);
            case Constants.path.name.LOGOUT:
                return logout(Constants.path.url.LOGOUT, body);
            case Constants.path.name.REGISTRATION:
                return registration(Constants.path.url.REGISTRATION, body);
            case Constants.path.name.ACTIVATE_USER:
                return activate_user(Constants.path.url.ACTIVATE_USER, body);
            case Constants.path.name.DEACTIVATE_USER:
                return deactivate_user(Constants.path.url.DEACTIVATE_USER, body);
            case Constants.path.name.GET_USER_LIST:
                return get_all_user(Constants.path.url.GET_USER_LIST, body);
            case Constants.path.name.ACTIVATE_TERMINAL:
                return activate_terminal(Constants.path.url.ACTIVATE_TERMINAL, body);
            case Constants.path.name.SET_DEVICE_ID:
                return set_device(Constants.path.url.SET_DEVICE_ID, body);
            case Constants.path.name.REQUEST_RESET_PASSWORD_BY_USER:
                return request_reset_password_user(Constants.path.url.REQUEST_RESET_PASSWORD_BY_USER, body);
            case Constants.path.name.RESET_PASSWORD_BY_USER:
                return reset_password_user(Constants.path.url.RESET_PASSWORD_BY_USER, body);
            case Constants.path.name.BILLING_NOTIFICATION:
                return billing_notification(Constants.path.url.BILLING_NOTIFICATION, body);
            case Constants.path.name.GET_SERIAL_NUMBER:
                return get_serial_number(Constants.path.url.GET_SERIAL_NUMBER);
            case Constants.path.name.GENERATE_SERIAL_NUMBER:
                return generate_serial_number(Constants.path.url.GENERATE_SERIAL_NUMBER);
            case Constants.path.name.RESET_SERIAL_NUMBER:
                return reset_serial_number(Constants.path.url.RESET_SERIAL_NUMBER, body);
            case Constants.path.name.CHECK_ADMIN_AVAILABILITY:
                return UserReq.check_admin(Constants.path.url.CHECK_ADMIN_AVAILABILITY, body);
            default:
                return null;
        }

    }

    private static JSONObject getVpn(String url) {
        JSONObject json = new JSONObject();
        json.put(Constants.INPUT_BANKID, Lembaga.getBankID());
        json.put(Constants.INPUT_MACADDRESS, Terminal.getMacaddress());
        json.put("bank_id", Lembaga.getBankID());
        json.put("mac_address", Terminal.getMacaddress());
        json.put(Constants.INPUT_JUMLAH, 1);
//        return SendRequest.postPublic(url, json.toString());
        return SendRequest.postPublicWOEncrypt(url, json.toString());
    }

    private static JSONObject login(String url, String body) {
        try {
            
//            return new JSONObject().put("success", "loginIn.response()");

            // desc: get Input Body
            /* <editor-fold> */
            JSONObject inBody = new JSONObject(body);
            String username = inBody.getString("username");
            String password = inBody.getString("password");
            /* END </editor-fold> */
            
            Terminal.init();

            Lembaga.readFile();
            if (Terminal.getMacaddress() != null) {
                if (Lembaga.getBankID() != null) {

                    // body request
                    /* <editor-fold> */
                    TripleDES tdes = new TripleDES(Constants.TDES_KEY_APP);
                    JSONObject json = new JSONObject();
                    json.put("user", new JSONObject()
                            .put("username", tdes.encrypt(username))
                            .put("password", tdes.encrypt(password)));
                    json.put("terminal", Terminal.getTerminalData());
                    /* </editor-fold> */

                    // desc: Send request
                    /* <editor-fold> */
                    JSONObject response = SendRequest.post(url, json.toString());
                    if (response != null) {
                        if (response.getString("rc").equals("00") || response.getString("rc").equals("71")) {
                            try {
                                JSONObject data = response.getJSONObject("data");

                                UserOperator.setUsername(username);
                                UserOperator.setPassword(password);
                                UserOperator.setBPRName(data.getString("bank_name"));
                                UserOperator.setFullname(data.getString("nama"));
                                UserOperator.setEmail(data.getString("email"));
                                UserOperator.setNIK(data.getString("nik"));
                                UserOperator.setPhoneNumber(data.getString("no_hp"));
                                UserOperator.setExpiredPassword(data.getString("expired_password"));
                                UserOperator.setPrivilege(String.valueOf(data.getInt("privilage")));
                                UserOperator.writeFile();

                                String pattern = "yyyy-MM-dd";
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                                LocalDateTime currentDate = new Date().toInstant()
                                        .atZone(ZoneId.systemDefault())
                                        .toLocalDateTime();
                                LocalDateTime expiredDate = simpleDateFormat.parse(data.getString("expired_password")).toInstant()
                                        .atZone(ZoneId.systemDefault())
                                        .toLocalDateTime();
                                long expiredPasswordBetween = ChronoUnit.DAYS.between(currentDate, expiredDate);
                                if (expiredPasswordBetween > 0) {
                                    return new JSONObject().put("success", "loginIn.response()");
                                } else {
                                    return new JSONObject().put("success", "resetPassword.reset()");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                return new JSONObject().put("fail", "Gagal mengenali data dari server, " + e.getMessage());
                            }

                        } else {
                            return response;
                        }
                    } else {
                        return new JSONObject().put("fail", "Wah, aplikasi tidak memperoleh respon dari server, tunggu beberapa saat kemudian silahkan coba lagi");
                    }
                    /* END </editor-fold> */

                } else {
                    return new JSONObject().put("fail", "Wah, aplikasi tidak dapat menemukan bank id komputer anda, pastikan anda sudah mengisi form pengaturan");
                }
            } else {
                return new JSONObject().put("fail", "Wah, aplikasi tidak dapat menemukan mac address komputer anda, pastikan anda tidak sedang terhubung dengan jaringan private lain");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject().put("fail", e.getMessage());
        }
    }

    private static JSONObject logout(String url, String body) {
        try {
            // desc: body request
            // <editor-fold> */
            /*
            {
                "terminal": {
                        "mac": "0A-00-27-00-00-05,,",
                        "proc_name": "Intel(R) Core(TM)2 Duo CPU T6500@ 2.10GHz",
                        "proc_id": "BFEBFBFF0001067A",
                        "uuid": "FC689911-D2BE-F7E6-7109-00235AD95FC6",
                        "bank_id": 2499
                },
                "user": {
                        "username": "ebWIN7eTin91yzmMwSGPdg=="
                }
            }
             */
            // END </editor-fold> */

            TripleDES tdes = new TripleDES(Constants.TDES_KEY_APP);

            JSONObject user = new JSONObject()
                    .put("username",
                            tdes.encrypt(new JSONObject(body)
                                    .getString("username"))
                    );
            JSONObject jsReqBody = new JSONObject()
                    .put("terminal", Terminal.getTerminalData())
                    .put("user", user);

            JSONObject response = SendRequest.post(url, jsReqBody.toString());
            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    return res.success("console.log('logout')");
                } else {
                    return res.fail(response.getString("rm"));
                }
            } else {
                return res.fail("Gagal, response tidak ada atau tidak dikenali");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return res.fail("Gagal melakukan logout", e.getMessage());
        }
    }

    private static JSONObject billing_notification(String url, String body) {
        try {
            JSONObject jsReqBody = new JSONObject().put("bankid", Lembaga.getBankID());

            JSONObject response = SendRequest.post(url, jsReqBody.toString());
            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    return res.success("setAlert", response.getString("rm"));
                } else {
                    return res.fail(response.getString("rm"));
                }
            } else {
                log.info("tidak ada tagihan");
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("tidak ada response dari aplikasi");
            return null;
        }
    }

    private static JSONObject registration(String url, String body) {

        // desc: body example
        // <editor-fold> */
        /*
            {
                "terminal": {
                        "mac": "0A-00-27-00-00-05",
                        "proc_name": "Intel(R) Core(TM)2 Duo CPU T6500@ 2.10GHz",
                        "proc_id": "BFEBFBFF0001067A",
                        "uuid": "FC689911-D2BE-F7E6-7109-00235AD95FC6",
                        "bank_id": 2499
                },
                "user": {
                        "username": "5g6SVEhbzSh+AmgRMkxR5w==.",
                        "password": "8JtbQsr40FkHkUuswPsgBg==",
                        "email": "nadafhraaak@gmail.com",
                        "nik": "mwqCKIHXYc/Q7PsBC+hMinXLOYzBIY92",
                        "phone_number": "083161041484",
                        "fullname": "Morten Gantteng",
                        "privillage": 1
                }
            }
         */
        // END </editor-fold> */
        // desc: view request body
        // <editor-fold> */
        /*
            username: username, 
            fullname: fullname, 
            email: email, 
            nik: nik,
            phonenumber: phonenumber, 
            password: password,
         */
        // END </editor-fold> */
        try {

            JSONObject viewBody = new JSONObject(body);

            // desc: json req body
            // <editor-fold>
            /*
            "user": {
		"username": "5g6SVEhbzSh+AmgRMkxR5w==.",
		"password": "5g6SVEhbzSh+AmgRMkxR5w==.",
		"email": "nadafhraaa@gmail.com.",
		"nik": "UV4X5SibSg1g+VfrtYcyo3XLOYzBIY92.",
		"phone_number": "083161041484.",
		"privillage": "admin"
                }
             */
            // END </editor-fold>
            TripleDES tdes = new TripleDES(Constants.TDES_KEY_APP);

            JSONObject user = new JSONObject()
                    .put("username", tdes.encrypt(viewBody.getString("username")))
                    .put("password", tdes.encrypt(viewBody.getString("password")))
                    .put("fullname", viewBody.getString("fullname"))
                    .put("email", viewBody.getString("email"))
                    .put("nik", tdes.encrypt(viewBody.getString("nik")))
                    .put("phone_number", viewBody.getString("phonenumber"))
                    .put("privillage", viewBody.get("privillage"));

            Lembaga.init();
            Terminal.init();
            JSONObject jsReqBody = new JSONObject()
                    .put("user", user)
                    .put("terminal", Terminal.getTerminalData());

            try {
                JSONObject response = SendRequest.post(url, jsReqBody.toString());
                if (response != null && response.has("rc")) {
                    if (response.getString("rc").equals("00")) {
                        try {
//                            return res.success("setAlert", response.getString("rm"));
                            return res.success("regis.onSucceed", "User berhasil diregistrasikan, "
                                    + "jika anda mendaftar sebagai admin silahkan login, "
                                    + "jika anda mendaftar sebagai user operator, "
                                    + "silahkan meminta admin di bpr anda untuk mengaktifkan akun anda, "
                                    + "kemudian lakukan login");
                        } catch (Exception e) {
                            e.printStackTrace();
                            return res.fail("Gagal mengenali data dari server", e.getMessage());
                        }
                    } else {
                        return response;
                    }
                } else {
                    return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return res.fail("Gagal Melakukan Registrasi", e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();

            return res.fail("Gagal Melakukan Registrasi", e.getMessage());
        }
    }

    private static JSONObject activate_terminal(String url, String body) {

        return null;
    }

    private static JSONObject get_all_user(String url, String body) {
        try {
            JSONObject reqBody = new JSONObject()
                    .put("bank_id", Integer.parseInt(Lembaga.getBankID()))
                    .put("terminal", Terminal.getTerminalData());

            JSONObject response = SendRequest.post(url, reqBody.toString());
            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {

                        if (response.has("data")) {

                            return res.success("manuser.getUserList", response.getJSONArray("data").toString());

                        } else {
                            return res.success("setAlert", response.getString("data tidak ditemukan"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static JSONObject activate_user(String url, String userid) {
        try {
            JSONObject reqBody = new JSONObject()
                    .put("user_id", Integer.parseInt(userid))
                    .put("terminal", Terminal.getTerminalData());

            JSONObject response = SendRequest.post(url, reqBody.toString());
            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("setAlert", "User Diaktifkan");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static JSONObject deactivate_user(String url, String userid) {
        try {
            JSONObject reqBody = new JSONObject()
                    .put("user_id", Integer.parseInt(userid))
                    .put("terminal", Terminal.getTerminalData());

            JSONObject response = SendRequest.post(url, reqBody.toString());
            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("setAlert", "User Diblokir");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;
    }

    private static JSONObject set_device(String url, String body) {
        try {
            String filelocation = System.getProperty("user.dir");
            String filename = "\\conf\\device.conf";

            log.info("Get Device ID");
            Ektp.ExtractJar();
            Ektp.myInterface = EktpInterface.INSTANCE;
            if (Ektp.hContext == null) {
            }
            int ret = -1;
            byte[] uidData = new byte[20];
            ret = Ektp.myInterface.EktpReadPsamUid(Ektp.hContext, uidData);
            if (ret != 0) {

            }

            String psamUid = new String(uidData);
            int tempLen = Ektp.getRealLength(uidData);
            psamUid = String.copyValueOf(psamUid.toCharArray(), 0, tempLen);

            JSONObject deviceconf = new JSONObject(body);

            if (deviceconf != null) {
                deviceconf.put("bank_id", Lembaga.getBankID());
                deviceconf.put("device_id", psamUid);

                Device.setBankid(Lembaga.getBankID());
                Device.setDeviceId(psamUid);
                Device.setPcid(deviceconf.getString("pcid"));
                Device.setConf(deviceconf.getString("conf"));

                if (Device.writeFile()) {
                    return res.success("setAlert", "Device ID tersimpan");
                } else {
                    return res.success("setAlert", "Gagal menyimpan data Device ID");
                }
            } else {
                return res.fail("Konfigurasi alat tidak ditemukan, pastikan sudah melakukan siknronisasi alat pemindai");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return res.fail("Gagal menyimpan konfigurasi device", e.getMessage());
        }
    }

    public static JSONObject request_reset_password_user(String url, String body) {
        try {
            JSONObject reqBody = new JSONObject().put("nik", body);
            JSONObject response = SendRequest.post(url, reqBody.toString());

            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("setAlert", response.getString("rm"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }
        } catch (Exception e) {
            log.error(e);
            return res.fail("Gagal melakukan request reset password anda", e.getMessage());
        }
    }

    public static JSONObject reset_password_user(String url, String body) {
        try {

            log.info("reset_password_user: " + body);

            TripleDES tripleDES = new TripleDES("APLIKASISHARINGBANDWIDTHENKRIPSI");

            JSONObject data = new JSONObject(body);

            String old_password = tripleDES.encrypt(data.getString("old_password"));
            String new_password = tripleDES.encrypt(data.getString("new_password"));
            String username = tripleDES.encrypt(UserOperator.getUsername());

            JSONObject reqBody = new JSONObject()
                    .put("terminal", Terminal.getTerminalData())
                    .put("user", new JSONObject()
                            .put("username", username)
                            .put("old_password", old_password)
                            .put("new_password", new_password)
                    );

            JSONObject response = SendRequest.post(url, reqBody.toString());

            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("setAlert", response.getString("rm"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }
        } catch (Exception e) {
            log.error(e);
            return res.fail("Gagal melakukan reset password anda", e.getMessage());
        }
    }

    public static JSONObject get_serial_number(String url) {
        try {
            log.info("get serial number");

            JSONObject reqBody = new JSONObject()
                    .put("bank_id", Lembaga.getBankID());

            JSONObject response = SendRequest.post(url, reqBody.toString());

            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("manterminal.setTerminalList", response.getJSONArray("data").toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }
        } catch (Exception e) {
            log.error(e);
            return res.fail("Gagal mendapatkan data serial number", e.getMessage());
        }
    }

    public static JSONObject generate_serial_number(String url) {
        try {
            log.info("generate serial number");

            JSONObject reqBody = new JSONObject()
                    .put("bank_id", Lembaga.getBankID());

            JSONObject response = SendRequest.post(url, reqBody.toString());

            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("manterminal.refreshGenerateSerial()");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }
        } catch (Exception e) {
            log.error(e);
            return res.fail("Gagal mendapatkan data serial number", e.getMessage());
        }
    }

    public static JSONObject reset_serial_number(String url, String body) {
        try {
            log.info("reset serial number");

            JSONObject reqBody = new JSONObject()
                    .put("serial_number", body);

            JSONObject response = SendRequest.post(url, reqBody.toString());

            if (response != null && response.has("rc")) {
                if (response.getString("rc").equals("00")) {
                    try {
                        return res.success("setAlert", response.getString("rm").toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return res.fail("Gagal mengenali data dari server", e.getMessage());
                    }
                } else {
                    return response;
                }
            } else {
                return res.fail("Tidak mendapatkan respon atau gagal mengenali parameter respon dari server");
            }
        } catch (Exception e) {
            log.error(e);
            return res.fail("Gagal mendapatkan data serial number", e.getMessage());
        }
    }

    public static class res {

        public static final JSONObject fail(String message) {
            String log = " [" + Tools.crLogId() + "]";
            return new JSONObject().put("fail", message);
        }

        public static final JSONObject fail(String message, String caused) {
            String log = " [" + Tools.crLogId() + "]";
            return new JSONObject().put("fail", message + ", " + caused + log);
        }

        public static final JSONObject success(String function) {
            String log = " [" + Tools.crLogId() + "]";
            return new JSONObject().put("success", function);
        }

        public static final JSONObject success(String function, String value) {
            String log = " [" + Tools.crLogId() + "]";

            function = function + "('" + value + "')";
            return new JSONObject().put("success", function);
        }
        
        public static final JSONObject execute(String parent, String function, JSONObject data) {
            return new JSONObject().put("execute", function).put("data", data).put("parent", parent);
        }
    }

}
