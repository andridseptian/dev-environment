/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Download {

    private static final Logger log = LogManager.getLogger(Download.class);

    private HttpURLConnection connection;
    
    private int percentage = 0;

    public boolean get(String FILE_NAME, String FILE_URL) {
        try {
            log.info("--> downloading file");
            log.info(FILE_URL);
            log.info(FILE_NAME);

            URL url = new URL(FILE_URL);
            connection = (HttpURLConnection) url.openConnection();
            int filesize = connection.getContentLength();
            float totalDataRead = 0;

            try (BufferedInputStream in = new BufferedInputStream(connection.getInputStream())) {
                FileOutputStream fos = new FileOutputStream(FILE_NAME);
                try (java.io.BufferedOutputStream bout = new BufferedOutputStream(fos, 1024)) {
                    byte[] data = new byte[1024];
                    int i;
                    log.info("processing download");
                    while ((i = in.read(data, 0, 1024)) >= 0) {
                        totalDataRead = totalDataRead + i;
                        bout.write(data, 0, i);
                        float Percent = (totalDataRead * 100) / filesize;
                        percentage = (int) Percent;
//                        log.info(percentage);
                    }
                    percentage = 100;
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(FILE_URL);
                    log.info("failed to download file");
                    return false;
//                    MainController.sendAlertExit("Terjadi kesahalah saat mengunduh file, " + e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            return false;
//            MainController.sendAlertExit("Terjadi kesahalah saat memulai mengunduh file, " + e.getMessage());
        }
    }

    public int getPercentage() {
        return percentage;
    }
    
}
