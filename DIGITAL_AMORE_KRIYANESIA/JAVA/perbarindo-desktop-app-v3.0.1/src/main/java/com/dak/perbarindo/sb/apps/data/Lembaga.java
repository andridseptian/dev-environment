/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.apps.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class Lembaga {

    private static final Logger log = LogManager.getLogger(Lembaga.class);

    private static String filelocation = System.getProperty("user.dir") + "\\config";
    private static String filename = "\\lembaga.conf";

    private static String BankID = null;
    private static String LembagaID = null;
    private static String LembagaName = null;
    private static String Token = null;

    public static void init() {
        readFile();
    }

    public static String getBankID() {
        readFile();
        return BankID;
    }

    public static void setBankID(String BankID) {
        Lembaga.BankID = BankID;
    }

    public static String getLembagaID() {
        return LembagaID;
    }

    public static void setLembagaID(String LembagaID) {
        Lembaga.LembagaID = LembagaID;
    }

    public static String getLembagaName() {
        return LembagaName;
    }

    public static void setLembagaName(String LembagaName) {
        Lembaga.LembagaName = LembagaName;
    }

    public static String getToken() {
        return Token;
    }

    public static void setToken(String Token) {
        Lembaga.Token = Token;
    }

    public static String readFile() {
        try {
            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            JSONObject dataIn = new JSONObject(data);
            Lembaga.setBankID(dataIn.getString("bankid"));
            Lembaga.setLembagaID(dataIn.getString("lembagaid"));
            Lembaga.setLembagaName(dataIn.getString("lembaganame"));
            Lembaga.setToken(dataIn.getString("token"));
            myReader.close();
            return data;
        } catch (FileNotFoundException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    public static boolean writeFile() {

        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }

            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            JSONObject data = new JSONObject();
            data.put("bankid", BankID)
                    .put("lembagaid", LembagaID)
                    .put("lembaganame", LembagaName)
                    .put("token", Token);

            log.info("write CONFIG -");
            log.info(data.toString());

            write.write(data.toString());
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }

}
