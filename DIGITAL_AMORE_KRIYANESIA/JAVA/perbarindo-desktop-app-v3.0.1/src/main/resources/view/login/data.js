class data {
    static setTerminal(data) {

    }

    static setMacAddress(data) {
        try {
            var parsed = JSON.parse(data);
            $("#activate-macaddress").val(parsed.macaddress);
            $("#activate-procid").val(parsed.procid);

            $("#config-macaddress").val(parsed.macaddress);
            $("#config-procid").val(parsed.procid);

            $("#logo-macaddress").html('ready<br>' + parsed.macaddress);
            $("#logo-version").html(parsed.buildversion);
            $("#logo-build").html(parsed.buildnumber);

            $("#login-submit").prop("disabled",false);
            $("#regis-submit").prop("disabled",false);

        } catch (error) {
            alert(error);
        }

    }

    static setLoginData(data) {
        try {
            var parsed = JSON.parse(data);
            $("#login-username").val(parsed.username);
            // $("#login-password").val(parsed.password);
        } catch (error) {
            alert(error);
        }
    }

    static setLembaga(data) {
        try {
            var parsed = JSON.parse(data);
            $("#config-bankid").val(parsed.bankid);
            $("#config-lembagaid").val(parsed.lembagaid);
            $("#config-lembaganame").val(parsed.lembaganame);
            $("#config-token").val(parsed.token);
            $("#activate-bankid").val(parsed.bankid);
        } catch (error) {
            alert(error);
        }
    }
}