class checkUpdate {
    static checkForUpdate() {
        downloadConnector.checkUpdate();
    }

    static waitingForUpdate() {
        setLoading('sedang memperbarui launcher aplikasi')
    }

    static updateCompleted() {
        setAlert('Update selesai, silahkan gunakan aplikasi kembali');
        setTimeout(() => {
            $("#modal-alert").modal('hide');
        }, 5000)
    }
}