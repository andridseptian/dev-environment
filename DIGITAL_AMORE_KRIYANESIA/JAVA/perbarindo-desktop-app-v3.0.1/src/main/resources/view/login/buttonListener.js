class buttonListener {
    static init() {
        $("#a-forgotpassword").click(function () {
            $("#modal-requestreset").modal();

            $("#requestreset-submit").click(() => {
                if ($("#requestreset-input-nik").val() != "") {
                    var nik = $("#requestreset-input-nik").val();
                    javaConnector.sendRequestFromJS("requestresetpasswordbyuser", nik);
                    setLoading('Sedang memeriksa data anda');
                } else {
                    setAlert('Form NIK tidak boleh kosong');
                }
            });
            // $("#modal-reset").modal();
        });

        $("#a-registration").click(function () {
            $("#pane-login").hide();
            $("#pane-registration").show("fast", "swing");
        });

        $("#a-login").click(function () {
            $("#pane-registration").hide();
            $("#pane-login").show("fast", "swing");
        });

        $("#a-config").click(function () {
            config.showConfig();
        });

        $("#a-activate").click(function () {
            activate.showActivate();
        });

        $("#config-save").click(function () {
            config.saveConfig();
        });

        $("#activate-save").click(function () {
            activate.saveActivate();
        });

        $("#login-submit").click(function () {
            $("#login-submit").prop("disabled", true)
            loginIn.submit();
            setTimeout(() => {
                $("#login-submit").prop("disabled", false)
            }, 5000);
        });

        $("#regis-submit").click(function () {
            $("#regis-submit").prop("disabled", true)
            regis.submit();
            setTimeout(() => {
                $("#regis-submit").prop("disabled", false)
            }, 5000);
        });

        $('#login-visible').click(function () {
            var x = document.getElementById("login-password");
            if ($(this).prop("checked") == true) {
                console.log("Checkbox is checked.");
                x.type = "text";
            }
            else if ($(this).prop("checked") == false) {
                console.log("Checkbox is unchecked.");
                x.type = "password";
            }
        });
    }
}