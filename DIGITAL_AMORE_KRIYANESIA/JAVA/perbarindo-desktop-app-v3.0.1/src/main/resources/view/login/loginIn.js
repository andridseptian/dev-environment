class loginIn {
    static submit() {
        try {
            if (
                $("#login-username").val() != "" &&
                $("#login-password").val() != ""
            ) {
                setLoading('Sedang memeriksa akun anda');
                var indata = {
                    username: $("#login-username").val(),
                    password: $("#login-password").val()
                }
                javaConnector.sendRequestFromJS('login', JSON.stringify(indata));
            } else {
                setTimeout(() => {
                    setAlert('Form login tidak boleh ada yang kosong.');
                }, 1000);
            }
        } catch (error) {
            alert(error);
        }
    }

    static response() {
        try {
            window.open('dashboard.html');
            $("#modal-loading").modal('hide');
        } catch (error) {
            alert(error);
        }
    }
}