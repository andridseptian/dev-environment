class regis {
    static submit() {
        try {
            if (
                $('#regis-fullname').val() != "" &&
                $('#regis-username').val() != "" &&
                $('#regis-nik').val() != "" &&
                $('#regis-email').val() != "" &&
                $('#regis-phonenumber').val() != "" &&
                $('#regis-password').val() != "" &&
                $('#regis-confpassword').val() != ""
            ) {
                setLoading("sedang memeriksa data anda");

                var password = $("#regis-password").val();
                var confpassword = $("#regis-confpassword").val();

                if (password == confpassword) {
                    javaConnector.logFromJS("password confirmation confirm");

                    var reqBody = JSON.stringify({
                        bank_id: $('#config-bankid').val()
                    })

                    javaConnector.logFromJS("req body: " + reqBody);

                    userConnector.sendRequestFromJS('checkadmin', reqBody);
                } else {
                    setAlert('Konfirmasi password anda belum sesuai, coba sekali lagi');
                }
            } else {
                var dummy = $('#regis-username').val()
                if (dummy.startsWith("xbuild")) {
                    alert('set dummy data ' + dummy)
                    $('#regis-fullname').val(`Dummy ${dummy}`)
                    $('#regis-nik').val('74710418099500' + dummy.replace('xbuild', ''))
                    $('#regis-email').val(dummy + '@mail.com')
                    $('#regis-phonenumber').val('0821967400' + dummy.replace('xbuild', ''))
                    $('#regis-password').val('Qawsed123!')
                    $('#regis-confpassword').val('Qawsed123!')
                } else {
                    setAlert('Form data harus diisi semua, tidak boleh ada yg kosong');
                }
            }
        } catch (error) {
            alert(error);
        }
    }

    static response() {
        $("#modal-loading").modal('hide');
        $("#pane-registration").hide();
        $("#pane-login").show("fast", "swing");
    }

    static onSucceed(message) {
        setAlert(message);
        $("#modal-loading").modal('hide');
        $("#pane-registration").hide();
        $("#pane-login").show("fast", "swing");
    }
}

var regisConnector = {
    registeringUser: function (res) {
        try {
            var resBody = res;
            var data = {
                username: $('#regis-username').val(),
                fullname: $('#regis-fullname').val(),
                email: $('#regis-email').val(),
                nik: $('#regis-nik').val(),
                phonenumber: $('#regis-phonenumber').val(),
                password: $('#regis-password').val()
            };
            if (resBody.rc == "04") {
                $('#modal-loading').modal('hide');
                $('#modal-adminconfirm').modal();

                $('#adminconfirm-yes').click(() => {
                    setLoading("mendaftarkan user");
                    data.privillage = 1;
                    userConnector.sendRequestFromJS('registration', JSON.stringify(data));
                });

                $('#adminconfirm-no').click(() => {
                    setLoading("mendaftarkan user");
                    data.privillage = 0;
                    userConnector.sendRequestFromJS('registration', JSON.stringify(data));
                });
            } else {
                data.privillage = 0;
                userConnector.sendRequestFromJS('registration', JSON.stringify(data));
            }
        } catch (error) {
            alert(error);
        }
    }
}

function getRegisConnector() {
    return regisConnector;
};