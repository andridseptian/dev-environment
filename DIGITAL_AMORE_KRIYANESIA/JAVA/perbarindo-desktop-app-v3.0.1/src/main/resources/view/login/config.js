class config {
    static showConfig() {
        $("#modal-config").modal('toggle');
    }

    static saveConfig() {
        try {
            $("#modal-config").modal('hide');

            if ($("#config-bankid").val() != "") {
                var send = {
                    bankid: $("#config-bankid").val(),
                    lembagaid: $("#config-lembagaid").val(),
                    lembaganame: $("#config-lembaganame").val(),
                    token: $("#config-token").val()
                }
                javaConnector.saveLembagaData(JSON.stringify(send));
            } else {
                setAlert("Bank ID harus diisi dengan angka dan tidak boleh terdapat spasi")
            }
        } catch (error) {
            alert(error);
        }
    }
}