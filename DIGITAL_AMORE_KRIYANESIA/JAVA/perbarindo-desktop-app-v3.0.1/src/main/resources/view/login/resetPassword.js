class resetPassword {
    static reset() {
        try {
            $("#modal-loading").modal('toggle');
            $("#modal-reset").modal();
            $("#reset-submit").click(() => {
                if (
                    $("#reset-input-oldpassword").val() != "" &&
                    $("#reset-input-newpassword").val() != "" &&
                    $("#reset-input-confpassword").val() != ""
                ) {
                    if ($("#reset-input-newpassword").val() == $("#reset-input-confpassword").val()) {
                        setLoading('Sedang memeriksa data reset password anda');
                        var data = JSON.stringify({
                            old_password: $("#reset-input-oldpassword").val(),
                            new_password: $("#reset-input-newpassword").val()
                        });
                        javaConnector.sendRequestFromJS('resetpasswordbyuser', data);
                    } else {
                        setAlert('Konfirmasi password anda salah');
                    }
                } else {
                    setAlert('form tidak boleh ada yg kosong')
                }
            });
        } catch (error) {
            alert(error);
        }
    }
}