
// GLOBAL VARIABLE
var returnDataContainer = [];
var globalBankId;
var globalBankName;
var globalOperatorUserName;
var globalOperatorFullname;
var offlineKTPData = "";
var jsKTP = "";
var counterTick = 0;

////////////////////////////////////////////////////////////////

$(document).ready(function () {

    $('#menu-panes').hide();
    $('#side-nav').hide();
    setLoading('Memuat Komponen');
    setTimeout(() => {
        try {
            $("#modal-loading").modal('hide');
            data.getInitialData();
            returndata.getdata();
            javaConnector.getReturnData();

            returndata.setReturnDataRange();

            $('#return-table').DataTable();

            try {
                $("#disclaimer-input-tanggal").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0"
                });
            } catch (error) {
                alert(error)
            }


            $("#page-dashboard").show();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $('#onlinecek-container').hide();


            $("#disclaimer-input-alamat").prop('disabled', true);
            $("#disclaimer-input-rtrw").prop('disabled', true);
            $("#disclaimer-input-keldesa").prop('disabled', true);
            $("#disclaimer-input-kec").prop('disabled', true);
            $("#disclaimer-input-kerja").prop('disabled', true);
            $("#disclaimer-input-kawin").prop('disabled', true);
            $("#disclaimer-input-warga").prop('disabled', true);


            buttonListener.menuButtonListener();
            buttonListener.portalButtonListener();
            buttonListener.returnDataButtonListener();
            buttonListener.connectionButtonListener();
            buttonListener.disclaimerButtonListener();
            buttonListener.OfflineKTPButtonListener();
            $('#side-nav').show(500);
            $('#menu-panes').show(500, 'swing');


            $(window).resize(function () {
                if ($(window).width() <= 800) {
                    $('#side-nav').hide(500, 'swing');
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $('#side-nav').show(500, 'swing');
                    $("#menu-panes").css("padding-left", "350px");
                }
            });

            if ($(window).width() < 800) {
                setTimeout(() => {
                    $('#side-nav').hide(500, 'swing');
                    $("#menu-panes").css("padding-left", "50px");
                }, 1000);
            }

            //#region 
            // setTimeout(() => {
            //     var info_dt = '';
            //     function createDatatable() {
            //         erTable_dt_invoice = $("#dt_invoice").DataTable({
            //             processing: true,
            //             serverSide: true,
            //             searchDelay: 500,
            //             autoWidth: false,
            //             columns: [{ "orderable": false, "className": "text-center" }, { "orderable": true }, { "orderable": true }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }],
            //             order: [[4, 'desc']],
            //             lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            //             dom: "<'row'<'col-sm-6'l><'col-sm-6'B>> <'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            //             buttons: [
            //                 {
            //                     extend: 'print',
            //                     text: '<i class="fa fa-print"></i>',
            //                     titleAttr: 'Print',
            //                     tag: "button",
            //                     className: ""
            //                 },
            //                 {
            //                     extend: 'copyHtml5',
            //                     text: '<i class="fa fa-files-o"></i>',
            //                     titleAttr: 'Copy',
            //                     tag: "button",
            //                     className: ""
            //                 },
            //                 {
            //                     extend: 'excelHtml5',
            //                     text: '<i class="fa fa-file-excel-o"></i>',
            //                     titleAttr: 'Excel',
            //                     tag: "button",
            //                     className: ""
            //                 },
            //                 {
            //                     extend: 'csvHtml5',
            //                     text: '<i class="fa fa-file-text-o"></i>',
            //                     titleAttr: 'CSV',
            //                     tag: "button",
            //                     className: ""
            //                 },
            //                 {
            //                     extend: 'pdfHtml5',
            //                     text: '<i class="fa fa-file-pdf-o"></i>',
            //                     titleAttr: 'PDF',
            //                     tag: "button",
            //                     className: ""
            //                 },
            //             ],

            //             ajax: {
            //                 url: `https://perbarindo.org/index.php/services/invoice/invoice_bypass?id_bpr=${globalBankId}&key=61d90eb6c09b48cf5802b823eac08980&tab=sb&filter_bulan=all&filter_tahun=all&filter_status=all`,
            //                 type: "POST",
            //                 data: function (d, dt) {
            //                     d.dt_name = "dt_invoice";
            //                 }
            //             }
            //         });
            //         info_dt = erTable_dt_invoice.page.info();
            //     };
            //     createDatatable();
            // }, 1000);
            //#endregion

        } catch (error) {
            alert(error);
        }
    }, 5000);
});

function setReturnData(data) {
    try {
        // javaConnector.logFromJS('data return: ' + JSON.stringify(data));
        returnDataContainer = JSON.parse(data);
        $('#return-table').DataTable().destroy();
        $('#return-table').DataTable({
            data: returnDataContainer
        });
        $('#return-table').DataTable().destroy();
        $('#return-table').DataTable({
            data: returnDataContainer
        });
    } catch (error) {
        alert(error);
    }
}

function setAlert(message) {
    try {
        $("#modal-loading").modal('hide');
        $("#alert-title").html("Informasi")
        $("#alert-body").html(message);
        $("#alert-footer").html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>`);
        $("#modal-alert").modal('toggle');
    } catch (error) {
        alert(error);
    }

}

function setLoading(message) {
    try {
        $("#img-loading").attr("src", "");
        $("#loading-subtitle").html(message + "<br><br>*tekan F5 untuk membatalkan");
        $("#modal-loading").modal('toggle');
    } catch (error) {
        alert(error);
    }
}

function showConnectionAlert() {
    try {
        $("#modal-loading").modal('hide');
        $("#modal-alert").modal('hide');
        // countdown(1);
        counter();
        $('#modal-connection').modal();
    } catch (error) {
        alert(error);
    }
}

function showInputReturnData() {
    try {
        var today = new Date();
        var date = today.getFullYear()
            + "-" + (today.getMonth() + 1).toString().padStart(2, 0)
            + "-" + today.getDate().toString().padStart(2, 0)
            + " " + today.getHours().toString().padStart(2, 0)
            + ":" + today.getMinutes().toString().padStart(2, 0)
            + ":" + today.getSeconds().toString().padStart(2, 0);

        $('#returndata-input-date').val(date);
        $('#modal-returndata').modal();
    } catch (error) {
        alert(error);
    }
}

function showReturnDataPage() {
    try {
        $("#page-dashboard").hide();
        $("#page-webportal").hide();
        $("#page-manuser").hide();
        $("#page-manterminal").hide();
        $("#page-offlinektp").hide();

        $("#page-returndata").show("fast");
    } catch (error) {
        alert(error);
    }
}

function counter() {
    counterTick = 180;
    var counter = document.getElementById("counter");
    function tick() {
        counter.innerHTML = counterTick.toString();
        counterTick = counterTick - 1;
        if (counterTick > 0) {
            portalConnector.checkVPNConnection();
            setTimeout(tick, 1000);
        } else if (counterTick == 0) {
            $('#modal-connection').modal('hide');
            setAlert('Koneksi jaringan bersama diputuskan');
            portalConnector.disconnectingFromVPN();
        }
    }
    tick();
}



class clock {
    static startTime() {
        var today = new Date();
        var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = clock.checkTime(m);
        s = clock.checkTime(s);
        document.getElementById('nav-clock').innerHTML =
            date + " " + h + ":" + m + ":" + s + "";
        var t = setTimeout(clock.startTime, 500);
    }
    static checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }
}

class plainModal {
    static show() {
        $("#modal-plain").modal();
    }

    static show(tittle, body, footer) {
        $("#modal-plain").modal();
        $("#plain-title").html(tittle);
        $("#plain-body").html(body);
        $("#plain-footer").html(footer);
    }

    static hide() {
        $("#modal-plain").modal('hide');
    }
}

class data {
    static getInitialData() {
        javaConnector.getDashboardData();
        scanConnector.getOnlineKtpData();
    }

    static setInitialData(data) {
        try {

            javaConnector.logFromJS('parsing data');

            /* Parsing Data */
            var parsed = JSON.parse(data);
            var dashboard = $("#page-dashboard").html();
            dashboard = dashboard.replace("[user-fullname]", parsed.fullname);
            dashboard = dashboard.replace("[bpr-fullname]", parsed.bprname);
            javaConnector.logFromJS(dashboard);

            /* set global variable */
            globalBankName = parsed.bprname;
            globalBankId = parsed.bankid;
            globalOperatorUserName = parsed.username;
            globalOperatorFullname = parsed.fullname;

            if (parsed.pcid != null) {
                $("#offlineconf-input-pcid").val(parsed.pcid);
            }

            if (parsed.conf != null) {
                $("#offlineconf-input-conf").val(parsed.conf);
            }

            /** set view data */
            $("#returndata-input-user").val(parsed.username);
            $("#page-dashboard").html(dashboard);
            $("#nav-bankname").html(globalBankName);


            if (parsed.privilege == 0) {
                manuser.hidemenu();
                manterminal.hidemenu();
            }

            /** get billing data */
            billing.show();

        } catch (err) {
            javaConnector.logFromJS(err.message);
            alert(err.message);
        }
    }
}

var jsConnector = {
    setReturn: function (result) {
        try {
            $("#return-table").html("");
            returnDataContainer = JSON.parse(result);
            // returnDataContainer = result;
            $('#return-table').DataTable().destroy();
            $('#return-table').DataTable({
                data: returnDataContainer
            });
            $('#return-table').DataTable().destroy();
            $('#return-table').DataTable({
                data: returnDataContainer
            });
        } catch (error) {
            alert(error);
        }
    },

    showReturnDataPage: function () {
        try {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();

            $("#page-returndata").show("fast");
        } catch (error) {
            alert(error);
        }
    },

    setOfflineKTPResult: function (result) {
        try {
            jsKTP = JSON.parse(result);

            //#region jsKTP Value
            /*
            jsEktp.put("nik", strList[1]);
            jsEktp.put("alamat", strList[2]);
            jsEktp.put("rtrw", strList[3] + "/" + strList[4]);
            jsEktp.put("ttl", strList[5] + "," + strList[15]);
            jsEktp.put("kec", strList[6]);
            jsEktp.put("kel", strList[7]);
            jsEktp.put("kota", strList[8]);
            jsEktp.put("kelamin", strList[9]);
            jsEktp.put("agama", strList[11]);
            jsEktp.put("status", strList[12]);
            jsEktp.put("pekerjaan", strList[13]);
            jsEktp.put("nama", strList[14]);
            jsEktp.put("provinsi", strList[16]);
            jsEktp.put("goldar", strList[10]);
            jsEktp.put("warga", strList[20]);
            jsEktp.put("berlaku", strList[17]);
            jsEktp.put("photo", strList[0]);
            jsEktp.put("signature", strList[strList.length - 1]);
             *  */
            //#endregion

            var ktphead = `
                <section>
                    <div class="text-center">
                        <h6>PROVINSI ${jsKTP.provinsi}</h6>
                        <h6>${jsKTP.kota}</h6>
                    </div>
                </section>
            `;

            var copyButton = `
                    <button id="offline-copy-nik" class="btn btn-sm" style="background-color: transparent; width:30px;">
                        <img style="width:24px" src="./img/icon/copy.png">
                    </button>
                `

            var ktp = [
                ["NIK", `: ${jsKTP.nik} ${copyButton}`],
                ["Nama", `: ${jsKTP.nama}`],
                ["Tempat/Tgl Lahir", `: ${jsKTP.ttl}`],
                ["Jenis Kelamin", `: ${jsKTP.kelamin}`],
                ["Alamat", `: ${jsKTP.alamat}`],
                ["RT/RW", `: ${jsKTP.rtrw}`],
                ["Kel/Desa", `: ${jsKTP.kel} / ${jsKTP.kota}`],
                ["Kecamatan", `: ${jsKTP.kec}`],
                ["Agama", `: ${jsKTP.agama}`],
                ["Status Perkawinan", `: ${jsKTP.status}`],
                ["Pekerjaan", `: ${jsKTP.pekerjaan}`],
                ["Kewarganegaraan", `: ${jsKTP.warga}`]
            ];

            var rows = function () {
                var html = `<section id=\"offline-ktp-value\"> 
                                <div class = "row">
                                    <div class = "col-md-9">
                `;
                ktp.forEach(element => {
                    html += `
                                        <div class="row  align-items-center">
                                            <div class="col-sm-4">
                                                <label>${element[0]}</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <label>${element[1]}</label>
                                            </div>
                                        </div>
                  `;
                });
                html += `   </div>
                                    <div class = "col-md-3">
                                        <img id="offline-result-photo"
                                        src="" class="rounded" alt="foto"
                                        style="width:100px">
                                    <div>
                                <div>
                            </section>`;

                return html;
            };

            $('#offline-result').html(`
                <div class="container reponsive-offline" style="background-image: url('./img/KTP-background.png');">
                        ${ktphead}
                        ${rows()}

                        <div class="text-center">
                            <button id="offline-disclaimer" type="button" class="btn btn-success">Cetak Form
                            Disclaimer</button>
                        </div>
                </div>
            `);

            buttonListener.OfflineKTPButtonListener();

            function hexToBase64(hexstring) {
                return btoa(hexstring.match(/\w{2}/g).map(function (a) {
                    return String.fromCharCode(parseInt(a, 16));
                }).join(""));
            }

            document.getElementById('offline-result-photo').src = 'data:image/png;base64,' + hexToBase64(jsKTP.photo);
            // document.getElementById('offline-result-signature').src = 'data:image/png;base64,' + hexToBase64(jsKTP.signature);

            $("#offline-copy-nik").click(() => {
                try {
                    const el = document.createElement('textarea'); // Create a <textarea> element
                    el.value = jsKTP.nik;  // Set its value to the string that you want copied
                    el.setAttribute('readonly', ''); // Make it readonly to be tamper-proof
                    el.style.position = 'absolute';
                    el.style.left = '-9999px'; // Move outside the screen to make it invisible
                    document.body.appendChild(el); // Append the <textarea> element to the HTML document
                    el.select(); // Select the <textarea> content
                    document.execCommand('copy'); // Copy - only works as a result of a user action (e.g. click events)
                    alert("text di-copy: " + el.value);
                    document.body.removeChild(el); // Remove the <textarea> element
                } catch (error) {
                    alert(error)
                }
            })

            $("#modal-loading").modal('hide');
        } catch (error) {
            alert(error);
        }
    }
};

function getJsConnector() {
    return jsConnector;
};
