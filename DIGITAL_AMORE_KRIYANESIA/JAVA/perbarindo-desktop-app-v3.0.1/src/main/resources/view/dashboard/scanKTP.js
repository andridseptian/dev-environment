class scanKTP {
    static setKTPResult(result) {
        var jsKTP = JSON.parse(result);

        $('offline-result').html(result);
    }

    static scanningKTP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Memeriksa data ktp, letakkan ktp pada alat pembaca ktp dan tunggu hingga proses selesai...');
            $("#img-loading").attr("src", "./img/assets/scanner/put-card.gif");
            scanConnector.scanKTP();
        }, 500);
    }

    static scanningFingerPrint() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Memeriksa data sidik jari, Letakkan jari telunjuk yang akan di scan pada alat pembaca sidik jari');
            $("#img-loading").attr("src", "./img/assets/scanner/put-finger.gif");
            // javaConnector.scanKTP();
        }, 500);
    }

    static failedReadKTP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Gagal, mengulang pembacaan');
            scanningFingerPrint()
        }, 2000);
    }

    static failedReadFP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Sidik jari tidak cocok, mengulang pembacaan');
            scanningFingerPrint()
        }, 1000);
    }
}

var scanJsConnector = {
    setOnlineScanResult: (result) => {
        try {

            if (JSON.parse(result).rc !== "00") {
                setAlert(JSON.parse(result).rm)
                return;
            } else {
                jsKTP = JSON.parse(result).data.content[0];

                //#region jsKTP Value
                /*
                "content": [
                    {
                        "NO_KK": 7471042502080014,
                        "NIK": 7471041809950001,
                        "NAMA_LGKP": "ANDRI DWI SEPTIAN",
                        "KAB_NAME": "KOTA KENDARI",
                        "AGAMA": "ISLAM",
                        "NO_RW": 7,
                        "KEC_NAME": "POASIA",
                        "JENIS_PKRJN": "BELUM/TIDAK BEKERJA",
                        "NO_RT": 19,
                        "ALAMAT": "JL. BUNGGASI",
                        "NO_KEC": 4,
                        "TMPT_LHR": "KENDARI",
                        "STATUS_KAWIN": "BELUM KAWIN",
                        "NO_PROP": 74,
                        "NAMA_LGKP_IBU": "HJ.NUR ALAM",
                        "PROP_NAME": "SULAWESI TENGGARA",
                        "NO_KAB": 71,
                        "KEL_NAME": "ANDONOHU",
                        "JENIS_KLMIN": "Laki-Laki",
                        "TGL_LHR": "1995-09-18"
                    }
                ]
                */
                //#endregion

                var ktphead = `
                    <section>
                        <div class="text-center">
                            <h6>PROVINSI ${jsKTP.PROP_NAME}</h6>
                            <h6>${jsKTP.KAB_NAME}</h6>
                        </div>
                    </section>
                `;

                var copyButton = `
                    <button id="online-copy-nik" class="btn btn-sm" style="background-color: transparent; width:30px;">
                        <img style="width:24px" src="./img/icon/copy.png">
                    </button>
                `

                var ktp = [
                    ["NIK", `: ${jsKTP.NIK} ${copyButton}`, "online-val-nik"],
                    ["Nama", `: ${jsKTP.NAMA_LGKP}`, "online-val-nama"],
                    ["Tempat/Tgl Lahir", `: ${jsKTP.TMPT_LHR}, ${jsKTP.TGL_LHR}`, "online-val-ttl"],
                    ["Jenis Kelamin", `: ${jsKTP.JENIS_KLMIN}`, "online-val-jnsklmn"],
                    ["Alamat", `: ${jsKTP.ALAMAT}`, "online-val-alamat"],
                    ["RT/RW", `: ${jsKTP.NO_RT} / ${jsKTP.NO_RW}`, "online-val-rtrw"],
                    ["Kelurahan", `: ${jsKTP.KEL_NAME}`, "online-val-kel"],
                    ["Kecamatan", `: ${jsKTP.KEC_NAME}`, "online-val-kec"],
                    ["Status Perkawinan", `: ${jsKTP.STATUS_KAWIN}`, "online-val-stskawin"],
                    ["Pekerjaan", `: ${jsKTP.JENIS_PKRJN}`, "online-val-kerja"],
                    ["Nama Ibu Kandung", `: ${jsKTP.NAMA_LGKP_IBU}`, "online-val-nmibu"],
                    ["Nomor KK", `: ${jsKTP.NO_KK}`, "online-val-nokk"]
                ];

                var rows = function () {
                    var html = `<section id=\"offline-ktp-value\"> 
                                    <div class = "row justify-content-md-center">
                                        <div class = "col-md-6">
                    `;
                    ktp.forEach(element => {
                        html += `
                                            <div class="row align-items-center">
                                                <div class="col-sm-4">
                                                    <label>${element[0]}</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <label id="${element[2]}">
                                                        ${element[1]}
                                                    </label>
                                                </div>
                                            </div>
                      `;
                    });
                    html += `           </div>
                                        <div class = "col-md-4">
                                            <img id="offline-result-photo"
                                            src="./img/profpic.png" class="rounded bg-light p-2" alt="foto"
                                            style="width:150px">
                                        <div>
                                    <div>
                                </section>`;

                    return html;
                };

                $('#online-result').html(`
                    <div class="container reponsive-offline" style="background-image: url('./img/KTP-background.png');">
                            ${ktphead}
                            ${rows()}

                            <div class="text-center">
                                <button id="online-result-disclaimer" type="button" class="btn btn-success">Cetak Form Disclaimer</button>
                            </div>
                    </div>
                `);

                $("#modal-loading").modal('hide');
                setAlert("Jangan lupa untuk mengisikan databalikan dari setiap NIK yang di cek Secara Online")

                setTimeout(() => {
                    $("#returndata-input-user").val(globalOperatorUserName)
                    $("#returndata-input-nik").val(jsKTP.NIK)
                    showInputReturnData()
                }, 10000);

                $("#online-copy-nik").click(() => {
                    try {
                        const el = document.createElement('textarea'); // Create a <textarea> element
                        el.value = jsKTP.NIK;  // Set its value to the string that you want copied
                        el.setAttribute('readonly', ''); // Make it readonly to be tamper-proof
                        el.style.position = 'absolute';
                        el.style.left = '-9999px'; // Move outside the screen to make it invisible
                        document.body.appendChild(el); // Append the <textarea> element to the HTML document
                        el.select(); // Select the <textarea> content
                        document.execCommand('copy'); // Copy - only works as a result of a user action (e.g. click events)
                        alert("text di-copy: " + el.value);
                        document.body.removeChild(el); // Remove the <textarea> element
                    } catch (error) {
                        alert(error)
                    }
                })

                $("#online-result-disclaimer").click(() => {
                    try {
                        $("#modal-disclaimer").modal('toggle');

                        $("#checked-by").html(globalOperatorFullname);

                        $("#disclaimer-input-nama").change(function () {
                            $("#approved-by").html(this.value);
                        });


                        /*
                            var ktp = [
                                ["NIK", `: ${jsKTP.NIK} ${copyButton}`, "online-val-nik"],
                                ["Nama", `: ${jsKTP.NAMA_LGKP}`, "online-val-nama"],
                                ["Tempat/Tgl Lahir", `: ${jsKTP.TMPT_LHR}, ${jsKTP.TGL_LHR}`, "online-val-ttl"],
                                ["Jenis Kelamin", `: ${jsKTP.JENIS_KLMIN}`, "online-val-jnsklmn"],
                                ["Alamat", `: ${jsKTP.ALAMAT}`, "online-val-alamat"],
                                ["RT/RW", `: ${jsKTP.NO_RT} / ${jsKTP.NO_RW}`, "online-val-rtrw"],
                                ["Kelurahan", `: ${jsKTP.KEL_NAME}`, "online-val-kel"],
                                ["Kecamatan", `: ${jsKTP.KEC_NAME}`, "online-val-kec"],
                                ["Status Perkawinan", `: ${jsKTP.STATUS_KAWIN}`, "online-val-stskawin"],
                                ["Pekerjaan", `: ${jsKTP.JENIS_PKRJN}`, "online-val-kerja"],
                                ["Nama Ibu Kandung", `: ${jsKTP.NAMA_LGKP_IBU}`, "online-val-nmibu"],
                                ["Nomor KK", `: ${jsKTP.NO_KK}`, "online-val-nokk"]
                            ];
                        */

                        if (jsKTP != "") {
                            $("#approved-by").html(jsKTP.NAMA_LGKP);

                            $("#disclaimer-input-nik").val(jsKTP.NIK);
                            $("#disclaimer-input-nama").val(jsKTP.NAMA_LGKP);

                            try {
                                $("#disclaimer-input-tanggal").val(jsKTP.TGL_LHR);
                                $("#disclaimer-input-tempat").val(jsKTP.TMPT_LHR);
                                $("#disclaimer-input-kelamin").val(jsKTP.JENIS_KLMIN);
                                $("#disclaimer-input-alamat").val(jsKTP.ALAMAT);
                                $("#disclaimer-input-rtrw").val(jsKTP.NO_RT + '/' + jsKTP.NO_RW);
                                $("#disclaimer-input-keldesa").val(jsKTP.KEL_NAME);
                                $("#disclaimer-input-kec").val(jsKTP.KEC_NAME);
                                $("#disclaimer-input-kawin").val(jsKTP.STATUS_KAWIN);
                                $("#disclaimer-input-kerja").val(jsKTP.JENIS_PKRJN);
                                $("#disclaimer-input-warga").val('WNI');
                            } catch (error) { }
                        };
                    } catch (error) {
                        alert(error);
                    }
                })
            }
        } catch (error) {
            alert(error)
        }
    },

    setOnlineKtpConfig: (result) => {
        $("#modal-offlinesync").modal('hide');
        var data = JSON.parse(result)
        $('#offlinesync-input-url').val(data.url)
        $('#offlinesync-input-username').val(data.userid)
        $('#offlinesync-input-password').val(data.password)
    }
}

function getScanConnector() {
    return scanJsConnector;
};