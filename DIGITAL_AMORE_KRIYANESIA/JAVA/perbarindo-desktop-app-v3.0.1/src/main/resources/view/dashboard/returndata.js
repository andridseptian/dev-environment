class returndata {
    static getdata() {
        try {
            $("#return-header").html(`
                <tr>
                    <th>Waktu cek</th>
                    <th>User Operator</th>
                    <th>NIK</th>
                    <th>CIF</th>
                    <th></th>
                </tr>
            `);

            $("#return-body").html(`
                <tr class="row-of-data">
                    <td class="time">--------------------------------</td>
                    <td class="user">--------------------------------</td>
                    <td class="nik">--------------------------------</td>
                    <td class="cif">--------------------------------</td>
                    <td class="return-button">--------------------------------</td>
                </tr>
            `);

        } catch (error) {
            alert(error);
        }
    }

    static setReturnDataRange() {
        var nowMonth = ("0" + (new Date().getMonth() + 1)).slice(-2);
        var nowYear = new Date().getFullYear();

        var monthParse = [
            ["Januari", "01"],
            ["Februari", "02"],
            ["Maret", "03"],
            ["April", "04"],
            ["Mei", "05"],
            ["Juni", "06"],
            ["Juli", "07"],
            ["Agustus", "08"],
            ["September", "09"],
            ["Oktober", "10"],
            ["November", "11"],
            ["Desember", "12"],
        ]

        var nowMonthName = "";
        monthParse.forEach(element => {
            if (element[1] === nowMonth) {
                nowMonthName = element[0]
            }
        });

        $("#return-year").val(nowYear);
        $("#return-month").val(nowMonthName);
    }

}