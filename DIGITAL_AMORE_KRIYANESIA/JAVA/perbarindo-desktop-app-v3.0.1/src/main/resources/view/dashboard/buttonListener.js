class buttonListener {
    static menuButtonListener() {

        $("#menu-fold").click(function () {
            if ($(window).width() > 800) {
                if ($("#side-nav").is(":visible")) {
                    $("#side-nav").toggle("fast", "swing");
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $("#side-nav").toggle("fast", "swing");
                    $("#menu-panes").css("padding-left", "350px");
                }
            } else {
                $("#side-nav").toggle("fast", "swing");
                $("#menu-panes").css("padding-left", "50px");
            }
        });

        $("#a-dashboard").click(function () {
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-dashboard").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-webportal").click(function () {

            $("#page-dashboard").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-webportal").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-returndata").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-returndata").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-manuser").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $("#page-manterminal").hide();

            $("#page-manuser").show("fast");

            setTimeout(() => {
                javaConnector.sendRequestFromJS('getuserlist', 'null');
            }, 500);

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-manterminal").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $("#page-manuser").hide();

            $("#page-manterminal").show("fast");

            setTimeout(() => {
                javaConnector.sendRequestFromJS('getserialnumber', 'null');
            }, 500);

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-offlinektp").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-billing").hide();

            $("#page-offlinektp").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-billing").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();

            $("#page-billing").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#footer-logout").click(function () {
            try {
                setLoading('Membersihkan data....');
                // alert('logout');
                javaConnector.sendRequestFromJS('logout', JSON.stringify({
                    username: globalOperatorUserName
                }));
                setTimeout(() => {
                    window.open('login.html');
                }, 5000);
            } catch (error) {
                alert(error);
            }
        });
    }

    static portalButtonListener() {
        portal.buttonListener();
    }

    static returnDataButtonListener() {
        $("#return-inputdata").click(function () {
            returndata.setReturnDataRange();
            javaConnector.getReturnData();
            showInputReturnData();
        });

        $("#return-saveexcel").click(function () {
            javaConnector.saveReturnDataToExcel(JSON.stringify(returnDataContainer));
        });

        var parseReturnMonthAndYear = function () {
            var year = $("#return-year").val();
            var month = $("#return-month").val();

            var monthParse = [
                ["Januari", "01"],
                ["Februari", "02"],
                ["Maret", "03"],
                ["April", "04"],
                ["Mei", "05"],
                ["Juni", "06"],
                ["Juli", "07"],
                ["Agustus", "08"],
                ["September", "09"],
                ["Oktober", "10"],
                ["November", "11"],
                ["Desember", "12"],
            ]

            var monthValue = "false";

            monthParse.forEach(element => {
                if (element[0] == month) {
                    monthValue = element[1];
                }
            });

            return {
                year: year,
                month: monthValue
            }
        }

        $("#returndata-input").click(function () {
            try {
                var data = [
                    $("#returndata-input-date").val(),
                    $("#returndata-input-user").val(),
                    $("#returndata-input-nik").val(),
                    $("#returndata-input-cif").val(),
                    `<button id="returndata-button" type="button" class="btn btn-danger btn-sm">delete</button>`
                ];

                returnDataContainer.push(data);

                var data = parseReturnMonthAndYear();
                javaConnector.saveReturnData(data.year, data.month, JSON.stringify(returnDataContainer));

                $('#return-table').DataTable().destroy();

                $('#return-table').DataTable({
                    data: returnDataContainer
                });

            } catch (error) {
                alert(error);
            }
        });

        function getReturnDataByMonthAndYear() {
            try {
                var year = $("#return-year").val();
                var month = $("#return-month").val();

                var monthParse = [
                    ["Januari", "01"],
                    ["Februari", "02"],
                    ["Maret", "03"],
                    ["April", "04"],
                    ["Mei", "05"],
                    ["Juni", "06"],
                    ["Juli", "07"],
                    ["Agustus", "08"],
                    ["September", "09"],
                    ["Oktober", "10"],
                    ["November", "11"],
                    ["Desember", "12"],
                ]

                var monthValue = "false";

                monthParse.forEach(element => {
                    if (element[0] == month) {
                        monthValue = element[1];
                    }
                });

                // alert(year + " " + monthValue);

                $("#return-table").html("tidak ada data");

                var data = parseReturnMonthAndYear();

                javaConnector.getReturnDataByMonthAndYear(data.month, data.year);
            } catch (error) {
                alert(error);
            }
        }

        $("#return-getdatabymonth").click(function () {
            getReturnDataByMonthAndYear();
        })

        $("#return-year").change(function () {
            getReturnDataByMonthAndYear();
        });

        $("#return-month").change(function () {
            getReturnDataByMonthAndYear();
        });

        $("#return-table").on('click', '.btn', function () {
            try {
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                var cell1 = tr.find('td').eq(1).html();
                var cell2 = tr.find('td').eq(2).html();
                var cell3 = tr.find('td').eq(3).html();
                var cell4 = tr.find('td').eq(4).html();

                var output = `["${cell0}","${cell1}","${cell2}","${cell3}",${JSON.stringify(cell4)}]`;

                var temp = JSON.stringify(returnDataContainer)
                    .replace(output, "")
                    .replace(',,', ',')
                    .replace(',]', ']')
                    .replace('[,', '[');

                returnDataContainer = JSON.parse(temp);

                var data = parseReturnMonthAndYear();

                javaConnector.saveReturnData(data.year, data.month, JSON.stringify(returnDataContainer));

                $('#return-table').DataTable().destroy();

                $('#return-table').DataTable({
                    data: returnDataContainer
                });
            } catch (error) {
                alert(error);
            }
        });

        $("#return-openportal").click(function () {
            try {
                portalConnector.openWebPortalDatabalikan();
            } catch (error) {
                alert(error);
            }
        });
    }

    static connectionButtonListener() {
        $("#connection-continue").click(function () {
            setAlert('silahkan lanjutkan koneksi anda');
            counterTick = -1;
            portalConnector.continueVPN();
        });

        $("#connection-disconnect").click(function () {
            setAlert('koneksi anda akan diputuskan');
            counterTick = -1;
            portalConnector.disconnectingFromVPN();
        });
    }

    static disclaimerButtonListener() {
        $("#generate-disclaimer").click(function () {
            try {
                $("#modal-disclaimer").modal('toggle');

                $("#checked-by").html(globalOperatorFullname);

                $("#disclaimer-input-nama").change(function () {
                    $("#approved-by").html(this.value);
                });
            } catch (error) {
                alert(error);
            }
        });

        $("#disclaimer-cb-alamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-alamat").prop('disabled', false);
            } else {
                $("#disclaimer-input-alamat").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-detailalamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-rtrw").prop('disabled', false);
                $("#disclaimer-input-keldesa").prop('disabled', false);
                $("#disclaimer-input-kec").prop('disabled', false);
            } else {
                $("#disclaimer-input-rtrw").prop('disabled', true);
                $("#disclaimer-input-keldesa").prop('disabled', true);
                $("#disclaimer-input-kec").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-status").click(function () {
            if (this.checked) {
                $("#disclaimer-input-kerja").prop('disabled', false);
                $("#disclaimer-input-kawin").prop('disabled', false);
                $("#disclaimer-input-warga").prop('disabled', false);
            } else {
                $("#disclaimer-input-kerja").prop('disabled', true);
                $("#disclaimer-input-kawin").prop('disabled', true);
                $("#disclaimer-input-warga").prop('disabled', true);
            }
        });

        $("#disclaimer-footer").html(`
            <button id="disclaimer-disagree" type="button" class="btn btn-danger" 
                data-dismiss="modal">Tidak Setuju</button>
            <button id="disclaimer-portal-agree" type="button" class="btn btn-primary">Setuju</button>
        `)

        $("#disclaimer-portal-agree").click(function () {

            if (
                $("#disclaimer-input-nik").val() != "" &&
                $("#disclaimer-input-nama").val() != "" &&
                $("#disclaimer-input-tanggal").val() != "" &&
                $("#disclaimer-input-tempat").val() != ""
            ) {

                $("#modal-disclaimer").modal('toggle');

                var data = {
                    nik: $("#disclaimer-input-nik").val(),
                    nama: $("#disclaimer-input-nama").val(),
                    tanggal: $("#disclaimer-input-tanggal").val(),
                    tempat: $("#disclaimer-input-tempat").val(),
                    kelamin: $("#disclaimer-input-kelamin").val(),
                    alamat: $("#disclaimer-input-alamat").val(),
                    rtrw: $("#disclaimer-input-rtrw").val(),
                    keldesa: $("#disclaimer-input-keldesa").val(),
                    kec: $("#disclaimer-input-kec").val(),
                    kawin: $("#disclaimer-input-kawin").val(),
                    kerja: $("#disclaimer-input-kerja").val(),
                    warga: $("#disclaimer-input-warga").val()
                }

                javaConnector.createDiscalimerToPDF(JSON.stringify(data));
            } else {
                setAlert('form nik,nama dan tempat tanggal lahir tidak boleh kosong');
            }
        });
    }

    static OfflineKTPButtonListener() {
        $("#offline-scan").click(() => {
            scanConnector.ktpInitialize();
            setLoading('Mengakses alat scan ktp, pastikan alat telah terhubung....');
            $("#img-loading").attr("src", "./img/assets/scanner/plug-device.gif");
            // setTimeout(() => {
            //     $("#modal-loading").modal('hide');
            //     setTimeout(() => {
            //         setLoading('Memeriksa ktp anda....');
            //         javaConnector.scanKTP();
            //     }, 500);
            // }, 5000);
        });

        $("#offline-conf").click(() => {
            setTimeout(() => {
                $("#modal-offlineconf").modal('toggle');

                $("#offlineconf-sync").click(() => {

                    $("#offlineconf-sync").prop('disabled', true);
                    setTimeout(() => {
                        $("#offlineconf-sync").prop('disabled', false);
                    }, 5000);

                    javaConnector.sendRequestFromJS('setdeviceid', JSON.stringify({
                        pcid: $("#offlineconf-input-pcid").val(),
                        conf: $("#offlineconf-input-conf").val()
                    }));;
                })
            }, 300);
        })

        $("#offline-disclaimer").click(() => {
            try {
                $("#modal-disclaimer").modal('toggle');

                $("#checked-by").html(globalOperatorFullname);

                $("#disclaimer-input-nama").change(function () {
                    $("#approved-by").html(this.value);
                });

                if (jsKTP != "") {

                    //#region jsKTP Body
                    /* 
                        var ktp = [
                        ["NIK", `: ${jsKTP.nik}`],
                        ["Nama", `: ${jsKTP.nama}`],
                        ["Tempat/Tgl Lahir", `: ${jsKTP.ttl}`],
                        ["Jenis Kelamin", `: ${jsKTP.kelamin}`],
                        ["Alamat", `: ${jsKTP.alamat}`],
                        ["RT/RW", `: ${jsKTP.rtrw}`],
                        ["Kel/Desa", `: ${jsKTP.kel} / ${jsKTP.kota}`],
                        ["Kecamatan", `: ${jsKTP.kec}`],
                        ["Agama", `: ${jsKTP.agama}`],
                        ["Status Perkawinan", `: ${jsKTP.status}`],
                        ["Pekerjaan", `: ${jsKTP.pekerjaan}`],
                        ["Kewarganegaraan", `: ${jsKTP.warga}`]
                        ];
                    */
                    //#endregion


                    $("#approved-by").html(jsKTP.nama);

                    $("#disclaimer-input-nik").val(jsKTP.nik);
                    $("#disclaimer-input-nama").val(jsKTP.nama);

                    try {
                        var ttl = jsKTP.ttl.split(",");

                        $("#disclaimer-input-tanggal").val(typeof (ttl[1]) !== 'undefined' ? ttl[1] : '');
                        $("#disclaimer-input-tempat").val(typeof (ttl[0]) !== 'undefined' ? ttl[0] : '');
                        $("#disclaimer-input-kelamin").val(jsKTP.kelamin);
                        $("#disclaimer-input-alamat").val(jsKTP.alamat);
                        $("#disclaimer-input-rtrw").val(jsKTP.rtrw);
                        $("#disclaimer-input-keldesa").val(jsKTP.kel);
                        $("#disclaimer-input-kec").val(jsKTP.kec);
                        $("#disclaimer-input-kawin").val(jsKTP.status);
                        $("#disclaimer-input-kerja").val(jsKTP.pekerjaan);
                        $("#disclaimer-input-warga").val(jsKTP.warga);
                    } catch (error) { }
                };

            } catch (error) {
                alert(error);
            }
        });

        $("#disclaimer-cb-alamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-alamat").prop('disabled', false);
            } else {
                $("#disclaimer-input-alamat").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-detailalamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-rtrw").prop('disabled', false);
                $("#disclaimer-input-keldesa").prop('disabled', false);
                $("#disclaimer-input-kec").prop('disabled', false);
            } else {
                $("#disclaimer-input-rtrw").prop('disabled', true);
                $("#disclaimer-input-keldesa").prop('disabled', true);
                $("#disclaimer-input-kec").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-status").click(function () {
            if (this.checked) {
                $("#disclaimer-input-kerja").prop('disabled', false);
                $("#disclaimer-input-kawin").prop('disabled', false);
                $("#disclaimer-input-warga").prop('disabled', false);
            } else {
                $("#disclaimer-input-kerja").prop('disabled', true);
                $("#disclaimer-input-kawin").prop('disabled', true);
                $("#disclaimer-input-warga").prop('disabled', true);
            }
        });

        $("#disclaimer-footer").html(`
            <button id="disclaimer-disagree" type="button" class="btn btn-danger" 
                data-dismiss="modal">Tidak Setuju</button>
            <button id="disclaimer-offline-agree" type="button" class="btn btn-primary">Setuju</button>
        `)

        $("#disclaimer-offline-agree").click(function () {
            if (
                $("#disclaimer-input-nik").val() != "" &&
                $("#disclaimer-input-nama").val() != "" &&
                $("#disclaimer-input-tanggal").val() != "" &&
                $("#disclaimer-input-tempat").val() != ""
            ) {
                $("#modal-disclaimer").modal('toggle');
                var data = {
                    nik: $("#disclaimer-input-nik").val(),
                    nama: $("#disclaimer-input-nama").val(),
                    tanggal: $("#disclaimer-input-tanggal").val(),
                    tempat: $("#disclaimer-input-tempat").val(),
                    kelamin: $("#disclaimer-input-kelamin").val(),
                    alamat: $("#disclaimer-input-alamat").val(),
                    rtrw: $("#disclaimer-input-rtrw").val(),
                    keldesa: $("#disclaimer-input-keldesa").val(),
                    kec: $("#disclaimer-input-kec").val(),
                    kawin: $("#disclaimer-input-kawin").val(),
                    kerja: $("#disclaimer-input-kerja").val(),
                    warga: $("#disclaimer-input-warga").val()
                }
                javaConnector.createDiscalimerToPDF(JSON.stringify(data));
            } else {
                setAlert('form nik,nama dan tempat tanggal lahir tidak boleh kosong');
            }
        });

        $("#dukcapil-online-conf").click(function () {
            $("#modal-offlinesync").modal('toggle');

            // $('#offlinesync-input-url').val('/dukcapil/get_json/PERBARINDO/CALL_NIK')
            // $('#offlinesync-input-nik').val('7471041809950001')
            // $('#offlinesync-input-username').val('317502riwa')
            // $('#offlinesync-input-password').val('PkGLejm9')
            // $('#offlinesync-input-IP_USER').val('admindev')

            /* 
                    <label for="offlinesync-input-url">URL </label>
                    <input id="offlinesync-input-url" class="form-control form-control-sm" type="text"
                        placeholder="path url dukcapil" required> <br>
                    <label for="offlinesync-input-username">USERNAME </label>
                    <input id="offlinesync-input-username" class="form-control form-control-sm" type="text"
                        placeholder="user id dukcapil" required> <br>
                    <label for="offlinesync-input-password">PASSWORD </label>
                    <input id="offlinesync-input-password" class="form-control form-control-sm" type="text"
                        placeholder="password dukcapil" required>
                    <label for="offlinesync-input-nik">NIK </label>
                    <input id="offlinesync-input-nik" class="form-control form-control-sm" type="text"
                        placeholder="password dukcapil" required>
                public void checkNikDukcapil(String path, String nik, String user_id, String password, String IP_USER)

                public void saveOnlineScanConfig(String url, String userid, String password)
            */

            $("#offlinesync-save-conf").click(() => {
                var path = $('#offlinesync-input-url').val().replace('http://172.16.160.128:8000', '')
                var user_id = $('#offlinesync-input-username').val()
                var password = $('#offlinesync-input-password').val()

                $('#offlinesync-input-url').val(path)

                scanConnector.saveOnlineScanConfig(path, user_id, password)
            })
        });

        $("#dukcapil-online-check").click(function () {

            setLoading("Mengecek Data NIK")

            $('#online-result').html(`<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>`)

            var path = $('#offlinesync-input-url').val()
            var nik = $('#input-online-nik').val()
            var user_id = $('#offlinesync-input-username').val()
            var password = $('#offlinesync-input-password').val()
            var IP_USER = globalOperatorUserName

            setTimeout(() => {
                if (
                    path != '' &&
                    nik != '' &&
                    user_id != '' &&
                    password != ''
                ) {
                    if (nik.length != 16 || isNaN(nik)) {
                        setAlert("NIK harus terdiri dari 16 digit angka")
                    } else {
                        scanConnector.checkNikDukcapil(path, nik, user_id, password, IP_USER)
                    }
                } else {
                    setAlert('Konfigurasi/NIK tidak boleh kosong, pastikan anda sudah mengisi konfigurasi dengan benar')
                }
            }, 2000);

        })
    }
}