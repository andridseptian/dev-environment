package com.acs.ussd.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "parse_xml")
public class ParseXML implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "parse_xml_seq")
  @SequenceGenerator(name = "parse_xml_seq", sequenceName = "parse_xml_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "client_code", nullable = false)
  private String clientCode;
  
  @Column(name = "provider", nullable = false)
  private String provider;
  
  @Column(name = "target_content")
  @Type(type = "text")
  private String targetContent;
  
  @Column(name = "replace_content")
  private String replaceContent;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getClientCode() {
    return this.clientCode;
  }
  
  public void setClientCode(String clientCode) {
    this.clientCode = clientCode;
  }
  
  public String getProvider() {
    return this.provider;
  }
  
  public void setProvider(String provider) {
    this.provider = provider;
  }
  
  public String getTargetContent() {
    return this.targetContent;
  }
  
  public void setTargetContent(String targetContent) {
    this.targetContent = targetContent;
  }
  
  public String getReplaceContent() {
    return this.replaceContent;
  }
  
  public void setReplaceContent(String replaceContent) {
    this.replaceContent = replaceContent;
  }
}
