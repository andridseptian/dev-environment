package com.acs.ussd.util;

import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.Session;
import org.json.JSONException;
import org.json.JSONObject;

public class UssdUtility {
  public static void setSelectedMenu(Session session, Menu selectedMenu) {
    JSONObject json = new JSONObject(session.getFreeText());
    json.put("selected_menu", selectedMenu.getMenuName());
    session.setFreeText(json.toString());
  }
  
  public static JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
    JSONObject mergedJSON = new JSONObject();
    try {
      mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
      for (String crunchifyKey : JSONObject.getNames(json2))
        mergedJSON.put(crunchifyKey, json2.get(crunchifyKey)); 
    } catch (JSONException e) {
      throw new JSONException("JSON Exception" + e);
    } 
    return mergedJSON;
  }
}
