package com.acs.ussd.service;

import com.acs.ussd.dao.MLoggerDao;
import com.acs.ussd.entity.MLogger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("loggerService")
public class LoggerService {
  @Autowired
  private MLoggerDao mLoggerDao;
  
  @Transactional
  public void logMsg(MLogger ml) {
    this.mLoggerDao.save(ml);
  }
  
  public List<MLogger> find(String keyword, int first, int max) {
    return this.mLoggerDao.find(keyword, first, max);
  }
  
  public Integer count(String keyword) {
    return this.mLoggerDao.count(keyword);
  }
}
