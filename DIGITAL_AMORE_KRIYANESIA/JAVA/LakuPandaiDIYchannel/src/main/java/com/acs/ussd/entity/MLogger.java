package com.acs.ussd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "m_logger")
public class MLogger implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "dcms.m_logger_id_seq")
  @SequenceGenerator(name = "dcms.m_logger_id_seq", sequenceName = "dcms.m_logger_id_seq")
  @Column(name = "logger_id", nullable = false)
  private Long id;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "log_time")
  private Date logTime;
  
  @Column(name = "incoming", length = 50)
  private Boolean incoming;
  
  @Column(name = "msg")
  @Type(type = "text")
  private String msg;
  
  @Column(name = "host_address", length = 20)
  private String hostAddress;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Date getLogTime() {
    return this.logTime;
  }
  
  public void setLogTime(Date logTime) {
    this.logTime = logTime;
  }
  
  public String getMsg() {
    return this.msg;
  }
  
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  public Boolean getIncoming() {
    return this.incoming;
  }
  
  public void setIncoming(Boolean incoming) {
    this.incoming = incoming;
  }
  
  public String getHostAddress() {
    return this.hostAddress;
  }
  
  public void setHostAddress(String hostAddress) {
    this.hostAddress = hostAddress;
  }
}
