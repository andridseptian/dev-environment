package com.acs.ussd.util;

import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.Prefix;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.spring.SpringImpl;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.util.StringUtils;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class CheckPrefix implements TransactionParticipant, Configurable {
  Configuration cfg;
  
  public Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    String no_hp = ctx.getString("NO_HP");
    Trx trx = (Trx)ctx.get("TRX");
    String prefix = "";
    if (StringUtils.isNumber(no_hp) && no_hp.length() >= 6) {
      if (no_hp.substring(0, 2).equals("62"))
        no_hp = "0" + no_hp.substring(2); 
      Prefix pref = SpringImpl.getPrefixDao().find(no_hp.substring(0, 4));
      if (pref == null) {
        pref = SpringImpl.getPrefixDao().find(no_hp.substring(0, 5));
        if (pref == null) {
          pref = SpringImpl.getPrefixDao().find(no_hp.substring(0, 6));
          if (pref == null) {
            prefix = "not-supported";
          } else {
            prefix = pref.getProvider().toLowerCase();
          } 
        } else {
          prefix = pref.getProvider().toLowerCase();
        } 
      } else {
        prefix = pref.getProvider().toLowerCase();
      } 
    } else {
      prefix = "not-supported";
    } 
    if (prefix.equals("axis"))
      prefix = "xl"; 
    this.log.info("Provider : " + prefix);
    List<Menu> menuUtama = SpringImpl.getMenuDao().findByClientGroupType(trx.getClient().getClientCode(), prefix);
    this.log.info(Integer.valueOf(menuUtama.size()));
    if (menuUtama.size() > 0) {
      try {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (int i = 0; i < menuUtama.size(); i++) {
          sb.append(((Menu)menuUtama.get(i)).getDisplayName());
          if (i + 1 < menuUtama.size())
            sb.append(", "); 
        } 
        sb.append(")");
        JSONObject json = new JSONObject();
        json.put("prefix", prefix.toUpperCase());
        json.put("denom", sb);
        trx.setClientRc("00");
        trx.setMsgContent(json.toString());
      } catch (JSONException ex) {
        Logger.getLogger(CheckPrefix.class.getName()).log(Level.SEVERE, (String)null, (Throwable)ex);
      } 
    } else {
      trx.setClientRc("99");
      trx.setMsgContent("Pulsa Tidak Tersedia");
    } 
    ctx.put("TRX", trx);
    return 65;
  }
  
  public void commit(long l, Serializable srlzbl) {}
  
  public void abort(long l, Serializable srlzbl) {}
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
