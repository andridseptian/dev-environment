package com.acs.ussd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "spam")
public class SpamGammu implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "spam_id_seq")
  @SequenceGenerator(name = "spam_id_seq", sequenceName = "spam_id_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "dtm_receive")
  private Date dtmReceive;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "dtm_processed")
  private Date dtmProcessed;
  
  @Column(name = "sender", length = 20)
  private String sender;
  
  @Column(name = "sms_center", length = 20)
  private String smsCenter;
  
  @Column(name = "text")
  @Type(type = "text")
  private String text;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Date getDtmReceive() {
    return this.dtmReceive;
  }
  
  public void setDtmReceive(Date dtmReceive) {
    this.dtmReceive = dtmReceive;
  }
  
  public Date getDtmProcessed() {
    return this.dtmProcessed;
  }
  
  public void setDtmProcessed(Date dtmProcessed) {
    this.dtmProcessed = dtmProcessed;
  }
  
  public String getSender() {
    return this.sender;
  }
  
  public void setSender(String sender) {
    this.sender = sender;
  }
  
  public String getSmsCenter() {
    return this.smsCenter;
  }
  
  public void setSmsCenter(String smsCenter) {
    this.smsCenter = smsCenter;
  }
  
  public String getText() {
    return this.text;
  }
  
  public void setText(String text) {
    this.text = text;
  }
}
