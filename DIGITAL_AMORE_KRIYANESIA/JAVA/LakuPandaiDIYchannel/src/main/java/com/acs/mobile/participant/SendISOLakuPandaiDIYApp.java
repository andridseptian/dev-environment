package com.acs.mobile.participant;

import com.acs.txn.ISOSender;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.TransactionStatus;
import com.acs.util.Utility;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;

public class SendISOLakuPandaiDIYApp implements TransactionParticipant, Configurable {

    private Configuration cfg;

    Log log = Log.getLog("Q2", getClass().getName());

    public int prepare(long l, Serializable srlzbl) {
        return 1;
    }

    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get("TRX");
        ISOMsg req = new ISOMsg(Constant.MTI_POSTING);
        String pc = ctx.getString("pc");
        String token = ctx.getString("token");
        String fcm_key = ctx.getString("fcm_key");
        String action = ctx.getString("SMS");
        String dc = this.cfg.get("dc", "6027");
        String destination = this.cfg.get("host");
        int pathRc = this.cfg.getInt("path_rc", 39);
        int pathClientReff = this.cfg.getInt("path_reff", 37);
        int pathMessage = this.cfg.getInt("path_message", 61);
        ISOMsg hasil = new ISOMsg();
        String rrn = Utility.generateRrn();
        if (action.equals("history_trx"))
        {
            String sDate = ctx.getString("sDate");
            String eDate = ctx.getString("eDate");
            String userId = trx.getMsisdn();
            String page = ctx.getString("page");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("startDate", sDate);
            content.put("endDate", eDate);
            content.put("page", page);
            String url = "http://localhost:10020/service/web/mutasiAgent";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                String outputLog = output.length() <= 2000 ? output : "repsonse body too long...";;
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + outputLog + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else if (action.equals("notifikasi"))
        {
            String userId = trx.getMsisdn();
            String page = ctx.getString("page");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("page", page);
            String url = "http://localhost:10020:8787/service/web/notificationAgent";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + output + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else if (action.equals("notifikasi_mobile"))
        {
            String userId = trx.getMsisdn();
            String page = ctx.getString("page");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("page", page);
            String url = "http://localhost:10020/service/web/notifikasi_mobile";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + output + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else if (action.equals("detail_notifikasi_mobile_xx"))
        {
            String userId = trx.getMsisdn();
            String page = ctx.getString("resi");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("resi", page);
            String url = "http://localhost:10020/service/web/detail_notifikasi_mobile";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + output + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else if (action.equals("list_bank"))
        {
            String userId = trx.getMsisdn();
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            String url = "http://localhost:10020/service/web/bank";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + output + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else if (action.equals("get_menu"))
        {
            String tipe = trx.getReqMessage();
            JSONObject content = new JSONObject();
            content.put("type", tipe);
            String url = "http://localhost:10020/service/web/product";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + output + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else if (action.equals("initialize"))
        {
            String userId = trx.getMsisdn();
            String info = ctx.getString("info");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("info", info);
            String url = "http://localhost:10020/service/web/initialize";
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity((HttpEntity) body);
            try
            {
                CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
                int RC = closeableHttpResponse.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
                String output = br.readLine();
                this.log.info("Send request to " + url + "\n    Body : " + content
                        .toString() + "\n    Resp : " + output + "\n    Status : " + RC);
                trx.setMsgContent((RC != 200) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                if (RC != 200)
                {
                    trx.setClientRc("99");
                } else
                {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus((RC == 200) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (IOException ex)
            {
                Logger.getLogger(SendISOLakuPandaiDIYApp.class.getName()).log(Level.SEVERE, (String) null, ex);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        } else
        {
            try
            {
                trx.setSendingTime(new Date());
                req.setPackager((ISOPackager) new ISO87APackager());
                req.set(2, trx.getMsisdn());
                req.set(3, pc);
                req.set(7, (new SimpleDateFormat("MMddHHmmss")).format(new Date()));
                req.set(11, trx.getStan());
                req.set(32, dc);
                req.set(37, rrn);
                req.set(41, "MOBI-APP");
                req.set(42, trx.getProvider());
                req.set(46, rrn);
                req.set(48, trx.getReqMessage());
                req.set(61, token);
                req.set(62, fcm_key);
                hasil = ISOSender.request(destination, req);
                if (hasil != null && hasil.hasField(pathMessage) && hasil.hasField(pathRc) && hasil.hasField(pathClientReff))
                {
                    String rsp = hasil.getString(pathMessage);
                    rsp = rsp.replace("<", "&lt;");
                    rsp = rsp.replace(">", "&gt;");
                    if (action.equals("request_setor_tunai") || action.equals("inquiry_payment"))
                    {
                        ctx.put("TOKEN_BE", hasil.getString("61"));
                    }
                    if (action.equals("request_transfer_on_us") || action.equals("request_transfer_on_us_lakupandai"))
                    {
                        ctx.put("TOKEN_BE", hasil.getString("61"));
                    }
                    if (action.equals("request_transfer_off_us") || action.equals("request_transfer_off_us_lakupandai"))
                    {
                        ctx.put("TOKEN_BE", hasil.getString("61"));
                    }
                    if (action.equals("inquiry_prepaid"))
                    {
                        ctx.put("TOKEN_BE", hasil.getString("61"));
                    }
                    String rc = hasil.getString(pathRc);
                    trx.setMsgContent(rc.equals(Constant.RC_UNABLE_TO_ROUTE_TRANSACTION) ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : rsp);
                    trx.setClientRc(rc);
                    trx.setClientReff(hasil.getString(pathClientReff));
                    trx.setStatus((rc.equals(Constant.RC_APPROVED) || rc.equals(Constant.RC_TIME_OUT)) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
                } else
                {
                    ctx.put("INFOTRX", pathMessage + "," + pathRc + "," + pathClientReff + " not found in response");
                    trx.setStatus(TransactionStatus.FAILED);
                    trx.setClientRc(Constant.RC_MISSING_MANDATORY_PARAMETER);
                }
            } catch (org.jpos.util.NameRegistrar.NotFoundException ex)
            {
                ctx.put("INFOTRX", "UNABLE_TO_ROUTE_TRANSACTION");
                trx.setStatus(TransactionStatus.FAILED);
                trx.setClientRc(Constant.RC_UNABLE_TO_ROUTE_TRANSACTION);
                this.log.error(ex);
            } catch (ISOException isoe)
            {
                ctx.put("INFOTRX", "FORMAT_ERROR");
                trx.setStatus(TransactionStatus.FAILED);
                trx.setClientRc(Constant.RC_FORMAT_ERROR);
                this.log.error(isoe);
            } catch (NullPointerException noe)
            {
                ctx.put("INFOTRX", "NULL");
                trx.setStatus(TransactionStatus.FAILED);
                trx.setClientRc(Constant.RC_UNKNOWN_ERROR);
                this.log.error(noe);
            } finally
            {
                ctx.put(Constant.OUT, hasil);
                ctx.put("TRX", trx);
            }
        }
    }

    public void abort(long l, Serializable srlzbl) {
    }

    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
