package com.acs.ussd.service;

import com.acs.ussd.dao.MenuDao;
import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("menuService")
public class MenuService {
  @Autowired
  private MenuDao menuDao;
  
  public List<Menu> findDefaultMenu(RequestMapping req) {
    return this.menuDao.findByClientAndGroup(req.getClient().getClientCode(), req.getDefaultGroup());
  }
}
