package com.acs.ussd.participant;

import com.acs.ussd.entity.Session;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.TransactionStatus;
import com.acs.ussd.util.EncryptUtility;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestParser implements TransactionParticipant, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    Session session = (Session)ctx.get("SESSION");
    LogEvent le = new LogEvent(trx.getQueueKey());
    trx.setRequestTime(new Date());
    try {
      if ((new JSONObject(session.getFreeText())).has("selected_menu")) {
        JSONObject jsonStep = new JSONObject(session.getFreeText());
        String[] hasil = trx.getReqMessage().split("(?<=[{}])");
        for (String req : hasil) {
          if (req.endsWith("}") && 
            !jsonStep.has(req.substring(0, req.length() - 1))) {
            ctx.put("INFOTRX", "MENU ERROR : parameter {" + req + " not found in freetext");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_FORMAT_ERROR);
            return 64;
          } 
        } 
        return 1;
      } 
      return 64;
    } catch (JSONException|NullPointerException ex) {
      le.addMessage(ex.toString());
      return 64;
    } finally {
      ctx.put("LE", le);
    } 
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    Session session = (Session)ctx.get("SESSION");
    LogEvent le = new LogEvent(trx.getQueueKey());
    String reqMessage = trx.getReqMessage();
    try {
      JSONObject jsonStep = new JSONObject(session.getFreeText());
      String message = "";
      String[] hasil = reqMessage.split("[{}]");
      for (String x : hasil) {
        if (jsonStep.has(x))
          x = jsonStep.getString(x); 
        message = message + x;
      } 
      String reqBase64 = "";
      String req1Des = "";
      if (message.indexOf("Base64") > 0) {
        message = message.replace("Base64", "~");
        String[] replace = message.split("(?<=[~])");
        for (String r : replace) {
          if (r.startsWith("(") && r.contains(")")) {
            String replaceThis = r.substring(r.indexOf('(') + 1, r.indexOf(')'));
            String messageAll = r.substring(r.indexOf('('), r.indexOf(')') + 1);
            byte[] encodedBytes = Base64.encodeBase64(replaceThis.getBytes());
            String h = new String(encodedBytes);
            r = r.replaceAll(messageAll, h);
          } 
          reqBase64 = reqBase64 + r;
        } 
        trx.setReqMessage(reqBase64.replace("~", "").replaceAll("\\(", "").replaceAll("\\)", ""));
        ctx.put("TRX", trx);
      } else if (message.contains("1Des")) {
        message = message.replace("1Des", "~");
        String[] replace = message.split("(?<=[~])");
        for (String r : replace) {
          if (r.startsWith("(") && r.contains(")")) {
            String replaceThis = r.substring(r.indexOf('(') + 1, r.indexOf(')'));
            String messageAll = r.substring(r.indexOf('('), r.indexOf(')') + 1);
            EncryptUtility EU = null;
            EU = new EncryptUtility("0404040404040404");
            String padding = "FFFFFFFFFFFFFFFF";
            String h = EU.encrypt(StringUtils.substring(replaceThis + padding, 0, 16)).toLowerCase();
            r = r.replaceAll(messageAll, h);
          } 
          req1Des = req1Des + r;
        } 
        trx.setReqMessage(req1Des.replace("~", "").replaceAll("\\(", "").replaceAll("\\)", ""));
        ctx.put("TRX", trx);
      } else {
        trx.setReqMessage(message);
        ctx.put("TRX", trx);
      } 
    } catch (JSONException|NullPointerException ex) {
      le.addMessage(ex);
    } catch (Exception ex) {
      Logger.getLogger(RequestParser.class.getName()).log(Level.SEVERE, (String)null, ex);
    } 
  }
  
  public void abort(long l, Serializable srlzbl) {}
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
