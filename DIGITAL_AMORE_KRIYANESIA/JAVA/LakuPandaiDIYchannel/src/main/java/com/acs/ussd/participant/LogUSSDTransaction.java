package com.acs.ussd.participant;

import com.acs.ussd.entity.Registration;
import com.acs.ussd.entity.Session;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.spring.SpringImpl;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONObject;

public class LogUSSDTransaction implements AbortParticipant {
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    Session sesi = (Session)ctx.get("SESSION");
    String freeText = sesi.getFreeText();
    JSONObject ft = new JSONObject(freeText);
    if (ft.has("pc") && 
      ft.getString("pc").equals("100000")) {
      Registration reg = new Registration();
      reg.setMsisdnNasabah(ft.getString("lakupandai_jatim_aktivasi_nohp"));
      reg.setNama(ft.getString("lakupandai_jatim_aktivasi_nama"));
      reg.setMsisdnAgen(ft.getString("msisdn"));
      reg.setDc("USSD-LAKU-PANDAI");
      reg.setBankCode(trx.getClient().getClientCode());
      reg.setProvider(trx.getProvider());
      reg.setReff(trx.getClientReff());
      reg.setRc(trx.getClientRc());
      reg.setRm(trx.getMsgContent());
      reg.setFreeText(trx.getReqMessage());
      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      try {
        reg.setDtm(df.parse(df.format(new Date())));
      } catch (ParseException ex) {
        Logger.getLogger(LogUSSDTransaction.class.getName()).log(Level.SEVERE, (String)null, ex);
      } 
      SpringImpl.getRegDao().save(reg);
    } 
    trx.setTrxInfo(ctx.getString("INFOTRX"));
    if (trx.getSendingTime() != null && trx.getRequestTime() != null)
      trx.setElapsed(Long.valueOf(trx.getSendingTime().getTime() - trx.getRequestTime().getTime())); 
    try {
      SpringImpl.getTransactionDao().save(trx);
    } catch (NullPointerException npe) {
      StringWriter stringWriter = new StringWriter();
      PrintWriter printWriter = new PrintWriter(stringWriter);
      npe.printStackTrace(printWriter);
      this.log.error(stringWriter.toString());
    } 
    LogEvent le = (LogEvent)ctx.get("LE");
    this.log.info(le);
  }
  
  public void abort(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    trx.setTrxInfo(ctx.getString("INFOTRX"));
    if (trx.getSendingTime() != null && trx.getRequestTime() != null)
      trx.setElapsed(Long.valueOf(trx.getSendingTime().getTime() - trx.getRequestTime().getTime())); 
    SpringImpl.getTransactionDao().save(trx);
    LogEvent le = (LogEvent)ctx.get("LE");
    this.log.info(le);
  }
  
  public int prepareForAbort(long l, Serializable srlzbl) {
    return 0;
  }
}
