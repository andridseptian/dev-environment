package com.acs.ussd.util;

public class Constants {
  public static String RC = "RC";
  
  public static String RM = "RM";
  
  public static String TXLOG = "TXLOG";
  
  public static String IN = "IN";
  
  public static String OUT = "OUT";
  
  public static String UUID = "UUID";
  
  public static String PROVIDER = "PROVIDER";
  
  public static String PROVIDER_REFF = "PROVIDER_REFF";
  
  public static String REQ = "REQ";
  
  public static String RSP = "RSP";
  
  public static String MAP = "MAP";
  
  public static String DB = "DB";
  
  public static String HE = "HE";
  
  public static String TIMEOUT = "50";
  
  public static int FAILED = 0;
  
  public static int SUCCESS = 1;
  
  public static int PENDING = 2;
  
  public static String APPROVED = "00";
  
  public static String MSISDN = "CUST_MSISDN";
  
  public static String TIME_OUT = "50";
  
  public static String MSG_DATE = "MSG_DATE";
  
  public static String TRX_ID = "TRX_ID";
  
  public static String ACCOUNT = "ACCOUNT";
  
  public static String SMS = "SMS";
  
  public static String AMOUNT = "amount";
  
  public static String SOURCE = "source";
  
  public static String COMMAND = "CMD";
  
  public static String DC = "DC";
  
  public static String ORIGIN = "ORIGIN";
}
