package com.acs.ussd.dao;

import com.acs.ussd.entity.Menu;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("menuDao")
@Transactional
public class MenuDao {
  @Resource(name = "sharedEntityManager")
  private EntityManager em1;
  
  public List<Menu> find() {
    try {
      return this.em1.createQuery("SELECT m FROM Menu AS m ORDER BY m.index")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public Menu findByKey(String clientCode, String key) {
    try {
      return (Menu)this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.clientCode =:clientCode AND m.menuName =:key ORDER BY m.index")
        .setParameter("clientCode", clientCode)
        .setParameter("key", key)
        .setMaxResults(1)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Menu> findByParentId(Long parentId) {
    try {
      return this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.parentId = :parentId ORDER BY m.index")
        .setParameter("parentId", parentId)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Menu> findByParentIdByLength(Long parentId, String clientCode, int Limit) {
    try {
      return this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.clientCode =:clientCode AND m.parentId = :parentId ORDER BY m.id")
        .setParameter("parentId", parentId)
        .setParameter("clientCode", clientCode)
        .setMaxResults(Limit)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public Menu findByGroupAndIndex(String clientCode, String groupName, String index) {
    try {
      return (Menu)this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.groupMenu = :groupName and m.index=:index and m.clientCode =:clientCode ORDER BY m.index")
        .setParameter("groupName", groupName)
        .setParameter("index", index)
        .setParameter("clientCode", clientCode)
        .setMaxResults(1)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public Menu findById(Long id) {
    try {
      return (Menu)this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.id = :id ORDER BY m.index")
        .setParameter("id", id)
        .setMaxResults(1)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Menu> findMenu(String clientCode, Menu menu) {
    try {
      return this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.clientCode =:clientCode m.hierarchyLevel = :lv AND m.parentId = :parentId")
        .setParameter("lv", Integer.valueOf(menu.getHierarchyLevel()))
        .setParameter("parentId", menu.getParentId())
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public Menu findChild(String clientCode, Menu menu) {
    try {
      Menu hasil = (Menu)this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.clientCode =:clientCode AND m.parentId =:menuId ORDER BY m.id").setParameter("menuId", menu.getId()).setParameter("clientCode", clientCode).setMaxResults(1).getSingleResult();
      if (hasil == null) {
        Menu header = Menu.getHeader(findByClientAndGroup(clientCode, menu.getGroupMenu()));
        return findChild(clientCode, header);
      } 
      return hasil;
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public Menu findParent(String clientCode, Menu menu) {
    try {
      return (Menu)this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.id = :menuId and m.clientCode =:clientCode ORDER BY m.id")
        .setParameter("menuId", menu.getParentId())
        .setParameter("clientCode", clientCode)
        .setMaxResults(1)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Menu> findByClientAndGroup(String clientCode, String groupName) {
    try {
      return this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.groupMenu = :groupName AND m.clientCode =:clientCode ORDER BY m.id")
        .setParameter("groupName", groupName)
        .setParameter("clientCode", clientCode)
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
  
  public List<Menu> findByClientGroupType(String clientCode, String groupName) {
    try {
      return this.em1.createQuery("SELECT m FROM Menu AS m WHERE m.groupMenu like :groupName AND m.clientCode =:clientCode AND m.addToSession like :tipe ORDER BY m.id")
        .setParameter("groupName", "%" + groupName + "%")
        .setParameter("clientCode", clientCode)
        .setParameter("tipe", "%denom%")
        .getResultList();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}
