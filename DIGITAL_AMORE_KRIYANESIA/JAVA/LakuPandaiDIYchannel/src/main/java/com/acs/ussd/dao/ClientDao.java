package com.acs.ussd.dao;

import com.acs.ussd.entity.Client;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("clientDao")
@Transactional
public class ClientDao {
  @Resource(name = "sharedEntityManager")
  private EntityManager em1;
  
  public Client find(String clientCode) {
    try {
      return (Client)this.em1.createQuery("SELECT c FROM Client AS c WHERE c.clientCode = :clientCode")
        .setParameter("clientCode", clientCode)
        .getSingleResult();
    } catch (NoResultException nre) {
      return null;
    } 
  }
}
