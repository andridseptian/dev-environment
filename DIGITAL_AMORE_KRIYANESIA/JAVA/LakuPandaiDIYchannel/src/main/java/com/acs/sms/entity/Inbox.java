package com.acs.sms.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "inbox")
public class Inbox implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "UpdatedInDB")
  private Date updatedInDB;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ReceivingDateTime")
  private Date receivingDateTime;
  
  @Column(name = "Text")
  @Type(type = "text")
  private String text;
  
  @Column(name = "SenderNumber", length = 20)
  private String senderNumber;
  
  @Column(name = "UDH")
  @Type(type = "text")
  private String udh;
  
  @Column(name = "SMSCNumber", length = 20)
  private String smscNumber;
  
  @Column(name = "TextDecoded")
  @Type(type = "text")
  private String textDecoded;
  
  @Column(name = "RecipientID")
  @Type(type = "text")
  private String recipientID;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Date getUpdatedInDB() {
    return this.updatedInDB;
  }
  
  public void setUpdatedInDB(Date updatedInDB) {
    this.updatedInDB = updatedInDB;
  }
  
  public Date getReceivingDateTime() {
    return this.receivingDateTime;
  }
  
  public void setReceivingDateTime(Date receivingDateTime) {
    this.receivingDateTime = receivingDateTime;
  }
  
  public String getText() {
    return this.text;
  }
  
  public void setText(String text) {
    this.text = text;
  }
  
  public String getSenderNumber() {
    return this.senderNumber;
  }
  
  public void setSenderNumber(String senderNumber) {
    this.senderNumber = senderNumber;
  }
  
  public String getUdh() {
    return this.udh;
  }
  
  public void setUdh(String udh) {
    this.udh = udh;
  }
  
  public String getSmscNumber() {
    return this.smscNumber;
  }
  
  public void setSmscNumber(String smscNumber) {
    this.smscNumber = smscNumber;
  }
  
  public String getTextDecoded() {
    return this.textDecoded;
  }
  
  public void setTextDecoded(String textDecoded) {
    this.textDecoded = textDecoded;
  }
  
  public String getRecipientID() {
    return this.recipientID;
  }
  
  public void setRecipientID(String recipientID) {
    this.recipientID = recipientID;
  }
}
