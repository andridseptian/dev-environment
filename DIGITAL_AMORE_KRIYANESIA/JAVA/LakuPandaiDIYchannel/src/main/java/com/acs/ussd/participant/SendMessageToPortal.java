package com.acs.ussd.participant;

import com.acs.http.HttpChannelMsg;
import com.acs.http.HttpSender;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;

public class SendMessageToPortal implements TransactionParticipant, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    String telco = trx.getProvider();
    String msisdn = trx.getMsisdn();
    String sms = trx.getMsgContent();
    String bankCode = trx.getClient().getClientCode();
    String transId = trx.getStan();
    String transDate = Constant.FORMAT_BANK_DATE.format(new Date());
    String noSid = this.cfg.get("sid");
    String destination = this.cfg.get("destination", "telcoportal");
    String additionalUrl = this.cfg.get("additionalUrl", "/MTRequestUSSDBPRKS");
    int timeout = this.cfg.getInt("timeout", 30000);
    try {
      if (msisdn.startsWith("620")) {
        msisdn = "62" + msisdn.substring(3);
      } else if (msisdn.startsWith("0")) {
        msisdn = "62" + msisdn.substring(1);
      } 
      String reqParam = "telco=" + URLEncoder.encode(telco, "UTF-8") + "&msisdn=" + URLEncoder.encode(msisdn, "UTF-8") + "&sms=" + URLEncoder.encode(sms, "UTF-8") + "&bank_code=" + URLEncoder.encode(bankCode, "UTF-8") + "&trans_id=" + URLEncoder.encode(transId, "UTF-8") + "&trans_date=" + URLEncoder.encode(transDate, "UTF-8") + "&no_sid=" + URLEncoder.encode(noSid, "UTF-8");
      HttpChannelMsg req = new HttpChannelMsg();
      req.setUrlParam(reqParam);
      HttpChannelMsg rsp = HttpSender.request(destination, req, additionalUrl, "get", timeout);
      if (rsp.getTxStatusCode() == Constant.SUCCESS) {
        this.log.info(rsp.getContent());
      } else {
        this.log.info(rsp.getContent());
      } 
    } catch (UnsupportedEncodingException ex) {
      Logger.getLogger(SendMessageToPortal.class.getName()).log(Level.SEVERE, (String)null, ex);
    } catch (org.jpos.util.NameRegistrar.NotFoundException ex) {
      Logger.getLogger(SendMessageToPortal.class.getName()).log(Level.SEVERE, (String)null, (Throwable)ex);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SendMessageToPortal.class.getName()).log(Level.SEVERE, (String)null, ex);
    } 
  }
  
  public void abort(long l, Serializable srlzbl) {}
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
