package com.acs.ussd.participant;

import com.acs.ussd.entity.SessionApp;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.spring.SpringImpl;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;

public class SendResponse implements AbortParticipant, Configurable {
  private Configuration cfg;
  
  Log log = Log.getLog("Q2", getClass().getName());
  
  public int prepare(long l, Serializable srlzbl) {
    return 1;
  }
  
  public void commit(long l, Serializable srlzbl) {
    Space sp = SpaceFactory.getSpace();
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    String action = ctx.getString("SMS");
    ctx.put("TRX", trx);
    ctx.put(Constant.RC, trx.getClientRc());
    ctx.put(Constant.RM, trx.getMsgContent());
    if ("login".equals(action) && "00".equals(trx.getClientRc())) {
      SessionApp ambilWaktu = SpringImpl.getSessionAppDao().findLastLogin(trx.getMsisdn());
      if (ambilWaktu != null)
        ctx.put("LAST_LOGIN", ambilWaktu.getDtm().toString()); 
    } 
    String queueKey = trx.getQueueKey();
    if (ctx.get("PROVIDER") != null && ctx
      .getString("PROVIDER").equals("XL")) {
      String session = ctx.getString("SESSION");
      String msisdn = ctx.getString("MSISDN");
      queueKey = msisdn + session;
    } 
    String response = Constant.RSP + queueKey;
    this.log.info("Trying put to : " + response);
    sp.out(response, ctx, 60000L);
  }
  
  public void abort(long l, Serializable srlzbl) {
    Space sp = SpaceFactory.getSpace();
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    ctx.put("TRX", trx);
    String queueKey = trx.getQueueKey();
    String response = Constant.RSP + queueKey;
    this.log.info("Trying put to : " + response);
    sp.out(response, ctx, 60000L);
  }
  
  public int prepareForAbort(long l, Serializable srlzbl) {
    Context ctx = (Context)srlzbl;
    Trx trx = (Trx)ctx.get("TRX");
    LogEvent le = (LogEvent)ctx.get("LE");
    le.addMessage(ctx.getString("INFOTRX"));
    String message = this.cfg.get("error_message", "Maaf, saat ini menu sedang tidak dapat diakses");
    trx.setMsgContent(message);
    return 0;
  }
  
  public void setConfiguration(Configuration c) throws ConfigurationException {
    this.cfg = c;
  }
}
