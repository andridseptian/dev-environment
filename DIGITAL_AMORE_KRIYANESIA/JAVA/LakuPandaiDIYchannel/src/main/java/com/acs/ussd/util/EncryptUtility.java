package com.acs.ussd.util;

import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import org.jpos.iso.ISOUtil;

public class EncryptUtility {
  public static final String DES_ENCRYPTION_SCHEME = "DES";
  
  private KeySpec myKeySpec;
  
  private SecretKeyFactory mySecretKeyFactory;
  
  private Cipher cipher;
  
  byte[] keyAsBytes;
  
  private byte[] myEncryptionKey;
  
  SecretKey key;
  
  public EncryptUtility(String param) throws Exception {
    this.myEncryptionKey = ISOUtil.hex2byte(param);
    this.myKeySpec = new DESKeySpec(this.myEncryptionKey);
    this.mySecretKeyFactory = SecretKeyFactory.getInstance("DES");
    this.cipher = Cipher.getInstance("DES/ECB/NoPadding");
    this.key = this.mySecretKeyFactory.generateSecret(this.myKeySpec);
  }
  
  public String encrypt(String unencryptedString) {
    String encryptedString = null;
    try {
      this.cipher.init(1, this.key);
      byte[] plainText = ISOUtil.hex2byte(unencryptedString);
      byte[] encryptedText = this.cipher.doFinal(plainText);
      encryptedString = ISOUtil.hexString(encryptedText);
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return encryptedString;
  }
  
  public String decrypt(String encryptedString) {
    String decryptedText = null;
    try {
      this.cipher.init(2, this.key);
      byte[] plainText = this.cipher.doFinal(ISOUtil.hex2byte(encryptedString));
      decryptedText = ISOUtil.hexString(plainText);
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return decryptedText;
  }
}
