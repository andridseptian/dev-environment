package com.acs.ussd.web;

import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.parser.XMLParser;
import java.util.List;
import org.jpos.util.Log;

public class TSELParser extends XMLParser {
  Log log = Log.getLog("Q2", getClass().getName());
  
  final String mark_header = "[HEADER]";
  
  final String mark_startLooping = "[START_LOOPING]";
  
  final String mark_endLooping = "[END_LOOPING]";
  
  final String mark_index = "[INDEX]";
  
  final String mark_displayName = "[DISPLAY_NAME]";
  
  final String mark_info = "[INFO]";
  
  final String mark_backCode = "[BACK_CODE]";
  
  final String mark_totalMenu = "[TOTAL_MENU]";
  
  final String PATH_INFO;
  
  final String PATH_MENU;
  
  final String PATH_INPUT;
  
  final String DefaultResponse = "Maaf session telah habis / menu tidak tersedia";
  
  private String response;
  
  public TSELParser(RequestMapping req) {
    this.PATH_INFO = req.getXmlPathInfo();
    this.PATH_INPUT = req.getXmlPathInput();
    this.PATH_MENU = req.getXmlPathMenu();
  }
  
  public String generateMenu(List<Menu> listMenu) {
    this.response = XMLParser.getXMLByLocation(this.PATH_MENU);
    if (this.response.contains("[HEADER]"))
      this.response = XMLParser.replace(this.response, "[HEADER]", Menu.getHeader(listMenu).getDisplayName()); 
    if (this.response.contains("[TOTAL_MENU]"))
      this.response = XMLParser.replace(this.response, "[TOTAL_MENU]", String.valueOf(listMenu.size())); 
    this.response = loopingProcess(listMenu, this.response, "[START_LOOPING]", "[END_LOOPING]");
    return this.response;
  }
  
  public String generateInfo(Menu menu) {
    this.response = XMLParser.getXMLByLocation(this.PATH_INFO);
    if (this.response.contains("[INFO]"))
      this.response = XMLParser.replace(this.response, "[INFO]", menu.getDisplayName()); 
    if (this.response.contains("[BACK_CODE]"))
      this.response = XMLParser.replace(this.response, "[BACK_CODE]", Constant.DEFAULT_BACK_CODE); 
    return this.response;
  }
  
  public String generateInput(Menu menu) {
    this.response = XMLParser.getXMLByLocation(this.PATH_INPUT);
    if (this.response.contains("[INFO]"))
      this.response = XMLParser.replace(this.response, "[INFO]", menu.getDisplayName()); 
    if (this.response.contains("[BACK_CODE]"))
      this.response = XMLParser.replace(this.response, "[BACK_CODE]", Constant.DEFAULT_BACK_CODE); 
    return this.response;
  }
  
  public String generateDefaultResponse() {
    this.response = XMLParser.getXMLByLocation(this.PATH_INFO);
    if (this.response.contains("[INFO]"))
      this.response = XMLParser.replace(this.response, "[INFO]", "Maaf session telah habis / menu tidak tersedia"); 
    if (this.response.contains("[BACK_CODE]"))
      this.response = XMLParser.replace(this.response, "[BACK_CODE]", Constant.DEFAULT_BACK_CODE); 
    return this.response;
  }
  
  public String loopingProcess(List<Menu> listMenu, String messageLooping, String mark_start, String mark_end) {
    String messageText = messageLooping;
    String loopingText = getLoopingMessage(messageLooping, mark_start, mark_end);
    String responseText = "";
    String messageTotalLooping = "";
    for (Menu m : listMenu) {
      if (m.getIndex() != null) {
        responseText = loopingText;
        if (responseText.contains("[DISPLAY_NAME]"))
          responseText = XMLParser.replace(responseText, "[DISPLAY_NAME]", m.getDisplayName()); 
        if (responseText.contains("[INDEX]"))
          responseText = XMLParser.replace(responseText, "[INDEX]", m.getIndex()); 
      } else {
        responseText = responseText.trim();
      } 
      messageTotalLooping = messageTotalLooping + responseText;
    } 
    responseText = messageText.replace(loopingText, messageTotalLooping).replace(mark_start, "").replace(mark_end, "");
    return responseText;
  }
}
