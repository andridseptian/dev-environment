package com.acs.ussd.entity;

import com.acs.ussd.entity.enums.MenuType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "menu_ussd")
public class Menu implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "menu_ussd_id_seq")
  @SequenceGenerator(name = "menu_ussd_id_seq", sequenceName = "menu_ussd_id_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "menu_name")
  private String menuName;
  
  @Column(name = "parent_id")
  private Long parentId;
  
  @Column(name = "type", nullable = false)
  @Enumerated(EnumType.STRING)
  private MenuType type;
  
  @Column(name = "client_code")
  private String clientCode;
  
  @Column(name = "display_name")
  private String displayName;
  
  @Column(name = "hierarchy_level")
  private int hierarchyLevel;
  
  @Column(name = "index")
  private String index;
  
  @Column(name = "group_menu")
  private String groupMenu;
  
  @Column(name = "add_to_session")
  private String addToSession;
  
  @Column(name = "trx_message")
  @Type(type = "text")
  private String trxMessage;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getMenuName() {
    return this.menuName;
  }
  
  public void setMenuName(String menuName) {
    this.menuName = menuName;
  }
  
  public Long getParentId() {
    return this.parentId;
  }
  
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }
  
  public MenuType getType() {
    return this.type;
  }
  
  public void setType(MenuType type) {
    this.type = type;
  }
  
  public String getClientCode() {
    return this.clientCode;
  }
  
  public void setClientCode(String clientCode) {
    this.clientCode = clientCode;
  }
  
  public String getDisplayName() {
    return this.displayName;
  }
  
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
  
  public int getHierarchyLevel() {
    return this.hierarchyLevel;
  }
  
  public void setHierarchyLevel(int hierarchyLevel) {
    this.hierarchyLevel = hierarchyLevel;
  }
  
  public String getIndex() {
    return this.index;
  }
  
  public void setIndex(String index) {
    this.index = index;
  }
  
  public static Menu getHeader(List<Menu> listMenu) {
    Menu headerMenu = new Menu();
    for (Menu menu : listMenu) {
      if (menu.getType() == MenuType.MENU_HEADER) {
        headerMenu = menu;
        return menu;
      } 
    } 
    headerMenu.setDisplayName("Header Not Found");
    return headerMenu;
  }
  
  public String getGroupMenu() {
    return this.groupMenu;
  }
  
  public void setGroupMenu(String group_menu) {
    this.groupMenu = group_menu;
  }
  
  public String getTrxMessage() {
    return this.trxMessage;
  }
  
  public void setTrxMessage(String trxMessage) {
    this.trxMessage = trxMessage;
  }
  
  public String getAddToSession() {
    return this.addToSession;
  }
  
  public void setAddToSession(String addToSession) {
    this.addToSession = addToSession;
  }
}
