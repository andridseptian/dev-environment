package com.acs.ussd.dao;

import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.Session;
import com.acs.ussd.entity.enums.SessionType;
import java.util.Date;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("sessionDao")
@Transactional
public class SessionDao {
  Log log = Log.getLog("Q2", getClass().getName());
  
  LogEvent le;
  
  @Resource(name = "sharedEntityManager")
  private EntityManager em1;
  
  public Session findSession(String msisdn, String clientCode, String messageId, String provider) {
    try {
      Session session = (Session)this.em1.createQuery("SELECT s FROM Session AS s WHERE s.msisdn = :msisdn and s.clientCode = :clientCode AND s.msgId = :messageId AND s.provider = :provider ORDER BY s.crDate DESC").setParameter("msisdn", msisdn).setParameter("clientCode", clientCode).setParameter("messageId", messageId).setParameter("provider", provider.toUpperCase()).setMaxResults(1).getSingleResult();
      return session;
    } catch (NoResultException nre) {
      return null;
    } catch (Exception ex) {
      this.le = new LogEvent(getClass().getName());
      this.le.addMessage("Error find : " + msisdn);
      this.le.addMessage(ex);
      this.log.error(this.le);
      return null;
    } 
  }
  
  @Transactional
  public void save(Session session, RequestMapping req) {
    try {
      if (session.getId() == null) {
        Date tanggal = new Date();
        session.setCrDate(tanggal);
        session.setActive(Boolean.TRUE);
        session.setExpDate(new Date(tanggal.getTime() + req.getExpTime().longValue()));
        this.em1.persist(session);
      } else {
        if (session.isActive().booleanValue())
          switch (req.getSessionType()) {
            case REFRESH_TIME_LIMIT:
              if ((new Date()).before(session.getExpDate())) {
                session.setExpDate(new Date((new Date()).getTime() + req.getExpTime().longValue()));
                break;
              } 
              session.setActive(Boolean.FALSE);
              break;
            case TIME_LIMIT:
              session.setActive(Boolean.valueOf(session.getExpDate().after(new Date())));
              break;
            case NO_LIMIT:
              session.setActive(Boolean.TRUE);
              break;
            default:
              session.setActive(Boolean.valueOf(session.getExpDate().after(new Date())));
              break;
          }  
        this.em1.merge(session);
      } 
    } catch (Exception ex) {
      this.le = new LogEvent(getClass().getName());
      this.le.addMessage("Cannot save : " + session.getMsisdn());
      this.le.addMessage(ex);
      this.log.error(this.le);
    } 
  }
  
  @Transactional
  public void setInactive(Session session) {
    try {
      session.setActive(Boolean.valueOf(false));
      this.em1.merge(session);
    } catch (Exception ex) {
      this.le = new LogEvent(getClass().getName());
      this.le.addMessage("Cannot update : " + session.getMsisdn());
      this.le.addMessage(ex);
      this.log.error(this.le);
    } 
  }
}
