package com.acs.sms.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "outbox")
public class Outbox implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "UpdatedInDB")
  private Date updatedInDB;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "InsertIntoDB")
  private Date insertIntoDB;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "SendingDateTime")
  private Date sendingDateTime;
  
  @Temporal(TemporalType.TIME)
  @Column(name = "SendBefore")
  private Date sendBefore;
  
  @Temporal(TemporalType.TIME)
  @Column(name = "SendAfter")
  private Date sendAfter;
  
  @Column(name = "Text")
  @Type(type = "text")
  private String text;
  
  @Column(name = "DestinationNumber", length = 20)
  private String destinationNumber;
  
  @Column(name = "Coding", length = 50)
  private String coding;
  
  @Column(name = "UDH")
  @Type(type = "text")
  private String udh;
  
  @Column(name = "Class")
  private Integer clazz;
  
  @Column(name = "TextDecoded")
  @Type(type = "text")
  private String textDecoded;
  
  @Column(name = "RelativeValidity")
  private Integer relativeValidity;
  
  @Column(name = "SenderID", length = 255)
  private String senderId;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "SendingTimeOut")
  private Date sendingTimeOut;
  
  @Column(name = "DeliveryReport", length = 10)
  private String deliveryReport;
  
  @Column(name = "CreatorID")
  @Type(type = "text")
  private String creatorId;
  
  public Outbox() {
    Date currentTimestamp = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    String sendBeforeTime = "23:59:59";
    String sendAfterTime = "00:00:00";
    this.updatedInDB = currentTimestamp;
    this.insertIntoDB = currentTimestamp;
    this.sendingDateTime = currentTimestamp;
    try {
      this.sendBefore = sdf.parse(sendBeforeTime);
      this.sendAfter = sdf.parse(sendAfterTime);
    } catch (ParseException ex) {
      Logger.getLogger(Outbox.class.getName()).log((Priority)Level.ERROR, null, ex);
    } 
    this.coding = "Default_No_Compression";
    this.deliveryReport = "default";
    this.clazz = Integer.valueOf(-1);
    this.relativeValidity = Integer.valueOf(-1);
    this.sendingTimeOut = currentTimestamp;
    this.textDecoded = "";
    this.creatorId = "default-sender";
  }
  
  public Date getUpdatedInDB() {
    return this.updatedInDB;
  }
  
  public void setUpdatedInDB(Date updatedInDB) {
    this.updatedInDB = updatedInDB;
  }
  
  public Date getInsertIntoDB() {
    return this.insertIntoDB;
  }
  
  public void setInsertIntoDB(Date insertIntoDB) {
    this.insertIntoDB = insertIntoDB;
  }
  
  public Date getSendingDateTime() {
    return this.sendingDateTime;
  }
  
  public void setSendingDateTime(Date sendingDateTime) {
    this.sendingDateTime = sendingDateTime;
  }
  
  public Date getSendBefore() {
    return this.sendBefore;
  }
  
  public void setSendBefore(Date sendBefore) {
    this.sendBefore = sendBefore;
  }
  
  public Date getSendAfter() {
    return this.sendAfter;
  }
  
  public void setSendAfter(Date sendAfter) {
    this.sendAfter = sendAfter;
  }
  
  public String getText() {
    return this.text;
  }
  
  public void setText(String text) {
    this.text = text;
  }
  
  public String getDestinationNumber() {
    return this.destinationNumber;
  }
  
  public void setDestinationNumber(String destinationNumber) {
    this.destinationNumber = destinationNumber;
  }
  
  public String getCoding() {
    return this.coding;
  }
  
  public void setCoding(String coding) {
    this.coding = coding;
  }
  
  public String getUdh() {
    return this.udh;
  }
  
  public void setUdh(String udh) {
    this.udh = udh;
  }
  
  public int getClazz() {
    return this.clazz.intValue();
  }
  
  public void setClazz(int clazz) {
    this.clazz = Integer.valueOf(clazz);
  }
  
  public String getTextDecoded() {
    return this.textDecoded;
  }
  
  public void setTextDecoded(String textDecoded) {
    this.textDecoded = textDecoded;
  }
  
  public Integer getRelativeValidity() {
    return this.relativeValidity;
  }
  
  public void setRelativeValidity(Integer relativeValidity) {
    this.relativeValidity = relativeValidity;
  }
  
  public String getSenderId() {
    return this.senderId;
  }
  
  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }
  
  public Date getSendingTimeOut() {
    return this.sendingTimeOut;
  }
  
  public void setSendingTimeOut(Date sendingTimeOut) {
    this.sendingTimeOut = sendingTimeOut;
  }
  
  public String getDeliveryReport() {
    return this.deliveryReport;
  }
  
  public void setDeliveryReport(String deliveryReport) {
    this.deliveryReport = deliveryReport;
  }
  
  public String getCreatorId() {
    return this.creatorId;
  }
  
  public void setCreatorId(String creatorId) {
    this.creatorId = creatorId;
  }
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
}
