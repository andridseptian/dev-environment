package com.acs.ussd.parser;

import com.acs.ussd.entity.Menu;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class XMLParser {
  static Logger log = LoggerFactory.getLogger(XMLParser.class);
  
  public abstract String generateMenu(List<Menu> paramList);
  
  public abstract String generateInfo(Menu paramMenu);
  
  public abstract String generateInput(Menu paramMenu);
  
  public abstract String generateDefaultResponse();
  
  public static String getXMLByLocation(String path) {
    File file = new File(path);
    try {
      return FileUtils.readFileToString(file);
    } catch (IOException ex) {
      log.info(path + "[" + file.getAbsolutePath() + "] not found");
      return null;
    } 
  }
  
  public static String replace(String message, String mark, String text) {
    return message.replace(mark, text);
  }
  
  public static String getLoopingMessage(String message, String mark_start, String mark_end) {
    int awal = message.indexOf(mark_start);
    int akhir = message.indexOf(mark_end);
    if (awal != -1 && akhir != -1)
      return message.substring(awal, akhir).replace(mark_start, "").trim(); 
    return message;
  }
  
  public abstract String loopingProcess(List<Menu> paramList, String paramString1, String paramString2, String paramString3);
}
