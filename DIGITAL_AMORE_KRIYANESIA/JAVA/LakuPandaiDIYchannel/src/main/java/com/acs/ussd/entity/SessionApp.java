package com.acs.ussd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "session_app")
public class SessionApp implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "session_app_seq")
  @SequenceGenerator(name = "session_app_seq", sequenceName = "session_app_seq")
  @Column(name = "id")
  private Long id;
  
  @Column(name = "bank_code", length = 10)
  private String bankCode;
  
  @Column(name = "msisdn_agen", length = 20)
  private String msisdnAgen;
  
  @Column(name = "action", length = 255)
  private String action;
  
  @Column(name = "reff", length = 100)
  private String reff;
  
  @Column(name = "status", length = 255)
  private String status;
  
  @Column(name = "datetime", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtm;
  
  @Column(name = "dc", length = 20)
  private String dc;
  
  @Column(name = "pc", length = 10)
  private String pc;
  
  @Column(name = "req_msg")
  @Type(type = "text")
  private String req_msg;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getBankCode() {
    return this.bankCode;
  }
  
  public void setBankCode(String bankCode) {
    this.bankCode = bankCode;
  }
  
  public String getMsisdnAgen() {
    return this.msisdnAgen;
  }
  
  public void setMsisdnAgen(String msisdnAgen) {
    this.msisdnAgen = msisdnAgen;
  }
  
  public String getAction() {
    return this.action;
  }
  
  public void setAction(String action) {
    this.action = action;
  }
  
  public String getReff() {
    return this.reff;
  }
  
  public void setReff(String reff) {
    this.reff = reff;
  }
  
  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public Date getDtm() {
    return this.dtm;
  }
  
  public void setDtm(Date dtm) {
    this.dtm = dtm;
  }
  
  public String getDc() {
    return this.dc;
  }
  
  public void setDc(String dc) {
    this.dc = dc;
  }
  
  public String getPc() {
    return this.pc;
  }
  
  public void setPc(String pc) {
    this.pc = pc;
  }
  
  public String getReq_msg() {
    return this.req_msg;
  }
  
  public void setReq_msg(String req_msg) {
    this.req_msg = req_msg;
  }
}
