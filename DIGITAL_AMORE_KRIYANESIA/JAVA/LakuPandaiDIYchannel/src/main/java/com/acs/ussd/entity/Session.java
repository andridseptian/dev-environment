package com.acs.ussd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

@Entity
@Table(name = "session")
public class Session implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "session_seq")
  @SequenceGenerator(name = "session_seq", sequenceName = "session_seq")
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "client_code")
  private String clientCode;
  
  @Column(name = "client_reff")
  private String clientReff;
  
  @Column(name = "msisdn", length = 50)
  private String msisdn;
  
  @Column(name = "cr_date", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date crDate;
  
  @Column(name = "exp_date", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date expDate;
  
  @Column(name = "last_position")
  private Long lastMenuId;
  
  @Column(name = "active")
  private Boolean active;
  
  @Column(name = "free_text")
  @Type(type = "text")
  private String freeText;
  
  @Column(name = "user_input")
  @Type(type = "text")
  private String userInput;
  
  @Column(name = "provider")
  private String provider;
  
  @Column(name = "msgId")
  private String msgId;
  
  public Session() {}
  
  public Session(String msisdn, String clientCode, String DefaultMenuGroup, String provider, String messageId) {
    this.msisdn = msisdn;
    this.clientCode = clientCode;
    this.freeText = (new JSONObject()).put("msisdn", msisdn).toString();
    this.provider = provider.toUpperCase();
    this.msgId = messageId;
  }
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getClientCode() {
    return this.clientCode;
  }
  
  public void setClientCode(String clientCode) {
    this.clientCode = clientCode;
  }
  
  public String getClientReff() {
    return this.clientReff;
  }
  
  public void setClientReff(String clientReff) {
    this.clientReff = clientReff;
  }
  
  public String getMsisdn() {
    return this.msisdn;
  }
  
  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
  
  public Date getCrDate() {
    return this.crDate;
  }
  
  public void setCrDate(Date crDate) {
    this.crDate = crDate;
  }
  
  public Date getExpDate() {
    return this.expDate;
  }
  
  public void setExpDate(Date expDate) {
    this.expDate = expDate;
  }
  
  public Long getLastMenuId() {
    return this.lastMenuId;
  }
  
  public void setLastMenuId(Long lastMenuId) {
    this.lastMenuId = lastMenuId;
  }
  
  public Boolean isActive() {
    return this.active;
  }
  
  public void setActive(Boolean active) {
    this.active = active;
  }
  
  public String getFreeText() {
    return this.freeText;
  }
  
  public void setFreeText(String freeText) {
    this.freeText = freeText;
  }
  
  public String getUserInput() {
    return this.userInput;
  }
  
  public void setUserInput(String userInput) {
    this.userInput = userInput;
  }
  
  public String getProvider() {
    return this.provider;
  }
  
  public void setProvider(String provider) {
    this.provider = provider;
  }
  
  public String getMsgId() {
    return this.msgId;
  }
  
  public void setMsgId(String msgId) {
    this.msgId = msgId;
  }
}
