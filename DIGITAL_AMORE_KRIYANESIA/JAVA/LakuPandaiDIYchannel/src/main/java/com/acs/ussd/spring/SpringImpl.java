package com.acs.ussd.spring;

import com.acs.spring.Spring;
import com.acs.ussd.dao.ClientDao;
import com.acs.ussd.dao.MenuDao;
import com.acs.ussd.dao.ParseXMLDao;
import com.acs.ussd.dao.PrefixDao;
import com.acs.ussd.dao.RegistrationDao;
import com.acs.ussd.dao.RequestMappingDao;
import com.acs.ussd.dao.SessionAppDao;
import com.acs.ussd.dao.SessionDao;
import com.acs.ussd.dao.TransactionDao;
import com.acs.ussd.service.LoggerService;
import org.jpos.core.ConfigurationException;

public class SpringImpl extends Spring {
  private static MenuDao menuDao;
  
  private static ClientDao clientDao;
  
  private static SessionDao sessionDao;
  
  private static RequestMappingDao requestMappingDao;
  
  private static LoggerService loggerService;
  
  private static TransactionDao transactionDao;
  
  private static ParseXMLDao parseXMLDao;
  
  private static RegistrationDao regDao;
  
  private static SessionAppDao sessionAppDao;
  
  private static PrefixDao prefixDao;
  
  public static RequestMappingDao getRequestMappingDao() {
    return requestMappingDao;
  }
  
  public static void setRequestMappingDao(RequestMappingDao requestMappingDao) {
    SpringImpl.requestMappingDao = requestMappingDao;
  }
  
  public static MenuDao getMenuDao() {
    return menuDao;
  }
  
  public static void setMenuDao(MenuDao menuDao) {
    SpringImpl.menuDao = menuDao;
  }
  
  public static SessionDao getSessionDao() {
    return sessionDao;
  }
  
  public static void setSessionDao(SessionDao sessionDao) {
    SpringImpl.sessionDao = sessionDao;
  }
  
  public static ClientDao getClientDao() {
    return clientDao;
  }
  
  public static void setClientDao(ClientDao clientDao) {
    SpringImpl.clientDao = clientDao;
  }
  
  public static LoggerService getLoggerService() {
    return loggerService;
  }
  
  public static void setLoggerService(LoggerService loggerService) {
    SpringImpl.loggerService = loggerService;
  }
  
  public static TransactionDao getTransactionDao() {
    return transactionDao;
  }
  
  public static void setTransactionDao(TransactionDao transactionDao) {
    SpringImpl.transactionDao = transactionDao;
  }
  
  public static ParseXMLDao getParseXMLDao() {
    return parseXMLDao;
  }
  
  public static void setParseXMLDao(ParseXMLDao parseXMLDao) {
    SpringImpl.parseXMLDao = parseXMLDao;
  }
  
  public static RegistrationDao getRegDao() {
    return regDao;
  }
  
  public static void setRegDao(RegistrationDao regDao) {
    SpringImpl.regDao = regDao;
  }
  
  public static SessionAppDao getSessionAppDao() {
    return sessionAppDao;
  }
  
  public static void setSessionAppDao(SessionAppDao sessionAppDao) {
    SpringImpl.sessionAppDao = sessionAppDao;
  }
  
  public static PrefixDao getPrefixDao() {
    return prefixDao;
  }
  
  public static void setPrefixDao(PrefixDao prefixDao) {
    SpringImpl.prefixDao = prefixDao;
  }
  
  public void initService() throws ConfigurationException {
    super.initService();
    setMenuDao((MenuDao)this.context.getBean("menuDao"));
    setSessionDao((SessionDao)this.context.getBean("sessionDao"));
    setClientDao((ClientDao)this.context.getBean("clientDao"));
    setRequestMappingDao((RequestMappingDao)this.context.getBean("requestMappingDao"));
    setLoggerService((LoggerService)this.context.getBean("loggerService"));
    setTransactionDao((TransactionDao)this.context.getBean("transactionDao"));
    setParseXMLDao((ParseXMLDao)this.context.getBean("parseXMLDao"));
    setRegDao((RegistrationDao)this.context.getBean("regDao"));
    setSessionAppDao((SessionAppDao)this.context.getBean("sessionAppDao"));
    setPrefixDao((PrefixDao)this.context.getBean("prefixDao"));
  }
}
