
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ROG
 */
public class testJSON {

    public static void main(String[] args) throws Exception {
        String data = "{\n"
                + "\"fee\":\"1500\",\n"
                + "\"accountType\":\"A\",\n"
                + "\"tax\":\"0\",\n"
                + "\"sourceAccountNumber\":\"001221000580\",\n"
                + "\"responseCode\":\"00\",\n"
                + "\"rrn\":\"12150410142020\",\n"
                + "\"agentFee\":\"0\",\n"
                + "\"transactionType\":\"2\",\n"
                + "\"responseDesc\":\"SUKSES\",\n"
                + "\"productCode\":\"013001\",\n"
                + "\"idBilling\":\"081812345001\",\n"
                + "\"jobIdInquiry\":\"56679812150410142020\",\n"
                + "\"transactionAmount\":\"1000000\",\n"
                + "\"additionalData\":\"{\\\"nama\\\":\\\"IDHAM DHIYAULHAQ HABIBI\\\",\\\"noHp\\\":\\\"\\\",\\\"jmlPembayaran\\\":10000}\",\n"
                + "\"channelId\":\"LP\"\n"
                + "}";

        JSONObject bankResponseDetail = new JSONObject(data);
        
        JSONObject addtionalData = new JSONObject(bankResponseDetail.getString("additionalData"));
        System.out.println(addtionalData.getString("nama"));
        
        
    }
}
