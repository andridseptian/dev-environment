package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.SettingRepository;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CustomerResetMsisdnParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final AgentRepository agentRepository;
    private final NasabahRepository nasabahRepository;
    private final SettingRepository settingRepository;

    public CustomerResetMsisdnParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
        nasabahRepository = context.getBean(NasabahRepository.class);
        settingRepository = context.getBean(SettingRepository.class);

    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String iResetMsisdn = dataRequest.getString(Constants.PARAM.RESET_MSISDN);
        String iMsisdn = Utility.formatMsisdn(iResetMsisdn);
        if (nasabah.getStatus() != Constants.Status.ACTIVE) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Status Agen harus aktif untuk melakukan reset nomor handphone");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        //check msisdn active
        MAgent checkAgent = agentRepository.findFirstByMsisdnOrderByCrTimeDesc(iMsisdn);
        if (checkAgent != null) {
            if (checkAgent.getStatus() == Constants.Status.ACTIVE) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar menjadi Agen (status aktif)");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            if (checkAgent.getStatus() == Constants.Status.BLOCK) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar menjadi Agen (status blokir)");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            if (checkAgent.getStatus() == Constants.Status.INACTIVE) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar menjadi Agen (status inactive)");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        }
        MNasabah checkNasabah = nasabahRepository.findFirstByMsisdnOrderByCrTimeDesc(iMsisdn);
        if (checkNasabah != null) {
            if (checkNasabah.getStatus() == Constants.Status.ACTIVE) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar menjadi Nasabah (status aktif)");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            if (checkNasabah.getStatus() == Constants.Status.BLOCK) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar menjadi Nasabah (status blokir)");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            if (checkNasabah.getStatus() == Constants.Status.INACTIVE) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar menjadi Nasabah (status inactive)");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        }
        nasabah.setResetMsisdn(iResetMsisdn);
        ctx.put(Constants.NASABAH, nasabah);
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        MNasabah saveNasabah = nasabahRepository.save(nasabah);
        if (saveNasabah == null) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Data Pengguna gagal disimpan, mohon hubungi admin untuk tindakan lebih lanjut");
            ctx.put(Constants.OBJ.WSREQUEST, req);
        } else {
            req.setRm("mohon untuk konfirmasi OTP yang dikirimkan ke nomor baru agen");
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            long timeout = Long.parseLong(settingRepository.findOne(KeyMap.TIMEOUT_OTP.name()).getValue());
            JSONObject resp = new JSONObject().put(Constants.PARAM.TIMEOUT, timeout).put(Constants.PARAM.MSISDN, saveNasabah.getResetMsisdn());
            ctx.put(Constants.OBJ.WSREQUEST, req);
            ctx.put(Constants.OBJ.RESP, resp);
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
