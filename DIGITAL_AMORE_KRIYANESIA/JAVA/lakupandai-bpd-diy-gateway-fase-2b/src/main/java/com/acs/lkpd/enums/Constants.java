/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.enums;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author ACS
 */
public class Constants {

    public static final DateTimeFormatter FORMAT_VIEW = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    public static final DateTimeFormatter FORMAT_DDMMYYYY = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public static final DateTimeFormatter FORMAT_YYYYMMDD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter FORMAT_PLAINYYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static final DateTimeFormatter FORMAT_MMDDHHMMSS = DateTimeFormatter.ofPattern("MMddHHmmss");
    public static final DateTimeFormatter FORMAT_DDMMYYYYHHMMSS = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    public static final DateTimeFormatter FORMAT_YYYYMMDDHHMMSS = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter FORMAT_HHMM = DateTimeFormatter.ofPattern("HHmm");
    
    public static final String HEADER_VALUE = "HEADER_VALUE";
    public static final String NOTIFIKASI_LOG = "NOTIFIKASI_LOG";
    public static final String ADDITIONALDATA = "ADDITIONALDATA";
    public static final String JUMLAHBULAN = "JUMLAHBULAN";
    
    public static final String FLD48 = "NOTIFIKASI_LOG";

    public class OBJ {

        public static final String SPRING = "spring";
        public static final String WSREQUEST = "wsRequest";
        public static final String HTTPREQUEST = "httpRequest";
        public static final String TRANSACTION = "transaction";
        public static final String INQTRX = "inqTrx";
        public static final String RESP = "resp";
        public static final String SERVICEPROXY = "serviceProxy";
        public static final String AGENT = "agent";
        public static final String NASABAH = "nasabah";
        public static final String SESSION = "session";
        public static final String BANKRESP = "bankresp";
        public static final String PRODUCTPAYMENT = "productPayment";
    }

    public class PARAM {

        public static final String ACCOUNT_NUMBER = "accountNumber";
        public static final String PHONE_NUMBER = "phoneNumber";
        public static final String RESET_MSISDN = "resetMsisdn";
        public static final String MSISDN = "msisdn";
        public static final String USER_ID = "userId";
        public static final String CUSTOMER_ID = "customerId";
        public static final String PIN = "pin";
        public static final String WS_RESPONSECODE = "responseCode";
        public static final String WS_RESPONSEDESC = "responseDesc";
        public static final String WS_DESTINATIONACCOUNTNAME = "destinationAccountName";
        public static final String WS_DESTINATIONACCOUNTNUMBER = "destinationAccountNumber";
        public static final String WS_PAY_TRANSACTIONAMMOUNT = "transactionAmount";
        public static final String WS_PAY_JOBIDINQUIRY = "jobIdInquiry";
        public static final String WS_PAY_IDBILLING = "idBilling";
        public static final String WS_PAY_ADDITIONALDATA = "additionalData";
        public static final String WS_PAY_PRODUCTCODE = "productCode";
        public static final String WS_PAY_FEE = "fee";
        public static final String WS_PAY_RRN = "rrn";
        public static final String ADDRESS = "address";
        public static final String IMEI = "imei";
        public static final String NAME = "name";
        public static final String TIMEOUT = "timeout";
        public static final String OTP = "otp";
        public static final String KODE_CABANG = "kodeCabang";
        public static final String AGENT = "agent";
        public static final String NIK = "nik";
        public static final String STATUS = "status";
        public static final String INFO = "info";
        public static final String ID = "id";
        public static final String USERNAME = "username";
        public static final String EMAIL = "email";
        public static final String MENU = "menu";
        public static final String NPWP = "npwp";
        public static final String ACCOUNT_TYPE = "accountType";
        public static final String AGENT_TYPE = "agentType";

        //Fase 2
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String PAGE = "page";
        public static final String TYPE = "type";
    }

    public class TRANSACTION {

        public class TYPE {

            public static final String INQUIRY = "1";
            public static final String POSTING = "2";
            public static final String TOP_UP = "3";
        }
    }

    public class FCM {

        public static final String URL = "https://fcm.googleapis.com/fcm/send";
        public static final String KEY = "key=AAAAhxtkmtI:APA91bHtG61lhIKnWNSAs4S8K5J4T7-Ll6p7ZlItF7yxuA-KnCR10muJEpoIdaN8kAzDAy5ggrXQkmuYTpema8jhLFOckYeFbPI6uKYm0R4RLrEKZ-_1ljPsiy13Bq9X13OPYf5wBdzB";

        public static final String DATA = "data";
        public static final String TO = "to";
        public static final String TITLE = "title";
        public static final String MESSAGE = "message";

        public class STATUS {

            public static final int SUCCESS = 1;
            public static final int FAILED = 2;
        }

    }

    // Internal Response Code
    public static String RC_APPROVED = "00";
    public static String RC_RESET_PASSWORD = "02";
    public static String RC_INTERNAL_ERROR = "06";
    public static String RC_TRANSACTION_AUTHENTICATION_FAILED = "07";
    public static String RC_INSUFFICIENT_BALANCE = "11";
    public static String RC_ACCOUNT_NOT_FOUND = "12";
    public static String RC_ACCOUNT_NOT_ACTIVE = "13";
    public static String RC_SECONDARY_ACCOUNT_NOT_FOUND = "14";
    public static String RC_SECONDARY_ACCOUNT_NOT_ACTIVE = "15";
    public static String RC_ACCOUNT_BLOCKED = "16";
    public static String RC_PARTNER_NOT_FOUND = "17";
    public static String RC_INVALID_HOST = "18";
    public static String RC_TRANSACTION_NOT_FOUND = "21";
    public static String RC_TRANSACTION_ALREADY_PAID = "22";
    public static String RC_CARD_OVERLIMIT = "23";
    public static String RC_INVALID_PRODUCT = "31";
    public static String RC_INVALID_TRANSACTION_CHANNEL = "32";
    public static String RC_TIME_OUT = "50";
    public static String RC_UNKNOWN_ERROR = "53";
    public static String RC_DB_ERROR = "56";
    public static String RC_MISSING_MANDATORY_PARAMETER = "60";
    public static String RC_CONN_DOWN = "69";
    public static String RC_FORMAT_ERROR = "70";
    public static String RC_UNABLE_TO_ROUTE_TRANSACTION = "73";
    public static String RC_REPRINT_NOT_AVAILABLE = "80";
    public static String RC_TRANSACTION_IS_FAILED = "81";

    //ISO8583 1987 MTI
    public static String MTI_POSTING = "0200";
    public static String MTI_INQUIRY = "0100";
    public static String MTI_REVERSAL = "0400";

    // Transaction Status
    public static int SUCCESS = 1;
    public static int FAILED = 0;
    public static int PENDING = 2;
    public static int REVERSED = 3;
    public static int SYNC_REQUIRED = 5;

    // Context
    public static String RC = "RC";
    public static String TXLOG = "TXLOG";
    public static String IN = "IN";
    public static String OUT = "OUT";
    public static String UUID = "UUID";
    public static String ORIGIN = "ORIGIN";
    public static String REQ = "REQ";
    public static String RSP = "RSP";
    public static String SRC = "SRC";
    public static String PRD = "PRODUCT";
    public static String AMOUNT = "AMOUNT";
    public static String FEE_AMOUNT = "FEE_AMOUNT";
    public static String FEE_INDICATOR = "FEE_INDICATOR";
    public static String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    public static String MAIN_ACCOUNT_TX = "MAIN_ACCOUNT_TX";
    public static String SECONDARY_ACCOUNT_TX = "SECONDARY_ACCOUNT_TX";
    public static String AUTHENTICATOR = "AUTHENTICATOR";
    public static String APPROVER = "APPROVER";
    public static String PRODUCT = "PRODUCT";
    public static String CHANNEL = "CHANNEL";
    public static String ACC_ORIGIN = "ACC_ORIGIN";
    public static String ACC_DEST = "ACC_DEST";
    public static String BALANCE = "BALANCE";
    public static String ACCOUNT = "ACCOUNT";
    public static String MERCHANT = "MERCHANT";
    public static String TERMINAL = "TERMINAL";
    public static String USER = "USER";
    public static String ACCOUNT_TEMP = "ACCOUNT_TEMP";
    public static String DEST_BANK_CODE = "DEST_BANK_CODE";
    public static String SMS_CONTENT = "SMS_CONTENT";
    public static String FEATURE_STRING = "FEATURE_STRING";
    public static String ACCOUNT_STRING = "FEATURE_STRING";
    public static String MERCHANT_STRING = "FEATURE_STRING";
    public static String MSG_OBJECT = "MSG_OBJECT";
    public static String STATUS = "STATUS";
    public static String RM = "RESPONSE_MSG";
    public static String START_TIMESTAMP = "START_TIMESTAMP";
    public static String VER = "VER";
    public static String SPRING_CONTEXT = "SPRING_CONTEXT";
    public static String DESTINATION = "DEST";
    public static String KEY = "KEY";
    public static String GROUP = "GROUP";
    public static String MESSAGE_RESPONSE = "MESSAGE_RESPONSE";
    public static String SESSION = "SESSION";
    public static String AGENT = "AGENT";
    public static String NASABAH = "NASABAH";
    public static String TARGET = "TARGET";
    public static String SMS_TYPE = "SMS_TYPE";
    public static String NOTIFTRX = "NOTIFTRX";

    public class SMS_RESPONSE {
        public static final String STATUS = "STATUS";
    }

    //RC MAPPING
    public static String SERVICE_CUSTOMER_REGISTRATION = "CustomerRegistration";
    public static String SERVICE_CHECK_BALANCE = "BalanceInquiry";
    public static String SERVICE_AGENT_LOGIN = "AgentLogin";
    public static String SERVICE_AGENT_CHANGE_PIN = "AgentChangePin";
    public static String SERVICE_AGENT_INQUIRY = "AgentInquiry";
    public static String SERVICE_PAYMENT_INQUIRY = "PaymentInquiry";
    public static String SERVICE_UPDATE_NO_HP = "UpdateNoHp";

    //Message Response
    public static final String CONNECTOR_SERVER = "CONNECTOR_SERVER";
    public static final String WS_PASSWORD = "WS_PASSWORD";
    public static final String TIMEOUT_TRANSACTION = "TIMEOUT_TRANSACTION";

    public static final String RM_PENDAFTARAN_SUCCESS = "RM_PENDAFTARAN_SUCCESS";
    public static final String RM_ACCOUNT_AGENT_INACTIVE = "RM_ACCOUNT_AGENT_INACTIVE";
    public static final String RM_ACCOUNT_AGENT_BLOCKED = "RM_ACCOUNT_AGENT_BLOCKED";
    public static final String RM_ACCOUNT_AGENT_DELETED = "RM_ACCOUNT_AGENT_DELETED";
    public static final String RM_ACCOUNT_AGENT_NOT_FOUND = "RM_ACCOUNT_AGENT_NOT_FOUND";
    public static final String RM_INVALID_REQUEST = "RM_INVALID_REQUEST";
    public static final String PATH_INQUIRY_REKENING = "PATH_INQUIRY_REKENING";
    public static final String RM_ACCOUNT_NASABAH_INACTIVE = "RM_ACCOUNT_NASABAH_INACTIVE";
    public static final String RM_ACCOUNT_NASABAH_BLOCKED = "RM_ACCOUNT_NASABAH_BLOCKED";
    public static final String RM_ACCOUNT_NASABAH_DELETED = "RM_ACCOUNT_NASABAH_DELETED";
    public static final String RM_ACCOUNT_NASABAH_NOT_FOUND = "RM_ACCOUNT_NASABAH_NOT_FOUND";
    public static final String RM_ACCOUNT_NASABAH_DUPLICATE = "RM_ACCOUNT_NASABAH_DUPLICATE";
    public static final String RM_ACCOUNT_TARGET_NOT_FOUND = "RM_ACCOUNT_TARGET_NOT_FOUND";
    public static final String RM_GENERATE_OTP_ERROR = "RM_GENERATE_OTP_ERROR";
    public static final String RM_ACCOUNT_AGENT_WRONG_PIN = "RM_ACCOUNT_AGENT_WRONG_PIN";
    public static final String RM_INQUIRY_TARIK_TUNAI = "RM_INQUIRY_TARIK_TUNAI";
    public static final String RM_INQUIRY_TOKEN_TARIK_TUNAI = "RM_INQUIRY_TOKEN_TARIK_TUNAI";
    public static final String RM_INQUIRY_TOKEN_TRANSFER_ON_US = "RM_INQUIRY_TOKEN_TRANSFER_ON_US";
    public static final String RM_INQUIRY_SETOR_TUNAI = "RM_INQUIRY_SETOR_TUNAI";
    public static final String RM_INQUIRY_PEMBELIAN_PULSA = "RM_INQUIRY_PEMBELIAN_PULSA";
    public static final String RM_TRANSACTION_NOT_FOUND = "RM_TRANSACTION_NOT_FOUND";
    public static final String TARIK_TUNAI = "TARIK_TUNAI";
    public static final String RM_MESSAGE_TOKEN = "RM_MESSAGE_TOKEN";
    public static final String RM_MESSAGE_NOTIF = "RM_MESSAGE_NOTIF";
    public static final String RM_MESSAGE_PIN = "RM_MESSAGE_PIN";
    public static final String RM_TOKEN_TARIK_TUNAI = "RM_TOKEN_TARIK_TUNAI";
    public static final String RM_TOKEN_TRANSFER_ON_US = "RM_TOKEN_TRANSFER_ON_US";
    public static final String RM_OTP_TRANSACTION_NOT_FOUND = "RM_OTP_TRANSACTION_NOT_FOUND";
    public static final String RM_POSTING_FORMAT_TARIK_TUNAI = "RM_POSTING_FORMAT_TARIK_TUNAI";
    public static final String RM_FORMAT_SMS_POSTING_TARIK_TUNAI = "RM_FORMAT_SMS_POSTING_TARIK_TUNAI";
    public static final String RM_FORMAT_SMS_POSTING_SETOR_TUNAI = "RM_FORMAT_SMS_POSTING_SETOR_TUNAI";
    public static final String RM_FORMAT_SMS_POSTING_TRANSFER_ON_US = "RM_FORMAT_SMS_POSTING_TRANSFER_ON_US";
    public static final String RM_FORMAT_SMS_POSTING_TRANSFER = "RM_FORMAT_SMS_POSTING_TRANSFER";
    public static final String RM_FORMAT_SMS_ADD_SALDO = "RM_FORMAT_SMS_ADD_SALDO";
    public static final String RM_POSTING_FORMAT_PEMBELIAN_PULSA = "RM_POSTING_FORMAT_PEMBELIAN_PULSA";
    public static final String SCHEDULE_TYPE = "SCHEDULE_TYPE";
    public static final String RM_ACCOUNT_NASABAH_OVERLIMIT = "RM_ACCOUNT_NASABAH_OVERLIMIT";
    public static final String RM_ACCOUNT_NASABAH_OVERLIMIT_DEBET = "RM_ACCOUNT_NASABAH_OVERLIMIT_DEBET";
    public static final String RM_FORMAT_POSTING_TRANSFER_ON_US = "RM_FORMAT_POSTING_TRANSFER_ON_US";
    public static final String RM_TIMEOUT_TRANSACTION = "RM_TIMEOUT_TRANSACTION";
    public static final String RM_POSTING_SETOR_TUNAI = "RM_POSTING_SETOR_TUNAI";
    public static final String RM_OVERLIMIT_SALDO_BSA = "RM_OVERLIMIT_SALDO_BSA";
    public static final String RM_PRODUCT_NOT_FOUND = "RM_PRODUCT_NOT_FOUND";
    public static final String RM_FORMAT_TRANSAKSI_TERAKHIR_TARIK_TUNAI = "RM_FORMAT_TRANSAKSI_TERAKHIR_TARIK_TUNAI";
    public static final String RM_FORMAT_TRANSAKSI_TERAKHIR_SETOR_TUNAI = "RM_FORMAT_TRANSAKSI_TERAKHIR_SETOR_TUNAI";
    public static final String RM_FORMAT_TRANSAKSI_TERAKHIR_TRANSFER_ON_US = "RM_FORMAT_TRANSAKSI_TERAKHIR_TRANSFER_ON_US";
    public static final String RM_FORMAT_TRANSAKSI_TERAKHIR_TRANSFER_LAKUPANDAI = "RM_FORMAT_TRANSAKSI_TERAKHIR_TRANSFER_LAKUPANDAI";

    public static String RC_ACCOUNT_AGEN_BLOCKED = "04";
    public static String RC_ACCOUNT_AGEN_DELETED = "05";
    public static String RC_ACCOUNT_AGEN_NOTFOUND = "06";
    //ADD BY STEVEN
    public static String RC_ACCOUNT_NASABAH_BLOCKED = "01";
    public static String RC_ACCOUNT_NASABAH_DELETED = "02";
    public static String RC_ACCOUNT_NASABAH_NOTFOUND = "03";

    //OTP
    public static final Long DEFAULT_OTP_TIMEOUT = new Long(3600000);
    public static final String OTP_TIMEOUT = "OTP_TIMEOUT";
    public static final String PATH_SEND_SMS_BY_NUMBER = "PATH_SEND_SMS_BY_NUMBER";
    public static final String PATH_SEND_SMS_BY_ACCOUNT = "PATH_SEND_SMS_BY_ACCOUNT";
    public static final String OTP_HEADER_MESSAGE = "OTP_HEADER_MESSAGE";
    public static final String PATH_INQUIRY_SALDO = "PATH_INQUIRY_SALDO";
    public static final String RM_BANK_RESPONSE_ERROR = "RM_BANK_RESPONSE_ERROR";
    public static final String RM_DEFAULT_INFO_SALDO = "RM_DEFAULT_INFO_SALDO";
    public static final String RM_INFORMASI_SALDO = "RM_INFORMASI_SALDO";
    public static final String RM_OTP_EXP_NOT_FOUND = "RM_OTP_EXP_NOT_FOUND";
    public static final String RM_OTP_EXPIRED = "RM_OTP_EXPIRED";
    public static final String RM_POSTING_TARIK_TUNAI = "RM_POSTING_TARIK_TUNAI";
    public static final String RM_SMS_PENDAFTARAN_SUKSES = "RM_SMS_PENDAFTARAN_SUKSES";
    public static final String PATH_EKSEKUSI_TRANSAKSI = "PATH_EKSEKUSI_TRANSAKSI";
    public static final String PATH_REVERSAL_EKSEKUSI_TRANSAKSI = "PATH_REVERSAL_EKSEKUSI_TRANSAKSI";
    public static final String RM_INQUIRY_TRANSFER_ON_US = "RM_INQUIRY_TRANSFER_ON_US";
    public static final String RM_MESSAGE_TOKEN_TARIK_TUNAI = "Tarik Tunai";
    public static final String RM_MESSAGE_INPUT_PIN = "Masukkan 6 digit nomor PIN Agent";
    public static final String FEE_AGENT = "FEE_AGENT";
    public static final String FEE_BANK = "FEE_BANK";
    public static final String PENDAFTARAN_NASABAH = "Pendaftaran Nasabah";
    public static final String DESC_PENDAFTARAN_NASABAH = "Pendaftaran Nasabah";
    public static final String DESC_TARIK_TUNAI = "Tarik Tunai";
    public static final String DESC_SETOR_TUNAI = "Setor Tunai";
    public static final String DESC_TRANSFER_ON_US = "Transfer On Us";
    public static final String DESC_PEMBELIAN_PULSA = "Pembelian Pulsa";
    public static final String DESC_TRANSAKSI_TERAKHIR = "Transaksi Terakhir";
    public static final String DESC_TRANSFER_LAKUPANDAI = "Transfer Lakupandai";
    public static final String DESC_MUTASI_AGENT = "Mutasi Agent";
    public static final String LIMIT_NASABAH_PER_BULAN = "LIMIT_NASABAH_PER_BULAN";
    public static final String LIMIT_NASABAH_PER_TAHUN = "LIMIT_NASABAH_PER_TAHUN";
    public static final String LIMIT_DEBET_NASABAH = "LIMIT_DEBET_NASABAH";
    public static final String RM_MESSAGE_TOKEN_PEMBELIAN_PULSA = "RM_MESSAGE_TOKEN_PEMBELIAN_PULSA";
    public static final String RM_INQUIRY_TOKEN_PEMBELIAN_PULSA = "RM_INQUIRY_TOKEN_PEMBELIAN_PULSA";
    public static final String PATH_INQUIRY_REKENING_BY_PHONENUMBER = "PATH_INQUIRY_REKENING_BY_PHONENUMBER";
    public static final String PATH_INQUIRY_REKENING_BY_ACCOUNTNUMBER = "PATH_INQUIRY_REKENING_BY_ACCOUNTNUMBER";
    public static final String TYPE_AGEN_LAKUPANDAI = "A";
    public static final String TYPE_NASABAH_BIASA = "R";
    public static final String TYPE_NASABAH_LAKUPANDAI = "L";
    public static final String PATH_VALIDATE_TIN = "PATH_VALIDATE_TIN";
    public static final String PATH_PINDAH_BUKU = "PATH_PINDAH_BUKU";
    public static final String RM_INTERNAL_ERROR = "RM_INTERNAL_ERROR";
    public static final String PATH_INQUIRY_BILLER_SMS_BANKING = "PATH_INQUIRY_BILLER_SMS_BANKING";
    public static final String PATH_POSTING_BILLER_SMS_BANKING = "PATH_POSTING_BILLER_SMS_BANKING";
    public static final String RM_FORMAT_SMS_POSTING_PEMBELIAN_PULSA = "RM_FORMAT_SMS_POSTING_PEMBELIAN_PULSA";
    public static final String LIMIT_SALDO_BSA = "LIMIT_SALDO_BSA";
//    public static final S
    // Account Type
    public static final int ONE_TIME_ACCOUNT = 1;

    // ISO8583 Field Number
    public static int ISO_FIELD_PAN = 2;
    public static int ISO_FIELD_PROCESSING_CODE = 3;
    public static int ISO_FIELD_TX_AMOUNT = 4;
    public static int ISO_FIELD_SETTLE_AMOUNT = 5;
    public static int ISO_FIELD_DATETIME = 7;
    public static int ISO_FIELD_STAN = 11;
    public static int ISO_FIELD_TIME = 12;
    public static int ISO_FIELD_DATE = 13;
    public static int ISO_FIELD_SETTLE_DATE = 15;
    public static int ISO_FIELD_CHANNEL_TYPE = 18;
    public static int ISO_FIELD_TID = 41;
    public static int ISO_FIELD_MID = 42;
    public static int ISO_FIELD_TRACK2 = 35;
    public static int ISO_FIELD_RRN = 37;
    public static int ISO_FIELD_RC = 39;
    public static int ISO_FIELD_PRIVATE_DATA = 48;
    public static int ISO_FIELD_CURRENCY = 49;
    public static int ISO_FIELD_ACC_INFO = 54;
    public static int ISO_FIELD_IGATE_FEATURE_CODE = 56;
    public static int ISO_FIELD_AMS_FEATURE_CODE = 63;
    public static int ISO_FIELD_NETWORK_CODE = 70;
    public static int ISO_FIELD_DESTINATION_BANK_CODE = 100;
    public static int ISO_FIELD_ACC_IDENTIFICATION1 = 102;
    public static int ISO_FIELD_ACC_IDENTIFICATION2 = 103;
    public static int ISO_FIELD_TX_INFO = 104;
    public static int ISO_FIELD_ACC_NAME1 = 105;
    public static int ISO_FIELD_ACC_NAME2 = 106;
    public static int ISO_FIELD_CUST_REFF = 126;
    public static int ISO_FIELD_ACQUIRING_INSTITUTION_CODE = 32;
    public static int ISO_FIELD_FORWARDING_INSTITUTION_CODE = 33;

    public static String ISO_VALUE_CURRCODE_IDR = "360";

    public static String PDAM_PAYMENT_FORMAT = "[\n"
            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
            + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
            + "    {\"header\":\"ID Pelanggan\",\"value\":\"[ID_Pelanggan]\"},\n"
            + "    {\"header\":\"Nama\",\"value\":\"[Nama]\"},\n"
            + "    {\"header\":\"Bulan Tagihan\",\"value\":\"[BULAN_TAGIHAN]\"},\n"
            + "    {\"header\":\"Tagihan\",\"value\":\"Rp.[NOMINAL]\"},\n"
            + "    {\"header\":\"Biaya Admin\",\"value\":\"Rp.[BIAYA_ADMIN]\"},\n"
            + "    {\"header\":\"Jumlah Bayar\",\"value\":\"Rp.[JUMLAH_BIAYA]\"},\n"
            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
            + "]";

    public static String PDAM_PAYMENT_FORMAT_PERIODE = "[\n"
            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
            + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
            + "    {\"header\":\"ID Pelanggan\",\"value\":\"[ID_Pelanggan]\"},\n"
            + "    {\"header\":\"Nama\",\"value\":\"[Nama]\"},\n"
            + "    {\"header\":\"Bulan Tagihan\",\"value\":\"[BULAN_TAGIHAN]\"},\n"
            + "    {\"header\":\"Periode\",\"value\":\"[PERIODE]\"},\n"
            + "    {\"header\":\"Tagihan\",\"value\":\"Rp.[NOMINAL]\"},\n"
            + "    {\"header\":\"Biaya Admin\",\"value\":\"Rp.[BIAYA_ADMIN]\"},\n"
            + "    {\"header\":\"Jumlah Bayar\",\"value\":\"Rp.[JUMLAH_BIAYA]\"},\n"
            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
            + "]";

    public static final int SMS_LENGTH = 160;
    public static final String HEADER_ENC = "[ENC]";
    public static final String ENC = "lakupandaipwd";

    public class ISO {

        public static final String REQUEST = "request";
        public static final String RESPONSE = "response";
    }

    public enum LIMIT_TYPE {

        NO_LIMIT(0), MONTHLY(1), YEARLY(2);
        private int status;

        private LIMIT_TYPE(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

    public enum WS_RC {

        FAILED(0), SUCCESS(1);
        private int status;

        private WS_RC(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }

        public int getRc() {
            return status;
        }
    }

    public enum TRX_STATUS {

        FAILED(0),
        SUCCESS(1),
        PENDING(2),
        ACTIVATION(3),
        PROCESS(4);

        private int status;

        private TRX_STATUS(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

    public enum TRX_TYPE {

        INQUIRY,
        POSTING,
        TOP_UP;

        private String trxtype;

        public String getToString() {
            return name();
        }
    }

    public enum Status {

        BLOCK(0),
        ACTIVE(1),
        NEED_APPROVAL(2),
        INACTIVE(3),
        NEED_APPROVAL_CS(4),
        REJECTED(5),
        UNDEF(6);

        private int status;

        private Status(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

    public enum AccountType {

        Agent('A'),
        Customer('C');

        public char asChar() {
            return asChar;
        }

        private final char asChar;

        private AccountType(char asChar) {
            this.asChar = asChar;
        }

        public static AccountType getAgentAccountType(String data) {
            for (AccountType d : AccountType.values()) {
                if (d.asChar == data.charAt(0)) {
                    return d;
                }
            }
            return null;
        }
    }

    public enum AgentAccountType {

        P, B, I;

        private String accountType;

        public String getToString() {
            return name();
        }
    }

    public enum ProductStatus {

        INACTIVE(0),
        ACTIVE(1);

        private int status;

        private ProductStatus(int status) {
            this.status = status;
        }

        /**
         * @return the status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

    public enum SettingStatus {

        INACTIVE(0),
        ACTIVE(1),
        NEED_APPROVE(2);

        private int status;

        private SettingStatus(int status) {
            this.status = status;
        }

        /**
         * @return the status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

    public enum ProductType {

        mainmenu,
        pembelian,
        pembayaran;
    }

    public enum OtpStatus {

        INACTIVE(0),
        ACTIVE(1),
        USED(2);

        private int status;

        private OtpStatus(int status) {
            this.status = status;
        }

        /**
         * @return the status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

    public enum SessionStatus {

        INACTIVE(0),
        ACTIVE(1);

        private int status;

        private SessionStatus(int status) {
            this.status = status;
        }

        /**
         * @return the status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(int status) {
            this.status = status;
        }

        public String getToString() {
            return name();
        }
    }

}
