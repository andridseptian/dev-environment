/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MServiceProxy;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentInquiryParticipant implements TransactionParticipant {

    private RcRepository rcRepository;

    public AgentInquiryParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MServiceProxy proxy = (MServiceProxy) ctx.get(Constants.OBJ.SERVICEPROXY);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String iAccountNumber = dataRequest.getString(Constants.PARAM.ACCOUNT_NUMBER);
        String iId = dataRequest.getString(Constants.PARAM.ID);
        JSONObject hasil = Service.serviceInquiryAgent(iAccountNumber);
        Service.saveSessionWeb(iId, "Inquiry rekening " + iAccountNumber);
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE) && hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_AGENT_INQUIRY, rc);
            if (mapRc == null) {
                req.setRm(rm);
                req.setStatus(Constants.TRX_STATUS.FAILED);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            req.setRm(mapRc.getRm());
            req.setStatus(mapRc.getStatus());
            if (Constants.TRX_STATUS.SUCCESS == req.getStatus()) {
                ctx.put(Constants.OBJ.RESP, hasil);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return PREPARED | NO_JOIN;
            } else {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                return ABORTED | NO_JOIN;
            }
        } else {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }

}
