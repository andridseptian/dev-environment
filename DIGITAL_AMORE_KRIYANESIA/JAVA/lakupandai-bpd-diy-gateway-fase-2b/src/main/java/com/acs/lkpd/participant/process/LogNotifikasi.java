/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.SessionRepository;
import java.io.Serializable;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Andri D. Septian
 */
public class LogNotifikasi implements AbortParticipant{
    Log log = Log.getLog("Q2", getClass().getName());
    private NotificationTrxRepository notificationTrxRepository;
    private SessionRepository sessionRepository;

    public LogNotifikasi() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
        sessionRepository = context.getBean(SessionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    /**
     *
     * @param l
     * @param srlzbl
     * @context TXLOG
     *
     * @return
     */
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        
//        MNotifikasiTrx mnt = (MNotifikasiTrx) ctx.get(Constants.NOTIFIKASI_LOG);
//        MSession mSession = sessionRepository.findFirstByStanAndAgentOrderByCrTimeDesc(mnt.getStan(), mnt.getAgent());
//        mnt.setMobileResponse(mSession.getResponseToClient());
//        notificationTrxRepository.save(mnt);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        
//        MNotifikasiTrx mnt = (MNotifikasiTrx) ctx.get(Constants.NOTIFIKASI_LOG);
//        MSession mSession = sessionRepository.findFirstByStanAndAgentOrderByCrTimeDesc(mnt.getStan(), mnt.getAgent());
//        mnt.setMobileResponse(mSession.getResponseToClient());
//        notificationTrxRepository.save(mnt);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

}
