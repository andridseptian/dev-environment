/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface AgentRepository extends JpaRepository<MAgent, String> {

    MAgent findByMsisdn(String msisdn);

    MAgent findById(String id);
    
    MAgent findFirstByAccountNumber(String accountnumber);

    MAgent findByIdAndMsisdn(String id, String msisdn);

    MAgent findByIdAndPin(String id, String pin);

    MAgent findFirstByMsisdnOrderByCrTimeDesc(String msisdn);
    
    MAgent findFirstByResetMsisdnOrderByCrTimeDesc(String msisdn);
}
