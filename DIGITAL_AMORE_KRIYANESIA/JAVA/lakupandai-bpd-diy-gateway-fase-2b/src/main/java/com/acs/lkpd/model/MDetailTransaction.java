package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "trx_detail_logger")
public class MDetailTransaction implements Serializable {

    private Long id;
    private Long transactionId;
    private LocalDateTime crTime;
    private String processName;
    private double amount;
    private double feeBank;
    private double feeAgent;
    private String rrn;
    private String rc;
    private String rcDescription;
    private String msgRequest;
    private String msgResponse;
    private Constants.TRX_STATUS status;
    private String coreReference;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "detail_trx_seq")
    @SequenceGenerator(name = "detail_trx_seq", sequenceName = "detail_trx_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "transaction_id", nullable = false)
    public Long getTransactionId() {
        return transactionId;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "process_name")
    public String getProcessName() {
        return processName;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.TRX_STATUS getStatus() {
        return status;
    }

    @Column(name = "amount")
    public double getAmount() {
        return amount;
    }

    @Column(name = "fee_bank")
    public double getFeeBank() {
        return feeBank;
    }

    @Column(name = "fee_agent")
    public double getFeeAgent() {
        return feeAgent;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    @Column(name = "rc")
    public String getRc() {
        return rc;
    }

    @Column(name = "rc_description")
    public String getRcDescription() {
        return rcDescription;
    }

    @Column(name = "msg_request")
    @Type(type = "text")
    public String getMsgRequest() {
        return msgRequest;
    }

    @Column(name = "msg_response")
    @Type(type = "text")
    public String getMsgResponse() {
        return msgResponse;
    }

    @Column(name = "core_reference")
    public String getCoreReference() {
        return coreReference;
    }

    //============SET===========//
    public void setId(Long id) {
        this.id = id;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setFeeBank(double feeBank) {
        this.feeBank = feeBank;
    }

    public void setFeeAgent(double feeAgent) {
        this.feeAgent = feeAgent;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public void setRcDescription(String rcDescription) {
        this.rcDescription = rcDescription;
    }

    public void setMsgRequest(String msgRequest) {
        this.msgRequest = msgRequest;
    }

    public void setMsgResponse(String msgResponse) {
        this.msgResponse = msgResponse;
    }

    public void setStatus(Constants.TRX_STATUS status) {
        this.status = status;
    }

    public void setCoreReference(String coreReference) {
        this.coreReference = coreReference;
    }

}
