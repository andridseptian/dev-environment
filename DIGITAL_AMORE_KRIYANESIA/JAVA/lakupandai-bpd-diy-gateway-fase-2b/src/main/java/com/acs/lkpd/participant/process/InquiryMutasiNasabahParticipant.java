/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class InquiryMutasiNasabahParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());
    private final TransactionRepository transactionRepository;
    private final SessionRepository sessionRepository;

    public InquiryMutasiNasabahParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        transactionRepository = context.getBean(TransactionRepository.class);
        sessionRepository = context.getBean(SessionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        try {
            MSession tempSession = null;
            MTransaction tempTransaction = transactionRepository.findFirstByTypeAndDescriptionNotAndAgentAndNasabahOrderByCrTimeDesc(Constants.TRX_TYPE.POSTING, trx.getDescription(), agent, nasabah);
            if (tempTransaction != null) {
                log.info("Last Transaction Found");
                tempSession = sessionRepository.findFirstByAgentAndNasabahAndOtpOrderByCrTimeDesc(tempTransaction.getAgent(), tempTransaction.getNasabah(), tempTransaction.getOtp());
            }
            if (tempTransaction != null && tempSession != null) {
                Long tempSessionId = session.getId();
                String tempOrigin = session.getOrigin();
                String tempOriginReff = session.getOriginReff();
                String tempStan = session.getStan();

                session = tempSession;
                trx = tempTransaction;

                session.setId(tempSessionId);
                session.setOrigin(tempOrigin);
                session.setOriginReff(tempOriginReff);
                session.setStan(tempStan);

                trx.setOrigin(tempOrigin);
                trx.setOriginReff(tempOriginReff);
                session.setStan(tempStan);

                String descResponse = "";
                log.info(trx.getDescription());
                switch (trx.getDescription()) {
                    case Constants.DESC_TARIK_TUNAI:
                        descResponse = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_TARIK_TUNAI.name());
                        descResponse = descResponse.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                                .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                                .replace("[Name]", session.getNasabah().getName())
                                .replace("[Status]", trx.viewStatus())
                                .replace("[Ref]", trx.getOriginReff());
                        break;
                    case Constants.DESC_SETOR_TUNAI:
                        descResponse = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_SETOR_TUNAI.name());
                        descResponse = descResponse.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                                .replace("[NasabahMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                                .replace("[NasabahName]", session.getNasabah().getName())
                                .replace("[Status]", trx.viewStatus())
                                .replace("[Ref]", trx.getOriginReff());
                        break;
                    case Constants.DESC_TRANSFER_ON_US:
                        descResponse = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_TRANSFER_ON_US.name());
                        descResponse = descResponse.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                                .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                                .replace("[TargetAccount]", session.getTargetNasabahAccountNumber())
                                .replace("[TargetName]", session.getTargetNasabahName())
                                .replace("[Status]", trx.viewStatus())
                                .replace("[Ref]", trx.getOriginReff());
                        break;
                    case Constants.DESC_TRANSFER_LAKUPANDAI:
                        descResponse = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_TRANSFER_LAKUPANDAI.name());
                        descResponse = descResponse.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                                .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                                .replace("[TargetMsisdn]", Utility.viewMsisdn(session.getTargetNasabahMsisdn()))
                                .replace("[TargetName]", session.getTargetNasabahName())
                                .replace("[Status]", trx.viewStatus())
                                .replace("[Ref]", trx.getOriginReff());
                        break;
                    default:
                        descResponse = Service.getResponseFromSettings(KeyMap.RM_TRANSACTION_NOT_FOUND.name());
                        break;
                }
                trx.setInfo(descResponse);
                session.setRcToClient(Constants.RC_APPROVED);
                session.setResponseToClient(descResponse);
                if (sendSMS) {
                    boolean statusSms = Utility.sendSMS(descResponse, session.getNasabah().getMsisdn());
                    trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSms);
                    ctx.log(trx.getSmsInfo());
                } else {
                    trx.setSmsInfo("Not Send SMS " + trx.getDescription());
                    ctx.log(trx.getSmsInfo());
                }
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return PREPARED;
            } else {
                log.info("Inquiry Transaksi Terakhir Null Data");
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
                return ABORTED;
            }
        } catch (Exception e) {
            ctx.log(e);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
            return ABORTED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
