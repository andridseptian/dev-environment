/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.util;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author USER
 */
public class FormResponse {

    public static class bank {

        public static String setorTunai() {
            String format = new JSONArray()
                    .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                    .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                    .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                    .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                    .put(new JSONObject().put("header", "Nama Nasabah").put("value", "[NASABAH]"))
                    .put(new JSONObject().put("header", "No. HP").put("value", "[NOHP]"))
                    .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                    .toString();

            return format;
        }
        
        public static String setorTunaiReguler() {
            String format = new JSONArray()
                    .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                    .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                    .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                    .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                    .put(new JSONObject().put("header", "Nama Nasabah").put("value", "[NASABAH]"))
                    .put(new JSONObject().put("header", "No. REK").put("value", "[NOREK]"))
                    .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                    .toString();

            return format;
        }

        public static String tarikTunai() {
            String format = new JSONArray()
                    .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                    .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                    .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                    .put(new JSONObject().put("header", "Nama Nasabah").put("value", "[REKPENERIMA]"))
                    .put(new JSONObject().put("header", "No HP").put("value", "[NAMAPENERIMA]"))
                    .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                    .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                    .toString();

            return format;
        }

        public static String transferLakupandai() {
            String format = new JSONArray()
                    .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                    .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                    .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                    .put(new JSONObject().put("header", "NoHP Penerima").put("value", "[REKPENERIMA]"))
                    .put(new JSONObject().put("header", "Nama Penerima").put("value", "[NAMAPENERIMA]"))
                    .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                    .put(new JSONObject().put("header", "Nama Pengirim").put("value", "[NAMAPENGIRIM]"))
                    .put(new JSONObject().put("header", "NoHP Pengirim").put("value", "[NOHPPENGIRIM]"))
                    .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                    .toString();

            return format;
        }

        public static String transferOnUs() {
            String format = new JSONArray()
                    .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                    .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                    .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                    .put(new JSONObject().put("header", "Rek. Penerima").put("value", "[REKPENERIMA]"))
                    .put(new JSONObject().put("header", "Nama Penerima").put("value", "[NAMAPENERIMA]"))
                    .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                    .put(new JSONObject().put("header", "Nama Pengirim").put("value", "[NAMAPENGIRIM]"))
                    .put(new JSONObject().put("header", "NoHP Pengirim").put("value", "[NOHPPENGIRIM]"))
                    .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                    .toString();

            return format;
        }
    }
}
