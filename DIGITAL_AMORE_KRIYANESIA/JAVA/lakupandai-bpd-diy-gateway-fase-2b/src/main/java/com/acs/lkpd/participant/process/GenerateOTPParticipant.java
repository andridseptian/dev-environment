package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class GenerateOTPParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());
    private final OTPRepository oTPRepository;

    public GenerateOTPParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        oTPRepository = context.getBean(OTPRepository.class);

    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.log("--Generate OTP Participant--");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
        String[] request = session.getRequestClient() != null ? session.getRequestClient().split("\\|") : null;
        String targetNumber = session.getNasabah() != null ? session.getNasabah().getMsisdn()
                : request != null ? request[0]
                        : "-";
        ctx.log("request : " + request.toString());
        ctx.log("request[0] : " + request[0]);
        ctx.log("targetNumber : " + targetNumber);
        ctx.log("agent : " + session.getAgent().getId());

        MOtp otp = Service.generateOTPPayment(targetNumber, session.getAgent().getId());
        if (otp == null) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_GENERATE_OTP_ERROR));
            ctx.log("ResponseToClient: " + session.getResponseToClient());
            ctx.log("OTP IS NULL");
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        } else {
            otp.setProduct(session.getProduct().getProductCode());
            otp = oTPRepository.save(otp);
            ctx.log("otp : " + otp.getOtp());
            ctx.log("product : " + otp.getProduct());
            isomsg.set(61, otp.getOtp());
            session.setOtp(otp.getOtp());
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.IN, isomsg);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
