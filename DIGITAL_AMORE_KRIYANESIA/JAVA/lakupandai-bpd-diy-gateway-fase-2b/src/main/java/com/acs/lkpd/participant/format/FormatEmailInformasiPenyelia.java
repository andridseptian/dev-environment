/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MCabang;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.WebReportUser;
import com.acs.lkpd.repository.CabangRepository;
import com.acs.lkpd.repository.WebReportUserRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class FormatEmailInformasiPenyelia implements TransactionParticipant, Configurable {

    private Configuration cfg;
    private final CabangRepository cabangRepository;
    private final WebReportUserRepository webReportUserRepository;

    public FormatEmailInformasiPenyelia() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        cabangRepository = context.getBean(CabangRepository.class);
        webReportUserRepository = context.getBean(WebReportUserRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String iId, iKodeCabang, iMenu;
        iId = dataRequest.getString(Constants.PARAM.ID);
        iKodeCabang = dataRequest.getString(Constants.PARAM.KODE_CABANG);
        iMenu = dataRequest.getString(Constants.PARAM.MENU);
        String jabatan = cfg.get("jabatan", "Penyelia");
        MCabang cabang = cabangRepository.findOne(iKodeCabang);
        if (cabang == null) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Kode cabang [" + iKodeCabang + "] tidak terdaftar dalam sistem");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        WebReportUser issuerCs = null;
        try {
            issuerCs = webReportUserRepository.findOne(Long.parseLong(iId));
        } catch (NullPointerException | NumberFormatException ex) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("CS [" + iId + "] format id salah/tidak ditemukan dalam sistem");
            req.setInfo(ex.getMessage());
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        if (issuerCs == null) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("CS [" + iId + "] tidak terdaftar dalam sistem");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        WebReportUser penyelia = webReportUserRepository.findFirstByCabangAndJabatanOrderByCrTimeDesc(cabang, "Penyelia");
        if (penyelia == null) {
            penyelia = webReportUserRepository.findOne(Long.parseLong(iId));
            if (penyelia == null) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Jabatan " + jabatan + " cabang " + cabang.getNama() + " tidak ditemukan, " + "CS [" + iId + "] tidak terdaftar dalam sistem");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        }
        Service.saveSessionWeb(iId, "Notifikasi untuk Penyelia " + penyelia.getName() + " untuk menu " + iMenu);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_EMAIL_WEB_APPROVAL.name());
        String title = Service.getResponseFromSettings(KeyMap.RM_FORMAT_EMAIL_TITLE_WEB_APPROVAL.name());
        try {
            response = response.replace("[ApprovalName]", penyelia.getName())
                    .replace("[CsUsername]", issuerCs.getUsername())
                    .replace("[Menu]", iMenu);
            if (Service.sendEmail(response, title, penyelia.getEmail())) {
                req.setInfo(req.getInfo() + "Send Email to " + penyelia.getEmail() + " success");
            } else {
                req.setInfo(req.getInfo() + "Send Email to " + penyelia.getEmail() + " failed");
            }
            ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "send Email Success"));
            return PREPARED | NO_JOIN;
        } catch (Exception ex) {
            ctx.log(ex.getMessage());
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setInfo(ex.getMessage());
            req.setRm("Terjadi Kesalahan Sistem, mohon maaf atas ketidaknyamanan ini");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
