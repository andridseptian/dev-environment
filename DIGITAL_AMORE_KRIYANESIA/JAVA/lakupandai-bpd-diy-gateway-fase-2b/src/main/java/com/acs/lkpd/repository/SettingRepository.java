/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MSettings;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface SettingRepository extends JpaRepository<MSettings, String> {

}
