/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class SendOTPParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String rm = cfg.get("rm", "OTP_HEADER_MESSAGE");
        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
        MOtp otp = Service.findOTP(isomsg.getString(61));
        if (session.getOtp() == null || otp == null || session.getOtp().isEmpty()) {
            session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        if (sendSMS) {
            String isiPesanOTP = Service.getResponseFromSettings(rm) + otp.getOtp();
            boolean statusSms = Utility.sendSMS(isiPesanOTP, session.getNasabah().getMsisdn());
            trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSms);
        } else {
            trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
        }
        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
