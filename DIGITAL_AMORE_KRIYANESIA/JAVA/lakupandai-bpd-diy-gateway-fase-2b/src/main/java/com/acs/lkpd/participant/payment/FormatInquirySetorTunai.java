/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class FormatInquirySetorTunai implements TransactionParticipant, Configurable {

    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;

    public FormatInquirySetorTunai() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        productRepository = context.getBean(ProductRepository.class);
    }

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.log("--Format Inquiry Setor Tunai--");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        if (product == null) {
            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
            session.setResponseToClient("Product tidak ditemukan atau tidak terdaftar, mohon coba beberapa saat lagi / hubungi admin untuk tindakan lebih lanjut");
            ctx.log("product null");
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        String mobileResponse = "";
        try {
            String productName = product.getProductName();
            double amount = Double.valueOf(request[1].trim());
            double fee = trx.getFeeAgent() + trx.getFeeBank();
            double totalAmount = amount + fee;
            JSONArray formatInquiryArray = null;

            ctx.log("productName: " + productName);
            ctx.log("productcode: " + product.getProductCode());
            ctx.log("amount: " + amount);

            if (product.getProductCode() == "340000" || product.getProductCode() == "350000") {
                try {

                } catch (Exception e) {
                    log.error(e);
                }
            }

            //Build Mobile response
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[Amount]", "Rp." + Utility.formatAmount(amount))
                            .replace("[NasabahName]",
                                    session.getNasabah() != null ? session.getNasabah().getName()
                                    : session.getTargetNasabahName() != null ? session.getTargetNasabahName()
                                    : "-")
                            .replace("[msisdn]",
                                    session.getNasabah() != null ? Utility.viewMsisdn(session.getNasabah().getMsisdn())
                                    : session.getTargetNasabahAccountNumber() != null ? session.getTargetNasabahAccountNumber() : "-")
                            .replace("[ProductName]", productName)
                    );
                }
                mobileResponse = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                log.error(ex);
                trx.setInfo("error, format inquiry tidak ditemukan/null, detail:" + ex.getMessage());
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient("Transaksi tidak dapat dilakukan, format product tidak ditemukan");
                ctx.log("Transaksi tidak dapat dilakukan, format product tidak ditemukan");
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.SESSION, session);
            }

            trx.setAmount(amount);

            MProduct feeProduct = productRepository.findByProductCode("320000");

//            trx.setFeeAgent(product.getFeeAgent());
//            trx.setFeeBank(product.getFeeBank());
            trx.setFeeAgent(feeProduct.getFeeAgent());
            trx.setFeeBank(feeProduct.getFeeBank());
            trx.setTotalAmount(trx.getAmount() + trx.getFeeBank());
            trx.setMobileResponse(mobileResponse);
            session.setAmount(totalAmount);
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(mobileResponse);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);

            ctx.log("mobileResponse: " + mobileResponse);

            return PREPARED | NO_JOIN;
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            ctx.log(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
