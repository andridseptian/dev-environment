package com.acs.util;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNasabah;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.util.Log;
import org.w3c.dom.DOMException;
import org.w3c.dom.ls.LSException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author dhacker
 */
public class Utility {

    Log log = Log.getLog("Q2", getClass().getName());

    public static JSONObject formatParameter(String header, String value) {
        return new JSONObject().put("header", header).put("value", value);
    }

    public static String toPrettyFormat(String jsonString) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonString).getAsJsonObject();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJson = gson.toJson(json);

        return prettyJson;
    }

    public static String gsonView(Object data, boolean isView, String... viewField) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return isView != Arrays.asList(viewField).contains(f.getName());
            }

            @Override
            public boolean shouldSkipClass(Class<?> arg0) {
                return false;
            }
        });
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                .registerTypeAdapter(LocalTime.class, new LocalTimeAdapter());
        Gson gson = gsonBuilder.create();
        return gson.toJson(data);
    }

    public static class LocalDateAdapter implements JsonSerializer<LocalDate> {

        @Override
        public JsonElement serialize(LocalDate t, java.lang.reflect.Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(t.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))); // "yyyy-mm-dd"
        }
    }

    public static class LocalTimeAdapter implements JsonSerializer<LocalTime> {

        @Override
        public JsonElement serialize(LocalTime t, java.lang.reflect.Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(t.format(DateTimeFormatter.ofPattern("HHmm"))); // "yyyy-mm-dd"
        }
    }

    public static String getFormatXML(String xml) {
        try {
            final InputSource src = new InputSource(new StringReader(xml));
            final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
            boolean keepDeclaration = xml.startsWith("<?xml");
            final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
            final LSSerializer writer = impl.createLSSerializer();
            writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set this to true if the output needs to be beautified.
            writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set this to true if the declaration is needed to be outputted.
            return writer.writeToString(document);
        } catch (IOException | ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException | ParserConfigurationException | DOMException | LSException | SAXException ex) {
            return xml;
        }
    }

    public static double getParseAmount(String amount, int multiplier) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setMultiplier(multiplier);
            return df.parse(amount).doubleValue();
        } catch (Exception ex) {
            return -1d;
        }
    }

    public static String setParseAmount(double amount, int multiplier) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setMultiplier(multiplier);
            return df.format(amount);
        } catch (Exception ex) {
            return "";
        }
    }

    public static String getViewStatus(Constants.TRX_STATUS status) {
        switch (status) {
            case SUCCESS:
                return "Sukses";
            case FAILED:
                return "Gagal";
            case PENDING:
                return "Pending";
            default:
                return "Pending";
        }
    }

    public static String getStringWithDefault(JSONObject data, String header, String dataDefault) {
        if (data.has(header)) {
            return data.getString(header);
        } else {
            return dataDefault;
        }
    }

    public static double getDoubleWithDefault(JSONObject data, String header, double dataDefault) {
        if (data.has(header)) {
            return data.getDouble(header);
        } else {
            return dataDefault;
        }
    }

    public static Boolean sendSMS(String message, String msisdn) {
        return Service.sendSms(
                msisdn,
                message,
                "LdAiKyU",
                "1121",
                "lakupandaiDIY");
    }

    public static String generateKeyTrx(String rrn, String stan) {
        return rrn + stan;
    }

    public static String replaceTokens(String text, Map<String, String> replacements) {
        Pattern pattern = Pattern.compile("\\[(.+?)\\]");
        Matcher matcher = pattern.matcher(text);
        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (matcher.find()) {
            String replacement = replacements.get(matcher.group(1));
            builder.append(text.substring(i, matcher.start()));
            if (replacement == null) {
                builder.append(matcher.group(0));
            } else {
                builder.append(replacement);
            }
            i = matcher.end();
        }
        builder.append(text.substring(i, text.length()));
        return builder.toString();
    }

    public static JSONObject notHas(String json, String... keyCheck) throws JSONException, ParameterTidakDitemukanException {
        JSONObject data = new JSONObject(json);
        for (String s : keyCheck) {
            if (!data.has(s)) {
                throw new ParameterTidakDitemukanException("Parameter " + s + " tidak ada", (Arrays.asList(keyCheck).indexOf(s)) + 1);
            }
        }
        return data;
    }

    public static class ParameterTidakDitemukanException extends Exception {

        int fieldNotFound;

        public ParameterTidakDitemukanException(String message, int fieldNotFound) {
            super(message);
            this.fieldNotFound = fieldNotFound;
        }

        public int getFieldNotFound() {
            return fieldNotFound;
        }

        public void setFieldNotFound(int fieldNotFound) {
            this.fieldNotFound = fieldNotFound;
        }

    }

    static ArrayList modules = new ArrayList();
    static final String Encoding = "ISO8859_1";
    static final int PadLeft = 0;
    static final int PadRight = 1;

    public static String formatMsisdn(String msisdn) {
        if (msisdn.startsWith("+")) {
            return msisdn.replaceFirst("+", "");
        }
        if (msisdn.startsWith("620")) {
            return "62" + msisdn.substring(3);
        } else if (msisdn.startsWith("62")) {
            return msisdn;
        } else if (msisdn.startsWith("0")) {
            return "62" + msisdn.substring(1);
        } else {
            return msisdn;
        }
    }

    public static String viewMsisdn(String msisdn) {
        if (msisdn.startsWith("+")) {
            return msisdn.replaceFirst("+", "");
        }
        if (msisdn.startsWith("620")) {
            return msisdn.substring(2);
        } else if (msisdn.startsWith("62")) {
            return "0" + msisdn.substring(2);
        } else if (msisdn.startsWith("0")) {
            return msisdn;
        } else {
            return msisdn;
        }
    }

    public static class LocalDateTimeAdapter implements JsonSerializer<LocalDateTime> {

        @Override
        public JsonElement serialize(LocalDateTime t, java.lang.reflect.Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(t.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))); // "yyyy-mm-dd"
        }
    }

    public static String toJSON(Object t) throws Exception {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();
        return gson.toJson(t);
    }

    /**
     * Convert value to amount format, "##,###,###,###".
     *
     * @param val
     * @return Formatted amount.
     */
    public static String formatAmount(double val) {
        String ret = "";
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("##,###,###,###");
        ret = decFrm.format(val).replace(',', '.');
        return ret;
    }

    public static String stringAmount(double val) {
        String ret;
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("#");
        ret = decFrm.format(val);
        return ret;
    }

    public static String stringAmount(BigDecimal val) {
        val.setScale(0, BigDecimal.ROUND_DOWN);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(0);
        df.setGroupingUsed(false);
        return df.format(val);
    }

    public static String zeropad(String i, int len) {
        try {
            return ISOUtil.zeropad(i, len);
        } catch (ISOException isoe) {
            return i.substring(0, len);
        }
    }

    /**
     * Get settle date from given format. Settle date is H + 1
     *
     * @param format
     * @return Settled from given parameter.
     */
    public static String getReconDate(String format) {
        String ret = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        long t = System.currentTimeMillis();
        try {
            {
                //add 1 day
                t += (24 * 60 * 60 * 1000);
                Date dateadd = new Date(t);
                cal.setTime(dateadd);
                ret = df.format(dateadd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static String getDate(String format) {
        String ret = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        long t = System.currentTimeMillis();
        try {
            Date dateadd = new Date(t);
            cal.setTime(dateadd);
            ret = df.format(dateadd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static Date getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    /**
     * Get current date time, return value format is yyyyMMddHHmmss
     *
     * @return Current date time String, return value format is yyyyMMddHHmmss
     */
    public static String getCurrentDateTime() {
        String datetime;
        SimpleDateFormat df;
        Date now = new Date();
        df = new SimpleDateFormat("yyyyMMddHHmmss");
        datetime = df.format(now);
        return datetime;
    }

    /**
     * Get formatted date from String s.
     *
     * @param s
     * @param pattern
     * @return Formatted date from String s
     */
    public static Date getDateFromStr(String s, String pattern) {
        Date datetime = null;
        SimpleDateFormat df;
        try {
            df = new SimpleDateFormat(pattern);
            //df.p
            datetime = df.parse(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datetime;
    }

    /**
     * Get string formatted date from Date value.
     *
     * @param dt
     * @param pattern
     * @return Formatted date base on pattern parameter.
     */
    public static String getStrFromDate(Date dt, String pattern) {
        String ret = null;
        SimpleDateFormat df;
        try {
            df = new SimpleDateFormat(pattern);
            ret = df.format(dt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Get settle date from given date. Settle date is H + 1
     *
     * @param date
     * @return Settle date from given parameter.
     */
    public static String getSettleDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String ret = "";

        if (date == null) {
            return ret;
        }

        if (date.equals("00000000")) {
            return ret;
        }

        try {
            df.parse(date);
            ret = date;
        } catch (Exception e) {
            e.printStackTrace();
            ret = "";
        }
        return ret;
    }

    /**
     * Convert format date from fromFormat to toFormat
     *
     * @param date
     * @param fromFormat
     * @param toFormat
     * @return String
     */
    public static String formatDate(String date, String fromFormat, String toFormat) {
        SimpleDateFormat src = new SimpleDateFormat();
        SimpleDateFormat dst = new SimpleDateFormat();

        if (date == null || date.trim().length() == 0) {
            return "";
        }

        if (fromFormat == null || fromFormat.trim().length() == 0 || toFormat == null || toFormat.trim().length() == 0) {
            return null;
        }
        try {
            src.applyPattern(fromFormat);
            dst.applyPattern(toFormat);
            String result = dst.format(src.parse(date));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get MD5 value of string s
     *
     * @param s
     * @return ret
     */
    public static String hash(String s, String algorithm) {
        String ret = "";
        try {
            MessageDigest msg = MessageDigest.getInstance(algorithm);
            msg.update(s.getBytes());
            byte[] hash = msg.digest();
            ret = ISOUtil.hexString(hash);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    /**
     * Count character ch, in String s
     *
     * @param s
     * @param ch
     * @return amount of character in String s
     */
    public static int charCount(String s, String ch) {
        int ret = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (ch.equalsIgnoreCase(s.substring(i, i + 1))) {
                ret++;
            }
        }
        return ret;
    }

    /*
     * public static String convertThBl2BlTh(String data) { String ret =
     * (data.substring(4, 6) + data.substring(0, 4)); return ret; }
     *
     * public static String convertBlTh2ThBl(String data) { String ret =
     * (data.substring(2, 6) + data.substring(0, 2)); return ret; }
     */
    /**
     * Generate random unique ID.
     *
     * @return random generate UUID.
     */
    public static String getUUID() {
        UUID gen = UUID.randomUUID();
        String uuid = gen.toString();
        uuid = uuid.replace("-", "");
        uuid = uuid.toUpperCase();
        return uuid;
    }

    public static String generateQR() {
        String rrn = generateRrn();
        UUID gen = UUID.randomUUID();
        return (rrn + gen.toString().replace("-", ""));
    }

//    public static String generateQRBarang() {
//        UUID gen = UUID.randomUUID();
//        UUID gen2 = UUID.randomUUID();
//        return (gen.toString() + gen2.toString()).replace("-", "");
//    }
//
//    public static String generateQRUser() {
//        String rrn = generateRrn();
//        UUID gen2 = UUID.randomUUID();
//        return (rrn + gen2.toString()).replace("-", "");
//    }
    public static String generateRrn() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
        String dateString = sdf.format(new Date());
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return dateString + s.substring(s.length() - 6);
    }

    public static String generateEdcRrn() throws ISOException {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return ISOUtil.zeropad(s.substring(s.length() - 6), 12);
    }

    public static String generateStan() {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return s.substring(s.length() - 6);
    }

    public static String generateRandomNumber(int length) {
        UUID gen = UUID.randomUUID();
        long randomNumber = Math.abs(gen.getLeastSignificantBits());
        String stringRep = String.valueOf(randomNumber);
        String reff1 = stringRep.substring(0, length);
        return reff1;
    }

    public byte[] decrypt(String path, String plaintext) throws
            IllegalBlockSizeException,
            BadPaddingException,
            NoSuchPaddingException,
            InvalidKeySpecException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(path));
        byte[] rawkey = new byte[(int) path.length()];
        in.readFully(rawkey);
        in.close();

        DESedeKeySpec keyspec = new DESedeKeySpec(rawkey);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey key = keyfactory.generateSecret(keyspec);
        Cipher cipher = Cipher.getInstance("DES");
        byte[] ciphertext = cipher.doFinal(plaintext.getBytes());
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plaindecrypt = cipher.doFinal(ciphertext);
        return plaindecrypt;
    }

    /**
     * Hold decrypt until finish time.
     *
     * @param finish
     */
    public static void waitUntil(int finish) {
        int c = 0;
        while (true) {
            if (c < finish) {
                ISOUtil.sleep(1000);
                c++;
                continue;
            } else {
                break;
            }
        }
    }

    public static byte[] hex2Byte(String h) {
        if (h.length() % 2 != 0) {
            h = "0" + h;
        }

        int l = h.length() / 2;

        byte[] r = new byte[l];

        int i = 0;
        int j = 0;
        for (int k = h.length(); i < k; j++) {
            r[j] = Short.valueOf(h.substring(i, i + 2), 16).byteValue();

            i += 2;
        }

        return r;
    }

    public static String hex2String(String h) {
        return new String(hex2Byte(h));
    }

    public static String byte2Hex(byte[] b) {
        StringBuffer sbuf = new StringBuffer();

        int i = 0;
        for (int n = b.length; i < n; i++) {
            byte hiByte = (byte) ((b[i] & 0xF0) >> 4);
            byte loByte = (byte) (b[i] & 0xF);

            sbuf.append(Character.forDigit(hiByte, 16));
            sbuf.append(Character.forDigit(loByte, 16));
        }

        return sbuf.toString();
    }

    public static String String2Hex(String s) {
        return byte2Hex(s.getBytes());
    }

    public static String str2bcd(int argInt) {
        try {
            ByteArrayOutputStream o = new ByteArrayOutputStream();
            o.write(argInt >> 8);
            o.write(argInt);

            return o.toString("ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "0";
    }

    public static int bcd2str(String argInt) {
        try {
            byte[] b = argInt.getBytes("ISO8859_1");

            return (b[0] & 0xFF) << 8 | b[1] & 0xFF;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static int bcd2str(byte[] argInt) {
        try {
            return (argInt[0] & 0xFF) << 8 | argInt[1] & 0xFF;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String pad(String m, int pos, String with, int total) {
        if (m.length() < total) {
            StringBuffer s = new StringBuffer(m);

            while (s.length() < total) {
                if (pos == 0) {
                    s.insert(0, with);
                } else {
                    if (pos != 1) {
                        continue;
                    }
                    s.append(with);
                }
            }

            return s.toString();
        }

        if (pos == 0) {
            return m.substring(m.length() - total);
        }
        if (pos == 1) {
            return m.substring(0, total);
        }

        return m.substring(0, total);
    }

    public static String pad(String m, String pos, String with, int total) {
        if (m.length() < total) {
            StringBuffer s = new StringBuffer(m);

            while (s.length() < total) {
                if ("L".equals(pos)) {
                    s.insert(0, with);
                } else if ("R".equals(pos)) {
                    s.append(with);
                }
            }
            return s.toString();
        }

        if ("L".equals(pos)) {
            return m.substring(m.length() - total);
        }
        if ("R".equals(pos)) {
            return m.substring(0, total);
        }

        return m.substring(0, total);
    }

    public static String decode7bit(String src) {
        String result = null;
        String temp = null;
        byte left = 0;

        if ((src != null) && (src.length() % 2 == 0)) {
            result = "";
            int[] b = new int[src.length() / 2];
            temp = src + "0";

            int i = 0;
            int j = 0;
            for (int k = 0; i < temp.length() - 2; j++) {
                b[j] = Integer.parseInt(temp.substring(i, i + 2), 16);

                k = j % 7;
                byte srcAscii = (byte) (b[j] << k & 0x7F | left);
                result = result + (char) srcAscii;
                left = (byte) (b[j] >>> 7 - k);

                if (k == 6) {
                    result = result + (char) left;
                    left = 0;
                }
                if (j == src.length() / 2) {
                    result = result + (char) left;
                }
                i += 2;
            }
        }

        return result.trim();
    }

    public static Boolean compareDate(Date date1, Date date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddmmyy");
        String dateString1 = sdf.format(date1);

        String dateString2 = sdf.format(date2);
        if (dateString1.equals(dateString2)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public static boolean isValidValue(Pattern pattern, String value) {
        return pattern.matcher(value).matches();
    }

    public static JSONObject merge(JSONObject... jsonObjects) throws JSONException {

        JSONObject jsonObject = new JSONObject();

        for (JSONObject temp : jsonObjects) {
            Iterator<String> keys = temp.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                jsonObject.put(key, temp.get(key));
            }
        }
        return jsonObject;
    }

    public boolean sendFirebaseNotifFcm(String to, String title, String message) {

        log.info("TO FCM : " + to);
        log.info("TITLE FCM : " + title);
        log.info("MESSAGE FCM : " + message);

        HTTPQueryHost httpqh = new HTTPQueryHost();
        JSONObject requestNotif = new JSONObject();
        JSONObject requestDataNotif = new JSONObject();
        requestDataNotif.put(Constants.FCM.TITLE, title);
        requestDataNotif.put(Constants.FCM.MESSAGE, message);

        requestNotif.put(Constants.FCM.DATA, requestDataNotif);
        requestNotif.put(Constants.FCM.TO, to);

        String httpResponse = httpqh.httprequest(Constants.FCM.URL, requestNotif.toString());
        try {
            log.info("INI HTTP RESPONSE CUY : " + httpResponse);
            JSONObject jsonResponse = new JSONObject(httpResponse);

            int status = jsonResponse.getInt("success");
            if (status != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            log.info(ExceptionUtils.getStackTrace(e));
            return false;
        }

    }

}
