/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_cabang")
public class MCabang implements Serializable {

    private String kodeKantor;
    private String kodeInduk;
    private String email;
    private String nama;
    private String alamat;
    private String kota;
    private String noTelp;
    private String noFaks;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private int status;
    private MCabang cabang;
    private String request_kd_induk;
    private String request_alamat_kantor;
    private String request_kota_alamat;
    private String request_nama_kantor;
    private String request_no_faks;
    private String request_no_telp;
    private String request_email;
    private boolean status_approval;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }

    @Id
    @Column(name = "kd_kantor", length = 3, unique = true)
    public String getKodeKantor() {
        return kodeKantor;
    }

    @Column(name = "kd_induk", length = 3)
    public String getKodeInduk() {
        return kodeInduk;
    }

    @Column(name = "nama_kantor", length = 25)
    public String getNama() {
        return nama;
    }

    @Column(name = "email", length = 100)
    public String getEmail() {
        return email;
    }

    @Column(name = "alamat_kantor", length = 200)
    public String getAlamat() {
        return alamat;
    }

    @Column(name = "kota_alamat", length = 30)
    public String getKota() {
        return kota;
    }

    @Column(name = "no_telp", length = 20)
    public String getNoTelp() {
        return noTelp;
    }

    @Column(name = "no_faks", length = 20)
    public String getNoFaks() {
        return noFaks;
    }

    @Column(name = "cr_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "status", columnDefinition = "int default 0")
    public int getStatus() {
        return status;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MCabang.class)
    public MCabang getCabang() {
        return cabang;
    }

    @Column(name = "request_kd_induk", length = 3)
    public String getRequest_kd_induk() {
        return request_kd_induk;
    }

    @Column(name = "request_alamat_kantor", length = 200)
    public String getRequest_alamat_kantor() {
        return request_alamat_kantor;
    }

    @Column(name = "request_kota_alamat", length = 30)
    public String getRequest_kota_alamat() {
        return request_kota_alamat;
    }

    @Column(name = "request_nama_kantor", length = 25)
    public String getRequest_nama_kantor() {
        return request_nama_kantor;
    }

    @Column(name = "request_no_faks", length = 20)
    public String getRequest_no_faks() {
        return request_no_faks;
    }

    @Column(name = "request_no_telp", length = 20)
    public String getRequest_no_telp() {
        return request_no_telp;
    }

    @Column(name = "request_email", length = 100)
    public String getRequest_email() {
        return request_email;
    }

    @Column(name = "status_approval", columnDefinition = "boolean default false")
    public boolean isStatus_approval() {
        return status_approval;
    }

    //=============SET=============//
    public void setKodeKantor(String kodeKantor) {
        this.kodeKantor = kodeKantor;
    }

    public void setKodeInduk(String kodeInduk) {
        this.kodeInduk = kodeInduk;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public void setNoFaks(String noFaks) {
        this.noFaks = noFaks;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setCabang(MCabang cabang) {
        this.cabang = cabang;
    }

    public void setRequest_kd_induk(String request_kd_induk) {
        this.request_kd_induk = request_kd_induk;
    }

    public void setRequest_alamat_kantor(String request_alamat_kantor) {
        this.request_alamat_kantor = request_alamat_kantor;
    }

    public void setRequest_kota_alamat(String request_kota_alamat) {
        this.request_kota_alamat = request_kota_alamat;
    }

    public void setRequest_nama_kantor(String request_nama_kantor) {
        this.request_nama_kantor = request_nama_kantor;
    }

    public void setRequest_no_faks(String request_no_faks) {
        this.request_no_faks = request_no_faks;
    }

    public void setRequest_no_telp(String request_no_telp) {
        this.request_no_telp = request_no_telp;
    }

    public void setStatus_approval(boolean status_approval) {
        this.status_approval = status_approval;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRequest_email(String request_email) {
        this.request_email = request_email;
    }

}
