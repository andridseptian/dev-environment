package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author risyamaulana
 */
public class InquiryPrepaidParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());
    private final RcRepository rcRepository;
    private final ProductRepository paymentRepository;

    public InquiryPrepaidParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        paymentRepository = context.getBean(ProductRepository.class);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        String productCode = cfg.get("product", "-");
        String debet = cfg.get("debet", "-");
        int idbilling = cfg.getInt("idbilling",0);
        int denom = cfg.getInt("denom");
        String accountType = "A";
        String sourceAccountNumber = "";
        if ("-".equals(productCode)) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Invalid Product Code");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        MProduct productPayment = paymentRepository.findOne(productCode);
        if (null == productPayment) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Product Code [" + productCode + "] not found");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        if ("-".equals(debet)) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Invalid Product Code");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        //init Transaction
        trx.setAccountType(debet);
        trx.setProductCode(productPayment.getProductCode());
        switch (debet) {
            case "agent": //agent dibayar cash
                accountType = "A";
                sourceAccountNumber = agent.getAccountNumber();
                break;
            case "customer": //customer menggunakan akun nasabah
                accountType = "C";
                sourceAccountNumber = nasabah.getAccountNumber();
                break;
            default:
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setDetail("Invalid Source Account Number");
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED | NO_JOIN;
        }
        
        //init inquiry
        JSONObject respHasil = Service.servicePayment(
                agent.getId(),
                agent.getPin(),
                sourceAccountNumber,
                productPayment.getProductCode(),
                request[idbilling],
                0,
                0,
                0,
                "",
                Constants.TRX_TYPE.INQUIRY,
                "",
                accountType,
                trx.getStan());
        if (respHasil == null) {
            return ABORTED | NO_JOIN;
        }
        ctx.log("Bank Resp : " + respHasil.toString());
//        trx.setBankResponse(respHasil.toString());
        if (respHasil.has(Constants.PARAM.WS_RESPONSECODE)
                && respHasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
            String rc = respHasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = respHasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            trx.setBankRc(rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(productCode, rc);
            if (mapRc == null) {
                mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_PAYMENT_INQUIRY, rc);
            }
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rm);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRc() + " | " + mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                ctx.put("DENOM_PULSA", request[denom]);
                ctx.put(Constants.OBJ.BANKRESP, respHasil);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.OBJ.PRODUCTPAYMENT, productPayment);
                return PREPARED;
            } else {
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED;
            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
