/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatPostingSMSTransferOnUs implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.log("--FormatPostingSMSTransferOnUs--");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_TRANSFER_ON_US.name());
        try {
            trx.setInfo(session.getTargetNasabahName());
            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[TargetAccount]", session.getTargetNasabahAccountNumber())
                    .replace("[TargetName]", session.getTargetNasabahName().length()<=10?session.getTargetNasabahName():session.getTargetNasabahName().substring(0, 10))
                    .replace("[Status]", trx.viewStatus())
                    .replace("[Ref]", trx.getOriginReff());
            if (sendSMS) {
//                boolean statusSMSAgent = Utility.sendSMS(response, session.getAgent().getMsisdn());
//                trx.setSmsInfo(trx.getSmsInfo()+ "SMS " + trx.getDescription() + " Agent [" + session.getAgent().getMsisdn() + "] = " + statusSMSAgent);
                JSONObject hasil = Service.serviceBalanceInquiry(session.getAgent().getId(), session.getNasabah().getAccountNumber());
                double saldoNasabah = 0;
                if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
                    String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
                    if (rc.equals(Constants.RC_APPROVED)) {
                        saldoNasabah = Utility.getParseAmount(hasil.getString("balance"),100);
                    } else {
                        saldoNasabah = -1;
                    }
                } else {
                    saldoNasabah = -1;
                }
                if (saldoNasabah != -1) {
                    try {
                        response = response + Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_ADD_SALDO.name())
                                .replace("[Balance]", Utility.formatAmount(saldoNasabah));
                        log.info("response:" + response);
                    } catch (Exception ex) {
                        log.error(ex.getMessage());
                    }
                }
                boolean statusSMSNasabah = Utility.sendSMS(response, session.getNasabah().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Nasabah [" + session.getNasabah().getMsisdn() + "] = " + statusSMSNasabah);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
//            session.setRcToClient(Constants.RC_APPROVED);
//            session.setResponseToClient(response);
        } catch (Exception ex) {
            ctx.log(ex);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Constants.RM_INVALID_REQUEST);
        }

        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String response = Service.getResponseFromSettings(Constants.RM_FORMAT_SMS_POSTING_TRANSFER_ON_US);
        try {
            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[TargetAccount]", session.getTargetNasabahAccountNumber())
                    .replace("[TargetName]", session.getTargetNasabahName())
                    .replace("[Status]", trx.viewStatus())
                    .replace("[Ref]", trx.getOriginReff());
            if (sendSMS) {
//                boolean statusSMSAgent = Utility.sendSMS(response, session.getAgent().getMsisdn());
//                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Agent [" + session.getAgent().getMsisdn() + "] = " + statusSMSAgent);
                JSONObject hasil = Service.serviceBalanceInquiry(session.getAgent().getId(), session.getNasabah().getAccountNumber());
                double saldoNasabah = 0;
                if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
                    String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
                    if (rc.equals(Constants.RC_APPROVED)) {
                        saldoNasabah = Utility.getParseAmount(hasil.getString("balance"),100);
                    } else {
                        saldoNasabah = -1;
                    }
                } else {
                    saldoNasabah = -1;
                }
                if (saldoNasabah != -1) {
                    try {
                        response = response + Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_ADD_SALDO.name())
                                .replace("[Balance]", Utility.formatAmount(saldoNasabah));
                        log.info("response:" + response);
                    } catch (Exception ex) {
                        log.error(ex.getMessage());
                    }
                }
                boolean statusSMSNasabah = Utility.sendSMS(response, session.getNasabah().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Nasabah [" + session.getNasabah().getMsisdn() + "] = " + statusSMSNasabah);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
        } catch (Exception ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Constants.RM_INVALID_REQUEST);
        }
        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }

}
