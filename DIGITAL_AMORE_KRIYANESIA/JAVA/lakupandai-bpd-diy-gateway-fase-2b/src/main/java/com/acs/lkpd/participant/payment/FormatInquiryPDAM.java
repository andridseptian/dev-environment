package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ACS
 */
public class FormatInquiryPDAM implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        JSONObject bankResp = (JSONObject) ctx.get(Constants.OBJ.BANKRESP);
        MProduct productPayment = (MProduct) ctx.get(Constants.OBJ.PRODUCTPAYMENT);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        if (productPayment == null) {
            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
            session.setResponseToClient("Product tidak ditemukan atau tidak terdaftar, mohon coba beberapa saat lagi / hubungi admin untuk tindakan lebih lanjut");
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        String mobileResponse = "", otpResponse = "";

        JSONArray formatInquiryArray = null;
        try {
            JSONObject additionalData = new JSONObject(bankResp.getString(Constants.PARAM.WS_PAY_ADDITIONALDATA));
            ctx.put(Constants.ADDITIONALDATA, additionalData);
            
            double amount = Utility.getParseAmount(bankResp.getString(Constants.PARAM.WS_PAY_TRANSACTIONAMMOUNT), 100);
            String productName = productPayment.getProductName();
            String nop = additionalData.getString("idPelanggan");
            String nama = additionalData.getString("nama");
            String bulanTagihan = additionalData.getString("jmlBulanTagihan");
            ctx.put(Constants.JUMLAHBULAN, bulanTagihan);
            double fee = productPayment.getFeeBank() * Double.valueOf(bulanTagihan);
//            double fee = trx.getFeeAgent() + trx.getFeeBank();
            double totalAmount = amount + fee;

            formatInquiryArray = new JSONArray(productPayment.getFormatInquiry());
            for (int i = 0; i < formatInquiryArray.length(); i++) {
                JSONObject data = formatInquiryArray.getJSONObject(i);

                data.put("value", data.getString("value")
                        .replace("[LAYANAN]", productPayment.getProductName())
                        .replace("[NO_TAGIHAN]", nop)
                        .replace("[NAMA]", nama)
                        .replace("[TAHUN]", bulanTagihan)
                        .replace("[NOMINAL]", Utility.formatAmount(amount))
                        .replace("[BIAYA_ADMIN]", Utility.formatAmount(fee))
                        .replace("[JUMLAH_BIAYA]", Utility.formatAmount(totalAmount)));
            }
            mobileResponse = new JSONObject().put("data", formatInquiryArray).toString();

            //Build Mobile response
//            JSONArray array = new JSONArray();
//            array.put(0, Utility.formatParameter("Layanan", "Pajak Daerah Kota"));
//            array.put(1, Utility.formatParameter("No.Tagihan", nop));
//            array.put(2, Utility.formatParameter("Nama", nama));
//            array.put(3, Utility.formatParameter("Tahun", tahun));
//            array.put(4, Utility.formatParameter("Nominal", "Rp." + Utility.formatAmount(amount)));
//            array.put(5, Utility.formatParameter("Biaya Admin", "Rp." + Utility.formatAmount(fee)));
//            array.put(6, Utility.formatParameter("Jumlah Biaya", "Rp." + Utility.formatAmount(totalAmount)));
//            mobileResponse = new JSONObject().put("data", array).toString();
            //Build SMS OTP Response
            String formatSMS = productPayment.getFormatSMSToken();
            otpResponse = formatSMS
                    .replace("[product]", productName)
                    .replace("[IdPelanggan]", nop)
                    .replace("[Name]", nama)
                    .replace("[BulanTagihan]", bulanTagihan)
                    .replace("[Amount]", Utility.formatAmount(amount))
                    .replace("[Price]", Utility.formatAmount(totalAmount));
            trx.setBankReff(bankResp.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY));
            trx.setAmount(amount);
            trx.setFeeAgent(productPayment.getFeeAgent());
            trx.setFeeBank(productPayment.getFeeBank());
            trx.setTotalAmount(trx.getAmount() + trx.getFeeBank());
            trx.setMobileResponse(mobileResponse);
            trx.setSmsResponse(otpResponse);
            trx.setIdBilling(bankResp.getString("idBilling"));
            trx.setDescription(productPayment.getProductName());
            trx.setAdditionalData(additionalData.toString());
            trx.setInfo(bulanTagihan);
            session.setAmount(trx.getTotalAmount());
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(mobileResponse);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            ctx.log(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
