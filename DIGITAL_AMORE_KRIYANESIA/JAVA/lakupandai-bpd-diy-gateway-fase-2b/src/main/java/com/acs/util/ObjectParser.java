/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.util;

import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotificationHistory;
import com.acs.lkpd.model.MTransaction;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ACS
 */
public class ObjectParser {

    public static String listMutasiView(List<MTransaction> listTransaksi) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return true != Arrays.asList(
                        "id",
                        "description",
                        "crTime",
                        "nasabah",
                        "totalAmount"
                ).contains(f.getName());
            }

            @Override
            public boolean shouldSkipClass(Class<?> arg0) {
                return false;
            }
        });
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .registerTypeAdapter(MNasabah.class, new NasabahAdapter());
        Gson gson = gsonBuilder.create();
        return gson.toJson(listTransaksi);
    }
    
    public static String listNotificationHistoryView(List<MNotificationHistory> listHistory) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return true != Arrays.asList(
                        "rrn",
                        "title",
                        "message",
                        "image",
                        "agent",
                        "crTime"
                ).contains(f.getName());
            }

            @Override
            public boolean shouldSkipClass(Class<?> arg0) {
                return false;
            }
        });
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .registerTypeAdapter(MNasabah.class, new NasabahAdapter());
        Gson gson = gsonBuilder.create();
        return gson.toJson(listHistory);
    }

    public static class NasabahAdapter implements JsonSerializer<MNasabah> {

        @Override
        public JsonElement serialize(MNasabah n, java.lang.reflect.Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(n.getName());
        }
    }

    public static class LocalDateTimeAdapter implements JsonSerializer<LocalDateTime> {

        @Override
        public JsonElement serialize(LocalDateTime t, java.lang.reflect.Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(t.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss"))); // "yyyy-mm-dd"
        }
    }
}
