
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MLogEmail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface EmailRepository extends JpaRepository<MLogEmail, Long>{
    
}
