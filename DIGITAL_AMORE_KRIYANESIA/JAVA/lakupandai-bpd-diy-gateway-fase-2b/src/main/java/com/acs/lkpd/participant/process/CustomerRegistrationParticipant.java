/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.SettingRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CustomerRegistrationParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private final NasabahRepository nasabahRepository;
    private final SettingRepository settingRepository;

    Log log = Log.getLog("Q2", getClass().getName());

    public CustomerRegistrationParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        nasabahRepository = context.getBean(NasabahRepository.class);
        settingRepository = context.getBean(SettingRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        int fieldNIK = cfg.getInt("field_nik", 0);
        int fieldName = cfg.getInt("field_name", 1);
        int fielddob = cfg.getInt("field_dob", 2);
        int fieldMsisdn = cfg.getInt("field_msisdn", 3);
        String iNik = request[fieldNIK];
        String iName = request[fieldName];
        String iDob = request[fielddob];
        iDob = iDob.replace("/", "");
        log.info("DOB " + iDob);
        String iMsisdn = request[fieldMsisdn];
        String formatMsisdn = Utility.formatMsisdn(iMsisdn);
//        MNasabah nasabah = nasabahRepository.findFirstByNikAndMsisdnOrderByCrTimeDesc(iNik, formatMsisdn);
        MNasabah nasabah = nasabahRepository.findFirstByMsisdnOrderByCrTimeDesc(formatMsisdn);
        if (nasabah != null) {
            log.info("Nasabah Not NULL");
            log.info("Nasabah Status: " + nasabah.getStatus());
            switch (nasabah.getStatus()) {
                case ACTIVE: {
                    session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_DUPLICATE.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case UNDEF: {
                    session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_DUPLICATE.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
//                case NEED_APPROVAL:
//                case NEED_APPROVAL_CS: {
//                    nasabah.setAgentId(agent);
//                    nasabah.setMsisdn(formatMsisdn);
//                    ctx.put(Constants.NASABAH, nasabah);
//                    ctx.put(Constants.SESSION, session);
//                    return PREPARED;
//                }
                case INACTIVE: {
                    session.setRcToClient(Constants.RC_ACCOUNT_NASABAH_DELETED);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_INACTIVE.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case BLOCK: {
                    session.setRcToClient(Constants.RM_ACCOUNT_AGENT_INACTIVE);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_BLOCK.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
            }
        }
        MNasabah nasabahNik = nasabahRepository.findFirstByNikAndStatusOrderByCrTimeDesc(iNik, Constants.Status.ACTIVE);
        if (nasabahNik != null && nasabahNik.getStatus() == Constants.Status.ACTIVE) {
            session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_NIK_DUPLICATE.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }

        //--------------CEK NIK PADA TABEL-----------------
        log.info("Agent Type: " + agent.getAgentType());
        MNasabah newNasabah = new MNasabah();
        newNasabah.setNik(iNik);
        newNasabah.setName(iName);
        newNasabah.setMsisdn(formatMsisdn);
        newNasabah.setDob(iDob);
        newNasabah.setAgentId(agent);
        newNasabah.setStatus(Constants.Status.NEED_APPROVAL);
        newNasabah.setNasabahType(agent.getAgentType());
        session.setNasabah(newNasabah);
        ctx.put(Constants.NASABAH, newNasabah);
        ctx.put(Constants.SESSION, session);
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        MNasabah saveNasabah = nasabahRepository.save(nasabah);
        if (saveNasabah == null) {
            session.setRcToClient(Constants.RC_ACCOUNT_NASABAH_NOTFOUND);
            session.setResponseToClient("Data Nasabah gagal disimpan, mohon hubungi admin untuk tindakan lebih lanjut");
            ctx.put(Constants.SESSION, session);
        } else {
            long timeout = Long.parseLong(settingRepository.findOne(KeyMap.TIMEOUT_OTP.name()).getValue());
            session.setNasabah(nasabah);
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(timeout + "|" + nasabah.getMsisdn());
            ctx.put(Constants.SESSION, session);
        }
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }
}
