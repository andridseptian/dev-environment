/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNasabah;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class FormatRegistrasiNasabah implements TransactionParticipant, org.jpos.core.Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_REGISTRASI_CUSTOMER.name());
        
        log.info("RESPONSE SMS : " + response);
        log.info("NASABAH MSISDN : " + nasabah.getMsisdn());
               
        try {
            response = response.replace("[Msisdn]", Utility.viewMsisdn(nasabah.getMsisdn()))
                    .replace("[Name]", nasabah.getName())
                    .replace("[NoRekening]", nasabah.getAccountNumber());
            boolean statusSms = Utility.sendSMS(response, nasabah.getMsisdn());
            session.setDetail(session.getDetail()+"|sms info registrasi nasabah success["+statusSms+"]");
            
            log.info("SESSION : " + session.getDetail() + " STATUS SMS : " + statusSms);
            
        } catch (Exception ex) {
            session.setDetail(session.getDetail()+"|sms info registrasi nasabah failed");
        }
        ctx.put(Constants.SESSION, session);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
