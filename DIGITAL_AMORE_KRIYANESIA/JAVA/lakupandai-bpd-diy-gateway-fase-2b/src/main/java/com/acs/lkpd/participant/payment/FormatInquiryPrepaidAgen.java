package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author risyamaulana
 */
public class FormatInquiryPrepaidAgen implements TransactionParticipant, Configurable{

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        String billingId = request[0];
        String requestDenom = request[1];
//        JSONObject bankResp = (JSONObject) ctx.get(Constants.OBJ.BANKRESP);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
//        String response = Service.getResponseFromSettings(KeyMap.RM_INQUIRY_TELKOMSEL_POSTPAID.name());
        String response = new String();
        
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        try {
            
            double denom = Double.valueOf(requestDenom);
//            String respCode = bankResp.getString("responseCode");
//            String respDesc = bankResp.getString("responseDesc");
//            String accType = bankResp.getString("accountType");

            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[Denom]", Utility.formatAmount(denom))
                            .replace("[Msisdn]", billingId)
                            .replace("[Price]", Utility.formatAmount(denom))
                            .replace("[Desc]", Service.getResponseFromSettings(KeyMap.DESC_INQ_PREPAID_AGEN.name())));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();

//                trx.setBankReff(bankResp.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY));
                trx.setAmount(denom);
                trx.setFeeAgent(product.getFeeAgent());
                trx.setFeeBank(product.getFeeBank());
                trx.setTotalAmount(trx.getAmount() + trx.getFeeBank());
                trx.setAccountType("agent");
                trx.setProductCode(product.getProductCode());
                
                trx.setIdBilling(billingId);
                trx.setDescription(product.getProductName());
                trx.setMobileResponse(response);
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";
                trx.setMobileResponse(response);
                
            }

//            JSONObject additionalData = new JSONObject(bankResp.getString(Constants.PARAM.WS_PAY_ADDITIONALDATA));
//            ctx.log("Additional data " + additionalData);
////            double amount = Utility.getParseAmount(bankResp.getString(Constants.PARAM.WS_PAY_TRANSACTIONAMMOUNT), 100);
//            response = response.replace("[Amount]", Utility.formatAmount(amount))
//                    .replace("[MSISDN]", Utility.viewMsisdn(bankResp.getString("idBilling")))
//                    .replace("[Name]", additionalData.getString("nama"));
//            switch (trx.getAccountType()) {
//                case "customer":
//                    response = response + Service.getResponseFromSettings(Constants.RM_MESSAGE_TOKEN);
//                    break;
//            }
//            trx.setAdditionalData(additionalData.toString());
            trx.setSmsResponse("Proses " + product.getProductName() + " " + Utility.formatAmount(denom) + " akan segera diproses. Silahkan masukan token ini [otp]");
            
            trx.setInfo(response);
            
            session.setAmount(trx.getTotalAmount());
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return PREPARED;
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            ctx.log(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
    
}
