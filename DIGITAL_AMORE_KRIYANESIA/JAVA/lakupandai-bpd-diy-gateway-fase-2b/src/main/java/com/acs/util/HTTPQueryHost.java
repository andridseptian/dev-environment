package com.acs.util;

import com.acs.lkpd.enums.Constants;
import java.io.IOException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jpos.util.Log;

/**
 *
 * @author risyamaulana
 */
public class HTTPQueryHost {
    
    
     public String httprequest(String url, String body) {
        
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPost request = new HttpPost(url);
            StringEntity params = new StringEntity(body);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", Constants.FCM.KEY);
            request.setEntity(params);
            HttpResponse result = httpClient.execute(request);
            
            
            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            
            return json;
            
        } catch (IOException ex) {
            return null;
        }
        
    }
    
}
