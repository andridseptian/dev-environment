/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_listbank")
public class MDataBank implements Serializable {

    private String bankCode;
    private String bankName;
    private int priority;
    private int status;

    @Id
    @Column(name = "bank_code", length = 3)
    public String getBankCode() {
        return bankCode;
    }

    @Column(name = "bank_name", length = 100)
    public String getBankName() {
        return bankName;
    }

    @Column(name = "priority", columnDefinition = "int default 2")
    public int getPriority() {
        return priority;
    }

    @Column(name = "status", columnDefinition = "int default 0")
    public int getStatus() {
        return status;
    }

    //SET
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
