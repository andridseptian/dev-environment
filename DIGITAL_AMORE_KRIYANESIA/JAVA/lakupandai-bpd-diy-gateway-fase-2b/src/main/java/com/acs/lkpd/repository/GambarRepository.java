/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MGambar;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Andri D Septian
 */
public interface GambarRepository extends JpaRepository<MGambar, Long> {
//    
//    @Query("SELECT t From MGambar")
//    List<MGambar> mutasiTransaksiAgent();

    MGambar findFirstById(Long id);
    
    MGambar findFirstByName(String name);
    
}
