/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.payment;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MTransaction;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatPaymentTelkomselPostpaid implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.OBJ.PRODUCTPAYMENT);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        String response = product.getFormatPayment();
        try {
            JSONObject additionalData = new JSONObject(trx.getAdditionalData());
            response = response.replace("[Amount]", Utility.formatAmount(trx.getAmount()))
                    .replace("[MSISDN]", Utility.viewMsisdn(trx.getIdBilling()))
                    .replace("[Name]", additionalData.getString("nama"))
                    .replace("[Status]", trx.viewStatus()
                    .replace("[Ref]", trx.getOriginReff()));
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            trx.setInfo(response);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (Exception ex) {
            ctx.log(ex);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            return ABORTED | NO_JOIN;
        }
    }

}
