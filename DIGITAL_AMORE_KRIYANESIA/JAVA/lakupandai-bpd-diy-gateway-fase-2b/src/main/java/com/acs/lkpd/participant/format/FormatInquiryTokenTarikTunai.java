package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.util.Service;
import org.jpos.util.Log;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatInquiryTokenTarikTunai implements TransactionParticipant {

    private Log log = Log.getLog("Q2", getClass().getName());
    
    

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String response = Service.getResponseFromSettings(Constants.RM_INQUIRY_TOKEN_TARIK_TUNAI);
        String[] request = (String[]) ctx.get(Constants.REQ);
        try {
            MOtp otp = Service.findOTP(request[0]);

            String otpProduct = otp.getProduct().toString();
            otpProduct = otpProduct.substring(0, 3);

            if (!otpProduct.equals("210")) {
                if (!otpProduct.equals("220")) {
                    if (!otpProduct.equals("230")) {
                        session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                        session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
                        ctx.put(Constants.SESSION, session);
                        return ABORTED | NO_JOIN;
                    }
                }
            }

            MSession tempSession = Service.findSession(otp.getOtp(), agent);
            session.setNasabah(tempSession.getNasabah());
            session.setAmount(tempSession.getAmount());
            ctx.put(Constants.SESSION, session);
//            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
//                    .replace("[Msisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
//                    .replace("[Name]", session.getNasabah().getName());
//            
//            response = response + Service.getResponseFromSettings(Constants.RM_MESSAGE_PIN);

            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[Amount]", "Rp." + Utility.formatAmount(session.getAmount()))
                            .replace("[NasabahName]", session.getNasabah().getName())
                            .replace("[Desc]", Service.getResponseFromSettings(Constants.RM_MESSAGE_PIN))
                            .replace("[Msisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn())));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";

            }

            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (IndexOutOfBoundsException | NumberFormatException | NullPointerException ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
    }

}
