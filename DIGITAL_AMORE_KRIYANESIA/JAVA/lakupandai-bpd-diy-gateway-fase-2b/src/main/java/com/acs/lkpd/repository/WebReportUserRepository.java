package com.acs.lkpd.repository;

import com.acs.lkpd.model.MCabang;
import com.acs.lkpd.model.WebReportUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface WebReportUserRepository extends JpaRepository<WebReportUser, Long> {

    WebReportUser findFirstByCabangAndJabatanOrderByCrTimeDesc(MCabang cabang, String jabatan);
}
