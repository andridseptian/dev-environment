/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MAgentTax;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentTaxRepository;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AccountTransactionProcessSetorTunai implements TransactionParticipant, Configurable {

    Log log = Log.getLog("Q2", getClass().getName());

    protected Configuration cfg;
    private RcRepository rcRepository;
    private NotificationTrxRepository notTransactionRepository;
    private TransactionRepository transactionRepository;
    private AgentTaxRepository agentTaxRepository;
    private ProductRepository productRepository;

    public AccountTransactionProcessSetorTunai() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
        notTransactionRepository = context.getBean(NotificationTrxRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
        agentTaxRepository = context.getBean(AgentTaxRepository.class);
        productRepository = context.getBean(ProductRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.log("--Account Transaction Process Setor Tunai--");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MAgent agent = session.getAgent();
        MNasabah nasabah = session.getNasabah() != null ? session.getNasabah() : null;
        int fieldPin = cfg.getInt("field_pin", 0);

        try
        {
            ctx.log("agent : " + agent.getName());
            ctx.log("nasabah : " + nasabah != null ? nasabah.getName() : "-");
            ctx.log("nasabah_non_bsa : " + session.getTargetNasabahAccountNumber() != null ? session.getTargetNasabahAccountNumber() : "-");
        } catch (Exception e)
        {
            ctx.log("gagal log target");
            log.error(e);
        }

        //Set Fee
        double feeAgent = 0,
                feeBank = 0;
        try
        {
            feeAgent = Double.parseDouble(Service.findSettings(KeyMap.FEE_AGENT_SETOR_TUNAI.name()));
            ctx.log("FeeAgent:" + feeAgent);
        } catch (Exception ex)
        {
            ctx.log(ex.getMessage());
            feeAgent = 0;
        }
        try
        {
            feeBank = Double.parseDouble(Service.findSettings(KeyMap.FEE_BANK_SETOR_TUNAI.name()));
            ctx.log("FeeBank:" + feeBank);
        } catch (Exception ex)
        {
            ctx.log(ex.getMessage());
            feeBank = 0;
        }
//        trx.setFeeAgent(feeAgent);
//        trx.setFeeBank(feeBank);
        trx.setStatus(Constants.TRX_STATUS.PENDING);
        trx.setBankResponse("SEDANG DIPROSES");

        trx = transactionRepository.save(trx);
        //Set Amount
        double totalAmount = trx.getAmount();

        MProduct product = productRepository.findByProductCode(session.getProduct().getProductCode());

        ctx.log("product: " + product.getProductCode());
        ctx.log("product name: " + product.getProductName());

        feeAgent = product.getFeeAgent();
        feeBank = product.getFeeBank();

//        MProduct product = productRepository.findByProductCode(trx.getProductCode());
        Constants.AgentAccountType acType = agent.getAccountType();
        String acString = acType.toString();
        String NPWP = agent.getNpwp();
        MAgentTax mtax = agentTaxRepository.findByAccountType(acString);
        double feePajak = 0.0;

//        log.info(product.getFeeAgent());
        log.info("PRODUCT CODE" + trx.getProductCode());

//        int NPWPin = Integer.parseInt(NPWP);
//        long NPWPin = Integer.parseInt(NPWP);
        if (NPWP.length() > 19)
        {
            NPWP = NPWP.substring(0, 19);
        }
        long NPWPin = Long.parseLong(NPWP);
        log.info("NPWP : " + NPWPin);
        if (NPWPin <= 0)
        {
            feePajak = feeAgent * mtax.getTaxNonNpwp() * mtax.getAddtionalTax();
            log.info("BUKAN NPWP");
            String pesan = "BUKAN NPWP"
                    + "\nfeeAgent " + feeAgent
                    + "\ntax " + mtax.getTaxNonNpwp()
                    + "\nadditional " + mtax.getAddtionalTax()
                    + "\nfeePajak " + feePajak;
            log.info(pesan);
        } else
        {
            feePajak = feeAgent * mtax.getTaxNpwp() * mtax.getAddtionalTax();
            String pesan = "NPWP"
                    + "\nfeeAgent " + feeAgent
                    + "\ntax " + mtax.getTaxNpwp()
                    + "\nadditional " + mtax.getAddtionalTax()
                    + "\nfeePajak " + feePajak;
            log.info(pesan);
        }

        feePajak = Math.round(feePajak);
        trx.setFeePajak(feePajak);
        transactionRepository.save(trx);

        String targetMSISDN = nasabah != null ? nasabah.getMsisdn()
                : session.getTargetNasabahAccountNumber() != null ? session.getTargetNasabahAccountNumber()
                : "-";

        ctx.log("targetMSISDN: " + targetMSISDN);

        JSONObject hasil = Service.serviceAccountTransaction(
                agent.getId(),
                agent.getPin(),
                agent.getAccountNumber(),
                targetMSISDN,
                totalAmount,
                feeBank,
                feeAgent,
                feePajak,
                trx.getOriginReff(),
                trx.getDescription() + "[agent:" + agent.getAccountNumber() + ",nasabah:" + targetMSISDN + "]",
                3,
                null);

        log.info("BANK RESP DETAIL: " + hasil.toString());
        log.info("ID TRX :" + trx.getId());
        log.info("ORIGIN REFF TRX :" + trx.getOriginReff());

        trx.setBankResponseDetail(hasil.toString());
        trx = transactionRepository.save(trx);
        transactionRepository.save(trx);

        if (hasil.has(Constants.PARAM.WS_RESPONSECODE))
        {
            trx.setBankResponseDetail(hasil.toString());
            trx = transactionRepository.save(trx);
//            trx.setBankReff(hasil.getString("rrn"));
            MNotifikasiTrx notif = new MNotifikasiTrx();
            ctx.log("INPUT KE DATABASE");
            try
            {

                notif.setCrTime(trx.getCrTime());
                notif.setOriginReff(trx.getOriginReff());
                notif.setStan(trx.getStan());
                notif.setType(Constants.TRX_TYPE.POSTING);
                notif.setDescription(product.getProductName());
//                notif.setDescription(trx.getProductName());
                notif.setStatus(Constants.TRX_STATUS.PENDING);
                notif.setNasabah(trx.getNasabah());
                notif.setAgent(trx.getAgent());
                notif.setAmount(trx.getAmount());
                notif.setFeeAgent(feeAgent);
                notif.setFeeBank(feeBank);
                notif.setTotalAmount(trx.getTotalAmount());
                notif.setProductCode(product.getProductCode());
                notif.setOtp(session.getOtp());
                notif.setAccountType(trx.getAccountType());
                notif.setIdBilling(trx.getIdBilling());
                notif.setAdditionalData(trx.getAdditionalData());
                notif.setBankResponseDetail(hasil.toString());
                notif.setBankReff(trx.getOriginReff());
                notif.setIsRead(Boolean.FALSE);

                log.info("FEE AGENT: " + feeAgent);
                log.info("FEE BANK: " + feeBank);
                trx.setFeeAgent(feeAgent);
                trx.setFeeBank(feeBank);
                trx = transactionRepository.save(trx);
                transactionRepository.save(trx);
                ctx.put(Constants.TXLOG, trx);

//                notif.setMobileResponse(ctx.getString(Constants.HEADER_VALUE));
                ctx.put(Constants.NOTIFIKASI_LOG, notif);
                notif = notTransactionRepository.save(notif);
                ctx.log("BERHASIL");
            } catch (Exception ex)
            {
                ctx.log("GAGAL");
                ex.printStackTrace();
            }

            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC))
            {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
                if (rc.equals(Constants.RC_APPROVED))
                {
                    trx.setStatus(Constants.TRX_STATUS.SUCCESS);
                    notif.setStatus(Constants.TRX_STATUS.SUCCESS);
                } else
                {
                    trx.setStatus(Constants.TRX_STATUS.FAILED);
                    notif.setStatus(Constants.TRX_STATUS.FAILED);
                }

                trx = transactionRepository.save(trx);
                notif = notTransactionRepository.save(notif);
                ctx.put(Constants.TXLOG, trx);
            }
            ctx.log("Check " + ctx.get(Constants.GROUP) + " " + rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(ctx.getString(Constants.GROUP), rc);
            if (mapRc == null)
            {
                session.setRcToClient(rc);
                session.setResponseToClient(rm);
                trx.setBankRc(rc);
                trx.setBankResponse(rm);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return PREPARED;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRc() + " | " + mapRc.getRm());
            trx.setBankRc(mapRc.getRc());
            trx.setBankResponse(mapRc.getRm());
//            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            return PREPARED;
//            } else {
//                ctx.put(Constants.SESSION, session);
//                ctx.put(Constants.TXLOG,trx);
//                return ABORTED;
//            }
        } else
        {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            return PREPARED;
        }
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        if (trx.getBankRc().equals(Constants.RC_APPROVED))
        {
            trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        } else
        {
            trx.setStatus(Constants.TRX_STATUS.FAILED);
        }

        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

}
