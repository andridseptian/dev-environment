package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRequest;
import com.acs.util.EncryptUtility;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;

/**
 *
 * @author ACS
 */
public class AgentPasswordParticipant implements TransactionParticipant, Configurable {

    private Configuration cfg;

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String response = "", pin = "";
        response = Service.getResponseFromSettings(KeyMap.RM_OTP_AGENT.name());
        try {
            EncryptUtility eu = new EncryptUtility(Service.findSettings(KeyMap.DES_KEY.name()));
            pin = eu.decrypt(agent.getPin());
        } catch (Exception ex) {
            ctx.log(ex.getCause());
            String rm = "Password agent gagal terbentuk";
            req.setInfo(rm);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        if (sendSMS) {
            response = response
                    .replace("[UserId]", agent.getId())
                    .replace("[Pin]", pin);
            if (sendSMS) {
                boolean statusSMSAgent = Utility.sendSMS(response, agent.getMsisdn());
                req.setInfo(req.getInfo() + "SMS Agent [password] = " + statusSMSAgent);
            } else {
                req.setInfo(req.getInfo() + "Not Send SMS Agent [password] " + agent.getId());
            }
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return PREPARED | NO_JOIN;
        } else {
            String rm = "Not Send SMS Password Agent";
            req.setInfo(rm);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
