/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import com.acs.lkpd.repository.ProductRepository;

/**
 *
 * @author ACS
 */
public class InquiryPaymentParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());
    private final RcRepository rcRepository;
    private final ProductRepository paymentRepository;

    public InquiryPaymentParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        paymentRepository = context.getBean(ProductRepository.class);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);

        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String debet = cfg.get("debet", "-");

        int idbilling = cfg.getInt("idbilling", 0);
        String accountType = "A";
        String sourceAccountNumber = "";

        if ("-".equals(debet)) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Invalid Product Code");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        String noBiller = "";
        if (product.getMenu().equalsIgnoreCase("PAJAK DAERAH")) {
            if (product.getFormatAdditionalInformation() == null) {
                noBiller = "" + request[idbilling];
            } else {
                noBiller = "" + product.getFormatAdditionalInformation() + request[idbilling];
            }

        } else {
            noBiller = request[idbilling];
        }

        String productCode = product.getProductCode();
        //init Transaction
        trx.setAccountType(debet);
        trx.setProductCode(productCode);
        switch (debet) {
            case "agent": //agent dibayar cash
                accountType = "A";
                sourceAccountNumber = agent.getAccountNumber();
                productCode = productCode.replaceFirst("5", "0");
                break;
            case "customer": //customer menggunakan akun nasabah
                accountType = "C";
                sourceAccountNumber = nasabah.getAccountNumber();
                break;
            default:
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setDetail("Invalid Source Account Number");
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED | NO_JOIN;
        }

        //init inquiry
        JSONObject respHasil = Service.servicePaymentString(
                agent.getId(),
                agent.getPin(),
                sourceAccountNumber,
                productCode,
                noBiller,
                0,
                0,
                0,
                0,
                "",
                Constants.TRANSACTION.TYPE.INQUIRY,
                "",
                accountType,
                trx.getStan());
        if (respHasil == null) {
            return ABORTED | NO_JOIN;
        }
        ctx.log("Bank Resp : " + respHasil.toString());
        if (respHasil.has(Constants.PARAM.WS_RESPONSECODE)
                && respHasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
            String rc = respHasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = respHasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(productCode, rc);
            if (mapRc == null) {
                mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_PAYMENT_INQUIRY, rc);
            }
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rm);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRc() + " | " + mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                ctx.put(Constants.OBJ.BANKRESP, respHasil);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.OBJ.PRODUCTPAYMENT, product);
                return PREPARED;
            } else {
                if (mapRc.getRc().equals("68")) {
                    session.setResponseToClient(mapRc.getRc() + " | " + rm);
                }
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED;
            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
