/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author yogha
 */
public class CreateNewTransactionParticipant implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private TransactionRepository transactionRepository;

    public CreateNewTransactionParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        if (trx != null) {
            return PREPARED | NO_JOIN;
        } else {
            String paramType = cfg.get("trx_type", "inquiry");
            String paramDescription = cfg.get("trx_description", "-");
            MTransaction newTrx = Service.setDefaultTransaction(session);
            newTrx.setStatus(Constants.TRX_STATUS.FAILED);
            newTrx.setDescription(paramDescription);
            switch (paramType) {
                case "inquiry":
                    newTrx.setType(Constants.TRX_TYPE.INQUIRY);
                    break;
                case "posting":
                    newTrx.setType(Constants.TRX_TYPE.POSTING);
                    break;
            }
            newTrx = transactionRepository.save(newTrx);
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, newTrx);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String paramType = cfg.get("trx_type", "inquiry");
        if (product == null) {
            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_INACTIVE.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        MTransaction trx = new MTransaction();
        trx.setNasabah(session.getNasabah());
        trx.setAgent(session.getAgent());
        trx.setOriginReff(session.getOriginReff());
        trx.setStan(session.getStan());
        trx.setStatus(Constants.TRX_STATUS.PENDING);
        trx.setOrigin(session.getOrigin());
        trx.setSmsInfo("");
        trx.setDescription(product.getProductName());
        
        MNotifikasiTrx mnt = new MNotifikasiTrx();
        
        switch (paramType) {
            case "inquiry":
                trx.setType(Constants.TRX_TYPE.INQUIRY);
                trx.setStatus(Constants.TRX_STATUS.PENDING);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            case "posting":
                trx.setType(Constants.TRX_TYPE.POSTING);
                trx.setStatus(Constants.TRX_STATUS.PENDING);
                MOtp otp = Service.findOTP(session.getOtp());
                Service.setOTPStatus(otp, Constants.OtpStatus.USED);
                trx.setOtp(session.getOtp());
                trx = transactionRepository.save(trx);
                
                
                
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            default:
                return ABORTED;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
