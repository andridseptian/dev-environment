/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class InquirySaldoTrxParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private RcRepository rcRepository;
    private Log log = Log.getLog("Q2", getClass().getName());

    public InquirySaldoTrxParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String target = cfg.get("case", "");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        String accountNumber = "";
        log.info("target:" + target);
        switch (target) {
            case "tariktunai":
            case "nasabah":
                accountNumber = session.getNasabah().getAccountNumber();
                break;
            case "setortunai":
            case "transfer":
            case "agent":
                accountNumber = agent.getMsisdn();
                break;
            default:
                session.setDetail(session.getDetail()+"[skip check saldo trx]");
                ctx.put(Constants.AGENT, agent);
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
        }
        JSONObject hasil = Service.serviceBalanceInquiry(agent.getId(), accountNumber);
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);

            }
            session.setRcToClient(rc);
            session.setResponseToClient(rc + " | " + rm);
            log.info(session.getResponseToClient());
            MRc mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_CHECK_BALANCE, rc);
            if (mapRc.getStatus() == Constants.TRX_STATUS.SUCCESS) {
                session.setResponseToClient(mapRc.getRm());
                double balance = hasil.getDouble("balance");
                if (balance > session.getAmount()) {
                    ctx.put(Constants.AGENT, agent);
                    ctx.put(Constants.SESSION, session);
                    return PREPARED | NO_JOIN;
                } else {
                    session.setRcToClient(Constants.RC_INSUFFICIENT_BALANCE);
                    session.setResponseToClient("Transaksi tidak dapat dilakukan karena Saldo kurang dari total transaksi");
                    ctx.put(Constants.AGENT, agent);
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
            } else {
                session.setRcToClient(mapRc.getRc());
                session.setResponseToClient(mapRc.getRm());
                ctx.put(Constants.AGENT, agent);
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        } else {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.AGENT, agent);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
