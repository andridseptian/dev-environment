/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class FormatInquirySetorTunai implements TransactionParticipant, org.jpos.core.Configurable {
    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }
    
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        String response = Service.getResponseFromSettings(KeyMap.RM_INQUIRY_SETOR_TUNAI.name());
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        try {
            double amount = Double.valueOf(request[1].trim());
            response = response
                    .replace("[Amount]", "Rp." + Utility.formatAmount(amount))
                    .replace("[NasabahMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[NasabahName]",session.getNasabah().getName())
                    .replace("[msisdn]",Utility.viewMsisdn(session.getNasabah().getMsisdn()));
            response = response + Constants.RM_MESSAGE_INPUT_PIN;
            trx.setInfo(response);
            trx.setAmount(amount);
            session.setAmount(amount);
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            ctx.log(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
        }
        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }
    
    @Override
    public void abort(long l, Serializable srlzbl) {
        
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
