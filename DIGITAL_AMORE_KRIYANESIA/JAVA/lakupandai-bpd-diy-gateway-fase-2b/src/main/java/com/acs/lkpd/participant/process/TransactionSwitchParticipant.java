package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;
import org.jpos.util.Log;

public class TransactionSwitchParticipant implements GroupSelector, Configurable {
    
    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public String select(long arg0, Serializable o) {
        Context ctx = (Context) o;
        String groups = (String) ctx.get(Constants.GROUP);
        return cfg.get(groups, "DefaultRoute");
    }
    
    @Override
    public void setConfiguration(Configuration cfg)
            throws ConfigurationException {
        this.cfg = cfg;
    }
    
    @Override
    public void commit(long id, Serializable context) {
    }
    
    @Override
    public void abort(long id, Serializable context) {
    }
    
    @Override
    public int prepare(long id, Serializable context) {
        try {
            Context ctx = (Context) context;
            ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
            String group;
            
            group = isomsg.getMTI() + isomsg.getString(3);
            
            if (group == null) {
                ctx.put(Constants.RC, Constants.RC_MISSING_MANDATORY_PARAMETER);
                log.error("Group Not Found");
                return ABORTED | NO_JOIN;
            } else {
                ctx.put(Constants.GROUP, group);
                return PREPARED | NO_JOIN;
            }
        } catch (ISOException ex) {
            log.error(ex.getMessage());
            return ABORTED | NO_JOIN;
        }
    }
}
