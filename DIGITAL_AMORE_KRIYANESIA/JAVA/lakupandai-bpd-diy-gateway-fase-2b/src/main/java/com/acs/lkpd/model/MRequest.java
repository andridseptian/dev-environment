/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_request")
public class MRequest implements Serializable {

    private long id;
    private String remoteAddr;
    private String request;
    private String response;
    private LocalDateTime requestTime;
    private LocalDateTime responseTime;
    private String rm;
    private String otp;
    private String info;
    private Constants.TRX_STATUS status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "request_seq")
    @SequenceGenerator(name = "request_seq", sequenceName = "request_seq", allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    @Column(name = "remote_addr")
    public String getRemoteAddr() {
        return remoteAddr;
    }

    @Column(name = "request")
    @Type(type = "text")
    public String getRequest() {
        return request;
    }

    @Column(name = "response")
    @Type(type = "text")
    public String getResponse() {
        return response;
    }

    @Column(name = "request_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    @Column(name = "response_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getResponseTime() {
        return responseTime;
    }

    @Column(name = "rm")
    public String getRm() {
        return rm;
    }

    @Column(name = "otp")
    public String getOtp() {
        return otp;
    }

    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.TRX_STATUS getStatus() {
        return status;
    }

    //===================SET===============//
    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public void setResponseTime(LocalDateTime responseTime) {
        this.responseTime = responseTime;
    }

    public void setStatus(Constants.TRX_STATUS status) {
        this.status = status;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

}
