package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentCheckParticipant implements TransactionParticipant, Configurable {

    public Configuration cfg;
    private AgentRepository agentRepository;

    public AgentCheckParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        String checkBy = cfg.get("checkBy", "username");
        String mappingName = cfg.get("mapping_user", Constants.AGENT);
        JSONObject obj = new JSONObject(req.getRequest());
        String iUserId, iPin, iId, iMsisdn;
        MAgent agent;
        switch (checkBy.toLowerCase()) {
            case "id":
                iUserId = obj.getString(Constants.PARAM.USER_ID);
                agent = agentRepository.findOne(iUserId);
                if (agent == null) {
                    req.setRm("Data Pengguna tidak ditemukan atau tidak terdaftar");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(mappingName, agent);
                return PREPARED | NO_JOIN;
            case "id,pin":
            case "pin,id":
                iUserId = obj.getString(Constants.PARAM.USER_ID);
                iPin = obj.getString(Constants.PARAM.PIN);
                agent = agentRepository.findByIdAndPin(iUserId, iPin);
                if (agent == null) {
                    req.setRm("ID Pengguna atau Kata Sandi yang anda masukkan Salah");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(mappingName, agent);
                return PREPARED | NO_JOIN;
            case "msisdn":
                iMsisdn = obj.getString(Constants.PARAM.MSISDN);
                iMsisdn = Utility.formatMsisdn(iMsisdn);
                agent = agentRepository.findFirstByMsisdnOrderByCrTimeDesc(iMsisdn);
                if (agent == null) {
                    req.setRm("User tidak ditemukan atau tidak terdaftar");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(mappingName, agent);
                return PREPARED | NO_JOIN;
            case "reset":
                iMsisdn = obj.getString(Constants.PARAM.RESET_MSISDN);
                iMsisdn = Utility.formatMsisdn(iMsisdn);
                agent = agentRepository.findFirstByResetMsisdnOrderByCrTimeDesc(iMsisdn);
                if (agent == null) {
                    req.setRm("User tidak ditemukan atau tidak terdaftar");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(mappingName, agent);
                ctx.put(Constants.AGENT, agent);
                return PREPARED | NO_JOIN;
            default:
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Kesalahan internal, Data pengguna tidak ditemukan");
                return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
