package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "m_agent")
public class MAgent implements Serializable {

    private String id;
    private String pin;
    private String accountNumber;
    private String name;
    private String address;
    private String msisdn;
    private String npwp;
    private Constants.AgentAccountType accountType;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private Constants.Status status;
    private String info;
    private String imei;
    private boolean statusBank;
    private boolean statusPin;
    private MCabang cabang;
    private int failLogin;
    private boolean statusResetPin;
    private int statusApproval;
    private String token;
    private String resetMsisdn;
    private String agentType;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }

    @Id
    @Column(name = "id", length = 10)
    public String getId() {
        return id;
    }

    @Column(name = "pin", nullable = false)
    public String getPin() {
        return pin;
    }

    @Column(name = "account_number", length = 12)
    public String getAccountNumber() {
        return accountNumber;
    }

    @Column(name = "name", length = 50)
    public String getName() {
        return name;
    }

    @Column(name = "address", length = 100)
    public String getAddress() {
        return address;
    }

    @Column(name = "msisdn", length = 15)
    public String getMsisdn() {
        return msisdn;
    }

    @Column(name = "npwp", length = 20)
    public String getNpwp() {
        return npwp;
    }

    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    public Constants.AgentAccountType getAccountType() {
        return accountType;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.Status getStatus() {
        return status;
    }

    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    @Column(name = "imei")
    public String getImei() {
        return imei;
    }

    @Column(name = "status_bank", columnDefinition = "boolean default false")
    public boolean isStatusBank() {
        return statusBank;
    }

    @Column(name = "status_pin", columnDefinition = "boolean default false")
    public boolean isStatusPin() {
        return statusPin;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MCabang.class)
    public MCabang getCabang() {
        return cabang;
    }

    @Column(name = "fail_login", columnDefinition = "int default 0")
    public int getFailLogin() {
        return failLogin;
    }

    @Column(name = "status_reset_pin", columnDefinition = "boolean default false")
    public boolean isStatusResetPin() {
        return statusResetPin;
    }

    @Column(name = "status_approval", columnDefinition = "int default 0")
    public int getStatusApproval() {
        return statusApproval;
    }

    @Column(name = "token")
    public String getToken() {
        return token;
    }

    @Column(name = "reset_msisdn", length = 15)
    public String getResetMsisdn() {
        return resetMsisdn;
    }
    
    @Column(name = "agent_type", length = 1)
    public String getAgentType() {
        return agentType;
    }

    //================SET================//
    public void setId(String id) {
        this.id = id;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setStatus(Constants.Status status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setStatusBank(boolean statusBank) {
        this.statusBank = statusBank;
    }

    public void setStatusPin(boolean statusPin) {
        this.statusPin = statusPin;
    }

    public void setCabang(MCabang cabang) {
        this.cabang = cabang;
    }

    public void setFailLogin(int failLogin) {
        this.failLogin = failLogin;
    }

    public void setStatusResetPin(boolean statusResetPin) {
        this.statusResetPin = statusResetPin;
    }

    public void setStatusApproval(int statusApproval) {
        this.statusApproval = statusApproval;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public void setAccountType(Constants.AgentAccountType accountType) {
        this.accountType = accountType;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setResetMsisdn(String resetMsisdn) {
        this.resetMsisdn = resetMsisdn;
    }

    public void setAgentType(String agenType) {
        this.agentType = agenType;
    }

}
