package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.util.Service;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class CheckOTPRegistrationParticipant implements TransactionParticipant {

    private final SessionRepository sessionRepository;
    private final NasabahRepository nasabahRepository;

    public CheckOTPRegistrationParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        sessionRepository = context.getBean(SessionRepository.class);
        nasabahRepository = context.getBean(NasabahRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        try {
            MOtp otp = Service.findOTP(request[0]);
            MSession tempSession = sessionRepository.findFirstByOtpAndAgentOrderByCrTimeDesc(otp.getOtp(), agent);
            Boolean statusOTP = LocalDateTime.now().isBefore(otp.getEndTime());
            ctx.log("OTP - status : " + statusOTP);
            if (otp.getStatus().equals(Constants.OtpStatus.USED)) {
                session.setRcToClient(Constants.RC_TRANSACTION_IS_FAILED);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.AGENT, agent);
                return ABORTED | NO_JOIN;
            } else if (otp.getStatus().equals(Constants.OtpStatus.INACTIVE)) {
                session.setRcToClient(Constants.RC_TRANSACTION_IS_FAILED);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.AGENT, agent);
                return ABORTED | NO_JOIN;
            }
            if (tempSession != null) {
                if (statusOTP) {
                    int stat = Service.setOTPStatus(otp, Constants.OtpStatus.USED);
                    ctx.log("OTP - change to inactive [" + stat + "]");
                    ctx.log(tempSession.getNasabah().getName()+" "+tempSession.getNasabah().getId());
                    MNasabah nasabah = tempSession.getNasabah();
                    session.setNasabah(nasabah);
                    ctx.log("nasabah "+nasabah.getNik());
                    ctx.put(Constants.NASABAH,nasabah);
                    ctx.put(Constants.SESSION, session);
                    ctx.put(Constants.AGENT, agent);
                    return PREPARED;
                } else {
                    int stat = Service.setOTPStatus(otp, Constants.OtpStatus.INACTIVE);
                    ctx.log("OTP -  " + stat);
                    session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                    ctx.put(Constants.SESSION, session);
                    ctx.put(Constants.AGENT, agent);
                    return ABORTED | NO_JOIN;
                }
            } else {
                session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.AGENT, agent);
                return ABORTED | NO_JOIN;
            }
        } catch (NullPointerException ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.AGENT, agent);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MNasabah nasabah = session.getNasabah();
        nasabah.setStatus(Constants.Status.UNDEF);
        MNasabah savedNasabah = nasabahRepository.save(nasabah);
        session.setResponseToClient("OTP berhasil, status nasabah " + savedNasabah.getName() + " Menunggu Approval [" + savedNasabah.getModTime().format(Constants.FORMAT_DDMMYYYYHHMMSS) + "]");
        ctx.put(Constants.SESSION, session);
    }

}
