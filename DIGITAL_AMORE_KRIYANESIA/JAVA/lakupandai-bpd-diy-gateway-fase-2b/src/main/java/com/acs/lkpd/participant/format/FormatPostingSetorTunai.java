/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class FormatPostingSetorTunai implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String response = Service.getResponseFromSettings(KeyMap.RM_POSTING_SETOR_TUNAI.name());
        ctx.log("FormMsisdn::"+session.getNasabah().getMsisdn());
        try {
            response = response
                    .replace("[Amount]", Utility.formatAmount(session.getAmount()))
                    .replace("[NasabahMsisdn]", "Rp." + Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[NasabahName]", session.getNasabah().getName())
                    .replace("[msisdn]",Utility.viewMsisdn(session.getNasabah().getMsisdn()));
            response = response + Service.getResponseFromSettings(KeyMap.RM_MESSAGE_NOTIF.name());
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (Exception ex) {
            ctx.log(ex);
            session.setDetail(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

}
