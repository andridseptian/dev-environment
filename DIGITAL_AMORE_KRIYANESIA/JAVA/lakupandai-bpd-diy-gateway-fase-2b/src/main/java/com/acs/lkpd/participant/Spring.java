/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import java.util.Arrays;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author ACS
 */
public class Spring extends QBeanSupport {

    Configuration conf;

    @Override
    protected void initService() throws Exception {
        LogEvent le = new LogEvent(getName());
        String contextLocation = conf.get("contextLocation", "cfg/spring.xml");
        ApplicationContext context = new FileSystemXmlApplicationContext(contextLocation);
        NameRegistrar.register(Constants.OBJ.SPRING, context);
        le.addMessage("Context " + getName() + " Success Loaded");
        le.addMessage("List Beans :");
        Arrays.asList(context.getBeanDefinitionNames()).stream().forEach(s -> le.addMessage(s));
        log.info(le);
    }

    @Override
    protected void destroyService() throws Exception {
        log.info("Spring " + getName() + " is stopped");
        NameRegistrar.unregister(Constants.OBJ.SPRING);
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.conf = cfg;
    }
}
