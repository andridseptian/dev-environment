package com.acs.lkpd.participant.agent;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MNotificationHistory;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.NotificationHistoryRepository;
import com.acs.lkpd.repository.NotificationRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.ObjectParser;
import com.acs.util.Service;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author ACS
 */
public class AgentNotifParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final NotificationHistoryRepository notificationHistoryRepository;

    public AgentNotifParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationHistoryRepository = context.getBean(NotificationHistoryRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);

        LocalDateTime startDate = LocalDateTime.now();
        int maxDaysNotification = 30;
        int requestPage = 0;
        if (dataRequest.has(Constants.PARAM.PAGE)) {
            requestPage = dataRequest.getInt(Constants.PARAM.PAGE);
        }
        try {
            maxDaysNotification = Integer.valueOf(Service.getResponseFromSettings(KeyMap.MAX_DAYS_NOTIFICATION.name()));
            startDate = LocalDate.now().minusDays(maxDaysNotification).atStartOfDay();
        } catch (DateTimeParseException | NullPointerException ex) {
            log.error("notif  ex: cannot parse date [" + startDate + "|" + maxDaysNotification + "]");
            log.error("detail ex:" + ex.getMessage());
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Format parse data tidak valid");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        int sizePerPage = 10; //bisa diubah ke parameterize
        PageRequest pageRequest = new PageRequest(requestPage, sizePerPage);
        Page<MNotificationHistory> listHistory = notificationHistoryRepository.listNotificationAgentWithPaging(startDate, agent.getId(), pageRequest);
        if (listHistory == null || listHistory.getContent().isEmpty()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Halaman kosong / data tidak ditemukan");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        JSONObject response = new JSONObject();
        response.put("notification", new JSONArray(ObjectParser.listNotificationHistoryView(listHistory.getContent())));
        response.put("sizePerPage", sizePerPage);
        response.put("totalPages", listHistory.getTotalPages());
        response.put("totalNotification", listHistory.getTotalElements());
        response.put("page", requestPage);
        req.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.OBJ.RESP, response);
        ctx.put(Constants.OBJ.WSREQUEST, req);
        return PREPARED | NO_JOIN;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
