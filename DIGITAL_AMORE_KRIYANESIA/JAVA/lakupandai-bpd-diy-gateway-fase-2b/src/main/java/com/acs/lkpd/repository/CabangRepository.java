
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MCabang;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface CabangRepository extends JpaRepository<MCabang, String>{
    
}
