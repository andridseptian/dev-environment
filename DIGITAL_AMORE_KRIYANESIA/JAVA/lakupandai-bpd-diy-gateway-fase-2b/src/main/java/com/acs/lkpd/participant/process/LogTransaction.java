package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.repository.TransactionRepository;
import java.io.Serializable;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Erwin
 */
public class LogTransaction implements AbortParticipant {

    Log log = Log.getLog("Q2", getClass().getName());
    private TransactionRepository transactionRepository;

    public LogTransaction() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    /**
     *
     * @param l
     * @param srlzbl
     * @context TXLOG
     *
     * @return
     */
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        ctx.log("--Log Transaction--");
        ctx.log("Account Type: " + trx.getAccountType());
        ctx.log("Agent : " + trx.getAgentName());
        ctx.log("Transaction Type: " + trx.getType().name());
        ctx.log("Mobile Response: \n " + trx.getMobileResponse());
        transactionRepository.save(trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        transactionRepository.save(trx);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

}
