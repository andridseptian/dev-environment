/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatCheckNIK implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        session.setRcToClient(Constants.RC_APPROVED);
        String[] config = cfg.get("data").split(",");
        String response = cfg.get("resp", Constants.OBJ.RESP);
        String data = ctx.getString(response);
        JSONObject nikResp = new JSONObject(data);
        String createResp = "";
        try {
            for (String dataConfig : config) {
                if(nikResp.has(dataConfig)){
                    createResp += nikResp.getString(dataConfig)+"|";
                }
            }
            if(createResp.endsWith("|")){
                createResp = createResp.substring(0, createResp.length()-1);
            }
        } catch (Exception ex) {
            ctx.log("error not found in resp : " + ex);
        }
        ctx.log("Add Resp : " + createResp);
        session.setResponseToClient(createResp);
        ctx.put(Constants.SESSION, session);
        return PREPARED | NO_JOIN;
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }
}
