/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class GenerateAndSendOTPPaymentParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());

    private final OTPRepository oTPRepository;

    public GenerateAndSendOTPPaymentParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        oTPRepository = context.getBean(OTPRepository.class);

    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);

        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        Boolean agen = cfg.getBoolean("agen", false);
        MOtp otp = new MOtp();
//        otp.setAgent(session.getAgent().getId());
//        otp = oTPRepository.save(otp);

        if (agen) {
            otp = Service.generateOTPPaymentWithProduct(session.getAgent().getMsisdn(), session);
        } else {
            otp = Service.generateOTPPaymentWithProduct(session.getNasabah().getMsisdn(), session);
        }

        if (otp == null) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_GENERATE_OTP_ERROR));
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            return ABORTED | NO_JOIN;
        } else {
            String isiPesanOTP = trx.getSmsResponse();
            if (isiPesanOTP.contains("[otp]") == true) {
                isiPesanOTP = isiPesanOTP.replace("[otp]", otp.getOtp());
            } else {
                isiPesanOTP += otp.getOtp();
            }
            isomsg.set(61, otp.getOtp());
            session.setOtp(otp.getOtp());
            trx.setOtp(otp.getOtp());

            if (sendSMS) {
                boolean statusSMS = Utility.sendSMS(isiPesanOTP, session.getNasabah().getMsisdn());
                if (statusSMS) {
                    ctx.log("send message success : " + session.getNasabah().getMsisdn() + ", otp:" + otp.getOtp());
                    String info = "SMS success, OTP " + otp.getOtp();
                    ctx.log(info);
                    session.setDetail(info);
                    trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSMS);
                    return PREPARED | NO_JOIN;
                } else {
                    ctx.log("Status pesan tidak dapat terkirim, ulangi beberapa saat lagi");
                    return ABORTED | NO_JOIN;
                }
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.IN, isomsg);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        log.info("Commit OTP");
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
