/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.FormatTransaksiTerakhir;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author yogha
 */
public class InquiryTransaksiTerakhirParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());
    private final TransactionRepository transactionRepository;
    private final NotificationTrxRepository notificationTrxRepository;
    private final SessionRepository sessionRepository;
    private ProductRepository productRepository;

    public InquiryTransaksiTerakhirParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        transactionRepository = context.getBean(TransactionRepository.class);
        sessionRepository = context.getBean(SessionRepository.class);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);

        ctx.log("--Inquiry Transaksi Terakhir Participant--");
//        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        try {

            MSession tempSession = null;
            MTransaction tempTransaction = transactionRepository.findFirstByTypeAndDescriptionNotAndAgentAndNasabahOrderByCrTimeDesc(Constants.TRX_TYPE.POSTING, trx.getDescription(), agent, nasabah);
            if (tempTransaction != null) {
                ctx.log("Last Transaction Found");
                tempSession = sessionRepository.findFirstByAgentAndNasabahAndOtpOrderByCrTimeDesc(tempTransaction.getAgent(), tempTransaction.getNasabah(), tempTransaction.getOtp());
            }
            if (tempTransaction != null && tempSession != null) {
//            if (tempTransaction != null) {
                Long tempSessionId = session.getId();
                String tempOrigin = session.getOrigin();
                String tempOriginReff = session.getOriginReff();
                String tempStan = session.getStan();

                session = tempSession;
                trx = tempTransaction;
                session.setId(tempSessionId);
                session.setOrigin(tempOrigin);
                session.setOriginReff(tempOriginReff);
                session.setStan(tempStan);

                trx.setOrigin(tempOrigin);
                trx.setOriginReff(tempOriginReff);
                session.setStan(tempStan);

                ctx.log("Transaction Description : " + trx.getDescription());

                MNotifikasiTrx notif;
                ctx.log("searching origin reff: " + trx.getOriginReff());
                notif = notificationTrxRepository.findFirstByOriginReff(trx.getOriginReff());
                if (notif == null) {
                    ctx.log("searching bank reff " + trx.getBankReff());
                    notif = notificationTrxRepository.findFirstByBankReff(trx.getBankReff());
                }
                String formatCekTransaksiTerkahir = null;
                try {
                    formatCekTransaksiTerkahir = new FormatTransaksiTerakhir().getFormatNotif(notif, notif.getStatus().toString(), notif.getDescription()).toString();
                } catch (Exception e) {
                    log.error(e);
                    formatCekTransaksiTerkahir = new FormatTransaksiTerakhir().getFormat(trx, trx.getStatus().toString());
                }

                ctx.log("formatCekTransaksiTerkahir : " + formatCekTransaksiTerkahir);
                session.setRcToClient(Constants.RC_APPROVED);
                session.setResponseToClient(formatCekTransaksiTerkahir);

//                ===================SMS====================
                String smsformat = "Cek Status Terakhir \n";
                JSONObject jsonsms = new JSONObject(formatCekTransaksiTerkahir);

                ctx.log("Json SMS : " + jsonsms);

//                JSONArray jsonsmsarr = new JSONArray(jsonsms.getString("data
                JSONArray jsonsmsarr = new JSONArray();
                try {
                    ctx.log("get one data");
                    JSONArray getsatudata = new JSONArray(jsonsms.getString("data"));
                    jsonsmsarr = getsatudata;
                    jsonsms.put("data", jsonsmsarr);
                    session.setResponseToClient(jsonsms.toString());
                } catch (Exception e) {
                    try {
                        ctx.log("one data not found => get data from json sms to string");
                        log.error("one data not found " + e.getMessage() + " find alternate ");
                        String jsonarr = jsonsms.get("data").toString();
//                    jsonarr = jsonarr.replace(tempStan, smsformat);
//                    jsonsmsarr = new JSONArray(jsonsms.get("data"));
                        jsonsmsarr = new JSONArray(jsonarr);
                    } catch (Exception exe) {
                        log.error("json sms string not found " + exe.getMessage() + " find alternate ");
                        try {
                            ctx.log("json sms to string not found => get data from json sms to json");
                            JSONObject jobj = new JSONObject(jsonsms.getString("data"));
                            session.setResponseToClient(jobj.toString());
                            ctx.log("set response to client: " + jobj.toString());
                        } catch (Exception ex) {
                            ctx.log("alternate not found");
                            log.info("alternate not found");
                            log.error(ex);
                        }
                    }
                }

                log.info("JSON SMSARRAY : " + jsonsmsarr);

                for (int i = 0; i < jsonsmsarr.length(); i++) {
                    JSONObject arrval = jsonsmsarr.getJSONObject(i);
                    log.info("ARRVALUE " + i + " : " + arrval);
                    smsformat = smsformat + arrval.getString("header") + " : " + arrval.getString("value") + "\n";
                }

                log.info("SMS FORMAT " + smsformat);

                boolean statusSms = Utility.sendSMS(smsformat, nasabah.getMsisdn());

                log.info("STATUS SMS : " + statusSms);

//                ============================================
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return PREPARED;
            } else {
                log.info("Inquiry Transaksi Terakhir Null Data");
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
                return ABORTED;
            }
        } catch (Exception e) {
            ctx.log(e);
            log.error(e);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
            return ABORTED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
