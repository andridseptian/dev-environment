package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.repository.NotificationRepository;
import com.acs.util.Service;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Akses Nusantara
 */
public class NotificationHandler extends QBeanSupport implements Configurable {

    private Timer timer;
    private TimerTask task;
    private Long delaySchedule;
    private String broadcastHandlerName;
    private boolean run = false;
    private NotificationRepository notificationRepository;

    public NotificationHandler() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationRepository = context.getBean(NotificationRepository.class);
    }

    @Override
    protected void startService() throws Exception {
        try {
            timer = new Timer();
            task = new TimerTask() {

                @Override
                public void run() {
                    String sender = cfg.get("sender");
                    try {
                        processBroadcastSchedule(sender);
                    } catch (Exception ex) {
                        log.error(ex);
                        log.error(ex.getMessage());
                    }
                }
            };
            broadcastHandlerName = cfg.get("name");
            delaySchedule = cfg.getLong("delay_schedule", 10000);
            Date startDate = new Date();
            timer.schedule(task, startDate, delaySchedule);
            log.info("Starting Notification Schedule [" + broadcastHandlerName + "] .. delay schedule : " + delaySchedule + "ms");
        } catch (Exception ex) {
            log.info("Error Notification Schedule [" + broadcastHandlerName + "] ..");
            log.info(ex.getMessage());
        }
    }

    @Override
    protected void stopService() throws Exception {
        run = false;
        if (timer != null) {
            timer.cancel();
        }
        if (task != null) {
            task.cancel();
        }
        log.info("Notification Schedule [" + broadcastHandlerName + "] Stop..");
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
        this.log = Log.getLog("Q2", getClass().getName());
    }

    private void processBroadcastSchedule(String senderId) throws Exception {
        int maxProcessTrx = cfg.getInt("max_process_trx", 10);
        if (!run) {
            run = true;
            List<MNotification> list = notificationRepository.selectProcesNotification(maxProcessTrx);
            if (list == null || list.isEmpty()) {
                LogEvent le = new LogEvent("NotificationController [" + senderId + "]");
                le.addMessage("No Data Found on sender : " + senderId);
                log.info(le);
                run = false;
            } else {
                LogEvent le = new LogEvent("NotificationController [ " + senderId + "]");
                list.forEach((dataNotification) -> {
                    MNotification hasil = Service.sendNotif(dataNotification);
                    notificationRepository.save(hasil);
                });
                log.info(le);
            }
            run = false;
        } else {
            log.info("Notification Broadcast still running..");
        }
    }
}
