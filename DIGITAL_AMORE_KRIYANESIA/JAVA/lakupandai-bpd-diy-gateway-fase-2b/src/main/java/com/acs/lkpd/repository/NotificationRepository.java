package com.acs.lkpd.repository;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MTransaction;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ACS
 */
public interface NotificationRepository extends JpaRepository<MNotification, Long> {

    @Query(value = "SELECT "
            + "COUNT(*) "
            + "FROM m_notification "
            + "WHERE rrn = ?1 "
            + "AND cr_time >= current_date", nativeQuery = true)
    int countByReqIdToday(String rrn);
    
    @Query(value = "SELECT * from load_notification(:input)", nativeQuery = true)
    List<MNotification> selectProcesNotification(@Param("input") int maxProcess);
}
