/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_rc")
public class MRc implements Serializable {

    private Long id;
    private String processCode;
    private String rc;
    private String rm;
    private String info;
    private String description;
    private Constants.TRX_STATUS status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "rc_seq")
    @SequenceGenerator(name = "rc_seq", sequenceName = "rc_seq",allocationSize = 1,initialValue = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "process_code", nullable = false)
    public String getProcessCode() {
        return processCode;
    }

    @Column(name = "rc", nullable = false)
    public String getRc() {
        return rc;
    }

    @Column(name = "rm")
    public String getRm() {
        return rm;
    }

    @Column(name = "description")
    @Type(type = "text")
    public String getDescription() {
        return description;
    }

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    public Constants.TRX_STATUS getStatus() {
        return status;
    }

    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(Constants.TRX_STATUS status) {
        this.status = status;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
    
}
