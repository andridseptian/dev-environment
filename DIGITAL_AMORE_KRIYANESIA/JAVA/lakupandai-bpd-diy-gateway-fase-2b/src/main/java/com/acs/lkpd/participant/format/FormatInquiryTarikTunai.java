/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MProduct;
import com.acs.util.Service;
import org.jpos.util.Log;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatInquiryTarikTunai implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        log.info("Prepare Inq Tarik Tunai");
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        String amountFormat = cfg.get("amountFormat", "[Amount]");
        String msisdnFormat = cfg.get("msisdnFormat", "[MSISDN]");
        String nameFormat = cfg.get("nameFormat", "[Name]");
        String processFormat = cfg.get("processFormat", "[Process]");
        String response = Service.getResponseFromSettings(Constants.RM_INQUIRY_TARIK_TUNAI);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        try {
            double amount = Double.valueOf(request[1].trim());
//            response = response.replace(amountFormat, Utility.formatAmount(amount))
//                    .replace(msisdnFormat, Utility.viewMsisdn(nasabah.getMsisdn()))
//                    .replace(nameFormat, nasabah.getName());
//            response = response + Service.getResponseFromSettings(Constants.RM_MESSAGE_TOKEN).replace(processFormat, trx.getDescription());

            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[Amount]", "Rp." + Utility.formatAmount(amount))
                            .replace("[NasabahName]", nasabah.getName())
                            .replace("[Desc]", Service.getResponseFromSettings(Constants.RM_MESSAGE_TOKEN).replace(processFormat, trx.getDescription()))
                            .replace("[Msisdn]", Utility.viewMsisdn(nasabah.getMsisdn())));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";

            }

            trx.setInfo(response);
            trx.setAmount(amount);
            session.setAmount(amount);
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return PREPARED;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
            return ABORTED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
