/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.util.Service;
import java.io.IOException;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class PrepareISOResponseParticipant implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        if (source == null || !source.isConnected()) {
            session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
            session.setResponseToClient("Partner Not Found");
            return ABORTED | NO_JOIN;
        }
        ISOMsg in = (ISOMsg) ctx.get(Constants.IN);
        try {
            log.info("Prepare Response To Client");
            String response = cfg.get("message", "Transaksi anda sedang diproses, silahkan cek status di menu Log Transaksi");
            in.setResponseMTI();
            ISOMsg out = (ISOMsg) in.clone();
            String rc = session.getRcToClient();
            out.set(39, "13");
            out.set(48, response);
            source.send(out);
            return PREPARED | NO_JOIN;
        } catch (IOException ex) {
            log.error(ex);
            return ABORTED | NO_JOIN;
        } catch (ISOFilter.VetoException ex) {
            log.error(ex);
            return ABORTED | NO_JOIN;
        } catch (ISOException isoe) {
            log.error(isoe);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {

    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        if (source == null || !source.isConnected()) {
            session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
            session.setResponseToClient("Partner Not Found");
        } else {
            ISOMsg in = (ISOMsg) ctx.get(Constants.IN);
            try {
                log.info("Prepare Response To Client");
                in.setResponseMTI();
                ISOMsg out = (ISOMsg) in.clone();
                out.set(39, session.getRcToClient());
                out.set(48, session.getResponseToClient());
                source.send(out);
            } catch (IOException ex) {
                log.error(ex);
            } catch (ISOFilter.VetoException ex) {
                log.error(ex);
            } catch (ISOException isoe) {
                log.error(isoe);
            }
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
