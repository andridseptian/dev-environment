/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.agent;

import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class AgentInquiryParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final AgentRepository agentRepository;
    private final TransactionRepository transactionRepository;

    public AgentInquiryParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String[] request = (String[]) ctx.get(Constants.REQ);
        String findBy = cfg.get("findby", "id");
        String idReq = cfg.get("field_id", Constants.PARAM.USER_ID);
        int pinReq = cfg.getInt("field_pin", 0);
        int msisdnReq = cfg.getInt("field_msisdn", 0);
        String responseNegatif = cfg.get("response_negatif", "RM_AGENT_NOT_FOUND");
        MSession session = (MSession) ctx.get(Constants.SESSION);

//        MTransaction oldtrx = transactionRepository.findByOriginReff(session.getOriginReff());
//        
////        String Bulan = oldtrx.getInfo();
//        
//        ctx.put(Constants.JUMLAHBULAN, Bulan);
//        
        MAgent agent = null;
        switch (findBy.toLowerCase()) {
            case "id": {
                int field = 0;
                try {
                    field = Integer.parseInt(idReq);
                    agent = agentRepository.findOne(request[field]);
                } catch (NullPointerException | NumberFormatException ex) {
                    ctx.log("find agent by ctx, " + idReq);
                    String hasil = ctx.getString(idReq);
                    ctx.log(ctx.getString(idReq));
                    agent = agentRepository.findOne(hasil);
                }
                break;
            }
            case "id,pin":
            case "pin,id": {
                int field = 0;
                String pin = request[pinReq];
                try {
                    field = Integer.parseInt(idReq);
                    agent = agentRepository.findOne(request[field]);
                    ctx.log("agent : " + agent.getId());
                    ctx.log("fail login : " + agent.getFailLogin());
                    if (!pin.equals(agent.getPin())) {
                        ctx.log("pin salah");
                        agent.setFailLogin(agent.getFailLogin() + 1);
                        ctx.log("agent getfail login" + agent.getFailLogin());
                        int maxFailLogin = 3;
                        try {
                            maxFailLogin = Integer.parseInt(Service.getResponseFromSettings(KeyMap.MAX_FAIL_LOGIN_AGENT.name()));
                        } catch (NullPointerException | NumberFormatException ex) {
                            maxFailLogin = 3;
                        }
                        if (agent.getFailLogin() > maxFailLogin) {
                            agent.setFailLogin(0);
                            agent.setStatus(Constants.Status.BLOCK);
                        }
                        agent = agentRepository.save(agent);
                    } else {
                        ctx.log("pin benar");
                        agent.setFailLogin(0);
                        agent = agentRepository.save(agent);
                    }
                } catch (NullPointerException | NumberFormatException ex) {
                    ctx.log("find agent&pin by ctx, " + idReq);
                    String hasil = ctx.getString(idReq);
                    agent = agentRepository.findOne(hasil);
                    if (agent == null) {
                        session.setRcToClient(Constants.RC_FORMAT_ERROR);
                        session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_NOT_FOUND.name()));
                        ctx.put(Constants.SESSION, session);
                        return ABORTED | NO_JOIN;
                    }
                    if (!pin.equals(agent.getPin())) {
                        ctx.log("pin salah");
                        agent.setFailLogin(agent.getFailLogin() + 1);
                        int maxFailLogin = 3;
                        try {
                            maxFailLogin = Integer.parseInt(Service.getResponseFromSettings(KeyMap.MAX_FAIL_LOGIN_AGENT.name()));
                        } catch (NullPointerException | NumberFormatException de) {
                            maxFailLogin = 3;
                        }
                        agent = agentRepository.save(agent);
                        if (agent.getFailLogin() > maxFailLogin) {
                            agent.setFailLogin(0);
                            agent.setStatus(Constants.Status.BLOCK);
                            agent = agentRepository.saveAndFlush(agent);
                            break;
                        }
                        session.setRcToClient(Constants.RC_FORMAT_ERROR);
                        session.setResponseToClient(Service.getResponseFromSettings(responseNegatif));
                        ctx.put(Constants.SESSION, session);
                        return ABORTED | NO_JOIN;
                    } else {
                        ctx.log("pin benar");
                        agent.setFailLogin(0);
                        agent = agentRepository.save(agent);
                    }
                }
                break;
            }
            case "msisdn": {
                agent = agentRepository.findByMsisdn(request[msisdnReq]);
                break;
            }
        }
        if (agent != null) {
            switch (agent.getStatus()) {
                case ACTIVE: {
                    break;
                }
                case BLOCK: {
                    session.setRcToClient(Constants.RC_ACCOUNT_AGEN_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_BLOCK.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case REJECTED: {
                    session.setRcToClient(Constants.RC_ACCOUNT_AGEN_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_REJECTED.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case INACTIVE: {
                    session.setRcToClient(Constants.RC_ACCOUNT_AGEN_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_INACTIVE.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case NEED_APPROVAL:
                case NEED_APPROVAL_CS:
                    session.setRcToClient(Constants.RC_ACCOUNT_AGEN_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_NEED_APPROVAL.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
            }
            ctx.put(Constants.AGENT, agent);
            session.setAgent(agent);
            ctx.put(Constants.SESSION, session);

            ctx.log("--Agent Inquiry Participant--");
            ctx.log("Agent Name: " + session.getAgent().getName());
            ctx.log("Account Number: " + session.getAgent().getAccountNumber());
            ctx.log("MSISDN: " + session.getAgent().getMsisdn());
            ctx.log("Agent Type: " + session.getAgent().getAgentType());

            return PREPARED | NO_JOIN;
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(responseNegatif));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
