package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_service_proxy")
public class MServiceProxy implements Serializable {

    private String service;
    private LocalDateTime crTime;
    private String description;
    private String param;
    private String txnmgr;
    private boolean active;
    private Constants.TRX_TYPE type;
    private String procode;
    private long timeout;

    @Id
    @Column(name = "service", length = 100)
    public String getService() {
        return service;
    }

    @Column(name = "active", columnDefinition = "boolean default true", nullable = false)
    public boolean isActive() {
        return active;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "description", length = 100)
    public String getDescription() {
        return description;
    }

    @Column(name = "param")
    @Type(type = "text")
    public String getParam() {
        return param;
    }

    @Column(name = "txnmgr", length = 50)
    public String getTxnmgr() {
        return txnmgr;
    }

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public Constants.TRX_TYPE getType() {
        return type;
    }

    @Column(name = "procode", length = 30)
    public String getProcode() {
        return procode;
    }

    @Column(name = "timeout")
    public long getTimeout() {
        return timeout;
    }

    //=============== SET ===============//
    public void setService(String service) {
        this.service = service;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public void setTxnmgr(String txnmgr) {
        this.txnmgr = txnmgr;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setType(Constants.TRX_TYPE type) {
        this.type = type;
    }

    public void setProcode(String procode) {
        this.procode = procode;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

}
