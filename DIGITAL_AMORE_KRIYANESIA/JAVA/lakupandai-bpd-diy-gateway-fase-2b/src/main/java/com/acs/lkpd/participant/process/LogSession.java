package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.repository.SessionRepository;
import java.io.Serializable;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Erwin
 */
public class LogSession implements AbortParticipant {

    Log log = Log.getLog("Q2", getClass().getName());
    private SessionRepository sessionRepository;

    public LogSession() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        sessionRepository = context.getBean(SessionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    /**
     *
     * @param l
     * @param srlzbl
     * @context TXLOG
     *
     * @return
     */
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        sessionRepository.save(session);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        sessionRepository.save(session);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

}
