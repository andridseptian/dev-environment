/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.util;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MLogEmail;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MSessionWeb;
import com.acs.lkpd.model.MSettings;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.EmailRepository;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.lkpd.repository.SessionWebRepository;
import com.acs.lkpd.repository.SettingRepository;
import com.acs.lkpd.repository.TransactionRepository;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HTTP;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.NodeList;

/**
 *
 * @author ACS
 */
@org.springframework.stereotype.Service
public class Service {

    public static Log log = Log.getLog("Q2", "Service");
//    oublicLog logx = Log.getLog("Q2", getClass().getName());

    public static JSONObject sendNotification(String keyServer, String token, String title, String message) {
        JSONObject setting = new JSONObject();
        JSONObject content = new JSONObject();
        JSONObject resp = new JSONObject();
        String rm = "";
        int rc = 0;
        setting.put("to", token);
        content.put("title", title);
        content.put("text", message);
        setting.put("data", content);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost postRequest = new HttpPost("https://fcm.googleapis.com/fcm/send");
        StringEntity body = new StringEntity(setting.toString(), "UTF-8");
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
        postRequest.setHeader("Authorization", "key=" + keyServer);
        postRequest.setEntity(body);
        try {
            HttpResponse response = httpClient.execute(postRequest);
            rm += new Date() + " Send New Order To Phone";
            int RC = response.getStatusLine().getStatusCode();
            rm += new Date() + " Got RC : " + RC;
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            String output;
            while ((output = br.readLine()) != null) {
                rm += new Date() + " Response : " + output;
            }
            if (RC == HttpStatus.SC_OK) {
                rm += new Date() + " Notification Success";
                rc = 1;
            } else {
                rm += new Date() + " Notification Failed";
            }
        } catch (IOException ex) {
            rm += new Date() + " Status : Send Notification Failed (FCM Not Respon)";
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
            resp.put("rm", rm);
            resp.put("rc", rc);
        }
        return resp;
    }

    public static boolean sendEmailRegistrationWebUSer(String name, String targetEmail, String password) {
        MLogEmail logEmail = new MLogEmail();
        String titleEmail = Service.getResponseFromSettings(KeyMap.RM_FORMAT_EMAIL_TITLE_AGENT_RESETPIN.name());
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_EMAIL_AGENT_RESETPIN.name());
        try {
            response = response.replace("[Name]", name)
                    .replace("[Password]", password);
            logEmail.setMessage(response);
            logEmail.setTarget(targetEmail);
            if (Service.sendEmail(logEmail, "lakupandai@bpddiy.co.id", titleEmail)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean sendEmail(String message, String title, String targetEmail) {
        MLogEmail logEmail = new MLogEmail();
        try {
            logEmail.setMessage(message);
            logEmail.setTarget(targetEmail);
            logEmail.setSubject(title);
            return Service.sendEmail(logEmail, "lakupandai@bpddiy.co.id", title);
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean sendEmail(MLogEmail logEmail, String from, String title) {
        EmailRepository emailRepository;
        try {
            ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            emailRepository = context.getBean(EmailRepository.class);
        } catch (Exception ex) {
            return false;
        }
        try {
            String mailSmtpHost = "mail.bpddiy.co.id";//"172.168.100.226";
            String mailUsername = "lakupandai@bpddiy.co.id";//"support@aksesciptasolusi.com";//
            String mailPassword = "e7e65cTs0";//"4k5354c5";
//            String mailPort = "587";
            String mailPort = "25";
            logEmail.setSmtpHost(mailSmtpHost);
            Properties properties = System.getProperties();
            properties.setProperty("mail.smtp.host", mailSmtpHost);
            properties.setProperty("mail.smtp.port", mailPort);
//            properties.setProperty("mail.user", mailUsername);
//            properties.setProperty("mail.password", mailPassword);
            properties.setProperty("mail.smtp.starttls.enable", "false"); // ini penambahan
            properties.setProperty("mail.smtp.auth", "true");
            Authenticator auth = new SMTPAuthenticator();
            Session session = Session.getDefaultInstance(properties, auth);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(logEmail.getTarget()));
            message.setSubject(title);
            message.setText(logEmail.getMessage());
            Transport.send(message);
            String info = "Send email Successful to " + logEmail.getTarget();
            log.info(info);
            logEmail.setInfo(info);
            logEmail.setStatus(1);
            return true;
        } catch (MessagingException ex) {
            ex.printStackTrace();
            log.info(ExceptionUtils.getStackTrace(ex));
            log.info(ex.getMessage());
            logEmail.setInfo(ex.getMessage());
            logEmail.setStatus(0);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info(ExceptionUtils.getStackTrace(ex));
            log.info(ex.getMessage());
            logEmail.setInfo(ex.getMessage());
            logEmail.setStatus(0);
        } finally {
            emailRepository.save(logEmail);
        }
        return false;
    }

    private static class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            String mailUsername = "lakupandai@bpddiy.co.id";//"support@aksesciptasolusi.com";//
            String mailPassword = "e7e65cTs0";//"4k5354c5";
            return new PasswordAuthentication(mailUsername, mailPassword);
        }

    }

    public static boolean isLimitNasabah(double totalAmount, Constants.LIMIT_TYPE limitType) {
        double limit = 0;
        switch (limitType) {
            case NO_LIMIT:
                limit = -1;
                break;
            case MONTHLY:
                limit = Double.parseDouble(Service.findSettings(Constants.LIMIT_NASABAH_PER_BULAN));
                break;
            case YEARLY:
                limit = Double.parseDouble(Service.findSettings(Constants.LIMIT_NASABAH_PER_TAHUN));
                break;
        }
        if (limit == -1) {
            return false;
        }
        return totalAmount > limit;
    }

    public static double findTotalTransaction(MNasabah nasabah, String type) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            TransactionRepository transactionRepository = context.getBean(TransactionRepository.class);
            LocalDateTime startDate = LocalTime.MIDNIGHT.atDate(LocalDate.now()).withDayOfMonth(1);
            LocalDateTime endDate = LocalDateTime.now();
            return 0;
//            return transactionRepository.sumTotalAmountByNasabahAndTypeAndStatusAndDescriptionAndCrTimeBetween(nasabah, Constants.TRX_TYPE.POSTING, Constants.TRX_STATUS.SUCCESS, startDate, endDate);
        } catch (NameRegistrar.NotFoundException | BeansException | NullPointerException ex) {
            return 0.0;
        }
    }

    public static void saveSessionWeb(String userId, String keterangan) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            SessionWebRepository sessionWebRepository = context.getBean(SessionWebRepository.class);
            MSessionWeb newSession = new MSessionWeb();
            newSession.setUserId(userId);
            newSession.setKeterangan(keterangan);
            sessionWebRepository.save(newSession);
        } catch (Exception ex) {
            log.error(ex);
        }
    }

    public static MSession findSession(String otp, MAgent agent) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            SessionRepository sessionRepository = context.getBean(SessionRepository.class);
            return sessionRepository.findFirstByOtpAndAgentOrderByCrTimeDesc(otp, agent);
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MTransaction findTransaction(String rrn, String stan, String otp) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            TransactionRepository transactionRepository = context.getBean(TransactionRepository.class);
            return transactionRepository.findByOriginReffAndStanAndOtp(rrn, stan, otp);
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static int setOTPStatus(MOtp otpBaru, Constants.OtpStatus otpStatus) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            return oTPRepository.updateOtpStatus(otpBaru, otpStatus);
        } catch (NameRegistrar.NotFoundException ex) {
            return 0;
        }
    }

    public static MOtp findOTP(String otp) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            return oTPRepository.findFirstByOtpOrderByCrTimeDesc(otp);
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MOtp findOTPPayment(String agent) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            return oTPRepository.findFirstByAgentOrderByCrTimeDesc(agent);
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MOtp findMarkerAndOTP(String marker, String otp) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            return oTPRepository.findFirstByMarkerAndOtpOrderByCrTimeDesc(marker, otp);
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MOtp generateOTP(String targetNumber) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            MOtp newOtp = new MOtp();
            newOtp.setMarker(targetNumber);
            newOtp.setOtp(Utility.generateStan());
            newOtp.setStatus(Constants.OtpStatus.ACTIVE);
            MOtp responseOTP = oTPRepository.save(newOtp);
            return responseOTP;
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MOtp generateOTPPayment(String targetNumber, String Agent) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            MOtp newOtp = new MOtp();
            newOtp.setMarker(targetNumber);
            newOtp.setOtp(Utility.generateStan());
            newOtp.setStatus(Constants.OtpStatus.ACTIVE);
            newOtp.setAgent(Agent);
            MOtp responseOTP = oTPRepository.save(newOtp);
            return responseOTP;
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MOtp generateOTPPaymentWithProduct(String targetNumber, MSession session) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            OTPRepository oTPRepository = context.getBean(OTPRepository.class);
            MOtp newOtp = new MOtp();
            newOtp.setMarker(targetNumber);
            newOtp.setOtp(Utility.generateStan());
            newOtp.setStatus(Constants.OtpStatus.ACTIVE);
            newOtp.setAgent(session.getAgent().getId());
            newOtp.setProduct(session.getProduct().getProductCode());
            MOtp responseOTP = oTPRepository.save(newOtp);
            return responseOTP;
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static MAgent findAgent(String username) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            AgentRepository agentRepo = context.getBean(AgentRepository.class);
            MAgent agent = agentRepo.findById(username);

            return agent;

        } catch (NameRegistrar.NotFoundException | BeansException ex) {
            return null;
        }

    }

    public static MTransaction setDefaultTransaction(MSession session) {
        MTransaction trx = new MTransaction();
        trx.setNasabah(session.getNasabah());
        trx.setAgent(session.getAgent());
        trx.setOriginReff(session.getOriginReff());
        trx.setStan(session.getStan());
        trx.setStatus(Constants.TRX_STATUS.PENDING);
        trx.setOrigin(session.getOrigin());
        trx.setSmsInfo("");
        return trx;
    }

    public static String findSettings(String key) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            SettingRepository settingRepository = context.getBean(SettingRepository.class);
            MSettings setting = settingRepository.findOne(key);
            if (setting != null) {
                return setting.getValue();
            } else {
                return null;
            }
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
    }

    public static String getResponseFromSettings(String key) {
        ApplicationContext context;
        try {
            context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        } catch (NameRegistrar.NotFoundException ex) {
            return null;
        }
        SettingRepository settingRepository = context.getBean(SettingRepository.class);
        MSettings setting = settingRepository.findOne(key);
        if (setting != null) {
            return setting.getValue();
        } else {
            log.info("Cannot Find Setting : " + key);
            return key;
        }
    }

    public static boolean sendSms(String phoneNumber, String message, String pass, String partnerId, String partnerName) {
        LogEvent le = new LogEvent("Send SMS to Phone Number : " + phoneNumber);
        String output = "", hasil = "";
        String msisdn = Utility.formatMsisdn(phoneNumber);
        int timeout = 30000;
        String reqParam = null;

        message = message
                .replace("RC:", "-")
                .replace("rc: ", "-")
                .replace("RM: ", "-")
                .replace("rm: ", "-")
                .replace("HOST TUJUAN TIDAK BEROPERASI", "tidak beroprasi")
                .replace("Host Tujuan Tidak Beroprasi", "tidak beroprasi")
                .replace("HOST", "-")
                .replace("host", "-");

        try {
            reqParam = "<smsbc>"
                    + "<request>"
                    + "<datetime>" + LocalDateTime.now().format(Constants.FORMAT_MMDDHHMMSS) + "</datetime>"
                    + "<rrn>" + "112" + LocalDateTime.now().format(Constants.FORMAT_YYYYMMDDHHMMSS) + Utility.generateRandomNumber(14) + "</rrn>"
                    + "<partnerId>" + partnerId + "</partnerId>"
                    + "<partnerName>" + partnerName + "</partnerName>"
                    + "<password>" + pass + "</password>"
                    + "<destinationNumber>" + msisdn + "</destinationNumber>"
                    + "<message>" + message + "</message>"
                    + "</request>"
                    + "</smsbc>";
            le.addMessage(reqParam);
            le.addMessage("------------SMS View------------");
            le.addMessage("send to     : " + msisdn);
            le.addMessage(message);
            le.addMessage("date time   : " + new Date());
            le.addMessage("--------------------------------");
        } catch (Exception ex) {
            return false;
        }
        String host = Service.findSettings(KeyMap.HOST_SMS.name());
        le.addMessage("host " + host);
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            le.addMessage("SMS Response : " + hasil);
            log.info("SMS Response : " + hasil);
            wr.close();
            br.close();
            conn.disconnect();
            return true;
        } catch (Exception ex) {
            return false;
        } finally {
            log.info(le.toString());
        }
    }

    public static JSONObject serviceInquiryAgent(String accountNumber) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_AGENT_INQUIRY.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AgentInquiry/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:AgentInquiry>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<accountNumber xsi:type=\"xsd:string\">" + accountNumber + "</accountNumber>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "</param0>"
                + "</ns1:AgentInquiry>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nAgentInquiry\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("AgentInquiry\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException | IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceAgentRegistration(String accountNumber, String phoneNumber, String userId, String pin, String agentType) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_AGENT_REGISTRATION.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AgentRegistration/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:AgentRegistration>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<accountNumber xsi:type=\"xsd:string\">" + accountNumber + "</accountNumber>"
                + "<phoneNumber xsi:type=\"xsd:string\">" + phoneNumber + "</phoneNumber>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                //                + "<agentType xsi:type=\"xsd:string\">" + "1" + "</agentType>"
                + "<agentType xsi:type=\"xsd:string\">" + agentType + "</agentType>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "</param0>"
                + "</ns1:AgentRegistration>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nAgentRegistration\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("AgentRegistration\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceAgentLogin(String phoneNumber, String userId, String pin) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_AGENT_LOGIN.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AgentRegistration/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:AgentLogin>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<phoneNumber xsi:type=\"xsd:string\">" + phoneNumber + "</phoneNumber>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<type xsi:type=\"xsd:string\">1</type>"
                + "</param0>"
                + "</ns1:AgentLogin>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nAgentLogin\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/xml");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info(Utility.getFormatXML(hasil));
            log.info("AgentLogin\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceAgentUpdatePin(String userId, String pin) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_AGENT_LOGIN.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AgentRegistration/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"///LPProxy
                + "<SOAP-ENV:Body>"
                + "<ns1:AgentLogin>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<type xsi:type=\"xsd:string\">2</type>"
                + "</param0>"
                + "</ns1:AgentLogin>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nAgentUpdatePin\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("AgentUpdatePin\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceCustomerInquiry(String userId, String nik) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_CUSTOMER_INQUIRY.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/CustomerInquiry/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:CustomerInquiry>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<nik xsi:type=\"xsd:string\">" + nik + "</nik>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "</param0>"
                + "</ns1:CustomerInquiry>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nCustomerInquiry\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("CustomerInquiry\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceCustomerRegistration(String userId, String pin, String phoneNumber, String nik, String nasabahType) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_CUSTOMER_REGISTRATION.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/CustomerRegistration/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:CustomerRegistration>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<phoneNumber xsi:type=\"xsd:string\">" + phoneNumber + "</phoneNumber>"
                + "<nik xsi:type=\"xsd:string\">" + nik + "</nik>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                //                + "<accountType xsi:type=\"xsd:string\">" + "1" + "</accountType>"
                //                + "<accountType xsi:type=\"xsd:string\">" + nasabahType + "</accountType>"
                + "</param0>"
                + "</ns1:CustomerRegistration>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nCustomerRegistration\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("CustomerRegistration\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceBalanceInquiry(String userId, String phoneNumber) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_BALANCE_INQUIRY.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/BalanceInquiry/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:BalanceInquiry>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<phoneNumber xsi:type=\"xsd:string\">" + phoneNumber + "</phoneNumber>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "</param0>"
                + "</ns1:BalanceInquiry>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nInquiryBalance\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("InquiryBalance\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceAccountTransaction(String userId, String pin, String souceMsisdn, String descAccount, double amount, double bankFee, double agentFee, double feePajak, String rrn, String description, int type, String destinationBankCode) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_ACCOUNT_TRANSACTION.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AccountTransaction/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:AccountTransaction>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<sourceAccountNumber xsi:type=\"xsd:string\">" + souceMsisdn + "</sourceAccountNumber>"
                + "<destinationAccountNumber xsi:type=\"xsd:string\">" + descAccount + "</destinationAccountNumber>"
                + "<transactionAmount xsi:type=\"xsd:string\">" + Utility.setParseAmount(amount, 100) + "</transactionAmount>"
                + "<chargeAmount xsi:type=\"xsd:string\">" + Utility.stringAmount(bankFee) + "</chargeAmount>"
                + "<agentFee xsi:type=\"xsd:string\">" + Utility.stringAmount(agentFee) + "</agentFee>"
                + "<tax xsi:type=\"xsd:number\">" + Utility.stringAmount(feePajak) + "</tax>"
                + "<referenceNumber xsi:type=\"xsd:string\">" + rrn + "</referenceNumber>"
                + "<descriptionTransaction xsi:type=\"xsd:string\">" + description + "</descriptionTransaction>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<type xsi:type=\"xsd:string\">" + type + "</type>"
                + "<destinationBankCode xsi:type=\"xsd:string\">" + destinationBankCode + "</destinationBankCode>"
                + "</param0>"
                + "</ns1:AccountTransaction>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nAccountTransaction\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        JSONObject response = new JSONObject();
        response.put(Constants.REQ, reqParam);
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("AccountTransaction\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    response.put(dataNode.getNodeName(), dataNode.getTextContent());
                }

                return response;
            } catch (SOAPException ex) {
                log.error(ex);
                return response;
            } catch (IOException ex) {
                log.error(ex);
                return response;
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return response;
        } catch (IOException ex) {
            log.error(ex);
            return response;
        }
    }

    public static JSONObject serviceInquiryAccount(String userId, String pin, String descAccount, String rrn, String fromMsisdn, String amount, String destinationBankCode) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_ACCOUNT_INQUIRY.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/AccountTransaction/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:AccountTransaction>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                //                + "<sourceAccountNumber xsi:type=\"xsd:string\">-</sourceAccountNumber>"
                + "<sourceAccountNumber xsi:type=\"xsd:string\">" + fromMsisdn + "</sourceAccountNumber>"
                + "<destinationAccountNumber xsi:type=\"xsd:string\">" + descAccount + "</destinationAccountNumber>"
                + "<transactionAmount xsi:type=\"xsd:string\">" + amount + "</transactionAmount>"
                + "<chargeAmount xsi:type=\"xsd:string\">0</chargeAmount>"
                + "<agentFee xsi:type=\"xsd:string\">0</agentFee>"
                + "<tax xsi:type=\"xsd:number\">0</tax>"
                + "<referenceNumber xsi:type=\"xsd:string\">" + rrn + "</referenceNumber>"
                + "<descriptionTransaction xsi:type=\"xsd:string\">inquiryAccount</descriptionTransaction>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<type xsi:type=\"xsd:string\">1</type>"
                + "<destinationBankCode xsi:type=\"xsd:string\">" + destinationBankCode + "</destinationBankCode>"
                + "</param0>"
                + "</ns1:AccountTransaction>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nAccountInquiry\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("AccountInquiry\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }

    }

    public static JSONObject serviceUpdateNoHP(String oldMsisdn, String newMsisdn, char accountType) {
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_CHANGE_MSISDN.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/UpdateNoHP/\" "
                + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                //                + "<ns1:serviceRequestUpdateNoHP>"
                + "<ns1:UpdateNoHP>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<oldPhoneNumber xsi:type=\"xsd:string\">" + oldMsisdn + "</oldPhoneNumber>"
                + "<newPhoneNumber xsi:type=\"xsd:string\">" + newMsisdn + "</newPhoneNumber>"
                + "<accountType xsi:type=\"xsd:string\">" + accountType + "</accountType>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "</param0>"
                //                + "</ns1:serviceRequestUpdateNoHP>"
                + "</ns1:UpdateNoHP>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        log.info("reqParam : " + reqParam);
        String host = destination + additionalUrl;
        log.info("Host : " + host + "\nUpdateNoHP\nRequest:\n" + Utility.getFormatXML(reqParam));
//        log.info("Host : " + host + "\nserviceRequestUpdateNoHP\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;
        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
//            log.info("serviceRequestUpdateNoHP\nResponse:\n" + Utility.getFormatXML(hasil));
            log.info("UpdateNoHP\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject serviceInquiryPrepaid(
            String userId,
            String pin,
            String sourceAccountNumber,
            String productCode,
            String idBilling,
            double amount,
            double bankFee,
            double agentFee,
            String additionalData,
            Constants.TRX_TYPE transactionType,
            String jobIdInquiry,
            String accountType,
            String stan) {

        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_PAYMENT.name());
        String formatAmount = Utility.setParseAmount(amount, 100);
        String formatBankFee = Utility.stringAmount(bankFee);
        String formatAgentFee = Utility.stringAmount(agentFee);
        String formatAdditionalData = additionalData;
        String formatJobIdInquiry = jobIdInquiry;
        //SPec 2.4.1
        if (null == transactionType) {
            transactionType = Constants.TRX_TYPE.INQUIRY;
        }

        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/Payment/\" "
                + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:Payment>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<sourceAccountNumber xsi:type=\"xsd:number\">" + sourceAccountNumber + "</sourceAccountNumber>"
                + "<productCode xsi:type=\"xsd:number\">" + productCode + "</productCode>"
                + "<idBilling xsi:type=\"xsd:number\">" + idBilling + "</idBilling>"
                + "<transactionAmount xsi:type=\"xsd:number\">" + formatAmount + "</transactionAmount>"
                + "<fee xsi:type=\"xsd:number\">" + formatBankFee + "</fee>"
                //                + "<agentFee xsi:type=\"xsd:number\">" + formatAgentFee + "</agentFee>"
                + "<additionalData xsi:type=\"xsd:text\">" + formatAdditionalData + "</additionalData>"
                + "<transactionType xsi:type=\"xsd:text\">" + transactionType.ordinal() + "</transactionType>"
                + "<jobIdInquiry xsi:type=\"xsd:number\">" + formatJobIdInquiry + "</jobIdInquiry>"
                + "<accountType xsi:type=\"xsd:string\">" + accountType + "</accountType>"
                + "<stan xsi:type=\"xsd:string\">" + stan + "</stan>"
                + "</param0>"
                + "</ns1:Payment>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";

        log.info(
                "reqParam : " + reqParam);
        String host = destination + additionalUrl;

        log.info(
                "Host : " + host + "\nPayment\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;

        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("Payment\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject servicePaymentString(
            String userId,
            String pin,
            String sourceAccountNumber,
            String productCode,
            String idBilling,
            double amount,
            double bankFee,
            double agentFee,
            double pajakFee,
            String additionalData,
            String trxType,
            String jobIdInquiry,
            String accountType,
            String stan) {
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_PAYMENT.name());
        String formatAmount = Utility.setParseAmount(amount, 100);
        String formatBankFee = Utility.stringAmount(bankFee);
        String formatAgentFee = Utility.stringAmount(agentFee);
        String formatPajakFee = Utility.stringAmount(pajakFee);
//        String formatPajakFee = String.valueOf(pajakFee);

        String formatAdditionalData = additionalData;
        String formatJobIdInquiry = jobIdInquiry;
        //SPec 2.4.1
        if (null == trxType) {
            trxType = "1";
        }
        switch (trxType) {
            case "1":
                formatAmount = "Null";
                formatBankFee = "Null";
                formatAgentFee = "Null";
                formatPajakFee = "Null";
                formatAdditionalData = "Null";
                formatJobIdInquiry = "Null";
                break;
            case "2":
                break;
            case "3":
                break;
            default:
                break;
        }
        String output = "", hasil = "";
//        int timeout = 10000;
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/Payment/\" "
                + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:Payment>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<sourceAccountNumber xsi:type=\"xsd:number\">" + sourceAccountNumber + "</sourceAccountNumber>"
                + "<productCode xsi:type=\"xsd:number\">" + productCode + "</productCode>"
                + "<idBilling xsi:type=\"xsd:number\">" + idBilling + "</idBilling>"
                + "<transactionAmount xsi:type=\"xsd:number\">" + formatAmount + "</transactionAmount>"
                + "<fee xsi:type=\"xsd:number\">" + formatBankFee + "</fee>"
                + "<agentFee xsi:type=\"xsd:number\">" + formatAgentFee + "</agentFee>"
                + "<tax xsi:type=\"xsd:number\">" + formatPajakFee + "</tax>"
                + "<additionalData xsi:type=\"xsd:text\">" + formatAdditionalData + "</additionalData>"
                + "<transactionType xsi:type=\"xsd:text\">" + trxType + "</transactionType>"
                + "<accountType xsi:type=\"xsd:string\">" + accountType + "</accountType>"
                + "<jobIdInquiry xsi:type=\"xsd:number\">" + formatJobIdInquiry + "</jobIdInquiry>"
                + "<stan xsi:type=\"xsd:string\">" + stan + "</stan>"
                + "</param0>"
                + "</ns1:Payment>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";

        log.info(
                "reqParam : " + reqParam);
        String host = destination + additionalUrl;

        log.info(
                "Host : " + host + "\nPayment\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;

        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("Payment\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }

                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static JSONObject servicePayment(
            String userId,
            String pin,
            String sourceAccountNumber,
            String productCode,
            String idBilling,
            double amount,
            double bankFee,
            double agentFee,
            String additionalData,
            Constants.TRX_TYPE transactionType,
            String jobIdInquiry,
            String accountType,
            String stan) {
        String channelId = Service.findSettings(KeyMap.CHANNEL_ID.name());
        String servicePassword = Service.findSettings(KeyMap.SERVICE_PASSWORD.name());
        String destination = Service.findSettings(KeyMap.IP_DESTINATION.name());
        String additionalUrl = Service.findSettings(KeyMap.URL_PAYMENT.name());
        String formatAmount = Utility.setParseAmount(amount, 100);
        String formatBankFee = Utility.stringAmount(bankFee);
        String formatAgentFee = Utility.stringAmount(agentFee);
        String formatAdditionalData = additionalData;
        String formatJobIdInquiry = jobIdInquiry;
        //SPec 2.4.1
        if (null == transactionType) {
            transactionType = Constants.TRX_TYPE.INQUIRY;
        }
        switch (transactionType) {
            case INQUIRY:
                formatAmount = "Null";
                formatBankFee = "Null";
                formatAgentFee = "Null";
                formatAdditionalData = "Null";
                formatJobIdInquiry = "Null";
                break;
            case POSTING:
                break;
            case TOP_UP:
                break;
            default:
                break;
        }
        String output = "", hasil = "";
        int timeout = 50000;
        String reqParam = null;
        reqParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:ns1=\"https://lpproxy.bpddiy.int:4646/Payment/\" "
                + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" >"
                + "<SOAP-ENV:Body>"
                + "<ns1:Payment>"
                + "<param0 xsi:type=\"SOAP-ENC:Struct\">"
                + "<channelId xsi:type=\"xsd:string\">" + channelId + "</channelId>"
                + "<servicePassword xsi:type=\"xsd:string\">" + servicePassword + "</servicePassword>"
                + "<userId xsi:type=\"xsd:string\">" + userId + "</userId>"
                + "<pin xsi:type=\"xsd:string\">" + pin + "</pin>"
                + "<sourceAccountNumber xsi:type=\"xsd:number\">" + sourceAccountNumber + "</sourceAccountNumber>"
                + "<productCode xsi:type=\"xsd:number\">" + productCode + "</productCode>"
                + "<idBilling xsi:type=\"xsd:number\">" + idBilling + "</idBilling>"
                + "<transactionAmount xsi:type=\"xsd:number\">" + formatAmount + "</transactionAmount>"
                + "<fee xsi:type=\"xsd:number\">" + formatBankFee + "</fee>"
                + "<agentFee xsi:type=\"xsd:number\">" + formatAgentFee + "</agentFee>"
                + "<additionalData xsi:type=\"xsd:text\">" + formatAdditionalData + "</additionalData>"
                + "<transactionType xsi:type=\"xsd:text\">" + transactionType.ordinal() + "</transactionType>"
                + "<jobIdInquiry xsi:type=\"xsd:number\">" + formatJobIdInquiry + "</jobIdInquiry>"
                + "<accountType xsi:type=\"xsd:string\">" + accountType + "</accountType>"
                + "<stan xsi:type=\"xsd:string\">" + stan + "</stan>"
                + "</param0>"
                + "</ns1:Payment>"
                + "</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";

        log.info(
                "reqParam : " + reqParam);
        String host = destination + additionalUrl;

        log.info(
                "Host : " + host + "\nPayment\nRequest:\n" + Utility.getFormatXML(reqParam));
        URL url;
        HttpURLConnection conn = null;

        try {
            url = new URL(host);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeout);
            conn.setConnectTimeout(timeout);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            wr.close();
            br.close();
            conn.disconnect();
            log.info("Payment\nResponse:\n" + Utility.getFormatXML(hasil));
            try {
                JSONObject json = new JSONObject();
                InputStream is = new ByteArrayInputStream(hasil.getBytes());
                SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
                SOAPBody soapBody = request.getSOAPBody();
                Node nodeAgentInquiryResponse = (Node) soapBody.getChildElements().next();
                NodeList listServiceResponse = nodeAgentInquiryResponse.getChildNodes();
                Node serviceResponse = (Node) listServiceResponse.item(0);
                NodeList listDataService = serviceResponse.getChildNodes();
                for (int i = 0; i < listDataService.getLength(); i++) {
                    Node dataNode = (Node) listDataService.item(i);
                    json.put(dataNode.getNodeName(), dataNode.getTextContent());
                }
                return json;
            } catch (SOAPException ex) {
                log.error(ex);
                return new JSONObject();
            } catch (IOException ex) {
                log.error(ex);
                return new JSONObject();
            }
        } catch (ProtocolException | MalformedURLException ex) {
            log.error(ex);
            return new JSONObject();
        } catch (IOException ex) {
            log.error(ex);
            return new JSONObject();
        }
    }

    public static MNotification sendNotif(MNotification notif) {
        LogEvent le = new LogEvent("Send Notification");
        if (null == notif) {
            le.addMessage("Parameter is null,cannot process notification");
            log.info(le);
        }
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("message", notif.getMessage());
        data.put("title", notif.getTitle());
        if (notif.getImage().isEmpty()) {
            data.put("image", "");
        } else {
            data.put("image", notif.getImage());
        }
        request.put("id", Utility.generateRandomNumber(15));
        request.put("from", "lkpddiy");
        request.put("data", data);
        if ("all".equals(notif.getAgent().toLowerCase())) {
            request.put("to", Service.findSettings(KeyMap.NOTIFICATION_ALL.name()));
        } else {
            request.put("to", notif.getToken());
        }
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        String hostNotification = Service.findSettings(KeyMap.HOST_NOTIFICATION.name());
        HttpPost postRequest = new HttpPost(hostNotification);
        StringEntity body = new StringEntity(request.toString(), "UTF-8");
        postRequest.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
        postRequest.setEntity(body);
        try {
            HttpResponse response = (HttpResponse) httpClient.execute(postRequest);
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            int RC = response.getStatusLine().getStatusCode();
            String hasil = "";
            String output;
            while ((output = br.readLine()) != null) {
                hasil = hasil + output;
            }
            le.addMessage(hostNotification + "\n" + "Request :" + request.toString() + "\n" + new Date() + "\n" + "Response : " + hasil);
            if (RC == HttpStatus.SC_OK) {
                le.addMessage("Success");
                notif.setStatus(1);
            } else {
                le.addMessage("Failed");
                notif.setStatus(2);
            }
            notif.setDescription(hasil);
            return notif;
        } catch (IOException ex) {
            le.addMessage(ex);
            notif.setStatus(2);
            notif.setDescription("ex:" + ex.getMessage());
            return notif;
        } finally {
            log.info(le.toString());
            try {
                httpClient.close();
            } catch (IOException ex) {
                notif.setStatus(2);
                notif.setDescription("ex:" + ex.getMessage());
                return notif;
            }
        }
    }

}
