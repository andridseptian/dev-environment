/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.util.Service;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentCheckOTPParticipant implements TransactionParticipant, Configurable {

    public Configuration cfg;
    private OTPRepository otpRepository;
    private AgentRepository agentRepository;

    public AgentCheckOTPParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        otpRepository = context.getBean(OTPRepository.class);
        agentRepository = context.getBean(AgentRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        if (agent.getStatus() != Constants.Status.NEED_APPROVAL) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Status Agent tidak membutuhkan approval");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED|NO_JOIN;
        }
        try {
            JSONObject obj = new JSONObject(req.getRequest());
            String iId = obj.getString(Constants.PARAM.ID);
            String iOtp = obj.getString(Constants.PARAM.OTP);
            Service.saveSessionWeb(iId, "Check OTP "+iOtp);
            MOtp otp = Service.findOTP(iOtp);
            Boolean statusOTP = LocalDateTime.now().isBefore(otp.getEndTime());
            if (otp.getStatus().equals(Constants.OtpStatus.USED)) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            } else if (otp.getStatus().equals(Constants.OtpStatus.INACTIVE)) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            if (!statusOTP) {
                Service.setOTPStatus(otp, Constants.OtpStatus.INACTIVE);
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            } else {
                otp.setStatus(Constants.OtpStatus.USED);
                otpRepository.saveAndFlush(otp);
                req.setStatus(Constants.TRX_STATUS.SUCCESS);
                agent.setStatus(Constants.Status.NEED_APPROVAL_CS);
                agentRepository.save(agent);
                ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Aktivasi Agent Sukses"));
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return PREPARED | NO_JOIN;
            }
        } catch (NullPointerException ex) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
