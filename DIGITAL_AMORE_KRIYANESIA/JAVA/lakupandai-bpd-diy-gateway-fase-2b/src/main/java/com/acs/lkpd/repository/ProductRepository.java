/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MProduct;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 *
 * @author ACS
 */
public interface ProductRepository extends JpaRepository<MProduct, String> {

    List<MProduct> findByTypeAndStatus(Constants.ProductType type, Constants.ProductStatus status);

    @Query(nativeQuery = true, name = "findDataPembelian")
    List<Object[]> listProductPembelian();
    
    MProduct findByProductCode(String productCode);
}
