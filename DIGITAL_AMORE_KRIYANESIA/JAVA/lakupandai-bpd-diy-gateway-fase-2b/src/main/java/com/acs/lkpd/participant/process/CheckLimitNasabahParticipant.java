/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNasabah;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class CheckLimitNasabahParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        int fieldAmount = cfg.getInt("field_amount", 0);
        String type = cfg.get("type", "-");
        double totalTransaction = 0;
        double transaction = 0;
        try {
            transaction = Double.valueOf(request[fieldAmount]);
        } catch (NumberFormatException | NullPointerException ex) {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        switch (type) {
            case "Tarik Tunai":
                totalTransaction = Service.findTotalTransaction(nasabah, Constants.DESC_TARIK_TUNAI);
                break;
            case "Transfer On Us":
                totalTransaction = Service.findTotalTransaction(nasabah, Constants.DESC_TRANSFER_ON_US);
                break;
            case "Pembelian Pulsa":
                totalTransaction = Service.findTotalTransaction(nasabah, Constants.DESC_PEMBELIAN_PULSA);
                break;
            default:
                totalTransaction = 0;
                break;
        }
        double totalTrx = totalTransaction + transaction;
        log.info("totalTransaction : " + totalTransaction + ", transaction :" + transaction + ", Total :" + totalTrx);
        if (Service.isLimitNasabah(totalTrx, Constants.LIMIT_TYPE.MONTHLY)) {
            session.setRcToClient(Constants.RC_CARD_OVERLIMIT);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_OVERLIMIT.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        } else {
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
