/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONObject;

/**
 *
 * @author ACS
 */
public class GenerateRespParticipant implements TransactionParticipant, Configurable {

    private Configuration cfg;


    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String[] config = cfg.get("data").split(",");
        JSONObject resp = (JSONObject) ctx.get(Constants.OBJ.RESP);
        if (resp == null) {
            resp = new JSONObject();
        }
        JSONObject mergeResp = new JSONObject();
        try {
            for (String data : config) {
                ctx.log("data : "+data);
                String[] parsing = data.split("\\|");
                mergeResp.put(parsing[0], ctx.get(parsing[1]));
            }
        } catch (Exception ex) {
            ctx.log("error not found in resp : " + ex);
        }
        ctx.log("Add Resp : "+mergeResp.toString());
        ctx.put(Constants.OBJ.RESP, Utility.merge(resp,mergeResp));
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
