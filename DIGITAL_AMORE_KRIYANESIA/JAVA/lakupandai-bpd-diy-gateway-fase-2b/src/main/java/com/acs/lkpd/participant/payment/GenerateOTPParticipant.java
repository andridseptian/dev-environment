
package com.acs.lkpd.participant.payment;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MTransaction;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class GenerateOTPParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MOtp otp = Service.generateOTPPaymentWithProduct(session.getAgent().getMsisdn(), session);
        if (otp == null) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_GENERATE_OTP_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        } else {
            isomsg.set(61, otp.getOtp());
            session.setOtp(otp.getOtp());
            trx.setOtp(otp.getOtp());
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG,trx);
            ctx.put(Constants.IN, isomsg);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
