package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author yogha
 */
public class CreateTransactionPaymentParticipant implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private TransactionRepository transactionRepository;

    public CreateTransactionPaymentParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        if (trx != null) {
            return PREPARED | NO_JOIN;
        } else {
            String paramType = cfg.get("trx_type", "inquiry");
            String paramDescription = cfg.get("trx_description", "-");
            MTransaction newTrx = Service.setDefaultTransaction(session);
            newTrx.setStatus(Constants.TRX_STATUS.FAILED);
            switch (paramType) {
                case "inquiry":
                    newTrx.setType(Constants.TRX_TYPE.INQUIRY);
                    break;
                case "posting":
                    newTrx.setType(Constants.TRX_TYPE.POSTING);
                    break;
            }
            newTrx.setDescription(paramDescription);
            newTrx = transactionRepository.save(newTrx);
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, newTrx);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String paramType = cfg.get("trx_type", "inquiry");
        String paramDescription = cfg.get("trx_description", "-");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = new MTransaction();
        switch (paramType) {
            case "inquiry":
                trx = Service.setDefaultTransaction(session);
                trx.setType(Constants.TRX_TYPE.INQUIRY);
                trx.setDescription(paramDescription);
                trx.setStatus(Constants.TRX_STATUS.PENDING);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            case "posting":
                MOtp otp = Service.findOTP(session.getOtp());
                Service.setOTPStatus(otp, Constants.OtpStatus.USED);
                MTransaction inquiryTrx = transactionRepository.findFirstByTypeAndAgentAndNasabahAndOtpOrderByCrTimeDesc(Constants.TRX_TYPE.INQUIRY, session.getAgent(), session.getNasabah(), otp.getOtp());
                if (null == inquiryTrx) {
                    session.setRcToClient(Constants.RC_INTERNAL_ERROR);
//                    session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_TRANSACTION_NOT_FOUND.name()));
                    ctx.put(Constants.SESSION, session);
                    ctx.put(Constants.TXLOG, trx);
                    return ABORTED | NO_JOIN;
                }
                trx = Service.setDefaultTransaction(session);
                trx.setType(Constants.TRX_TYPE.POSTING);
                trx.setDescription(paramDescription);
                trx.setStatus(Constants.TRX_STATUS.PENDING);
                trx.setNasabah(trx.getNasabah());
                trx.setAgent(trx.getAgent());
                trx.setAmount(inquiryTrx.getAmount());
                trx.setFeeAgent(inquiryTrx.getFeeAgent());
                trx.setFeeBank(inquiryTrx.getFeeBank());
                trx.setTotalAmount(inquiryTrx.getTotalAmount());
                trx.setProductCode(inquiryTrx.getProductCode());
                trx.setOtp(session.getOtp());
                trx.setAccountType(inquiryTrx.getAccountType());
                trx.setIdBilling(inquiryTrx.getIdBilling());
                trx.setAdditionalData(inquiryTrx.getAdditionalData());
                trx = transactionRepository.save(trx);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.OBJ.INQTRX, inquiryTrx);
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            default:
                return ABORTED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
