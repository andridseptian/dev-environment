/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.util.EncryptUtility;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentSendPasswordParticipant implements TransactionParticipant, Configurable {

    public Configuration cfg;
    private final AgentRepository agentRepository;

    public AgentSendPasswordParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String iId = dataRequest.getString(Constants.PARAM.ID);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String descResponse = "";
        
//        if (agent)
        
        if (agent.isStatusResetPin()) {
            Service.saveSessionWeb(iId, "Reset Password Agent " + agent.getId());
        } else {
            Service.saveSessionWeb(iId, "Resend Password Agent " + agent.getId());
        }
        descResponse = Service.getResponseFromSettings(KeyMap.RM_PASS_AGENT.name());
        if (agent.isStatusPin()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Status Password Agent sudah tidak dapat diubah");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        try {
            if (sendSMS) {
                EncryptUtility eu = new EncryptUtility(Service.findSettings(KeyMap.DES_KEY.name()));
                if (agent.isStatusResetPin()) {
                    agent.setPin(eu.encrypt(Utility.generateStan()));
                    agent.setStatusResetPin(false);
                    agent.setStatusPin(true);
                    agent = agentRepository.save(agent);
                }
                String plainPin = eu.decrypt(agent.getPin());
                descResponse = descResponse
                        .replace("[UserId]", agent.getId())
                        .replace("[Pin]", plainPin);
                boolean statusSMS = Utility.sendSMS(descResponse, agent.getMsisdn());
                if (statusSMS) {
                    String messageResponse = "SMS success[" + Utility.viewMsisdn(agent.getMsisdn()) + "]";
                    ctx.log(messageResponse);
                    req.setInfo(messageResponse);
                    req.setStatus(Constants.TRX_STATUS.SUCCESS);
                    ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Send Password Sementara Success"));
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return PREPARED | NO_JOIN;
                } else {
                    String rm = "Status pesan tidak dapat terkirim, ulangi beberapa saat lagi";
                    req.setRm(rm);
                    req.setInfo("SMS failed[" + Utility.viewMsisdn(agent.getMsisdn()) + "]");
                    req.setStatus(Constants.TRX_STATUS.FAILED);
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
            } else {
                String rm = "Not Send SMS[" + Utility.viewMsisdn(agent.getMsisdn()) + "]";
                req.setInfo(rm);
                req.setStatus(Constants.TRX_STATUS.SUCCESS);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return PREPARED | NO_JOIN;
            }
        } catch (Exception ex) {
            ctx.log(ex.getCause());
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
