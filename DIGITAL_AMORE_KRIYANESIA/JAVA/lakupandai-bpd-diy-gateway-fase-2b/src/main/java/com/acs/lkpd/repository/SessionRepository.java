/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MSession;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ACS
 */
public interface SessionRepository extends JpaRepository<MSession, Long> {

    MSession findFirstByStanAndAgentOrderByCrTimeDesc(String stan, MAgent agent);

    MSession findFirstByOriginReffAndAgentOrderByCrTimeDesc(String originReff, MAgent agent);
    
    MSession findFirstByOtpAndAgentOrderByCrTimeDesc(String otp, MAgent agent);

    MSession findFirstByAgentAndNasabahAndOtpOrderByCrTimeDesc(MAgent agent, MNasabah nasabah, String otp);

    @Query("SELECT s FROM MSession s WHERE s.agent = :agent AND s.nasabah = :nasabah AND s.otp = :otp ORDER BY s.crTime DESC")
    List<MSession> findLastTransaction(
            @Param("agent") MAgent agent,
            @Param("nasabah") MNasabah nasabah,
            @Param("otp") String otp);

}
