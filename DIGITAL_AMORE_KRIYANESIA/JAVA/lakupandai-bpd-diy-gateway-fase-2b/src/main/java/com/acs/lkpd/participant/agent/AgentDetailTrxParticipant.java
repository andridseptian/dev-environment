/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.agent;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.FormResponse;
import com.acs.util.FormatTanggal;
import com.acs.util.FormatTransaksiTerakhir;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Andri D. Septian
 */
public class AgentDetailTrxParticipant implements TransactionParticipant, Configurable {
    
    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final NotificationTrxRepository notificationTrxRepository;
    private final TransactionRepository transactionRepository;
    private final SessionRepository sessionRepository;
    private final ProductRepository productRepository;
    private final NasabahRepository nasabahRepository;
    private final AgentRepository agentRepository;
    
    public AgentDetailTrxParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
        sessionRepository = context.getBean(SessionRepository.class);
        productRepository = context.getBean(ProductRepository.class);
        nasabahRepository = context.getBean(NasabahRepository.class);
        agentRepository = context.getBean(AgentRepository.class);
    }
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MNotifikasiTrx tempNotif = null;
        
        ctx.log("--Agent Detail Transaction Participant--");
        
        String fld48 = ctx.getString(Constants.FLD48);
        ctx.log("REQUEST DATA: " + fld48);
        try
        {
            ctx.log("searching origin reff");
            ctx.log("fld48: " + fld48);
            tempNotif = notificationTrxRepository.findFirstByOriginReff(fld48);
            if (tempNotif == null)
            {
                ctx.log("searching bank reff");
                tempNotif = notificationTrxRepository.findFirstByBankReff(fld48);
            }

            if (tempNotif != null)
            {
                tempNotif.setIsRead(Boolean.TRUE);
                MProduct product = productRepository.findByProductCode(tempNotif.getProductCode());
                
                try
                {
                    ctx.log("product code   : " + product.getProductCode());
                    ctx.log("product name   : " + product.getProductName());
                    ctx.log("trx status     : " + tempNotif.getStatus());
                    ctx.log("trx descr      : " + tempNotif.getDescription());
                } catch (Exception e)
                {
                    ctx.log("trx status     : " + tempNotif.getStatus());
                    ctx.log("trx descr      : " + tempNotif.getDescription());
                }
                
                if (tempNotif.getDescription().contains("Setor Tunai"))
                {   
                    JSONObject bankResponseDetail = new JSONObject(tempNotif.getBankResponseDetail());
                    ctx.log("bankResponseDetail: " + bankResponseDetail);
                    MNasabah targetNasabah = nasabahRepository.findFirstByMsisdn(bankResponseDetail.getString("destinationAccountNumber"));
                    
                    String crtime = String.valueOf(tempNotif.getCrTime());
                    ctx.log(tempNotif.getCrTime());
                    String tanggal = crtime.substring(0, 16).replace('T', ' ');
                    ctx.log("tanggal " + tanggal);
                    String parsingDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(tanggal);
                    ctx.log("waktu " + parsingDate);
                    
                    String format = FormResponse.bank.setorTunai();
                    
                    String nominal = "nominal tidak ditemukan";
                    if (bankResponseDetail.has("transactionAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100));
                    } else if (bankResponseDetail.has("chargeAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("chargeAmount"), 100));
                    }
                    
                    format = format
                            .replace("[LAYANAN]", product.getProductName())
                            .replace("[WAKTU]", parsingDate)
                            .replace("[RESI]", bankResponseDetail.has("rrn") ? bankResponseDetail.getString("rrn")
                                    : bankResponseDetail.has("referenceNumber") ? bankResponseDetail.getString("referenceNumber") : "-")
                            .replace("[NOMINAL]", nominal)
                            .replace("[NASABAH]", targetNasabah != null ? targetNasabah.getName()
                                    : bankResponseDetail.has("destinationAccountName") ? bankResponseDetail.getString("destinationAccountName")
                                    : "-")
                            .replace("[NOHP]", targetNasabah != null ? Utility.viewMsisdn(targetNasabah.getMsisdn())
                                    : bankResponseDetail.has("destinationAccountNumber") ? bankResponseDetail.getString("destinationAccountNumber")
                                    : "-")
                            .replace("[NOREK]", targetNasabah != null ? Utility.viewMsisdn(targetNasabah.getMsisdn())
                                    : bankResponseDetail.has("destinationAccountNumber") ? bankResponseDetail.getString("destinationAccountNumber")
                                    : "-")
                            .replace("[STATUS]", bankResponseDetail.has("responseDesc") ? bankResponseDetail.getString("responseDesc") : "-");
                    
                    String response = new JSONObject().put("data", new JSONArray(format)).toString();
                    
                    ctx.log("Response: " + response);
//                    tempSession.setResponseToClient(response);
                    session.setResponseToClient(response);
                    ctx.put(Constants.SESSION, session);
                } else if (tempNotif.getDescription().equals("Tarik Tunai"))
                {
                    MTransaction tempTrx = transactionRepository.findByOriginReff(tempNotif.getOriginReff());
                    product = productRepository.findByProductCode("210000");
                    JSONArray formatInquiryArray = new JSONArray(product.getFormatInquiry());
                    
                    JSONObject bankResponseDetail = new JSONObject(tempNotif.getBankResponseDetail());
                    ctx.log("bankResponseDetail: " + bankResponseDetail);
                    MNasabah sourceNasabah = nasabahRepository.findFirstByMsisdn(bankResponseDetail.getString("sourceAccountNumber"));
                    
                    JSONObject waktu = new JSONObject();
                    String crtime = String.valueOf(tempNotif.getCrTime());
                    String tanggal = crtime.substring(0, 16).replace('T', ' ');
                    Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(tanggal);
//                    String parsingDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date);
                    String parsingDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(tanggal);
                    
                    String format = FormResponse.bank.tarikTunai();
                    
                    String nominal = "nominal tidak ditemukan";
                    if (bankResponseDetail.has("transactionAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100));
                    } else if (bankResponseDetail.has("chargeAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("chargeAmount"), 100));
                    }
                    
                    format = format
                            .replace("[LAYANAN]", product.getProductName())
                            .replace("[WAKTU]", parsingDate)
                            .replace("[RESI]", bankResponseDetail.has("rrn") ? bankResponseDetail.getString("rrn")
                                    : bankResponseDetail.has("referenceNumber") ? bankResponseDetail.getString("referenceNumber") : "-")
                            .replace("[REKPENERIMA]", sourceNasabah != null ? Utility.viewMsisdn(sourceNasabah.getMsisdn()) : "-")
                            .replace("[NAMAPENERIMA]", sourceNasabah != null ? sourceNasabah.getName() : "-")
                            .replace("[NOMINAL]", nominal)
                            .replace("[STATUS]", bankResponseDetail.has("responseDesc") ? bankResponseDetail.getString("responseDesc") : "-");
                    
                    String response = new JSONObject().put("data", new JSONArray(format)).toString();
                    
                    ctx.log("Response: " + response);
                    session.setResponseToClient(response);
                    ctx.put(Constants.SESSION, session);
                } else if (tempNotif.getDescription().equals("Transfer Lakupandai"))
                {
                    
                    product = productRepository.findByProductCode("421100");
                    JSONArray formatInquiryArray = new JSONArray(product.getFormatInquiry());
                    
                    JSONObject bankResponseDetail = new JSONObject(tempNotif.getBankResponseDetail());
                    ctx.log("bankResponseDetail: " + bankResponseDetail);
                    MNasabah targetNasabah = nasabahRepository.findFirstByMsisdn(bankResponseDetail.getString("destinationAccountNumber"));
                    MNasabah sourceNasabah = nasabahRepository.findFirstByMsisdn(bankResponseDetail.getString("sourceAccountNumber"));
                    
                    JSONObject waktu = new JSONObject();
                    String crtime = String.valueOf(tempNotif.getCrTime());
                    String tanggal = crtime.substring(0, 16).replace('T', ' ');
                    Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(tanggal);
//                    String parsingDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date);
                    String parsingDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(tanggal);
                    
                    String format = FormResponse.bank.transferLakupandai();
                    
                    MTransaction tempTrx = transactionRepository.findByOriginReff(tempNotif.getOriginReff());
                    
                    String nominal = "nominal tidak ditemukan";
                    if (bankResponseDetail.has("transactionAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100));
                    } else if (bankResponseDetail.has("chargeAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("chargeAmount"), 100));
                    }
                    
                    format = format
                            .replace("[LAYANAN]", product.getProductName())
                            .replace("[WAKTU]", parsingDate)
                            .replace("[RESI]", bankResponseDetail.has("rrn") ? bankResponseDetail.getString("rrn")
                                    : bankResponseDetail.has("referenceNumber") ? bankResponseDetail.getString("referenceNumber") : "-")
                            .replace("[REKPENERIMA]", bankResponseDetail != null ? Utility.viewMsisdn(bankResponseDetail.getString("destinationAccountNumber")) : "-")
                            .replace("[NAMAPENERIMA]", tempTrx.getInfo() != null ? tempTrx.getInfo() : "-")
                            .replace("[NOMINAL]", nominal)
                            .replace("[NAMAPENGIRIM]", sourceNasabah != null ? sourceNasabah.getName() : "-")
                            .replace("[NOHPPENGIRIM]", sourceNasabah != null ? Utility.viewMsisdn(sourceNasabah.getMsisdn()) : "-")
                            .replace("[STATUS]", bankResponseDetail.has("responseDesc") ? bankResponseDetail.getString("responseDesc") : "-");
                    
                    String response = new JSONObject().put("data", new JSONArray(format)).toString();

//                    String response = new JSONObject().put("data", formatInquiryArray).toString();
                    ctx.log("RESPONSE: " + response);
                    session.setResponseToClient(response);
                    ctx.put(Constants.SESSION, session);
                } else if (tempNotif.getDescription().equals("Transfer On Us"))
                {
                    product = productRepository.findByProductCode("411300");
//                    JSONArray formatInquiryArray = new JSONArray(product.getFormatInquiry());

                    JSONObject bankResponseDetail = new JSONObject(tempNotif.getBankResponseDetail());
                    ctx.log("bankResponseDetail: " + bankResponseDetail);
                    MAgent targetAgent = agentRepository.findFirstByAccountNumber(bankResponseDetail.getString("destinationAccountNumber"));
                    MNasabah sourceNasabah = nasabahRepository.findFirstByMsisdn(bankResponseDetail.getString("sourceAccountNumber"));
                    
                    JSONObject waktu = new JSONObject();
                    String crtime = String.valueOf(tempNotif.getCrTime());
                    String tanggal = crtime.substring(0, 16).replace('T', ' ');
                    Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(tanggal);
//                    String parsingDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date);
                    String parsingDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(tanggal);
                    
                    String format = FormResponse.bank.transferOnUs();
                    
                    MTransaction tempTrx = transactionRepository.findByOriginReff(tempNotif.getOriginReff());
                    
                    String nominal = "nominal tidak ditemukan";
                    if (bankResponseDetail.has("transactionAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100));
                    } else if (bankResponseDetail.has("chargeAmount"))
                    {
                        nominal = Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("chargeAmount"), 100));
                    }
                    
                    format = format
                            .replace("[LAYANAN]", product.getProductName())
                            .replace("[WAKTU]", parsingDate)
                            .replace("[RESI]", bankResponseDetail.has("rrn") ? bankResponseDetail.getString("rrn")
                                    : bankResponseDetail.has("referenceNumber") ? bankResponseDetail.getString("referenceNumber") : "-")
                            .replace("[REKPENERIMA]", bankResponseDetail != null ? bankResponseDetail.getString("destinationAccountNumber") : "-")
                            .replace("[NAMAPENERIMA]", tempTrx.getInfo() != null ? tempTrx.getInfo() : "-")
                            .replace("[NOMINAL]", nominal)
                            .replace("[NAMAPENGIRIM]", sourceNasabah != null ? sourceNasabah.getName() : "-")
                            .replace("[NOHPPENGIRIM]", sourceNasabah != null ? Utility.viewMsisdn(sourceNasabah.getMsisdn()) : "-")
                            .replace("[STATUS]", bankResponseDetail.has("responseDesc") ? bankResponseDetail.getString("responseDesc") : "-");
                    
                    String response = new JSONObject().put("data", new JSONArray(format)).toString();
                    
                    ctx.log("RESPONSE: " + response);
                    session.setResponseToClient(response);
                    ctx.put(Constants.SESSION, session);
                } else
                {
                    ctx.log("get format cek transaksi terakhir");
                    JSONObject formatCekTransaksiTerkahir = new FormatTransaksiTerakhir().getFormatNotif(tempNotif, tempNotif.getStatus().toString(), product.getProductName());
                    ctx.log("INFO : " + formatCekTransaksiTerkahir);
                    session.setRcToClient(Constants.RC_APPROVED);
                    session.setResponseToClient(formatCekTransaksiTerkahir.toString());
                    ctx.put(Constants.SESSION, session);
//                    if (tempNotif.getStatus().equals(Constants.TRX_STATUS.PROCESS)) {
//                        ctx.put(Constants.SESSION, tempSession);
//                    } else {
//                        ctx.put(Constants.SESSION, session);
//                    }
                }
                
                ctx.put(Constants.TXLOG, trx);
                notificationTrxRepository.save(tempNotif);
                return PREPARED;
            } else
            {
                ctx.log("Inquiry Transaksi Terakhir Null Data");
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
                return ABORTED;
            }
        } catch (Exception e)
        {
            ctx.log(e);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_TRANSACTION_NOT_FOUND));
            return ABORTED;
        }
    }
    
    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
