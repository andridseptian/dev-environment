/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MCabang;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CustomerRegistrationProcess implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final RcRepository rcRepository;
    private final NasabahRepository nasabahRepository;

    public CustomerRegistrationProcess() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
        nasabahRepository = context.getBean(NasabahRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        if (null == agent) {
            session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
        MNasabah nasabah = session.getNasabah();
        if (null == nasabah) {
            session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
        MCabang cabang = agent.getCabang();
        if (null == agent.getCabang()) {
            session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_CABANG_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
        log.info("Agent Type: " + agent.getAgentType());
        log.info("Nasbah Type: " + nasabah.getNasabahType());
        log.info("Nasbah Nik: " + nasabah.getNik());
        JSONObject hasil = Service.serviceCustomerRegistration(agent.getId(), agent.getPin(), Utility.formatMsisdn(nasabah.getMsisdn()), nasabah.getNik(), agent.getAgentType());
        log.info("RESP : " + hasil.toString());
        log.info("Response to Client : " + hasil.getString(Constants.PARAM.WS_RESPONSEDESC));
        log.info("RC : " + hasil.getString(Constants.PARAM.WS_RESPONSEDESC));

        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {

//            log.info("RESP : " + hasil.toString());
//            log.info("Response to Client : " + hasil.getString(Constants.PARAM.WS_RESPONSEDESC));
//            log.info("RC : " + hasil.getString(Constants.PARAM.WS_RESPONSEDESC));

            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String responseMessage = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            }
            ctx.log("Check " + Constants.SERVICE_CUSTOMER_REGISTRATION + " " + rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_CUSTOMER_REGISTRATION, rc);
            if (mapRc == null) {
                session.setRcToClient(Constants.RC_DB_ERROR);
                session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]");
                ctx.put(Constants.SESSION, session);
                return ABORTED;
            }

            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                String accountNumber = hasil.getString("accountNumber");
                nasabah.setAccountNumber(accountNumber);
                log.info("NASABAH VALUE STATUS : " + Constants.Status.UNDEF);
                nasabah.setStatus(Constants.Status.UNDEF);
                nasabah.setNasabahType(agent.getAgentType());
                log.info("NASABAH STATUS: " + nasabah.getStatus());
                nasabah.setCabang(cabang);
                MNasabah savedNasabah = nasabahRepository.save(nasabah);
                nasabah = nasabahRepository.save(nasabah);
                JSONObject resp = new JSONObject().put("rm", Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_REGISTRATION_SUCCESS.name()));

                session.setRcToClient(rc);
                session.setResponseToClient(responseMessage);

                log.info("RESP : " + resp.toString());
                log.info("Response to Client : " + responseMessage);
                log.info("RC : " + rc);

                if (rc.equals("16")) {
                    trx.setInfo(resp.toString());
                    ctx.put(Constants.OBJ.RESP, resp);
                    ctx.put(Constants.NASABAH, nasabah);
                    ctx.put(Constants.SESSION, session);
                    ctx.put(Constants.TXLOG, trx);
                    return ABORTED;
                } else {
                    trx.setInfo(resp.toString());
                    ctx.put(Constants.OBJ.RESP, resp);
                    ctx.put(Constants.NASABAH, nasabah);
                    ctx.put(Constants.SESSION, session);
                    ctx.put(Constants.TXLOG, trx);
                    return PREPARED;
                }

            } else {
                ctx.put(Constants.NASABAH, nasabah);
                ctx.put(Constants.SESSION, session);
                return ABORTED;
            }
        } else {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }
}
