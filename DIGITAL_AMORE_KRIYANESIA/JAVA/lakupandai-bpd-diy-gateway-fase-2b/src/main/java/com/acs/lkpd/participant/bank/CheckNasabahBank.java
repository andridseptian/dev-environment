package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CheckNasabahBank implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private RcRepository rcRepository;

    public CheckNasabahBank() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        JSONObject hasil = new JSONObject();
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        hasil = Service.serviceBalanceInquiry(agent.getId(), nasabah.getAccountNumber());
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            }
            ctx.log("Check " + ctx.get(Constants.GROUP) + " " + rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_CHECK_BALANCE, rc);
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rc + " | " + rm);
                String mapRcInfo = "Map RC not found, set transaction to failed [" + rc + "]";
                session.setDetail(session.getDetail() + ", " + mapRcInfo);
                ctx.log(mapRcInfo);
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            } else {
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }

}
