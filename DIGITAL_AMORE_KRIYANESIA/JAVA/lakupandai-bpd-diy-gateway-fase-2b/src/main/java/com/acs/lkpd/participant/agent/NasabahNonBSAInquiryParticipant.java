/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.agent;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class NasabahNonBSAInquiryParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final NasabahRepository nasabahRepository;

    public NasabahNonBSAInquiryParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        nasabahRepository = context.getBean(NasabahRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.log("--Nasabah Non BSA Inquiry Participant--");
        String[] request = (String[]) ctx.get(Constants.REQ);
        int msisdnReq = cfg.getInt("field_no_rek", 0);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String formatMsisdn = Utility.formatMsisdn(request[msisdnReq]);
        MNasabah nasabah = nasabahRepository.findFirstByMsisdnOrderByCrTimeDesc(formatMsisdn);
        ctx.log("nasabah getStatus : " + nasabah.getStatus());
        if (nasabah != null) {
            switch (nasabah.getStatus()) {
                case ACTIVE: {
                    break;
                }
                case BLOCK: {
                    session.setRcToClient(Constants.RC_ACCOUNT_NASABAH_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_BLOCK.name()));
                    ctx.log("getResponseToClient : " + session.getResponseToClient());
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case REJECTED: {
                    session.setRcToClient(Constants.RC_ACCOUNT_NASABAH_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_REJECTED.name()));
                    ctx.log("getResponseToClient : " + session.getResponseToClient());
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case INACTIVE: {
                    session.setRcToClient(Constants.RC_ACCOUNT_NASABAH_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_INACTIVE.name()));
                    ctx.log("getResponseToClient : " + session.getResponseToClient());
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
                case NEED_APPROVAL:
                case NEED_APPROVAL_CS:
                    session.setRcToClient(Constants.RC_ACCOUNT_NASABAH_NOTFOUND);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_NEED_APPROVAL.name()));
                    ctx.log("getResponseToClient : " + session.getResponseToClient());
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
            }
            ctx.put(Constants.NASABAH, nasabah);
            session.setNasabah(nasabah);
            ctx.put(Constants.SESSION, session);

            ctx.log("--Nasabah Inquiry Participant--");
            ctx.log("Nasabah Name: " + session.getNasabah().getName());
            ctx.log("Account Number: " + session.getNasabah().getAccountNumber());
            ctx.log("MSISDN: " + session.getNasabah().getMsisdn());
            ctx.log("Nasabah Type: " + session.getNasabah().getNasabahType());

            return PREPARED | NO_JOIN;
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_CUSTOMER_NOT_FOUND.name()));
            ctx.log("getResponseToClient : " + session.getResponseToClient());
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
