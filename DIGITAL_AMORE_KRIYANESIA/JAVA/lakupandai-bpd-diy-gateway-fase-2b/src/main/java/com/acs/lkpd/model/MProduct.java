/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

/**
 *
 * @author ACS
 */
@NamedNativeQuery(name = "findDataPembelian", query = "SELECT p.menu, p.product_code, p.product_name, denom from m_product p join (select product, string_agg(cast(denom as text), ',') as denom from m_denominasi where status = 1 group by product) d ON p.product_code = d.product group by p.menu,p.product_code,d.denom order by p.product_code,d.denom")

@Entity
@Table(name = "m_product")
public class MProduct implements Serializable {

    private String productCode;
    private String productName;
    private double feeBank;
    private double feeAgent;
    private double feePajak;
    private String formatInquiry;
    private String formatPayment;
    private String formatAdditionalInformation;
    private String formatSMSToken;
    private String formatSMSPayment;
    private Constants.ProductStatus status;
    private Constants.ProductType type;
    private String menu;

    @Id
    @Column(name = "product_code")
    public String getProductCode() {
        return productCode;
    }

    @Column(name = "product_name")
    public String getProductName() {
        return productName;
    }

    @Column(name = "fee_bank")
    public double getFeeBank() {
        return feeBank;
    }

    @Column(name = "fee_agent")
    public double getFeeAgent() {
        return feeAgent;
    }

    @Column(name = "fee_pajak")
    public double getFeePajak() {
        return feePajak;
    }

    @Column(name = "format_inquiry", length = 500)
    public String getFormatInquiry() {
        return formatInquiry;
    }

    @Column(name = "format_payment", length = 1000)
    public String getFormatPayment() {
        return formatPayment;
    }

    @Column(name = "format_additional_information", length = 500)
    public String getFormatAdditionalInformation() {
        return formatAdditionalInformation;
    }

    @Column(name = "format_sms_token", length = 160)
    public String getFormatSMSToken() {
        return formatSMSToken;
    }

    @Column(name = "format_sms_payment", length = 160)
    public String getFormatSMSPayment() {
        return formatSMSPayment;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.ProductStatus getStatus() {
        return status;
    }

    @Column(name = "menu")
    public String getMenu() {
        return menu;
    }

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public Constants.ProductType getType() {
        return type;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setFeeBank(double feeBank) {
        this.feeBank = feeBank;
    }

    public void setFeeAgent(double feeAgent) {
        this.feeAgent = feeAgent;
    }

    public void setFeePajak(double feePajak) {
        this.feePajak = feePajak;
    }

    public void setStatus(Constants.ProductStatus status) {
        this.status = status;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setType(Constants.ProductType type) {
        this.type = type;
    }

    public void setFormatInquiry(String formatInquiry) {
        this.formatInquiry = formatInquiry;
    }

    public void setFormatPayment(String formatPayment) {
        this.formatPayment = formatPayment;
    }

    public void setFormatSMSToken(String formatSMSToken) {
        this.formatSMSToken = formatSMSToken;
    }

    public void setFormatSMSPayment(String formatSMSPayment) {
        this.formatSMSPayment = formatSMSPayment;
    }

    public void setFormatAdditionalInformation(String formatAdditionalInformation) {
        this.formatAdditionalInformation = formatAdditionalInformation;
    }

}
