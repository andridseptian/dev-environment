/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatPostingSMSTarikTunai implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_TARIK_TUNAI.name());
        try {
            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[Name]", session.getNasabah().getName().length()<=10?session.getNasabah().getName():session.getNasabah().getName().substring(0, 10))
                    .replace("[Status]", trx.viewStatus())
//                    .replace("[Status]", ctx.getString(Constants.SMS_RESPONSE.STATUS))
//                    .replace("[Status]", "TESTING GAGAL")
                    .replace("[Ref]", trx.getOriginReff());
            log.info(ctx.getString(Constants.SMS_RESPONSE.STATUS));
            if (sendSMS) {
//                boolean statusSms = Utility.sendSMS(response, agent.getMsisdn());
//                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSms);
                JSONObject hasil = Service.serviceBalanceInquiry(agent.getId(), session.getNasabah().getAccountNumber());
                double saldoNasabah = 0;
                if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
                    String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
                    if (rc.equals(Constants.RC_APPROVED)) {
                        saldoNasabah = Utility.getParseAmount(hasil.getString("balance"),100);
                    } else {
                        saldoNasabah = -1;
                    }
                    ctx.log("balance:"+saldoNasabah);
                } else {
                    saldoNasabah = -1;
                }
                if (saldoNasabah != -1) {
                    try {
                        response = response + Service.getResponseFromSettings(Constants.RM_FORMAT_SMS_ADD_SALDO)
                                .replace("[Balance]", Utility.formatAmount(saldoNasabah));
                        log.info("response:" + response);
                    } catch (Exception ex) {
                        log.error(ex.getMessage());
                    }
                }
                boolean statusSMS = Utility.sendSMS(response, session.getNasabah().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSMS);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
        } catch (Exception ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Constants.RM_INVALID_REQUEST);
        }
        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String response = Service.getResponseFromSettings(Constants.RM_FORMAT_SMS_POSTING_TARIK_TUNAI);
        try {
            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[Name]", session.getNasabah().getName())
//                    .replace("[Status]", trx.viewStatus())
                    .replace("[Status]", ctx.getString(Constants.SMS_RESPONSE.STATUS))
                    .replace("[Ref]", trx.getOriginReff());
            if (sendSMS) {
//                boolean statusSms = Utility.sendSMS(response, agent.getMsisdn());
//                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSms);
                JSONObject hasil = Service.serviceBalanceInquiry(agent.getId(), session.getNasabah().getAccountNumber());
                double saldoNasabah = 0;
                if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
                    String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
                    if (rc.equals(Constants.RC_APPROVED)) {
                        saldoNasabah = Utility.getParseAmount(hasil.getString("balance"),100);
                    } else {
                        saldoNasabah = -1;
                    }
                } else {
                    saldoNasabah = -1;
                }
                if (saldoNasabah != -1) {
                    try {
                        response = response + Service.getResponseFromSettings(Constants.RM_FORMAT_SMS_ADD_SALDO)
                                .replace("[Balance]", Utility.formatAmount(saldoNasabah));
                        log.info("response:" + response);
                    } catch (Exception ex) {
                        log.error(ex.getMessage());
                    }
                }
                boolean statusSMS = Utility.sendSMS(response, session.getNasabah().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo()+ "SMS " + trx.getDescription() + " = " + statusSMS);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
        } catch (Exception ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Constants.RM_INVALID_REQUEST);
        }
        ctx.put(Constants.TXLOG, trx);
        ctx.put(Constants.SESSION, session);
    }

}
