/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MProduct;
import com.acs.util.Service;
import org.jpos.util.Log;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatInquiryTokenTransfer implements TransactionParticipant {

    private Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String response = Service.getResponseFromSettings(KeyMap.RM_INQUIRY_TOKEN_TRANSFER_ON_US.name());
        String[] request = (String[]) ctx.get(Constants.REQ);
        try {
            MOtp otp = Service.findOTP(request[0]);

            String otpProduct = otp.getProduct().toString();
            otpProduct = otpProduct.substring(0, 3);

            if (!otpProduct.equals("411")) {
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }

            MSession tempSession = Service.findSession(otp.getOtp(), session.getAgent());
            session.setNasabah(tempSession.getNasabah());
            session.setTargetNasabahName(tempSession.getTargetNasabahName());
            session.setTargetNasabahAccountNumber(tempSession.getTargetNasabahAccountNumber());
            session.setAmount(tempSession.getAmount());
            ctx.put(Constants.SESSION, session);

//            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
//                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
//                    .replace("[TargetAccount]", session.getTargetNasabahAccountNumber())
//                    .replace("[TargetName]", session.getTargetNasabahName());
            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[DestinationAccount]", session.getTargetNasabahAccountNumber())
                            .replace("[DestinationName]", session.getTargetNasabahName())
                            .replace("[Amount]", "Rp." + Utility.formatAmount(session.getAmount()))
                            .replace("[FromMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                            .replace("[FromName]", session.getNasabah().getName())
                            .replace("[Desc]", Service.getResponseFromSettings(Constants.RM_MESSAGE_PIN)));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";

            }

//            response = response + Service.getResponseFromSettings(Constants.RM_MESSAGE_PIN);
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (IndexOutOfBoundsException | NumberFormatException | NullPointerException ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
    }

}
