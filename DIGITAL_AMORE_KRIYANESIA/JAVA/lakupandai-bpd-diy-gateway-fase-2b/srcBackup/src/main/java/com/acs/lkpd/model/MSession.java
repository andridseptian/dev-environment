/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "m_session")
public class MSession implements Serializable {

    private Long id;
    private String originReff;
    private String stan;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private MAgent agent;
    private MNasabah nasabah;
    private MProduct product;
    private String otp;
    private String rcToClient;
    private String requestClient;
    private String responseToClient;
    private String detail;
    private String terminal;
    private String origin;
    private double amount;
    private String targetNasabahName;
    private String targetNasabahAccountNumber;
    private String targetNasabahMsisdn;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MAgent.class)
    @JoinColumn(name = "agent")
    public MAgent getAgent() {
        return agent;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MNasabah.class)
    @JoinColumn(name = "nasabah")
    public MNasabah getNasabah() {
        return nasabah;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MProduct.class)
    @JoinColumn(name = "product")
    public MProduct getProduct() {
        return product;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "session_seq")
    @SequenceGenerator(name = "session_seq", sequenceName = "session_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "origin_reff", nullable = true, length = 20)
    public String getOriginReff() {
        return originReff;
    }

    @Column(name = "stan", nullable = false, length = 20)
    public String getStan() {
        return stan;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "otp")
    public String getOtp() {
        return otp;
    }

    @Column(name = "rc_to_Client")
    public String getRcToClient() {
        return rcToClient;
    }

    @Column(name = "request_client")
    public String getRequestClient() {
        return requestClient;
    }

    @Column(name = "response_to_client")
    public String getResponseToClient() {
        return responseToClient;
    }

    @Column(name = "detail")
    public String getDetail() {
        return detail;
    }

    @Column(name = "terminal", nullable = true, length = 20)
    public String getTerminal() {
        return terminal;
    }

    @Column(name = "origin")
    public String getOrigin() {
        return origin;
    }

    @Column(name = "target_nasabah_name", length = 100)
    public String getTargetNasabahName() {
        return targetNasabahName;
    }

    @Column(name = "target_nasabah_account_number", length = 20)
    public String getTargetNasabahAccountNumber() {
        return targetNasabahAccountNumber;
    }

    @Column(name = "target_nasabah_msisdn", length = 20)
    public String getTargetNasabahMsisdn() {
        return targetNasabahMsisdn;
    }

    @Column(name = "amount")
    public double getAmount() {
        return amount;
    }

    //===============SET================//
    public void setId(Long id) {
        this.id = id;
    }

    public void setOriginReff(String originReff) {
        this.originReff = originReff;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setAgent(MAgent agent) {
        this.agent = agent;
    }

    public void setNasabah(MNasabah nasabah) {
        this.nasabah = nasabah;
    }

    public void setProduct(MProduct product) {
        this.product = product;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setRcToClient(String rcToClient) {
        this.rcToClient = rcToClient;
    }

    public void setRequestClient(String requestClient) {
        this.requestClient = requestClient;
    }

    public void setResponseToClient(String responseToClient) {
        this.responseToClient = responseToClient;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setTargetNasabahName(String targetNasabahName) {
        this.targetNasabahName = targetNasabahName;
    }

    public void setTargetNasabahAccountNumber(String targetNasabahAccountNumber) {
        this.targetNasabahAccountNumber = targetNasabahAccountNumber;
    }

    public void setTargetNasabahMsisdn(String targetNasabahMsisdn) {
        this.targetNasabahMsisdn = targetNasabahMsisdn;
    }

}
