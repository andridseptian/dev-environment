package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_notification_history")
public class MNotificationHistory implements Serializable {

    private Long id;
    private LocalDateTime crTime;
    private String rrn;
    private String title;
    private String message;
    private String image;
    private String agent;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notifhistory_generator")
    @SequenceGenerator(name = "notifhistory_generator", sequenceName = "notifhistory_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "cr_time", insertable = false, columnDefinition = "timestamp default now()::timestamp")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "rrn", length = 15)
    public String getRrn() {
        return rrn;
    }

    @Column(name = "title", length = 50)
    public String getTitle() {
        return title;
    }

    @Column(name = "message", length = 500)
    public String getMessage() {
        return message;
    }

    @Column(name = "image")
    @Type(type = "text")
    public String getImage() {
        return image;
    }

    @Column(name = "agent", nullable = true)
    public String getAgent() {
        return agent;
    }

    //=======SET=========//
    public void setId(Long id) {
        this.id = id;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

}
