/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_tax_agent")
public class MAgentTax implements Serializable {

    private String accountType;
    private String description;
    private String jenis;
    private double taxNpwp;
    private double taxNonNpwp;
    private double addtionalTax;

    @Id
    @Column(name = "account_type", length = 1)
    public String getAccountType() {
        return accountType;
    }

    @Column(name = "description", length = 20)
    public String getDescription() {
        return description;
    }

    @Column(name = "tax_npwp", precision = 10, scale = 3)
    public double getTaxNpwp() {
        return taxNpwp;
    }

    @Column(name = "tax_non_npwp", precision = 10, scale = 3)
    public double getTaxNonNpwp() {
        return taxNonNpwp;
    }

    @Column(name = "jenis")
    public String getJenis() {
        return jenis;
    }
    
    @Column(name = "addtional_tax")
    public Double getAddtionalTax() {
        return addtionalTax;
    }

    //SET
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTaxNpwp(double taxNpwp) {
        this.taxNpwp = taxNpwp;
    }

    public void setTaxNonNpwp(double taxNonNpwp) {
        this.taxNonNpwp = taxNonNpwp;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public void setAddtionalTax(double addtionalTax) {
        this.addtionalTax = addtionalTax;
    }
    
    

}
