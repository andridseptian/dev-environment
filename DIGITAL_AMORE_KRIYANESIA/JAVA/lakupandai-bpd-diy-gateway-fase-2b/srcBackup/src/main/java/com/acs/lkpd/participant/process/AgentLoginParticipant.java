package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class AgentLoginParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        String[] request = (String[]) ctx.get(Constants.REQ);
//        //check agent active / tidak
//        if (Constants.Status.ACTIVE != agent.getStatus()) {
//            session.setRcToClient(Constants.RC_ACCOUNT_AGEN_NOTFOUND);
//            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_NOT_FOUND.name()));
//            ctx.put(Constants.OBJ.SESSION, session);
//        }
        //check status pin dan imei, jika salah satu kosong agent saat login harus reset password
        if (agent.isStatusPin() || agent.getImei() == null || agent.getImei().equalsIgnoreCase("")) {
            session.setAgent(agent);
            session.setRcToClient(Constants.RC_RESET_PASSWORD);
            session.setResponseToClient("Reset password");
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        } else if (agent.getImei() != null && !agent.getImei().equalsIgnoreCase("")) {
            int fieldImei = cfg.getInt("field_imei", 0);
            String imei = request[fieldImei];
            if (imei.equals(agent.getImei())) {
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            } else {
                session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_UNKNOWN_DEVICE.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        } else {
            session.setRcToClient(Constants.RC_ACCOUNT_AGEN_NOTFOUND);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_AGENT_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
