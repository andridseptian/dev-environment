/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CheckBalanceProcess implements TransactionParticipant, Configurable {

    Log log = Log.getLog("Q2", getClass().getName());

    protected Configuration cfg;
    private RcRepository rcRepository;

    public CheckBalanceProcess() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String targetSMS = cfg.get("target", "nasabah");
        String productNameFormat = cfg.get("productNameFormat", "[ProductName]");
        String tanggalFormat = cfg.get("tanggalFormat", "[Date]");
        String msisdnFormat = cfg.get("msisdnFormat", "[Msisdn]");
        String amountFormat = cfg.get("amountFormat", "[Amount]");
        String userNameFormat = cfg.get("userNameFormat", "[UserName]");
        String userIdFormat = cfg.get("userIdFormat", "[UserId]");
        String userAccountFormat = cfg.get("userAccountFormat", "[UserAccount]");
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String bankResponse = cfg.get("resp", Constants.OBJ.RESP);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        String nomorMsisdn = "";
        String userName = "";
        String userId = "";
        String userAccount = "";

        JSONObject hasil = new JSONObject();
        switch (targetSMS) {
            case "nasabah":
                MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
                nomorMsisdn = nasabah.getMsisdn();
                userName = nasabah.getName();
                userId = nasabah.getId().toString();
                userAccount = nasabah.getAccountNumber();
//                hasil = Service.serviceBalanceInquiry(agent.getId(), nasabah.getAccountNumber());
                hasil = Service.serviceBalanceInquiry(agent.getId(), nasabah.getMsisdn());
                break;
            case "agent":
                nomorMsisdn = agent.getAccountNumber();
                userName = agent.getName();
                userId = agent.getId();
                userAccount = agent.getAccountNumber();
                hasil = Service.serviceBalanceInquiry(agent.getId(), nomorMsisdn);
                break;
        }
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            }
            ctx.log("Check " + ctx.get(Constants.GROUP) + " " + rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_CHECK_BALANCE, rc);
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rc + " | " + rm);
                if (sendSMS) {
                    session.setRcToClient(rc);
                    session.setResponseToClient(rm);
                    boolean statusSMS = Utility.sendSMS(rm, Utility.viewMsisdn(nomorMsisdn));
                    if (statusSMS) {
                        String info = "SMS Success [" + nomorMsisdn + "]";
                        ctx.log(info);
                        session.setDetail(info);
                    } else {
                        String info = "SMS Failed [" + nomorMsisdn + "]";
                        ctx.log(info);
                        session.setDetail(info);
                    }
                }
                String mapRcInfo = "Map RC not found, set transaction to failed [" + rc + "]";
                session.setDetail(session.getDetail() + ", " + mapRcInfo);
                ctx.log(mapRcInfo);
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                String bankFormatBalance = hasil.getString("balance");
                double balance = Utility.getParseAmount(bankFormatBalance, 100);
                session.setRcToClient(Constants.RC_APPROVED);
                String descResponse = "";
//                String descResponse = Service.getResponseFromSettings(Constants.RM_INFORMASI_SALDO);
//                descResponse = descResponse
//                        .replace(tanggalFormat, LocalDateTime.now().format(Constants.FORMAT_DDMMYYYY))
//                        .replace("[NoMSISDN]", Utility.viewMsisdn(nomorMsisdn))
//                        .replace(amountFormat, Utility.formatAmount(balance));

                JSONArray formatInquiryArray = null;

                if (targetSMS.equals("agent")) {
                    try {
                        formatInquiryArray = new JSONArray(product.getFormatInquiry());
                        for (int i = 0; i < formatInquiryArray.length(); i++) {
                            JSONObject data = formatInquiryArray.getJSONObject(i);
                            data.put("value", data.getString("value")
                                    .replace(productNameFormat, product.getProductCode())
                                    .replace(userIdFormat, userId)
                                    .replace(userNameFormat, userName)
                                    .replace(userAccountFormat, userAccount)
                                    .replace(msisdnFormat, Utility.viewMsisdn(nomorMsisdn))
                                    .replace(amountFormat, Utility.formatAmount(balance))
                                    .replace(tanggalFormat, LocalDateTime.now().format(Constants.FORMAT_DDMMYYYY))
                            );
                        }
                        descResponse = new JSONObject().put("data", formatInquiryArray).toString();
                    } catch (JSONException | NullPointerException ex) {
                        log.info("ERROR " + ex.toString());
                        ex.printStackTrace();
                        descResponse = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";
                    }

                    session.setResponseToClient(descResponse);
                }

                if (sendSMS) {
                    descResponse = Service.getResponseFromSettings(Constants.RM_INFORMASI_SALDO);
                    descResponse = descResponse
                            .replace(tanggalFormat, LocalDateTime.now().format(Constants.FORMAT_DDMMYYYY))
                            .replace("[NoMSISDN]", Utility.viewMsisdn(nomorMsisdn))
                            .replace(amountFormat, Utility.formatAmount(balance));
                    boolean statusSMS = Utility.sendSMS(descResponse, Utility.viewMsisdn(nomorMsisdn));
                    if (statusSMS) {
                        ctx.log("send message success : " + nomorMsisdn);
                        String info = "send SMS success";
                        ctx.log(info);
                        session.setDetail(info);
                        ctx.put(Constants.SESSION, session);
                        session.setResponseToClient(descResponse);
                    } else {
                        ctx.log("Status pesan tidak dapat terkirim, ulangi beberapa saat lagi");
                        return ABORTED | NO_JOIN;
                    }
                } else {
                    descResponse = Service.getResponseFromSettings(Constants.RM_INFORMASI_SALDO);
                    descResponse = descResponse
                            .replace(tanggalFormat, LocalDateTime.now().format(Constants.FORMAT_DDMMYYYY))
                            .replace("[NoMSISDN]", Utility.viewMsisdn(nomorMsisdn))
                            .replace(amountFormat, Utility.formatAmount(balance));
//                    session.setResponseToClient(descResponse);
                }
                ctx.put(bankResponse, hasil.toString());
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            } else {
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }

}
