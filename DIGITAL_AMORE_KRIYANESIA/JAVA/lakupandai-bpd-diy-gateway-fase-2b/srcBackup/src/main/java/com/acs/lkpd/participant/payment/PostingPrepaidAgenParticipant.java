/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MAgentTax;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentTaxRepository;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.FormatTanggal;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author risyamaulana
 */
public class PostingPrepaidAgenParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private Log log = Log.getLog("Q2", getClass().getName());
    private RcRepository rcRepository;
    private ProductRepository paymentRepository;
    private NotificationTrxRepository notTransactionRepository;
    private AgentTaxRepository agentTaxRepository;
    private NotificationTrxRepository notificationTrxRepository;
    private TransactionRepository transactionRepository;

    public PostingPrepaidAgenParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        paymentRepository = context.getBean(ProductRepository.class);
        rcRepository = context.getBean(RcRepository.class);
        notTransactionRepository = context.getBean(NotificationTrxRepository.class);
        agentTaxRepository = context.getBean(AgentTaxRepository.class);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.log("--Posting Prepaid Agent Participant--");
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
//        String productCode = cfg.get("product", "-");
        String debet = cfg.get("debet", "-");
        int idbilling = cfg.getInt("idbilling", 0);
        int denom = cfg.getInt("denom");
        String accountType = "A";
        String sourceAccountNumber = "";
        String sourceSMSNumber = "";
        if ("-".equals(session.getProduct().getProductCode())) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Invalid Product Code");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        MProduct productPayment = session.getProduct();
        if (null == productPayment) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Product Code [" + session.getProduct().getProductCode() + "] not found");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        if ("-".equals(debet)) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setDetail("Invalid Product Code");
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        //init Transaction
        trx.setAccountType(debet);
        trx.setProductCode(productPayment.getProductCode());
        switch (debet) {
            case "agent": //agent dibayar cash
                accountType = "A";
                sourceAccountNumber = agent.getAccountNumber();
                sourceSMSNumber = agent.getMsisdn();
                break;
            case "customer": //customer menggunakan akun nasabah
                accountType = "C";
                sourceAccountNumber = nasabah.getAccountNumber();
                sourceSMSNumber = nasabah.getMsisdn();
                break;
            default:
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setDetail("Invalid Source Account Number");
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED | NO_JOIN;
        }

        double amount = Utility.getParseAmount(request[denom], 1);

        String productCode = productPayment.getProductCode();
        productCode = productCode.replaceFirst("5", "0");

        Constants.AgentAccountType acType = agent.getAccountType();

        String acString = acType.toString();

        String NPWP = agent.getNpwp();
        MAgentTax mtax = agentTaxRepository.findByAccountType(acString);

        double feePajak = 0.0;

        if (NPWP.equals("0000000")) {
            feePajak = productPayment.getFeeAgent() * mtax.getTaxNonNpwp() * mtax.getAddtionalTax();
        } else {
            feePajak = productPayment.getFeeAgent() * mtax.getTaxNpwp() * mtax.getAddtionalTax();
        }

        feePajak = Math.round(feePajak);

        //init inquiry
        JSONObject respHasil = Service.servicePaymentString(
                agent.getId(),
                agent.getPin(),
                sourceAccountNumber,
                productCode,
                request[idbilling],
                amount,
                productPayment.getFeeBank(),
                productPayment.getFeeAgent(),
                feePajak,
                "",
                Constants.TRANSACTION.TYPE.TOP_UP,
                "",
                accountType,
                trx.getStan());
        if (respHasil == null) {
            return ABORTED | NO_JOIN;
        }

        ctx.log("Bank Resp : " + respHasil.toString());
        if (respHasil.has(Constants.PARAM.WS_RESPONSECODE)
                && respHasil.has(Constants.PARAM.WS_RESPONSEDESC)) {

            /* Set data from bank response */
            String rc = respHasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = respHasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            String jobId = respHasil.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY);
            double admin = Double.valueOf(respHasil.getString(Constants.PARAM.WS_PAY_FEE));
            String rrn = "-";
            rrn = respHasil.has("rrn") ? respHasil.getString(Constants.PARAM.WS_PAY_RRN) : "-";
//            if (rc.equals(Constants.RC_APPROVED)) {
//                rrn = respHasil.getString(Constants.PARAM.WS_PAY_RRN);
//            } else {
//                rrn = respHasil.getString(Constants.PARAM.WS_PAY_RRN);
//            }
            SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
            double totalPayment = amount + admin;

            /* Save data transaction to trx logger */
            ctx.log("SAVE TRANSACTION LOGGER");
            trx.setBankResponseDetail(respHasil.toString());
            trx.setBankReff(respHasil.getString("rrn"));
            trx.setDescription(productPayment.getProductName());
            trx.setType(Constants.TRX_TYPE.POSTING);
            trx.setBankResponseDetail(respHasil.toString());
            trx.setBankReff(respHasil.getString("rrn"));
            trx.setAmount(amount);
            trx.setFeeBank(feePajak + productPayment.getFeeBank());
            trx.setTotalAmount(totalPayment);
            trx.setAdditionalData(respHasil.getString("additionalData"));
            trx.setIdBilling(respHasil.getString("idBilling"));
            trx = transactionRepository.save(trx);

            /* Save notification data */
            ctx.log("SAVE NOTIFICATION");
            MNotifikasiTrx notif = new MNotifikasiTrx();
            notif.setCrTime(trx.getCrTime());
            notif.setOriginReff(trx.getOriginReff());
            notif.setStan(trx.getStan());
            notif.setType(Constants.TRX_TYPE.POSTING);
            notif.setDescription(productPayment.getProductName());
            notif.setStatus(Constants.TRX_STATUS.PENDING);
            notif.setNasabah(trx.getNasabah());
            notif.setAgent(trx.getAgent());
            notif.setAmount(trx.getAmount());
            notif.setFeeAgent(trx.getFeeAgent());
            notif.setFeeBank(trx.getFeeBank());
            notif.setTotalAmount(trx.getTotalAmount());
            notif.setProductCode(trx.getProductCode());
            notif.setOtp(session.getOtp());
            notif.setAccountType(trx.getAccountType());
            notif.setIdBilling(trx.getIdBilling());
            notif.setAdditionalData(trx.getAdditionalData());
            notif.setBankResponseDetail(respHasil.toString());
            notif.setIsRead(Boolean.FALSE);
            notif.setBankReff(respHasil.getString("rrn"));
            notif = notTransactionRepository.save(notif);

            /* Get mapped rc from database */
            MRc mapRc = rcRepository.findByProcessCodeAndRc(productPayment.getProductCode(), rc);
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rm);
            } else {
                session.setRcToClient(mapRc.getRc());
            }

            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.NOTIFTRX, notif);

            String paymentResponse = "";
            String smsResponse = "";

//            JSONArray formatInquiryArray = null;
//            String statusMessage = "Gagal";
//            if (rc.equals("00")) {
//                statusMessage = "Sukses";
//            }
////
//            try {
//                log.info("FORMAT PAYMENT : " + productPayment.getFormatPayment());
//                log.info("FORMAT ADDITIONAL DATA : " + productPayment.getFormatAdditionalInformation());
//                log.info("FORMAT SMS COY : " + productPayment.getFormatSMSPayment());
////                SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("DD MMMMM YYYY HH:mm");
////                double totalPayment = amount + admin;
//
////                String formatPayment = productPayment.getFormatPayment();
////                paymentResponse = formatPayment
////                        .replace("[ProductName]", productPayment.getProductName())
////                        .replace("[Resi]", respHasil.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY))
////                        .replace("[Waktu]", sdfFormatPayment.format(new Date()))
////                        .replace("[DestinationMsisdn]", respHasil.getString(Constants.PARAM.WS_PAY_IDBILLING))
////                        .replace("[Amount]", Utility.formatAmount(amount))
////                        .replace("[Admin]", Utility.formatAmount(admin))
////                        .replace("[Total]", Utility.formatAmount(totalPayment))
////                        .replace("[VSN]", jobId)
////                        .replace("[Status]", statusMessage);
////                log.info(formatPayment);
//            } catch (JSONException | NullPointerException ex) {
//                log.info(ExceptionUtils.getStackTrace(ex));
//            }
            try {
                ctx.log("Product: " + productPayment);
                JSONObject additionalData = new JSONObject(respHasil.has("additionalData") ? !respHasil.getString("additionalData").equals("") ? respHasil.getString("additionalData") : "{}" : "{}");
                log.info("additional Data: " + additionalData);
                String formatPayment = productPayment.getFormatPayment();
                paymentResponse = formatPayment
                        .replace("[ProductName]", productPayment.getProductName())
                        .replace("[Resi]", rrn)
                        .replace("[Waktu]", FormatTanggal.tanggalLokal(sdfFormatPayment.format(new Date())))
                        .replace("[DestinationMsisdn]", respHasil.getString(Constants.PARAM.WS_PAY_IDBILLING))
                        .replace("[Amount]", Utility.formatAmount(amount))
                        .replace("[Admin]", Utility.formatAmount(admin))
                        .replace("[Total]", Utility.formatAmount(totalPayment))
                        .replace("[VoucherSN]", additionalData.has("voucherSN") ? additionalData.getString("voucherSN") : "-")
                        .replace("[Status]", rm);

                trx.setMobileResponse(paymentResponse);
                trx = transactionRepository.save(trx);
                notif.setMobileResponse(paymentResponse);
                notif = notTransactionRepository.save(notif);

                SimpleDateFormat sdfFormatSMS = new SimpleDateFormat("dd/MM/YY HH:mm");

                String formatSMS = productPayment.getFormatSMSPayment();

                smsResponse = formatSMS
                        .replace("[ProductName]", productPayment.getProductName())
                        .replace("[DateTime]", sdfFormatSMS.format(new Date()))
                        .replace("[Resi]", rrn)
                        .replace("[DestinationMsisdn]", respHasil.getString(Constants.PARAM.WS_PAY_IDBILLING))
                        .replace("[Amount]", Utility.formatAmount(amount))
                        .replace("[Admin]", Utility.formatAmount(admin))
                        .replace("[VoucherSN]", additionalData.has("voucherSN") ? additionalData.getString("voucherSN") : "-")
                        .replace("[Status]", rm);

//                
//                smsResponse = formatSMS
//                        .replace("[ProductName]", productPayment.getProductName())
//                        .replace("[DestinationMsisdn]", respHasil.getString(Constants.PARAM.WS_PAY_IDBILLING))
//                        .replace("[Amount]", Utility.formatAmount(amount))
//                        .replace("[Ref]", respHasil.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY))
//                        .replace("[Status]", statusMessage)
//                        .replace("[Detail]", rm);
            } catch (JSONException | NullPointerException ex) {
                ex.printStackTrace();
                log.error(ex);
                smsResponse = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";
                paymentResponse = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";
            }
//            }

            if (rc.equals(Constants.RC_APPROVED)) {
                session.setRcToClient(rc);
//                session.setRcToClient("13");
                session.setResponseToClient(paymentResponse);
                log.info("Payment Response " + paymentResponse);
                notif.setMobileResponse(paymentResponse);
                notif = notificationTrxRepository.save(notif);

                ctx.put("DENOM_PULSA", amount);
                ctx.put(Constants.OBJ.BANKRESP, respHasil);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.NOTIFTRX, notif);
                ctx.put(Constants.OBJ.PRODUCTPAYMENT, productPayment);

                if (sendSMS) {
                    boolean statusSMS = Utility.sendSMS(smsResponse, Utility.viewMsisdn(sourceSMSNumber));
                    if (statusSMS) {
                        ctx.log("send message success : " + sourceSMSNumber);
                        String info = "send SMS success";
                        ctx.log(info);
                        session.setDetail(info);
                    } else {
                        ctx.log("Status pesan tidak dapat terkirim, ulangi beberapa saat lagi");
                    }
                }

                return PREPARED;
            } else {
//                session.setResponseToClient(paymentResponse);
                if (sendSMS) {
                    boolean statusSMS = Utility.sendSMS(smsResponse, Utility.viewMsisdn(sourceSMSNumber));
                    if (statusSMS) {
                        ctx.log("send message success : " + sourceSMSNumber);
                        String info = "send SMS success";
                        ctx.log(info);
                        session.setDetail(info);
                    } else {
                        ctx.log("Status pesan tidak dapat terkirim, ulangi beberapa saat lagi");
                    }
                }

                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.NOTIFTRX, notif);
                ctx.put(Constants.OBJ.BANKRESP, respHasil);
                ctx.put(Constants.OBJ.PRODUCTPAYMENT, productPayment);
                return ABORTED;
            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);

        MNotifikasiTrx NotifTrx = (MNotifikasiTrx) ctx.get(Constants.NOTIFTRX);
        NotifTrx.setStatus(Constants.TRX_STATUS.SUCCESS);
        NotifTrx = notificationTrxRepository.save(NotifTrx);

        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);

        MNotifikasiTrx NotifTrx = (MNotifikasiTrx) ctx.get(Constants.NOTIFTRX);
        NotifTrx.setStatus(Constants.TRX_STATUS.FAILED);
        NotifTrx = notificationTrxRepository.save(NotifTrx);

        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
