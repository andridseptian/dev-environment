package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MServiceProxy;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentRegistrationProcess implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private final AgentRepository agentRepository;
    private final RcRepository rcRepository;

    Log log = Log.getLog("Q2", getClass().getName());

    public AgentRegistrationProcess() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MServiceProxy proxy = (MServiceProxy) ctx.get(Constants.OBJ.SERVICEPROXY);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        JSONObject request = new JSONObject(req.getRequest());
        String iId = request.getString(Constants.PARAM.ID);
        String info = "-";
        if (request.has(Constants.PARAM.INFO)) {
            info = request.getString(Constants.PARAM.INFO);
            agent.setInfo(info);
        }
        boolean status = request.getBoolean(Constants.PARAM.STATUS);
        Service.saveSessionWeb(iId, "Approval Agent " + agent.getId() + ",status:" + status + ",info:" + info);

        String logs = ""
                + "\nSTATUS " + status
                + "\nAGEN " + agent.getId()
                + "\nINFO " + info;

        log.info("==> LOG : " + logs);
        if (status) {
            JSONObject hasil = Service.serviceAgentRegistration(agent.getAccountNumber(), agent.getMsisdn(), agent.getId(), agent.getPin(), agent.getAgentType());
            if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {

                String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
                String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";

                if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                    rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
                }
                MRc mapRc = rcRepository.findByProcessCodeAndRc(proxy.getProcode(), rc);
                log.info("RESP: " + hasil);
                log.info("RC: " + rc + " RM: " + rm + "\n MapRC: " + mapRc);
                if (mapRc == null) {
                    if (rc.equals("16")) {
                        req.setRm(rm);
//                        req.setResponse(new JSONObject().put("rm",rm));
                        agent.setStatusBank(true);
                        agent.setStatus(Constants.Status.ACTIVE);
                        agent.setModTime(LocalDateTime.now());
                        MAgent savedAgent = agentRepository.save(agent);
                        req.setStatus(Constants.TRX_STATUS.SUCCESS);
                        ctx.put(Constants.OBJ.WSREQUEST, req);
                        return ABORTED | NO_JOIN;
                    }
                    req.setRm(rc + " | " + rm);
                    req.setStatus(Constants.TRX_STATUS.FAILED);
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                req.setRm(mapRc.getRm());
                req.setStatus(mapRc.getStatus());
                if (Constants.TRX_STATUS.SUCCESS == req.getStatus()) {
                    req.setStatus(Constants.TRX_STATUS.SUCCESS);
                    ctx.put(Constants.AGENT, agent);
                    ctx.put(Constants.OBJ.RESP, hasil);
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return PREPARED;
                } else {
                    req.setStatus(Constants.TRX_STATUS.FAILED);
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
            } else {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Transaksi tidak dapat dilakukan, Bank Response Error");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        } else {
            agent.setStatus(Constants.Status.REJECTED);
            MAgent savedAgent = agentRepository.save(agent);
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            req.setRm("Agent " + agent.getId() + " ditolak, keterangan : " + agent.getInfo() + ", " + savedAgent.getModTime().format(Constants.FORMAT_DDMMYYYYHHMMSS));
            ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Agent " + agent.getId() + " ditolak, keterangan : " + agent.getInfo() + ", " + savedAgent.getModTime().format(Constants.FORMAT_DDMMYYYYHHMMSS)));
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        agent.setStatusBank(true);
        agent.setStatus(Constants.Status.ACTIVE);
        agent.setModTime(LocalDateTime.now());
        MAgent savedAgent = agentRepository.save(agent);
        JSONObject resp = new JSONObject().put("rm", savedAgent.getId() + " berhasil diaktifkan [" + savedAgent.getModTime().format(Constants.FORMAT_DDMMYYYYHHMMSS) + "]");
        ctx.put(Constants.OBJ.RESP, resp);
        ctx.put(Constants.OBJ.WSREQUEST, req);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
