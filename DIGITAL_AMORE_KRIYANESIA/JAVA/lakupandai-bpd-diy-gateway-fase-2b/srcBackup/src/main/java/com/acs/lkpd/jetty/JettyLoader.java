// Copyright (c) Micro Pay Nusantara 2010
package com.acs.lkpd.jetty;

import com.acs.util.Utility;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import org.eclipse.jetty.deploy.DeploymentManager;
import org.eclipse.jetty.deploy.PropertiesConfigurationManager;
import org.eclipse.jetty.deploy.bindings.DebugListenerBinding;
import org.eclipse.jetty.deploy.providers.WebAppProvider;
import org.eclipse.jetty.server.DebugListener;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnectionStatistics;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ScheduledExecutorScheduler;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.xml.XmlConfiguration;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author Erwin, updated Yogha
 *
 */
public class JettyLoader extends QBeanSupport implements Serializable {

    protected Server server;
    protected String name;

    @Override
    public void initService() throws Exception {
        //Load Parameter;
        int port = cfg.getInt("port", 8080);
        name = cfg.get("name", Utility.generateStan());
        String logLocation = cfg.get("log_location", "./log/jetty/");
        String logName = "yyyy_MM_dd." + name + ".request.log";
        int retainDays = cfg.getInt("log.setRetainDays", 90);
        boolean append = cfg.getBoolean("log.setAppend", true);
        boolean extended = cfg.getBoolean("log.setExtended", true);
        int minThread = cfg.getInt("minThreads", 10);
        int maxThread = cfg.getInt("maxThreads", 500);
        int idleTimeout = cfg.getInt("idleTimeout", 60000);
        String directory = cfg.get("directory", "/webapps");
        String webdefault = cfg.get("web_default", "cfg/etc/webdefault.xml");
        String home = new File(".").getCanonicalPath();
        String pathDirectory = home + directory;
        String pathWeb = home + webdefault;
        String context = cfg.get("webAppContext", "");
        LogEvent le = new LogEvent("Jetty " + name);
        le.addMessage("!<><><><><><><><><><><><>!");
        le.addMessage("Name          : " + name);
        le.addMessage("Port          : " + port);
        le.addMessage("ScanPath      : " + pathDirectory);
        le.addMessage("WebPath       : " + pathDirectory);
        le.addMessage("LogLocation   : " + logLocation + logName);
        le.addMessage("!<><><><><><><><><><><><>!");
        log.info(le.toString());
        // === jetty.xml ===
        server = new Server();

        // Scheduler
        server.addBean(new ScheduledExecutorScheduler());

        // Setup Threadpool
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(minThread);
        threadPool.setMaxThreads(maxThread);
        threadPool.setIdleTimeout(idleTimeout);

        // HTTP Configuration
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSecurePort(8443);
        http_config.setOutputBufferSize(32768);
        http_config.setRequestHeaderSize(8192);
        http_config.setResponseHeaderSize(8192);
        http_config.setSendServerVersion(false);
        http_config.setSendDateHeader(false);

        // Handler Structure
        HandlerCollection handlers = new HandlerCollection();
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        Handler[] arrayHandler;
        if (context.isEmpty()) {
            WebAppContext webAppContext = new WebAppContext();
            webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
            arrayHandler = new Handler[3];
            arrayHandler[0] = contexts;
            arrayHandler[1] = new DefaultHandler();
        } else {
            String[] dataSplit = context.split(",");
            arrayHandler = new Handler[(2 + dataSplit.length)];
            for (int i = 0; i < dataSplit.length; i++) {
                if (dataSplit[i].contains(":")) {
                    //Remove Dir List
                    String warNameValue = dataSplit[i].split(":")[0];
                    String contextPathValue = dataSplit[i].split(":")[1];
                    WebAppContext webAppContext = new WebAppContext(pathDirectory+warNameValue, directory);
                    webAppContext.setContextPath(contextPathValue);
                    webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
                    log.info("DEPLOY webAppContext " + warNameValue + " | Context Path : " + contextPathValue);
                    arrayHandler[0] = contexts;
                    arrayHandler[1] = new DefaultHandler();
                    arrayHandler[2 + i] = webAppContext;
                } else {
                    log.info("NOT DEPLOY webAppContext " + dataSplit[i] + " : Separator ':' not found in parameter");
                }
            }
        }
        handlers.setHandlers(arrayHandler);
        server.setHandler(handlers);

        // Extra options
        server.setDumpAfterStart(false);
        server.setDumpBeforeStop(false);
        server.setStopAtShutdown(true);

        // === jetty-http.xml ===
        ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(http_config));
        http.setPort(port);
        http.setIdleTimeout(idleTimeout);
        server.addConnector(http);

        // === jetty-deploy.xml ===
        DebugListener debug = new DebugListener(System.err, true, true, true);

        DeploymentManager deployer = new DeploymentManager();
        deployer.addLifeCycleBinding(new DebugListenerBinding(debug));
        deployer.setContexts(contexts);
        deployer.setContextAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern", ".*/servlet-api-[^/]*\\.jar$");

        WebAppProvider webapp_provider = new WebAppProvider();
        webapp_provider.setMonitoredDirName(pathDirectory);
        webapp_provider.setDefaultsDescriptor(pathWeb);
        webapp_provider.setExtractWars(true);
        webapp_provider.setConfigurationManager(new PropertiesConfigurationManager());
        deployer.addAppProvider(webapp_provider);
        server.addBean(debug);
        server.addBean(deployer);

        // === jetty-stats.xml ===
        StatisticsHandler stats = new StatisticsHandler();
        stats.setHandler(server.getHandler());
        server.setHandler(stats);
        ServerConnectionStatistics.addToAllConnectors(server);

//        // === Rewrite Handler
//        RewriteHandler rewrite = new RewriteHandler();
//        rewrite.setHandler(server.getHandler());
//        server.setHandler(rewrite);
        // === jetty-requestlog.xml ===
        NCSARequestLog requestLog = new NCSARequestLog();
        requestLog.setFilename(logLocation + logName);
        requestLog.setFilenameDateFormat("yyyy_MM_dd");
        requestLog.setRetainDays(retainDays);
        requestLog.setAppend(append);
        requestLog.setExtended(extended);
        requestLog.setLogCookies(false);
        requestLog.setLogTimeZone("GMT+7");
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        requestLogHandler.setRequestLog(requestLog);
        handlers.addHandler(requestLogHandler);

        FileInputStream fis = new FileInputStream(cfg.get("config"));
        XmlConfiguration xml = new XmlConfiguration(fis);
        xml.configure(server);
    }

    /**
     * Starting Jetty Web-Server service.
     *
     * @throws java.lang.Exception
     */
    @Override
    public void startService() throws Exception {
        log.info("webapps " + name + " started...");
        NameRegistrar.register("jetty-" + name, server);
        server.start();
    }

    /**
     * Stopped Jetty Web-Server service.
     *
     * @throws java.lang.Exception
     */
    @Override
    protected void destroyService() throws Exception {
        log.info("webapps " + name + " stopped...");
        NameRegistrar.unregister("jetty-" + name);
        server.stop();
    }

}
