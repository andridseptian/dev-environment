/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MProduct;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatInquiryTransferOnUs implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String response = Service.getResponseFromSettings(KeyMap.RM_INQUIRY_TRANSFER_ON_US.name());
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        ctx.log(session.getNasabah().getMsisdn());
        ctx.log(session.getTargetNasabahAccountNumber());
        ctx.log(session.getTargetNasabahName());
       
        try {
//            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
//                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
//                    .replace("[TargetAccount]", session.getTargetNasabahAccountNumber())
//                    .replace("[TargetName]", session.getTargetNasabahName());

            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[DestinationAccount]", session.getTargetNasabahAccountNumber())
                            .replace("[DestinationName]", session.getTargetNasabahName())
                            .replace("[Amount]", "Rp." + Utility.formatAmount(session.getAmount()))
                            .replace("[FromMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                            .replace("[FromName]", session.getNasabah().getName())
                            .replace("[Desc]", Service.getResponseFromSettings(KeyMap.RM_MESSAGE_TOKEN.name())
                            .replace("[Process]", trx.getDescription())));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";

            }

//            response = response + Service.getResponseFromSettings(KeyMap.RM_MESSAGE_TOKEN.name()).replace("[Process]", trx.getDescription());
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            trx.setInfo(response);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return PREPARED;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }

}
