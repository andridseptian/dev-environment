package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "web_group_menu")
public class WebGroupMenu implements Serializable {

    private Long id;
    private String value;
    private String request_value;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private boolean menu_dashboard;
    private boolean menu_manage_agent;
    private boolean menu_manage_customer;
    private boolean menu_approve_configuration;
    private boolean menu_approve_user;
    private boolean menu_approve_agent;
    private boolean menu_approve_customer;
    private boolean menu_approve_cabang;
    private boolean menu_approve_group_menu;
    private boolean sub_approveconfiguration_approvechange;
    private boolean sub_approveconfiguration_rejectchange;
    private boolean sub_approveuser_approvechange;
    private boolean sub_approveuser_rejectchange;
    private boolean sub_approvecabang_approve;
    private boolean sub_approvecabang_reject;
    private boolean sub_approvegroupmenu_approve;
    private boolean sub_approvegroupmenu_reject;
    private boolean menu_performance_agent;
    private boolean menu_performance_customer;
    private boolean menu_performance_cabang;
    private boolean menu_check_limit;
    private boolean menu_manage_user;
    private boolean menu_manage_group_menu_user;
    private boolean menu_manage_configuration;
    private boolean menu_manage_configuration_rc;
    private boolean menu_manage_cabang;
    private boolean menu_manage_product;
    private boolean menu_session;
    private boolean menu_session_web;
    private boolean menu_transaction;
    private boolean sub_manageconfiguration_editparameter;
    private boolean sub_managecustomer_createcustomer;
    private boolean sub_managecustomer_editcustomer;
    private boolean sub_managecustomer_removecustomer;
    private boolean sub_managecustomer_editphone;
    private boolean sub_manageuser_createuser;
    private boolean sub_manageuser_unlockuser;
    private boolean sub_manageuser_resetpassword;
    private boolean sub_manageuser_edit;
    private boolean sub_manageuser_activeinactive;
    private boolean sub_manageuser_deleteuser;
    private boolean sub_managegroupmenuuser_creategroup;
    private boolean sub_managegroupmenuuser_editgroup;
    private boolean sub_manageconfigurationrc_editparameter;
    private boolean sub_manageagent_registeragent;
    private boolean sub_manageagent_resendotp;
    private boolean sub_manageagent_validasi;
    private boolean sub_manageagent_blockunblock;
    private boolean sub_manageagent_resetpassword;
    private boolean sub_approveagent_approvechange;
    private boolean sub_approveagent_rejectchange;
    private boolean sub_manageagent_validasiotp;
    private boolean sub_manageagent_delete;
    private boolean sub_managecabang_create;
    private boolean sub_managecabang_update;
    private boolean sub_managecabang_delete;
    private boolean sub_approvecustomer_approve;
    private boolean sub_approvecustomer_reject;
    private boolean sub_managecustomer_blockunblock;
    private boolean sub_manageproduct_editparameter;
    private boolean sub_manageagent_editnpwp;
    private boolean sub_manageagent_editphone;

    //request 
    private boolean request_menu_dashboard;
    private boolean request_menu_manage_agent;
    private boolean request_menu_manage_customer;
    private boolean request_menu_approve_configuration;
    private boolean request_menu_approve_user;
    private boolean request_menu_manage_configuration;
    private boolean request_menu_check_limit;
    private boolean request_menu_manage_user;
    private boolean request_menu_manage_group_menu_user;
    private boolean request_menu_session;
    private boolean request_menu_transaction;
    private boolean request_menu_manage_configuration_rc;
    private boolean request_menu_approve_agent;
    private boolean request_menu_approve_customer;
    private boolean request_menu_manage_cabang;
    private boolean request_menu_session_web;
    private boolean request_menu_approve_cabang;
    private boolean request_sub_approveconfiguration_approvechange;
    private boolean request_sub_approveconfiguration_rejectchange;
    private boolean request_sub_approveuser_approvechange;
    private boolean request_sub_approveuser_rejectchange;
    private boolean request_sub_manageconfiguration_editparameter;
    private boolean request_sub_managecustomer_createcustomer;
    private boolean request_sub_managecustomer_editcustomer;
    private boolean request_sub_managecustomer_removecustomer;
    private boolean request_sub_manageuser_createuser;
    private boolean request_sub_manageuser_unlockuser;
    private boolean request_sub_manageuser_resetpassword;
    private boolean request_sub_manageuser_edit;
    private boolean request_sub_manageuser_activeinactive;
    private boolean request_sub_manageuser_deleteuser;
    private boolean request_sub_managegroupmenuuser_creategroup;
    private boolean request_sub_managegroupmenuuser_editgroup;
    private boolean request_sub_manageconfigurationrc_editparameter;
    private boolean request_sub_manageagent_registeragent;
    private boolean request_sub_manageagent_resendotp;
    private boolean request_sub_manageagent_validasi;
    private boolean request_sub_manageagent_blockunblock;
    private boolean request_sub_manageagent_resetpassword;
    private boolean request_sub_approveagent_approvechange;
    private boolean request_sub_approveagent_rejectchange;
    private boolean request_sub_manageagent_validasiotp;
    private boolean request_sub_manageagent_delete;
    private boolean request_sub_managecabang_create;
    private boolean request_sub_managecabang_update;
    private boolean request_sub_managecabang_delete;
    private boolean request_sub_approvecustomer_approve;
    private boolean request_sub_approvecustomer_reject;
    private boolean request_sub_managecustomer_blockunblock;
    private boolean request_sub_approvecabang_approve;
    private boolean request_sub_approvecabang_reject;
    private boolean request_menu_approve_group_menu;
    private boolean request_sub_approvegroupmenu_approve;
    private boolean request_sub_approvegroupmenu_reject;
    private boolean request_menu_performance_agent;
    private boolean request_menu_performance_customer;
    private boolean request_menu_performance_cabang;
    private boolean request_menu_manage_product;
    private boolean request_sub_manageproduct_editparameter;
    private boolean request_sub_manageagent_editnpwp;
    private boolean request_sub_manageagent_editphone;
    private boolean request_sub_managecustomer_editphone;
    private int status;
    private int status_approval;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_menu_seq")
    @SequenceGenerator(name = "group_menu_seq", sequenceName = "group_menu_seq", allocationSize = 1, initialValue = 1)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "value", nullable = false)
    public String getValue() {
        return value;
    }

    @Column(name = "request_value")
    public String getRequest_value() {
        return request_value;
    }

    @Column(name = "menu_dashboard", columnDefinition = "boolean default false")
    public boolean isMenu_dashboard() {
        return menu_dashboard;
    }

    @Column(name = "menu_manage_agent", columnDefinition = "boolean default false")
    public boolean isMenu_manage_agent() {
        return menu_manage_agent;
    }

    @Column(name = "menu_manage_customer", columnDefinition = "boolean default false")
    public boolean isMenu_manage_customer() {
        return menu_manage_customer;
    }

    @Column(name = "menu_approve_configuration", columnDefinition = "boolean default false")
    public boolean isMenu_approve_configuration() {
        return menu_approve_configuration;
    }

    @Column(name = "menu_approve_user", columnDefinition = "boolean default false")
    public boolean isMenu_approve_user() {
        return menu_approve_user;
    }

    @Column(name = "menu_manage_configuration", columnDefinition = "boolean default false")
    public boolean isMenu_manage_configuration() {
        return menu_manage_configuration;
    }

    @Column(name = "menu_check_limit", columnDefinition = "boolean default false")
    public boolean isMenu_check_limit() {
        return menu_check_limit;
    }

    @Column(name = "menu_session_web", columnDefinition = "boolean default false")
    public boolean isMenu_session_web() {
        return menu_session_web;
    }

    @Column(name = "menu_manage_user", columnDefinition = "boolean default false")
    public boolean isMenu_manage_user() {
        return menu_manage_user;
    }

    @Column(name = "menu_manage_group_menu_user", columnDefinition = "boolean default false")
    public boolean isMenu_manage_group_menu_user() {
        return menu_manage_group_menu_user;
    }

    @Column(name = "menu_session", columnDefinition = "boolean default false")
    public boolean isMenu_session() {
        return menu_session;
    }

    @Column(name = "menu_transaction", columnDefinition = "boolean default false")
    public boolean isMenu_transaction() {
        return menu_transaction;
    }

    @Column(name = "menu_manage_configuration_rc", columnDefinition = "boolean default false")
    public boolean isMenu_manage_configuration_rc() {
        return menu_manage_configuration_rc;
    }

    @Column(name = "menu_approve_agent", columnDefinition = "boolean default false")
    public boolean isMenu_approve_agent() {
        return menu_approve_agent;
    }

    @Column(name = "menu_manage_cabang", columnDefinition = "boolean default false")
    public boolean isMenu_manage_cabang() {
        return menu_manage_cabang;
    }

    @Column(name = "sub_approveconfiguration_approvechange", columnDefinition = "boolean default false")
    public boolean isSub_approveconfiguration_approvechange() {
        return sub_approveconfiguration_approvechange;
    }

    @Column(name = "sub_approveconfiguration_rejectchange", columnDefinition = "boolean default false")
    public boolean isSub_approveconfiguration_rejectchange() {
        return sub_approveconfiguration_rejectchange;
    }

    @Column(name = "sub_approveuser_approvechange", columnDefinition = "boolean default false")
    public boolean isSub_approveuser_approvechange() {
        return sub_approveuser_approvechange;
    }

    @Column(name = "sub_approveuser_rejectchange", columnDefinition = "boolean default false")
    public boolean isSub_approveuser_rejectchange() {
        return sub_approveuser_rejectchange;
    }

    @Column(name = "sub_manageconfiguration_editparameter", columnDefinition = "boolean default false")
    public boolean isSub_manageconfiguration_editparameter() {
        return sub_manageconfiguration_editparameter;
    }

    @Column(name = "sub_managecustomer_createcustomer", columnDefinition = "boolean default false")
    public boolean isSub_managecustomer_createcustomer() {
        return sub_managecustomer_createcustomer;
    }

    @Column(name = "sub_managecustomer_editcustomer", columnDefinition = "boolean default false")
    public boolean isSub_managecustomer_editcustomer() {
        return sub_managecustomer_editcustomer;
    }

    @Column(name = "sub_managecustomer_removecustomer", columnDefinition = "boolean default false")
    public boolean isSub_managecustomer_removecustomer() {
        return sub_managecustomer_removecustomer;
    }

    @Column(name = "sub_manageuser_createuser", columnDefinition = "boolean default false")
    public boolean isSub_manageuser_createuser() {
        return sub_manageuser_createuser;
    }

    @Column(name = "sub_manageuser_unlockuser", columnDefinition = "boolean default false")
    public boolean isSub_manageuser_unlockuser() {
        return sub_manageuser_unlockuser;
    }

    @Column(name = "sub_manageuser_resetpassword", columnDefinition = "boolean default false")
    public boolean isSub_manageuser_resetpassword() {
        return sub_manageuser_resetpassword;
    }

    @Column(name = "sub_managegroupmenuuser_creategroup", columnDefinition = "boolean default false")
    public boolean isSub_managegroupmenuuser_creategroup() {
        return sub_managegroupmenuuser_creategroup;
    }

    @Column(name = "sub_managegroupmenuuser_editgroup", columnDefinition = "boolean default false")
    public boolean isSub_managegroupmenuuser_editgroup() {
        return sub_managegroupmenuuser_editgroup;
    }

    @Column(name = "sub_manageconfigurationrc_editparameter", columnDefinition = "boolean default false")
    public boolean isSub_manageconfigurationrc_editparameter() {
        return sub_manageconfigurationrc_editparameter;
    }

    @Column(name = "sub_manageagent_registeragent", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_registeragent() {
        return sub_manageagent_registeragent;
    }

    @Column(name = "sub_manageagent_resendotp", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_resendotp() {
        return sub_manageagent_resendotp;
    }

    @Column(name = "sub_manageagent_validasi", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_validasi() {
        return sub_manageagent_validasi;
    }

    @Column(name = "sub_approveagent_approvechange", columnDefinition = "boolean default false")
    public boolean isSub_approveagent_approvechange() {
        return sub_approveagent_approvechange;
    }

    @Column(name = "sub_approveagent_rejectchange", columnDefinition = "boolean default false")
    public boolean isSub_approveagent_rejectchange() {
        return sub_approveagent_rejectchange;
    }

    @Column(name = "sub_manageagent_validasiotp", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_validasiotp() {
        return sub_manageagent_validasiotp;
    }

    @Column(name = "sub_managecabang_create", columnDefinition = "boolean default false")
    public boolean isSub_managecabang_create() {
        return sub_managecabang_create;
    }

    @Column(name = "sub_managecabang_update", columnDefinition = "boolean default false")
    public boolean isSub_managecabang_update() {
        return sub_managecabang_update;
    }

    @Column(name = "sub_managecabang_delete", columnDefinition = "boolean default false")
    public boolean isSub_managecabang_delete() {
        return sub_managecabang_delete;
    }

    @Column(name = "menu_approve_customer", columnDefinition = "boolean default false")
    public boolean isMenu_approve_customer() {
        return menu_approve_customer;
    }

    @Column(name = "sub_approvecustomer_approve", columnDefinition = "boolean default false")
    public boolean isSub_approvecustomer_approve() {
        return sub_approvecustomer_approve;
    }

    @Column(name = "sub_approvecustomer_reject", columnDefinition = "boolean default false")
    public boolean isSub_approvecustomer_reject() {
        return sub_approvecustomer_reject;
    }

    @Column(name = "sub_manageagent_blockunblock", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_blockunblock() {
        return sub_manageagent_blockunblock;
    }

    @Column(name = "sub_managecustomer_blockunblock", columnDefinition = "boolean default false")
    public boolean isSub_managecustomer_blockunblock() {
        return sub_managecustomer_blockunblock;
    }

    @Column(name = "menu_approve_cabang", columnDefinition = "boolean default false")
    public boolean isMenu_approve_cabang() {
        return menu_approve_cabang;
    }

    @Column(name = "sub_manageuser_edit", columnDefinition = "boolean default false")
    public boolean isSub_manageuser_edit() {
        return sub_manageuser_edit;
    }

    @Column(name = "sub_manageagent_resetpassword", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_resetpassword() {
        return sub_manageagent_resetpassword;
    }

    @Column(name = "sub_approvecabang_approve", columnDefinition = "boolean default false")
    public boolean isSub_approvecabang_approve() {
        return sub_approvecabang_approve;
    }

    @Column(name = "sub_approvecabang_reject", columnDefinition = "boolean default false")
    public boolean isSub_approvecabang_reject() {
        return sub_approvecabang_reject;
    }

    @Column(name = "sub_manageuser_activeinactive", columnDefinition = "boolean default false")
    public boolean isSub_manageuser_activeinactive() {
        return sub_manageuser_activeinactive;
    }

    @Column(name = "sub_manageuser_deleteuser", columnDefinition = "boolean default false")
    public boolean isSub_manageuser_deleteuser() {
        return sub_manageuser_deleteuser;
    }

    @Column(name = "sub_manageagent_delete", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_delete() {
        return sub_manageagent_delete;
    }

    //============NEW REQUEST==========//
    @Column(name = "request_menu_dashboard", columnDefinition = "boolean default false")
    public boolean isRequest_menu_dashboard() {
        return request_menu_dashboard;
    }

    @Column(name = "request_menu_manage_agent", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_agent() {
        return request_menu_manage_agent;
    }

    @Column(name = "request_menu_manage_customer", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_customer() {
        return request_menu_manage_customer;
    }

    @Column(name = "request_menu_approve_configuration", columnDefinition = "boolean default false")
    public boolean isRequest_menu_approve_configuration() {
        return request_menu_approve_configuration;
    }

    @Column(name = "request_menu_approve_user", columnDefinition = "boolean default false")
    public boolean isRequest_menu_approve_user() {
        return request_menu_approve_user;
    }

    @Column(name = "request_menu_manage_configuration", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_configuration() {
        return request_menu_manage_configuration;
    }

    @Column(name = "request_menu_check_limit", columnDefinition = "boolean default false")
    public boolean isRequest_menu_check_limit() {
        return request_menu_check_limit;
    }

    @Column(name = "request_menu_manage_user", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_user() {
        return request_menu_manage_user;
    }

    @Column(name = "request_menu_manage_group_menu_user", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_group_menu_user() {
        return request_menu_manage_group_menu_user;
    }

    @Column(name = "request_menu_session", columnDefinition = "boolean default false")
    public boolean isRequest_menu_session() {
        return request_menu_session;
    }

    @Column(name = "request_menu_transaction", columnDefinition = "boolean default false")
    public boolean isRequest_menu_transaction() {
        return request_menu_transaction;
    }

    @Column(name = "request_menu_manage_configuration_rc", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_configuration_rc() {
        return request_menu_manage_configuration_rc;
    }

    @Column(name = "request_menu_approve_agent", columnDefinition = "boolean default false")
    public boolean isRequest_menu_approve_agent() {
        return request_menu_approve_agent;
    }

    @Column(name = "request_menu_approve_customer", columnDefinition = "boolean default false")
    public boolean isRequest_menu_approve_customer() {
        return request_menu_approve_customer;
    }

    @Column(name = "request_menu_manage_cabang", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_cabang() {
        return request_menu_manage_cabang;
    }

    @Column(name = "request_menu_session_web", columnDefinition = "boolean default false")
    public boolean isRequest_menu_session_web() {
        return request_menu_session_web;
    }

    @Column(name = "request_menu_approve_cabang", columnDefinition = "boolean default false")
    public boolean isRequest_menu_approve_cabang() {
        return request_menu_approve_cabang;
    }

    @Column(name = "request_sub_approveconfiguration_approvechange", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approveconfiguration_approvechange() {
        return request_sub_approveconfiguration_approvechange;
    }

    @Column(name = "request_sub_approveconfiguration_rejectchange", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approveconfiguration_rejectchange() {
        return request_sub_approveconfiguration_rejectchange;
    }

    @Column(name = "request_sub_approveuser_approvechange", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approveuser_approvechange() {
        return request_sub_approveuser_approvechange;
    }

    @Column(name = "request_sub_approveuser_rejectchange", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approveuser_rejectchange() {
        return request_sub_approveuser_rejectchange;
    }

    @Column(name = "request_sub_manageconfiguration_editparameter", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageconfiguration_editparameter() {
        return request_sub_manageconfiguration_editparameter;
    }

    @Column(name = "request_sub_managecustomer_createcustomer", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecustomer_createcustomer() {
        return request_sub_managecustomer_createcustomer;
    }

    @Column(name = "request_sub_managecustomer_editcustomer", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecustomer_editcustomer() {
        return request_sub_managecustomer_editcustomer;
    }

    @Column(name = "request_sub_managecustomer_removecustomer", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecustomer_removecustomer() {
        return request_sub_managecustomer_removecustomer;
    }

    @Column(name = "request_sub_manageuser_createuser", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageuser_createuser() {
        return request_sub_manageuser_createuser;
    }

    @Column(name = "request_sub_manageuser_unlockuser", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageuser_unlockuser() {
        return request_sub_manageuser_unlockuser;
    }

    @Column(name = "request_sub_manageuser_resetpassword", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageuser_resetpassword() {
        return request_sub_manageuser_resetpassword;
    }

    @Column(name = "request_sub_manageuser_edit", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageuser_edit() {
        return request_sub_manageuser_edit;
    }

    @Column(name = "request_sub_manageuser_activeinactive", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageuser_activeinactive() {
        return request_sub_manageuser_activeinactive;
    }

    @Column(name = "request_sub_manageuser_deleteuser", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageuser_deleteuser() {
        return request_sub_manageuser_deleteuser;
    }

    @Column(name = "request_sub_managegroupmenuuser_creategroup", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managegroupmenuuser_creategroup() {
        return request_sub_managegroupmenuuser_creategroup;
    }

    @Column(name = "request_sub_managegroupmenuuser_editgroup", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managegroupmenuuser_editgroup() {
        return request_sub_managegroupmenuuser_editgroup;
    }

    @Column(name = "request_sub_manageconfigurationrc_editparameter", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageconfigurationrc_editparameter() {
        return request_sub_manageconfigurationrc_editparameter;
    }

    @Column(name = "request_sub_manageagent_registeragent", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_registeragent() {
        return request_sub_manageagent_registeragent;
    }

    @Column(name = "request_sub_manageagent_resendotp", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_resendotp() {
        return request_sub_manageagent_resendotp;
    }

    @Column(name = "request_sub_manageagent_validasi", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_validasi() {
        return request_sub_manageagent_validasi;
    }

    @Column(name = "request_sub_manageagent_blockunblock", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_blockunblock() {
        return request_sub_manageagent_blockunblock;
    }

    @Column(name = "request_sub_manageagent_resetpassword", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_resetpassword() {
        return request_sub_manageagent_resetpassword;
    }

    @Column(name = "request_sub_approveagent_approvechange", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approveagent_approvechange() {
        return request_sub_approveagent_approvechange;
    }

    @Column(name = "request_sub_approveagent_rejectchange", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approveagent_rejectchange() {
        return request_sub_approveagent_rejectchange;
    }

    @Column(name = "request_sub_manageagent_validasiotp", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_validasiotp() {
        return request_sub_manageagent_validasiotp;
    }

    @Column(name = "request_sub_manageagent_delete", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_delete() {
        return request_sub_manageagent_delete;
    }

    @Column(name = "request_sub_managecabang_create", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecabang_create() {
        return request_sub_managecabang_create;
    }

    @Column(name = "request_sub_managecabang_update", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecabang_update() {
        return request_sub_managecabang_update;
    }

    @Column(name = "request_sub_managecabang_delete", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecabang_delete() {
        return request_sub_managecabang_delete;
    }

    @Column(name = "request_sub_approvecustomer_approve", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approvecustomer_approve() {
        return request_sub_approvecustomer_approve;
    }

    @Column(name = "request_sub_approvecustomer_reject", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approvecustomer_reject() {
        return request_sub_approvecustomer_reject;
    }

    @Column(name = "request_sub_managecustomer_blockunblock", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecustomer_blockunblock() {
        return request_sub_managecustomer_blockunblock;
    }

    @Column(name = "request_sub_approvecabang_approve", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approvecabang_approve() {
        return request_sub_approvecabang_approve;
    }

    @Column(name = "request_sub_approvecabang_reject", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approvecabang_reject() {
        return request_sub_approvecabang_reject;
    }

    @Column(name = "status", columnDefinition = "int default 0")
    public int getStatus() {
        return status;
    }

    @Column(name = "status_approval", columnDefinition = "int default 0")
    public int getStatus_approval() {
        return status_approval;
    }

    @Column(name = "menu_approve_group_menu", columnDefinition = "boolean default false")
    public boolean isMenu_approve_group_menu() {
        return menu_approve_group_menu;
    }

    @Column(name = "sub_approvegroupmenu_approve", columnDefinition = "boolean default false")
    public boolean isSub_approvegroupmenu_approve() {
        return sub_approvegroupmenu_approve;
    }

    @Column(name = "sub_approvegroupmenu_reject", columnDefinition = "boolean default false")
    public boolean isSub_approvegroupmenu_reject() {
        return sub_approvegroupmenu_reject;
    }

    @Column(name = "menu_performance_agent", columnDefinition = "boolean default false")
    public boolean isMenu_performance_agent() {
        return menu_performance_agent;
    }

    @Column(name = "menu_performance_customer", columnDefinition = "boolean default false")
    public boolean isMenu_performance_customer() {
        return menu_performance_customer;
    }

    @Column(name = "menu_performance_cabang", columnDefinition = "boolean default false")
    public boolean isMenu_performance_cabang() {
        return menu_performance_cabang;
    }

    @Column(name = "request_menu_approve_group_menu", columnDefinition = "boolean default false")
    public boolean isRequest_menu_approve_group_menu() {
        return request_menu_approve_group_menu;
    }

    @Column(name = "request_sub_approvegroupmenu_approve", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approvegroupmenu_approve() {
        return request_sub_approvegroupmenu_approve;
    }

    @Column(name = "request_sub_approvegroupmenu_reject", columnDefinition = "boolean default false")
    public boolean isRequest_sub_approvegroupmenu_reject() {
        return request_sub_approvegroupmenu_reject;
    }

    @Column(name = "request_menu_performance_agent", columnDefinition = "boolean default false")
    public boolean isRequest_menu_performance_agent() {
        return request_menu_performance_agent;
    }

    @Column(name = "request_menu_performance_customer", columnDefinition = "boolean default false")
    public boolean isRequest_menu_performance_customer() {
        return request_menu_performance_customer;
    }

    @Column(name = "request_menu_performance_cabang", columnDefinition = "boolean default false")
    public boolean isRequest_menu_performance_cabang() {
        return request_menu_performance_cabang;
    }

    @Column(name = "menu_manage_product", columnDefinition = "boolean default false")
    public boolean isMenu_manage_product() {
        return menu_manage_product;
    }

    @Column(name = "sub_managecustomer_editphone", columnDefinition = "boolean default false")
    public boolean isSub_managecustomer_editphone() {
        return sub_managecustomer_editphone;
    }

    @Column(name = "sub_manageproduct_editparameter", columnDefinition = "boolean default false")
    public boolean isSub_manageproduct_editparameter() {
        return sub_manageproduct_editparameter;
    }

    @Column(name = "sub_manageagent_editnpwp", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_editnpwp() {
        return sub_manageagent_editnpwp;
    }

    @Column(name = "sub_manageagent_editphone", columnDefinition = "boolean default false")
    public boolean isSub_manageagent_editphone() {
        return sub_manageagent_editphone;
    }

    @Column(name = "request_menu_manage_product", columnDefinition = "boolean default false")
    public boolean isRequest_menu_manage_product() {
        return request_menu_manage_product;
    }

    @Column(name = "request_sub_manageproduct_editparameter", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageproduct_editparameter() {
        return request_sub_manageproduct_editparameter;
    }

    @Column(name = "request_sub_manageagent_editnpwp", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_editnpwp() {
        return request_sub_manageagent_editnpwp;
    }

    @Column(name = "request_sub_manageagent_editphone", columnDefinition = "boolean default false")
    public boolean isRequest_sub_manageagent_editphone() {
        return request_sub_manageagent_editphone;
    }

    @Column(name = "request_sub_managecustomer_editphone", columnDefinition = "boolean default false")
    public boolean isRequest_sub_managecustomer_editphone() {
        return request_sub_managecustomer_editphone;
    }

    //============SET=============//
    public void setId(Long id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setMenu_dashboard(boolean menu_dashboard) {
        this.menu_dashboard = menu_dashboard;
    }

    public void setMenu_manage_agent(boolean menu_manage_agent) {
        this.menu_manage_agent = menu_manage_agent;
    }

    public void setMenu_manage_customer(boolean menu_manage_customer) {
        this.menu_manage_customer = menu_manage_customer;
    }

    public void setMenu_approve_configuration(boolean menu_approve_configuration) {
        this.menu_approve_configuration = menu_approve_configuration;
    }

    public void setMenu_approve_user(boolean menu_approve_user) {
        this.menu_approve_user = menu_approve_user;
    }

    public void setMenu_manage_configuration(boolean menu_manage_configuration) {
        this.menu_manage_configuration = menu_manage_configuration;
    }

    public void setMenu_check_limit(boolean menu_check_limit) {
        this.menu_check_limit = menu_check_limit;
    }

    public void setMenu_manage_user(boolean menu_manage_user) {
        this.menu_manage_user = menu_manage_user;
    }

    public void setMenu_manage_group_menu_user(boolean menu_manage_group_menu_user) {
        this.menu_manage_group_menu_user = menu_manage_group_menu_user;
    }

    public void setMenu_session(boolean menu_session) {
        this.menu_session = menu_session;
    }

    public void setMenu_transaction(boolean menu_transaction) {
        this.menu_transaction = menu_transaction;
    }

    public void setSub_approveconfiguration_approvechange(boolean sub_approveconfiguration_approvechange) {
        this.sub_approveconfiguration_approvechange = sub_approveconfiguration_approvechange;
    }

    public void setSub_approveconfiguration_rejectchange(boolean sub_approveconfiguration_rejectchange) {
        this.sub_approveconfiguration_rejectchange = sub_approveconfiguration_rejectchange;
    }

    public void setSub_approveuser_approvechange(boolean sub_approveuser_approvechange) {
        this.sub_approveuser_approvechange = sub_approveuser_approvechange;
    }

    public void setSub_approveuser_rejectchange(boolean sub_approveuser_rejectchange) {
        this.sub_approveuser_rejectchange = sub_approveuser_rejectchange;
    }

    public void setSub_manageconfiguration_editparameter(boolean sub_manageconfiguration_editparameter) {
        this.sub_manageconfiguration_editparameter = sub_manageconfiguration_editparameter;
    }

    public void setSub_managecustomer_createcustomer(boolean sub_managecustomer_createcustomer) {
        this.sub_managecustomer_createcustomer = sub_managecustomer_createcustomer;
    }

    public void setSub_managecustomer_editcustomer(boolean sub_managecustomer_editcustomer) {
        this.sub_managecustomer_editcustomer = sub_managecustomer_editcustomer;
    }

    public void setSub_managecustomer_removecustomer(boolean sub_managecustomer_removecustomer) {
        this.sub_managecustomer_removecustomer = sub_managecustomer_removecustomer;
    }

    public void setSub_manageuser_createuser(boolean sub_manageuser_createuser) {
        this.sub_manageuser_createuser = sub_manageuser_createuser;
    }

    public void setSub_manageuser_unlockuser(boolean sub_manageuser_unlockuser) {
        this.sub_manageuser_unlockuser = sub_manageuser_unlockuser;
    }

    public void setSub_manageuser_resetpassword(boolean sub_manageuser_resetpassword) {
        this.sub_manageuser_resetpassword = sub_manageuser_resetpassword;
    }

    public void setSub_managegroupmenuuser_creategroup(boolean sub_managegroupmenuuser_creategroup) {
        this.sub_managegroupmenuuser_creategroup = sub_managegroupmenuuser_creategroup;
    }

    public void setSub_managegroupmenuuser_editgroup(boolean sub_managegroupmenuuser_editgroup) {
        this.sub_managegroupmenuuser_editgroup = sub_managegroupmenuuser_editgroup;
    }

    public void setMenu_manage_configuration_rc(boolean menu_manage_configuration_rc) {
        this.menu_manage_configuration_rc = menu_manage_configuration_rc;
    }

    public void setSub_manageconfigurationrc_editparameter(boolean sub_manageconfigurationrc_editparameter) {
        this.sub_manageconfigurationrc_editparameter = sub_manageconfigurationrc_editparameter;
    }

    public void setMenu_approve_agent(boolean menu_approve_agent) {
        this.menu_approve_agent = menu_approve_agent;
    }

    public void setSub_manageagent_registeragent(boolean sub_manageagent_registeragent) {
        this.sub_manageagent_registeragent = sub_manageagent_registeragent;
    }

    public void setSub_manageagent_resendotp(boolean sub_manageagent_resendotp) {
        this.sub_manageagent_resendotp = sub_manageagent_resendotp;
    }

    public void setSub_manageagent_validasi(boolean sub_manageagent_validasi) {
        this.sub_manageagent_validasi = sub_manageagent_validasi;
    }

    public void setSub_approveagent_approvechange(boolean sub_approveagent_approvechange) {
        this.sub_approveagent_approvechange = sub_approveagent_approvechange;
    }

    public void setSub_approveagent_rejectchange(boolean sub_approveagent_rejectchange) {
        this.sub_approveagent_rejectchange = sub_approveagent_rejectchange;
    }

    public void setSub_manageagent_validasiotp(boolean sub_manageagent_validasiotp) {
        this.sub_manageagent_validasiotp = sub_manageagent_validasiotp;
    }

    public void setSub_managecabang_create(boolean sub_managecabang_create) {
        this.sub_managecabang_create = sub_managecabang_create;
    }

    public void setSub_managecabang_update(boolean sub_managecabang_update) {
        this.sub_managecabang_update = sub_managecabang_update;
    }

    public void setSub_managecabang_delete(boolean sub_managecabang_delete) {
        this.sub_managecabang_delete = sub_managecabang_delete;
    }

    public void setMenu_manage_cabang(boolean menu_manage_cabang) {
        this.menu_manage_cabang = menu_manage_cabang;
    }

    public void setMenu_approve_customer(boolean menu_approve_customer) {
        this.menu_approve_customer = menu_approve_customer;
    }

    public void setSub_approvecustomer_approve(boolean sub_approvecustomer_approve) {
        this.sub_approvecustomer_approve = sub_approvecustomer_approve;
    }

    public void setSub_approvecustomer_reject(boolean sub_approvecustomer_reject) {
        this.sub_approvecustomer_reject = sub_approvecustomer_reject;
    }

    public void setMenu_session_web(boolean menu_session_web) {
        this.menu_session_web = menu_session_web;
    }

    public void setSub_manageagent_blockunblock(boolean sub_manageagent_blockunblock) {
        this.sub_manageagent_blockunblock = sub_manageagent_blockunblock;
    }

    public void setSub_managecustomer_blockunblock(boolean sub_managecustomer_blockunblock) {
        this.sub_managecustomer_blockunblock = sub_managecustomer_blockunblock;
    }

    public void setMenu_approve_cabang(boolean menu_approve_cabang) {
        this.menu_approve_cabang = menu_approve_cabang;
    }

    public void setSub_manageuser_edit(boolean sub_manageuser_edit) {
        this.sub_manageuser_edit = sub_manageuser_edit;
    }

    public void setSub_manageagent_resetpassword(boolean sub_manageagent_resetpassword) {
        this.sub_manageagent_resetpassword = sub_manageagent_resetpassword;
    }

    public void setSub_approvecabang_approve(boolean sub_approvecabang_approve) {
        this.sub_approvecabang_approve = sub_approvecabang_approve;
    }

    public void setSub_approvecabang_reject(boolean sub_approvecabang_reject) {
        this.sub_approvecabang_reject = sub_approvecabang_reject;
    }

    public void setSub_manageuser_activeinactive(boolean sub_manageuser_activeinactive) {
        this.sub_manageuser_activeinactive = sub_manageuser_activeinactive;
    }

    public void setSub_manageuser_deleteuser(boolean sub_manageuser_deleteuser) {
        this.sub_manageuser_deleteuser = sub_manageuser_deleteuser;
    }

    public void setSub_manageagent_delete(boolean sub_manageagent_delete) {
        this.sub_manageagent_delete = sub_manageagent_delete;
    }

    public void setRequest_menu_dashboard(boolean request_menu_dashboard) {
        this.request_menu_dashboard = request_menu_dashboard;
    }

    public void setRequest_menu_manage_agent(boolean request_menu_manage_agent) {
        this.request_menu_manage_agent = request_menu_manage_agent;
    }

    public void setRequest_menu_manage_customer(boolean request_menu_manage_customer) {
        this.request_menu_manage_customer = request_menu_manage_customer;
    }

    public void setRequest_menu_approve_configuration(boolean request_menu_approve_configuration) {
        this.request_menu_approve_configuration = request_menu_approve_configuration;
    }

    public void setRequest_menu_approve_user(boolean request_menu_approve_user) {
        this.request_menu_approve_user = request_menu_approve_user;
    }

    public void setRequest_menu_manage_configuration(boolean request_menu_manage_configuration) {
        this.request_menu_manage_configuration = request_menu_manage_configuration;
    }

    public void setRequest_menu_check_limit(boolean request_menu_check_limit) {
        this.request_menu_check_limit = request_menu_check_limit;
    }

    public void setRequest_menu_manage_user(boolean request_menu_manage_user) {
        this.request_menu_manage_user = request_menu_manage_user;
    }

    public void setRequest_menu_manage_group_menu_user(boolean request_menu_manage_group_menu_user) {
        this.request_menu_manage_group_menu_user = request_menu_manage_group_menu_user;
    }

    public void setRequest_menu_session(boolean request_menu_session) {
        this.request_menu_session = request_menu_session;
    }

    public void setRequest_menu_transaction(boolean request_menu_transaction) {
        this.request_menu_transaction = request_menu_transaction;
    }

    public void setRequest_menu_manage_configuration_rc(boolean request_menu_manage_configuration_rc) {
        this.request_menu_manage_configuration_rc = request_menu_manage_configuration_rc;
    }

    public void setRequest_menu_approve_agent(boolean request_menu_approve_agent) {
        this.request_menu_approve_agent = request_menu_approve_agent;
    }

    public void setRequest_menu_approve_customer(boolean request_menu_approve_customer) {
        this.request_menu_approve_customer = request_menu_approve_customer;
    }

    public void setRequest_menu_manage_cabang(boolean request_menu_manage_cabang) {
        this.request_menu_manage_cabang = request_menu_manage_cabang;
    }

    public void setRequest_menu_session_web(boolean request_menu_session_web) {
        this.request_menu_session_web = request_menu_session_web;
    }

    public void setRequest_menu_approve_cabang(boolean request_menu_approve_cabang) {
        this.request_menu_approve_cabang = request_menu_approve_cabang;
    }

    public void setRequest_sub_approveconfiguration_approvechange(boolean request_sub_approveconfiguration_approvechange) {
        this.request_sub_approveconfiguration_approvechange = request_sub_approveconfiguration_approvechange;
    }

    public void setRequest_sub_approveconfiguration_rejectchange(boolean request_sub_approveconfiguration_rejectchange) {
        this.request_sub_approveconfiguration_rejectchange = request_sub_approveconfiguration_rejectchange;
    }

    public void setRequest_sub_approveuser_approvechange(boolean request_sub_approveuser_approvechange) {
        this.request_sub_approveuser_approvechange = request_sub_approveuser_approvechange;
    }

    public void setRequest_sub_approveuser_rejectchange(boolean request_sub_approveuser_rejectchange) {
        this.request_sub_approveuser_rejectchange = request_sub_approveuser_rejectchange;
    }

    public void setRequest_sub_manageconfiguration_editparameter(boolean request_sub_manageconfiguration_editparameter) {
        this.request_sub_manageconfiguration_editparameter = request_sub_manageconfiguration_editparameter;
    }

    public void setRequest_sub_managecustomer_createcustomer(boolean request_sub_managecustomer_createcustomer) {
        this.request_sub_managecustomer_createcustomer = request_sub_managecustomer_createcustomer;
    }

    public void setRequest_sub_managecustomer_editcustomer(boolean request_sub_managecustomer_editcustomer) {
        this.request_sub_managecustomer_editcustomer = request_sub_managecustomer_editcustomer;
    }

    public void setRequest_sub_managecustomer_removecustomer(boolean request_sub_managecustomer_removecustomer) {
        this.request_sub_managecustomer_removecustomer = request_sub_managecustomer_removecustomer;
    }

    public void setRequest_sub_manageuser_createuser(boolean request_sub_manageuser_createuser) {
        this.request_sub_manageuser_createuser = request_sub_manageuser_createuser;
    }

    public void setRequest_sub_manageuser_unlockuser(boolean request_sub_manageuser_unlockuser) {
        this.request_sub_manageuser_unlockuser = request_sub_manageuser_unlockuser;
    }

    public void setRequest_sub_manageuser_resetpassword(boolean request_sub_manageuser_resetpassword) {
        this.request_sub_manageuser_resetpassword = request_sub_manageuser_resetpassword;
    }

    public void setRequest_sub_manageuser_edit(boolean request_sub_manageuser_edit) {
        this.request_sub_manageuser_edit = request_sub_manageuser_edit;
    }

    public void setRequest_sub_manageuser_activeinactive(boolean request_sub_manageuser_activeinactive) {
        this.request_sub_manageuser_activeinactive = request_sub_manageuser_activeinactive;
    }

    public void setRequest_sub_manageuser_deleteuser(boolean request_sub_manageuser_deleteuser) {
        this.request_sub_manageuser_deleteuser = request_sub_manageuser_deleteuser;
    }

    public void setRequest_sub_managegroupmenuuser_creategroup(boolean request_sub_managegroupmenuuser_creategroup) {
        this.request_sub_managegroupmenuuser_creategroup = request_sub_managegroupmenuuser_creategroup;
    }

    public void setRequest_sub_managegroupmenuuser_editgroup(boolean request_sub_managegroupmenuuser_editgroup) {
        this.request_sub_managegroupmenuuser_editgroup = request_sub_managegroupmenuuser_editgroup;
    }

    public void setRequest_sub_manageconfigurationrc_editparameter(boolean request_sub_manageconfigurationrc_editparameter) {
        this.request_sub_manageconfigurationrc_editparameter = request_sub_manageconfigurationrc_editparameter;
    }

    public void setRequest_sub_manageagent_registeragent(boolean request_sub_manageagent_registeragent) {
        this.request_sub_manageagent_registeragent = request_sub_manageagent_registeragent;
    }

    public void setRequest_sub_manageagent_resendotp(boolean request_sub_manageagent_resendotp) {
        this.request_sub_manageagent_resendotp = request_sub_manageagent_resendotp;
    }

    public void setRequest_sub_manageagent_validasi(boolean request_sub_manageagent_validasi) {
        this.request_sub_manageagent_validasi = request_sub_manageagent_validasi;
    }

    public void setRequest_sub_manageagent_blockunblock(boolean request_sub_manageagent_blockunblock) {
        this.request_sub_manageagent_blockunblock = request_sub_manageagent_blockunblock;
    }

    public void setRequest_sub_manageagent_resetpassword(boolean request_sub_manageagent_resetpassword) {
        this.request_sub_manageagent_resetpassword = request_sub_manageagent_resetpassword;
    }

    public void setRequest_sub_approveagent_approvechange(boolean request_sub_approveagent_approvechange) {
        this.request_sub_approveagent_approvechange = request_sub_approveagent_approvechange;
    }

    public void setRequest_sub_approveagent_rejectchange(boolean request_sub_approveagent_rejectchange) {
        this.request_sub_approveagent_rejectchange = request_sub_approveagent_rejectchange;
    }

    public void setRequest_sub_manageagent_validasiotp(boolean request_sub_manageagent_validasiotp) {
        this.request_sub_manageagent_validasiotp = request_sub_manageagent_validasiotp;
    }

    public void setRequest_sub_manageagent_delete(boolean request_sub_manageagent_delete) {
        this.request_sub_manageagent_delete = request_sub_manageagent_delete;
    }

    public void setRequest_sub_managecabang_create(boolean request_sub_managecabang_create) {
        this.request_sub_managecabang_create = request_sub_managecabang_create;
    }

    public void setRequest_sub_managecabang_update(boolean request_sub_managecabang_update) {
        this.request_sub_managecabang_update = request_sub_managecabang_update;
    }

    public void setRequest_sub_managecabang_delete(boolean request_sub_managecabang_delete) {
        this.request_sub_managecabang_delete = request_sub_managecabang_delete;
    }

    public void setRequest_sub_approvecustomer_approve(boolean request_sub_approvecustomer_approve) {
        this.request_sub_approvecustomer_approve = request_sub_approvecustomer_approve;
    }

    public void setRequest_sub_approvecustomer_reject(boolean request_sub_approvecustomer_reject) {
        this.request_sub_approvecustomer_reject = request_sub_approvecustomer_reject;
    }

    public void setRequest_sub_managecustomer_blockunblock(boolean request_sub_managecustomer_blockunblock) {
        this.request_sub_managecustomer_blockunblock = request_sub_managecustomer_blockunblock;
    }

    public void setRequest_sub_approvecabang_approve(boolean request_sub_approvecabang_approve) {
        this.request_sub_approvecabang_approve = request_sub_approvecabang_approve;
    }

    public void setRequest_sub_approvecabang_reject(boolean request_sub_approvecabang_reject) {
        this.request_sub_approvecabang_reject = request_sub_approvecabang_reject;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus_approval(int status_approval) {
        this.status_approval = status_approval;
    }

    public void setRequest_value(String request_value) {
        this.request_value = request_value;
    }

    public void setMenu_approve_group_menu(boolean menu_approve_group_menu) {
        this.menu_approve_group_menu = menu_approve_group_menu;
    }

    public void setSub_approvegroupmenu_approve(boolean sub_approvegroupmenu_approve) {
        this.sub_approvegroupmenu_approve = sub_approvegroupmenu_approve;
    }

    public void setSub_approvegroupmenu_reject(boolean sub_approvegroupmenu_reject) {
        this.sub_approvegroupmenu_reject = sub_approvegroupmenu_reject;
    }

    public void setMenu_performance_agent(boolean menu_performance_agent) {
        this.menu_performance_agent = menu_performance_agent;
    }

    public void setMenu_performance_customer(boolean menu_performance_customer) {
        this.menu_performance_customer = menu_performance_customer;
    }

    public void setMenu_performance_cabang(boolean menu_performance_cabang) {
        this.menu_performance_cabang = menu_performance_cabang;
    }

    public void setRequest_menu_approve_group_menu(boolean request_menu_approve_group_menu) {
        this.request_menu_approve_group_menu = request_menu_approve_group_menu;
    }

    public void setRequest_sub_approvegroupmenu_approve(boolean request_sub_approvegroupmenu_approve) {
        this.request_sub_approvegroupmenu_approve = request_sub_approvegroupmenu_approve;
    }

    public void setRequest_sub_approvegroupmenu_reject(boolean request_sub_approvegroupmenu_reject) {
        this.request_sub_approvegroupmenu_reject = request_sub_approvegroupmenu_reject;
    }

    public void setRequest_menu_performance_agent(boolean request_menu_performance_agent) {
        this.request_menu_performance_agent = request_menu_performance_agent;
    }

    public void setRequest_menu_performance_customer(boolean request_menu_performance_customer) {
        this.request_menu_performance_customer = request_menu_performance_customer;
    }

    public void setRequest_menu_performance_cabang(boolean request_menu_performance_cabang) {
        this.request_menu_performance_cabang = request_menu_performance_cabang;
    }

    public void setMenu_manage_product(boolean menu_manage_product) {
        this.menu_manage_product = menu_manage_product;
    }

    public void setSub_managecustomer_editphone(boolean sub_managecustomer_editphone) {
        this.sub_managecustomer_editphone = sub_managecustomer_editphone;
    }

    public void setSub_manageproduct_editparameter(boolean sub_manageproduct_editparameter) {
        this.sub_manageproduct_editparameter = sub_manageproduct_editparameter;
    }

    public void setSub_manageagent_editnpwp(boolean sub_manageagent_editnpwp) {
        this.sub_manageagent_editnpwp = sub_manageagent_editnpwp;
    }

    public void setSub_manageagent_editphone(boolean sub_manageagent_editphone) {
        this.sub_manageagent_editphone = sub_manageagent_editphone;
    }

    public void setRequest_menu_manage_product(boolean request_menu_manage_product) {
        this.request_menu_manage_product = request_menu_manage_product;
    }

    public void setRequest_sub_manageproduct_editparameter(boolean request_sub_manageproduct_editparameter) {
        this.request_sub_manageproduct_editparameter = request_sub_manageproduct_editparameter;
    }

    public void setRequest_sub_manageagent_editnpwp(boolean request_sub_manageagent_editnpwp) {
        this.request_sub_manageagent_editnpwp = request_sub_manageagent_editnpwp;
    }

    public void setRequest_sub_manageagent_editphone(boolean request_sub_manageagent_editphone) {
        this.request_sub_manageagent_editphone = request_sub_manageagent_editphone;
    }

    public void setRequest_sub_managecustomer_editphone(boolean request_sub_managecustomer_editphone) {
        this.request_sub_managecustomer_editphone = request_sub_managecustomer_editphone;
    }

}
