/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Andri D. Septian
 */
public interface NotificationTrxRepository extends JpaRepository<MNotifikasiTrx, Long> {
    
    List<MNotifikasiTrx> findTop100ByAgentOrderByCrTimeDesc(MAgent agent);
    List<MNotifikasiTrx> findTop5ByAgentOrderByCrTimeDesc(MAgent agent);
    
    MNotifikasiTrx findFirstByOriginReff(String resi);
    
    MNotifikasiTrx findFirstByBankReff(String BankReff);

    MNotifikasiTrx findByOriginReffAndStanAndOtp(String rrn, String stan, String otp);

    MNotifikasiTrx findFirstByTypeAndDescriptionNotAndAgentAndNasabahOrderByCrTimeDesc(Constants.TRX_TYPE type, String description, MAgent agent, MNasabah nasabah);

    MNotifikasiTrx findFirstByTypeAndAgentAndNasabahAndOtpOrderByCrTimeDesc(Constants.TRX_TYPE type, MAgent agent, MNasabah nasabah, String otp);

    MNotifikasiTrx findFirstByTypeAndAgentAndOtpOrderByCrTimeDesc(Constants.TRX_TYPE type, MAgent agent, String otp);

    @Query("SELECT t From MNotifikasiTrx t WHERE t.crTime between :startDate and :endDate and t.agent = :agent ORDER BY t.crTime")
    List<MNotifikasiTrx> mutasiTransaksiAgent(
            @Param("startDate") LocalDateTime startDate,
            @Param("endDate") LocalDateTime endDate,
            @Param("agent") MAgent agent
    );

    @Query("SELECT t From MNotifikasiTrx t WHERE t.crTime BETWEEN :startDate AND :endDate AND t.agent = :agent AND t.type = :type and t.status = :status AND t.description != :description ORDER BY t.crTime DESC")
    Page<MNotifikasiTrx> mutasiTransaksiAgentWithPaging(
            @Param("startDate") LocalDateTime startDate,
            @Param("endDate") LocalDateTime endDate,
            @Param("agent") MAgent agent,
            @Param("type") Constants.TRX_TYPE type,
            @Param("status") Constants.TRX_STATUS status,
            @Param("page") Pageable pageablex,
            @Param("description") String decription
    );

//    ========================== query notifikasi ============================
    @Query("SELECT t From MNotifikasiTrx t WHERE t.agent = :agent AND t.type = :type and t.status = :status ORDER BY t.crTime DESC")
    List<MNotifikasiTrx> notifikasiTanpaPaging(
            @Param("agent") MAgent agent,
            @Param("type") Constants.TRX_TYPE type,
            @Param("status") Constants.TRX_STATUS status
    );

//    bisaaa
    @Query("SELECT t From MNotifikasiTrx t WHERE t.agent = :agent ORDER BY t.crTime DESC")
    List<MNotifikasiTrx> notifikasiAgen(
            @Param("agent") MAgent agent
    );

//    @Query("SELECT t From MNotifikasiTrx t WHERE t.agent = :agent ORDER BY t.crTime DESC LIMIT 100")
//    Page<MNotifikasiTrx> notifikasiTransaksiAgent(
//            @Param("agent") MAgent agent
//    );
//    @Query("SELECT t From MNotifikasiTrx t WHERE t.agent = :agent ORDER BY t.crTime DESC LIMIT 100")
//    Page<MNotifikasiTrx> notifikasiTransaksiAgent(
//            @Param("agent") MAgent agent
//    );
    @Query("SELECT t From MNotifikasiTrx t WHERE t.crTime between :startDate and :endDate and t.agent = :agent ORDER BY t.crTime")
    List<MNotifikasiTrx> pagingMutasiTransaksiAgent(
            @Param("startDate") LocalDateTime startDate,
            @Param("endDate") LocalDateTime endDate,
            @Param("agent") MAgent agent
    );

    @Query("SELECT t FROM MNotifikasiTrx t WHERE t.type = :trxType AND t.description <> :trxDescription AND t.agent = :agent AND t.nasabah = :nasabah ORDER BY t.crTime")
    List<MNotifikasiTrx> findLastTransaction(
            @Param("trxType") Constants.TRX_TYPE trxType,
            @Param("trxDescription") String trxDescription,
            @Param("agent") MAgent agent,
            @Param("nasabah") MNasabah nasabah);

//     Calendar cal = Calendar.getInstance();
//            cal.set(Calendar.DAY_OF_MONTH, 1);
//            cal.set(Calendar.HOUR_OF_DAY, 0);
//            cal.clear(Calendar.MINUTE);
//            cal.clear(Calendar.SECOND);
//            cal.clear(Calendar.MILLISECOND);
//            Date startDate = cal.getTime();
//            Date endDate = new Date();
//            Object hasil = em.createQuery("SELECT SUM(t.amount) FROM Transaction t WHERE t.accountNumber = :accountNumber and t.createTime BETWEEN :startDate and :endDate and t.transactionType = :type and t.status = :status and t.description = :desc")
//                    .setParameter("accountNumber", accountNumber)
//                    .setParameter("startDate", startDate)
//                    .setParameter("endDate", endDate)
//                    .setParameter("type", Constants.TransactionType.POSTING)
//                    .setParameter("status", Constants.TransactionStatus.SUCCESS)
//                    .setParameter("desc", desc)
//                    .getSingleResult();
//    double sumTotalAmountByNasabahAndTypeAndStatusAndDescriptionAndCrTimeBetween(MNasabah nasabah, Constants.TRX_TYPE type, Constants.TRX_STATUS status, LocalDateTime startDate,LocalDateTime endTime);
//    @Query("SELECT SUM(t.amount) FROM Transaction t WHERE t.nasabah = :nasabah and t.createTime BETWEEN :startDate and :endDate and t.transactionType = :type and t.status = :status and t.description = :desc")
//    double findTotalAmount(
//            @Param("nasabah") MNasabah nasabah;
}
