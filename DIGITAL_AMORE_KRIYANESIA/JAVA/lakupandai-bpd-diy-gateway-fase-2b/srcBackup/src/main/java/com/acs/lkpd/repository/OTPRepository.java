/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MOtp;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ACS
 */
public interface OTPRepository extends JpaRepository<MOtp, Long> {

    MOtp findFirstByMarkerAndOtpOrderByCrTimeDesc(String marker, String otp);

    MOtp findFirstByMarkerOrderByCrTimeDesc(String marker);

    MOtp findFirstByOtpOrderByCrTimeDesc(String otp);
    
    MOtp findFirstByAgentOrderByCrTimeDesc(String agent);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update MOtp otp set otp.status =:status where otp =:otp")
    int updateOtpStatus(@Param("otp") MOtp otp, @Param("status") Constants.OtpStatus status);

}
