package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "trx_logger")
public class MTransaction implements Serializable {

    public MTransaction() {
        status = Constants.TRX_STATUS.FAILED;
        stan = Utility.generateStan();
    }

    private Long id;
    private String originReff;
    private String stan;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private Constants.TRX_STATUS status;
    private Constants.TRX_TYPE type;
    private MAgent agent;
    private MNasabah nasabah;
    private String origin;
    private String otp;
    private double amount;
    private double feeAgent;
    private double feeBank;
    private double feePajak;
    private double totalAmount;
    private String info;
    private String description;
    private String additionalData;
    private String productCode;
    private String accountType;
    private String bankReff;
    private String idBilling;
    private String smsResponse;
    private String smsInfo;
    private String mobileResponse;
    private String bankRc;
    private String bankResponse;
    private String bankResponseDetail;
    private String agentName;
    private Boolean isRead;
    
    
    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "trx_seq")
    @SequenceGenerator(name = "trx_seq", sequenceName = "trx_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.TRX_STATUS getStatus() {
        return status;
    }

    @Column(name = "origin_reff", nullable = true, length = 100)
    public String getOriginReff() {
        return originReff;
    }

    @Column(name = "stan", nullable = false, length = 20)
    public String getStan() {
        return stan;
    }

    @Column(name = "origin")
    public String getOrigin() {
        return origin;
    }

    @Column(name = "otp")
    public String getOtp() {
        return otp;
    }

    @Column(name = "amount")
    public double getAmount() {
        return amount;
    }

    @Column(name = "fee_agent")
    public double getFeeAgent() {
        return feeAgent;
    }

    @Column(name = "fee_bank")
    public double getFeeBank() {
        return feeBank;
    }
    
    @Column(name = "fee_pajak")
    public double getFeePajak() {
        return feePajak;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MAgent.class)
    @JoinColumn(name = "agent")
    public MAgent getAgent() {
        return agent;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MNasabah.class)
    @JoinColumn(name = "nasabah")
    public MNasabah getNasabah() {
        return nasabah;
    }

    @Column(name = "info")
    @Type(type = "text")
    public String getInfo() {
        return info;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    @Column(name = "additional_data")
    @Type(type = "text")
    public String getAdditionalData() {
        return additionalData;
    }

    @Column(name = "total_amount")
    public double getTotalAmount() {
        return totalAmount;
    }

    @Column(name = "product_code")
    public String getProductCode() {
        return productCode;
    }

    @Column(name = "account_type")
    public String getAccountType() {
        return accountType;
    }

    @Column(name = "id_billing", length = 25)
    public String getIdBilling() {
        return idBilling;
    }

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    public Constants.TRX_TYPE getType() {
        return type;
    }

    @Column(name = "sms_response", length = 255)
    public String getSmsResponse() {
        return smsResponse;
    }

    @Column(name = "sms_info")
    public String getSmsInfo() {
        return smsInfo;
    }

    @Column(name = "mobile_response")
    @Type(type = "text")
    public String getMobileResponse() {
        return mobileResponse;
    }

    public String viewStatus() {
        switch (status) {
            case SUCCESS:
                return "Sukses";
            case FAILED:
                return "Gagal";
            case PENDING:
                return "Pending";
            case PROCESS:
//                return "Transaksi Sedang dalam Proses, Cek Saldo Anda";
                return "Sukses";
            default:
                return "Pending";
        }
    }

    @Column(name = "bank_reff", length = 20)
    public String getBankReff() {
        return bankReff;
    }

    @Column(name = "bank_rc")
    public String getBankRc() {
        return bankRc;
    }

    @Column(name = "bank_response")
    public String getBankResponse() {
        return bankResponse;
    }
    
    @Column(name = "bank_response_detail")
    @Type(type = "text")
    public String getBankResponseDetail() {
        return bankResponseDetail;
    }
    
    
    @Column(name = "nama_agent")
    @Type(type = "text")
    public String getAgentName() {
        return agentName;
    }
    
    @Column(name = "isRead")
    public Boolean getIsRead() {
        return isRead;
    }
    

    //=============SET=============//
    public void setId(Long id) {
        this.id = id;
    }

    public void setOriginReff(String originReff) {
        this.originReff = originReff;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setAgent(MAgent agent) {
        this.agent = agent;
    }

    public void setNasabah(MNasabah nasabah) {
        this.nasabah = nasabah;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setFeeAgent(double feeAgent) {
        this.feeAgent = feeAgent;
    }

    public void setFeeBank(double feeBank) {
        this.feeBank = feeBank;
    }

    public void setFeePajak(double feePajak) {
        this.feePajak = feePajak;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setStatus(Constants.TRX_STATUS status) {
        this.status = status;
    }

    public void setType(Constants.TRX_TYPE type) {
        this.type = type;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public void setBankReff(String bankReff) {
        this.bankReff = bankReff;
    }

    public void setIdBilling(String idBilling) {
        this.idBilling = idBilling;
    }

    public void setSmsResponse(String smsResponse) {
        this.smsResponse = smsResponse;
    }

    public void setMobileResponse(String mobileResponse) {
        this.mobileResponse = mobileResponse;
    }

    public void setSmsInfo(String smsInfo) {
        this.smsInfo = smsInfo;
    }

    public void setBankRc(String bankRc) {
        this.bankRc = bankRc;
    }

    public void setBankResponse(String bankResponse) {
        this.bankResponse = bankResponse;
    }
    
    public void setBankResponseDetail(String bankResponseDetail) {
        this.bankResponseDetail = bankResponseDetail;
    }
    
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }
}
