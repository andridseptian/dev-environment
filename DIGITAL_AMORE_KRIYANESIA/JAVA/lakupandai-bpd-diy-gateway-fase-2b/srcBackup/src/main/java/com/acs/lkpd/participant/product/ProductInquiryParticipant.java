package com.acs.lkpd.participant.product;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class ProductInquiryParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final ProductRepository productRepository;

    public ProductInquiryParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        productRepository = context.getBean(ProductRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = null;
//        String findin = cfg.get("find_in", "iso|3");
        String findin = cfg.get("find_in");
        if (findin.equals("session")) {

            try {

//                log.info("CEK SESSION PRODUCT");
                product = session.getProduct();

//                log.info("CEK PRODUCT CODE");
//                ctx.log("INI PRODUCT CODE : " + product.getProductCode());
//                log.info("CEK PRODUCT CODE :" + product.getProductCode());

                if (product != null) {
                    switch (product.getStatus()) {
                        case ACTIVE: {
                            session.setProduct(product);
                            ctx.put(Constants.PRODUCT, product);
                            ctx.put(Constants.SESSION, session);

                            ctx.log("--Product Inquiry Participant--");
                            ctx.log("Product Code: " + product.getProductCode());
                            ctx.log("Product Name: " + product.getProductName());
                            ctx.log("Product Menu: " + product.getMenu());
                            ctx.log("Session Product Code: " + session.getProduct());

                            return PREPARED | NO_JOIN;
                        }
                        case INACTIVE: {
                            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_INACTIVE.toString()));
                            ctx.put(Constants.SESSION, session);
                            return ABORTED | NO_JOIN;
                        }
                        default:
                            ctx.log("DEFAULT CASE");
                            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
                            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_INACTIVE.toString()));
                            ctx.put(Constants.SESSION, session);
                            return ABORTED | NO_JOIN;
                    }
                } else {
                    session.setRcToClient(Constants.RC_FORMAT_ERROR);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_NOT_FOUND.toString()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
            } catch (Exception ex) {
                ctx.log("EXCEPTION: " + ex.getMessage());
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_NOT_FOUND.toString()));
                ctx.put(Constants.SESSION, session);
                log.error(ex);
                return ABORTED | NO_JOIN;
            }
        } else {
            String[] value = findin.toLowerCase().split("\\|");
            String by = value[0];
            String bit = "3";
            try {
                switch (by) {
                    case "iso": {
                        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
                        try {
                            bit = value[1];
                        } catch (ArrayIndexOutOfBoundsException aex) {
                            ctx.log("find product by default bit " + bit);
                        }
                        String productCode = isomsg.getString(bit);

                        product = productRepository.findOne(productCode);
                        ctx.log("PRODUCT CODE : " + productCode);
                        break;
                    }
                }
                if (product != null) {
                    switch (product.getStatus()) {
                        case ACTIVE: {
                            session.setProduct(product);
                            ctx.put(Constants.PRODUCT, product);
                            ctx.put(Constants.SESSION, session);

                            ctx.log("--Product Inquiry Participant--");
                            ctx.log("Product Code: " + product.getProductCode());
                            ctx.log("Product Name: " + product.getProductName());
                            ctx.log("Product Menu: " + product.getMenu());
                            ctx.log("Session Product Code: " + session.getProduct());

                            return PREPARED | NO_JOIN;
                        }
                        case INACTIVE: {
                            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_INACTIVE.toString()));
                            ctx.put(Constants.SESSION, session);
                            return ABORTED | NO_JOIN;
                        }
                        default:
                            ctx.log("masuk default case coy");
                            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
                            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_INACTIVE.toString()));
                            ctx.put(Constants.SESSION, session);
                            return ABORTED | NO_JOIN;
                    }
                } else {
                    session.setRcToClient(Constants.RC_FORMAT_ERROR);
                    session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_NOT_FOUND.toString()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                }
            } catch (Exception ex) {
                log.info("CEK SESSION PRODUCTxxxxxxxxxxxxx");
                ctx.log("CEK SESSION PRODUCTxxxxxxxxxxxxx");
                ctx.log("masuk exception case coy");
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_PRODUCT_NOT_FOUND.toString()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        }

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
