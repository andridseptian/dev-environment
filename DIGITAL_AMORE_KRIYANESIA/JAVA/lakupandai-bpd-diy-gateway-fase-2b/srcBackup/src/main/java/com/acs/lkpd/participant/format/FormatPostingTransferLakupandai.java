/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MProduct;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatPostingTransferLakupandai implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_POSTING_TRANSFER_LAKUPANDAI.name());
        try {
//            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
//                    .replace("[MSISDN]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
//                    .replace("[TargetMsisdn]", Utility.viewMsisdn(session.getTargetNasabahMsisdn()))
//                    .replace("[TargetName]", session.getTargetNasabahName());

            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[DestinationMsisdn]", Utility.viewMsisdn(session.getTargetNasabahAccountNumber()))
                            .replace("[DestinationName]", session.getTargetNasabahName())
                            .replace("[Amount]", "Rp." + Utility.formatAmount(session.getAmount()))
                            .replace("[FromMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                            .replace("[FromName]", session.getNasabah().getName())
                            .replace("[Desc]", Service.getResponseFromSettings(KeyMap.RM_MESSAGE_NOTIF.name()).replace("[Process]", "Transfer On Us")));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";

            }

//            response = response + Service.getResponseFromSettings(KeyMap.RM_MESSAGE_NOTIF.name()).replace("[Process]", "Transfer Lakupandai");
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (Exception ex) {
            ctx.log(ex);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            return ABORTED | NO_JOIN;
        }
    }

}
