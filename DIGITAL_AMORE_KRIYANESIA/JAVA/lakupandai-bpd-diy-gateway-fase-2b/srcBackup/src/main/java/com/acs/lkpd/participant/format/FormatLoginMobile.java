/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.repository.AgentRepository;
import java.io.File;
import java.io.Serializable;
import java.util.Base64;
import org.apache.commons.io.FileUtils;
import org.jpos.core.Configuration;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class FormatLoginMobile implements TransactionParticipant {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final AgentRepository agentRepository;

    public FormatLoginMobile() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        if (!isomsg.getString("62").isEmpty()) {
            String token = isomsg.getString("62");
            if (agent.getToken() == null || !agent.getToken().equals(token)) {
                agent.setToken(isomsg.getString("62"));
                agentRepository.save(agent);
            }
        }

        String formatResponse = "[\n"
                + "	{\"header\":\"Status\",\"value\":\"[StatusLogin]\"},\n"
                + "	{\"header\":\"Agent Id\",\"value\":\"[agentId]\"},\n"
                + "	{\"header\":\"Agent Name\",\"value\":\"[agentName]\"},\n"
                + "	{\"header\":\"Gambar1\",\"value\":\"[gambar1]\"},\n"
                + "	{\"header\":\"Gambar2\",\"value\":\"[gambar2]\"}\n"
                + "]";

        String agentId = agent.getId();
        String agentName = agent.getName();

        String encodedString1 = "-";
        String encodedString2 = "-";

        log.info("MENGAMBIL GAMBAR");

//        try {
//            byte[] fileContent1 = FileUtils.readFileToByteArray(new File("gambar/1.jpg"));
//            encodedString1 = Base64.getEncoder().encodeToString(fileContent1);
//            log.info(encodedString1);
//            byte[] fileContent2 = FileUtils.readFileToByteArray(new File("gambar/2.jpg"));
//            encodedString2 = Base64.getEncoder().encodeToString(fileContent2);
//            log.info(encodedString2);
//        } catch (Exception e) {
//            log.info("INI ADA ERROR" + e);
//        }

        JSONArray formatInquiryArray = new JSONArray(formatResponse);
        for (int i = 0; i < formatInquiryArray.length(); i++) {
            JSONObject data = formatInquiryArray.getJSONObject(i);

            data.put("value", data.getString("value")
                    .replace("[StatusLogin]", "Login Success")
                    .replace("[agentId]", agent.getId())
                    .replace("[agentName]", agent.getName())
                    .replace("[gambar1]", encodedString1)
                    .replace("[gambar2]", encodedString2)
            );

        }
        

//        JSONObject gambar = new JSONObject();
//        gambar.put(agentId, value)
        String responseMessage = new JSONObject().put("data", formatInquiryArray).toString();

        session.setRcToClient(Constants.RC_APPROVED);
//        session.setResponseToClient("Login Success");
        session.setResponseToClient(responseMessage);
        ctx.put(Constants.SESSION, session);
        log.info("SELESAIIIII");
        return PREPARED | NO_JOIN;
    }
}
