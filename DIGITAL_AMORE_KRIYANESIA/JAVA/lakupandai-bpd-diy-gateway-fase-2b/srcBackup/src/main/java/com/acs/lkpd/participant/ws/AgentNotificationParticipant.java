/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.NotificationRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentNotificationParticipant implements TransactionParticipant, Configurable {

    public Configuration cfg;
    private Log log = Log.getLog("Q2", "Service");
    private NotificationRepository notificationRepository;
    private AgentRepository agentRepository;

    public AgentNotificationParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationRepository = context.getBean(NotificationRepository.class);
        agentRepository = context.getBean(AgentRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        LogEvent le = new LogEvent("SendNotification");
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONArray arrayTo = null;
        String rrn = "";
        String hasil = "";
        JSONObject dataReq;
        try {
            dataReq = new JSONObject(req.getRequest());
            rrn = dataReq.getString("rrn");
            int value = notificationRepository.countByReqIdToday(rrn);
            if (value > 0) {
                String rm = "Notifikasi tidak dapat diproses, duplikasi rrn";
                req.setRm(rm);
                req.setInfo("duplikasi rrn " + rrn);
                req.setStatus(Constants.TRX_STATUS.FAILED);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        } catch (JSONException jre) {
            String rm = "Notifikasi tidak dapat diproses, format request error";
            req.setRm(rm);
            req.setInfo("format error");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        try {
            arrayTo = dataReq.getJSONArray("to");
            for (int i = 0; i < arrayTo.length(); i++) {
                MAgent user = agentRepository.findById(arrayTo.getString(i));
                if (user == null) {
                    hasil += ("|skip " + arrayTo.getString(i) + ", user not found");
                    continue;
                }
                if (user.getToken() == null || user.getToken().isEmpty()) {
                    hasil += ("|skip " + arrayTo.getString(i) + ", token not found");
                    continue;
                }
                log.info("user found:" + user.getId());
                MNotification notif = new MNotification();
                notif.setRrn(rrn);
                notif.setToken(user.getToken());
                notif.setAgent(user.getId());
                notif.setTitle(dataReq.getString("title"));
                notif.setMessage(dataReq.getString("message"));
                notif.setImage(dataReq.has("image") ? dataReq.getString("image") : "");
                Utility util = new Utility();
                boolean sendFcmMessage = util.sendFirebaseNotifFcm(notif.getToken(), notif.getTitle(), notif.getMessage());
                if (sendFcmMessage) {
                    notif.setStatus(Constants.FCM.STATUS.SUCCESS);
                } else {
                    notif.setStatus(Constants.FCM.STATUS.FAILED);
                }
                notificationRepository.save(notif);
            }
            req.setRm("success");
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            JSONObject resp = new JSONObject().put("log", hasil);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            ctx.put(Constants.OBJ.RESP, resp);
            log.info(le.toString());
            return PREPARED | NO_JOIN;
        } catch (JSONException jse) {
            try {
                if (dataReq.getString("to").equalsIgnoreCase("all")) {
                    MNotification notif = new MNotification();
                    notif.setRrn(rrn);
                    notif.setAgent("all");
                    notif.setToken("all");
                    notif.setTitle(dataReq.getString("title"));
                    notif.setMessage(dataReq.getString("message"));
                    notif.setImage(dataReq.has("image") ? dataReq.getString("image") : "");
                    notificationRepository.save(notif);
                    req.setRm("success");
                    req.setStatus(Constants.TRX_STATUS.SUCCESS);
                    JSONObject resp = new JSONObject().put("log", hasil);
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    ctx.put(Constants.OBJ.RESP, resp);
                    log.info(le.toString());
                    return PREPARED | NO_JOIN;
                }
            } catch (JSONException jsex) {
                String rm = "Notifikasi tidak dapat diproses, format request error";
                req.setRm(rm);
                req.setInfo("format error");
                req.setStatus(Constants.TRX_STATUS.FAILED);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            String rm = "Notifikasi tidak dapat diproses, format request error";
            req.setRm(rm);
            req.setInfo("format error");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
