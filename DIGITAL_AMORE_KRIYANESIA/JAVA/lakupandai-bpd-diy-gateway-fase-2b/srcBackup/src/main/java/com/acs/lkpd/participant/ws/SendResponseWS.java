package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.SettingRepository;
import com.acs.util.Service;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class SendResponseWS implements AbortParticipant {

    private Log log = Log.getLog("Q2", getClass().getName());
    private SettingRepository settingRepository;

    public SendResponseWS() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        settingRepository = context.getBean(SettingRepository.class);
    }

    @Override
    public int prepareForAbort(long id, Serializable context) {
        return ABORTED;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        long timeout = Long.parseLong(Service.findSettings(KeyMap.TIMEOUT_TRANSACTION.name()));
        log.info("send " + req.getId() + " to space");
        req.setResponseTime(LocalDateTime.now());
        req.setResponse(ctx.get(Constants.OBJ.RESP).toString());
        log.info("Set Response : " + req.getResponse());
        sp.out(req.getId(), ctx, timeout);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        long timeout = Long.parseLong(Service.findSettings(KeyMap.TIMEOUT_TRANSACTION.name()));
        log.info("send " + req.getId() + " to space");
        req.setResponseTime(LocalDateTime.now());
        req.setResponse(req.getRm());
        if (req.getRm().contains("REKENING SUDAH TERDAFTAR")) {
            req.setResponse(new JSONObject().put("rm",req.getRm()).toString());
        }
        log.info("Set Response : " + req.getResponse());
        sp.out(req.getId(), ctx, timeout);
    }

}
