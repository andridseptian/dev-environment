/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Andri D Septian
 */
public class FormatTanggal {

    public static String tanggalLokalFromCrTime(String CreateTime) throws ParseException {

        String crtime = String.valueOf(CreateTime);
        String tanggal = crtime.substring(0, 16).replace('T', ' ');
        Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(tanggal);
        String parsingDate = new SimpleDateFormat("dd-MM-yyyy hh:m").format(date);

        parsingDate = parsingDate
                .replace("January", "Januari")
                .replace("February", "Februari")
                .replace("March", "Maret")
                .replace("April", "April")
                .replace("May", "Mei")
                .replace("June", "Juni")
                .replace("July", "Juli")
                .replace("August", "Agustus")
                .replace("September", "September")
                .replace("October", "Oktober")
                .replace("November", "November")
                .replace("December", "Desember");

        return parsingDate;

    }
    
    public static String tanggalLokalFromCrTimeToFullMonth(String CreateTime) throws ParseException {

        String crtime = String.valueOf(CreateTime);
        String tanggal = crtime.substring(0, 16).replace('T', ' ');
        Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(tanggal);
        String parsingDate = new SimpleDateFormat("dd MMMMM YYYY HH:mm").format(date);

        parsingDate = parsingDate
                .replace("January", "Januari")
                .replace("February", "Februari")
                .replace("March", "Maret")
                .replace("April", "April")
                .replace("May", "Mei")
                .replace("June", "Juni")
                .replace("July", "Juli")
                .replace("August", "Agustus")
                .replace("September", "September")
                .replace("October", "Oktober")
                .replace("November", "November")
                .replace("December", "Desember");

        return parsingDate;

    }

    public static String tanggalLokal(String parsingDate) {

        parsingDate = parsingDate
                .replace("January", "Januari")
                .replace("February", "Februari")
                .replace("March", "Maret")
                .replace("April", "April")
                .replace("May", "Mei")
                .replace("June", "Juni")
                .replace("July", "Juli")
                .replace("August", "Agustus")
                .replace("September", "September")
                .replace("October", "Oktober")
                .replace("November", "November")
                .replace("December", "Desember");

        return parsingDate;

    }

    public static String tanggalLokalNow() {

        SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
        String parsingDate = sdfFormatPayment.format(new Date());

        parsingDate = parsingDate
                .replace("January", "Januari")
                .replace("February", "Februari")
                .replace("March", "Maret")
                .replace("April", "April")
                .replace("May", "Mei")
                .replace("June", "Juni")
                .replace("July", "Juli")
                .replace("August", "Agustus")
                .replace("September", "September")
                .replace("October", "Oktober")
                .replace("November", "November")
                .replace("December", "Desember");

        return parsingDate;

    }
}
