/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_otp")
public class MOtp implements Serializable {

    public MOtp() {
    }

    public MOtp(String marker) {
        this.marker = marker;
        this.otp = Utility.generateStan();
        this.status = Constants.OtpStatus.ACTIVE;
    }

    private Long id;
    private LocalDateTime crTime;
    private LocalDateTime endTime;
    private String marker;
    private String otp;
    private Constants.OtpStatus status;
    private String product;
    private String agent;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
        long longExpiredOtp;
        try {
            longExpiredOtp = Long.parseLong(Service.getResponseFromSettings(KeyMap.TIMEOUT_OTP.name()));
        } catch (NullPointerException | NumberFormatException nfe) {
            longExpiredOtp = 60;
        }
        LocalDateTime expiredTime = crTime.plusSeconds(longExpiredOtp);
        this.endTime = expiredTime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "otp_seq")
    @SequenceGenerator(name = "otp_seq", sequenceName = "otp_seq", allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "marker", nullable = false)
    public String getMarker() {
        return marker;
    }

    @Column(name = "otp")
    public String getOtp() {
        return otp;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.OtpStatus getStatus() {
        return status;
    }

    @Column(name = "end_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getEndTime() {
        return endTime;
    }

    @Column(name = "product")
    public String getProduct() {
        return product;
    }
    
    @Column(name = "agent", length = 10)
    public String getAgent() {
        return agent;
    }

    //============ SET ==============//
    public void setId(Long id) {
        this.id = id;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setStatus(Constants.OtpStatus status) {
        this.status = status;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }
    
    

}
