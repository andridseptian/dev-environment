package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class ChangeMsisdnProcess implements TransactionParticipant, Configurable {

    private RcRepository rcRepository;
    private AgentRepository agentRepository;
    private NasabahRepository nasabahRepository;
    protected Configuration cfg;

    public ChangeMsisdnProcess() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
        nasabahRepository = context.getBean(NasabahRepository.class);
        rcRepository = context.getBean(RcRepository.class);
    }

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        String type = cfg.get("type", "agent");
        String oldMsisdn = "", newMsisdn = "";
        char typeUser = 'A';
        switch (type) {
            case "agent":
            default:
                MAgent agent = (MAgent) ctx.get(Constants.AGENT);
                oldMsisdn = Utility.formatMsisdn(agent.getMsisdn());
                newMsisdn = Utility.formatMsisdn(agent.getResetMsisdn());
                typeUser = 'A';
                break;
            case "nasabah":
            case "customer":
                MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
                oldMsisdn = Utility.formatMsisdn(nasabah.getMsisdn());
                newMsisdn = Utility.formatMsisdn(nasabah.getResetMsisdn());
                typeUser = 'C';
                break;
        }
        JSONObject hasil = Service.serviceUpdateNoHP(oldMsisdn, newMsisdn, typeUser);
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            }
            ctx.log("Check " + Constants.SERVICE_UPDATE_NO_HP + " " + rc);
//            MRc mapRc = rcRepository.findByProcessCodeAndRc(ctx.getString(Constants.SERVICE_UPDATE_NO_HP), rc);
            ctx.log(cfg);
            try {
//                MRc mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_UPDATE_NO_HP, rc);

                req.setRm(hasil.getString(Constants.PARAM.WS_RESPONSECODE));
                if (hasil.getString(Constants.PARAM.WS_RESPONSECODE).equals("00")) {

                    req.setStatus(Constants.TRX_STATUS.SUCCESS);
                    log.info("RESULT AVAILABLE");
                    ctx.put(Constants.OBJ.WSREQUEST, req);

                    ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Change MSISDN Success"));

                    switch (type) {
                        case "agent":
                        default:
                            MAgent agent = (MAgent) ctx.get(Constants.AGENT);
                            oldMsisdn = Utility.formatMsisdn(agent.getMsisdn());
                            newMsisdn = Utility.formatMsisdn(agent.getResetMsisdn());

                            log.info("SAVE AGENT DATA" + "\n"
                                    + "agent: " + agent.getId() + "\n"
                                    + "old_msisdn: " + oldMsisdn + "\n"
                                    + "new_msisdn: " + newMsisdn + "\n"
                            );
                            
                            /* save data conflict with web */
                            agent.setMsisdn(newMsisdn);
//                            agent.setResetMsisdn(null);
                            agent.setStatusApproval(0);
                            agentRepository.save(agent);
                            typeUser = 'A';
                            break;
                        case "nasabah":
                        case "customer":
                            MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
                            oldMsisdn = Utility.formatMsisdn(nasabah.getMsisdn());
                            newMsisdn = Utility.formatMsisdn(nasabah.getResetMsisdn());

                            log.info("SAVE NASABAH DATA" + "\n"
                                    + "nasabah: " + nasabah.getId() + "\n"
                                    + "old_msisdn: " + oldMsisdn + "\n"
                                    + "new_msisdn: " + newMsisdn + "\n"
                            );

                            nasabah.setMsisdn(newMsisdn);
//                            nasabah.setResetMsisdn(null);
                            nasabah.setStatus_approval(0);
                            nasabahRepository.save(nasabah);
                            typeUser = 'C';
                            break;
                    }

                    return PREPARED | NO_JOIN;
                } else {
                    req.setStatus(Constants.TRX_STATUS.FAILED);
                    req.setInfo(rm);

                    log.info("RESULT FAILED");
                    ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Change MSISDN Failed"));
                    return ABORTED | NO_JOIN;
                }
            } catch (Exception ex) {
                log.info("RESULT EXCEPTION");
                log.info(ExceptionUtils.getStackTrace(ex));
//                 if (mapRc == null) {
                req.setRm(rc + " | " + rm);
                req.setStatus(Constants.TRX_STATUS.FAILED);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Change MSISDN Failed"));
                return ABORTED | NO_JOIN;
//                }

            }

        } else {
            log.info("WS RESPONSE CODE NOT FOUND");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Change MSISDN Failed"));
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
