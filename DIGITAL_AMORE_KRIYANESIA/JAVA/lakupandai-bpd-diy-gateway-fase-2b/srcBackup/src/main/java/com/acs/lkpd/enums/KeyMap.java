/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.enums;

/**
 *
 * @author ACS Load all Data from MSetting
 */
public enum KeyMap {

    TIMEOUT_TRANSACTION, //60
    TIMEOUT_OTP, //60
    DEFAULT_TIMEOUT_OTP, //60000
    TIMEOUT_DRIVER_REQUEST, //60000
    RM_SELL_TRANSACTION,
    TIMEOUT_SESSION, //600000 = 10 menit
    MAX_LIMIT_QUOTA, //MAX limit QUOTA = 5jt
    RM_BANK_RESPONSE_ERROR,
    RM_AGENT_NOT_FOUND,
    RM_RC_NOT_FOUND,
    RM_OTP_AGENT,
    RM_PASS_AGENT,
    RM_OTP_NASABAH,
    RM_OTP_EXP_NOT_FOUND,
    RM_OTP_TRANSACTION_NOT_FOUND,
    RM_POSTING_TARIK_TUNAI,
    FEE_AGENT_TARIK_TUNAI,
    FEE_BANK_TARIK_TUNAI,
    FEE_AGENT_SETOR_TUNAI,
    FEE_BANK_SETOR_TUNAI,
    FEE_AGENT_TRANSFER_ON_US,
    FEE_BANK_TRANSFER_ON_US,
    FEE_AGENT_TRANSFER_LAKUPANDAI,
    FEE_BANK_TRANSFER_LAKUPANDAI,
    CHARGE_AMOUNT_TARIK_TUNAI,
    CHARGE_AMOUNT_SETOR_TUNAI,
    CHARGE_AMOUNT_TRANSFER_ON_US,
    RM_INQUIRY_SETOR_TUNAI,
    RM_INVALID_REQUEST,
    RM_POSTING_SETOR_TUNAI,
    RM_MESSAGE_NOTIF,
    RM_INQUIRY_TRANSFER_ON_US,
    RM_INQUIRY_TRANSFER_LAKUPANDAI,
    RM_INQUIRY_TELKOMSEL_POSTPAID,
    RM_POSTING_TELKOMSEL_POSTPAID,
    DESC_INQ_PREPAID,
    DESC_INQ_PREPAID_AGEN,
    RM_MESSAGE_TOKEN,
    RM_TRANSACTION_NOT_FOUND,
    RM_INQUIRY_TOKEN_TRANSFER_ON_US,
    RM_INQUIRY_TOKEN_TRANSFER_LAKUPANDAI,
    RM_FORMAT_POSTING_TRANSFER_ON_US,
    RM_FORMAT_POSTING_TRANSFER_LAKUPANDAI,
    RM_FORMAT_SMS_ADD_SALDO,
    RM_AGENT_CABANG_NOT_FOUND,
    IP_DESTINATION,
    URL_AGENT_INQUIRY,
    URL_AGENT_REGISTRATION,
    RM_AGENT_BLOCK,
    RM_AGENT_REJECTED,
    RM_AGENT_INACTIVE,
    RM_AGENT_NEED_APPROVAL,
    RM_AGENT_REGISTRATION_ACTIVE,
    RM_AGENT_REGISTRATION_INACTIVE,
    RM_AGENT_REGISTRATION_BLOCK,
    RM_AGENT_REGISTRATION_NEED_APPROVAL_CS,
    RM_AGENT_REGISTRATION_REJECTED,
    RM_AGENT_REGISTRATION_NOT_FOUND,
    URL_AGENT_LOGIN,
    URL_CUSTOMER_INQUIRY,
    URL_CUSTOMER_REGISTRATION,
    URL_BALANCE_INQUIRY,
    URL_ACCOUNT_TRANSACTION,
    URL_ACCOUNT_INQUIRY,
    URL_PAYMENT,
    URL_CHANGE_MSISDN,
    SERVICE_PASSWORD,
    CHANNEL_ID,
    DES_KEY,
    RM_CUSTOMER_BLOCK,
    RM_CUSTOMER_INACTIVE,
    RM_CUSTOMER_OVERLIMIT,
    RM_CUSTOMER_NOT_FOUND,
    RM_CUSTOMER_DUPLICATE,
    RM_CUSTOMER_REJECTED,
    RM_CUSTOMER_NIK_DUPLICATE,
    RM_CUSTOMER_NEED_APPROVAL,
    RM_CUSTOMER_REGISTRATION_SUCCESS,
    RM_TOKEN_TRANSFER_LAKUPANDAI,
    RM_TOKEN_TRANSFER_ON_US,
    RM_FORMAT_SMS_NEWPASSWORD,
    RM_FORMAT_SMS_REGISTRASI_CUSTOMER,
    RM_FORMAT_SMS_POSTING_TARIK_TUNAI,
    RM_FORMAT_SMS_POSTING_SETOR_TUNAI,
    RM_FORMAT_SMS_POSTING_TRANSFER_ON_US,
    RM_FORMAT_SMS_POSTING_TRANSFER_LAKUPANDAI,
    RM_EMAIL_REGISTRATION_FAILED,
    RM_FORMAT_EMAIL_REGISTRATION,
    RM_FORMAT_EMAIL_TITLE_REGISTRATION,
    RM_FORMAT_EMAIL_AGENT_RESETPIN,
    RM_FORMAT_EMAIL_WEB_APPROVAL,
    RM_FORMAT_EMAIL_TITLE_WEB_APPROVAL,
    RM_FORMAT_EMAIL_TITLE_AGENT_RESETPIN,
    MAX_FAIL_LOGIN_AGENT,
    RM_AGENT_UNKNOWN_DEVICE,
    HOST_SMS,
    HOST_NOTIFICATION,
    NOTIFICATION_ALL,
    //FASE 2
    MAX_DAYS_MUTASI, //30
    MAX_DAYS_NOTIFICATION, //30
    RM_PRODUCT_INACTIVE,
    RM_PRODUCT_NOT_FOUND,
    RM_FORMAT_TRANSAKSI_TERAKHIR_TARIK_TUNAI,
    RM_FORMAT_TRANSAKSI_TERAKHIR_SETOR_TUNAI,
    RM_FORMAT_TRANSAKSI_TERAKHIR_TRANSFER_ON_US,
    RM_FORMAT_TRANSAKSI_TERAKHIR_TRANSFER_LAKUPANDAI
}
