/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MSession;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configuration;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author ACS
 */
public class FormatEmailRegistrasiNasabah implements TransactionParticipant {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_EMAIL_REGISTRATION.name());
        String title = Service.getResponseFromSettings(KeyMap.RM_FORMAT_EMAIL_TITLE_REGISTRATION.name());
        try {
            if (nasabah.getCabang().getEmail() == null || nasabah.getCabang().getEmail().equals("")) {
                session.setDetail(session.getDetail() + "Email Kantor Cabang tidak ditemukan");
            }
            response = response.replace("[OfficeName]", nasabah.getCabang().getNama())
                    .replace("[AgentName]", nasabah.getAgentId().getName())
                    .replace("[AgentAddress]", nasabah.getAgentId().getAddress())
                    .replace("[CustomerAccount]", nasabah.getAccountNumber());
            if (Service.sendEmail(response, title, nasabah.getCabang().getEmail())) {
                session.setDetail(session.getDetail() + "Send Email to " + nasabah.getCabang().getEmail() + " success");
            } else {
                session.setDetail(session.getDetail() + "Send Email to " + nasabah.getCabang().getEmail() + " failed");
            }
        } catch (Exception ex) {
            log.info(ex);
            ctx.log(ex.getMessage());
            session.setDetail("Send Email failed [" + ex.getMessage() + "]");
//            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
//            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
        }
        ctx.put(Constants.SESSION, session);
    }

}
