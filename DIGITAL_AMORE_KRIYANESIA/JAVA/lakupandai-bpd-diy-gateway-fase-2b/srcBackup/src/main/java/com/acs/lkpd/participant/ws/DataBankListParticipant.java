package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MDataBank;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.DataBankRepository;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class DataBankListParticipant implements TransactionParticipant {

    private final DataBankRepository dataBankRepository;

    public DataBankListParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        dataBankRepository = context.getBean(DataBankRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        List<MDataBank> listData = dataBankRepository.findByStatusOrderByPriorityAscBankNameAsc(1);
        if (null == listData || listData.isEmpty()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("list data bank tidak tersedia");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        JSONArray data;
        try {
             data = new JSONArray(
                    listData.stream()
                            .map(d -> new JSONObject()
                            .put("bankCode", d.getBankCode())
                            .put("bankName", d.getBankName()))
                            .collect(Collectors.toList()));
        } catch (Exception ex) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Data tidak tersedia");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        String messageResponse = data.toString();
        ctx.log(messageResponse);
        req.setInfo(messageResponse);
        req.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Success").put("data", data));
        ctx.put(Constants.OBJ.WSREQUEST, req);
        return PREPARED | NO_JOIN;
    }
}
