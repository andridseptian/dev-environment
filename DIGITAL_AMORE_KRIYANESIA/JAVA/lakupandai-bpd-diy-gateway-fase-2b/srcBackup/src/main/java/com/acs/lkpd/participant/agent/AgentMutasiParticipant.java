package com.acs.lkpd.participant.agent;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.ObjectParser;
import com.acs.util.Service;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author ACS
 */
public class AgentMutasiParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final TransactionRepository transactionRepository;
    private final NotificationTrxRepository notificationTrxRepository;
    private final ProductRepository productRepository;

    public AgentMutasiParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        transactionRepository = context.getBean(TransactionRepository.class);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
        productRepository = context.getBean(ProductRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        String dstartDate = dataRequest.getString(Constants.PARAM.START_DATE);
        String dendDate = dataRequest.getString(Constants.PARAM.END_DATE);
        int requestPage = 0;
        if (dataRequest.has(Constants.PARAM.PAGE)) {
            requestPage = dataRequest.getInt(Constants.PARAM.PAGE);
        }
        LocalDateTime startDate;
        LocalDateTime endDate;
        try {
            startDate = LocalDateTime.parse(dstartDate + " 00:00:00", Constants.FORMAT_DDMMYYYYHHMMSS);
            endDate = LocalDateTime.parse(dendDate + " 23:59:59", Constants.FORMAT_DDMMYYYYHHMMSS);
        } catch (DateTimeParseException | NullPointerException ex) {
            log.error("mutasi ex: cannot parse date [" + dstartDate + "|" + dendDate + "]");
            log.error("detail ex:" + ex.getMessage());
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Format tanggal awal dan akhir mutasi tidak sesuai");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        int maxMutasiDays = 30; //bisa diubah ke parameterize
//        int maxMutasiDays = 5; //bisa diubah ke parameterize
        int sizePerPage = 10; //bisa diubah ke parameterize
//        int sizePerPage = 3;
        PageRequest pageRequest = new PageRequest(requestPage, sizePerPage);
        try {
            maxMutasiDays = Integer.parseInt(Service.getResponseFromSettings(KeyMap.MAX_DAYS_MUTASI.name()));
        } catch (NullPointerException ex) {
            log.error("mutasi ex: cannot parse setting " + KeyMap.MAX_DAYS_MUTASI.name() + ", default set 30");
        }

        if (startDate.isAfter(endDate)) {
            ctx.getLogEvent().addMessage(this.getClass().getName(), "error startDate > endDate");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Format tanggal awal tidak boleh lebih besar dari endDate");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        if (startDate.isBefore(LocalDateTime.now().minusDays(maxMutasiDays))) {
            ctx.getLogEvent().addMessage(this.getClass().getName(), "error startDate > maxMutasiDays");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Tanggal awal diluar batas yang ditentukan [" + maxMutasiDays + " hari]");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        if (endDate.isBefore(LocalDateTime.now().minusDays(maxMutasiDays))) {
            ctx.getLogEvent().addMessage(this.getClass().getName(), "error endDate > maxMutasiDays");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Tanggal akhir diluar batas yang ditentukan [" + maxMutasiDays + " hari]");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }

        String decription = "-";
//        Page<MTransaction> listMutasi = transactionRepository.mutasiTransaksiAgentWithPaging(startDate, endDate, agent, Constants.TRX_TYPE.POSTING, Constants.TRX_STATUS.SUCCESS, pageRequest, decription);
        Page<MTransaction> listMutasi = transactionRepository.mutasiTransaksiAgentWithPagingAndProcess(startDate, endDate, agent, Constants.TRX_TYPE.POSTING, Constants.TRX_STATUS.SUCCESS, Constants.TRX_STATUS.PROCESS, pageRequest, decription);
        if (listMutasi == null || listMutasi.getContent().isEmpty()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Halaman kosong / data tidak ditemukan");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        log.info(listMutasi);
        JSONObject response = new JSONObject();
        List<MTransaction> trxcoy = listMutasi.getContent();
//          "id",
//                        "description",
//                        "crTime",
//                        "nasabah",
//                        "totalAmount"

        JSONArray array = new JSONArray();
        for (MTransaction mTransaction : trxcoy) {
//            JSONObject respBank = new JSONObject(mTransaction.getBankResponseDetail());
            JSONObject objTrx = new JSONObject();
            objTrx.put("id", mTransaction.getId());
            objTrx.put("description", mTransaction.getDescription());
            
            int longTotalAmount = (int) Math.round(mTransaction.getTotalAmount());
            int longAmount = (int) Math.round(mTransaction.getTotalAmount());
            
            String add = "";
            if (Constants.TRX_STATUS.SUCCESS == mTransaction.getStatus() | mTransaction.getStatus() == Constants.TRX_STATUS.PROCESS) {
                if (mTransaction.getNasabah() == null | mTransaction.getDescription().equals("Setor Tunai")) {
                    add = "D ";
                } else if (mTransaction.getDescription().equals("Tarik Tunai")) {
                    add = "K ";
                }
            }

            try {
                String crtime = String.valueOf(mTransaction.getCrTime());
                String tanggal = crtime.substring(0, 16).replace('T', ' ');
                Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(tanggal);
                String parsingDate = new SimpleDateFormat("dd-MM-yyyy hh:mm").format(date);
                objTrx.put("crTime", parsingDate);
            } catch (Exception e) {
                log.error(e);
                objTrx.put("crTime", mTransaction.getCrTime());
            }

//            objTrx.put("crTime", mTransaction.getCrTime());
            if (mTransaction.getNasabah() != null) {
                objTrx.put("nasabah", mTransaction.getNasabah().getName());
            } else {
                objTrx.put("nasabah", mTransaction.getAgent().getName());
            }
//            if (mTransaction.getProductCode())

//            MProduct product = productRepository.findByProductCode(mTransaction.getProductCode());
//            
//            if(product.getProductName().equals("PDAM")){
//                JSONObject additionalData = new JSONObject(mTransaction.getAdditionalData());
//                String bulantgn = additionalData.getString("jmlBulanTagihan");
//                int blntgh = Integer.parseInt(bulantgn);
//                double feeBank = mTransaction.getFeeBank() * blntgh;
//                mTransaction.setTotalAmount(mTransaction.getAmount()+feeBank);
//            }
//            objTrx.put("nasabah", mTransaction.get);
            objTrx.put("feeAgen", mTransaction.getFeeAgent());
            objTrx.put("feeBank", mTransaction.getFeeBank());
//            objTrx.put("feeBank", mTransaction.getFeeBank());
//            objTrx.put("totalAmount",mTransaction.getAmount() + mTransaction.getFeeBank());
//            objTrx.put("totalAmount", add + mTransaction.getTotalAmount());
            objTrx.put("totalAmount", add + String.valueOf(longTotalAmount));
            objTrx.put("totalAmountNReal", mTransaction.getAmount() + mTransaction.getFeeBank());
            objTrx.put("Amount", mTransaction.getAmount());
//            objTrx.put("Amount", String.valueOf(longTotalAmount));
//            if (mTransaction.getDescription().equals("Setor Tunai")
//                    | mTransaction.getDescription().equals("Tarik Tunai")
//                    | mTransaction.getDescription().equals("Transfer Lakupandai")
//                    | mTransaction.getDescription().equals("Transfer On Us")) {
//                objTrx.put("resi", mTransaction.getOriginReff());
//            } else { 
//                objTrx.put("resi", respBank.getString("rrn"));
//            }
//            objTrx.put("resi", respBank.getString("rrn"));
            objTrx.put("resi", mTransaction.getOriginReff());
            array.put(objTrx);
        }

//        response.put("mutasi", new JSONArray(ObjectParser.listMutasiView(listMutasi.getContent())));
        response.put("mutasi", array);
        response.put("sizePerPage", sizePerPage);
        response.put("totalPages", listMutasi.getTotalPages());
        response.put("totalMutasi", listMutasi.getTotalElements());
        response.put("page", requestPage);
        req.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.OBJ.RESP, response);
        ctx.put(Constants.OBJ.WSREQUEST, req);
        return PREPARED | NO_JOIN;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
