/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MTransaction;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;

/**
 *
 * @author ACS
 */
public class NoRouteParticipant implements TransactionParticipant {

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return ABORTED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.OBJ.TRANSACTION);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        trx.setInfo("Transaksi tidak dapat dilakukan, kode transaksi tidak terdaftar");
        ctx.put(Constants.OBJ.TRANSACTION, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.OBJ.TRANSACTION);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        trx.setInfo("Transaksi tidak dapat dilakukan, kode transaksi tidak terdaftar");
        ctx.put(Constants.OBJ.TRANSACTION, trx);
    }
}
