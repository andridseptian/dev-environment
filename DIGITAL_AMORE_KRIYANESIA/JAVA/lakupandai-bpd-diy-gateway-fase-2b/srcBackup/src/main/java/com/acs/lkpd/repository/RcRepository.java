/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MRc;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface RcRepository extends JpaRepository<MRc, Long> {

    List<MRc> findByProcessCode(String processCode);

    MRc findByProcessCodeAndRc(String procode, String rc);
}
