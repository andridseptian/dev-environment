/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRequest;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONObject;

/**
 *
 * @author ACS
 */
public class SendEmailRegistrationWebParticipant implements TransactionParticipant {

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject obj = new JSONObject(req.getRequest());
        String iName = obj.getString(Constants.PARAM.NAME);
        String iEmail = obj.getString(Constants.PARAM.EMAIL);
        String iId = obj.getString(Constants.PARAM.ID);
        String iPassword = Utility.generateStan();
        Boolean SendEmailAdditional = true;
        if (SendEmailAdditional) {
            if (Service.sendEmailRegistrationWebUSer(iName, iEmail, iPassword)) {
                Service.saveSessionWeb(iId, "Send email to " + iEmail + " success");
                req.setStatus(Constants.TRX_STATUS.SUCCESS);
                req.setResponse("Success");
                ctx.put(Constants.OBJ.RESP, new JSONObject()
                        .put("password", iPassword)
                        .put("rm", req.getResponse())
                        .put("rc", Constants.SUCCESS));
                return PREPARED | NO_JOIN;
            } else {
                Service.saveSessionWeb(iId, "Send email to " + iEmail + " failed");
                req.setStatus(Constants.TRX_STATUS.SUCCESS);
                req.setResponse("Approval Success, WARN: Email Gagal Terkirim");
                ctx.put(Constants.OBJ.RESP, new JSONObject()
                        .put("password", iPassword)
                        .put("rm", req.getResponse())
                        .put("rc", Constants.SUCCESS));
                return PREPARED | NO_JOIN;
            }
        } else {
            Service.saveSessionWeb(iId, "Send Email to " + iEmail + " failed");
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm(Service.getResponseFromSettings(KeyMap.RM_EMAIL_REGISTRATION_FAILED.name()));
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
    }
}
