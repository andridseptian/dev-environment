/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.TransactionParticipant;

/**
 *
 * @author ACS
 */
public class ValidasiSetorTunaiParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;

    @Override
    public int prepare(long l, Serializable srlzbl) {
//        MSession session = (MSession) ctx.get(Constants.SESSION);
        return PREPARED;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
