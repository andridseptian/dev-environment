/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.payment;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.NotificationRepository;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.util.FormatTanggal;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author risyamaulana
 */
public class FormatPostingPDAM implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private NotificationRepository notificationRepository;
    private NotificationTrxRepository notificationTrxRepository;

    public FormatPostingPDAM() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationRepository = context.getBean(NotificationRepository.class);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        JSONObject bankResp = (JSONObject) ctx.get(Constants.OBJ.BANKRESP);
        MProduct productPayment = (MProduct) ctx.get(Constants.OBJ.PRODUCTPAYMENT);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        Boolean sendSMSAgent = cfg.getBoolean("sendAgentSMS", false);
        Boolean sendSMSNasabah = cfg.getBoolean("sendNasabahSMS", false);
        Boolean sendNotifAgent = cfg.getBoolean("sendAgentNotif", false);
        if (productPayment == null) {
            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
            session.setResponseToClient("Product tidak ditemukan atau tidak terdaftar, mohon coba beberapa saat lagi / hubungi admin untuk tindakan lebih lanjut");
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
        String mobileResponse = "", otpResponse = "";
        JSONArray formatInquiryArray = null;
        try {

            JSONObject additionalData = new JSONObject(bankResp.getString(Constants.PARAM.WS_PAY_ADDITIONALDATA));

            double amount = trx.getAmount();
            if (!trx.getStatus().equals(Constants.TRX_STATUS.PROCESS)) {
                amount = Utility.getParseAmount(bankResp.getString(Constants.PARAM.WS_PAY_TRANSACTIONAMMOUNT), 100);

            }

            String jobId = bankResp.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY);
            String productName = productPayment.getProductName();
            String nop = additionalData.getString("idPelanggan");
            String nama = additionalData.getString("nama");
            String bulanTagihan = "";
            if (productPayment.getProductCode().contains("40141")) {
                bulanTagihan = "1";
            } else {
                bulanTagihan = additionalData.getString("jmlBulanTagihan");
            }
//            double fee = trx.getFeeAgent() + trx.getFeeBank();
            double fee = productPayment.getFeeBank() * Double.valueOf(bulanTagihan);
//             double fee = productPayment.getFeeBank();
            double totalAmount = amount + fee;
            String rrn = bankResp.getString(Constants.PARAM.WS_PAY_RRN);
            //Build Mobile response
            SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
//            formatInquiryArray = new JSONArray(productPayment.getFormatPayment());
            StringBuilder sbPeriod = new StringBuilder();
            if (!bulanTagihan.equalsIgnoreCase("1") && !bulanTagihan.equalsIgnoreCase("0")) {
                formatInquiryArray = new JSONArray(Constants.PDAM_PAYMENT_FORMAT_PERIODE);

                Integer per = Integer.valueOf(bulanTagihan);

                if (!trx.getStatus().equals(Constants.TRX_STATUS.PROCESS)) {
                    for (int i = 1; i <= per; i++) {
                        String getPeriod = "periode" + i;
                        String getTagihan = "tagihan" + i;

                        String valuePeriod = additionalData.getString(getPeriod);
                        double valueTagihan = Double.valueOf(additionalData.getString(getTagihan));

                        if (i == per) {
                            sbPeriod.append(valuePeriod + " - Rp" + Utility.formatAmount(valueTagihan));
                        } else {
                            sbPeriod.append(valuePeriod + " - Rp" + Utility.formatAmount(valueTagihan) + "\n");
                        }
                    }
                } else {
                    sbPeriod.append("-");
                }

            } else {
                formatInquiryArray = new JSONArray(Constants.PDAM_PAYMENT_FORMAT);
            }
            for (int i = 0; i < formatInquiryArray.length(); i++) {
                JSONObject data = formatInquiryArray.getJSONObject(i);
//                if (sendSMSNasabah && trx.getAccountType().equals("customer")) {
                if (sendSMSNasabah && trx.getAccountType().equals("customer") && !trx.getStatus().equals(Constants.TRX_STATUS.PROCESS)) {
                    data.put("value", data.getString("value")
                            .replace("[LAYANAN]", productPayment.getProductName())
                            .replace("[NO_RESI]", rrn)
//                            .replace("[WAKTU]", sdfFormatPayment.format(new Date()))
                            .replace("[WAKTU]", FormatTanggal.tanggalLokalNow())
                            .replace("[ID_Pelanggan]", nop)
                            .replace("[Nama]", nama)
                            .replace("[BULAN_TAGIHAN]", bulanTagihan)
                            .replace("[PERIODE]", sbPeriod.toString())
                            .replace("[NOMINAL]", Utility.formatAmount(amount))
                            .replace("[BIAYA_ADMIN]", Utility.formatAmount(fee))
                            .replace("[JUMLAH_BIAYA]", Utility.formatAmount(totalAmount))
                            .replace("[VOUCHER_SN]", "-")
//                            .replace("[STATUS]", "Status transaksi " + productPayment.getProductName() + ", akan dikirimkan melalui SMS ke HP nasabah tersebut")
                            .replace("[STATUS]", trx.viewStatus())
                    );
                } else {
                    data.put("value", data.getString("value")
                            .replace("[LAYANAN]", productPayment.getProductName())
                            .replace("[NO_RESI]", rrn)
//                            .replace("[WAKTU]", sdfFormatPayment.format(new Date()))
                            .replace("[WAKTU]", FormatTanggal.tanggalLokalNow())
                            .replace("[ID_Pelanggan]", nop)
                            .replace("[Nama]", nama)
                            .replace("[BULAN_TAGIHAN]", bulanTagihan)
                            .replace("[PERIODE]", sbPeriod.toString())
                            .replace("[NOMINAL]", Utility.formatAmount(amount))
                            .replace("[BIAYA_ADMIN]", Utility.formatAmount(fee))
                            .replace("[JUMLAH_BIAYA]", Utility.formatAmount(totalAmount))
                            .replace("[VOUCHER_SN]", "-")
                            .replace("[STATUS]", trx.viewStatus())
                    );
                }
                ctx.put(Constants.HEADER_VALUE, data);
            }

            mobileResponse = new JSONObject().put("data", formatInquiryArray).toString();

            //Build SMS OTP Response
            SimpleDateFormat sdfFormatSMS = new SimpleDateFormat("dd/MM/YY HH:mm");
            String formatSMS = productPayment.getFormatSMSPayment();
//            otpResponse = formatSMS
//                    .replace("[product]", productName)
//                    .replace("[IdPelanggan]", nop)
//                    .replace("[Name]", nama)
//                    .replace("[BulanTagihan]", bulanTagihan)
//                    .replace("[Amount]", Utility.formatAmount(amount))
//                    .replace("[Price]", Utility.formatAmount(totalAmount))
//                    .replace("[status]", trx.viewStatus());

            otpResponse = formatSMS
                    .replace("[product]", productName)
                    .replace("[datetime]", sdfFormatSMS.format(new Date()))
                    .replace("[resi]", rrn)
                    .replace("[IdPelanggan]", nop)
                    .replace("[Name]", nama)
                    .replace("[BulanTagihan]", bulanTagihan)
                    .replace("[Amount]", Utility.formatAmount(amount))
                    .replace("[Admin]", Utility.formatAmount(fee))
                    .replace("[status]", trx.viewStatus());
            try {
                if (sendSMSNasabah) {
                    boolean statusSMSNasabah = Utility.sendSMS(otpResponse, session.getNasabah().getMsisdn());
                    trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Nasabah [" + session.getNasabah().getMsisdn() + "] = " + statusSMSNasabah);
                } else {
                    trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
                }
            } catch (Exception ex) {
                trx.setSmsInfo(trx.getSmsInfo() + "error sms nasabah:" + ex.getMessage() + "|");
            }
            try {
                if (sendSMSAgent) {
                    boolean statusSMSAgent = Utility.sendSMS(otpResponse, session.getAgent().getMsisdn());
                    trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Agent [" + session.getAgent().getMsisdn() + "] = " + statusSMSAgent);
                    MNotifikasiTrx notif = (MNotifikasiTrx) ctx.get(Constants.NOTIFTRX);
                    notif.setMobileResponse(mobileResponse);
                    notif = notificationTrxRepository.save(notif);
                    ctx.put(Constants.NOTIFTRX, notif);
                } else {
                    trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
                }
            } catch (Exception ex) {
                trx.setSmsInfo(trx.getSmsInfo() + "error sms nasabah:" + ex.getMessage() + "|");
            }
            try {
                if (sendNotifAgent) {
                    MNotification notif = new MNotification();
                    notif.setRrn(trx.getOriginReff());
                    notif.setToken(session.getAgent().getToken());
                    notif.setAgent(session.getAgent().getId());
                    notif.setTitle(productName);
                    notif.setImage("");
                    notif.setMessage(otpResponse);
                    Utility util = new Utility();
                    boolean sendFcmMessage = util.sendFirebaseNotifFcm(notif.getToken(), notif.getTitle(), notif.getMessage());
                    if (sendFcmMessage) {
                        notif.setStatus(Constants.FCM.STATUS.SUCCESS);
                    } else {
                        notif.setStatus(Constants.FCM.STATUS.FAILED);
                    }
                    notificationRepository.save(notif);
                } else {
                    trx.setSmsInfo(trx.getSmsInfo() + "Not Send Notif " + trx.getDescription());
                }
            } catch (Exception ex) {
                trx.setSmsInfo(trx.getSmsInfo() + "error notif agent:" + ex.getMessage() + "|");
            }
            trx.setBankReff(bankResp.getString(Constants.PARAM.WS_PAY_JOBIDINQUIRY));
            trx.setAmount(amount);
            trx.setFeeAgent(productPayment.getFeeAgent());
            trx.setFeeBank(productPayment.getFeeBank());
            trx.setTotalAmount(totalAmount);
            trx.setMobileResponse(mobileResponse);

            MNotifikasiTrx notif = (MNotifikasiTrx) ctx.get(Constants.NOTIFTRX);
            notif.setMobileResponse(mobileResponse);
            notif = notificationTrxRepository.save(notif);
            ctx.put(Constants.NOTIFTRX, notif);

            trx.setSmsResponse(otpResponse);
            trx.setIdBilling(bankResp.getString("idBilling"));
            trx.setDescription(productPayment.getProductName());
            trx.setAdditionalData(additionalData.toString());
            trx.setFeeBank(fee);
            session.setAmount(trx.getTotalAmount());
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(mobileResponse);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            ctx.log(ex.getMessage());
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
