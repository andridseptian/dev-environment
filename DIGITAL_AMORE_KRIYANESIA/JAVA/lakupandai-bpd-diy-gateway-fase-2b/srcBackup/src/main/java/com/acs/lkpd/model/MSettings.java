package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "m_settings")
public class MSettings implements Serializable {

    private String key;
    private String value;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private Long userId;
    private Long approvalId;
    private String requestValue;
    private String oldValue;
    private String description;
    private Constants.SettingStatus status;

    @Id
    @Column(name = "key")
    public String getKey() {
        return key;
    }

    @Column(name = "value")
    @Type(type = "text")
    public String getValue() {
        return value;
    }

    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "approval_id")
    public Long getApprovalId() {
        return approvalId;
    }

    @Column(name = "request_value")
    @Type(type = "text")
    public String getRequestValue() {
        return requestValue;
    }

    @Column(name = "old_value")
    @Type(type = "text")
    public String getOldValue() {
        return oldValue;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.SettingStatus getStatus() {
        return status;
    }

    //============ SET ==============//
    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public void setRequestValue(String requestValue) {
        this.requestValue = requestValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(Constants.SettingStatus status) {
        this.status = status;
    }

}
