/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import org.jpos.transaction.TransactionParticipant;

/**
 *
 * @author acs
 */
public class GetInquiryData implements TransactionParticipant {

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
        String delimeter = "\\|";
        try {
            MOtp otpCheck = Service.findOTPPayment(session.getAgent().getId());

            if (!otpCheck.getOtp().equals(isomsg.getString(61))) {
                session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }

            MOtp otp = Service.findOTP(isomsg.getString(61));
            if (otp == null) {
                session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
//                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_TRANSACTION_NOT_FOUND.name()));
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }

            if (otp.getStatus().equals(Constants.OtpStatus.USED)) {
                session.setRcToClient(Constants.RC_TRANSACTION_IS_FAILED);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            } else if (otp.getStatus().equals(Constants.OtpStatus.INACTIVE)) {
                session.setRcToClient(Constants.RC_TRANSACTION_IS_FAILED);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
            MSession tempSession = Service.findSession(otp.getOtp(), agent);
            if (tempSession == null) {
                session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_TRANSACTION_NOT_FOUND.name()));
//                session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }

            String tempProduct = tempSession.getProduct().toString();
            tempProduct = tempProduct.substring(0, 3);

            session = tempSession;
            session.setId(null);
            session.setOriginReff(Utility.generateRrn());
            session.setStan(Utility.generateStan());
            session.setCrTime(LocalDateTime.now());
            session.setRcToClient("");
            session.setResponseToClient("");
            session.setProduct(tempSession.getProduct());
            session.setNasabah(tempSession.getNasabah());

            ctx.log(tempSession.getRequestClient());
            String[] request = tempSession.getRequestClient().split(delimeter);

            ctx.put(Constants.PRODUCT, tempSession.getProduct());
            ctx.put(Constants.REQ, request);
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.NASABAH, tempSession.getNasabah());
            return PREPARED | NO_JOIN;
        } catch (Exception ex) {
            ctx.log(ex);
            session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
//            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_TRANSACTION_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

}
