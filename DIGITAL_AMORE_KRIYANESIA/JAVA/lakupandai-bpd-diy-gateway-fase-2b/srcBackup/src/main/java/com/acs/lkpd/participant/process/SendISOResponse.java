/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import java.io.IOException;
import java.io.Serializable;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class SendISOResponse implements AbortParticipant {
    
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return PREPARED;
    }
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        if (source == null || !source.isConnected()) {
            session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
            session.setResponseToClient("Partner Not Found");
            ctx.put(Constants.SESSION, session);
            return ABORTED;
        }
        return PREPARED;
    }
    
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        ISOMsg in = (ISOMsg) ctx.get(Constants.IN);
        try {
            in.setResponseMTI();
            in.set(39, "00");
            ISOMsg out = (ISOMsg) in.clone();
            if (session.getRcToClient() != null) {
                out.set(39, session.getRcToClient());
            }
            if (session.getResponseToClient() != null) {
                out.set(48, session.getResponseToClient());
            }
            source.send(out);
        } catch (IOException ex) {
            log.error(ex);
        } catch (ISOFilter.VetoException ex) {
            log.error(ex);
        } catch (ISOException isoe) {
            log.error(isoe);
        }
    }
    
    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        ISOMsg in = (ISOMsg) ctx.get(Constants.IN);
        try {
            in.setResponseMTI();
            in.set(39, "00");
            ISOMsg out = (ISOMsg) in.clone();
            if (session.getRcToClient() != null) {
                out.set(39, session.getRcToClient());
            }
            if (session.getResponseToClient() != null) {
                out.set(48, session.getResponseToClient());
            }
            source.send(out);
        } catch (IOException ex) {
            log.error(ex);
        } catch (ISOFilter.VetoException ex) {
            log.error(ex);
        } catch (ISOException isoe) {
            log.error(isoe);
        }
    }
}
