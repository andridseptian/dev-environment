package com.acs.lkpd.participant.process;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.lkpd.repository.TransactionRepository;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.iso.ISOMsg;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class CheckISOOTPParticipant implements TransactionParticipant {

    private final SessionRepository sessionRepository;
    private final TransactionRepository transactionRepository;

    public CheckISOOTPParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        sessionRepository = context.getBean(SessionRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        try {
            ISOMsg isomsg = (ISOMsg) ctx.get(Constants.IN);
            MOtp otp = com.acs.util.Service.findOTP(isomsg.getString(61));
            MSession tempSession = sessionRepository.findFirstByOtpAndAgentOrderByCrTimeDesc(otp.getOtp(), agent);
            Boolean statusOTP = LocalDateTime.now().isBefore(otp.getEndTime());
            if (otp.getStatus().equals(Constants.OtpStatus.USED)) {
                session.setRcToClient(Constants.RC_TRANSACTION_IS_FAILED);
                session.setResponseToClient(com.acs.util.Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                return ABORTED | NO_JOIN;
            } else if (otp.getStatus().equals(Constants.OtpStatus.INACTIVE)) {
                session.setRcToClient(Constants.RC_TRANSACTION_IS_FAILED);
                session.setResponseToClient(com.acs.util.Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                return ABORTED | NO_JOIN;
            }
            if (tempSession != null) {
                if (!statusOTP) {
                    com.acs.util.Service.setOTPStatus(otp, Constants.OtpStatus.INACTIVE);
                    MTransaction trx = com.acs.util.Service.findTransaction(tempSession.getOriginReff(), tempSession.getStan(), tempSession.getOtp());
                    trx.setStatus(Constants.TRX_STATUS.FAILED);
                    trx.setInfo("EXPIRED OTP");
                    trx.setDescription("OTP expired");
                    transactionRepository.save(trx);
                    session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
                    session.setResponseToClient(com.acs.util.Service.getResponseFromSettings(KeyMap.RM_OTP_EXP_NOT_FOUND.name()));
                    ctx.put(Constants.SESSION, session);
                    return ABORTED | NO_JOIN;
                } else {
                    return PREPARED | NO_JOIN;
                }
            } else {
                session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
                session.setResponseToClient(com.acs.util.Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        } catch (NullPointerException ex) {
            session.setRcToClient(Constants.RC_TRANSACTION_NOT_FOUND);
            session.setResponseToClient(com.acs.util.Service.getResponseFromSettings(KeyMap.RM_OTP_TRANSACTION_NOT_FOUND.name()));
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }

    }

}
