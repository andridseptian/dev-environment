/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.format;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MProduct;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author acs
 */
public class FormatPostingTarikTunai implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        ctx.log(session.getNasabah().getName());
        MNasabah nasabah = session.getNasabah();
        
        String response = Service.getResponseFromSettings(KeyMap.RM_POSTING_TARIK_TUNAI.name());
        try {
//            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
//                    .replace("[Msisdn]", Utility.viewMsisdn(nasabah.getMsisdn()))
//                    .replace("[Name]", nasabah.getName());
//            response = response + Service.getResponseFromSettings(KeyMap.RM_MESSAGE_NOTIF.name());

            JSONArray formatInquiryArray = null;
            try {
                formatInquiryArray = new JSONArray(product.getFormatInquiry());
                for (int i = 0; i < formatInquiryArray.length(); i++) {
                    JSONObject data = formatInquiryArray.getJSONObject(i);
                    data.put("value", data.getString("value")
                            .replace("[ProductName]", product.getProductName())
                            .replace("[Amount]", "Rp." + Utility.formatAmount(session.getAmount()))
                            .replace("[NasabahName]", nasabah.getName())
                            .replace("[Desc]", Service.getResponseFromSettings(KeyMap.RM_MESSAGE_NOTIF.name()))
                            .replace("[Msisdn]", Utility.viewMsisdn(nasabah.getMsisdn())));
                }
                response = new JSONObject().put("data", formatInquiryArray).toString();
            } catch (JSONException | NullPointerException ex) {
                response = "Transaksi tidak dapat dilakukan, format product tidak ditemukan";
            }

            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
            ctx.put(Constants.SESSION, session);
            return PREPARED | NO_JOIN;
        } catch (Exception ex) {
            ctx.log(ex);
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
            return ABORTED | NO_JOIN;
        }
    }

}
