package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentOTPParticipant implements TransactionParticipant, Configurable {

    private Configuration cfg;
    private OTPRepository otpRepository;

    public AgentOTPParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        otpRepository = context.getBean(OTPRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String iId = dataRequest.getString(Constants.PARAM.ID);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String marker = cfg.get("marker", agent.getMsisdn());
        String descResponse = "";
        descResponse = Service.getResponseFromSettings(KeyMap.RM_OTP_AGENT.name());
        if (marker.equalsIgnoreCase("reset")) {
            marker = agent.getResetMsisdn();
        }
        MOtp lastOtp = otpRepository.findFirstByMarkerOrderByCrTimeDesc(marker);
        MOtp otp = new MOtp(marker);
        if (lastOtp != null && lastOtp.getOtp() != null) {
            ctx.log("last otp not null");
            Boolean statusOTP = LocalDateTime.now().isBefore(lastOtp.getEndTime());
            if (statusOTP && lastOtp.getStatus() == Constants.OtpStatus.ACTIVE) {
                otp.setCrTime(LocalDateTime.now());
                long longExpiredOtp;
                try {
                    longExpiredOtp = Long.parseLong(Service.getResponseFromSettings(KeyMap.TIMEOUT_OTP.name()));
                } catch (NullPointerException | NumberFormatException nfe) {
                    longExpiredOtp = 60;
                }
                LocalDateTime expiredTime = otp.getCrTime().plusSeconds(longExpiredOtp);
                otp.setEndTime(expiredTime);
                otp.setStatus(Constants.OtpStatus.ACTIVE);
                otp.setOtp(lastOtp.getOtp());
                descResponse = descResponse.replace("[OTP]", otp.getOtp());
                ctx.log("desc:" + descResponse);
            } else {
                lastOtp.setStatus(Constants.OtpStatus.INACTIVE);
                otpRepository.saveAndFlush(lastOtp);
                req.setOtp(otp.getOtp());
                descResponse = descResponse.replace("[OTP]", otp.getOtp());
                ctx.log("desc" + descResponse);
            }
        } else {
            ctx.log("last otp null");
            req.setOtp(otp.getOtp());
            descResponse = descResponse.replace("[OTP]", otp.getOtp());
            ctx.log("desc" + descResponse);
        }
        MOtp savedOtp = otpRepository.save(otp);
        Service.saveSessionWeb(iId, "Create OTP " + savedOtp.getMarker());
        if (sendSMS) {
            boolean statusSMS = Utility.sendSMS(descResponse, marker);
            if (statusSMS) {
                String messageResponse = "SMS success[" + Utility.viewMsisdn(marker) + "], OTP " + savedOtp.getOtp();
                ctx.log(messageResponse);
                req.setInfo(messageResponse);
                req.setStatus(Constants.TRX_STATUS.SUCCESS);
                ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "send SMS Success"));
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return PREPARED | NO_JOIN;
            } else {
                String rm = "Status pesan tidak dapat terkirim, ulangi beberapa saat lagi";
                req.setRm(rm);
                req.setInfo("SMS failed[" + Utility.viewMsisdn(marker) + "], OTP " + savedOtp.getOtp());
                req.setStatus(Constants.TRX_STATUS.FAILED);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        } else {
            String rm = "Not Send SMS[" + Utility.viewMsisdn(marker) + "], OTP " + otp.getOtp();
            req.setInfo(rm);
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return PREPARED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
