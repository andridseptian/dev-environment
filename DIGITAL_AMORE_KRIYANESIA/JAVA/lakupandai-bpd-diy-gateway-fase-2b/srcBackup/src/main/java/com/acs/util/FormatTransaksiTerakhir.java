/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.util;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Andri D. Septian
 */
public class FormatTransaksiTerakhir {

    private static final Log log = Log.getLog("Q2", FormatTransaksiTerakhir.class.getName());

    private final NotificationTrxRepository notificationTrxRepository;
    private final ProductRepository productRepository;

    private LogEvent logEvent = new LogEvent();

    public FormatTransaksiTerakhir() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
        productRepository = context.getBean(ProductRepository.class);

    }

    public String getFormat(MTransaction trx, String status) throws ParseException {
        String description = trx.getDescription().toLowerCase();
        logEvent.addMessage("--Format Transaksi Terakhir--");
        logEvent.addMessage("get Format retun String");
        logEvent.addMessage("Product Code   : " + trx.getProductCode());
        logEvent.addMessage("Status         : " + status);
        logEvent.addMessage("Description    : " + description);

        SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
        String trxDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(trx.getCrTime().toString());

        if (status.equalsIgnoreCase("SUCCESS") || status.equalsIgnoreCase("PENDING") || status.equalsIgnoreCase("PROCESS")) {
            String format = "";
            JSONObject bankResponseDetail = new JSONObject(trx.getBankResponseDetail());
//            LocalDateTime trxDate = LocalDateTime.now();
//            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");

            String data = "";

            switch (description) {
                case "setor tunai":
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";
                    format = format.replace("[LAYANAN]", trx.getDescription())
                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[NAMA]", trx.getNasabah().getName())
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    data = new JSONObject().put("data", format).toString();
                    logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                    logEvent.addMessage(data);
                    log.info(logEvent);
                    return data;

                case "tarik tunai":
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";

                    format = format.replace("[LAYANAN]", trx.getDescription())
                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[NAMA]", trx.getNasabah().getName())
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    data = new JSONObject().put("data", format).toString();
                    logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                    logEvent.addMessage(data);
                    log.info(logEvent);
                    return data;

                case "transfer on us":
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";
                    format = format.replace("[LAYANAN]", trx.getDescription())
                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                            .replace("[ACC_NAME]", trx.getInfo())
                            .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    data = new JSONObject().put("data", format).toString();
                    logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                    logEvent.addMessage(data);
                    log.info(logEvent);
                    return data;

                case "transfer lakupandai":
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";

                    format = format.replace("[LAYANAN]", trx.getDescription())
                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                            .replace("[ACC_NAME]", trx.getInfo())
                            .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    data = new JSONObject().put("data", format).toString();
                    logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                    logEvent.addMessage(data);
                    log.info(logEvent);
                    return data;

                default:
                    String responseMobile = trx.getMobileResponse();
                    responseMobile = responseMobile.replace("Status transaksi " + trx.getDescription() + ", akan dikirimkan melalui SMS ke HP nasabah tersebut", bankResponseDetail.getString("responseDesc"));

                    logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                    logEvent.addMessage(responseMobile);
                    log.info(logEvent);

                    return responseMobile;
            }

        } else {
            String format = "";

//            MNotifikasiTrx notif = notificationTrxRepository.findFirstByOriginReff(trx.getOriginReff());
            JSONObject bankResponseDetail = new JSONObject(trx.getBankResponseDetail());
//            LocalDateTime trxDate = LocalDateTime.now();
//            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
            String data = "";

//            SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
//            String trxDate = FormatTanggal.tanggalLokal(sdfFormatPayment.format(trx.getCrTime()));
            if (description.contains("setor tunai")) {

                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                        + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                        + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                        + "]";
                format = format.replace("[LAYANAN]", trx.getDescription())
                        //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                        .replace("[WAKTU]", trxDate)
                        .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                        .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                        .replace("[NAMA]", trx.getNasabah().getName())
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );

                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("tarik tunai")) {

                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                        + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                        + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                        + "]";
                format = format.replace("[LAYANAN]", trx.getDescription())
                        //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                        .replace("[WAKTU]", trxDate)
                        .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                        .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                        .replace("[NAMA]", trx.getNasabah().getName())
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );

                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("transfer on us")) {

                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                        + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                        + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                        + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                        + "]";
                format = format.replace("[LAYANAN]", trx.getDescription())
                        //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                        .replace("[WAKTU]", trxDate)
                        .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                        .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                        .replace("[ACC_NAME]", trx.getInfo())
                        .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );

                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("transfer lakupandai")) {
                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                        + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                        + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                        + "    {\"header\":\"Nominal\",\"value\":\"[NOMINAL]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                        + "]";
                format = format.replace("[LAYANAN]", trx.getDescription())
                        //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                        .replace("[WAKTU]", trxDate)
                        .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                        .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                        .replace("[ACC_NAME]", trx.getInfo())
                        .replace("[NOMINAL]", "Rp." + Utility.formatAmount(trx.getAmount()))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );

                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("prepaid")) {

                format = "{\n"
                        + "    \"data\" : [\n"
                        + "        {\n"
                        + "            \"header\" : \"layanan\",\n"
                        + "            \"value\" : \"[ProductName]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Waktu\",\n"
                        + "            \"value\" : \"[Waktu]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"No Hp\",\n"
                        + "            \"value\" : \"[DestinationMsisdn]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Nominal\",\n"
                        + "            \"value\" : \"Rp.[Amount]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Status\",\n"
                        + "            \"value\" : \"[Status]\"\n"
                        + "        }\n"
                        + "    ]\n"
                        + "\n"
                        + "}";
                format = format
                        .replace("[ProductName]", trx.getDescription())
                        .replace("[Waktu]", trxDate)
                        .replace("[DestinationMsisdn]", bankResponseDetail.getString("idBilling"))
                        .replace("[Amount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100))
                                .replace("[Status]", bankResponseDetail.getString("responseDesc"))
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("paket data")) {
                format = "{\n"
                        + "    \"data\" : [\n"
                        + "        {\n"
                        + "            \"header\" : \"layanan\",\n"
                        + "            \"value\" : \"[ProductName]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Resi\",\n"
                        + "            \"value\" : \"[Resi]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Waktu\",\n"
                        + "            \"value\" : \"[Waktu]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"No Hp\",\n"
                        + "            \"value\" : \"[DestinationMsisdn]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Nominal\",\n"
                        + "            \"value\" : \"Rp.[Amount]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Status\",\n"
                        + "            \"value\" : \"[Status]\"\n"
                        + "        }\n"
                        + "    ]\n"
                        + "\n"
                        + "}";

                String resi = Optional.ofNullable(trx.getBankReff()).orElse(trx.getOriginReff());
                format = format
                        .replace("[ProductName]", trx.getDescription())
                        .replace("[Resi]", resi)
                        .replace("[Waktu]", trxDate)
                        .replace("[DestinationMsisdn]", bankResponseDetail.getString("idBilling"))
                        .replace("[Amount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100))
                                .replace("[Status]", bankResponseDetail.getString("responseDesc"))
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("postpaid")) {
                format = "{\n"
                        + "    \"data\" : [\n"
                        + "        {\n"
                        + "            \"header\" : \"layanan\",\n"
                        + "            \"value\" : \"[ProductName]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Waktu\",\n"
                        + "            \"value\" : \"[Waktu]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"No Hp\",\n"
                        + "            \"value\" : \"[DestinationMsisdn]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Status\",\n"
                        + "            \"value\" : \"[Status]\"\n"
                        + "        }\n"
                        + "    ]\n"
                        + "\n"
                        + "}";
                format = format
                        .replace("[ProductName]", trx.getDescription())
                        //                        .replace("[Resi]", rrn)
                        .replace("[Waktu]", trxDate)
                        .replace("[DestinationMsisdn]", bankResponseDetail.getString("idBilling"))
                        .replace("[Status]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("telkompay")) {
                format = "{\n"
                        + "    \"data\" : [\n"
                        + "        {\n"
                        + "            \"header\" : \"layanan\",\n"
                        + "            \"value\" : \"[ProductName]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Waktu\",\n"
                        + "            \"value\" : \"[Waktu]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Id Biller\",\n"
                        + "            \"value\" : \"[DestinationMsisdn]\"\n"
                        + "        },\n"
                        + "        {\n"
                        + "            \"header\" : \"Status\",\n"
                        + "            \"value\" : \"[Status]\"\n"
                        + "        }\n"
                        + "    ]\n"
                        + "\n"
                        + "}";
                format = format
                        .replace("[ProductName]", trx.getDescription())
                        //                        .replace("[Resi]", rrn)
                        .replace("[Waktu]", trxDate)
                        .replace("[DestinationMsisdn]", bankResponseDetail.getString("idBilling"))
                        .replace("[Status]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("kereta")) {

                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"Kode Bayar\",\"value\":\"[KODE_BAYAR]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                format = format
                        .replace("[LAYANAN]", trx.getDescription())
                        .replace("[WAKTU]", trxDate)
                        .replace("[KODE_BAYAR]", bankResponseDetail.getString("idBilling"))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("pbb")) {

                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"N.O.P\",\"value\":\"[NO_TAGIHAN]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                format = format
                        .replace("[LAYANAN]", trx.getDescription())
                        .replace("[WAKTU]", trxDate)
                        .replace("[NO_TAGIHAN]", bankResponseDetail.getString("idBilling"))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("asuransi")) {
                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"No Peserta\",\"value\":\"[ID_PELANGGAN]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                format = format
                        .replace("[LAYANAN]", trx.getDescription())
                        .replace("[WAKTU]", trxDate)
                        .replace("[ID_PELANGGAN]", bankResponseDetail.getString("idBilling"))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("pajak daerah")) {
                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"No. Tagihan\",\"value\":\"[NO_TAGIHAN]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                format = format
                        .replace("[LAYANAN]", trx.getDescription())
                        .replace("[WAKTU]", trxDate)
                        .replace("[NO_TAGIHAN]", bankResponseDetail.getString("idBilling"))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("retribusi")) {
                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"ID Retribusi\",\"value\":\"[ID_RETRIBUSI]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                format = format
                        .replace("[LAYANAN]", trx.getDescription())
                        .replace("[WAKTU]", trxDate)
                        .replace("[ID_RETRIBUSI]", bankResponseDetail.getString("idBilling"))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else if (description.contains("pdam")) {

                format = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "    {\"header\":\"ID Pelanggan\",\"value\":\"[ID_Pelanggan]\"},\n"
                        + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                format = format
                        .replace("[LAYANAN]", trx.getDescription())
                        .replace("[WAKTU]", trxDate)
                        .replace("[ID_Pelanggan]", bankResponseDetail.getString("idBilling"))
                        .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                        );
                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            } else {
                data = "[\n"
                        + "    {\"header\":\"Layanan\",\"value\":\"CEK STATUS TERAKHIR\"},\n"
                        + "    {\"header\":\"Informasi\",\"value\":\"Untuk sementara tidak bisa melakukan cek transakasi terakhir, silahkan hubungi customer service.]\"},\n"
                        + "]";

//                data = new JSONObject().put("data", format).toString();
                logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                logEvent.addMessage(data);
                log.info(logEvent);
                return data;
            }

        }
    }

    //==========================================================================================================
    public JSONObject getFormatNotif(MNotifikasiTrx trx, String status, String productName) {
        String description = trx.getDescription().toLowerCase();

        try {
            logEvent.addMessage("--Format Transaksi Terakhir--");
            logEvent.addMessage("get Format Notif retun JSON");
            logEvent.addMessage("Status         : " + status);
            logEvent.addMessage("Product Name   : " + productName);
            logEvent.addMessage("Description    : " + description);
            logEvent.addMessage("trx id         : " + trx.getId());
            logEvent.addMessage("trx add        : " + trx.getAdditionalData());
            logEvent.addMessage("trx cr_time    : " + trx.getCrTime());
        } catch (Exception e) {
            log.error(e);
        }

        try {
            if (status.equalsIgnoreCase("SUCCESS") || status.equalsIgnoreCase("SUKSES") || status.equalsIgnoreCase("PROCESS")) {
                logEvent.addMessage("Success case");
                String format = "";
                JSONArray array = new JSONArray();
                JSONObject bankResponseDetail = new JSONObject(trx.getBankResponseDetail());
                logEvent.addMessage("bank response detail : " + bankResponseDetail);
//                SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
                String trxDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(trx.getCrTime().toString());
                JSONObject data;

                switch (description) {
                    case "setor tunai":
                        format = "[\n"
                                + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                                + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                                + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                                + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                                + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                                + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                                + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                                + "]";
//                    format = format.replace("[LAYANAN]", trx.getDescription())
                        format = format.replace("[LAYANAN]", "Penyetoran Tunai")
                                .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                                .replace("[WAKTU]", trxDate)
                                .replace("[NOMINAL]", Utility.formatAmount(trx.getAmount()))
                                .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                                .replace("[NAMA]", trx.getNasabah().getName())
                                .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                                );

                        array = new JSONArray(format);
                        data = new JSONObject().put("data", array);

                        logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                        logEvent.addMessage(data);
                        log.info(logEvent);
                        return data;
                    case "tarik tunai":
                        format = "[\n"
                                + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                                + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                                + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                                + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                                + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                                + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                                + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                                + "]";

//                    format = format.replace("[LAYANAN]", trx.getDescription())
                        format = format.replace("[LAYANAN]", "Penarikan Tunai")
                                //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                                .replace("[WAKTU]", trxDate)
                                .replace("[NOMINAL]", Utility.formatAmount(trx.getAmount()))
                                .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                                .replace("[NAMA]", trx.getNasabah().getName())
                                .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                                );

                        array = new JSONArray(format);
                        data = new JSONObject().put("data", array);

                        logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                        logEvent.addMessage(data);
                        log.info(logEvent);

                        return data;

                    case "transfer on us":
                        format = "[\n"
                                + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                                //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                                + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                                + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                                + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                                + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                                + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                                + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                                + "]";
                        format = format.replace("[LAYANAN]", trx.getDescription())
                                //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                                .replace("[WAKTU]", trxDate)
                                .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                                .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                                .replace("[ACC_NAME]", trx.getInfo())
                                .replace("[NOMINAL]", trx.getNasabah().getName())
                                .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                                );

                        array = new JSONArray(format);
                        data = new JSONObject().put("data", array);

                        logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                        logEvent.addMessage(data);
                        log.info(logEvent);

                        return data;

                    case "transfer lakupandai":
                        format = "[\n"
                                + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                                //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                                + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                                + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                                + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                                + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                                + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                                + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                                + "]";

                        format = format.replace("[LAYANAN]", trx.getDescription())
                                //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                                .replace("[WAKTU]", trxDate)
                                .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                                .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                                .replace("[ACC_NAME]", trx.getInfo())
                                .replace("[NOMINAL]", trx.getNasabah().getName())
                                .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                                );

                        array = new JSONArray(format);
                        data = new JSONObject().put("data", array);

                        logEvent.addMessage(description + " - Format Transaksi Terakhir - Notif");
                        logEvent.addMessage(data);
                        log.info(logEvent);

                        return data;

                    default:

                        logEvent.addMessage(trx.getDescription());
                        String responseMobile = trx.getMobileResponse();
                        if (responseMobile == null) {
                            logEvent.addMessage("succes | pending not found | find else format");
                            return getFormatNotif(trx, "NOT FOUND", productName);
                        }

                        responseMobile = responseMobile.replace("Status transaksi " + trx.getDescription() + ", akan dikirimkan melalui SMS ke HP nasabah tersebut", bankResponseDetail.getString("responseDesc"));
                        logEvent.addMessage(trx.getDescription() + " default - Format Transaksi Terakhir - Notif");
                        log.info(logEvent);

                        try {
                            return new JSONObject(responseMobile);
                        } catch (Exception e) {
                            format = new JSONArray()
                                    .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                                    .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                                    .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                                    .put(new JSONObject().put("header", "No. Tagihan").put("value", "[BILLING]"))
                                    .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                                    .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[ADMIN]"))
                                    .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[JUMLAH_BAYAR]"))
                                    .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                                    .toString();

                            format = format
                                    .replace("[LAYANAN]", trx.getDescription())
                                    .replace("[WAKTU]", trxDate)
                                    .replace("[RESI]", bankResponseDetail.getString("rrn"))
                                    .replace("[BILLING]", bankResponseDetail.getString("idBilling"))
                                    .replace("[NOMINAL]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                                    .replace("[ADMIN]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                                    .replace("[JUMLAH_BAYAR]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)
                                            + Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                                    .replace("[STATUS]", bankResponseDetail.getString("responseDesc"));
                            array = new JSONArray(format);
                            logEvent.addMessage(format);
                            log.info(logEvent);
                            return new JSONObject().put("data", array);
                        }
                }
            } else {
                logEvent.addMessage("not success case");
                String format = "";
                JSONArray array = new JSONArray();
                JSONObject bankResponseDetail = new JSONObject(trx.getBankResponseDetail());
//                LocalDateTime trxDate = LocalDateTime.now();
//                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
                SimpleDateFormat sdfFormatPayment = new SimpleDateFormat("dd MMMMM YYYY HH:mm");
                String trxDate = FormatTanggal.tanggalLokalFromCrTimeToFullMonth(trx.getCrTime().toString());

                log.info("bank response detail: " + bankResponseDetail);

                if (description.contains("setor tunai")) {

                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";
//                format = format.replace("[LAYANAN]", trx.getDescription())
                    format = format.replace("[LAYANAN]", "Penyetoran Tunai")
                            //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[NOMINAL]", Utility.formatAmount(trx.getAmount()))
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[NAMA]", trx.getNasabah().getName())
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("tarik tunai")) {

                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[NAMA]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";
//                format = format.replace("[LAYANAN]", trx.getDescription())
                    format = format.replace("[LAYANAN]", "Penarikan Tunai")
                            //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[NOMINAL]", Utility.formatAmount(trx.getAmount()))
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[NAMA]", trx.getNasabah().getName())
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);

                } else if (description.contains("transfer on us")) {

                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";
                    format = format.replace("[LAYANAN]", trx.getDescription())
                            //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                            .replace("[ACC_NAME]", trx.getInfo())
                            .replace("[NOMINAL]", trx.getNasabah().getName())
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("transfer lakupandai")) {
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            //                        + "    {\"header\":\"No Resi\",\"value\":\"[NO_RESI]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"No HP\",\"value\":\"[MSISDN]\"},\n"
                            + "    {\"header\":\"Penerima\",\"value\":\"[ACC_NUM]\"},\n"
                            + "    {\"header\":\"Nama\",\"value\":\"[ACC_NAME]\"},\n"
                            + "    {\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"},\n"
                            + "]";
                    format = format.replace("[LAYANAN]", trx.getDescription())
                            //                            .replace("[NO_RESI]", bankResponseDetail.getString("referenceNumber"))
                            .replace("[WAKTU]", trxDate)
                            .replace("[MSISDN]", trx.getNasabah().getMsisdn())
                            .replace("[ACC_NUM]", bankResponseDetail.getString("destinationAccountNumber"))
                            .replace("[ACC_NAME]", trx.getInfo())
                            .replace("[NOMINAL]", trx.getNasabah().getName())
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc")
                            );

                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);

                } else if (description.contains("prepaid")) {

                    String additional = "";
                    MProduct mProduct = productRepository.findByProductCode(trx.getProductCode());
                    if (mProduct != null) {
                        try {
                            JSONArray dataArray = new JSONObject(mProduct.getFormatPayment()).getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject jsObj = dataArray.getJSONObject(i);
                                if (jsObj.getString("header").equals("additional")) {
                                    additional = jsObj.getString("value");
                                }
                            }
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                            .put(new JSONObject().put("header", "No Hp").put("value", "[NO_HP]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[ADMIN]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[JUMLAH_BAYAR]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                            .put(new JSONObject().put("header", "additional").put("value", additional))
                            .toString();

                    format = format
                            .replace("[LAYANAN]", trx.getDescription())
                            .replace("[WAKTU]", trxDate)
                            .replace("[RESI]", bankResponseDetail.getString("rrn"))
                            .replace("[NO_HP]", bankResponseDetail.getString("idBilling"))
                            .replace("[NOMINAL]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[ADMIN]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[JUMLAH_BAYAR]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)
                                    + Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("paket data")) {
                    log.info("paket data");

                    String additional = "";
                    MProduct mProduct = productRepository.findByProductCode(trx.getProductCode());
                    if (mProduct != null) {
                        try {
                            JSONArray dataArray = new JSONObject(mProduct.getFormatPayment()).getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject jsObj = dataArray.getJSONObject(i);
                                if (jsObj.getString("header").equals("additional")) {
                                    additional = jsObj.getString("value");
                                }
                            }
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                            .put(new JSONObject().put("header", "No Hp").put("value", "[NO_HP]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[ADMIN]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[JUMLAH_BAYAR]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                            .put(new JSONObject().put("header", "additional").put("value", additional))
                            .toString();

                    format = format
                            .replace("[LAYANAN]", trx.getDescription())
                            .replace("[WAKTU]", trxDate)
                            .replace("[RESI]", bankResponseDetail.getString("rrn"))
                            .replace("[NO_HP]", bankResponseDetail.getString("idBilling"))
                            .replace("[NOMINAL]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[ADMIN]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[JUMLAH_BAYAR]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)
                                    + Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[STATUS]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("postpaid")) {
                    JSONObject addtionalData = new JSONObject(trx.getAdditionalData());

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "No Hp").put("value", "[DestinationMsisdn]"))
                            .put(new JSONObject().put("header", "Nama").put("value", "[DestinationName]"))
                            .put(new JSONObject().put("header", "Tagihan").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[DestinationMsisdn]", bankResponseDetail.getString("idBilling"))
                            .replace("[DestinationName]", addtionalData.getString("nama"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("telkompay")) {
                    JSONObject addtionalData = new JSONObject(trx.getAdditionalData());

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "Id Biller").put("value", "[DestinationMsisdn]"))
                            .put(new JSONObject().put("header", "Nama").put("value", "[DestinationName]"))
                            .put(new JSONObject().put("header", "Tagihan").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    /*
                    {
                    "fee":"2500",
                    "accountType":"A",
                    "tax":"13",
                    "sourceAccountNumber":"004221030434",
                    "responseCode":"31",
                    "rrn":"105227637050",
                    "agentFee":"500",
                    "transactionType":"2",
                    "responseDesc":"TRANSAKSI DITOLAK OLEH BANK ANDA",
                    "productCode":"017001",
                    "idBilling":"021887904001",
                    "jobIdInquiry":"20201211105227637050",
                    "transactionAmount":"1000000",
                    "additionalData":"",
                    "channelId":"LP"
                    }                
                     */
                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[DestinationMsisdn]", bankResponseDetail.getString("idBilling"))
                            .replace("[DestinationName]", addtionalData.getString("nama"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("kereta")) {
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"Kode Bayar\",\"value\":\"[KODE_BAYAR]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                            + "]";

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "Kode Bayar").put("value", "[PaymenCode]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    /*
                {
                    "fee":"0",
                    "accountType":"C",
                    "tax":"0",
                    "sourceAccountNumber":"002262001111",
                    "responseCode":"00",
                    "rrn":"10101410152020",
                    "agentFee":"0",
                    "transactionType":"2",
                    "responseDesc":"SUKSES",
                    "productCode":"034001",
                    "idBilling":"9996489509405",
                    "jobIdInquiry":"21860010101410152020",
                    "transactionAmount":"10150000",
                    "additionalData":"{\"kodeBayar\":\"9996489509405\",\"nama\":\"AZIZ FATHUR\",\"noKereta\":\"179\",\"namaKereta\":\"PASUNDAN\",\"seats\":\"EKONOMI-4 12E\",\"tagihan\":101500,\"keterangan\":\"01 SEAT LPN-KAC 0000 03\\/01 14:10\"}",
                    "channelId":"LP"
                }
                     */
                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[PaymenCode]", bankResponseDetail.getString("idBilling"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("pbb")) {
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"N.O.P\",\"value\":\"[NO_TAGIHAN]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                            + "]";

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "No Tagihan").put("value", "[PaymenCode]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    /*
                {
                    "fee":"1500",
                    "accountType":"C",
                    "tax":"30",
                    "sourceAccountNumber":"002262001111",
                    "responseCode":"68",
                    "rrn":"30093910162020",
                    "agentFee":"1000",
                    "transactionType":"2",
                    "responseDesc":"RESPONSE TERALU LAMA",
                    "productCode":"050151",
                    "idBilling":"3404060005016107102018",
                    "jobIdInquiry":"70117730093910162020",
                    "transactionAmount":"34362300",
                    "additionalData":"{\"nop\":\"340406000501610710\",\"tahunSPPT\":\"2018\",\"namaWP\":\"YANTI KUSUMA WIJAYANTI\",\"kelurahan\":\"RASEK KIDUL\",\"pengesahan\":\"101300001079394643\",\"jmlBayar\":343623}",
                    "channelId":"LP"
                }
                     */
                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[PaymenCode]", bankResponseDetail.getString("idBilling"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("asuransi")) {
                    format = "[\n"
                            + "    {\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                            + "    {\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                            + "    {\"header\":\"No Peserta\",\"value\":\"[ID_PELANGGAN]\"},\n"
                            + "    {\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                            + "]";

                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "No Tagihan").put("value", "[PaymenCode]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[PaymenCode]", bankResponseDetail.getString("idBilling"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("pajak daerah")) {
                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "No Tagihan").put("value", "[PaymenCode]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[PaymenCode]", bankResponseDetail.getString("idBilling"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("retribusi")) {
                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "No Tagihan").put("value", "[PaymenCode]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[PaymenCode]", bankResponseDetail.getString("idBilling"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));

                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);
                } else if (description.contains("pdam")) {
                    format = new JSONArray()
                            .put(new JSONObject().put("header", "Layanan").put("value", "[ProductName]"))
                            .put(new JSONObject().put("header", "No. Resi").put("value", "[Resi]"))
                            .put(new JSONObject().put("header", "Waktu").put("value", "[Waktu]"))
                            .put(new JSONObject().put("header", "No Tagihan").put("value", "[PaymenCode]"))
                            .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[Nominal]"))
                            .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[AdminFee]"))
                            .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[TotalAmount]"))
                            .put(new JSONObject().put("header", "Status").put("value", "[Status]"))
                            .toString();

                    format = format
                            .replace("[ProductName]", trx.getDescription())
                            .replace("[Resi]", bankResponseDetail.getString("rrn"))
                            .replace("[Waktu]", trxDate)
                            .replace("[PaymenCode]", bankResponseDetail.getString("idBilling"))
                            .replace("[Nominal]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                            .replace("[AdminFee]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[TotalAmount]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)
                                    + Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                            .replace("[Status]", bankResponseDetail.getString("responseDesc"));
                    array = new JSONArray(format);
                    logEvent.addMessage(format);
                    log.info(logEvent);
                    return new JSONObject().put("data", array);

                } else {
                    String responseMobile = trx.getMobileResponse();
                    responseMobile = responseMobile.replace("Status transaksi " + trx.getDescription() + ", akan dikirimkan melalui SMS ke HP nasabah tersebut", bankResponseDetail.getString("responseDesc"));
                    logEvent.addMessage(trx.getDescription() + " default - Format Transaksi Terakhir - Notif");
                    log.info(logEvent);
                    try {
                        return new JSONObject(responseMobile);
                    } catch (Exception e) {
                        format = new JSONArray()
                                .put(new JSONObject().put("header", "Layanan").put("value", "[LAYANAN]"))
                                .put(new JSONObject().put("header", "No. Resi").put("value", "[RESI]"))
                                .put(new JSONObject().put("header", "Waktu").put("value", "[WAKTU]"))
                                .put(new JSONObject().put("header", "No. Tagihan").put("value", "[BILLING]"))
                                .put(new JSONObject().put("header", "Nominal").put("value", "Rp.[NOMINAL]"))
                                .put(new JSONObject().put("header", "Biaya Admin").put("value", "Rp.[ADMIN]"))
                                .put(new JSONObject().put("header", "Jumlah Bayar").put("value", "Rp.[JUMLAH_BAYAR]"))
                                .put(new JSONObject().put("header", "Status").put("value", "[STATUS]"))
                                .toString();

                        format = format
                                .replace("[LAYANAN]", trx.getDescription())
                                .replace("[WAKTU]", trxDate)
                                .replace("[RESI]", bankResponseDetail.getString("rrn"))
                                .replace("[BILLING]", bankResponseDetail.getString("idBilling"))
                                .replace("[NOMINAL]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                                .replace("[ADMIN]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)))
                                .replace("[JUMLAH_BAYAR]", Utility.formatAmount(Utility.getParseAmount(bankResponseDetail.getString("fee"), 1)
                                        + Utility.getParseAmount(bankResponseDetail.getString("transactionAmount"), 100)))
                                .replace("[STATUS]", bankResponseDetail.getString("responseDesc"));
                        array = new JSONArray(format);
                        logEvent.addMessage(format);
                        log.info(logEvent);
                        return new JSONObject().put("data", array);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e);
            log.info(logEvent);
            return null;
        }
    }
}
