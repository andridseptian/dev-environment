package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.ProductRepository;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class ProductListParticipant implements TransactionParticipant {

    private final ProductRepository productRepository;

    public ProductListParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        productRepository = context.getBean(ProductRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String type = dataRequest.getString(Constants.PARAM.TYPE);
        Optional<Constants.ProductType> optionalType = Arrays.asList(Constants.ProductType.values()).stream().filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
        if (!optionalType.isPresent()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Type product [" + type + "] tidak ditemukan");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        List<MProduct> listProduct = productRepository.findByTypeAndStatus(optionalType.get(), Constants.ProductStatus.ACTIVE);
        if (listProduct == null || listProduct.isEmpty()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Halaman kosong / data tidak ditemukan");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        if (optionalType.get() == Constants.ProductType.pembelian) {
            List<Object[]> listData = productRepository.listProductPembelian();
            List<String[]> listDataString = new LinkedList<>();
            listData.stream().map((arrayObj) -> {
                String[] newData = new String[4];
                newData[0] = arrayObj[0].toString();
                newData[1] = arrayObj[1].toString();
                newData[2] = arrayObj[2].toString();
                newData[3] = arrayObj[3].toString();
                return newData;
            }).forEachOrdered((newData) -> {
                listDataString.add(newData);
            });
            if (listDataString == null || listDataString.isEmpty()) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Data tidak tersedia");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            JSONObject hasil;
            try {
                hasil = new JSONObject(
                        listDataString.stream()
                                .collect(Collectors.groupingBy(s -> s[0],
                                        Collectors.mapping(
                                                s -> new JSONObject()
                                                        .put("productCode", s[1])
                                                        .put("productName", s[2])
                                                        .put("denom", s[3]),
                                                Collectors.toList()))));
            } catch (Exception ex) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Data tidak tersedia");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            String messageResponse = hasil.toString();
            ctx.log(messageResponse);
            req.setInfo(messageResponse);
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Success").put("data", new JSONObject(messageResponse)));
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return PREPARED | NO_JOIN;
        } else {
            JSONObject hasil;
            try {
                hasil = new JSONObject(
                        listProduct.stream()
                                .collect(Collectors.groupingBy(MProduct::getMenu,
                                        Collectors.mapping(
                                                s -> new JSONObject()
                                                        .put("productCode", s.getProductCode())
                                                        .put("productName", s.getProductName()),
                                                Collectors.toList()))));
            } catch (Exception ex) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Data tidak tersedia");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
            String messageResponse = hasil.toString();
            ctx.log(messageResponse);
            req.setInfo(messageResponse);
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            ctx.put(Constants.OBJ.RESP, new JSONObject().put("rm", "Success").put("data", new JSONObject(messageResponse)));
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return PREPARED | NO_JOIN;
        }
    }

}
