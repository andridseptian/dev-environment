/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.SettingRepository;
import java.io.Serializable;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ACS
 */
public class SendResponse implements AbortParticipant {

    private Log log = Log.getLog("Q2", getClass().getName());
    
    @Autowired
    private SettingRepository settingRepository;
    
    @Override
    public int prepareForAbort(long id, Serializable context) {
        return ABORTED;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.OBJ.TRANSACTION);
        long timeout = Long.parseLong(settingRepository.findOne(KeyMap.TIMEOUT_TRANSACTION.name()).getValue());
//        log.info("send " + trx.getKeyTrx() + " to space");
//        trx.setResponseTime(LocalDateTime.now());
//        trx.setResponse(ctx.get(Constants.OBJ.RESP).toString());
//        sp.out(trx.getKeyTrx(), ctx, timeout);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        MTransaction trx = (MTransaction) ctx.get(Constants.OBJ.TRANSACTION);
        long timeout = Long.parseLong(settingRepository.findOne(KeyMap.TIMEOUT_TRANSACTION.name()).getValue());
//        log.info("send " + trx.getKeyTrx() + " to space");
//        trx.setResponseTime(LocalDateTime.now());
//        trx.setResponse(trx.getRm());
//        sp.out(trx.getKeyTrx(), ctx, timeout);
    }

}
