package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import com.acs.lkpd.enums.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "m_nasabah")
public class MNasabah implements Serializable {

    private Long id;
    private String nik;
    private String accountNumber;
    private String name;
    private String dob;
    private String msisdn;
    private MAgent agentId;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private Constants.Status status;
    private String info;
    private String imei;
    private MCabang cabang;
    private int status_approval;
    private String resetMsisdn;
    private String nasabahType;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "nasabah_seq")
    @SequenceGenerator(name = "nasabah_seq", sequenceName = "nasabah_seq", allocationSize = 1, initialValue = 1)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @Column(name = "nik", length = 16, nullable = false)
    public String getNik() {
        return nik;
    }

    @Column(name = "account_number", length = 12)
    public String getAccountNumber() {
        return accountNumber;
    }

    @Column(name = "name", length = 50)
    public String getName() {
        return name;
    }

    @Column(name = "dob", length = 8)
    public String getDob() {
        return dob;
    }

    @Column(name = "msisdn", length = 15)
    public String getMsisdn() {
        return msisdn;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = MAgent.class)
    @JoinColumn(name = "agent")
    public MAgent getAgentId() {
        return agentId;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public Constants.Status getStatus() {
        return status;
    }

    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    @Column(name = "imei")
    public String getImei() {
        return imei;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MCabang.class)
    public MCabang getCabang() {
        return cabang;
    }

    @Column(name = "status_approval", columnDefinition = "int default 0")
    public int getStatus_approval() {
        return status_approval;
    }

    @Column(name = "reset_msisdn", length = 15)
    public String getResetMsisdn() {
        return resetMsisdn;
    }
    
    @Column(name = "nasabah_type", length = 1)
    public String getNasabahType() {
        return nasabahType;
    }

    //=============SET===============//
    public void setId(Long id) {
        this.id = id;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setAgentId(MAgent agentId) {
        this.agentId = agentId;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setStatus(Constants.Status status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setCabang(MCabang cabang) {
        this.cabang = cabang;
    }

    public void setStatus_approval(int status_approval) {
        this.status_approval = status_approval;
    }

    public void setResetMsisdn(String resetMsisdn) {
        this.resetMsisdn = resetMsisdn;
    }

    public void setNasabahType(String nasabahType) {
        this.nasabahType = nasabahType;
    }

}
