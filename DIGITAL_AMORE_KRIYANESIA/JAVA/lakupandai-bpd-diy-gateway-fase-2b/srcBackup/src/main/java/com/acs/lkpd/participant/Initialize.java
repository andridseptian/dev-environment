/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MGambar;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.GambarRepository;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Andri D Septian
 */
public class Initialize implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    private final GambarRepository gambarRepository;
//    private final VersionRepository versionRepository;

    public Initialize() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        gambarRepository = context.getBean(GambarRepository.class);
//        versionRepository = context.getBean(VersionRepository.class);
    }

    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);

        String encodedString1 = "-";
        String encodedString2 = "-";
        String encodedString3 = "-";
        String encodedString4 = "-";
        String encodedString5 = "-";

        log.info("Initialize picture");

        JSONArray encoded = new JSONArray();
        JSONObject base64_1 = new JSONObject();
        JSONObject base64_2 = new JSONObject();
        JSONObject base64_3 = new JSONObject();
        JSONObject base64_4 = new JSONObject();
        JSONObject base64_5 = new JSONObject();

        MGambar gambar1 = gambarRepository.findOne((long) 1);
        if (gambar1.getBase64() != null) {
            encodedString1 = gambar1.getBase64();
            base64_1.put("base64", encodedString1);
            encoded.put(base64_1);
        }

        MGambar gambar2 = gambarRepository.findOne((long) 2);
        if (gambar2.getBase64() != null) {
            encodedString2 = gambar2.getBase64();
            base64_2.put("base64", encodedString2);
            encoded.put(base64_2);
        }

        MGambar gambar3 = gambarRepository.findOne((long) 3);
        if (gambar3.getBase64() != null) {
            encodedString3 = gambar3.getBase64();
            base64_3.put("base64", encodedString3);
            encoded.put(base64_3);
        }

        MGambar gambar4 = gambarRepository.findOne((long) 4);
        if (gambar4.getBase64() != null) {
            encodedString4 = gambar4.getBase64();
            base64_4.put("base64", encodedString4);
            encoded.put(base64_4);
        }

        MGambar gambar5 = gambarRepository.findOne((long) 5);
        if (gambar5.getBase64() != null) {
            encodedString5 = gambar5.getBase64();
            base64_5.put("base64", encodedString5);
            encoded.put(base64_5);
        }

        JSONObject data = new JSONObject();
        data.put("image_slider", encoded);
        data.put("version", "1.2.0");

        JSONArray ArrVersion = new JSONArray();

//        List<MVersion> lVersion = versionRepository.findByStatusOrderByCrTimeDesc("1");
//        for (MVersion ver : lVersion) {
//            ArrVersion.put(ver.getVersion());
//        }

//        data.put("version", ArrVersion);
        data.put("version", "1.1.0");

        JSONObject response = new JSONObject();
        response.put("data", data);
        req.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.OBJ.RESP, response);
        ctx.put(Constants.OBJ.WSREQUEST, req);

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
