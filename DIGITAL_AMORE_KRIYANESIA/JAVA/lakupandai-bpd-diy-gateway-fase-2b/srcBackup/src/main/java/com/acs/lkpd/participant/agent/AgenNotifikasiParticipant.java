/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.agent;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.TransactionRepository;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Andri D. Septian
 */
public class AgenNotifikasiParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private final NotificationTrxRepository notificationTrxRepository;
    private final TransactionRepository transactionRepository;

    public AgenNotifikasiParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationTrxRepository = context.getBean(NotificationTrxRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);

        ctx.log("AMBIL LIST NOTIFIKASI");
        List<MNotifikasiTrx> listNotif = notificationTrxRepository.findTop100ByAgentOrderByCrTimeDesc(agent);
        if (listNotif == null || listNotif.isEmpty()) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Halaman kosong / data tidak ditemukan");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        log.info(listNotif);
        JSONObject response = new JSONObject();
        JSONArray arrayResp = new JSONArray();
        List<MNotifikasiTrx> trxcoy = listNotif;

        ctx.log("BUAT JSON NOTIFIKASI");
        JSONArray array = new JSONArray();
        for (MNotifikasiTrx mnt : trxcoy) {

            int longTotalAmount = (int) Math.round(mnt.getTotalAmount());
            int longAmount = (int) Math.round(mnt.getTotalAmount());

            String add = "";
            if (Constants.TRX_STATUS.SUCCESS == mnt.getStatus() | mnt.getStatus() == Constants.TRX_STATUS.PROCESS) {
                if (mnt.getNasabah() == null | mnt.getDescription().equals("Setor Tunai")) {
                    add = "D ";
                } else if (mnt.getDescription().equals("Tarik Tunai")) {
                    add = "K ";
                }
            }

            String parsingDate = "";
            try {
                String crtime = String.valueOf(mnt.getCrTime());
                String tanggal = crtime.substring(0, 16).replace('T', ' ');
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(tanggal);
                parsingDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date);
            } catch (Exception e) {
                log.error(e);
                parsingDate = String.valueOf(mnt.getCrTime()).substring(0, 16).replace('T', ' ');
            }

            JSONObject objTrx = new JSONObject();
            objTrx.put("id_notif", mnt.getId());
            objTrx.put("nominal", String.valueOf(longAmount));
            objTrx.put("date", parsingDate);
            objTrx.put("layanan", mnt.getDescription());
            objTrx.put("isRead", mnt.getIsRead());
            if (mnt.getIsRead() == null) {
                objTrx.put("isRead", "true");
            }
            objTrx.put("resi", mnt.getBankReff());
            objTrx.put("feeBank", mnt.getFeeBank());
            objTrx.put("feeAgen", mnt.getFeeAgent());
            objTrx.put("totalAmount", add + String.valueOf(longTotalAmount));
            array.put(objTrx);
        }

        ctx.log("PUT NOTIFIKASI KE JSON");
        response.put("data", array);
        arrayResp.put(array);
        req.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.OBJ.RESP, response);
        ctx.put(Constants.OBJ.WSREQUEST, req);
        return PREPARED | NO_JOIN;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
