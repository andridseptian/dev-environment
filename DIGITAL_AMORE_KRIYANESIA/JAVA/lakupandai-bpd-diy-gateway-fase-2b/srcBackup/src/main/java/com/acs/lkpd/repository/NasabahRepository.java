/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNasabah;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface NasabahRepository extends JpaRepository<MNasabah, Long> {

    MNasabah findByMsisdn(String msisdn);
    
    MNasabah findFirstByMsisdn(String msisdn);
    
    MNasabah findFirstByMsisdnOrderByCrTimeDesc(String msisdn, Constants.Status status);

    MNasabah findFirstByNikAndStatusOrderByCrTimeDesc(String nik, Constants.Status status);

    MNasabah findFirstByMsisdnOrderByCrTimeDesc(String msisdn);
    
    MNasabah findFirstByResetMsisdnOrderByCrTimeDesc(String msisdn);

    MNasabah findFirstByNikAndMsisdnOrderByCrTimeDesc(String nik, String msisdn);
}
