/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MServiceProxy;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.RcRepository;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CustomerNIKProcess implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    private RcRepository rcRepository;

    public CustomerNIKProcess() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        String[] request = (String[]) ctx.get(Constants.REQ);
        String bankResponse = cfg.get("resp", Constants.OBJ.RESP);
        int fieldNik = cfg.getInt("field_nik", 0);
        JSONObject hasil = Service.serviceCustomerInquiry(agent.getId(), request[fieldNik]);
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            }
            ctx.log("Check " + ctx.get(Constants.GROUP) + " " + rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(ctx.getString(Constants.GROUP), rc);
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rc + " | " + rm);
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                ctx.put(bankResponse, hasil.toString());
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            } else {
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;

            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration cfg)
            throws ConfigurationException {
        this.cfg = cfg;
    }

}
