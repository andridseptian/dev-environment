package com.acs.lkpd.iso;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.lkpd.repository.SessionRepository;
import com.acs.lkpd.repository.SettingRepository;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.jpos.util.NameRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class ISOGatewayRequestListener implements ISORequestListener, LogSource, Configurable {

    Configuration cfg;
    Log log;
    Logger logger;
    String realm;

    private final SettingRepository settingRepository;
    private final SessionRepository sessionRepository;

    public ISOGatewayRequestListener() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        settingRepository = context.getBean(SettingRepository.class);
        sessionRepository = context.getBean(SessionRepository.class);
    }

    @Override
    public boolean process(ISOSource isos, ISOMsg isomsg) {
        try {
            if ("0800".equals(isomsg.getMTI())) {
                isomsg.setResponseMTI();
                isomsg.set(Constants.ISO_FIELD_RC, Constants.RC_APPROVED);
                isos.send(isomsg);
            } else if ("0810".equals(isomsg.getMTI())) {
                log.info("DROP 0810 Request");
            } else if (isomsg.isResponse()) {
                log.info("DROP Late Response");
            } else {
                Context ctx = new Context();
                Space sp = SpaceFactory.getSpace();
                String txn = cfg.get("processor", "bypass-pc");
                int agentIdDe = cfg.getInt("de_agent_id", 2);
                int msgContentDe = cfg.getInt("de_msg_content", 48);
                String delimeter = cfg.get("delimeter", "\\|");
                int[] mandatoryField = new int[]{agentIdDe, 3, 7, 11, 32, 37, 41, 42, msgContentDe};
                if (isomsg.hasFields(mandatoryField)) {
                    String[] request = isomsg.getString(msgContentDe).split(delimeter);
                    
                    String fld48 = isomsg.getString(msgContentDe);
                    
                    MSession session = new MSession();
                    session.setStan(isomsg.getString(11));
                    session.setTerminal(isomsg.getString(41).trim());
                    session.setRequestClient(isomsg.getString(msgContentDe).trim());                   
                    String resp = isomsg.getString(msgContentDe).trim();
                    if (resp.contains("\"data\"")){
                        ctx.put(Constants.FLD48, isomsg.getString(msgContentDe));
                    }
                    session.setOrigin(isomsg.getString(32).trim());
                    session.setCrTime(LocalDateTime.now());
                    session.setOriginReff(isomsg.getString(37).replaceAll("[a-zA-Z]+", "").trim());
                    MSession savedSession = sessionRepository.save(session);
                    isomsg.set(Constants.ISO_FIELD_PRIVATE_DATA, settingRepository.findOne(KeyMap.RM_RC_NOT_FOUND.name()).getValue());
                    ctx.put(Constants.PARAM.USER_ID,isomsg.getString(agentIdDe));
                    ctx.put(Constants.SESSION, savedSession);
                    ctx.put(Constants.IN, isomsg);
                    ctx.put(Constants.SRC, isos);
                    ctx.put(Constants.REQ, request);
                    ctx.put(Constants.FLD48, fld48);
                    sp.out(txn, ctx, 60000);
                } else {
                    log.error("Missing Mandatory Parameter, must contain all these field : " + Arrays.toString(mandatoryField));
                    if (isomsg.isRequest()) {
                        isomsg.setResponseMTI();
                    }
                    isomsg.set(Constants.ISO_FIELD_RC, Constants.RC_MISSING_MANDATORY_PARAMETER);
                    isomsg.set(Constants.ISO_FIELD_PRIVATE_DATA, settingRepository.findOne(Constants.TIMEOUT_TRANSACTION).getValue());
                    isos.send(isomsg);
                }
            }
        } catch (ISOException | IOException isoe) {
            log.error(isoe);
        }
        return true;
    }

    @Override
    public void setLogger(Logger logger, String realm) {
        this.logger = logger;
        this.realm = realm;
    }

    @Override
    public String getRealm() {
        return this.realm;
    }

    @Override
    public Logger getLogger() {
        return this.logger;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
        log = Log.getLog("Q2", getClass().getName());
    }

}
