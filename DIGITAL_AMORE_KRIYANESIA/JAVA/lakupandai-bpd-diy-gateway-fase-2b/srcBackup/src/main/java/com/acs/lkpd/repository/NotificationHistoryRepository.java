package com.acs.lkpd.repository;

import com.acs.lkpd.model.MNotificationHistory;
import java.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ACS
 */
public interface NotificationHistoryRepository extends JpaRepository<MNotificationHistory, Long> {
    @Query("SELECT n From MNotificationHistory n WHERE n.crTime > :startDate and n.agent IN (:agent,'all') ORDER BY n.crTime DESC")
    Page<MNotificationHistory> listNotificationAgentWithPaging(
            @Param("startDate") LocalDateTime startDate,
            @Param("agent") String agent,            
            @Param("page") Pageable pageablex
    );
}
