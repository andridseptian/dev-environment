
package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MServiceProxy;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;

/**
 *
 * @author ACS
 */
public class ProcodeSwitch implements GroupSelector, Configurable {

    Configuration cfg;

    @Override
    public String select(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String key = (String) ctx.getString("GRP");
        String groups = cfg.get(key, "noRoute");
        return groups;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MServiceProxy proxy = (MServiceProxy) ctx.get(Constants.OBJ.SERVICEPROXY);
        ctx.put("GRP", proxy.getProcode());
        return PREPARED | NO_JOIN;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
