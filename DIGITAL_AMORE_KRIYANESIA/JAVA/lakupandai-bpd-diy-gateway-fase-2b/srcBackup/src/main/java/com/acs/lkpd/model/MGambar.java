/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author Andri D Septian
 */
@Entity
@Table(name = "gambar")
public class MGambar implements Serializable {
    
    private Long id;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private String Base64;
    private String Name;
    
    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }
    
    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gambar_seq")
    @SequenceGenerator(name = "gambar_seq", sequenceName = "gambar_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }
    
    @Column(name = "base64")
    @Type(type = "text")
    public String getBase64() {
        return Base64;
    }
    
    @Column(name = "name")
    @Type(type = "text")
    public String getName() {
        return Name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setBase64(String Base64) {
        this.Base64 = Base64;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    
}
