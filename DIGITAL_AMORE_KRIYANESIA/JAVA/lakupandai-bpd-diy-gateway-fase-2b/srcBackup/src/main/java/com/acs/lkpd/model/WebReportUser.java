package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author acs
 */
@Entity
@Table(name = "web_report_users")
public class WebReportUser implements Serializable {

    private Long id;
    private String name;
    private String username;
    private String password;
    private String email;
    private String jabatan;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private int status;
    private int failLogin;
    private boolean statusLogin;
    private int statusApproval;
    private String reqPwd;
    private Long idGroupMenu;
    private LocalDateTime lastActivity;
    private MCabang cabang;
    private boolean magicKey;
    private String request_name;
    private String request_id_group_menu;
    private String request_email;
    private String request_jabatan;
    private String request_cabang_kd_kantor;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "webreportuser_seq")
    @SequenceGenerator(name = "webreportuser_seq", sequenceName = "webreportuser_seq", allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    @Column(name = "fail_login")
    public int getFailLogin() {
        return failLogin;
    }

    @Column(name = "req_pwd")
    public String getReqPwd() {
        return reqPwd;
    }

    @Column(name = "id_group_menu")
    public Long getIdGroupMenu() {
        return idGroupMenu;
    }

    @Column(name = "status_login", columnDefinition = "boolean default false", nullable = false)
    public boolean isStatusLogin() {
        return statusLogin;
    }

    @Column(name = "last_activity")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getLastActivity() {
        return lastActivity;
    }

    @Column(name = "email", length = 50)
    public String getEmail() {
        return email;
    }

    @Column(name = "jabatan", length = 50)
    public String getJabatan() {
        return jabatan;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MCabang.class)
    public MCabang getCabang() {
        return cabang;
    }

    @Column(name = "magic_key", columnDefinition = "boolean default true", nullable = false)
    public boolean isMagicKey() {
        return magicKey;
    }

    @Column(name = "request_name")
    public String getRequest_name() {
        return request_name;
    }

    @Column(name = "request_id_group_menu")
    public String getRequest_id_group_menu() {
        return request_id_group_menu;
    }

    @Column(name = "request_email")
    public String getRequest_email() {
        return request_email;
    }

    @Column(name = "request_jabatan")
    public String getRequest_jabatan() {
        return request_jabatan;
    }

    @Column(name = "request_cabang_kd_kantor")
    public String getRequest_cabang_kd_kantor() {
        return request_cabang_kd_kantor;
    }

    @Column(name = "status_approval", columnDefinition = "int default 0", nullable = false)
    public int getStatusApproval() {
        return statusApproval;
    }

    //============SET=============//
    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setFailLogin(int failLogin) {
        this.failLogin = failLogin;
    }

    public void setStatusLogin(boolean statusLogin) {
        this.statusLogin = statusLogin;
    }

    public void setReqPwd(String reqPwd) {
        this.reqPwd = reqPwd;
    }

    public void setIdGroupMenu(Long idGroupMenu) {
        this.idGroupMenu = idGroupMenu;
    }

    public void setLastActivity(LocalDateTime lastActivity) {
        this.lastActivity = lastActivity;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public void setCabang(MCabang cabang) {
        this.cabang = cabang;
    }

    public void setMagicKey(boolean magicKey) {
        this.magicKey = magicKey;
    }

    public void setRequest_name(String request_name) {
        this.request_name = request_name;
    }

    public void setRequest_id_group_menu(String request_id_group_menu) {
        this.request_id_group_menu = request_id_group_menu;
    }

    public void setRequest_email(String request_email) {
        this.request_email = request_email;
    }

    public void setRequest_jabatan(String request_jabatan) {
        this.request_jabatan = request_jabatan;
    }

    public void setRequest_cabang_kd_kantor(String request_cabang_kd_kantor) {
        this.request_cabang_kd_kantor = request_cabang_kd_kantor;
    }

    public void setStatusApproval(int statusApproval) {
        this.statusApproval = statusApproval;
    }
    
}
