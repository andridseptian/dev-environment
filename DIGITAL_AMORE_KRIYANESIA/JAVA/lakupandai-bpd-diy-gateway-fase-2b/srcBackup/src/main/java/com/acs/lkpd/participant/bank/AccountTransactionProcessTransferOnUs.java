/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MAgentTax;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentTaxRepository;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AccountTransactionProcessTransferOnUs implements TransactionParticipant, Configurable {
    
    Log log = org.jpos.util.Log.getLog("Q2", getClass().getName());
    protected Configuration cfg;
    private RcRepository rcRepository;
    private NotificationTrxRepository notTransactionRepository;
    private AgentTaxRepository agentTaxRepository;
    private ProductRepository productRepository;
    private TransactionRepository transactionRepository;
    
    public AccountTransactionProcessTransferOnUs() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
        notTransactionRepository = context.getBean(NotificationTrxRepository.class);
        agentTaxRepository = context.getBean(AgentTaxRepository.class);
        productRepository = context.getBean(ProductRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
    }
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MAgent agent = session.getAgent();
        MNasabah nasabah = session.getNasabah();
        int fieldPin = cfg.getInt("field_pin", 0);

        //Set Fee
        double feeAgent = 0,
                feeBank = 0;
        try {
            feeAgent = Double.parseDouble(Service.findSettings(KeyMap.FEE_AGENT_TRANSFER_ON_US.name()));
            ctx.log("FeeAgent:" + feeAgent);
        } catch (Exception ex) {
            ctx.log(ex.getMessage());
            feeAgent = 0;
        }
        try {
            feeBank = Double.parseDouble(Service.findSettings(KeyMap.FEE_BANK_TRANSFER_ON_US.name()));
            ctx.log("FeeBank:" + feeBank);
        } catch (Exception ex) {
            ctx.log(ex.getMessage());
            feeBank = 0;
        }
//        trx.setFeeAgent(feeAgent);
//        trx.setFeeBank(feeBank);

        trx.setStatus(Constants.TRX_STATUS.PENDING);
        trx.setBankResponse("SEDANG DIPROSES");
        trx = transactionRepository.save(trx);

        //Set Amount
        double totalAmount = trx.getAmount();

//        MProduct product = productRepository.findOne(trx.getProductCode());
        MProduct product = productRepository.findOne("411300");
        
        feeAgent = product.getFeeAgent();
        feeBank = product.getFeeBank();

//        if (fee)
        Constants.AgentAccountType acType = agent.getAccountType();
        String acString = acType.toString();
        String NPWP = agent.getNpwp();
        MAgentTax mtax = agentTaxRepository.findByAccountType(acString);
        double feePajak = 0.0;
        
        if (NPWP.length() > 19) {
            NPWP = NPWP.substring(0, 19);
        }
        long NPWPin = Long.parseLong(NPWP);
        log.info("NPWP : " + NPWPin);
        if (NPWPin <= 0) {
            feePajak = feeAgent * mtax.getTaxNonNpwp() * mtax.getAddtionalTax();
            String pesan = "BUKAN NPWP"
                    + "\nfeeAgent " + feeAgent
                    + "\ntax " + mtax.getTaxNonNpwp()
                    + "\nadditional " + mtax.getAddtionalTax()
                    + "\nfeePajak " + feePajak;
            log.info(pesan);
        } else {
            feePajak = feeAgent * mtax.getTaxNpwp() * mtax.getAddtionalTax();
            String pesan = "NPWP"
                    + "\nfeeAgent " + feeAgent
                    + "\ntax " + mtax.getTaxNpwp()
                    + "\nadditional " + mtax.getAddtionalTax()
                    + "\nfeePajak " + feePajak;
            log.info(pesan);
        }
        
        feePajak = Math.round(feePajak);
        log.info("FEE PAJAK : " + feePajak);
        trx.setFeePajak(feePajak);
        transactionRepository.save(trx);
        
        JSONObject hasil = Service.serviceAccountTransaction(
                agent.getId(),
                agent.getPin(),
                nasabah.getMsisdn(),
                session.getTargetNasabahAccountNumber(),
                totalAmount,
                feeBank,
                feeAgent,
                feePajak,
                trx.getOriginReff(),
                trx.getDescription() + "[agent:" + agent.getAccountNumber() + ",nasabah:" + Utility.viewMsisdn(nasabah.getMsisdn()) + "]",
                4);
        
        log.info("BANK RESP DETAIL: " + hasil.toString());
        log.info("ID TRX :" + trx.getId());
        log.info("ORIGIN REFF TRX :" + trx.getOriginReff());
        
        trx.setBankResponseDetail(hasil.toString());
        trx = transactionRepository.save(trx);
        transactionRepository.save(trx);
        
        MNotifikasiTrx notif = new MNotifikasiTrx();
        
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            
            trx.setBankResponseDetail(hasil.toString());
            trx = transactionRepository.save(trx);
//            trx.setBankReff(hasil.getString("rrn"));

            notif.setId(trx.getId());
            notif.setCrTime(trx.getCrTime());
            notif.setOriginReff(trx.getOriginReff());
            notif.setStan(trx.getStan());
            notif.setType(Constants.TRX_TYPE.POSTING);
            notif.setDescription(trx.getDescription());
//            notif.setDescription(product.getProductName());
            notif.setStatus(Constants.TRX_STATUS.PENDING);
            notif.setNasabah(trx.getNasabah());
            notif.setAgent(trx.getAgent());
            notif.setAmount(trx.getAmount());
            notif.setFeeAgent(feeAgent);
            notif.setFeeBank(feeBank);
            notif.setTotalAmount(trx.getTotalAmount());
            notif.setProductCode(trx.getProductCode());
            notif.setOtp(session.getOtp());
            notif.setAccountType(trx.getAccountType());
            notif.setIdBilling(trx.getIdBilling());
            notif.setAdditionalData(trx.getAdditionalData());
            notif.setBankResponseDetail(hasil.toString());
            notif.setBankReff(trx.getOriginReff());
            notif.setIsRead(Boolean.FALSE);
            
            log.info("FEE AGENT: " + feeAgent);
            log.info("FEE BANK: " + feeBank);
            trx.setFeeAgent(feeAgent);
            trx.setFeeBank(feeBank);
            trx = transactionRepository.save(trx);
            transactionRepository.save(trx);

//                notif.setMobileResponse(ctx.getString(Constants.HEADER_VALUE));
            notif = notTransactionRepository.save(notif);
            ctx.put(Constants.NOTIFIKASI_LOG, notif);
            
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
                if (rc.equals(Constants.RC_APPROVED)) {
                    trx.setStatus(Constants.TRX_STATUS.SUCCESS);
                    notif.setStatus(Constants.TRX_STATUS.SUCCESS);
                } else {
                    trx.setStatus(Constants.TRX_STATUS.FAILED);
                    notif.setStatus(Constants.TRX_STATUS.FAILED);
                }
                
                trx = transactionRepository.save(trx);
                notif = notTransactionRepository.save(notif);
            }
            ctx.log("Check " + ctx.get(Constants.GROUP) + " " + rc);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(ctx.getString(Constants.GROUP), rc);
            if (mapRc == null) {
                session.setRcToClient(rc);
                session.setResponseToClient(rm);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED;
            }
            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRc() + " | " + mapRc.getRm());
            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return PREPARED;
            } else {
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED;
                
            }
        } else {
            session.setRcToClient(Constants.RC_FORMAT_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            return ABORTED;
        }
    }
    
    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }
    
    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.SUCCESS);
        ctx.put(Constants.TXLOG, trx);
    }
    
    @Override
    public void abort(long id, Serializable context) {
        Context ctx = (Context) context;
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        ctx.put(Constants.TXLOG, trx);
    }
    
}
