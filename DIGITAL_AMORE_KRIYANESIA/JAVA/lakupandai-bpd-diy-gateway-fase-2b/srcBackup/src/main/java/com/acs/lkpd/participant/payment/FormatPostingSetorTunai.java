package com.acs.lkpd.participant.payment;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.repository.NotificationRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author acs
 */
public class FormatPostingSetorTunai implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    private NotificationRepository notificationRepository;

    public FormatPostingSetorTunai() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        notificationRepository = context.getBean(NotificationRepository.class);
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setDescription(Constants.DESC_SETOR_TUNAI);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        Boolean sendSMSAgent = cfg.getBoolean("sendAgentSMS", false);
        Boolean sendSMSNasabah = cfg.getBoolean("sendNasabahSMS", true);
        Boolean sendNotifAgent = cfg.getBoolean("sendAgentNotif", false);
        if (null == product) {
            session.setRcToClient(Constants.RC_INVALID_PRODUCT);
            session.setResponseToClient("Product tidak ditemukan atau tidak terdaftar, mohon coba beberapa saat lagi / hubungi admin untuk tindakan lebih lanjut");
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.SESSION, session);
        }
        if (null == product.getFormatSMSPayment() || product.getFormatSMSPayment().isEmpty()) {
            trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS, Error format SMS");
        }
        String response = product.getFormatSMSPayment();
        String productName = product.getProductName();
        response = response
                .replace("[Amount]", "Rp." + Utility.formatAmount(session.getAmount()))
                .replace("[NasabahMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                .replace("[NasabahName]", session.getNasabah().getName().length() <= 10 ? session.getNasabah().getName() : session.getNasabah().getName().substring(0, 10))
                .replace("[Status]", trx.viewStatus())
                .replace("[Ref]", trx.getOriginReff())
                .replace("[msisdn]",Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                .replace("[Detail]", trx.getBankResponse());
        try {
            if (sendSMSAgent) {
                boolean statusSMSAgent = Utility.sendSMS(response, session.getAgent().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Agent [" + session.getAgent().getMsisdn() + "] = " + statusSMSAgent);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
        } catch (Exception ex) {
            trx.setSmsInfo(trx.getSmsInfo() + "Not Send Agent SMS, Error: " + ex);
        }
        try {
            if (sendSMSNasabah && trx.getAccountType().equals("customer")) {
                boolean statusSMSNasabah = Utility.sendSMS(response, session.getNasabah().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " Nasabah [" + session.getNasabah().getMsisdn() + "] = " + statusSMSNasabah);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
        } catch (Exception ex) {
            trx.setSmsInfo(trx.getSmsInfo() + "Not Send Nasabah SMS, Error: " + ex);
        }
        try {
            if (sendNotifAgent) {
                log.info("MASUK KE SMS NOTIF");
                MNotification notif = new MNotification();
                notif.setRrn(trx.getOriginReff());
                notif.setToken(session.getAgent().getToken());
                notif.setAgent(session.getAgent().getId());
                notif.setTitle(productName);
                notif.setImage("");
                notif.setMessage(response);
                Utility util = new Utility();
                boolean sendFcmMessage = util.sendFirebaseNotifFcm(notif.getToken(), notif.getTitle(), notif.getMessage());
                if (sendFcmMessage) {
                    notif.setStatus(Constants.FCM.STATUS.SUCCESS);
                } else {
                    notif.setStatus(Constants.FCM.STATUS.FAILED);
                }
                notificationRepository.save(notif);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send Notif " + trx.getDescription());
            }
        } catch (Exception ex) {
            trx.setSmsInfo(trx.getSmsInfo() + "error notif agent:" + ex.getMessage() + "|");
        }
        ctx.put(Constants.SESSION, session);
        ctx.put(Constants.TXLOG, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        trx.setStatus(Constants.TRX_STATUS.FAILED);
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        String response = Service.getResponseFromSettings(KeyMap.RM_FORMAT_SMS_POSTING_SETOR_TUNAI.name());
        try {
            response = response.replace("[Amount]", Utility.formatAmount(session.getAmount()))
                    .replace("[NasabahMsisdn]", Utility.viewMsisdn(session.getNasabah().getMsisdn()))
                    .replace("[NasabahName]", session.getNasabah().getName())
                    .replace("[Status]", trx.viewStatus())
                    .replace("[Ref]", trx.getOriginReff());
            if (sendSMS) {
                boolean statusSMSAgent = Utility.sendSMS(response, session.getAgent().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSMSAgent);
                boolean statusSMSNasabah = Utility.sendSMS(response, session.getNasabah().getMsisdn());
                trx.setSmsInfo(trx.getSmsInfo() + "SMS " + trx.getDescription() + " = " + statusSMSNasabah);
            } else {
                trx.setSmsInfo(trx.getSmsInfo() + "Not Send SMS " + trx.getDescription());
            }
            session.setRcToClient(Constants.RC_APPROVED);
            session.setResponseToClient(response);
        } catch (Exception ex) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(KeyMap.RM_INVALID_REQUEST.name()));
        }
        ctx.put(Constants.SESSION, session);
        ctx.put(Constants.TXLOG, trx);
    }

}
