package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class CustomerCheckParticipant implements TransactionParticipant, Configurable {

    public Configuration cfg;
    private NasabahRepository nasabahRepository;

    public CustomerCheckParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        nasabahRepository = context.getBean(NasabahRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        String checkBy = cfg.get("checkBy", "id");
        JSONObject obj = new JSONObject(req.getRequest());
        Long iUserId;
        String iMsisdn;
        MNasabah nasabah;
        switch (checkBy.toLowerCase()) {
            case "id":
                iUserId = obj.getLong(Constants.PARAM.CUSTOMER_ID);
                nasabah = nasabahRepository.findOne(iUserId);
                if (nasabah == null) {
                    req.setRm("Data Nasabah tidak ditemukan atau tidak terdaftar");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(Constants.NASABAH, nasabah);
                return PREPARED | NO_JOIN;
            case "msisdn":
                iMsisdn = obj.getString(Constants.PARAM.MSISDN);
                iMsisdn = Utility.formatMsisdn(iMsisdn);
                nasabah = nasabahRepository.findFirstByMsisdnOrderByCrTimeDesc(iMsisdn);
                if (nasabah == null) {
                    req.setRm("Nasabah tidak ditemukan atau tidak terdaftar");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(Constants.NASABAH, nasabah);
                return PREPARED | NO_JOIN;
            case "reset":
                iMsisdn = obj.getString(Constants.PARAM.RESET_MSISDN);
                iMsisdn = Utility.formatMsisdn(iMsisdn);
                nasabah = nasabahRepository.findFirstByResetMsisdnOrderByCrTimeDesc(iMsisdn);
                if (nasabah == null) {
                    req.setRm("User tidak ditemukan atau tidak terdaftar");
                    ctx.put(Constants.OBJ.WSREQUEST, req);
                    return ABORTED | NO_JOIN;
                }
                ctx.put(Constants.NASABAH, nasabah);
                return PREPARED | NO_JOIN;
            default:
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Kesalahan internal, Data pengguna tidak ditemukan");
                return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
}
