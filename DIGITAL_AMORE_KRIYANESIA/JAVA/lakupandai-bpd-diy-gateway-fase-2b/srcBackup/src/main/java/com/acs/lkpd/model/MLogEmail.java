/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author yogha
 */
@Entity
@Table(name = "m_log_email")
public class MLogEmail implements Serializable {

    private Long id;
    private String target;
    private String message;
    private String smtpHost;
    private String subject;
    private String info;
    private LocalDateTime crTime;
    private LocalDateTime modTime;

    private int status;

    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY, generator = "id_email_seq")
    @SequenceGenerator(name = "id_email_seq", sequenceName = "id_email_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "target", length = 50)
    public String getTarget() {
        return target;
    }

    @Column(name = "message")
    @Type(type = "text")
    public String getMessage() {
        return message;
    }

    @Column(name = "smtp_host")
    public String getSmtpHost() {
        return smtpHost;
    }

    @Column(name = "subject")
    public String getSubject() {
        return subject;
    }

    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    @Column(name = "cr_time", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "mod_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getModTime() {
        return modTime;
    }

    //===========SET==============//
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setModTime(LocalDateTime modTime) {
        this.modTime = modTime;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
