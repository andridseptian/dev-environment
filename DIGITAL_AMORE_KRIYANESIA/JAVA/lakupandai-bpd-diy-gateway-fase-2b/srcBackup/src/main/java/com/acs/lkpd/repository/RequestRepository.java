
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ACS
 */
public interface RequestRepository extends JpaRepository<MRequest, Long>{
    
}
