package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MProduct;
import java.io.IOException;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class ProductAdditionalResponseParticipant implements AbortParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MProduct product = (MProduct) ctx.get(Constants.PRODUCT);
        if (null == source || !source.isConnected()) {
            session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
            session.setResponseToClient("Partner Not Found/Not Connected");
            return ABORTED;
        }
        if (null == product || null == product.getFormatAdditionalInformation()) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient("Transaksi tidak dapat dilakukan, Product Not Found");
            return ABORTED;
        }
        ISOMsg in = (ISOMsg) ctx.get(Constants.IN);
        try {
            log.info("Additional Response To Client with Product");
            String format = product.getFormatAdditionalInformation()
                    .replace("[product]", product.getProductName());
            in.setResponseMTI();
            ISOMsg out = (ISOMsg) in.clone();
            out.set(39, Constants.RC_APPROVED);
            out.set(48, format);
            source.send(out);
            return PREPARED | NO_JOIN;
        } catch (IOException ex) {
            log.error(ex);
            return ABORTED | NO_JOIN;
        } catch (ISOFilter.VetoException ex) {
            log.error(ex);
            return ABORTED | NO_JOIN;
        } catch (ISOException isoe) {
            log.error(isoe);
            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOSource source = (ISOSource) ctx.get(Constants.SRC);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        if (source == null || !source.isConnected()) {
            session.setRcToClient(Constants.RC_PARTNER_NOT_FOUND);
            session.setResponseToClient("Partner Not Found/Not Connected");
        } else {
            ISOMsg in = (ISOMsg) ctx.get(Constants.IN);
            try {
                log.info("Additional Response To Client with Product");
                in.setResponseMTI();
                ISOMsg out = (ISOMsg) in.clone();
                out.set(39, session.getRcToClient());
                out.set(48, session.getResponseToClient());
                source.send(out);
            } catch (IOException ex) {
                log.error(ex);
            } catch (ISOFilter.VetoException ex) {
                log.error(ex);
            } catch (ISOException isoe) {
                log.error(isoe);
            }
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
