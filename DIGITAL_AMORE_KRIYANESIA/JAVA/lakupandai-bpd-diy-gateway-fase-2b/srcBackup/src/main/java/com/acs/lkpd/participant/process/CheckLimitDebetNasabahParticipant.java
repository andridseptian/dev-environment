/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.process;

import com.acs.lkpd.model.MSession;
import com.acs.lkpd.enums.Constants;
import com.acs.util.Service;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author acs
 */
public class CheckLimitDebetNasabahParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED|NO_JOIN;
//        Context ctx = (Context) srlzbl;
//        MSession session = (MSession) ctx.get(Constants.SESSION);
//        String[] request = (String[]) ctx.get(Constants.REQ);
//        int fieldAmount = cfg.getInt("field_amount", 0);
//        double totalTransaction = 0;
//        double transaction = 0;
//        switch(session.getNasabahType()){
//            case Constants.TYPE_NASABAH_LAKUPANDAI:
//                totalTransaction += Service.findTotalTransaction(session.getNasabahAccountNumber(), Constants.DESC_TARIK_TUNAI);
////                log.info("totalTransaksi Tarik Tunai"+totalTransaction);
//                totalTransaction += Service.findTotalTransaction(session.getNasabahAccountNumber(), Constants.DESC_TRANSFER_ON_US);
////                log.info("totalTransaksi Transfer"+totalTransaction);
//                totalTransaction += Service.findTotalTransaction(session.getNasabahAccountNumber(), Constants.DESC_PEMBELIAN_PULSA);
////                log.info("totalTransaksi Pembelian Pulsa"+totalTransaction);
//                break;
//            case Constants.TYPE_AGEN_LAKUPANDAI:
//            case Constants.TYPE_NASABAH_BIASA:
//                return PREPARED|NO_JOIN;
//            default:
//                totalTransaction = 0;
//                break;
//        }
//        try {
//            transaction = Double.valueOf(request[fieldAmount]);
//        } catch (NumberFormatException | NullPointerException ex) {
//            session.setRcToClient(Constants.RC_FORMAT_ERROR);
//            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INVALID_REQUEST));
//            ctx.put(Constants.SESSION, session);
//            return ABORTED | NO_JOIN;
//        }
//        double totalTrx = totalTransaction + transaction;
////        log.info("totalTransaction : " + totalTransaction + ", transaction :" + transaction + ", Total :" + totalTrx);
//        if (Service.isLimitDebetNasabah(totalTrx, Constants.LimitType.MONTHLY)) {
//            session.setRcToClient(Constants.RC_CARD_OVERLIMIT);
//            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_ACCOUNT_NASABAH_OVERLIMIT_DEBET));
//            ctx.put(Constants.SESSION, session);
//            return ABORTED | NO_JOIN;
//        } else {
//            return PREPARED | NO_JOIN;
//        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {

    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
