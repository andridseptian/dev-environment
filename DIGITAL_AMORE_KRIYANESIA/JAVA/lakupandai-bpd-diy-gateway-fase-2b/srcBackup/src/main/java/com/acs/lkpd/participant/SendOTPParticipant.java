package com.acs.lkpd.participant;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MOtp;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.repository.OTPRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class SendOTPParticipant implements TransactionParticipant, Configurable {

    private Configuration cfg;
    private OTPRepository otpRepository;

    public SendOTPParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        otpRepository = context.getBean(OTPRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String targetOtp = cfg.get("target", "agent");
        Boolean sendSMS = cfg.getBoolean("sendSMS", false);
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String msisdn = "";
        String descResponse = "";
        switch (targetOtp) {
            case "agent":
                MAgent agen = (MAgent) ctx.get(Constants.AGENT);
                descResponse = Service.getResponseFromSettings(KeyMap.RM_OTP_AGENT.name());
                msisdn = agen.getMsisdn();
                break;
            case "nasabah":
                MNasabah nasabah = (MNasabah) ctx.get(Constants.NASABAH);
                descResponse = Service.getResponseFromSettings(KeyMap.RM_OTP_NASABAH.name());
                msisdn = nasabah.getMsisdn();
                break;
            default:
                ctx.log("targetSMS not found, not send sms");
                return ABORTED|NO_JOIN;
        }
        MOtp otp = new MOtp(msisdn);
        MOtp savedOtp = otpRepository.save(otp);
        session.setOtp(savedOtp.getOtp());
        if (sendSMS) {
            descResponse = descResponse.replace("[OTP]", savedOtp.getOtp());
            boolean statusSMS = Utility.sendSMS(descResponse, msisdn);
            if (statusSMS) {
                ctx.log("send message success : " + msisdn + ", otp:" + otp.getOtp());
                String info = "SMS success, OTP " + otp.getOtp();
                ctx.log(info);
                session.setDetail(info);
                return PREPARED | NO_JOIN;
            } else {
                ctx.log("Status pesan tidak dapat terkirim, ulangi beberapa saat lagi");
                return ABORTED | NO_JOIN;
            }
        } else {
            return PREPARED|NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
