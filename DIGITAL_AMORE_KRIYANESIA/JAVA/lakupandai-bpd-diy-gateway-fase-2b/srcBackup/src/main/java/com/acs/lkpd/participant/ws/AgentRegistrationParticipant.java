/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.ws;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MCabang;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.AgentRepository;
import com.acs.lkpd.repository.CabangRepository;
import com.acs.lkpd.repository.NasabahRepository;
import com.acs.lkpd.repository.SettingRepository;
import com.acs.util.EncryptUtility;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AgentRegistrationParticipant implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());
    private final AgentRepository agentRepository;
    private final NasabahRepository nasabahRepository;
    private final SettingRepository settingRepository;
    private final CabangRepository cabangRepository;

    public AgentRegistrationParticipant() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        agentRepository = context.getBean(AgentRepository.class);
        nasabahRepository = context.getBean(NasabahRepository.class);
        settingRepository = context.getBean(SettingRepository.class);
        cabangRepository = context.getBean(CabangRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        JSONObject dataRequest = new JSONObject(req.getRequest());
        String iAccountNumber = dataRequest.getString(Constants.PARAM.ACCOUNT_NUMBER);
        String iMsisdn = dataRequest.getString(Constants.PARAM.MSISDN);
        iMsisdn = Utility.formatMsisdn(iMsisdn);
        String iUserId = dataRequest.getString(Constants.PARAM.USER_ID);
        String iAddress = dataRequest.getString(Constants.PARAM.ADDRESS);
        String iName = dataRequest.getString(Constants.PARAM.NAME);
        String iKodeCabang = dataRequest.getString(Constants.PARAM.KODE_CABANG);
        String iId = dataRequest.getString(Constants.PARAM.ID);
        String iNpwp = dataRequest.getString(Constants.PARAM.NPWP);
        String iAccountType = dataRequest.getString(Constants.PARAM.ACCOUNT_TYPE);
        String iAgenType = dataRequest.getString(Constants.PARAM.AGENT_TYPE);
        Service.saveSessionWeb(iId, "Registrasi Agent " + iUserId);
        MCabang cabang = cabangRepository.findOne(iKodeCabang);
        if (cabang == null) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Kode cabang tidak terdaftar dalam sistem");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        MAgent checkAgent = agentRepository.findByMsisdn(iMsisdn);
        if (checkAgent != null) {
            if (checkAgent.getStatus() == Constants.Status.ACTIVE) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Nomor Handphone sudah terdaftar sebagai Agen ");
//                checkAgent.setStatus(Constants.Status.NEED_APPROVAL);
//                agentRepository.save(checkAgent);
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            }
        }
        if (nasabahRepository.findByMsisdn(iMsisdn) != null) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Nomor Handphone sudah terdaftar menjadi Nasabah");
            ctx.put(Constants.OBJ.WSREQUEST, req);
            return ABORTED | NO_JOIN;
        }
        MAgent newAgent;
        MAgent dataAgentLama = agentRepository.findOne(iUserId);
        if (dataAgentLama != null) {
            newAgent = dataAgentLama;
        } else {
            newAgent = new MAgent();
        }

        if (dataAgentLama != null) {
            if (null == dataAgentLama.getStatus()) {
                req.setStatus(Constants.TRX_STATUS.FAILED);
                req.setRm("Status Agen tidak diketahui");
                ctx.put(Constants.OBJ.WSREQUEST, req);
                return ABORTED | NO_JOIN;
            } else {
                switch (dataAgentLama.getStatus()) {
                    case ACTIVE:
                        req.setStatus(Constants.TRX_STATUS.SUCCESS);
                        req.setRm(Service.getResponseFromSettings(KeyMap.RM_AGENT_REGISTRATION_ACTIVE.name()) + ",Status Agen dirubah menjadi Need Approval");
                        ctx.put(Constants.OBJ.WSREQUEST, req);
                        dataAgentLama.setStatus(Constants.Status.NEED_APPROVAL);
//                        agentRepository.save(dataAgentLama);
                        log.info("MASUK CASE " + dataAgentLama.getStatus());
                        ctx.put(Constants.AGENT, dataAgentLama);
                        return PREPARED;
                    case INACTIVE:
                        req.setStatus(Constants.TRX_STATUS.FAILED);
                        req.setRm(Service.getResponseFromSettings(KeyMap.RM_AGENT_REGISTRATION_INACTIVE.name()));
                        ctx.put(Constants.OBJ.WSREQUEST, req);
                        log.info("MASUK CASE " + dataAgentLama.getStatus());
                        return ABORTED | NO_JOIN;
                    case BLOCK:
                        req.setStatus(Constants.TRX_STATUS.FAILED);
                        req.setRm(Service.getResponseFromSettings(KeyMap.RM_AGENT_REGISTRATION_BLOCK.name()));
                        ctx.put(Constants.OBJ.WSREQUEST, req);
                        log.info("MASUK CASE " + dataAgentLama.getStatus());
                        return ABORTED | NO_JOIN;
                    case NEED_APPROVAL:
                        req.setStatus(Constants.TRX_STATUS.SUCCESS);
                        newAgent = dataAgentLama;
                        log.info("MASUK CASE " + dataAgentLama.getStatus());
                        break;
                    case NEED_APPROVAL_CS:
                        req.setStatus(Constants.TRX_STATUS.FAILED);
                        req.setRm(Service.getResponseFromSettings(KeyMap.RM_AGENT_REGISTRATION_NEED_APPROVAL_CS.name()));
                        ctx.put(Constants.OBJ.WSREQUEST, req);
                        log.info("MASUK CASE " + dataAgentLama.getStatus());
                        return ABORTED | NO_JOIN;
                    case REJECTED:
                        log.info("MASUK CASE " + dataAgentLama.getStatus());
                        req.setStatus(Constants.TRX_STATUS.SUCCESS);
                        newAgent = dataAgentLama;
                        break;
                    default:
                        log.info("MASUK CASE DEFAULT");
                        req.setStatus(Constants.TRX_STATUS.FAILED);
                        req.setRm(Service.getResponseFromSettings(KeyMap.RM_AGENT_REGISTRATION_NOT_FOUND.name()));
                        ctx.put(Constants.OBJ.WSREQUEST, req);
                        return ABORTED | NO_JOIN;
                }
            }
        }
        if (dataRequest.has(Constants.PARAM.IMEI)) {
            newAgent.setImei(dataRequest.getString(Constants.PARAM.IMEI));
        }
        if (iNpwp.length() > 20) {
            iNpwp = iNpwp.substring(0, 20);
        }

        newAgent.setId(iUserId);
        newAgent.setName(iName);
        newAgent.setAccountNumber(iAccountNumber);
        newAgent.setMsisdn(iMsisdn);
        newAgent.setAddress(iAddress);
        newAgent.setCabang(cabang);
        newAgent.setNpwp(iNpwp);
        newAgent.setAccountType(Constants.AgentAccountType.valueOf(iAccountType));
        newAgent.setAgentType(iAgenType);

        String pass = Utility.generateStan();
        EncryptUtility eu;
        String pin = "";
        try {
            eu = new EncryptUtility(Service.findSettings(KeyMap.DES_KEY.name()));
            pin = eu.encrypt(pass);
        } catch (Exception ex) {
            ctx.log("Enkripsi PIN Exception : " + ex.getMessage());
            pin = pass;
        }
        newAgent.setPin(pin);
        newAgent.setStatus(Constants.Status.NEED_APPROVAL);
        ctx.put(Constants.AGENT, newAgent);
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable context) {
        Context ctx = (Context) context;
        MRequest req = (MRequest) ctx.get(Constants.OBJ.WSREQUEST);
        MAgent agent = (MAgent) ctx.get(Constants.AGENT);
        MAgent saveUser;
        try {
            saveUser = agentRepository.save(agent);
        } catch (Exception ex) {
            ctx.log(ex);
            saveUser = null;
        }
        if (saveUser == null) {
            req.setStatus(Constants.TRX_STATUS.FAILED);
            req.setRm("Data Pengguna gagal disimpan, mohon hubungi admin untuk tindakan lebih lanjut");
            ctx.put(Constants.OBJ.WSREQUEST, req);
        } else {
            req.setRm("success");
            req.setStatus(Constants.TRX_STATUS.SUCCESS);
            long timeout = Long.parseLong(settingRepository.findOne(KeyMap.TIMEOUT_OTP.name()).getValue());
            JSONObject resp = new JSONObject().put(Constants.PARAM.TIMEOUT, timeout).put(Constants.PARAM.MSISDN, saveUser.getMsisdn());
            ctx.put(Constants.OBJ.WSREQUEST, req);
            ctx.put(Constants.OBJ.RESP, resp);
        }
    }

    @Override
    public void abort(long id, Serializable context) {

    }

}
