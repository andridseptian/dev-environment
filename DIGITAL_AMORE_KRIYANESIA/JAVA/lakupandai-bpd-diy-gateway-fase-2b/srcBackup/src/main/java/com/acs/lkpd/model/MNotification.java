package com.acs.lkpd.model;

import com.acs.lkpd.converter.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author ACS
 */
@Entity
@Table(name = "m_notification")
public class MNotification implements Serializable {

    private Long id;
    private String title;
    private String message;
    private String image;
    private LocalDateTime crTime;
    private LocalDateTime updateTime;
    private String rrn;
    private String agent;
    private String token;
    private int status;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "notif_seq")
    @SequenceGenerator(name = "notif_seq", sequenceName = "notif_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "cr_time", insertable = false, columnDefinition = "timestamp default now()::timestamp")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getCrTime() {
        return crTime;
    }

    @Column(name = "update_time")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    @Column(name = "title", length = 50)
    public String getTitle() {
        return title;
    }

    @Column(name = "message", length = 500)
    public String getMessage() {
        return message;
    }

    @Column(name = "image")
    public String getImage() {
        return image;
    }

    @Column(name = "rrn", length = 15)
    public String getRrn() {
        return rrn;
    }

    @Column(name = "agent", nullable = true)
    public String getAgent() {
        return agent;
    }

    @Column(name = "token", nullable = false)
    public String getToken() {
        return token;
    }

    @Column(name = "status", columnDefinition = "int default 0")
    public int getStatus() {
        return status;
    }

    @Column(name = "description")
    @Type(type = "text")
    public String getDescription() {
        return description;
    }

    //========Set===========//
    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCrTime(LocalDateTime crTime) {
        this.crTime = crTime;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

}
