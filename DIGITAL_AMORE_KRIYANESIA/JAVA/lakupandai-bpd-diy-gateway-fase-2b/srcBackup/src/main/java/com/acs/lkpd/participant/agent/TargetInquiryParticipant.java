/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.agent;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.enums.KeyMap;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author ACS
 */
public class TargetInquiryParticipant implements TransactionParticipant, Configurable {

    protected Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        MAgent agent = session.getAgent();
        String[] request = (String[]) ctx.get(Constants.REQ);
        int fieldTarget = cfg.getInt("field_target", 0);
        String fieldBy = cfg.get("field_by", "accountNumber");
        String target = request[fieldTarget];
        if (fieldBy.equals("msisdn")) {
            String msisdn = Utility.formatMsisdn(request[fieldTarget]);
            target = msisdn;
            session.setTargetNasabahMsisdn(msisdn);
        }

        String req = session.getRequestClient();

        log.info("REQUEST " + request);
//        log.info("INI ADALAH REQ " + req);

        String[] respClient = req.split("\\|");
        String[] respClient2 = req.split("|");
        String[] respClient3 = req.split("|");

        log.info("RESPCLIENT " + respClient[0]);
//        log.info("INI ADALAH RESPCLIENT" + respClient[1]);
//        log.info("INI ADALAH RESPCLIENT" + respClient[2]);
//
//        log.info("INI ADALAH RESPCLIENT 2 " + respClient2[0]);
//        log.info("INI ADALAH RESPCLIENT 2 " + respClient2[1]);
//        log.info("INI ADALAH RESPCLIENT 2 " + respClient2[2]);
//
//        log.info("INI ADALAH RESPCLIENT 3 " + respClient3[0]);
//        log.info("INI ADALAH RESPCLIENT 3 " + respClient3[1]);
//        log.info("INI ADALAH RESPCLIENT 3 " + respClient3[2]);

//        String fromMsisdn = Utility.formatMsisdn(respClient[0]);
        String fromMsisdn = respClient[0];
        
        log.info("FORMAT MSISDN : "+ Utility.formatMsisdn(respClient[0]));
        
        
        log.info("FROM : " + fromMsisdn);
        log.info("AMOUNT : " + respClient[2]);
        
        
        String targerMsisdn = Utility.formatMsisdn(respClient[1]);

        JSONObject hasil = Service.serviceInquiryAccount(agent.getId(), agent.getPin(), target, session.getOriginReff(), Utility.formatMsisdn(respClient[0]), respClient[2]);
        if (hasil.has(Constants.PARAM.WS_RESPONSECODE)) {
            String rc = hasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = "Transaksi tidak dapat dilakukan, Bank Response Error[" + rc + "]";
            if (hasil.has(Constants.PARAM.WS_RESPONSEDESC)) {
                rm = hasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            }
            if (rc.equals(Constants.RC_APPROVED)
                    && hasil.has(Constants.PARAM.WS_DESTINATIONACCOUNTNAME)
                    && hasil.has(Constants.PARAM.WS_DESTINATIONACCOUNTNUMBER)) {
                String targetName = hasil.getString(Constants.PARAM.WS_DESTINATIONACCOUNTNAME);
                String targetAccountNumber = hasil.getString(Constants.PARAM.WS_DESTINATIONACCOUNTNUMBER);
                session.setTargetNasabahName(targetName);
                session.setTargetNasabahAccountNumber(targetAccountNumber);
                ctx.put(Constants.SESSION, session);
                return PREPARED | NO_JOIN;
            } else {
                session.setRcToClient(rc);
                session.setResponseToClient(rc + " | " + rm);
                ctx.put(Constants.SESSION, session);
                return ABORTED | NO_JOIN;
            }
        } else {
            session.setRcToClient(Constants.RC_MISSING_MANDATORY_PARAMETER);
            session.setResponseToClient(KeyMap.RM_BANK_RESPONSE_ERROR.name());
            ctx.put(Constants.SESSION, session);
            return ABORTED | NO_JOIN;
        }

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
