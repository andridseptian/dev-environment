/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.participant.bank;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MAgent;
import com.acs.lkpd.model.MAgentTax;
import com.acs.lkpd.model.MNasabah;
import com.acs.lkpd.model.MNotification;
import com.acs.lkpd.model.MNotifikasiTrx;
import com.acs.lkpd.model.MProduct;
import com.acs.lkpd.model.MRc;
import com.acs.lkpd.model.MSession;
import com.acs.lkpd.model.MTransaction;
import com.acs.lkpd.repository.AgentTaxRepository;
import com.acs.lkpd.repository.NotificationTrxRepository;
import com.acs.lkpd.repository.ProductRepository;
import com.acs.lkpd.repository.RcRepository;
import com.acs.lkpd.repository.TransactionRepository;
import com.acs.util.Service;
import com.acs.util.Utility;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import org.jline.utils.Log;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.NameRegistrar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author ACS
 */
public class AccountTransactionProcessPayment implements TransactionParticipant, Configurable {

    org.jpos.util.Log log = org.jpos.util.Log.getLog("Q2", getClass().getName());
    protected Configuration cfg;
    private RcRepository rcRepository;
    private ProductRepository productRepository;
    private NotificationTrxRepository notTransactionRepository;
    private AgentTaxRepository agentTaxRepository;
    private TransactionRepository transactionRepository;

    public AccountTransactionProcessPayment() throws NameRegistrar.NotFoundException {
        ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        rcRepository = context.getBean(RcRepository.class);
        productRepository = context.getBean(ProductRepository.class);
        notTransactionRepository = context.getBean(NotificationTrxRepository.class);
        agentTaxRepository = context.getBean(AgentTaxRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MSession session = (MSession) ctx.get(Constants.SESSION);
        String[] request = (String[]) ctx.get(Constants.REQ);
        MTransaction trx = (MTransaction) ctx.get(Constants.TXLOG);
        MTransaction inqTrx = (MTransaction) ctx.get(Constants.OBJ.INQTRX);
        MAgent agent = trx.getAgent();
        MNasabah nasabah = trx.getNasabah();
        int fieldPin = cfg.getInt("field_pin", 0);
        String accountType = "A";
        String sourceAccountNumber = "";

        //check product
        MProduct product = productRepository.findOne(trx.getProductCode());
        if (null == product) {
            session.setRcToClient(Constants.RC_INTERNAL_ERROR);
            session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_PRODUCT_NOT_FOUND));
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            return ABORTED | NO_JOIN;
        }

        String productCode = product.getProductCode();
        //tipe pembayaran
        String debet = trx.getAccountType();
        switch (debet) {
            case "agent": //agent dibayar cash
                accountType = "A";
                sourceAccountNumber = agent.getAccountNumber();
                productCode = productCode.replaceFirst("5", "0");
                break;
            case "customer": //customer menggunakan akun nasabah
                accountType = "C";
                sourceAccountNumber = nasabah.getAccountNumber();
                break;
            default:
                session.setRcToClient(Constants.RC_INTERNAL_ERROR);
                session.setDetail("Invalid Source Account Number");
                session.setResponseToClient(Service.getResponseFromSettings(Constants.RM_INTERNAL_ERROR));
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED | NO_JOIN;
        }

        trx.setStatus(Constants.TRX_STATUS.PENDING);
        trx.setBankResponse("SEDANG DIPROSES");
        trx = transactionRepository.save(trx);
        ctx.put(Constants.TXLOG, trx);

//        double feeBank = Utility.getParseAmount(String.valueOf(trx.getFeeBank()), 1);
        double feeBank = product.getFeeBank();
//        double feeAgent = Utility.getParseAmount(String.valueOf(trx.getFeeAgent()), 1);
        double feeAgent = product.getFeeAgent();
//        double feePajak = product.getFeePajak();

        Constants.AgentAccountType acType = agent.getAccountType();

        String acString = acType.toString();

        String NPWP = agent.getNpwp();
        MAgentTax mtax = agentTaxRepository.findByAccountType(acString);

        double feePajak = 0.0;

        if (NPWP.length() > 19) {
            NPWP = NPWP.substring(0, 19);
        }
        long NPWPin = Long.parseLong(NPWP);
        log.info("INI ADALAH NPWP : " + NPWPin);
        if (NPWPin <= 0) {
            feePajak = product.getFeeAgent() * mtax.getTaxNonNpwp() * mtax.getAddtionalTax();
            log.info("BUKAN NPWP");
            String pesan = "BUKAN NPWP"
                    + "\nfeeAgent " + product.getFeeAgent()
                    + "\ntax " + mtax.getTaxNonNpwp()
                    + "\nadditional " + mtax.getAddtionalTax()
                    + "\nfeePajak " + feePajak;
            log.info(pesan);
        } else {
            feePajak = product.getFeeAgent() * mtax.getTaxNpwp() * mtax.getAddtionalTax();
            String pesan = "NPWP"
                    + "\nfeeAgent " + product.getFeeAgent()
                    + "\ntax " + mtax.getTaxNpwp()
                    + "\nadditional " + mtax.getAddtionalTax()
                    + "\nfeePajak " + feePajak;
            log.info(pesan);
        }

//        MTransaction
//        log.info("INI CONTEXT BULAN " + ctx.getString(Constants.JUMLAHBULAN));
//        String bulanTagihan = ctx.getString(Constants.JUMLAHBULAN);
//        int blntgh = Integer.parseInt(bulanTagihan);
//        log.info("INI BULAN INT " + blntgh);
//
//        if (product.getMenu().equals("PDAM")) {
//            feeBank = feeBank * blntgh;
//            feeAgent = feeAgent * blntgh;
//        }
        if (product.getMenu().equals("PDAM")) {

            JSONObject additionalData = new JSONObject(trx.getAdditionalData());
            String bulantgn = additionalData.getString("jmlBulanTagihan");
            log.info("BULAN TAGIHAN: " + bulantgn);
            int blntgh = Integer.parseInt(bulantgn);
            feeBank = feeBank * blntgh;
            trx.setFeeBank(feeBank);
            double amount = trx.getAmount();
            trx.setTotalAmount(amount + feeBank);
            double totalAmountPDAM = amount + feeBank;
//            feeAgent = feeAgent * blntgh;
        }

        feePajak = Math.round(feePajak);
        trx.setFeePajak(feePajak);
        transactionRepository.save(trx);

        //init inquiry
        JSONObject respHasil = Service.servicePaymentString(
                agent.getId(),
                agent.getPin(),
                sourceAccountNumber,
                productCode,
                trx.getIdBilling(),
                trx.getAmount(),
                feeBank,
                feeAgent,
                feePajak,
                trx.getAdditionalData(),
                Constants.TRANSACTION.TYPE.POSTING,
                inqTrx.getBankReff(),
                accountType,
                trx.getStan());
        if (respHasil == null) {
            return ABORTED | NO_JOIN;
        }

        ctx.log("Bank Resp : " + respHasil.toString());
        MNotifikasiTrx notif = new MNotifikasiTrx();

        if (respHasil.has(Constants.PARAM.WS_RESPONSECODE)
                && respHasil.has(Constants.PARAM.WS_RESPONSEDESC)) {

            trx.setBankResponseDetail(respHasil.toString());
            trx = transactionRepository.save(trx);
//            trx.setBankReff(respHasil.getString("rrn"));

            ctx.log("INPUT KE DATABASE");

            notif.setCrTime(trx.getCrTime());
            notif.setOriginReff(trx.getOriginReff());
            notif.setStan(trx.getStan());
            notif.setType(Constants.TRX_TYPE.POSTING);
            notif.setDescription(product.getProductName());
            notif.setStatus(Constants.TRX_STATUS.PENDING);
            notif.setNasabah(trx.getNasabah());
            notif.setAgent(trx.getAgent());
            notif.setAmount(trx.getAmount());
            notif.setFeeAgent(feeAgent);
            notif.setFeeBank(feeBank);
            notif.setTotalAmount(trx.getTotalAmount());
            notif.setProductCode(trx.getProductCode());
            notif.setOtp(session.getOtp());
            notif.setAccountType(trx.getAccountType());
            notif.setIdBilling(trx.getIdBilling());
            notif.setAdditionalData(trx.getAdditionalData());
            notif.setBankResponseDetail(respHasil.toString());
            notif.setIsRead(Boolean.FALSE);
            notif.setMobileResponse(trx.getMobileResponse());

//            notif.setMobileResponse(acString);
            String rc = respHasil.getString(Constants.PARAM.WS_RESPONSECODE);
            String rm = respHasil.getString(Constants.PARAM.WS_RESPONSEDESC);
            MRc mapRc = rcRepository.findByProcessCodeAndRc(productCode, rc);

//            log.info("============================ CEK INI MAP RC : " + mapRc.getRc() + " ======================================");
            if (mapRc == null) {
                mapRc = rcRepository.findByProcessCodeAndRc(Constants.SERVICE_PAYMENT_INQUIRY, rc);
            }
            if (mapRc == null) {
//                JSONObject mobileResp = new JSONObject();
//
//                String data = "[\n"
//                        + "{\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
//                        + "{\"header\":\"No. Tagihan\",\"value\":\"[NO_TAGIHAN]\"},\n"
//                        + "{\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
//                        + "{\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
//                        + "]";
//
//                data = data
//                        .replace("[LAYANAN]", product.getProductName())
//                        .replace("[NO_TAGIHAN]", notif.getIdBilling())
//                        .replace("[NOMINAL]", String.valueOf(notif.getAmount()))
//                        //                        .replace("[STATUS]", respHasil.getString(Constants.PARAM.WS_RESPONSEDESC));
//                        .replace("[STATUS]", respHasil.getString(Constants.PARAM.WS_RESPONSEDESC));
//
//                JSONArray dataArray = new JSONArray(data);
//
//                mobileResp.put("data", dataArray);

                String parsingDate = "";
                try {
                    String crtime = String.valueOf(notif.getCrTime());
                    String tanggal = crtime.substring(0, 16).replace('T', ' ');
                    Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(tanggal);
                    parsingDate = new SimpleDateFormat("dd-MM-yyyy hh:m").format(date);
                } catch (Exception e) {
                    log.info(e);
                }

                JSONObject mobileResp = new JSONObject();

                String data = "[\n"
                        + "{\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                        + "{\"header\":\"No. Tagihan\",\"value\":\"[NO_TAGIHAN]\"},\n"
                        + "{\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                        + "{\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                        + "{\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                        + "]";

                data = data
                        .replace("[LAYANAN]", product.getProductName())
                        .replace("[NO_TAGIHAN]", notif.getIdBilling())
                        .replace("[NOMINAL]", String.valueOf(notif.getAmount()))
                        .replace("[WAKTU]", parsingDate)
                        .replace("[STATUS]", "Transaksi Gagal, Silahkan Coba Beberapa Saat Lagi");

                JSONArray dataArray = new JSONArray(data);
                mobileResp.put("data", dataArray);

                trx.setBankResponseDetail(respHasil.toString());
                trx.setBankResponse(respHasil.getString(Constants.PARAM.WS_RESPONSEDESC));
                notif.setMobileResponse(mobileResp.toString());
                notif.setBankReff(trx.getOriginReff());
                transactionRepository.save(trx);
                notTransactionRepository.save(notif);
                session.setRcToClient(rc);
                session.setResponseToClient(rm);
                ctx.put(Constants.NOTIFTRX, notif);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED | NO_JOIN;
            }

            session.setRcToClient(mapRc.getRc());
            session.setResponseToClient(mapRc.getRc() + " | " + mapRc.getRm());

            if (Constants.TRX_STATUS.PROCESS == mapRc.getStatus()) {
                notif.setBankReff(trx.getOriginReff());
                trx.setStatus(Constants.TRX_STATUS.PROCESS);
                notif.setStatus(Constants.TRX_STATUS.PROCESS);
                trx.setBankResponse(respHasil.getString(Constants.PARAM.WS_RESPONSEDESC));
                trx = transactionRepository.save(trx);
                notTransactionRepository.save(notif);

                JSONObject additional = new JSONObject(trx.getAdditionalData());

                respHasil.put("additionalData", additional.toString());
//                respHasil.put(Constants.PARAM.WS_PAY_RRN, "-");
                session.setRcToClient(rc);
                session.setResponseToClient(rm);

                log.info("BANK RESP : " + respHasil.toString());
                ctx.put(Constants.OBJ.BANKRESP, respHasil);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.OBJ.PRODUCTPAYMENT, product);
                ctx.put(Constants.NOTIFTRX, notif);
                return PREPARED | NO_JOIN;
            }

            if (Constants.TRX_STATUS.SUCCESS == mapRc.getStatus()) {
                notif.setBankReff(respHasil.getString("rrn"));
                trx.setStatus(Constants.TRX_STATUS.SUCCESS);
                notif.setStatus(Constants.TRX_STATUS.SUCCESS);
                ctx.put(Constants.OBJ.BANKRESP, respHasil);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                ctx.put(Constants.OBJ.PRODUCTPAYMENT, product);

                trx.setStatus(Constants.TRX_STATUS.SUCCESS);
                notif.setStatus(Constants.TRX_STATUS.SUCCESS);
                trx = transactionRepository.save(trx);
                notif = notTransactionRepository.save(notif);

                ctx.put(Constants.TXLOG, trx);

                ctx.log("SELESAI");
                ctx.put(Constants.NOTIFTRX, notif);

                return PREPARED | NO_JOIN;
            } else {
                notif.setBankReff(trx.getOriginReff());
                trx.setStatus(Constants.TRX_STATUS.FAILED);
                notif.setStatus(Constants.TRX_STATUS.FAILED);
                trx = transactionRepository.save(trx);
//                notif = notTransactionRepository.save(notif);
                ctx.put(Constants.NOTIFTRX, notif);
                ctx.put(Constants.SESSION, session);
                ctx.put(Constants.TXLOG, trx);
                return ABORTED | NO_JOIN;
            }
        } else {

            JSONObject bankResp = new JSONObject();
            bankResp.put("fee", feeBank);
            bankResp.put("accountType", trx.getAgent().getAccountType());
            bankResp.put("tax", feePajak);
            bankResp.put("sourceAccountNumber", sourceAccountNumber);
            bankResp.put("responseCode", "99");
//            bankResp.put("rrn", "-");
            bankResp.put("rrn", trx.getBankReff());
            bankResp.put("agentFee", feeAgent);
            bankResp.put("transactionType", trx.getType());
            bankResp.put("responseDesc", "(TIMEOUT) Tidak mendapatkan response dari bank");
            bankResp.put("productCode", product.getProductCode());
            bankResp.put("idBilling", trx.getIdBilling());
            bankResp.put(Constants.PARAM.WS_PAY_JOBIDINQUIRY, inqTrx.getBankReff());
            bankResp.put("transactionAmount", trx.getAmount());
            bankResp.put("channelId", "-");
            bankResp.put("additionalData", trx.getAdditionalData());

            notif.setNasabah(trx.getNasabah());
            notif.setType(trx.getType());
            notif.setProductCode(productCode);
            notif.setAccountType(accountType);
            notif.setAdditionalData(trx.getAdditionalData());
            notif.setOtp(session.getOtp());
            notif.setAccountType(trx.getAccountType());
            notif.setIdBilling(trx.getIdBilling());
            notif.setIsRead(Boolean.FALSE);
            notif.setCrTime(trx.getCrTime());
//            notif.setStatus(Constants.TRX_STATUS.PENDING);
            notif.setNasabah(trx.getNasabah());
            notif.setDescription(product.getProductName());
            notif.setBankResponseDetail(bankResp.toString());
            notif.setFeeAgent(feeAgent);
            notif.setFeeBank(feeBank);
            notif.setAmount(trx.getAmount());
            notif.setTotalAmount(trx.getTotalAmount());
            notif.setAgent(agent);
            notif.setBankReff(trx.getOriginReff());
            notif.setOriginReff(trx.getOriginReff());
            notif.setStatus(Constants.TRX_STATUS.FAILED);

            String parsingDate = "";
            try {
                String crtime = String.valueOf(notif.getCrTime());
                String tanggal = crtime.substring(0, 16).replace('T', ' ');
                Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(tanggal);
                parsingDate = new SimpleDateFormat("dd-MM-yyyy hh:m").format(date);
            } catch (Exception e) {
                log.info(e);
            }

            JSONObject mobileResp = new JSONObject();

            String data = "[\n"
                    + "{\"header\":\"Layanan\",\"value\":\"[LAYANAN]\"},\n"
                    + "{\"header\":\"No. Tagihan\",\"value\":\"[NO_TAGIHAN]\"},\n"
                    + "{\"header\":\"Nominal\",\"value\":\"Rp.[NOMINAL]\"},\n"
                    + "{\"header\":\"Waktu\",\"value\":\"[WAKTU]\"},\n"
                    + "{\"header\":\"Status\",\"value\":\"[STATUS]\"}\n"
                    + "]";

            data = data
                    .replace("[LAYANAN]", product.getProductName())
                    .replace("[NO_TAGIHAN]", notif.getIdBilling())
                    .replace("[NOMINAL]", String.valueOf(notif.getAmount()))
                    .replace("[WAKTU]", parsingDate)
                    .replace("[STATUS]", "Transaksi Gagal, Silahkan Coba Beberapa Saat Lagi");

            JSONArray dataArray = new JSONArray(data);
            mobileResp.put("data", dataArray);

            notif.setMobileResponse(mobileResp.toString());

            JSONObject additional = new JSONObject(trx.getAdditionalData());
            respHasil.put("additionalData", additional.toString());
            respHasil.put(Constants.PARAM.WS_PAY_RRN, "-");
            session.setRcToClient("99");
            session.setResponseToClient("Tidak Mendapatkan Respon Dari Bank, Bank Response Error");
            session.setStan(notif.getStan());

            log.info("BANK RESP : " + bankResp.toString());

            trx.setStatus(Constants.TRX_STATUS.PROCESS);
            trx.setBankResponseDetail(bankResp.toString());
            trx.setBankResponse("TIDAK MENDAPATKAN RESPON BANK");
            trx = transactionRepository.save(trx);
            notTransactionRepository.save(notif);

//            ctx.put(Constants.OBJ.BANKRESP, respHasil);
            ctx.put(Constants.OBJ.BANKRESP, bankResp);
            ctx.put(Constants.SESSION, session);
            ctx.put(Constants.TXLOG, trx);
            ctx.put(Constants.OBJ.PRODUCTPAYMENT, product);
            ctx.put(Constants.NOTIFTRX, notif);
            return ABORTED | NO_JOIN;
//            transactionRepository.save(trx);
//            session.setRcToClient(Constants.RC_FORMAT_ERROR);
//            session.setResponseToClient("Transaksi tidak dapat dilakukan, Bank Response Error");
//            ctx.put(Constants.SESSION, session);
//            ctx.put(Constants.TXLOG, trx);
//            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        this.cfg = cfg;
    }

}
