/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author Andri D Septian
 */
@Entity
@Table(name = "version")
public class MVersion implements Serializable {
    private Long id;
    private LocalDateTime crTime;
    private LocalDateTime modTime;
    private String version;
    private String status;
    private String info;
        
    @PrePersist
    protected void onCreate() {
        this.crTime = LocalDateTime.now();
    }
    
    @PreUpdate
    protected void onUpdate() {
        this.modTime = LocalDateTime.now();
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gambar_seq")
    @SequenceGenerator(name = "gambar_seq", sequenceName = "gambar_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }
    
    @Column(name = "version")
    public String getVersion() {
        return version;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    @Column(name = "info")
    @Type(type = "text")
    public String getInfo() {
        return info;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
}
