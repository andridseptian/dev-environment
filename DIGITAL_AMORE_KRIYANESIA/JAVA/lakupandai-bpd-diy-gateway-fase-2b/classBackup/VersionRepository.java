/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.repository;

import com.acs.lkpd.model.MVersion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Andri D Septian
 */
public interface VersionRepository extends JpaRepository<MVersion, Long> {
    
    List<MVersion> findByStatusOrderByCrTimeDesc(String status);

    @Query("SELECT t From MVersion t WHERE t.status = :status ORDER BY t.crTime")
    List<MVersion> versionByStatus(
            @Param("status") String status
    );
}
