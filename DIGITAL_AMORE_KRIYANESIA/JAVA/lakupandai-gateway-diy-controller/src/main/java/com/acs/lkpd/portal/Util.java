package com.acs.lkpd.portal;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.repository.RequestRepository;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ACS
 */
@Controller
public class Util {

    /**
     *
     * @author ACS
     * @param json
     * @param keyCheck
     * @return 0 = has all data, 1 2 3.. etc = where is missing
     * @throws com.acs.julie.ctr.portal.Util.ParameterTidakDitemukanException
     *
     */
    public static JSONObject notHas(String json, String... keyCheck) throws JSONException, ParameterTidakDitemukanException {
        JSONObject data = new JSONObject(json);
        for (String s : keyCheck) {
            if (!data.has(s)) {
                throw new ParameterTidakDitemukanException("Parameter " + s + " tidak ada", (Arrays.asList(keyCheck).indexOf(s)) + 1);
            }
        }
        return data;
    }

    public static class ParameterTidakDitemukanException extends Exception {

        int fieldNotFound;

        public ParameterTidakDitemukanException(String message, int fieldNotFound) {
            super(message);
            this.fieldNotFound = fieldNotFound;
        }

        public int getFieldNotFound() {
            return fieldNotFound;
        }

        public void setFieldNotFound(int fieldNotFound) {
            this.fieldNotFound = fieldNotFound;
        }

    }

    public static MRequest createLogRequest(LogEvent le, String tag, HttpServletRequest req, String reqBody) {
        le.setTag(tag);
        MRequest newReq = new MRequest();
        if (req != null) {
            le.addMessage("Receive from : " + req.getRemoteAddr());
            newReq.setRemoteAddr(req.getRemoteAddr());
        } else {
            le.addMessage("Error         : HttpServletRequest empty/null");
        }
        if (reqBody != null) {
            le.addMessage("Body         : " + reqBody);
            newReq.setRequest(reqBody);
        }
        try {
            ApplicationContext context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
            RequestRepository requestRepository = context.getBean(RequestRepository.class);
            requestRepository.save(newReq);
            return newReq;
        } catch (Exception ex) {
            le.addMessage("failed to save : " + ex.getMessage());
            ex.getMessage();
            return null;
        }
    }
}
