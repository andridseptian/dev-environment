/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.portal;

import com.acs.lkpd.enums.Constants;
import com.acs.lkpd.model.MRequest;
import com.acs.lkpd.model.MServiceProxy;
import com.acs.lkpd.repository.ServiceProxyRepository;
import com.acs.lkpd.response.DefaultResponse;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ACS
 */
@RestController
public class WebRequestPortal {

    private final Log log = Log.getLog("Q2", "Controller " + getClass().getName());
    private final ApplicationContext context;
    private final ServiceProxyRepository serviceProxy;

    public WebRequestPortal() throws Exception {
        context = (ApplicationContext) NameRegistrar.get(Constants.OBJ.SPRING);
        serviceProxy = context.getBean(ServiceProxyRepository.class);
    }

    @PostMapping(value = "/web/*", produces = "text/plain")//consumes = "application/json",
    public Object loginProcess(@RequestBody String reqBody, HttpServletRequest req, HttpServletResponse resp) {
        Context ctx = new Context();
        LogEvent le = ctx.getLogEvent();
        MRequest request = Util.createLogRequest(le, "Service web", req, reqBody);
        MServiceProxy proxy = serviceProxy.findOne(req.getRequestURI());
        if (proxy == null || !proxy.isActive()) {
            return new DefaultResponse(Constants.WS_RC.FAILED, "Transaksi tidak dapat dilakukan, ulangi beberapa saat lagi").logPrint("Resp", le);
        }
        le.setTag(proxy.getDescription());
        String[] checkParameter = proxy.getParam().split(",");
        try {
            log.info("Incoming Request : " + reqBody);
            if (reqBody.isEmpty()) {
                return new DefaultResponse(Constants.WS_RC.FAILED, "Request tidak valid(empty)").logPrint("Resp", le);
            }
            try {
                JSONObject jsonRequest = Util.notHas(reqBody, checkParameter);
            } catch (Util.ParameterTidakDitemukanException ex) {
                log.error(ex.getMessage());
                return new DefaultResponse(Constants.WS_RC.FAILED, "Format Transaksi tidak valid(Missing Parameter)").logPrint("Resp", le);
            } catch (NullPointerException | JSONException ex) {
                log.error(ex);
                return new DefaultResponse(Constants.WS_RC.FAILED, "Format Transaksi tidak sesuai").logPrint("Resp", le);
            }
            ctx.getLogEvent().setTag(proxy.getDescription());
            Space sp = SpaceFactory.getSpace();
            long timeout = proxy.getTimeout(); //Long.parseLong(MasterParameter.get(KeyMap.TIMEOUT_TRANSACTION, "60000"));
            ctx.put(Constants.OBJ.WSREQUEST, request);
            ctx.put(Constants.OBJ.SERVICEPROXY, proxy);
            ctx.put(Constants.OBJ.HTTPREQUEST, req);

            log.info("insert request " + request.getId() + " to space[" + proxy.getTxnmgr() + "], timeout trx : " + timeout);
            sp.out(proxy.getTxnmgr(), ctx, timeout);
            log.info("waiting " + request.getId() + " from space");
            Context rspCtx = (Context) sp.in(request.getId(), timeout);
            log.info("get " + request.getId() + " from space");
            if (rspCtx == null) {
                return new DefaultResponse(Constants.WS_RC.FAILED, "Transaksi timeout, Mohon ulangi beberapa saat lagi");
            }
            MRequest processRequest = (MRequest) rspCtx.get(Constants.OBJ.WSREQUEST);
            if (processRequest.getStatus() == Constants.TRX_STATUS.SUCCESS) {
                return new DefaultResponse(Constants.WS_RC.SUCCESS, processRequest.getRm(), new JSONObject(processRequest.getResponse())).logPrint("Resp", le);
            } else {
                return new DefaultResponse(Constants.WS_RC.FAILED, processRequest.getRm()).logPrint("Resp", le);
            }
        } catch (JSONException | NullPointerException ex) {
            log.error(ex.getMessage());
            return new DefaultResponse(Constants.WS_RC.FAILED, "Request tidak valid").logPrint("Resp", le);
        } finally {
            log.info(le.toString());
        }
    }

}
