/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.lkpd.response;

import com.acs.lkpd.enums.Constants;
import com.acs.util.Utility;
import java.io.Serializable;
import org.jpos.util.LogEvent;
import org.json.JSONObject;

/**
 *
 * @author ACS
 */
public class DefaultResponse implements Serializable {

    public final String RC = "rc";
    public final String RM = "rm";
    private JSONObject json;

    public DefaultResponse(Constants.WS_RC rc, String desc) {
        json = new JSONObject().put(RC, rc.getRc()).put(RM, desc);
    }

    public DefaultResponse(Constants.WS_RC rc, String desc, JSONObject another) {
        json = Utility.merge(new JSONObject().put(RC, rc.getRc()).put(RM, desc), another);
    }

    public JSONObject toJSONObject() {
        return json;
    }

    public String logPrint() {
        return json.toString();
    }

    public String logPrint(String logHeader, LogEvent le) {
        String logjson = json.toString().length() <= 2000 ? json.toString() : "Response Terlalu Panjang";
        le.addMessage(logHeader + "\t" + json.toString());
        return json.toString();
    }

    public LogEvent DefaultResponse(Constants.WS_RC rc, String desc, LogEvent le, String tag) {
        json = new JSONObject().put(RC, rc).put(RM, desc);
        le.addMessage(tag + "         : " + this.toString());
        return le;
    }

}
