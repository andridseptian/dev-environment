/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.shc.btclient.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.json.JSONObject;

/**
 *
 * @author ROG
 */
public class d_configuration {

    public static String configFileName = "config";

    public static String comports = "COM4";
    public static String unlockDelay = "1000";
    public static String terminalID = d_computerIdentifier.get4digit();
    public static String terminalCode = "00";

    public static JSONObject getConfigJson() {
        return new JSONObject()
                .put("comports", comports)
                .put("unlockdelay", unlockDelay)
                .put("terminalcode", terminalCode)
                .put("terminalid", terminalID);
    }

    public static void readConfigFile() {
        try {
            File myObj = new File(configFileName);
            Scanner myReader = new Scanner(myObj);
            String dataTemp = "";
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
                dataTemp += data;
            }

            JSONObject JsonData = new JSONObject(dataTemp);
            comports = JsonData.getString("comports");
            unlockDelay = JsonData.getString("unlockdelay");
            terminalCode = JsonData.getString("terminalcode");

            myReader.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void writeConfigFile() {
        try {
            FileWriter myWriter = new FileWriter(configFileName);
            myWriter.write(getConfigJson().toString());
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
