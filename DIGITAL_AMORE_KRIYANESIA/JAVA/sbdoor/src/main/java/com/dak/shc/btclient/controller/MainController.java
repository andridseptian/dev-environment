package com.dak.shc.btclient.controller;

import com.dak.shc.btclient.MainApp;
import com.dak.shc.btclient.connector.MainConnector;
import com.dak.shc.btclient.data.d_configuration;
import com.fazecast.jSerialComm.SerialPort;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

public class MainController implements Initializable {
    
    private static final Logger log = LogManager.getLogger(MainController.class);
    
    public static MainConnector mainConnector;
    
    public static SerialPort port;
    
    public JSONArray serialAvailable = new JSONArray();
    
    public static boolean connected = false;
    
    private static Thread GetBtConnection;
    private static Thread CheckEchoConnection;
    
    @FXML
    public WebView webView;
    @FXML
    private Label lbInfo;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        SerialPort[] serialPorts = SerialPort.getCommPorts();
        for (SerialPort serialPort : serialPorts) {
            log.info(serialPort.getSystemPortName());
            serialAvailable.put(serialPort.getSystemPortName());
        }
        
        log.info("initialize main controller");

//        installCertificateVerificationBypassTools();
        webView.setContextMenuEnabled(false);
        webView.setFontSmoothingType(FontSmoothingType.GRAY);
        
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

//        Terminal.init();
        Platform.runLater(() -> {
            webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                    log.info("oldValue: " + oldValue);
                    log.info("newValue: " + newValue);
                    
                    if (newValue == Worker.State.SUCCEEDED) {
                        webView.setVisible(true);
                        mainConnector = new MainConnector(MainController.this);
                        JSObject window = (JSObject) webView.getEngine().executeScript("window");
                        window.setMember("mainConnector", mainConnector);
                        
                    } else if (newValue == Worker.State.FAILED) {
                        setAlertOnTopAndExit("Gagal memuat tampilan, aplikasi akan ditutup, pastikan anda sudah menginstall aplikasi dengan benar");
                    } else {
                        webView.setVisible(false);
                    }
                }
            });
            
            webView.getEngine().setOnAlert(new EventHandler<WebEvent<String>>() {
                @Override
                public void handle(WebEvent<String> event) {
                    log.info(event.getData());
                    
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setHeaderText(null);
                    alert.setContentText(event.getData());
                    alert.showAndWait();
                }
            });
            
            new Thread(() -> {
                Platform.runLater(() -> {
                    try {
                        String viewLocation = "file:///" + System.getProperty("user.dir").replace("\\", "/") + "/html/mainmenu.html";
                        webView.getEngine().load(viewLocation);
                    } catch (Exception e) {
                        e.printStackTrace();
                        setAlertOnTopAndExit("Gagal memuat tampilan, " + e.getMessage());
                    }
                });
            }).start();
        });
    }
    
    public static final void loadBluetoothLib(String Port) {
        d_configuration.readConfigFile();
        if (port != null) {
            port.closePort();
            sleep(1000);
        }
        port = SerialPort.getCommPort(Port);
        port.setComPortParameters(9600, 8, 1, 0);
        port.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
        log.info("Open port: " + port.openPort());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        if (port.openPort()) {
            Platform.runLater(() -> {
                mainConnector.javascriptConnector.call("showAlertTrue", "Bluetooth Active");
            });
            
            if (GetBtConnection != null) {
                System.out.println("Interrupt GetBtConnection");
                GetBtConnection.interrupt();
                GetBtConnection = null;
            }
            GetBtConnection = new Thread(() -> {
                PrintWriter out = new PrintWriter(port.getOutputStream(), true);
                out.println("Device Connected");
                out.flush();
                log.info("Send >> Get Connection");
                Platform.runLater(() -> {
                    mainConnector.javascriptConnector.call("setDeviceStatus", "Connected");
                    if (CheckEchoConnection != null) {
                        System.out.println("interrupt CheckEchoConnection");
                        CheckEchoConnection.interrupt();
                        CheckEchoConnection = null;
                    }
                    CheckEchoConnection = new Thread(() -> {
                        while (port.isOpen()) {
                            sleep(1000);
                            connected = false;
                            Thread task = new Thread(() -> {
                                sleep(5000);
                                if (!connected) {
                                    log.info("failed >> time out");
                                    Platform.runLater(() -> {
                                        mainConnector.javascriptConnector.call("setDeviceStatus", "Not Connected");
                                    });
                                }
                            });
                            task.start();
                            PrintWriter outEcho = new PrintWriter(port.getOutputStream(), true);
                            outEcho.println("echo");
                            outEcho.flush();
                            log.info("send >> echo");
                            connected = true;
                            task.interrupt();
                            Platform.runLater(() -> {
                                mainConnector.javascriptConnector.call("setDeviceStatus", "Connected");
                            });
                        }
                    });
                    CheckEchoConnection.start();
                });
            });
            GetBtConnection.start();
            
        } else {
            Platform.runLater(() -> {
                mainConnector.javascriptConnector.call("showAlertFalse", "Port Not Found");
            });
        }
        
    }
    
    public static void showAlert(String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(null);
            alert.initStyle(StageStyle.UTILITY);
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();
        });
    }
    
    public static void setAlertOnTopAndExit(String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();
            
            System.exit(0);
            Platform.exit();
        });
    }
    
    public void sendAlertToJS(String message) {
        new Thread(() -> {
            Platform.runLater(() -> {
                String function = "setAlert('[message]')";
                function = function.replace("[message]", message);
                webView.getEngine().executeScript(function);
                log.info("execute command - " + function);
            });
        }).start();
    }
    
    public void executeScript(String script) {
        Platform.runLater(() -> {
            webView.getEngine().executeScript(script);
        });
    }
    
    public static void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }
    
    @FXML
    private void doKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F5) {
            webView.getEngine().reload();
        }
        
        if (event.getCode() == KeyCode.F6) {
            webView.getEngine().executeScript("history.back()");
        }
        
        if (event.getCode() == KeyCode.F) {
            MainApp.mainStage.setFullScreen(true);
        }
    }
}
