package com.dak.shc.btclient;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainApp extends Application {

    private static final Logger log = LogManager.getLogger(MainApp.class);

    public static Stage mainStage;
    
    public static final boolean developement = false;


    @Override
    public void start(Stage stage) throws Exception {
        log.info("Starting App...");

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("SHC Bluetooth Client"); 
        stage.getIcons().add(new Image("/icon/shclogo.png"));
        stage.initStyle(StageStyle.DECORATED);
        stage.setMinWidth(500);
        stage.setMinHeight(700);
        stage.setMaximized(true);
        stage.setFullScreen(true);
        stage.setScene(scene);
        stage.show();
        
        mainStage = stage;

        stage.setOnCloseRequest(event -> {
            try {
                log.info("prepare for closing stage");
                Thread.sleep(1000);
                log.info("attempt close");
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
