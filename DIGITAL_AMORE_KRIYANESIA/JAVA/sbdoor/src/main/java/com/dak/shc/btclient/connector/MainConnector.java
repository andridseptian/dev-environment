/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.shc.btclient.connector;

import com.dak.shc.btclient.controller.MainController;
import com.dak.shc.btclient.data.d_configuration;
import java.io.PrintWriter;
import javafx.application.Platform;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class MainConnector {

    private static final Logger log = LogManager.getLogger(MainConnector.class);

    public JSObject javascriptConnector;
    private WebView webView;
    private MainController mc;

    public MainConnector(MainController mainController) {
        mc = mainController;
        this.webView = mainController.webView;
        try {
            javascriptConnector = (JSObject) webView.getEngine().executeScript("getJsConnector()");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public void logFromJS(String message) {
        try {
            log.info(message);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }
    }

    public void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean printLogBool(String value) {
        System.out.println(value);
        javascriptConnector.call("showAlertTrue", value);

        return true;
    }

    public void printLog(String value) {
        log.info(value);
    }

    public void saveConfiguration(String data) {
        JSONObject JsonData = new JSONObject(data);
        d_configuration.comports = JsonData.getString("comports");
        d_configuration.unlockDelay = JsonData.getString("unlockdelay");

        d_configuration.writeConfigFile();

        System.out.println("Config: ");
        System.out.println(d_configuration.getConfigJson());

        Platform.runLater(() -> {
            javascriptConnector.call("showAlertTrue", "Configuration Saved");
        });
    }

    public String getConfigurationValue() {
        d_configuration.readConfigFile();
        JSONObject value = d_configuration.getConfigJson().put("available", mc.serialAvailable);
        System.out.println(value.toString());
        return value.toString();
    }

    public void startBluetoothPort() {
        try {
            System.out.println("startBluetoothPort");
            mc.loadBluetoothLib(d_configuration.comports);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendBluetoothIn(String value) {
        sleep(1000);
        new Thread(() -> {
            try {
                System.out.println("sendBluetooth");

                JSONObject requestBody = new JSONObject()
                        .put("id_terminal", d_configuration.terminalID)
                        .put("random_code", value);

                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, requestBody.toString());
                Request request = new Request.Builder()
                        .url("http://srv.co.id:9090/shcuki/scanIn")
                        .post(body)
                        .addHeader("Content-Type", "application/json")
                        .build();

                log.info("request body: ");
                log.info(requestBody);
                Response response = client.newCall(request).execute();
                JSONObject responseBody = new JSONObject(response.body().string());
                log.info("response body: ");
                log.info(responseBody);

                if (responseBody.getString("rc").equals("00")) {
                    if (send(d_configuration.unlockDelay)) {
                        Platform.runLater(() -> {
                            javascriptConnector.call("showAlertTrue", responseBody.getString("rm"));
                        });
                    } else {
                        Platform.runLater(() -> {
                            javascriptConnector.call("showAlertFalse", "Device not connected");
                        });
                    }
                } else {
                    Platform.runLater(() -> {
                        javascriptConnector.call("showAlertFalse", responseBody.getString("rm"));
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void sendBluetoothOut(String value) {
        sleep(1000);
        new Thread(() -> {
            try {
                System.out.println("sendBluetooth");

                JSONObject requestBody = new JSONObject()
                        .put("id_terminal", d_configuration.terminalID)
                        .put("random_code", value);

                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, requestBody.toString());
                Request request = new Request.Builder()
                        .url("http://srv.co.id:9090/shcuki/scanOut")
                        .post(body)
                        .addHeader("Content-Type", "application/json")
                        .build();

                log.info("request body: ");
                log.info(requestBody);
                Response response = client.newCall(request).execute();
                JSONObject responseBody = new JSONObject(response.body().string());
                log.info("response body: ");
                log.info(responseBody);

                if (responseBody.getString("rc").equals("00")) {
                    if (send(d_configuration.unlockDelay)) {
                        Platform.runLater(() -> {
                            javascriptConnector.call("showAlertTrue", responseBody.getString("rm"));
                        });
                    } else {
                        Platform.runLater(() -> {
                            javascriptConnector.call("showAlertFalse", "Device not connected");
                        });
                    }
                } else {
                    Platform.runLater(() -> {
                        javascriptConnector.call("showAlertFalse", responseBody.getString("rm"));
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void activateTerminal(String terminalCode) {
        try {
            JSONObject requestBody = new JSONObject()
                    .put("code", terminalCode)
                    .put("terminal_id", d_configuration.terminalID);

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, requestBody.toString());
            Request request = new Request.Builder()
                    .url("http://srv.co.id:9090/shcuki/insertTerminal")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            log.info("request body: ");
            log.info(requestBody);
            Response response = client.newCall(request).execute();
            JSONObject responseBody = new JSONObject(response.body().string());
            log.info("response body: ");
            log.info(responseBody);
            if (responseBody.getString("rc").equals("00")) {
                Platform.runLater(() -> {
                    javascriptConnector.call("showAlertTrue", responseBody.getString("rm"));
                });
                d_configuration.terminalCode = terminalCode;
                d_configuration.writeConfigFile();
            } else {
                Platform.runLater(() -> {
                    javascriptConnector.call("showAlertFalse", responseBody.getString("rm"));
                });
            }
        } catch (Exception e) {
            log.error(new String(), e);
            Platform.runLater(() -> {
                javascriptConnector.call("showAlertFalse", e.getMessage());
            });
        }
    }

    public boolean send(String message) {
        try {
            if (mc.connected) {
                System.out.print("Send >> ");
                PrintWriter out = new PrintWriter(mc.port.getOutputStream(), true);
                out.println(message);
                out.flush();
                System.out.println(message);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
