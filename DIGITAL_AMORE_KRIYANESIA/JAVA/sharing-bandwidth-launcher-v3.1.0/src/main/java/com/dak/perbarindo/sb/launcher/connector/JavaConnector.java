/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.launcher.connector;

import com.dak.perbarindo.sb.launcher.controller.MainController;
import com.dak.perbarindo.sb.launcher.data.Lembaga;
import com.dak.perbarindo.sb.launcher.data.Terminal;
import com.dak.perbarindo.sb.launcher.data.Version;
import com.dak.perbarindo.sb.launcher.request.SendRequest;
import com.dak.perbarindo.sb.launcher.utility.Download;
import java.awt.Desktop;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.web.WebView;
import net.lingala.zip4j.core.ZipFile;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class JavaConnector {

    private static final Logger log = LogManager.getLogger(JavaConnector.class);

    private WebView webview;
    private JSObject jsConnector;
    private MainController mc;

    private TimerTask countingConnection;
    private Timer countingTimer;
    private long delay = 500;

    public JavaConnector(MainController main, WebView webview) {
        log.info("initialize Java Connector");
        this.webview = main.webview;
        this.mc = main;
    }

    public void log(Object obj) {
        log.info(obj);
    }

    public void executeScript(String call, String value) {
        try {
            log.info("--> execute Script " + call + " value: " + value);
            Platform.runLater(() -> {
                if (value != null) {
                    String function = call + "('[value]')";
                    webview.getEngine().executeScript(function.replace("[value]", value));
                } else {
                    String function = call + "()";
                    webview.getEngine().executeScript(function);
                }
            });
        } catch (Exception e) {
            log.error(new String(), e);
        }
    }

    boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    public void resetConfig() {
        log.info("resetConfig");
        File f = new File(System.getProperty("user.dir") + "\\config");
        if (deleteDirectory(f)) {
            log.info("deleted");
            MainController.sendAlert(Alert.AlertType.INFORMATION, "konfigurasi terhapus");
            Lembaga.setBankID(null);
            Lembaga.setLembagaID(null);
            Lembaga.setLembagaName(null);
            Lembaga.setToken(null);
        } else {
            if (f.exists()) {
                log.info("failed");
                MainController.sendAlert(Alert.AlertType.INFORMATION, "gagal menghapus konfigurasi");
            } else {
                log.info("failed");
                MainController.sendAlert(Alert.AlertType.INFORMATION, "konfigurasi sudah terhapus");
            }
        }
    }

    public void terminalActivation(String bank_id, String serial_number) {
        log.info("terminal activation");
        log.info(bank_id);
        log.info(serial_number);
        new Thread(() -> {

            if (!bank_id.startsWith("0") && bank_id.length() <= 4) {
                JSONObject regData = new JSONObject()
                        .put("bank_id", Integer.parseInt(bank_id))
                        .put("serial_number", serial_number);

                JSONObject terminal = new JSONObject()
                        .put("mac", Terminal.getMacaddress())
                        .put("win_serial", Terminal.getWinserial())
                        .put("hostname", Terminal.getHostname())
                        .put("proc_name", Terminal.getProcessorname())
                        .put("proc_id", Terminal.getProcessorid())
                        .put("uuid", Terminal.getUuid())
                        .put("bank_id", Integer.parseInt(bank_id));

                JSONObject reqBody = new JSONObject()
                        .put("reg_data", regData)
                        .put("terminal", terminal);

                JSONObject res = SendRequest.post("http://117.54.12.171:61004/sb/terminalRegistration",
                        reqBody.toString());

                Lembaga.init();
                Lembaga.setBankID(bank_id);
                if (Lembaga.writeFile()) {
                    log.info("Bank ID Updated");
                } else {
                    log.info("Bank ID Failed to Update");
                }

                if (res != null && res.has("rc")) {
                    if (res.getString("rc").equals("00")) {
                        executeScript("checkUpdate.send", null);
                    } else {
                        executeScript("checkTerminal.failed", res.getString("rm"));
                    }
                } else {
                    MainController.sendAlertExit("Tidak mendapatkan respon atau respon tidak dikenali");
                }
            } else {
                MainController.sendAlert(Alert.AlertType.ERROR, "Bank ID tidak boleh dimulai dengan angka 0 dan maksimal 4 digit");
            }

        }).start();
    }
    
    public void setSettingDisabled(){
         mc.disabledSetting();
    }

    public void checkTerminalStatus() {
        log.info("check terminal status process");

        new Thread(() -> {
            mc.enableSetting();
            Lembaga.init();

            if (Lembaga.getBankID() != null) {
                // desc: req body example
                // <editor-fold> */
                /* 
                "mac": "60-EB-69-17-A8-FE",
                "proc_name": "Intel(R) Core(TM)2 Duo CPU T6500@ 2.10GHz",
                "proc_id": "BFEBFBFF0001067A",
                "uuid": "FC689911-D2BE-F7E6-7109-00235AD95FC6a"
                 */
                // END </editor-fold> */
                JSONObject jsReq = new JSONObject()
                        .put("mac", Terminal.getMacaddress())
                        .put("proc_name", Terminal.getProcessorname())
                        .put("proc_id", Terminal.getProcessorid())
                        .put("uuid", Terminal.getUuid())
                        .put("bank_id", Lembaga.getBankID());
                JSONObject res = SendRequest.post("http://117.54.12.171:61004/sb/checkTerminalStatus",
                        jsReq.toString());

                // res = new JSONObject().put("rc", "00").put("rm", "success");
                if (res != null && res.has("rc")) {
                    if (res.getString("rc").equals("00")) {
                        executeScript("checkUpdate.send", null);
                    } else if (res.getString("rc").equals("04")) {
                        executeScript("checkTerminal.notRegistered", Lembaga.getBankID());
                    } else {
                        executeScript("checkTerminal.failed", res.getString("rm"));
                    }
                } else {
                    MainController.sendAlertExit("Tidak mendapatkan respon atau respon tidak dikenali");
                }
            } else {
                executeScript("checkTerminal.formLembaga", null);
            }
        }).start();
    }

    public void saveLembagaData(String data) {
        log.info("saveLembagaData");
        new Thread(() -> {

            try {
                JSONObject jsData = new JSONObject(data);

                if (!jsData.getString("bankid").startsWith("0") && jsData.getString("bankid").length() <= 4) {
                    Lembaga.setBankID(jsData.getString("bankid"));
                    Lembaga.setLembagaID(jsData.getString("lembagaid"));
                    Lembaga.setLembagaName(jsData.getString("lembaganame"));
                    Lembaga.setToken(jsData.getString("token"));

                    if (Lembaga.writeFile()) {
                        executeScript("pane.showMain", null);
                        executeScript("checkTerminal.send", null);
                    } else {
                        MainController.sendAlertExit("gagal menyimpan data lembaga");
                    }
                } else {
                    MainController.sendAlert(Alert.AlertType.ERROR, "Bank ID tidak boleh dimulai dengan angka 0 dan maksimal 4 digit");
                }

            } catch (Exception e) {
                log.error(new String(), e);
                MainController.sendAlertExit("gagal menyimpan data lembaga, " + e.getMessage());
            }
        }).start();

    }

    public void getLembagaData() {
        log.info("get Lembaga Data");
        new Thread(() -> {
            try {
                Lembaga.init();
                /*  Data lembaga js
                    $('#setting-lembaga-input-bankid').val() = lembaga.bankid;
                    $('#setting-lembaga-input-lembagaid').val() = lembaga.lembagaid;
                    $('#setting-lembaga-input-lembaganame').val() = lembaga.lembaganame;
                    $('#setting-lembaga-input-token').val() = lembaga.token;
                 */
                if (Lembaga.getBankID() != null) {
                    JSONObject retData = new JSONObject()
                            .put("bankid", Lembaga.getBankID())
                            .put("lembagaid", Lembaga.getLembagaID())
                            .put("lembaganame", Lembaga.getLembagaName())
                            .put("token", Lembaga.getToken());
                    executeScript("setting.setLembagaData", retData.toString());
                }
            } catch (Exception e) {
                log.error(new String(), e);
                MainController.sendAlertExit("gagal menyimpan data lembaga, " + e.getMessage());
            }
        }).start();

    }

    public void checkVersionProcess() {
        log.info("check version process");

        new Thread(() -> {
            try {
                Version.setFilelocation(System.getProperty("user.dir") + "\\config");
                Version.setFilename("\\version.conf");

                String currentversionFile = Version.readFile();

                String currentVersion = null;
                String databaseVersion = null;

                String urlPath = "http://103.28.148.203:61005/sb/check_version";
                JSONObject reqBody = new JSONObject();
                reqBody.put("type", "full");

                JSONObject data = null;

                JSONObject res = SendRequest.post(urlPath, reqBody.toString());
                if (res.getString("rc").equals("00")) {

                    data = res.getJSONObject("data");

                    currentVersion = Version.getCode();
                    databaseVersion = data.getString("version_code");

                } else {
                    if (res.has("rm")) {
                        MainController.sendAlertExit("Terjadi kesalahan pengecekan versi, " + res.getString("rm"));
                        return;
                    } else {
                        MainController.sendAlertExit("Terjadi kesalahan saat mengecek versi aplikasi, respon server tidak dikenali");
                        return;
                    }
                }

                log.info("read version config: " + Version.getFilelocation() + Version.getFilename());

                if (currentversionFile != null) {

                    Version.setCode(String.valueOf(data.getString("version_code")));
                    Version.setName(data.getString("version_name"));
                    Version.setLevel(String.valueOf(data.getInt("update_level")));
                    Version.setDesc(data.getString("description"));
                    Version.setUrl(data.getString("link"));

                    log.info("--> version comparsion ");
                    log.info("current   : " + currentVersion);
                    log.info("database  : " + databaseVersion);

                    String[] tempCurrentVersion = currentVersion.split("\\.");
                    String[] tempDatabaseVersion = databaseVersion.split("\\.");

                    for (int i = 0; i < tempDatabaseVersion.length; i++) {
                        System.out.print(tempCurrentVersion[i] + ":" + tempDatabaseVersion[i]);
                        if (Integer.parseInt(tempCurrentVersion[i])
                                < Integer.parseInt(tempDatabaseVersion[i])) {
                            System.out.print(" - Outdated");
                            executeScript("download.send", null);
//                            executeScript("checkUpdate.update", null);
                            return;
                        }
                        System.out.println("");
                    }
                    if (Version.getCode().equals(databaseVersion)) {
                        executeScript("checkUpdate.quickLaunch", null);
                    } else {
                        executeScript("checkUpdate.update", null);
                    }
                } else {

                    Version.setCode(String.valueOf(data.getString("version_code")));
                    Version.setName(data.getString("version_code"));
                    Version.setLevel(String.valueOf(data.getInt("update_level")));
                    Version.setDesc(data.getString("description"));
                    Version.setUrl(data.getString("link"));

                    executeScript("checkUpdate.newInstall", null);
                }

            } catch (Exception e) {
                log.error(new String(), e);
                MainController.sendAlertExit("Terjadi kesalahan saat mengecek versi aplikasi, " + e.getMessage());
            }
        }).start();
    }

    public void checkVersionTrigger() {
        new Thread(() -> {

        }).start();
    }

    public void downloadProcess() {

        String FILE_NAME = "\\downloaded.zip";
        String PATH = System.getProperty("user.dir");

        new Thread(() -> {
            try {
                Download down = new Download();
                if (countingTimer != null) {
                    countingTimer.cancel();
                }
                log.info("initialize counter");
                countingConnection = new TimerTask() {
                    public void run() {
                        try {
                            if (down.getPercentage() < 100) {
                                log.info(down.getPercentage());
                                downloadTrigger(String.valueOf(down.getPercentage()));
                            } else {
                                log.info(down.getPercentage());
                                log.info("download complete");
                                downloadTrigger("100");
                                countingTimer.cancel();
                            }
                        } catch (Exception e) {
                            log.error(new String(), e);
                        }
                    }
                };
                countingTimer = new Timer("Timer");
                countingTimer.schedule(countingConnection, 0, delay);
                down.get("downloaded.zip",
                        Version.getUrl());

                downloadInstall(FILE_NAME, PATH);
            } catch (Exception e) {
                log.error(new String(), e);
            }
        }).start();
    }

    public void downloadInstall(String FILE_NAME, String PATH) {
        try {
            log.info("unpacking");
            log.info(PATH + FILE_NAME);
            log.info("-->" + PATH);

            ZipFile zipFile = new ZipFile(PATH + FILE_NAME);
            zipFile.extractAll(PATH);

            Thread.sleep(3000);

            File downloaded = new File(PATH + FILE_NAME);
            if (downloaded.delete()) {
                log.info("Deleted the file: " + downloaded.getName());
            } else {
                log.info("Failed to delete the file.");
            }

//            executeScript("download.success", null);
            executeScript("checkUpdate.quickLaunch", null);

            if (Version.writeFile()) {
                log.info("version file saved");
            } else {
                log.info("version file failed to save");
            }

        } catch (Exception e) {
            log.error(new String(), e);
            MainController.sendAlertExit("Terjadi kesalahan pemasangan aplikasi, " + e.getMessage());
        }
    }

    public void downloadTrigger(String process) {
        try {
            Platform.runLater(() -> {
                String function = "download.update('[message]')";
                webview.getEngine().executeScript(function.replace("[message]", process));
            });
        } catch (Exception e) {
            log.error(new String(), e);
        }
    }

    public void launchOpenApp() {
        new Thread(() -> {
            mc.disabledSetting();
            try {
                String filename = "BPR-Sharing-Bandwidth-v3.exe";
                File file = new File(System.getProperty("user.dir") + "\\" + filename);
                Desktop.getDesktop().open(file);

                Thread.sleep(3000);
                System.exit(0);
                Platform.exit();
            } catch (Exception e) {
                log.error(new String(), e);
                try {
                    String filename = "BPR-Sharing-Bandwidth-v3.exe";
                    File file = new File(System.getProperty("user.dir") + "\\" + filename);
                    Process p = Runtime.getRuntime().exec(file.getPath());
                    Thread.sleep(1000);
                    System.exit(0);
                    Platform.exit();
                } catch (Exception eio) {
                    // TODO Auto-generated catch block
                    log.error(new String(), eio);
                    MainController.sendAlertExit("Terjadi kesalahan saat memulai aplikasi, " + eio.getMessage());
                }
            }
        }).start();
    }

    public void launchCloseApp() {
        try {
            Platform.exit();
            System.exit(0);
        } catch (Exception e) {
            log.error(new String(), e);
        }
    }

    public void refreshView() {
        mc.restartWeb();
    }
}
