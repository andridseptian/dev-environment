/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.launcher.data;

import com.dak.perbarindo.sb.launcher.utility.Command;
import com.dak.perbarindo.sb.launcher.utility.Constants;
import com.dak.perbarindo.sb.launcher.utility.TripleDES;
import java.net.InetAddress;
import java.net.NetworkInterface;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author andrids
 */
public class Terminal {

    private static final Logger log = LogManager.getLogger(Terminal.class);

    private static Command com = new Command();

    private static String macaddress = "MAC-ADDRESS-NOT-FOUND";
    private static String uuid = "UUID-NOT-FOUND";
    private static String processorid = "PROCESSORID-NOT-FOUND";
    private static String processorname = "PROCESSOR-NAME-NOT FOUND";
    private static String hostname = "HOST-NAME-NOT-FOUND";
    private static String winserial = "WIN-SERIAL-NOT-FOUND";
    private static String serial = "SERIAL-NOT-FOUND";

    public static boolean init() {
        log.info("initialize Terminal");

        try {
            try {
                InetAddress ip = InetAddress.getLocalHost();
                NetworkInterface network = NetworkInterface.getByInetAddress(ip);
                byte[] mac = network.getHardwareAddress();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < mac.length; i++) {
                    sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                }
                macaddress = sb.toString();
            } catch (Exception em) {
                log.error(new String(), em);
                try {
                    macaddress = com.getMacAddress();
                } catch (Exception ex) {
                    log.error(new String(), ex);
                    macaddress = "MAC-NOT-FOUND";
                }
            }

            uuid = strIfNull(com.getPcUUID(), "UUID-NOT-FOUND").replace(" ", "");
            processorid = strIfNull(com.getPcProcessorId(), "PROCID-NOT-FOUND").replace(" ", "");
            processorname = strIfNull(com.getPcProcessorName(), "PROCNAME-NOT-FOUND");
            hostname = strIfNull(com.getPcHostName(), "HOSTNAME-NOT-FOUND");
            winserial = strIfNull(com.getPcSerialNumber(), "WINSERIAL-NOT-FOUND");

            String getSerial = uuid + "|"
                    + processorid + "|"
                    + processorname + "|"
                    + hostname + "|"
                    + winserial;

            log.info(getSerial);
            log.info("mac address: " + macaddress);

            serial = new TripleDES(Constants.key.TDES_KEY_APP).encrypt(getSerial);
            return true;
        } catch (Exception e) {
            log.error(new String(), e);
            log.info("Gagal mendapatkan data Terminal, pastikan anda sedang tidak terhubung dengan jaringan private");
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText(null);
                alert.setContentText("Gagal mendapatkan data Terminal, pastikan anda sedang tidak terhubung dengan jaringan private, -> " + e.getMessage());
                alert.showAndWait();
                System.exit(0);
            });
            return false;
        }
    }

    public static String strIfNull(String data, String ifNull) {
        return ((data == null) ? ifNull : data);
    }

    public static Command getCom() {
        return com;
    }

    public static void setCom(Command com) {
        Terminal.com = com;
    }

    public static String getUuid() {
        return uuid;
    }

    public static void setUuid(String uuid) {
        Terminal.uuid = uuid;
    }

    public static String getProcessorid() {
        return processorid;
    }

    public static void setProcessorid(String processorid) {
        Terminal.processorid = processorid;
    }

    public static String getProcessorname() {
        return processorname;
    }

    public static void setProcessorname(String processorname) {
        Terminal.processorname = processorname;
    }

    public static String getHostname() {
        return hostname;
    }

    public static void setHostname(String hostname) {
        Terminal.hostname = hostname;
    }

    public static String getWinserial() {
        return winserial;
    }

    public static void setWinserial(String winserial) {
        Terminal.winserial = winserial;
    }

    public static String getMacaddress() {
        return macaddress;
    }

    public static void setMacaddress(String macaddress) {
        Terminal.macaddress = macaddress;
    }

    public static String getSerial() {
        return serial;
    }

    public static void setSerial(String serial) {
        Terminal.serial = serial;
    }

}
