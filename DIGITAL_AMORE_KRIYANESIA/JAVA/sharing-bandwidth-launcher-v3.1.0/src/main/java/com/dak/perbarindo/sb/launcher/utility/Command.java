/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.launcher.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Andri D Septian
 */
public class Command {

    public static ProcessBuilder processBuilder = new ProcessBuilder();
    public static Process process;

    public static String GET_ETHERNET_INFO = "getmac /fo csv /v | find \"Ethernet\"";
    public static String GET_ETHERNET_WIFI = "getmac /fo csv /v | find \"Wi-Fi\"";
    public static String GET_UUID = "wmic csproduct get uuid | find /v \"uuid\"";
    public static String GET_PROCESSORID = "wmic cpu get ProcessorId | find /v \"ProcessorId\"";
    public static String GET_PROCESSORNAME = "wmic cpu get Name | find /v \"Name\"";
    public static String GET_HOSTNAME = "hostname";
    public static String GET_SERIALNUMBER = "wmic os get \"SerialNumber\" | find /v \"SerialNumber\"";
    
    public static String DISCONNECT_VPN = "rasdial /DISCONNECT ";

    public Command() {

    }

    public String getPcUUID() {
        String getPcUUID = getCallCmd(GET_UUID);
        System.out.println(getPcUUID);
        return getPcUUID;
    }

    public String getPcProcessorName() {
        String getPcProcessorName = getCallCmd(GET_PROCESSORNAME);
        System.out.println(getPcProcessorName);
        return getPcProcessorName;
    }

    public String getPcHostName() {
        String getPcHostName = getCallCmd(GET_HOSTNAME);
        System.out.println(getPcHostName);
        return getPcHostName;
    }

    public String getPcSerialNumber() {
        String getPcSerialNumber = getCallCmd(GET_SERIALNUMBER);
        System.out.println(getPcSerialNumber);
        return getPcSerialNumber;
    }

    public String getPcProcessorId() {
        String getPcProcessorId = getCallCmd(GET_PROCESSORID);
        System.out.println(getPcProcessorId);
        return getPcProcessorId;
    }

    public String getMacAddress() {
        String[] tempArray;
        String info = getEthernetInfo();
        if (info == null || info.contains("N/A") || info.contains("Hardware not present")) {
//            System.out.println("Didn't have ethernet card, looking for Wi-Fi Adapter");
            info = getEthernetWifi();
        }
//        System.out.println(info);
        tempArray = info.split(",");
        for (int i = 0; i < tempArray.length; i++) {
            tempArray[i] = tempArray[i].replace("\"", "");
//            System.out.println(tempArray[i]);
        }
//        System.out.println("\n\nSo This is your MAC ADDRESS: " + tempArray[2]); 
        return tempArray[2];
    }

    public String getEthernetInfo() {
        String getEthernet = getCallCmd(GET_ETHERNET_INFO);
//        System.out.println(getEthernet);
        return getEthernet;
    }

    public String getEthernetWifi() {
        String getWifi = getFirstCallCmd(GET_ETHERNET_WIFI);
//        System.out.println(getWifi);
        return getWifi;
    }
    
    public String disconnectVPN() {
        String disconnectVPN = getCallCmd(DISCONNECT_VPN);
//        System.out.println(getWifi);
        return disconnectVPN;
    }

    public String getCallCmd(String Command) {
        try {
            processBuilder.command("cmd.exe", "/c", Command);
            process = processBuilder.start();

            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            String Output = null;
            while (process.isAlive()) {
                if ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.getProperty("line.separator"));
                    if (line.length() > 0) {
                        Output = line;
//                        LOGGER.info(Output);
                    }
                }
            }
            return Output;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getFirstCallCmd(String Command) {
        try {
            processBuilder.command("cmd.exe", "/c", Command);
            process = processBuilder.start();

            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            String Output = null;
            while (process.isAlive()) {
                if ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.getProperty("line.separator"));
                    if (line.length() > 0) {
                        Output = line;
                        break;
//                        LOGGER.info(Output);
                    }
                }
            }
            return Output;
        } catch (Exception e) {
            return null;
        }
    }

}
