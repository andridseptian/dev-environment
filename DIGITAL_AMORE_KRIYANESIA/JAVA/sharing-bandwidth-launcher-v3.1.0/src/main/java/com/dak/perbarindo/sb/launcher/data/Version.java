/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.launcher.data;

import com.dak.perbarindo.sb.launcher.controller.MainController;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class Version {

    private static final Logger log = LogManager.getLogger(Version.class);

    private static String filelocation;
    private static String filename;

    private static String code = null;
    private static String name = null;
    private static String url = null;
    private static String level = null;
    private static String desc = null;

    public static String readFile() {
        try {
            File myObj = new File(filelocation + filename);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
                log.info(data);
            }
            JSONObject dataIn = new JSONObject(data);

            if (dataIn.has("name")) {
                setName(dataIn.getString("name"));
            }
            if (dataIn.has("code")) {
                setCode(dataIn.getString("code"));
            }
            if (dataIn.has("url")) {
                setUrl(dataIn.getString("url"));
            }
            if (dataIn.has("level")) {
                setLevel(dataIn.getString("level"));
            }
            if (dataIn.has("desc")) {
                setDesc(dataIn.getString("desc"));
            }

            myReader.close();
            return data;
        } catch (FileNotFoundException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }

    public static boolean writeFile() {
        try {
            File dir = new File(filelocation);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.info("failed to create dir");
                    return false;
                }
            }
            File file = new File(filelocation + filename);
            if (file.createNewFile()) {
                log.info("File created: " + file.getName());
            } else {
                log.info("File already exists.");
            }
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }

        try {
            FileWriter write = new FileWriter(filelocation + filename);

            JSONObject data = new JSONObject();
            data.put("name", getName())
                    .put("code", getCode())
                    .put("url", getUrl())
                    .put("level", getLevel())
                    .put("desc", getDesc());

            log.info("write CONFIG -");
            log.info(data.toString());

            write.write(data.toString());
            write.close();
            log.info("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            log.info("An error occurred.");
            log.error(e);
            e.printStackTrace();
            return false;
        }
    }

    public static String getCode() {
        return code;
    }

    public static void setCode(String code) {
        Version.code = code;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Version.name = name;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Version.url = url;
    }

    public static String getDesc() {
        return desc;
    }

    public static void setDesc(String desc) {
        Version.desc = desc;
    }

    public static String getLevel() {
        return level;
    }

    public static void setLevel(String level) {
        Version.level = level;
    }

    public static String getFilelocation() {
        return filelocation;
    }

    public static void setFilelocation(String filelocation) {
        Version.filelocation = filelocation;
    }

    public static String getFilename() {
        return filename;
    }

    public static void setFilename(String filename) {
        Version.filename = filename;
    }

}
