/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.launcher.controller;

import com.dak.perbarindo.sb.launcher.MainApp;
import com.dak.perbarindo.sb.launcher.connector.JavaConnector;
import com.dak.perbarindo.sb.launcher.data.Terminal;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.stage.StageStyle;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author andrids
 */
public class MainController implements Initializable {

    private static final Logger log = LogManager.getLogger(MainController.class);
    private JavaConnector javaConnector;

    @FXML
    public WebView webview;
    @FXML
    public HBox tittlebar;
    @FXML
    public AnchorPane region;
    @FXML
    public Button btnSetting;
    @FXML
    public Label lbInfo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnSetting.setDisable(true);
        new Thread(() -> {
            Platform.runLater(() -> {
                if (initListener()) {
                    log.info("done Initialize");
                }
            });
        }).start();
    }

    public boolean initListener() {
        try {
            Terminal.init();
//            String viewLocation = "file:///" + System.getProperty("user.dir").replace("\\", "/") + "/html/index.html";
            String viewLocation = getClass().getResource("/html/index.html").toURI().toString();
            log.info("hit location: " + viewLocation);
            webview.setVisible(false);

            webview.getEngine().getLoadWorker().stateProperty().addListener(
                    new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                    try {
                        log.info("oldValue: " + oldValue);
                        log.info("newValue: " + newValue);

                        updateInfo("Memuat Data..." + newValue.toString());

                        if (newValue == Worker.State.SUCCEEDED) {
                            //document finished loading

                            updateInfo("Memuat Tampilan...");
                            sleep(2000);

                            webview.setVisible(true);
                            javaConnector = new JavaConnector(MainController.this, webview);
                            JSObject window = (JSObject) webview.getEngine().executeScript("window");
                            window.setMember("javaConnector", javaConnector);

                            webview.getEngine().executeScript("$('#launcher-version').html('" + MainApp.versionApp + "')");

//                        JSObject jsConnector = (JSObject) webview.getEngine().executeScript("getJsConnector()");
//                        javaConnector.initConnector(jsConnector);
                        } else {
                            webview.setVisible(false);
                        }
                    } catch (Exception e) {
                        log.error(new String(), e);
                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Information Dialog");
                            alert.initStyle(StageStyle.UTILITY);
                            alert.setHeaderText(null);
                            alert.setContentText("Gagal memuat tampilan, " + e.getMessage());
                            alert.showAndWait();
                            System.exit(0);
                        });
                    }
                }
            });

            webview.getEngine().setOnAlert(new EventHandler<WebEvent<String>>() {
                @Override
                public void handle(WebEvent<String> event) {
                    log.info(event.getData());
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Information Dialog");
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setHeaderText(null);
                        alert.setContentText(event.getData());
                        alert.showAndWait();
                    });
                }
            });
            webview.getEngine().load(viewLocation);
//            webview.getEngine().load("https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=2499");
        } catch (Exception e) {
            e.printStackTrace();
            String message = "Gagal mendapatkan data Terminal, pastikan anda sedang tidak terhubung dengan jaringan private (VPN) -> " + e.getMessage();
            log.info(message, Alert.AlertType.ERROR);
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText(null);
                alert.setContentText(message);
                alert.showAndWait();
                System.exit(0);
            });
            return false;
        }
        return true;
    }

    public void updateInfo(String message) {
        new Thread(() -> {
            Platform.runLater(() -> {
                lbInfo.setText(message);
            });
        }).start();
    }

    public static void sendAlert(Alert.AlertType type, String message) {
        try {
            Platform.runLater(() -> {
                Alert alert = new Alert(type);
                alert.setTitle(null);
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText(null);
                alert.setContentText(message);
                alert.showAndWait();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendAlertExit(String message) {
        try {
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle(null);
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText(null);
                alert.setContentText(message);
                alert.showAndWait();
                System.exit(0);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            log.error(new String(), e);
        }
    }

    public void disabledSetting() {
        Platform.runLater(() -> {
            btnSetting.setDisable(true);
        });
    }
    
    public void enableSetting() {
        Platform.runLater(() -> {
            btnSetting.setDisable(false);
        });
    }

    public void restartWeb() {
        Platform.runLater(() -> {
            webview.getEngine().reload();
        });
    }

    @FXML
    private void doMinimizeWindow(MouseEvent event) {
        MainApp.getMainStage().setIconified(true);
    }

    @FXML
    private void doExitWindow(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void onSetting(ActionEvent event) {
        log.info("--> execute Script on Setting");
        Platform.runLater(() -> {
            webview.getEngine().executeScript("pane.showSetting()");
        });
    }

}
