/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.launcher.request;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andrids
 */
public class SendRequest {
    
    private static final Logger log = LogManager.getLogger(SendRequest.class);

    public static JSONObject post(String url, String body) {
        try {
            log.info("--> send post request");
            log.info(url);
            log.info(body);
            
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody reqbody = RequestBody.create(mediaType, body);
            Request request = new Request.Builder()
                    .url(url)
                    .post(reqbody)
                    .addHeader("content-type", "application/json")
                    .build();

            Response response = client.newCall(request).execute();
            
            String responseBody = response.body().string();
            
            log.info("--> response");
            log.info(responseBody);
 
            return new JSONObject(responseBody);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            return null;
        }
    }

}
