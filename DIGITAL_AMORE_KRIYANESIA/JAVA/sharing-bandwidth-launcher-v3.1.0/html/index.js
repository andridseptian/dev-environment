
var jsConnector = {
    sendAlert: function (message) {
        console.log(message);
    }
};

function getJsConnector() {
    return jsConnector;
}

$(document).ready(function () {
    try {
        checkTerminal.send();
    } catch (error) {
        alert(error);
    }
});

class checkTerminal {
    static send() {
        try {
            $('#main-pane').html(`
                <div class="uk-animation-slide-bottom-medium">
                    <!-- center position -->
                    <div id="loading-pane" class="jumbotron jumbotron-fluid">
                        <div id="loading-tittle">
                            Memeriksa status terminal anda
                        </div>
                        <br>
                        <div>
                            <div class="spinner-grow text-success"></div>
                            <div class="spinner-grow text-info"></div>
                            <div class="spinner-grow text-warning"></div>
                        </div>
                        <br>
                        <div id="loading-subtittle">
                            Mengambil data
                        </div>
                    </div>
                </div>
            `);

            setTimeout(() => {
                javaConnector.checkTerminalStatus();
            }, 1000);

        } catch (error) {
            alert(error)
        }

    }

    static notRegistered() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        Terminal anda belum aktif, lakukan aktivasi terlebih dahulu
                    </div>
                    <br>
                    <br>
                    <div class="container">
                        <form>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width:150px">Bank ID</span>
                                </div>
                                <input id="terminal-act-bankid" type="number" class="form-control" placeholder="Bank ID">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width:150px">Serial Number</span>
                                </div>
                                <input id="terminal-act-serial" type="text" class="form-control" placeholder="Serial Number">
                            </div>
                        </form>
                        <button onclick="checkTerminal.activateTerminal()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Aktivasi</button>
                    </div>
                </div>
            </div>
        `);
    }

    static activateTerminal() {
        try {
            var bankid = $("#terminal-act-bankid").val();
            var serial = $("#terminal-act-serial").val();

            if (bankid != "") {
                javaConnector.terminalActivation(bankid, serial);
                $('#main-pane').html(`
                    <div class="uk-animation-slide-bottom-medium">
                        <!-- center position -->
                        <div id="loading-pane" class="jumbotron jumbotron-fluid">
                            <div id="loading-tittle">
                                Memeriksa versi aktivasi terminal
                            </div>
                            <br>
                            <div>
                                <div class="spinner-grow text-success"></div>
                                <div class="spinner-grow text-info"></div>
                                <div class="spinner-grow text-warning"></div>
                            </div>
                            <br>
                            <div id="loading-subtittle">
                                Memeriksa data
                            </div>
                        </div>
                    </div>
                `);
            } else {
                alert('Bank ID harus diisi dengan angka dan tidak diawali dengan angka 0');
            }
        } catch (error) {
            alert(error);
        }
    }

    static saveFormLembaga() {
        try {
            if ($('#lembaga-input-bankid').val() != "") {
                var data = JSON.stringify({
                    bankid: $('#lembaga-input-bankid').val(),
                    lembagaid: $('#lembaga-input-lembagaid').val(),
                    lembaganame: $('#lembaga-input-lembaganame').val(),
                    token: $('#lembaga-input-token').val()
                });
                javaConnector.saveLembagaData(data);
            } else {
                alert('Bank ID harus diisi dengan angka dan tidak diawali dengan angka 0');
            }
        } catch (error) {
            alert(error);
        }
    }

    static formLembaga() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        Konfigurasi lembaga belum ada, silahkan isi telebih dahulu
                    </div>
                    <br>
                    <br>
                    <div class="container">
                        <form>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width:150px">Bank ID</span>
                                </div>
                                <input id="lembaga-input-bankid" type="number" class="form-control" placeholder="Bank ID">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width:150px">Lembaga ID</span>
                                </div>
                                <input id="lembaga-input-lembagaid" type="text" class="form-control" placeholder="Serial Number">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width:150px">Nama Lembaga</span>
                                </div>
                                <input id="lembaga-input-lembaganame" type="text" class="form-control" placeholder="Nama Lembaga">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width:150px">Token</span>
                                </div>
                                <input id="lembaga-input-token" type="text" class="form-control" placeholder="Token">
                            </div>
                        </form>
                        <button onclick="checkTerminal.saveFormLembaga()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Simpan Data</button>
                    </div>
                </div>
            </div>
        `);
    }



    static failed(message) {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        ${message}
                    </div>
                    <br>
                    <br>
                    <div class="container">
                        <button onclick="checkTerminal.send()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Simpan Data</button>
                    </div>
                </div>
            </div>
        `);
    }
}

class checkUpdate {
    static send() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div id="loading-tittle">
                        Memeriksa versi pembaruan aplikasi
                    </div>
                    <br>
                    <div>
                        <div class="spinner-grow text-success"></div>
                        <div class="spinner-grow text-info"></div>
                        <div class="spinner-grow text-warning"></div>
                    </div>
                    <br>
                    <div id="loading-subtittle">
                        Mengambil data
                    </div>
                </div>
            </div>
        `);

        setTimeout(() => {
            javaConnector.checkVersionProcess();
        }, 1000);

    }

    static update() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        Anda menggunakan versi lama, apakah anda ingin update ke versi terbaru?
                    </div>
                    <br>
                    <br>
                    <div class="btn-group">
                        <button onclick="launch.exit()" type="button" class="btn btn-warning btn-sm" style="width: 150px;">Nanti Saja</button>
                        <button onclick="download.send()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Update Sekarang</button>
                    </div>
                </div>
            </div>
        `);
    }

    static newInstall() {
        $('#main-pane').html(`
        <div class="uk-animation-slide-bottom-medium">
            <!-- center position -->
            <div id="loading-pane" class="jumbotron jumbotron-fluid">
                <div>
                    Aplikasi belum ditemukan, apakah anda ingin menginstall aplikasi terlebih dahulu?
                </div>
                <br>
                <br>
                <div class="btn-group">
                    <button onclick="launch.exit()" type="button" class="btn btn-warning btn-sm" style="width: 150px;">Nanti Saja</button>
                    <button onclick="download.newInstall()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Install Aplikasi</button>
                </div>
            </div>
        </div>
    `);
    }

    static latestversion() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        Anda sudah menggunakan aplikasi terbaru
                    </div>
                    <br>
                    <br>
                    <div class="btn-group">
                        <button onclick="launch.exit()" type="button" class="btn btn-danger btn-sm" style="width: 150px;">Tutup Aplikasi</button>
                        <button onclick="launch.open()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Mulai Aplikasi</button>
                    </div>
                </div>
            </div>
        `);
    }

    static quickLaunch() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div id="loading-tittle">
                        Anda sudah menggunakan aplikasi terbaru
                    </div>
                    <br>
                    <div>
                        <div class="spinner-grow text-success"></div>
                        <div class="spinner-grow text-info"></div>
                        <div class="spinner-grow text-warning"></div>
                    </div>
                    <br>
                    <div id="loading-subtittle">
                        Memulai aplikasi
                    </div>
                </div>
            </div>
        `);

        setTimeout(() => {
            javaConnector.launchOpenApp();
        }, 1000);
    }

    static success() {

    }

    static failed() {

    }
}

class download {
    static send() {
        javaConnector.log('update application');

        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div id="loading-tittle">
                        Sedang mengunduh update aplikasi
                    </div>
                    <br>
                    <div class="container">
                        <div class="progress">
                            <div id="progress-value" class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>
                        </div>
                    </div>
                    <br>
                    <div id="loading-subtittle">
                        0%
                    </div>
                </div>
            </div>
        `);

        javaConnector.downloadProcess();
    }

    static update(percentage) {
        $("#loading-subtittle").html(`${percentage}%`);
        $("#progress-value").css("width", `${percentage}%`);
    }

    static newInstall() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div id="loading-tittle">
                        Sedang mengunduh aplikasi
                    </div>
                    <br>
                    <div class="container">
                        <div class="progress">
                            <div id="progress-value" class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>
                        </div>
                    </div>
                    <br>
                    <div id="loading-subtittle">
                        0%
                    </div>
                </div>
            </div>
        `);

        javaConnector.downloadProcess();
    }

    static success() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        Anda sudah menggunakan aplikasi terbaru
                    </div>
                    <br>
                    <br>
                    <div class="btn-group">
                        <button onclick="launch.exit()" type="button" class="btn btn-danger btn-sm" style="width: 150px;">Tutup Aplikasi</button>
                        <button onclick="launch.open()" type="button" class="btn btn-primary btn-sm" style="width: 150px;">Mulai Aplikasi</button>
                    </div>
                </div>
            </div>
        `);
    }

    static failed() {

    }
}

class driver {
    static options() {
        $('#main-pane').html(`
            <div class="uk-animation-slide-bottom-medium">
                <!-- center position -->
                <div id="loading-pane" class="jumbotron jumbotron-fluid">
                    <div>
                        Apakah anda ingin menginstall driver finger print?, jika sudah pernah menginstall sebelumnya maka pilih tidak, jika belum maka pilih iya
                    </div>
                    <br>
                    <br>
                    <div class="btn-group">
                        <button onclick="" type="button" class="btn btn-danger btn-sm" style="width: 150px;">Tidak</button>
                        <button onclick="" type="button" class="btn btn-success btn-sm" style="width: 150px;">Iya</button>
                    </div>
                </div>
            </div>
        `);
    }
}

class launch {
    static open() {
        javaConnector.launchOpenApp();
    }

    static exit() {
        javaConnector.launchCloseApp();
    }

    static success() {

    }

    static failed() {

    }
}

