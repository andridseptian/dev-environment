$(document).ready(function () {
    $("#img-logo").attr("src", "./img/icon/login/login-logo.gif");
    setTimeout(() => {
        $("#view-pages").show(1000);
        javaConnector.getLembagaData();
        javaConnector.getLoginData();
        javaConnector.getTerminalData();

        $("#pane-registration").hide();

        buttonListener.init();

        try {
            $("#img-logo").attr("src", "./img/icon/login/login-logo.gif");
            // $("#img-logo").show(1000);
            $("#pane-login").show(1000);

            $(window).resize(function () {
                if ($(window).width() <= 800) {
                    $('#side-nav').hide(500, 'swing');
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $('#side-nav').show(500, 'swing');
                    $("#menu-panes").css("padding-left", "350px");
                }
            });

        } catch (error) {
            alert(error);
        }
    }, 1000);

});


function setAlert(message) {
    try {
        $("#modal-loading").modal('hide');
        $("#alert-title").html("Informasi")
        $("#alert-body").html(message);
        $("#alert-footer").html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>`);
        $("#modal-alert").modal('toggle');
    } catch (error) {
        alert(error);
    }
}

function setLoading(message) {
    try {
        $("#loading-subtitle").html(message);
        $("#modal-loading").modal('toggle');
    } catch (error) {
        alert(error);
    }
}

class buttonListener {
    static init() {
        $("#a-forgotpassword").click(function () {
            $("#modal-requestreset").modal();

            $("#requestreset-submit").click(() => {
                if ($("#requestreset-input-nik").val() != "") {
                    var nik = $("#requestreset-input-nik").val();
                    javaConnector.sendRequestFromJS("requestresetpasswordbyuser", nik);
                    setLoading('Sedang memeriksa data anda');
                } else {
                    setAlert('Form NIK tidak boleh kosong');
                }
            });
            // $("#modal-reset").modal();
        });

        $("#a-registration").click(function () {
            $("#pane-login").hide();
            $("#pane-registration").show("fast", "swing");
        });

        $("#a-login").click(function () {
            $("#pane-registration").hide();
            $("#pane-login").show("fast", "swing");
        });

        $("#a-config").click(function () {
            config.showConfig();
        });

        $("#a-activate").click(function () {
            activate.showActivate();
        });

        $("#config-save").click(function () {
            config.saveConfig();
        });

        $("#activate-save").click(function () {
            activate.saveActivate();
        });

        $("#login-submit").click(function () {
            setLoading('Memeriksa akun anda');
            login.submit();
        });

        $("#regis-submit").click(function () {
            regis.submit();
        });

        $('#login-visible').click(function () {
            var x = document.getElementById("login-password");
            if ($(this).prop("checked") == true) {
                console.log("Checkbox is checked.");
                x.type = "text";
            }
            else if ($(this).prop("checked") == false) {
                console.log("Checkbox is unchecked.");
                x.type = "password";
            }
        });
    }
}

class data {
    static setTerminal(data) {

    }

    static setMacAddress(data) {
        try {
            var parsed = JSON.parse(data);
            $("#activate-macaddress").val(parsed.macaddress);
            $("#activate-procid").val(parsed.procid);

            $("#config-macaddress").val(parsed.macaddress);
            $("#config-procid").val(parsed.procid);

            $("#logo-macaddress").html('ready<br>' + parsed.macaddress);
            $("#logo-version").html(parsed.buildversion);
            $("#logo-build").html(parsed.buildnumber);
        } catch (error) {
            alert(error);
        }

    }

    static setLoginData(data) {
        try {
            var parsed = JSON.parse(data);
            $("#login-username").val(parsed.username);
            $("#login-password").val(parsed.password);
        } catch (error) {
            alert(error);
        }
    }

    static setLembaga(data) {
        try {
            var parsed = JSON.parse(data);
            $("#config-bankid").val(parsed.bankid);
            $("#config-lembagaid").val(parsed.lembagaid);
            $("#config-lembaganame").val(parsed.lembaganame);
            $("#config-token").val(parsed.token);
            $("#activate-bankid").val(parsed.bankid);
        } catch (error) {
            alert(error);
        }
    }
}

class config {
    static showConfig() {
        $("#modal-config").modal('toggle');
    }

    static saveConfig() {
        try {
            $("#modal-config").modal('hide');

            if ($("#config-bankid").val() != "") {
                var send = {
                    bankid: $("#config-bankid").val(),
                    lembagaid: $("#config-lembagaid").val(),
                    lembaganame: $("#config-lembaganame").val(),
                    token: $("#config-token").val()
                }
                javaConnector.saveLembagaData(JSON.stringify(send));
            } else {
                setAlert("Bank ID harus diisi dengan angka dan tidak boleh terdapat spasi")
            }
        } catch (error) {
            alert(error);
        }
    }
}

class activate {
    static showActivate() {
        $("#modal-activate").modal('toggle');
    }

    static saveActivate() {
        $("#modal-activate").modal('hide');
        setAlert('fungsi belum tersedia');
    }
}

class resetPassword {
    static reset() {
        try {
            $("#modal-loading").modal('toggle');
            $("#modal-reset").modal();
            $("#reset-submit").click(() => {
                if (
                    $("#reset-input-oldpassword").val() != "" &&
                    $("#reset-input-newpassword").val() != "" &&
                    $("#reset-input-confpassword").val() != ""
                ) {
                    if ($("#reset-input-newpassword").val() == $("#reset-input-confpassword").val()) {
                        setLoading('Sedang memeriksa data reset password anda');
                        var data = JSON.stringify({
                            old_password: $("#reset-input-oldpassword").val(),
                            new_password: $("#reset-input-newpassword").val()
                        });
                        javaConnector.sendRequestFromJS('resetpasswordbyuser', data);
                    } else {
                        setAlert('Konfirmasi password anda salah');
                    }
                } else {
                    setAlert('form tidak boleh ada yg kosong')
                }
            });
        } catch (error) {
            alert(error);
        }
    }
}

class login {
    static submit() {
        try {
            if (
                $("#login-username").val() != "" &&
                $("#login-password").val() != ""
            ) {
                setLoading('Sedang memeriksa akun anda');
                var indata = {
                    username: $("#login-username").val(),
                    password: $("#login-password").val()
                }
            } else {
                setAlert('Form login tidak boleh ada yang kosong.');
            }
            javaConnector.sendRequestFromJS('login', JSON.stringify(indata));
        } catch (error) {
            alert(error);
        }
    }

    static response() {
        try {
            window.open('dashboard.html');
            $("#modal-loading").modal('hide');
        } catch (error) {
            alert(error);
        }
    }
}

class regis {
    static submit() {
        try {
            if (
                $('#regis-fullname').val() != "" &&
                $('#regis-username').val() != "" &&
                $('#regis-nik').val() != "" &&
                $('#regis-email').val() != "" &&
                $('#regis-phonenumber').val() != "" &&
                $('#regis-password').val() != "" &&
                $('#regis-confpassword').val() != ""
            ) {
                setLoading("sedang memeriksa data anda");

                var password = $("#regis-password").val();
                var confpassword = $("#regis-confpassword").val();

                if (password == confpassword) {
                    var data = {
                        username: $('#regis-username').val(),
                        fullname: $('#regis-fullname').val(),
                        email: $('#regis-email').val(),
                        nik: $('#regis-nik').val(),
                        phonenumber: $('#regis-phonenumber').val(),
                        password: $('#regis-password').val()
                    };

                    const settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "http://103.28.148.203:61003/sb/checkAdminAvailability",
                        "method": "POST",
                        "headers": {
                            "content-type": "application/json"
                        },
                        "processData": false,
                        "data": "{\n\t\"bank_id\": \"2499\"\n}"
                    };

                    $.ajax(settings).done(function (response) {
                        // alert(response);
                        setTimeout(() => {
                            var resBody = JSON.parse(response);
                            if (resBody.rc == "00") {
                                data.privillage = 0;
                                javaConnector.sendRequestFromJS('registration', JSON.stringify(data));
                            } else {
                                $('#modal-loading').modal('hide');
                                $('#modal-adminconfirm').modal();

                                $('#adminconfirm-yes').click(() => {
                                    setLoading("mendaftarkan user");
                                    data.privillage = 1;
                                    javaConnector.sendRequestFromJS('registration', JSON.stringify(data));
                                });

                                $('#adminconfirm-no').click(() => {
                                    setLoading("mendaftarkan user");
                                    data.privillage = 0;
                                    javaConnector.sendRequestFromJS('registration', JSON.stringify(data));
                                });
                            }
                        }, 2000);
                    });
                } else {
                    setAlert('Konfirmasi password anda belum sesuai, coba sekali lagi');
                }
            } else {
                setAlert('Form data harus diisi semua, tidak boleh ada yg kosong');
            }
        } catch (error) {
            alert(error);
        }
    }
    static response() {
        $("#modal-loading").modal('hide');
        $("#pane-registration").hide();
        $("#pane-login").show("fast", "swing");
    }
}

var jsConnector = {
    set: function (result) {

    }
};

function getJsConnector() {
    return jsConnector;
};

