
// GLOBAL VARIABLE
var returnDataContainer = [];
var globalBankId;
var globalBankName;
var globalOperatorUserName;
var globalOperatorFullname;
var offlineKTPData = "";
var jsKTP = "";

////////////////////////////////////////////////////////////////

$(document).ready(function () {

    $('#menu-panes').hide();
    $('#side-nav').hide();

    setTimeout(() => {
        try {
            data.getInitialData();
            returndata.getdata();
            javaConnector.getReturnData();
            $('#return-table').DataTable();

            $("#disclaimer-input-tanggal").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });

            $("#page-dashboard").show();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();


            $("#disclaimer-input-alamat").prop('disabled', true);
            $("#disclaimer-input-rtrw").prop('disabled', true);
            $("#disclaimer-input-keldesa").prop('disabled', true);
            $("#disclaimer-input-kec").prop('disabled', true);
            $("#disclaimer-input-kerja").prop('disabled', true);
            $("#disclaimer-input-kawin").prop('disabled', true);
            $("#disclaimer-input-warga").prop('disabled', true);


            buttonListener.menuButtonListener();
            buttonListener.portalButtonListener();
            buttonListener.returnDataButtonListener();
            buttonListener.connectionButtonListener();
            buttonListener.disclaimerButtonListener();
            buttonListener.OfflineKTPButtonListener();
            $('#side-nav').show(500);
            $('#menu-panes').show(500, 'swing');


            $(window).resize(function () {
                if ($(window).width() <= 800) {
                    $('#side-nav').hide(500, 'swing');
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $('#side-nav').show(500, 'swing');
                    $("#menu-panes").css("padding-left", "350px");
                }
            });

            if ($(window).width() < 800) {
                setTimeout(() => {
                    $('#side-nav').hide(500, 'swing');
                    $("#menu-panes").css("padding-left", "50px");
                }, 1000);
            }


            setTimeout(() => {
                var info_dt = '';
                function createDatatable() {
                    erTable_dt_invoice = $("#dt_invoice").DataTable({
                        processing: true,
                        serverSide: true,
                        searchDelay: 500,
                        autoWidth: false,
                        columns: [{ "orderable": false, "className": "text-center" }, { "orderable": true }, { "orderable": true }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }, { "orderable": true, "className": "text-center" }],
                        order: [[4, 'desc']],
                        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
                        dom: "<'row'<'col-sm-6'l><'col-sm-6'B>> <'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                        buttons: [
                            {
                                extend: 'print',
                                text: '<i class="fa fa-print"></i>',
                                titleAttr: 'Print',
                                tag: "button",
                                className: ""
                            },
                            {
                                extend: 'copyHtml5',
                                text: '<i class="fa fa-files-o"></i>',
                                titleAttr: 'Copy',
                                tag: "button",
                                className: ""
                            },
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                titleAttr: 'Excel',
                                tag: "button",
                                className: ""
                            },
                            {
                                extend: 'csvHtml5',
                                text: '<i class="fa fa-file-text-o"></i>',
                                titleAttr: 'CSV',
                                tag: "button",
                                className: ""
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                titleAttr: 'PDF',
                                tag: "button",
                                className: ""
                            },
                        ],

                        ajax: {
                            url: `https://perbarindo.org/index.php/services/invoice/invoice_bypass?id_bpr=${globalBankId}&key=61d90eb6c09b48cf5802b823eac08980&tab=sb&filter_bulan=all&filter_tahun=all&filter_status=all`,
                            type: "POST",
                            data: function (d, dt) {
                                d.dt_name = "dt_invoice";
                            }
                        }
                    });
                    info_dt = erTable_dt_invoice.page.info();
                };
                createDatatable();
            }, 1000);


        } catch (error) {
            alert(error);
        }
    }, 500);

});

class buttonListener {
    static menuButtonListener() {

        $("#menu-fold").click(function () {
            if ($(window).width() > 800) {
                if ($("#side-nav").is(":visible")) {
                    $("#side-nav").toggle("fast", "swing");
                    $("#menu-panes").css("padding-left", "50px");
                } else {
                    $("#side-nav").toggle("fast", "swing");
                    $("#menu-panes").css("padding-left", "350px");
                }
            } else {
                $("#side-nav").toggle("fast", "swing");
                $("#menu-panes").css("padding-left", "50px");
            }
        });

        $("#a-dashboard").click(function () {
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-dashboard").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-webportal").click(function () {

            $("#page-dashboard").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-webportal").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-returndata").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();

            $("#page-returndata").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-manuser").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $("#page-manterminal").hide();

            $("#page-manuser").show("fast");

            setTimeout(() => {
                javaConnector.sendRequestFromJS('getuserlist', 'null');
            }, 500);

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-manterminal").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-offlinektp").hide();
            $("#page-billing").hide();
            $("#page-manuser").hide();

            $("#page-manterminal").show("fast");

            setTimeout(() => {
                javaConnector.sendRequestFromJS('getserialnumber', 'null');
            }, 500);

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-offlinektp").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-billing").hide();

            $("#page-offlinektp").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#a-billing").click(function () {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-returndata").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();

            $("#page-billing").show("fast");

            if ($(window).width() < 800) {
                $("#side-nav").toggle("fast", "swing");
            }
        });

        $("#footer-logout").click(function () {
            try {
                setLoading('Membersihkan data....');
                // alert('logout');
                javaConnector.sendRequestFromJS('logout', JSON.stringify({
                    username: globalOperatorUserName
                }));
                setTimeout(() => {
                    window.open('login.html');
                }, 5000);
            } catch (error) {
                alert(error);
            }
        });
    }

    static portalButtonListener() {
        $("#portal-openapp").click(function () {
            try {
                setLoading('Menghubungkan ke jaringan bersama');
                javaConnector.connectingToVPN('app');
            } catch (error) {
                alert(error);
            }
        });

        $("#portal-openbrowser").click(function () {
            try {
                setLoading('Menghubungkan ke jaringan bersama');
                javaConnector.connectingToVPN('browser');
            } catch (error) {
                alert(error);
            }
        });
    }

    static returnDataButtonListener() {
        $("#return-inputdata").click(function () {
            showInputReturnData();
        });

        $("#return-saveexcel").click(function () {
            javaConnector.saveReturnDataToExcel(JSON.stringify(returnDataContainer));
        });

        $("#returndata-input").click(function () {
            try {
                var data = [
                    $("#returndata-input-date").val(),
                    $("#returndata-input-user").val(),
                    $("#returndata-input-nik").val(),
                    $("#returndata-input-cif").val(),
                    `<button id="returndata-button" type="button" class="btn btn-danger btn-sm">delete</button>`
                ];

                returnDataContainer.push(data);

                javaConnector.saveReturnData(JSON.stringify(returnDataContainer));

                $('#return-table').DataTable().destroy();

                $('#return-table').DataTable({
                    data: returnDataContainer
                });

            } catch (error) {
                alert(error);
            }
        });

        $("#return-table").on('click', '.btn', function () {
            try {
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                var cell1 = tr.find('td').eq(1).html();
                var cell2 = tr.find('td').eq(2).html();
                var cell3 = tr.find('td').eq(3).html();
                var cell4 = tr.find('td').eq(4).html();

                var output = `["${cell0}","${cell1}","${cell2}","${cell3}",${JSON.stringify(cell4)}]`;

                var temp = JSON.stringify(returnDataContainer)
                    .replace(output, "")
                    .replace(',,', ',')
                    .replace(',]', ']')
                    .replace('[,', '[');

                returnDataContainer = JSON.parse(temp);

                javaConnector.saveReturnData(JSON.stringify(returnDataContainer));

                $('#return-table').DataTable().destroy();

                $('#return-table').DataTable({
                    data: returnDataContainer
                });
            } catch (error) {
                alert(error);
            }
        });

        $("#return-openportal").click(function () {
            try {
                javaConnector.connectingToVPN('returnDataBrowser');
            } catch (error) {
                alert(error);
            }
        });
    }

    static connectionButtonListener() {
        $("#connection-continue").click(function () {
            setAlert('silahkan lanjutkan koneksi anda');
            javaConnector.continueVPN();
        });

        $("#connection-disconnect").click(function () {
            setAlert('koneksi anda akan diputuskan');
            javaConnector.disconnectingFromVPN();
        });
    }

    static disclaimerButtonListener() {
        $("#generate-disclaimer").click(function () {
            try {
                $("#modal-disclaimer").modal('toggle');

                $("#checked-by").html(globalOperatorFullname);

                $("#disclaimer-input-nama").change(function () {
                    $("#approved-by").html(this.value);
                });
            } catch (error) {
                alert(error);
            }
        });

        $("#disclaimer-cb-alamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-alamat").prop('disabled', false);
            } else {
                $("#disclaimer-input-alamat").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-detailalamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-rtrw").prop('disabled', false);
                $("#disclaimer-input-keldesa").prop('disabled', false);
                $("#disclaimer-input-kec").prop('disabled', false);
            } else {
                $("#disclaimer-input-rtrw").prop('disabled', true);
                $("#disclaimer-input-keldesa").prop('disabled', true);
                $("#disclaimer-input-kec").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-status").click(function () {
            if (this.checked) {
                $("#disclaimer-input-kerja").prop('disabled', false);
                $("#disclaimer-input-kawin").prop('disabled', false);
                $("#disclaimer-input-warga").prop('disabled', false);
            } else {
                $("#disclaimer-input-kerja").prop('disabled', true);
                $("#disclaimer-input-kawin").prop('disabled', true);
                $("#disclaimer-input-warga").prop('disabled', true);
            }
        });

        $("#disclaimer-footer").html(`
            <button id="disclaimer-disagree" type="button" class="btn btn-danger" 
                data-dismiss="modal">Tidak Setuju</button>
            <button id="disclaimer-portal-agree" type="button" class="btn btn-primary">Setuju</button>
        `)

        $("#disclaimer-portal-agree").click(function () {

            if (
                $("#disclaimer-input-nik").val() != "" &&
                $("#disclaimer-input-nama").val() != "" &&
                $("#disclaimer-input-tanggal").val() != "" &&
                $("#disclaimer-input-tempat").val() != ""
            ) {

                $("#modal-disclaimer").modal('toggle');

                var data = {
                    nik: $("#disclaimer-input-nik").val(),
                    nama: $("#disclaimer-input-nama").val(),
                    tanggal: $("#disclaimer-input-tanggal").val(),
                    tempat: $("#disclaimer-input-tempat").val(),
                    kelamin: $("#disclaimer-input-kelamin").val(),
                    alamat: $("#disclaimer-input-alamat").val(),
                    rtrw: $("#disclaimer-input-rtrw").val(),
                    keldesa: $("#disclaimer-input-keldesa").val(),
                    kec: $("#disclaimer-input-kec").val(),
                    kawin: $("#disclaimer-input-kawin").val(),
                    kerja: $("#disclaimer-input-kerja").val(),
                    warga: $("#disclaimer-input-warga").val()
                }

                javaConnector.createDiscalimerToPDF(JSON.stringify(data));
            } else {
                setAlert('form nik,nama dan tempat tanggal lahir tidak boleh kosong');
            }
        });
    }

    static OfflineKTPButtonListener() {
        $("#offline-scan").click(() => {
            javaConnector.ktpInitialize();
            setLoading('Mengakses alat scan ktp, pastikan alat telah terhubung....');
            $("#img-loading").attr("src", "./img/assets/scanner/plug-device.gif");
            // setTimeout(() => {
            //     $("#modal-loading").modal('hide');
            //     setTimeout(() => {
            //         setLoading('Memeriksa ktp anda....');
            //         javaConnector.scanKTP();
            //     }, 500);
            // }, 5000);
        });

        $("#offline-conf").click(() => {
            setTimeout(() => {
                $("#modal-offlineconf").modal('toggle');

                $("#offlineconf-sync").click(() => {
                    javaConnector.sendRequestFromJS('setdeviceid', JSON.stringify({
                        pcid: $("#offlineconf-input-pcid").val(),
                        conf: $("#offlineconf-input-conf").val()
                    }));;
                })
            }, 300);
        })

        $("#offline-disclaimer").click(() => {
            try {
                $("#modal-disclaimer").modal('toggle');

                $("#checked-by").html(globalOperatorFullname);

                $("#disclaimer-input-nama").change(function () {
                    $("#approved-by").html(this.value);
                });

                if (jsKTP != "") {

                    //#region jsKTP Body
                    /* 
                        var ktp = [
                        ["NIK", `: ${jsKTP.nik}`],
                        ["Nama", `: ${jsKTP.nama}`],
                        ["Tempat/Tgl Lahir", `: ${jsKTP.ttl}`],
                        ["Jenis Kelamin", `: ${jsKTP.kelamin}`],
                        ["Alamat", `: ${jsKTP.alamat}`],
                        ["RT/RW", `: ${jsKTP.rtrw}`],
                        ["Kel/Desa", `: ${jsKTP.kel} / ${jsKTP.kota}`],
                        ["Kecamatan", `: ${jsKTP.kec}`],
                        ["Agama", `: ${jsKTP.agama}`],
                        ["Status Perkawinan", `: ${jsKTP.status}`],
                        ["Pekerjaan", `: ${jsKTP.pekerjaan}`],
                        ["Kewarganegaraan", `: ${jsKTP.warga}`]
                        ];
                    */
                    //#endregion

                    try {
                        var ttl = jsKTP.ttl.split(",");
                    } catch (error) { }

                    $("#approved-by").html(jsKTP.nama);

                    $("#disclaimer-input-nik").val(jsKTP.nik);
                    $("#disclaimer-input-nama").val(jsKTP.nama);
                    $("#disclaimer-input-tanggal").val(ttl[1]);
                    $("#disclaimer-input-tempat").val(ttl[0]);
                    $("#disclaimer-input-kelamin").val(jsKTP.kelamin);
                    $("#disclaimer-input-alamat").val(jsKTP.alamat);
                    $("#disclaimer-input-rtrw").val(jsKTP.rtrw);
                    $("#disclaimer-input-keldesa").val(jsKTP.kel);
                    $("#disclaimer-input-kec").val(jsKTP.kec);
                    $("#disclaimer-input-kawin").val(jsKTP.status);
                    $("#disclaimer-input-kerja").val(jsKTP.pekerjaan);
                    $("#disclaimer-input-warga").val(jsKTP.warga);

                };

            } catch (error) {
                alert(error);
            }
        });

        $("#disclaimer-cb-alamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-alamat").prop('disabled', false);
            } else {
                $("#disclaimer-input-alamat").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-detailalamat").click(function () {
            if (this.checked) {
                $("#disclaimer-input-rtrw").prop('disabled', false);
                $("#disclaimer-input-keldesa").prop('disabled', false);
                $("#disclaimer-input-kec").prop('disabled', false);
            } else {
                $("#disclaimer-input-rtrw").prop('disabled', true);
                $("#disclaimer-input-keldesa").prop('disabled', true);
                $("#disclaimer-input-kec").prop('disabled', true);
            }
        });

        $("#disclaimer-cb-status").click(function () {
            if (this.checked) {
                $("#disclaimer-input-kerja").prop('disabled', false);
                $("#disclaimer-input-kawin").prop('disabled', false);
                $("#disclaimer-input-warga").prop('disabled', false);
            } else {
                $("#disclaimer-input-kerja").prop('disabled', true);
                $("#disclaimer-input-kawin").prop('disabled', true);
                $("#disclaimer-input-warga").prop('disabled', true);
            }
        });

        $("#disclaimer-footer").html(`
            <button id="disclaimer-disagree" type="button" class="btn btn-danger" 
                data-dismiss="modal">Tidak Setuju</button>
            <button id="disclaimer-offline-agree" type="button" class="btn btn-primary">Setuju</button>
        `)

        $("#disclaimer-offline-agree").click(function () {
            if (
                $("#disclaimer-input-nik").val() != "" &&
                $("#disclaimer-input-nama").val() != "" &&
                $("#disclaimer-input-tanggal").val() != "" &&
                $("#disclaimer-input-tempat").val() != ""
            ) {
                $("#modal-disclaimer").modal('toggle');
                var data = {
                    nik: $("#disclaimer-input-nik").val(),
                    nama: $("#disclaimer-input-nama").val(),
                    tanggal: $("#disclaimer-input-tanggal").val(),
                    tempat: $("#disclaimer-input-tempat").val(),
                    kelamin: $("#disclaimer-input-kelamin").val(),
                    alamat: $("#disclaimer-input-alamat").val(),
                    rtrw: $("#disclaimer-input-rtrw").val(),
                    keldesa: $("#disclaimer-input-keldesa").val(),
                    kec: $("#disclaimer-input-kec").val(),
                    kawin: $("#disclaimer-input-kawin").val(),
                    kerja: $("#disclaimer-input-kerja").val(),
                    warga: $("#disclaimer-input-warga").val()
                }
                javaConnector.createDiscalimerToPDF(JSON.stringify(data));
            } else {
                setAlert('form nik,nama dan tempat tanggal lahir tidak boleh kosong');
            }
        });
    }
}

function setReturnData(data) {
    try {
        // javaConnector.logFromJS('data return: ' + JSON.stringify(data));
        returnDataContainer = JSON.parse(data);
        $('#return-table').DataTable().destroy();
        $('#return-table').DataTable({
            data: returnDataContainer
        });
        $('#return-table').DataTable().destroy();
        $('#return-table').DataTable({
            data: returnDataContainer
        });
    } catch (error) {
        alert(error);
    }
}

function setAlert(message) {
    try {
        $("#modal-loading").modal('hide');
        $("#alert-title").html("Informasi")
        $("#alert-body").html(message);
        $("#alert-footer").html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>`);
        $("#modal-alert").modal('toggle');
    } catch (error) {
        alert(error);
    }

}

function setLoading(message) {
    try {
        $("#img-loading").attr("src", "");
        $("#loading-subtitle").html(message);
        $("#modal-loading").modal('toggle');
    } catch (error) {
        alert(error);
    }
}

function showConnectionAlert() {
    try {
        $("#modal-loading").modal('hide');
        countdown(1);
        $('#modal-connection').modal();
    } catch (error) {
        alert(error);
    }
}

function showInputReturnData() {
    try {
        var today = new Date();
        var date = today.getFullYear()
            + "-" + today.getMonth().toString().padStart(2, 0)
            + "-" + today.getDate().toString().padStart(2, 0)
            + " " + today.getHours().toString().padStart(2, 0)
            + ":" + today.getMinutes().toString().padStart(2, 0)
            + ":" + today.getSeconds().toString().padStart(2, 0);

        $('#returndata-input-date').val(date);
        $('#modal-returndata').modal();
    } catch (error) {
        alert(error);
    }
}

function showReturnDataPage() {
    try {
        $("#page-dashboard").hide();
        $("#page-webportal").hide();
        $("#page-manuser").hide();
        $("#page-manterminal").hide();
        $("#page-offlinektp").hide();

        $("#page-returndata").show("fast");
    } catch (error) {
        alert(error);
    }
}

function countdown(minutes) {
    try {
        var seconds = 60;
        var mins = minutes
        function tick() {
            //This script expects an element with an ID = "counter". You can change that to what ever you want. 
            var counter = document.getElementById("counter");
            var current_minutes = mins - 1
            seconds--;
            counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
            if (seconds > 0) {
                setTimeout(tick, 1000);
            } else {
                if (mins > 1) {
                    countdown(mins - 1);
                } else {
                    $('#modal-connection').modal('hide');
                    setAlert('Koneksi jaringan bersama diputuskan');
                    javaConnector.disconnectingFromVPN();
                }
            }
        }
        tick();
    } catch (error) {
        alert(error);
    }
}

class portal {
    static connectingToVPN() {
        javaConnector.connectingToVPN('browser');
    }

    static connectedPortalApplied() {

    }

    static disconnectFromVPN() {
        javaConnector.disconnectingFromVPN();
    }

    static showScheduledTimeOut() {

    }
}

class clock {
    static startTime() {
        var today = new Date();
        var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = clock.checkTime(m);
        s = clock.checkTime(s);
        document.getElementById('nav-clock').innerHTML =
            date + " " + h + ":" + m + ":" + s + "";
        var t = setTimeout(clock.startTime, 500);
    }
    static checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }
}

class plainModal {
    static show() {
        $("#modal-plain").modal();
    }

    static show(tittle, body, footer) {
        $("#modal-plain").modal();
        $("#plain-title").html(tittle);
        $("#plain-body").html(body);
        $("#plain-footer").html(footer);
    }

    static hide() {
        $("#modal-plain").modal('hide');
    }
}

class data {
    static getInitialData() {
        javaConnector.getDashboardData();
    }

    static setInitialData(data) {
        try {

            javaConnector.logFromJS('parsing data');

            /* Parsing Data */
            var parsed = JSON.parse(data);
            var dashboard = $("#page-dashboard").html();
            dashboard = dashboard.replace("[user-fullname]", parsed.fullname);
            dashboard = dashboard.replace("[bpr-fullname]", parsed.bprname);
            javaConnector.logFromJS(dashboard);

            /* set global variable */
            globalBankName = parsed.bprname;
            globalBankId = parsed.bankid;
            globalOperatorUserName = parsed.username;
            globalOperatorFullname = parsed.fullname;

            if (parsed.pcid != null) {
                $("#offlineconf-input-pcid").val(parsed.pcid);
            }

            if (parsed.conf != null) {
                $("#offlineconf-input-conf").val(parsed.conf);
            }

            /** set view data */
            $("#returndata-input-user").val(parsed.username);
            $("#page-dashboard").html(dashboard);
            $("#nav-bankname").html(globalBankName);


            if (parsed.privilege == 0) {
                manuser.hidemenu();
                manterminal.hidemenu();
            }

        } catch (err) {
            javaConnector.logFromJS(err.message);
            alert(err.message);
        }
    }
}

class returndata {
    static getdata() {
        try {
            $("#return-header").html(`
                <tr>
                    <th>Waktu cek</th>
                    <th>User Operator</th>
                    <th>NIK</th>
                    <th>CIF</th>
                    <th></th>
                </tr>
            `);

            $("#return-body").html(`
                <tr class="row-of-data">
                    <td class="time">--------------------------------</td>
                    <td class="user">--------------------------------</td>
                    <td class="nik">--------------------------------</td>
                    <td class="cif">--------------------------------</td>
                    <td class="return-button">--------------------------------</td>
                </tr>
            `);

        } catch (error) {
            alert(error);
        }
    }
}

class scanKTP {
    static setKTPResult(result) {
        var jsKTP = JSON.parse(result);

        $('offline-result').html(result);
    }

    static scanningKTP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Memeriksa data ktp, letakkan ktp pada alat pembaca ktp dan tunggu hingga proses selesai...');
            $("#img-loading").attr("src", "./img/assets/scanner/put-card.gif");
            javaConnector.scanKTP();
        }, 500);
    }

    static scanningFingerPrint() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Memeriksa data sidik jari, Letakkan jari telunjuk yang akan di scan pada alat pembaca sidik jari');
            $("#img-loading").attr("src", "./img/assets/scanner/put-finger.gif");
            // javaConnector.scanKTP();
        }, 500);
    }

    static failedReadKTP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Gagal, mengulang pembacaan');
            scanningFingerPrint()
        }, 2000);
    }

    static failedReadFP() {
        $("#modal-loading").modal('hide');
        setTimeout(() => {
            setLoading('Sidik jari tidak cocok, mengulang pembacaan');
            scanningFingerPrint()
        }, 1000);
    }
}

class manuser {
    static getUserList(data) {
        try {
            var arrData = JSON.parse(data);
            /* 
            <tr>
                <th>username</th>
                <th>Nama</th>
                <th>No HP</th>
                <th>Email</th>
                <th>Kadaluarsa Password</th>
                <th>Status</th>
                <th> - </th>
            </tr>
            */
            var html = ``;
            arrData.forEach(element => {
                html += `<tr class="row-of-data">`

                html += `<td class="userid">${element.user_id}</td>`
                html += `<td class="fullname">${element.fullname}</td>`
                html += `<td class="phone">${element.no_hp}</td>`
                html += `<td class="email">${element.email}</td>`
                html += `<td class="expired">${element.expired_password}</td>`
                if (element.status == 0) {
                    html += `<td class="status">tidak aktif</td>`
                    html += `<td class="action">
                                <button type="button" class="man-activate btn btn-success btn-sm">aktifkan</button>
                            </td>`
                } else if (element.status == 1) {
                    html += `<td class="status">aktif</td>`
                    html += `<td class="action">
                                <button type="button" class="man-block btn btn-danger btn-sm">non-aktifkan</button>
                            </td>`
                } else if (element.status == 2) {
                    html += `<td class="status">terblokir</td>`
                    html += `<td class="action">
                                <button type="button" class="man-activate btn btn-warning btn-sm">aktifkan</button>
                            </td>`
                }
                html += `</tr>`
            });

            $('#manuser-body').html(html);
            $('#manuser-table').DataTable();

            $('.man-activate').click(function () {
                setLoading('Mengaktifkan user');
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                javaConnector.sendRequestFromJS('activateuser', cell0);
                setTimeout(() => {
                    javaConnector.sendRequestFromJS('getuserlist', 'null');
                }, 1000);

                // alert(cell0);
            });

            $('.man-block').click(function () {
                setLoading('Menonaktifkan User');
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                javaConnector.sendRequestFromJS('deactivateuser', cell0);
                setTimeout(() => {
                    javaConnector.sendRequestFromJS('getuserlist', 'null');
                }, 1000);
                // alert(cell0);
            });

        } catch (error) {
            alert(error);
        }
    }

    static hidemenu() {
        $('#a-manuser').hide();
    }
}

class manterminal {
    static setTerminalList(data) {
        try {
            var arrData = JSON.parse(data);
            /* 
                "bank_id"
                "serial_number"
                "mac"
                "status"
            */
            var html = ``;
            arrData.forEach(element => {
                html += `<tr class="row-of-data">`

                html += `<td class="serial_number">${element.serial_number}</td>`
                if (element.mac == undefined) {
                    html += `<td class="mac"> </td>`
                    html += `<td class="status">tidak aktif</td>`
                    html += `<td class="hostname"> </td>`
                    html += `<td class="processorid"> </td>`
                    html += `<td class="processorname"> </td>`
                    html += `<td class="action"> </td>`
                } else {
                    html += `<td class="mac">${element.mac}</td>`
                    html += `<td class="status">aktif</td>`
                    html += `<td class="hostname">${element.terminal.host_name}</td>`
                    html += `<td class="processorid">${element.terminal.processor_id}</td>`
                    html += `<td class="processorname">${element.terminal.processor_name}</td>`
                    html += `<td class="action">
                                <button type="button" class="manterminal-resetserial btn btn-danger btn-sm">reset</button>
                            </td>`
                }
                html += `</tr>`
            });

            $('#manterminal-body').html(html);
            $('#manterminal-table').DataTable();


            $('.manterminal-resetserial').click(function () {
                setLoading('Mereset Terminal');
                var tr = $(this).closest('tr');
                var cell0 = tr.find('td').eq(0).html();
                javaConnector.sendRequestFromJS('resetserialnumber', cell0);
                setTimeout(() => {
                    javaConnector.sendRequestFromJS('getserialnumber', 'null');
                }, 1000);
            })

        } catch (error) {
            alert(error)
        }
    }

    static generateSerialNumber() {
        setLoading('Generate Serial Number');
        setTimeout(() => {
            javaConnector.sendRequestFromJS('generateserialnumber', 'null');
        }, 1000);
    }

    static refreshGenerateSerial() {
        setAlert('Sukses Generates Serial Number');
        setTimeout(() => {
            javaConnector.sendRequestFromJS('getserialnumber', 'null');
        }, 1000);
    }

    static hidemenu() {
        $('#a-manterminal').hide();
    }
}

var jsConnector = {
    setReturn: function (result) {
        try {
            returnDataContainer = JSON.parse(result);
            // returnDataContainer = result;
            $('#return-table').DataTable().destroy();
            $('#return-table').DataTable({
                data: returnDataContainer
            });
            $('#return-table').DataTable().destroy();
            $('#return-table').DataTable({
                data: returnDataContainer
            });
        } catch (error) {
            alert(error);
        }
    },

    showReturnDataPage: function () {
        try {
            $("#page-dashboard").hide();
            $("#page-webportal").hide();
            $("#page-manuser").hide();
            $("#page-manterminal").hide();
            $("#page-offlinektp").hide();

            $("#page-returndata").show("fast");
        } catch (error) {
            alert(error);
        }
    },

    setOfflineKTPResult: function (result) {
        try {
            jsKTP = JSON.parse(result);

            //#region jsKTP Value
            /*
            jsEktp.put("nik", strList[1]);
            jsEktp.put("alamat", strList[2]);
            jsEktp.put("rtrw", strList[3] + "/" + strList[4]);
            jsEktp.put("ttl", strList[5] + "," + strList[15]);
            jsEktp.put("kec", strList[6]);
            jsEktp.put("kel", strList[7]);
            jsEktp.put("kota", strList[8]);
            jsEktp.put("kelamin", strList[9]);
            jsEktp.put("agama", strList[11]);
            jsEktp.put("status", strList[12]);
            jsEktp.put("pekerjaan", strList[13]);
            jsEktp.put("nama", strList[14]);
            jsEktp.put("provinsi", strList[16]);
            jsEktp.put("goldar", strList[10]);
            jsEktp.put("warga", strList[20]);
            jsEktp.put("berlaku", strList[17]);
            jsEktp.put("photo", strList[0]);
            jsEktp.put("signature", strList[strList.length - 1]);
             *  */
            //#endregion

            var ktphead = `
                <section>
                    <div class="text-center">
                        <h6>PROVINSI ${jsKTP.provinsi}</h6>
                        <h6>${jsKTP.kota}</h6>
                    </div>
                </section>
            `;

            var ktp = [
                ["NIK", `: ${jsKTP.nik}`],
                ["Nama", `: ${jsKTP.nama}`],
                ["Tempat/Tgl Lahir", `: ${jsKTP.ttl}`],
                ["Jenis Kelamin", `: ${jsKTP.kelamin}`],
                ["Alamat", `: ${jsKTP.alamat}`],
                ["RT/RW", `: ${jsKTP.rtrw}`],
                ["Kel/Desa", `: ${jsKTP.kel} / ${jsKTP.kota}`],
                ["Kecamatan", `: ${jsKTP.kec}`],
                ["Agama", `: ${jsKTP.agama}`],
                ["Status Perkawinan", `: ${jsKTP.status}`],
                ["Pekerjaan", `: ${jsKTP.pekerjaan}`],
                ["Kewarganegaraan", `: ${jsKTP.warga}`]
            ];

            var rows = function () {
                var html = `<section id=\"offline-ktp-value\"> 
                                <div class = "row">
                                    <div class = "col-md-9">
                `;
                ktp.forEach(element => {
                    html += `
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>${element[0]}</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <label>${element[1]}</label>
                                            </div>
                                        </div>
                  `;
                });
                html += `   </div>
                                    <div class = "col-md-3">
                                        <img id="offline-result-photo"
                                        src="" class="rounded" alt="foto"
                                        style="width:100px">
                                    <div>
                                <div>
                            </section>`;

                return html;
            };

            $('#offline-result').html(`
                <div class="container reponsive-offline" style="background-image: url('./img/KTP-background.png');">
                        ${ktphead}
                        ${rows()}
                </div>
            `);

            function hexToBase64(hexstring) {
                return btoa(hexstring.match(/\w{2}/g).map(function (a) {
                    return String.fromCharCode(parseInt(a, 16));
                }).join(""));
            }

            document.getElementById('offline-result-photo').src = 'data:image/png;base64,' + hexToBase64(jsKTP.photo);
            // document.getElementById('offline-result-signature').src = 'data:image/png;base64,' + hexToBase64(jsKTP.signature);

            $("#modal-loading").modal('hide');
        } catch (error) {
            alert(error);
        }
    }
};

function getJsConnector() {
    return jsConnector;
};
