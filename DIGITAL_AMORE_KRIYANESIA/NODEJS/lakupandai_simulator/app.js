var express = require('express')
var app = express()
var port = 12000
var { log4js } = require('./src/utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))
const { mainRouter } = require('./src/router/mainRouter')
const { serviceRouter } = require('./src/router/serviceRouter')

app.use(function (req, res, next) {
  req.rawBody = '';
  console.log(req.headers['content-type']);
  if (req.headers['content-type'] !== 'application/json') {
    req.setEncoding('utf8');
    req.on('data', function (chunk) {
      req.rawBody += chunk;
    });
    req.on('end', function () {
      next();
    });
  } else {
    next();
  }
});

// app.use(express.urlencoded({ extended: false }))
app.use(express.json({ limit: '50mb' }))
app.use(express.static('public'))
app.use("/node_modules", express.static('node_modules'))

app.get("/", function(req, res) {
  res.redirect("/dashboard")
})

// use log4j for logging request
app.use(log4js.connectLogger(logger, { level: 'info' }));

mainRouter.init(app)
serviceRouter.init(app)

// Statring services
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
