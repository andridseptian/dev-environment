const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    return payment.init(sequelize, DataTypes);
}

/*
<channelId>LP</channelId>
<responseCode>05</responseCode>
<responseDesc>RESPONSE  TERALU LAMA</responseDesc>
<sourceAccountNumber>001221001234</sourceAccountNumber>
<productCode>050151</productCode>
<idBilling>340402018</idBilling>
<transactionAmount>34362300</transactionAmount>
<fee>1500</fee>
<agentFee>1000</agentFee>
<tax>30</tax>
<additionalData>{"nop":"340406000501610710","tahunSPPT":"2018","namaWP":"YANTI KUSUMA WIJAYANTI","kelurahan":"RASEK KIDUL","pengesahan":"101300001079394643","jmlBayar":343623}</additionalData>
<transactionType>2</transactionType>
<accountType>A</accountType>
<jobIdInquiry>35936943075204142021</jobIdInquiry>
<rrn>43075204142021</rrn>
*/

class payment extends Sequelize.Model {
    static init(sequelize, DataTypess) {
        var DataTypes = Sequelize.DataTypes
        super.init({
            id_billing: {
                type: DataTypes.STRING(255),
                allowNull: false,
                primaryKey: true
            },
            transaction_type: {
                type: DataTypes.STRING(255),
                allowNull: false
            },
            transaction_name: {
                type: DataTypes.STRING(255),
                allowNull: false
            },
            product_code: {
                type: DataTypes.STRING(255),
                allowNull: false
            },
            transaction_amount: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            add_inquiry: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            inq_rc: {
                type: DataTypes.STRING(2),
                allowNull: true
            },
            inq_desc: {
                type: DataTypes.STRING(255),
                allowNull: true
            },
            add_posting: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            pos_rc: {
                type: DataTypes.STRING(2),
                allowNull: true
            },
            pos_desc: {
                type: DataTypes.STRING(255),
                allowNull: true
            },
        }, {
            sequelize,
            tableName: 'payment',
            schema: 'public',
            timestamps: false,
        });
        return payment;
    }
}
