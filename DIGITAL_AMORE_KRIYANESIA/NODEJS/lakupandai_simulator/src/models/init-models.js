var DataTypes = require("sequelize").DataTypes;
var _SequelizeMeta = require("./SequelizeMeta");
var _agent = require("./agent");
var _dukcapil = require("./dukcapil");
var _log_activity = require("./log_activity");
var _nasabah = require("./nasabah");
var _product = require("./product");
var _sms = require("./sms");
var _response = require("./response");
var _payment = require("./payment");

function initModels(sequelize) {
  var SequelizeMeta = _SequelizeMeta(sequelize, DataTypes);
  var agent = _agent(sequelize, DataTypes);
  var dukcapil = _dukcapil(sequelize, DataTypes);
  var log_activity = _log_activity(sequelize, DataTypes);
  var nasabah = _nasabah(sequelize, DataTypes);
  var product = _product(sequelize, DataTypes);
  var sms = _sms(sequelize, DataTypes);
  var response = _response(sequelize, DataTypes)
  var payment = _payment(sequelize, DataTypes)

  return {
    SequelizeMeta,
    agent,
    dukcapil,
    log_activity,
    nasabah,
    product,
    sms,
    response,
    payment
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
