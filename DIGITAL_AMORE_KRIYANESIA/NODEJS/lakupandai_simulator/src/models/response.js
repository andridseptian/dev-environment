const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return response.init(sequelize, DataTypes);
}

class response extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    super.init({
      id: {
        type: DataTypes.STRING(10),
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      description: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      rc: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      rm: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      response: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      sequelize,
      tableName: 'response',
      schema: 'public',
      timestamps: false,
      indexes: [
        {
          name: "response_pkey",
          unique: true,
          fields: [
            { name: "id" },
          ]
        },
      ]
    });
    return response;
  }
}
