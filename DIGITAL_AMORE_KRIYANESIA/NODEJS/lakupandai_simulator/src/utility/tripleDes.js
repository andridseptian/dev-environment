const forge = require("node-forge");

class tripleDes {
    constructor(key) {
        this.key = key
    }

    decrypt3DES(input) {
        let md5Key = forge.md.md5.create();
        md5Key.update(this.key);
        md5Key = md5Key.digest().toHex();
        const decipher = forge.cipher.createDecipher('3DES-ECB', this.key.substring(0, 24));
        decipher.start();

        const inputEx = forge.util.createBuffer(Buffer.from(input, "base64").toString("binary"));
        decipher.update(inputEx);
        decipher.finish();
        const decrypted = decipher.output;
        return Buffer.from(decrypted.getBytes(), "binary").toString("utf8")
    }

    encrypt3DES(input) {
        var md5Key = forge.md.md5.create();
        md5Key.update(this.key);
        md5Key = md5Key.digest().toHex();

        // var cipher = forge.cipher.createCipher('3DES-ECB', md5Key.substring(0, 24));
        var cipher = forge.cipher.createCipher('3DES-ECB', this.key.substring(0, 24));
        cipher.start();
        cipher.update(forge.util.createBuffer(Buffer.from(input, "utf8").toString("binary")));
        cipher.finish();
        var encrypted = cipher.output;

        return Buffer.from(encrypted.getBytes(), "binary").toString("base64")
    }
}

module.exports = tripleDes