var convert = require('xml-js');

exports.soap2json = function (xml) {
    var result = convert.xml2json(xml, { compact: false, spaces: 4 });
    var res = JSON.parse(result)
    var serviceresponse = res.elements[0].elements[0].elements[0].elements[0].elements
    var json = {}
    for (let index = 0; index < serviceresponse.length; index++) {
        const element = serviceresponse[index];
        json[element.name] = element.elements[0].text

    }
    var convertres = {
        name: res.elements[0].elements[0].elements[0].name,
        attributes: res.elements[0].attributes,
        data: json
    }
    // console.log(convertres);
    return convertres;
}

function testing(){
    var xml = "<smsbc><request><datetime>0421101033</datetime><rrn>1122021042110103360203769106827</rrn><partnerId>1121</partnerId><partnerName>lakupandaiDIY</partnerName><password>LdAiKyU</password><destinationNumber>6282196740588</destinationNumber><message>Proses penarikan dana akan segera diproses. Silahkan masukkan token ini 587418</message></request></smsbc>"
}