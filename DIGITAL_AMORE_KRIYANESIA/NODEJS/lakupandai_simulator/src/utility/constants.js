var builder = require('xmlbuilder');

function responsebuilder(responseCode, responseDesc) {
    return builder.create('SOAP-ENV:Envelope')
        .att("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/")
        .ele('SOAP-ENV:Body')
        .ele('ns1:AgentLoginResponse').att("xmlns:ns1", "urn:AgentLogin")
        .ele('ServiceResponse')
        .ele('channelId', "LP").up()
        .ele('responseCode', responseCode).up()
        .ele('responseDesc', responseDesc).up()
        .ele('type', "1").up()
        .end().replace("<?xml version=\"1.0\"?>", "")
}

class RESP {
    static SUCCESS = responsebuilder("00", "SUKSES")
    static FAILED = responsebuilder("99", "GAGAL")
    static BILLING_NOT_FOUND = responsebuilder("99", "TAGIHAN TIDAK ADA")
    static ERROR = responsebuilder("99", "INTERNAL SERVER ERROR")
    static BUILD = function (urn, data) {
        var ServiceResponse = builder.create('SOAP-ENV:Envelope')
            .att("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/")
            .ele('SOAP-ENV:Body')
            .ele(`ns1:${urn}Response`).att("xmlns:ns1", `urn:${urn}`)
            .ele('ServiceResponse')

        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            ServiceResponse.ele(element[0], element[1])
        }
        return ServiceResponse.end().replace("<?xml version=\"1.0\"?>", "")
    }
} exports.RESP = RESP
