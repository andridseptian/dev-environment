exports.converNumberToWA = async function (number) {
    try {
        if (number.length < 10) {
            return false
        }
        if (number.startsWith("+62")) {
            number = number.substring(1)
        }
        if (number.startsWith("0")) {
            number = "62" + number.substring(1)
        }
        if (!number.startsWith("628")) {
            return false
        }
        return number
    } catch (error) {
        return false;
    }
}