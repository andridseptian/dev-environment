var express = require('express')
const { agentController } = require('../controller/agentController')
const { RESP } = require('../utility/constants')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))
const dt = require('date-fns')
const { db } = require('../database/lakupandai_simulator');
const { default: initModels } = require('../models/init-models');
const { productController } = require('../controller/productController')
const { smsController } = require('../controller/smsController')
const { utilController } = require('../controller/utilController')
const models = initModels(db)

exports.init = async function (app) {
    try {
        await models.response.sync({ alter: true });
        app.use('/api', router)
        router.post('/:service*', async function (req, res) {
            try {
                logger.info("masuk service")
                var reqtime = new Date()
                logger.info(
                    '\nrequest time   : ', dt.format(reqtime, "yyyy-MM-dd hh:mm:ss"),
                    '\nrequest path   : ', req.path,
                    '\nrequest body   : ', req.body,
                    '\nrequest rawbody: ', req.rawBody,
                )

                var response = "RESP.ERROR";
                switch (req.path) {
                    case '/product/get/all': response = await productController.getAllProducts(req, res); break;
                    case '/product/add': response = await productController.addProducts(req, res); break;

                    case '/sms/get/latest': response = await smsController.getLatestSMS(req, res); break;
                    case '/sms/send': response = await smsController.sendSMS(req, res); break;

                    case '/util/encrypt': response = await utilController.tripldeDESEncrypt(req, res); break;
                    case '/util/decrypt': response = await utilController.tripldeDESDecrypt(req, res); break;
                    case '/util/sendrequest/channel': response = await utilController.sendReqToChannel(req, res); break;
                }
                res.send(response)

                var restime = new Date()
                var perftime = dt.differenceInSeconds(restime, reqtime) + '.' + dt.differenceInMilliseconds(restime, reqtime) + 's'
                logger.info(
                    '\nperformance    : ', perftime, "[", dt.formatDistance(reqtime, restime, { includeSeconds: true }), "]",
                    '\nresponse time  : ', dt.format(restime, "yyyy-MM-dd hh:mm:ss"),
                    '\nrequest time   : ', dt.format(reqtime, "yyyy-MM-dd hh:mm:ss"),
                    '\n', '[RETURN]'.padStart(25, "-").padEnd(50, "-"),
                    '\n\npath : ', req.path,
                    '\n\nrequest : ', req.rawBody,
                    '\n\nresponse : ', response,
                    '\n', '[END]'.padStart(25, "-").padEnd(50, "-"))
            } catch (error) {
                logger.error(error)
                res.setHeader('Content-Type', 'text/plain;charset=UTF-8')
                res.send(RESP.IE)
            }
        })

        logger.info("Service Ready")
    } catch (error) {
        logger.error(error)
    }
}

exports.serviceRouter = this