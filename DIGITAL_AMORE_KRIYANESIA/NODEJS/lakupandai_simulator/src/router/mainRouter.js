var express = require('express')
const { agentController } = require('../controller/agentController')
const { RESP } = require('../utility/constants')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))
const dt = require('date-fns')
const { db } = require('../database/lakupandai_simulator');
const { default: initModels } = require('../models/init-models');
const { paymentController } = require('../controller/paymentController')
const { accountTransactionController } = require('../controller/accountTransactionController')
const models = initModels(db)
const { soap2json } = require('../utility/xmltojson');
var xmlformat = require('xml-formatter');



exports.init = async function (app) {
    try {
        await models.response.sync({ alter: true });
        app.use('/', router)
        router.post('/:service', async function (req, res, next) {
            try {
                var reqtime = new Date()
                logger.info(
                    '\nrequest time   : ', dt.format(reqtime, "yyyy-MM-dd hh:mm:ss"),
                    '\nrequest path   : ', req.path,
                    '\nrequest body   : ', req.body,
                    '\nrequest rawbody: ', req.rawBody,
                )
                var response = RESP.ERROR;
                switch (req.params.service) {
                    case 'AgentLogin': response = await agentController.AgentLogin(req, res); break;
                    case 'BalanceInquiry': response = await agentController.BalanceInquiry(req, res); break;
                    case 'AccountTransaction': response = await accountTransactionController.AccountTransaction(req, res); break;
                    case 'Payment': response = await paymentController.generatePayment(req, res); break;
                }
                res.setHeader('Content-Type', 'text/plain;charset=UTF-8')
                res.send(response)

                var restime = new Date()
                var perftime = dt.differenceInSeconds(restime, reqtime) + '.' + dt.differenceInMilliseconds(restime, reqtime) + 's'
                logger.info(
                    '\nperformance    : ', perftime, "[", dt.formatDistance(reqtime, restime, { includeSeconds: true }), "]",
                    '\nresponse time  : ', dt.format(restime, "yyyy-MM-dd hh:mm:ss"),
                    '\nrequest time   : ', dt.format(reqtime, "yyyy-MM-dd hh:mm:ss"),
                    '\n', '[RETURN]'.padStart(25, "-").padEnd(50, "-"),
                    '\n\npath : ', req.path,
                    '\n\nrequest : ', xmlformat(req.rawBody),
                    '\n\nresponse : ', xmlformat(response),
                    '\n', '[END]'.padStart(25, "-").padEnd(50, "-"))
            } catch (error) {
                logger.error(error)
                res.setHeader('Content-Type', 'text/plain;charset=UTF-8')
                res.send(RESP.IE)
            }
        })

        logger.info("Service Ready")
    } catch (error) {
        logger.error(error)
    }
}

exports.mainRouter = this