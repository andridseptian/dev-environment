const { log4js } = require('../utility/logger')

var logger = function (params) {
    var log = log4js.getLogger("lakupandai_simulator.js")
    log.info(params)
}

const { Sequelize } = require('sequelize');
const db_connection = new Sequelize('postgres://postgres:4k535r00t@51.79.219.4:5432/lakupandai_simulator', {
    // logging: logger,
    logging: false,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: false
    }
}) // Example for postgres

exports.db = db_connection