var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');
const { db } = require('../database/lakupandai_simulator');
const { soap2json } = require('../utility/xmltojson');
const { RESP } = require('../utility/constants');
const { default: initModels } = require('../models/init-models');
const models = initModels(db)
const dt = require('date-fns')

class productController {
    static getAllProducts = async function (req, res) {
        logger.info("get product")
        return await models.payment.findAll()
    }
    static addProducts = async function (req, res) {
        logger.info("add product")
        return await models.payment.create(req.body)
    }
} exports.productController = productController