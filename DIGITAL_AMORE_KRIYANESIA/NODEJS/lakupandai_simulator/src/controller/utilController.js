var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))
const tripleDes = require('../utility/tripleDes');
const unirest = require('unirest');


class utilController {
    static tripldeDESEncrypt = async function (req, res) {
        logger.info("tripldeDESEncrypt")
        var text = req.body.text
        logger.info("incoming text: " + text)
        const td = new tripleDes("0102030405060708090A0B0C")
        var encrypted = td.encrypt3DES(text)
        return encrypted
    }

    static tripldeDESDecrypt = async function (req, res) {
        logger.info("tripldeDESDecrypt")
        var text = req.body.text
        logger.info("incoming text: " + text)
        const td = new tripleDes("0102030405060708090A0B0C")
        var decrypted = td.decrypt3DES(text)
        return decrypted
    }

    static sendReqToChannel = async function (req, res) {
        logger.info("sendReqToChannel")
        var td = new tripleDes("0102030405060708090A0B0C")
        var reqbodyenc = td.encrypt3DES(JSON.stringify(req.body))

        var data = await new Promise(function (resolve, reject) {
            unirest('POST', 'https://51.79.219.4:10010/lakupandaidiy/')
                .headers({
                    'Authorization': 'YW5kcm9pZHwxNjE4OTc0Njc0MjU4',
                    'Content-Type': 'text/plain'
                }).strictSSL(false)
                .send(reqbodyenc)
                .end(function (res) {
                    if (res.error) throw new Error(res.error);
                    console.log("resopnse enc: ", res.raw_body, "\n\n");
                    console.log("response dec: ", td.decrypt3DES(res.raw_body));
                    resolve(td.decrypt3DES(res.raw_body))
                });
        })

        return data
    }
} exports.utilController = utilController