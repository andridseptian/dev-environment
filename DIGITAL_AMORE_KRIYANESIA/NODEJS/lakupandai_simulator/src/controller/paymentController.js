var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');
const { db } = require('../database/lakupandai_simulator');
const { soap2json } = require('../utility/xmltojson');
const { RESP } = require('../utility/constants');
const { default: initModels } = require('../models/init-models');
const models = initModels(db)
const dt = require('date-fns')

class paymentController {
    static generatePayment = async function (req, res) {
        try {
            var body = soap2json(req.rawBody)
            logger.info("json body: ", body)

            if (body.data.transactionType === "1") { //PAYMENT INQUIRY
                var checkIdBillings = await models.payment.findOne({ where: { id_billing: body.data.idBilling } })
                logger.info(checkIdBillings)
                if (checkIdBillings) {
                    var additional_data = checkIdBillings.add_inquiry
                    logger.info("additional data: ", additional_data)
                    return RESP.BUILD("Payment",
                        [
                            ["channelId", body.data.channelId],
                            ["responseCode", checkIdBillings.inq_rc],
                            ["responseDesc", checkIdBillings.inq_desc],
                            ["sourceAccountNumber", body.data.sourceAccountNumber],
                            ["productCode", body.data.productCode],
                            ["idBilling", checkIdBillings.id_billing],
                            ["transactionAmount", checkIdBillings.transaction_amount],
                            ["fee", body.data.fee],
                            ["agentFee", body.data.agentFee],
                            ["tax", body.data.tax],
                            ["additionalData", checkIdBillings.add_inquiry],
                            ["transactionType", body.data.transactionType],
                            ["accountType", body.data.accountType],
                            ["jobIdInquiry", dt.format(new Date(), "yyyyMMddhhmmss")],
                            ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")]
                        ])
                } else {
                    return RESP.BILLING_NOT_FOUND
                }
            } else if (body.data.transactionType === "2") { //PAYMENT POSTING
                var checkIdBillings = await models.payment.findOne({ where: { id_billing: body.data.idBilling } })
                logger.info(checkIdBillings)
                if (checkIdBillings) {
                    var additional_data = checkIdBillings.add_posting
                    logger.info("additional data: ", additional_data)
                    return RESP.BUILD("Payment",
                        [
                            ["channelId", body.data.channelId],
                            ["responseCode", checkIdBillings.pos_rc],
                            ["responseDesc", checkIdBillings.pos_desc],
                            ["sourceAccountNumber", body.data.sourceAccountNumber],
                            ["productCode", body.data.productCode],
                            ["idBilling", checkIdBillings.id_billing],
                            ["transactionAmount", checkIdBillings.transaction_amount],
                            ["fee", body.data.fee],
                            ["agentFee", body.data.agentFee],
                            ["tax", body.data.tax],
                            ["additionalData", checkIdBillings.add_posting],
                            ["transactionType", body.data.transactionType],
                            ["accountType", body.data.accountType],
                            ["jobIdInquiry", dt.format(new Date(), "yyyyMMddhhmmss")],
                            ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")]
                        ])
                } else {
                    return RESP.BILLING_NOT_FOUND
                }
            } else if (body.data.transactionType === "3") { //PURCHASE POSTING

            } else {
                return RESP.BUILD("Payment",
                    [
                        ["channelId", body.data.channelId],
                        ["responseCode", "99"],
                        ["responseDesc", "TIPE TRANSAKSI TIDAK ADA"],
                        ["type", body.data.type],
                    ])
            }

            var checkIdBillings = await models.payment.findOne({ where: { id_billing: body.data.idBilling } })
            logger.info(checkIdBillings)
            if (checkIdBillings) {
                return RESP.BUILD("Payment",
                    [
                        ["channelId", body.data.channelId],
                        ["responseCode", "00"],
                        ["responseDesc", "SUKSES"],
                        ["type", body.data.type],
                    ])
            } else {
                return RESP.FAILED
            }
        } catch (error) {
            logger.error(error)
            return RESP.ERROR
        }
    }
} exports.paymentController = paymentController