var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))
const { RESP } = require('../utility/constants');
const dt = require('date-fns')
const { soap2json } = require('../utility/xmltojson');

class accountTransactionController {
    static AccountTransaction = async function (req, res) {
        try {
            var body = soap2json(req.rawBody)
            logger.info("json body: ", body)

            /* 
            <channelId>LP</channelId>
            <responseCode>00</responseCode>
            <responseDesc>SUKSES</responseDesc>
            <sourceAccountNumber>628562863402</sourceAccountNumber>
            <destinationAccountName>FADHLI</destinationAccountName>
            <destinationAccountNumber>001001001</destinationAccountNumber>
            <transactionAmount>11000000</transactionAmount>
            <chargeAmount>100</chargeAmount>
            <descriptionTransaction>INQUIRY</descriptionTransaction>
            <type>5</type>
            <destinationBankCode>009</destinationBankCode>
            */

            if (body.data.type === "1") { //TRANSACTION INQUIRY
                var params = [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["sourceAccountNumber", body.data.sourceAccountNumber],
                    ["destinationAccountName", "DUMMY ACCOUNT NAME"],
                    ["destinationAccountNumber", body.data.destinationAccountNumber],
                    ["transactionAmount", body.data.transactionAmount],
                    ["chargeAmount", body.data.chargeAmount],
                    ["agentFee", body.data.agentFee],
                    ["tax", body.data.tax],
                    ["referenceNumber", body.data.referenceNumber],
                    ["descriptionTransaction", body.data.descriptionTransaction],
                    ["type", body.data.type],
                    ["destinationBankCode", body.data.destinationBankCode],
                    ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")],
                ]
                logger.info("res params: ", params)
                return RESP.BUILD("AccountTransaction", params)
            }
            else if (body.data.type === "2") { // TARIK TUNAI
                var params = [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["sourceAccountNumber", body.data.sourceAccountNumber],
                    ["destinationAccountName", "DUMMY ACCOUNT NAME"],
                    ["destinationAccountNumber", body.data.destinationAccountNumber],
                    ["transactionAmount", body.data.transactionAmount],
                    ["chargeAmount", body.data.chargeAmount],
                    ["agentFee", body.data.agentFee],
                    ["tax", body.data.tax],
                    ["referenceNumber", body.data.referenceNumber],
                    ["descriptionTransaction", body.data.descriptionTransaction],
                    ["type", body.data.type],
                    ["destinationBankCode", body.data.destinationBankCode],
                    ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")],
                ]
                logger.info("res params: ", params)
                return RESP.BUILD("AccountTransaction", params)
            }
            else if (body.data.type === "3") { // SETOR TUNAI
                var params = [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["sourceAccountNumber", body.data.sourceAccountNumber],
                    ["destinationAccountName", "DUMMY ACCOUNT NAME"],
                    ["destinationAccountNumber", body.data.destinationAccountNumber],
                    ["transactionAmount", body.data.transactionAmount],
                    ["chargeAmount", body.data.chargeAmount],
                    ["agentFee", body.data.agentFee],
                    ["tax", body.data.tax],
                    ["referenceNumber", body.data.referenceNumber],
                    ["descriptionTransaction", body.data.descriptionTransaction],
                    ["type", body.data.type],
                    ["destinationBankCode", body.data.destinationBankCode],
                    ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")],
                ]
                logger.info("res params: ", params)
                return RESP.BUILD("AccountTransaction", params)
            }
            else if (body.data.type === "4") { // TRANSFER ON US
                var params = [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["sourceAccountNumber", body.data.sourceAccountNumber],
                    ["destinationAccountName", "DUMMY ACCOUNT NAME"],
                    ["destinationAccountNumber", body.data.destinationAccountNumber],
                    ["transactionAmount", body.data.transactionAmount],
                    ["chargeAmount", body.data.chargeAmount],
                    ["agentFee", body.data.agentFee],
                    ["tax", body.data.tax],
                    ["referenceNumber", body.data.referenceNumber],
                    ["descriptionTransaction", body.data.descriptionTransaction],
                    ["type", body.data.type],
                    ["destinationBankCode", body.data.destinationBankCode],
                    ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")],
                ]
                logger.info("res params: ", params)
                return RESP.BUILD("AccountTransaction", params)
            }
            else if (body.data.type === "5") { // TRANSFER INQUIRY OFF US
                var params = [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["sourceAccountNumber", body.data.sourceAccountNumber],
                    ["destinationAccountName", "DUMMY ACCOUNT NAME"],
                    ["destinationAccountNumber", body.data.destinationAccountNumber],
                    ["transactionAmount", body.data.transactionAmount],
                    ["chargeAmount", body.data.chargeAmount],
                    ["agentFee", body.data.agentFee],
                    ["tax", body.data.tax],
                    ["referenceNumber", body.data.referenceNumber],
                    ["descriptionTransaction", body.data.descriptionTransaction],
                    ["type", body.data.type],
                    ["destinationBankCode", body.data.destinationBankCode],
                    ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")],
                ]
                logger.info("res params: ", params)
                return RESP.BUILD("AccountTransaction", params)
            }
            else if (body.data.type === "6") { // TRANSFER OFF US
                var params = [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["sourceAccountNumber", body.data.sourceAccountNumber],
                    ["destinationAccountName", "DUMMY ACCOUNT NAME"],
                    ["destinationAccountNumber", body.data.destinationAccountNumber],
                    ["transactionAmount", body.data.transactionAmount],
                    ["chargeAmount", body.data.chargeAmount],
                    ["agentFee", body.data.agentFee],
                    ["tax", body.data.tax],
                    ["referenceNumber", body.data.referenceNumber],
                    ["descriptionTransaction", body.data.descriptionTransaction],
                    ["type", body.data.type],
                    ["destinationBankCode", body.data.destinationBankCode],
                    ["rrn", dt.format(new Date(), "yyyyMMddhhmmss")],
                ]
                logger.info("res params: ", params)
                return RESP.BUILD("AccountTransaction", params)
            }

        } catch (error) {
            logger.error(error)
            return RESP.ERROR
        }
    }
} exports.accountTransactionController = accountTransactionController
