var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');
const { db } = require('../database/lakupandai_simulator');
const { soap2json } = require('../utility/xmltojson');
const { RESP } = require('../utility/constants');
const { default: initModels } = require('../models/init-models');
const models = initModels(db)

class agentController {
    static AgentLogin = async function (req, res) {
        try {
            var body = soap2json(req.rawBody)
            logger.info("json body: ", body)
            var checkuser = await models.agent.findAll({ where: { id: body.data.userId } })
            if (checkuser.length > 0) {
                return RESP.BUILD("AgentLogin",
                    [
                        ["channelId", body.data.channelId],
                        ["responseCode", "00"],
                        ["responseDesc", "SUKSES"],
                        ["type", body.data.type],
                    ])
            } else {
                return RESP.FAILED
            }
        } catch (error) {
            logger.error(error)
        }
    }

    static BalanceInquiry = async function (req, res) {
        var body = soap2json(req.rawBody)
        logger.info("json body: ", body)
        var checkuser = await models.agent.findAll({ where: { id: body.data.userId } })
        if (checkuser.length > 0) {
            var user = checkuser[0]
            return RESP.BUILD("AgentLogin",
                [
                    ["channelId", body.data.channelId],
                    ["responseCode", "00"],
                    ["responseDesc", "SUKSES"],
                    ["phoneNumber", user.phone_number],
                    ["accountName", user.name],
                    ["balance", user.saldo],
                ])
        } else {
            return RESP.FAILED
        }
    }
} exports.agentController = agentController