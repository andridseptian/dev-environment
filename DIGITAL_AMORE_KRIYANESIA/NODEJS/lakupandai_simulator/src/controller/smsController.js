var { log4js } = require('../utility/logger')
var logger = log4js.getLogger(require('path').basename(__filename))

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');
const { db } = require('../database/lakupandai_simulator');
const { soap2json } = require('../utility/xmltojson');
const { RESP } = require('../utility/constants');
const { default: initModels } = require('../models/init-models');
const models = initModels(db)
const dt = require('date-fns')
const convert = require('xml-js');


class smsController {
    static getLatestSMS = async function (req, res) {
        logger.info("get latest sms")
        return await models.sms.findAll({
            order: [["id", "DESC"]],
            limit: 20
        })
    }

    static sendSMS = async function (req, res) {
        logger.info("send sms")
        var xml = req.rawBody
        var result = convert.xml2json(xml, { compact: false, spaces: 4 });
        var res = JSON.parse(result)
        console.log(res.elements[0].elements[0].elements);
        var serviceresponse = res.elements[0].elements[0].elements

        var json = {}
        for (let index = 0; index < serviceresponse.length; index++) {
            const element = serviceresponse[index];
            json[element.name] = element.elements[0].text
        }

        var sendedsms = await models.sms.create({
            cr_time: new Date(),
            date_time: json.datetime,
            destination_number: json.destinationNumber,
            message: json.message

        })

        logger.info("json body: ", sendedsms.toJSON())
        return `<?xml version="1.0" encoding="UTF-8"?>
        <smsbc>    
        <response>        
        <datetime>${json.datetime}</datetime>        
        <rrn>1122019080915345646518822679855</rrn>        
        <partnerId>1121</partnerId>        
        <partnerName>lakupandaiDIY</partnerName>        
        <destinationNumber>${json.destinationNumber}</destinationNumber>        
        <message>${json.message}</message>        
        <rc>1</rc>        
        <rm>Success</rm>    
        </response>
        </smsbc>`.trim().replace(/\n/g,"").replace(/  /g," ")
    }
} exports.smsController = smsController