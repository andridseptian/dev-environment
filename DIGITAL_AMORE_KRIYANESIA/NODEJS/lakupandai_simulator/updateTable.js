const { db } = require("./src/database/lakupandai_simulator");
const { default: initModels } = require("./src/models/init-models");
const models = initModels(db)

models.payment.sync({ alter: true })
