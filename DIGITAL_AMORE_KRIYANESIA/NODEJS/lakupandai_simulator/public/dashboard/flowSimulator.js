import { html, render } from '/node_modules/lit-html/lit-html.js';
import * as modal from './modal.js';
import { encrypt3DES } from './tripleDes.js';
import { tdencrypt, tddecrypt } from './util.js';

export async function flowSimulator(contentContainer) {
    render(html`
        <h2>Flow Simulator</h2>
        <hr>
        <section>
            <div class="form-group">
                <label for=""></label>
                <select class="form-control" name="" id="flowselector">
                    <option>select...</option>
                    <option value="requestTransferAntarBank">Transfer Antar Bank - Request</option>
                    <option value="confirmTransferAntarBank">Transfer Antar Bank - Confirm</option>
                    <option value="requestSetorTunai">Setor Tunai - Request</option>
                    <option value="requestSetorTunaiNonBSA">Setor Tunai Non BSA - Request</option>
                </select>
            </div>
            <div class="container-fluid" id="flowContent">
            </div>
        </section>
    `, document.getElementById(contentContainer))

    $("#flowselector").on("change", async function () {
        console.log("flow: ", $(this));
        if (this.value === "requestTransferAntarBank") {
            requestTransferAntarBank()
        }
        else if (this.value === "requestSetorTunai") {
            requestSetorTunai()
        }
        else if (this.value === "requestSetorTunaiNonBSA") {
            requestSetorTunaiNonBSA()
        }
        else {
            render(html`
                <p>Select Flow</p>
            `, document.getElementById("flowContent"))
        }
    });
}

async function requestTransferAntarBank() {
    render(html`
        <h4>Transfer antar bank</h4>
        <div class="form-group">
            <label for="">No HP Nasabah</label>
            <input type="text" class="form-control" name="" id="">
        </div>
        <div class="form-group">
            <label for="">Kode Bank Tujuan</label>
            <input type="text" class="form-control" name="" id="">
        </div>
        <div class="form-group">
            <label for="">Kode Bank Penerima</label>
            <input type="text" class="form-control" name="" id="">
        </div>
        <button type="button" name="" id="send-inquiry" class="btn btn-primary btn-lg btn-block">Send</button>
    `, document.getElementById("flowContent"))

    $("#send-inquiry").on("click", async function () {
        try {
            var body = {
                "msisdn_agen": "andri0580",
                "msisdn_nasabah": "082196740588",
                "nominal": "25000",
                "action": "request_setor_tunai"
            }
            var data = await sendReqToChannel(body)
            console.log(data);

            var response = JSON.parse(data)
            var rm = JSON.parse(response.RM)
            console.log(rm);
            var formData = rm.data
            var tokenData = rm.token

            for (let index = 0; index < formData.length; index++) {
                const element = formData[index];
                console.log(element.header, element.value);
            }

            render(html`
                <h4>Transfer antar bank</h4>
                ${formData.map(item => html`
                <div class="row">
                    <div class="col">${item.header}</div>
                    <div class="col">${item.value}</div>
                </div>
                `)}
                <button type="button" name="" id="send-inquiry-pin" class="btn btn-primary btn-lg btn-block">Send</button>
            `, document.getElementById("flowContent"))

            $("#send-inquiry-pin").on("click", async function () {
                var body = {
                    "msisdn_agen": "andri0580",
                    "msisdn_nasabah": "082196740588",
                    "nominal": "25000",
                    "action": "request_setor_tunai"
                }
                var data = await sendReqToChannel(body)
            })

        } catch (error) {
            console.log(error);
        }
    });
}

async function requestSetorTunai() {
    render(html`
        <h4>Setor Tunai</h4>
        <div class="form-group">
            <label for="">agen</label>
            <input type="text" class="form-control" name="" id="input-agent">
        </div>
        <div class="form-group">
            <label for="">No HP Nasabah</label>
            <input type="text" class="form-control" name="" id="input-msidn">
        </div>
        
        <div class="form-group">
            <label for="">Nominal</label>
            <input type="text" class="form-control" name="" id="input-nominal">
        </div>
        <button type="button" name="" id="send-inquiry" class="btn btn-primary btn-lg btn-block">Send</button>
    `, document.getElementById("flowContent"))

    $("#send-inquiry").on("click", async function () {
        try {
            $(this).prop('disabled', true);
            $(this).html(`
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            <span class="sr-only">Loading...</span>
            `);
            var agent = $("#input-agent").val()

            var body = {
                "msisdn_agen": $("#input-agent").val(),
                "msisdn_nasabah": $("#input-msidn").val(),
                "nominal": $("#input-nominal").val(),
                "action": "request_setor_tunai"
            }
            var data = await sendReqToChannel(body)
            console.log(data);

            var response = JSON.parse(data)
            var rm = JSON.parse(response.RM)
            console.log(rm);
            var formData = rm.data ? rm.data : null
            var tokenData = response.token

            for (let index = 0; index < formData.length; index++) {
                const element = formData[index];
                console.log(element.header, element.value);
            }

            render(html`
                <h4>Setor Tunai</h4>
                ${formData.map(item => html`
                <div class="row">
                    <div class="col">${item.header}</div>
                    <div class="col">${item.value}</div>
                </div>
                `)}
                <div class="form-group">
                    <input type="text" class="form-control" name="" id="input-pin" placeholder="PIN">
                </div>
                <button type="button" name="" id="send-inquiry-pin" class="btn btn-primary btn-lg btn-block">Send</button>
                
                <p class="my-3">
                    <pre>body: ${JSON.stringify(body)}</pre>
                </p>
                <p class="my-3">
                    <pre>response: ${data}</pre>
                </p>

            `, document.getElementById("flowContent"))

            $("#send-inquiry-pin").on("click", async function () {
                $(this).prop('disabled', true);
                $(this).html(`
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span class="sr-only">Loading...</span>
                `);
                var body = {
                    "msisdn_agen": agent,
                    "token": tokenData,
                    "pin": $("#input-pin").val(),
                    "action": "posting_setor_tunai"
                }
                var data = await sendReqToChannel(body)
                console.log(data);

                render(html`
                    <h4>Setor Tunai</h4>
                    <p class="my-3">
                        <pre>body: ${JSON.stringify(body)}</pre>
                    </p>
                    <p class="my-3">
                        <pre>response: ${data}</pre>
                    </p>
                `, document.getElementById("flowContent"))

            })

        } catch (error) {
            console.log(error);
        }
    });
}

async function requestSetorTunaiNonBSA() {
    render(html`
        <h4>Setor Tunai Non BSA</h4>
        <div class="form-group">
            <label for="">agen</label>
            <input type="text" class="form-control" name="" id="input-agent">
        </div>
        <div class="form-group">
            <label for="">No HP Nasabah</label>
            <input type="text" class="form-control" name="" id="input-msidn">
        </div>
        
        <div class="form-group">
            <label for="">Nominal</label>
            <input type="text" class="form-control" name="" id="input-nominal">
        </div>
        <button type="button" name="" id="send-inquiry" class="btn btn-primary btn-lg btn-block">Send</button>
    `, document.getElementById("flowContent"))

    $("#send-inquiry").on("click", async function () {
        try {
            $(this).prop('disabled', true);
            $(this).html(`
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            <span class="sr-only">Loading...</span>
            `);
            var agent = $("#input-agent").val()

            var body = {
                "msisdn_agen": $("#input-agent").val(),
                "rek_nasabah": $("#input-msidn").val(),
                "nominal": $("#input-nominal").val(),
                "action": "request_setor_tunai_non_bsa"
            }
            var data = await sendReqToChannel(body)
            console.log(data);

            var response = JSON.parse(data)
            var rm = JSON.parse(response.RM)
            console.log(rm);
            var formData = rm.data ? rm.data : null
            var tokenData = response.token

            for (let index = 0; index < formData.length; index++) {
                const element = formData[index];
                console.log(element.header, element.value);
            }

            render(html`
                <h4>Setor Tunai Non BSA</h4>
                ${formData.map(item => html`
                <div class="row">
                    <div class="col">${item.header}</div>
                    <div class="col">${item.value}</div>
                </div>
                `)}
                <div class="form-group">
                    <input type="text" class="form-control" name="" id="input-pin" placeholder="PIN">
                </div>
                <button type="button" name="" id="send-inquiry-pin" class="btn btn-primary btn-lg btn-block">Send</button>
                
                <p class="my-3">
                    <pre>body: ${JSON.stringify(body)}</pre>
                </p>
                <p class="my-3">
                    <pre>response: ${data}</pre>
                </p>

            `, document.getElementById("flowContent"))

            $("#send-inquiry-pin").on("click", async function () {
                $(this).prop('disabled', true);
                $(this).html(`
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span class="sr-only">Loading...</span>
                `);
                var body = {
                    "msisdn_agen": agent,
                    "token": tokenData,
                    "pin": $("#input-pin").val(),
                    "action": "posting_setor_tunai_non_bsa"
                }
                var data = await sendReqToChannel(body)
                console.log(data);

                render(html`
                    <p class="my-3">
                        <pre>body: ${JSON.stringify(body)}</pre>
                    </p>
                    <p class="my-3">
                        <pre>response: ${data}</pre>
                    </p>
                `, document.getElementById("flowContent"))

            })

        } catch (error) {
            console.log(error);
        }
    });
}

async function sendReqToChannel(body) {
    var data = await new Promise(function (resolve, reject) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(body)
        console.log("body: " + JSON.stringify(body));
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("/api/util/sendrequest/channel", requestOptions)
            .then(response => response.text())
            .then(result => resolve(result))
            .catch(error => reject(error));
    })

    return data;
}