/* globals Chart:false, feather:false */
import * as sidebar from "./sidebar.js"
import * as content from "./content.js"
import * as modal from './modal.js';

(function () {
  'use strict'

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  console.log(urlParams.toString());
  console.log(queryString);

  modal.init("modal-container")
  sidebar.init("sidebar-container")
  content.init("content-container", queryString.replace("?", ""))

  feather.replace()
})()
