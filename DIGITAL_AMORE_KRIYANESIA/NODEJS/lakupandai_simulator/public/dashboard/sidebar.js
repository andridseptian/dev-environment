import { html, render } from '/node_modules/lit-html/lit-html.js';

export function init(sidebarcontainer) {
    render(html`
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
        <div class="sidebar-sticky pt-3">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="?/product">
                        <span data-feather="home"></span>
                        Product <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?/otp">
                        <span data-feather="home"></span>
                        Otp <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?/sim">
                        <span data-feather="home"></span>
                        Flow Simulator <span class="sr-only">(current)</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    `
        , document.getElementById(sidebarcontainer))
}