import { html, render } from '/node_modules/lit-html/lit-html.js';
import * as modal from './modal.js';
import { flowSimulator } from './flowSimulator.js';

export async function init(contentContainer, query) {
    console.log("query:",query);
    if (query === "/product") {
        render(html`
        <h2>Product Payment</h2>
        <hr>
        <section>
        <button id="add-product" type="button" class="btn btn-sm btn-primary float-right mt-3 mb-3">Add Product</button>
        <div id="table-container" class="table-responsive"></div>
        </section>
        `
            , document.getElementById(contentContainer))

        generateTable("/api/product/get/all", "table-container")

    } 
    else if(query === "/otp") {
        render(html`
            <h2>Section OTP</h2>
            <hr>
            <section>
                <button id="otp-refresh" type="button" class="btn btn-sm btn-primary float-right mt-3 mb-3">refresh</button>
                <div id="table-otp" class="table-responsive"></div>
            </section>
        `
            , document.getElementById(contentContainer))
        getLatestOTP()

        document.getElementById("otp-refresh").onclick = function () {
            getLatestOTP()
        }
    }
    else if(query === "/sim") {
        flowSimulator(contentContainer)
    }
    else {
        render(html`
        <h2>Section Not Found</h2>
        <hr>
        <section>
            <p>Try to using another path</p>
        </section>
        `
            , document.getElementById(contentContainer))
    }
}

async function getLatestOTP(){
    var data = await new Promise(function (resolve, reject) {
        var requestOptions = {
            method: 'POST',
            redirect: 'follow'
        };

        fetch("/api/sms/get/latest", requestOptions)
            .then(response => response.text())
            .then(result => resolve(result))
            .catch(error => reject(error));
    })
    var s = JSON.parse(data)
    console.log(s);
    var keys = [];
    for (var k in s[0]) keys.push(k);
    console.log(keys);
    render(html`
    <table class="table table-sm table-striped table-bordered table-light table-hover">
        <thead>
            <tr>
                ${keys.map(item => html`
                <th>${item}</th>
                `)}
            </tr>
        </thead>
        <tbody>
            ${s.map(obj => html`
            <tr>
                ${keys.map((item) => 
                {
                    return html`
                    <td>
                        <p>${obj[item]}</p>
                    </td>
                    `
                })}
            </tr>
            `)}
        </tbody>
    </table>
    `
        , document.getElementById("table-otp"))

}

async function generateTable(path, tablecontainer) {

    var data = await new Promise(function (resolve, reject) {
        var requestOptions = {
            method: 'POST',
            redirect: 'follow'
        };

        fetch(path, requestOptions)
            .then(response => response.text())
            .then(result => resolve(result))
            .catch(error => reject(error));
    })

    var s = JSON.parse(data)
    console.log(s);
    var keys = [];
    for (var k in s[0]) keys.push(k);
    console.log(keys);
    render(html`
    <table class="table table-sm table-striped table-bordered table-light table-hover">
        <thead>
            <tr>
                <th style="width:100"></th>
                ${keys.map(item => html`
                <th>${item}</th>
                `)}
            </tr>
        </thead>
        <tbody>
            ${s.map(obj => html`
            <tr>
                <td>
                    <div class="container">
                        <button type="button" class="btn btn-sm btn-block btn-primary">?</button>
                        <button type="button" class="btn btn-sm btn-block btn-danger">X</button>
                    </div>
                </td>
                ${keys.map((item) => 
                {
                    if(item === "add_inquiry" || item === "add_posting"){
                        var json = JSON.parse(obj[item])
                        var pretty = JSON.stringify(json,null,2)
                        return html`
                        <td>
                        <pre>${pretty}</pre>
                        </td>`
                    } else {
                        return html`
                        <td>
                            <p>${obj[item]}</p>
                        </td>
                        `
                    }
                })}
            </tr>
            `)}
        </tbody>
    </table>
    `
        , document.getElementById(tablecontainer))

    document.getElementById("add-product").onclick = function () {
        addProductModal(keys)
    }
}

function addProductModal(keys){
    modal.setHeader("Add Product")
    modal.setBody(html`
        ${keys.map(item => {
            if(item === "add_inquiry" || item === "add_posting"){
                return html`
                        <div class="form-group">
                            <label for="add-product-${item}">${item.toUpperCase().replace(/_/g, " ")}</label>
                            <textarea class="form-control" name="" id="add-product-${item}" rows="3"></textarea>
                        </div>
                    `
            } else {
                return html`
                    <div class="form-group">
                        <label for="add-product-${item}">${item.toUpperCase().replace(/_/g, " ")}</label>
                        <input type="text"
                            class="form-control" name="" id="add-product-${item}" aria-describedby="helpId-${item}" placeholder="">
                    </div>
                `
            }
        })}

    `)
    modal.setFooter(html`
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button id="add-product-confirm" type="button" class="btn btn-sm btn-danger">Add</button>
    `)
    modal.show()

    document.getElementById("add-product-confirm").onclick = function() {

        var jsonData = {}
        for (let index = 0; index < keys.length; index++) {
            const element = keys[index];
            jsonData[element] = document.getElementById(`add-product-${element}`).value
        }

        console.log(jsonData);

        modal.showConfirm(html`
        <p>Yakin akan menambahkan data ini?</p>
        ${keys.map(item => {
            var value = document.getElementById(`add-product-${item}`).value
            return html`
            <li>${item} <ul>${value}</ul> </li>
            `
        })}
        `, addProduct, jsonData)
    }



}

async function addProduct(jsonData){
    
    var addProductResult = await new Promise(function (resolve, reject) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify(jsonData);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("/api/product/add", requestOptions)
        .then(response => response.text())
        .then(result => resolve(result))
        .catch(error => reject(error));
    })
    
    alert(addProductResult)
}

/*
       <thead>
            <tr>
                ${keys.map(item => html`
                <th>${item}</th>
                `)}
            </tr>
        </thead>
        <tbody>
            ${s.map(obj => html`
            <tr>
                ${keys.map(item => html`
                <td><p>${obj[item]}</p></td>
                `)}
            </tr>
            `)}
        </tbody>
*/