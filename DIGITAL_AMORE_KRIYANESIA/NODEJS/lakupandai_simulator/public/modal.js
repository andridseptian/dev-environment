import { html, render } from '/node_modules/lit-html/lit-html.js';

export function init(modalid) {
    render(html`
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="modal-title" class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modal-body" class="container-fluid">
                        Add rows here
                    </div>
                </div>
                <div id="modal-footer" class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    `
        , document.getElementById(modalid))
    // $('#modelId').modal()
}

export function setHeader(header) {
    render(header, document.getElementById("modal-title"))
}

export function setBody(body) {
    render(body, document.getElementById("modal-body"))
}

export function setFooter(body) {
    render(body, document.getElementById("modal-footer"))
}

export function show() {
    $('#modelId').modal('show')
}

export function hide() {
    $('#modelId').modal('hide')
}

export function showConfirm(confirmBody, callback) {
    setHeader("Konfirmasi")
    setBody(confirmBody)
    setFooter(html`
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
        <button id="modal-confirm" type="button" class="btn btn-primary">Iya</button>
    `)
    document.getElementById("modal-confirm").onclick = function() {
        hide()
        callback()
    }
    show()
}