import * as modal from './modal.js';
import { html, render } from '/node_modules/lit-html/lit-html.js';

(function () {
    render(html`
        <div class="row justify-content-md-center mt-5">
            <div class="col-md-auto">
                <button id="btn-daftar-produk" type="button" class="btn btn-primary">Daftar Produk</button>
                <button id="btn-daftar-agen" type="button" class="btn btn-primary">Daftar Agen</button>
            </div>
        </div>
        <div id="modal-container"></div>
    `
        , document.getElementById('content-body'))

    modal.init("modal-container")

    document.getElementById("btn-daftar-agen").onclick = function () {
        modal.setBody(html`
        <button id="btn-test" type="button" class="btn btn-primary">Daftar Agen</button>
        `)
        modal.show()

        document.getElementById("btn-test").onclick = function () {
            modal.showConfirm("konfirmasi nih?", function () {
                alert("bisa cuy")
            })
        }
    }


})()