try {
    const tripleDes = require("./tripleDes");

    // qwF90mTsxWpDIk6b5bC6FL5XhZ8hDqUqyFnn2EfjUc4=

    var td = new tripleDes("APLIKASISHARINGBANDWIDTHENKRIPSI");

    console.log(td.decrypt3DES("qwF90mTsxWpDIk6b5bC6FL5XhZ8hDqUqyFnn2EfjUc4="));
} catch (error) {
    console.error(error);
}
