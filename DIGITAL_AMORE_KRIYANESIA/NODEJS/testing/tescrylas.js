
async function update() {
    return new Promise(function (resolve, reject) {
        var request = require('request');
        var options = {
            'method': 'POST',
            'url': 'http://srv.co.id:9967/crylas/web_admin/update_flow_mob',
            'headers': {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "account_type": "Tes123",
                "deleted": false,
                "form": true,
                "id_ref": "2",
                "id_username": "anadara",
                "description": "-",
                "id": "68",
                "fromid": "1"
            })

        };

        console.log("update body: ", options.body);

        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log("update response: ", response.body);
            resolve(response.body)
        });
    })
}

async function get() {
    return new Promise(function (resolve, reject) {
        var request = require('request');
        var options = {
            'method': 'POST',
            'url': 'http://srv.co.id:9967/crylas/web_admin/get_flow_mob',
            'headers': {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "id_username": "anadara",
                "filters": [
                    {
                        "column": "id",
                        "operator": "!=",
                        "value": "0"
                    }
                ],
                "orderBy": "id",
                "orderDirection": "desc",
                "page": 0,
                "pageSize": 5
            })

        };

        console.log("get body: ", options.body);

        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log("get response: ", response.body);
            resolve(response.body);
        });
    })
}

(async function () {
    // update();
    // get();

    await update();
    await get();
})()

// update();
// setTimeout(() => {
//     get();
// }, 5000);
