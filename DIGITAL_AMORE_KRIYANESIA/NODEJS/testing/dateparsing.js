console.log(new Date())
console.log(new Date().toISOString())
console.log(new Date().getFullYear().toString().substring(2))
console.log((new Date().getMonth() + 1).toString().padStart(2,"0"))
console.log((new Date().getDate()).toString().padStart(2,"0"))
console.log(new Date().toTimeString())


var year = new Date().getFullYear().toString().substring(2)
var month = (new Date().getMonth() + 1).toString().padStart(2,"0")
var date = (new Date().getDate()).toString().padStart(2,"0")
var hour = (new Date().getHours()).toString().padStart(2,"0")
var minutes = (new Date().getMinutes()).toString().padStart(2,"0")
var seconds = (new Date().getSeconds()).toString().padStart(3,"0")
var milliseconds = (new Date().getMilliseconds()).toString().padStart(3,"0")

var result =  year + month + date + hour + minutes + seconds + milliseconds

console.log(result);