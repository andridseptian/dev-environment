var args = {}
for (let index = 0; index < process.argv.length; index++) {
    const val = process.argv[index];
    console.log(index + ': ' + val);
    var argument = val.split('=');
    if (argument[1]) args[argument[0]] = argument[1];
}

console.log(args);
var appconfig;
try {
    appconfig = require(args.configPath)
} catch (error) {
    try {
        appconfig = require('./app-configxx.json')
    } catch (error) {
        console.log(`config file not found in '${args.configPath}' and './', please specify your config file location using 'configPath' argument`)
        return
    }
}
console.log(appconfig);