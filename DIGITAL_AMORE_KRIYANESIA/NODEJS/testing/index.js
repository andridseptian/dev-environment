const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');
const sequelize = new Sequelize('postgres://postgres:postgres@localhost:5432/postgres') // Example for postgres

class User extends Model { }
User.init({
    userid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    fullname: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    status: DataTypes.INTEGER
}, { sequelize, modelName: 'user' });

// (async () => {
//     await sequelize.sync({ alter: true });
//     const jane = await User.create({
//         fullname: 'administrator',
//         username: 'admin',
//         password: 'testing',
//         status: 1
//     });
//     console.log(jane.toJSON());
// })();

(async function () {
    // await sequelize.sync({ alter: true });
    // const findusers = await sequelize.query("SELECT * FROM `user`", { type: QueryTypes.SELECT });
    const results = await sequelize.query("select * from users");
    // console.log(results[0])
})();


(async function () {
    const userselect = await User.findOne({ where: { userid: 3 } });
    console.log(userselect.toJSON())   
    userselect.username = "gantilagi"
    userselect.save()
    console.log("Updated: ");
    console.log(userselect.toJSON());
})();


