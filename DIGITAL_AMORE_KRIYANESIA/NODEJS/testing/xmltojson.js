var convert = require('xml-js');
var xml = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
	xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:ns1="urn:Payment">
	<SOAP-ENV:Body>
		<ns1:PaymentResponse>
			<ServiceResponse>
				<channelId>LP</channelId>
				<responseCode>00</responseCode>
				<responseDesc>SUKSES</responseDesc>
				<sourceAccountNumber>001221033743</sourceAccountNumber>
				<productCode>050113</productCode>
				<idBilling>3471000030599905</idBilling>
				<transactionAmount>2625700</transactionAmount>
				<additionalData>{
 "idRetribusi": "3471000030599905",
 "nama": "TESTING1",
 "tahun": "2020",
 "bulan": "03-2020 sd 07-2020",
 "posisi": "BERINGHARJO BARAT\/1LOS3B",
 "jmlTagihan": "Rp.26.257,00"
 }</additionalData>
				<transactionType>1</transactionType>
				<jobIdInquiry>20210326080305627038</jobIdInquiry>
				<rrn>080305627038</rrn>
			</ServiceResponse>
		</ns1:PaymentResponse>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
`;
var result2 = convert.xml2json(xml, { compact: false, spaces: 4 });
console.log(result2);
console.log("---------------------------");
var res = JSON.parse(result2)
console.log(res.elements[0].elements[0].elements[0].elements[0]);
var serviceresponse = res.elements[0].elements[0].elements[0].elements[0].elements
console.log(serviceresponse);
var json = {}
for (let index = 0; index < serviceresponse.length; index++) {
    const element = serviceresponse[index];
    json[element.name] = element.elements[0].text

}

var convertres = {
    attributes: res.elements[0].attributes,
    name: res.elements[0].elements[0].elements[0].name,
    data: json
}

console.log(res.elements[0].attributes)
console.log(json);
console.log(convertres);
