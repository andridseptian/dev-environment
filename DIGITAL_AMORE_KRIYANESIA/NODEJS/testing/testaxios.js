function update() {
    return new Promise(function (resolve, reject) {
        var axios = require('axios');
        var data = JSON.stringify({
            "account_type": "Tes123",
            "deleted": false,
            "form": true,
            "id_ref": "2",
            "id_username": "anadara",
            "description": "-",
            "id": "68",
            "fromid": "1"
        });

        var config = {
            method: 'post',
            url: 'http://srv.co.id:9967/crylas/web_admin/update_flow_mob',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        console.log("==>update request:", data);
        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
                console.log("==>update response:", JSON.stringify(response.data));
                resolve(JSON.stringify(response.data))
            })
            .catch(function (error) {
                console.log(error);
            });
    })
}

async function get() {
    return new Promise(function (resolve, reject) {
        var axios = require('axios');
        var data = JSON.stringify({
            "id_username": "anadara",
            "filters": [
                {
                    "column": "id",
                    "operator": "!=",
                    "value": "0"
                }
            ],
            "orderBy": "id",
            "orderDirection": "desc",
            "page": 0,
            "pageSize": 5
        });

        var config = {
            method: 'post',
            url: 'http://srv.co.id:9967/crylas/web_admin/get_flow_mob',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        console.log("==>get request:", data);
        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
                console.log("==>get response:", JSON.stringify(response.data));
                resolve(JSON.stringify(response.data))
            })
            .catch(function (error) {
                console.log(error);
            });
    })
}

(async function () {
    // update();
    // get();

    await update();
    await get();
})()