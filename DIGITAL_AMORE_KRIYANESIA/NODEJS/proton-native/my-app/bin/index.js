"use strict";

var _react = _interopRequireDefault(require("react"));

var _protonNative = require("proton-native");

var _app = _interopRequireDefault(require("./app"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_protonNative.AppRegistry.registerComponent("example", /*#__PURE__*/_react["default"].createElement(_app["default"], null)); // and finally render your main component
// ================================================================================
// This is for hot reloading (this will be stripped off in production by webpack)
// THIS SHOULD NOT BE CHANGED


if (module.hot) {
  module.hot.accept(["./app"], function () {
    var app = require("./app")["default"];

    _protonNative.AppRegistry.updateProxy(app);
  });
}