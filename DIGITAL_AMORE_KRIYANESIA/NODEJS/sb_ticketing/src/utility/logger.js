var path = require('path')
var date = require('../utility/dateFormat')
var log4js = require('log4js')

log4js.configure({
    appenders: {
        out: { type: 'stdout' },
        app: { type: 'file', filename: path.join(__dirname, '../../log', `app-${date.getNowDate()}.log`) }
    },
    categories: {
        default: { appenders: ['out', 'app'], level: 'info' }
    }
});

const logger = log4js.getLogger();

function info(params) {
    logger.info(params)
}

// console.log = function (params) {
//     logger.info(params)
// }

exports.logger = logger
exports.log4js = log4js
exports.info = info