// import * as postgres from '../database/postgres.js'
const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_gateway_db } = require('../../database/sb_gateway')

var df = require('../../utility/dateFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("userapp")

var sequelize = sb_gateway_db;

/* 

*/

class Lembaga extends Model { }
Lembaga.init({
    bank_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    bank_name: DataTypes.STRING,
    cr_time: DataTypes.DATE,
    mod_time: DataTypes.DATE,
    status: DataTypes.INTEGER,
    terminal_count: DataTypes.INTEGER,
    due_date: DataTypes.DATE,
    last_login_time: DataTypes.DATE,
    info: DataTypes.TEXT,
    additional_data: DataTypes.TEXT,
    status_login: DataTypes.INTEGER,
    sn_limit: {
        type: DataTypes.STRING,
        defaultValue: '5'
    }

}, { sequelize, modelName: 'lembaga' });

(async function () {
    logger.info("[Lembaga Initialize]");
    await sequelize.sync();
})();

exports.selectAll = async function () {
    const lembaga = await Lembaga.findAll();
    // logger.info(userapp)
    return lembaga;
}

exports.selectActive = async function () {
    const lembaga = await Lembaga.findAll({
        where: {
            status: 1
        },
        order: [
            ['cr_time', 'DESC']
        ]
    });
    return lembaga;
}

exports.selectNotActive = async function () {
    const lembaga = await Lembaga.findAll({
        where: {
            status: { [Op.ne]: 1 }
        },
        order: [
            ['cr_time', 'DESC']
        ]
    });
    return lembaga;
}

exports.selectNotPaid = async function () {
    const lembaga = await Lembaga.findAll({
        where: {
            due_date: { [Op.lte]: df.currentDueDateBilling() }
        },
        order: [
            ['due_date', 'DESC']
        ]
    });
    return lembaga;
}

exports.selectByBankId = async function (body) {
    const lembaga = await Lembaga.findOne({
        where: {
            bank_id: body.bank_id
        }
    });
    return lembaga;
}


exports.updateLembaga = async function (body) {
    try {
        const lembaga = await Lembaga.update(
            {
                bank_name: body.bank_name,
                mod_time: new Date(),
                status: body.status,
                due_date: body.due_date,
                sn_limit: body.sn_limit
            },
            {
                where: {
                    bank_id: body.bank_id
                }
            }
        );
        return lembaga;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}