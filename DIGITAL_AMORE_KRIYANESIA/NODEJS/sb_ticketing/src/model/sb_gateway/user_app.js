const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_gateway_db } = require('../../database/sb_gateway')
const sql = require('sql-template-strings')


var df = require('../../utility/dateFormat')
var rf = require('../../utility/responseFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("userapp")

var sequelize = sb_gateway_db;

/* 
    user_id int8 NOT NULL,
    cr_time timestamp(0) NULL,
    mod_time timestamp(0) NULL,
    bank_id bigserial NOT NULL,
    username varchar NULL,
    "password" varchar NULL,
    fullname varchar NULL,
    nik varchar NULL,
    email varchar NULL,
    phone_number varchar NULL,
    user_type int4 NULL,
    expired_password timestamp(0) NULL,
    fail_login int4 NULL,
    status int4 NULL,
    info text NULL,
    additional_data text NULL,
    status_login int4 NULL,
    last_login timestamp NULL,
    last_logout timestamp NULL,
    terminal_login int8 NULL,
    activate_date timestamp(0) NULL,
*/

class UserApp extends Model { }
UserApp.init({
    user_id: {
        type: DataTypes.BIGINT,
        primaryKey: true
    },
    cr_time: DataTypes.DATE,
    mod_time: DataTypes.DATE,
    bank_id: DataTypes.BIGINT,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    fullname: DataTypes.STRING,
    nik: DataTypes.STRING,
    email: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    user_type: DataTypes.INTEGER,
    expired_password: DataTypes.DATE,
    fail_login: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    info: DataTypes.TEXT,
    additional_data: DataTypes.TEXT,
    status_login: DataTypes.INTEGER,
    last_login: DataTypes.DATE,
    last_logout: DataTypes.DATE,
    terminal_login: DataTypes.INTEGER,
    activate_date: DataTypes.DATE,

}, { sequelize, modelName: 'user_app' });

(async function () {
    logger.info("[User App Initialize]");
    await sequelize.sync();
})();

exports.findByUsernameAndPassword = async function (username, password) {
    try {
        const [userapp, meta] = await sequelize.query(sql`
            select
                b.bank_id,
                b.bank_name,
                b.due_date,
                a.*
            from
                user_app a
            inner join lembaga b on
                a.bank_id = b.bank_id
            where 
                a.username = ${username} 
                AND a.password = ${password}
            LIMIT 1 
        `);
        return userapp[0];
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByUsername = async function (username) {
    try {
        const userapp = await UserApp.findOne({ where: { username: username } })
        return userapp;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByBankID = async function (bank_id) {
    try {
        const userapp = await UserApp.findAll({ attributes: ["username", "fullname", "email", "phone_number"], where: { bank_id: bank_id } })
        return userapp;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.selectAll = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
        inner join lembaga b on
            a.bank_id = b.bank_id
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.selectAllAdmin = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
        inner join lembaga b on
            a.bank_id = b.bank_id
        where 
            a.user_type = 1
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.selectAllOperator = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
        inner join lembaga b on
            a.bank_id = b.bank_id
        where 
            a.user_type = 0
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }

    }
}

exports.selectAllNotPaid = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
            inner join lembaga b on a.bank_id = b.bank_id
        where
            b.due_date <= ${df.currentDueDateBilling()}
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.selectAllAdminNotPaid = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
            inner join lembaga b on a.bank_id = b.bank_id
        where
            b.due_date <= ${df.currentDueDateBilling()}
            and a.user_type = 1
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
    }
}

exports.selectAllNotActive = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
            inner join lembaga b on a.bank_id = b.bank_id
        where
            a.status = 0
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }

    }
}

exports.selectAllBlocked = async function () {
    try {
        const [userapp, meta] = await sequelize.query(sql`
        select
            b.bank_id,
            b.bank_name,
            b.due_date,
            a.*
        from
            user_app a
            inner join lembaga b on a.bank_id = b.bank_id
        where
            a.status = 2
    `);
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}


