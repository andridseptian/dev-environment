const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_ticket_db } = require('../../database/sb_ticket')
const sql = require('sql-template-strings')


var df = require('../../utility/dateFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("support_auth")

var sequelize = sb_ticket_db;

var AuthDef = {
    ACTIVE: 'ACTIVE',
    NOTACTIVE: 'NOTACTIVE'
}

class Auth extends Model { }
Auth.init({
    auth_id: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    username: DataTypes.STRING,
    status: DataTypes.STRING,
    session: DataTypes.TEXT
}, { sequelize, modelName: 'sup_auth' });

(async function () {
    logger.info("[Support Auth Initialize]");
    await sequelize.sync({ alter: true });
})();

exports.selectAll = async function () {
    try {
        const userapp = await Auth.findAll()
        return userapp;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.insertAuth = async function (auth_id, username, status, session) {
    try {
        var auth = await Auth.create({
            auth_id: auth_id,
            username: username,
            status: status,
            session: session
        })
        return auth;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.findAuth = async function (auth_id) {
    try {
        var auth = await Auth.findByPk(auth_id)
        return auth;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.deleteAuth = async function (auth_id) {
    try {
        var auth = await Auth.destroy({
            where: {
                auth_id: auth_id
            }
        })
        return auth;
    } catch (error) {
        logger.error(error)
        return {
            rc: 99,
            rm: error.message
        }
    }
}

exports.AuthDef = AuthDef


