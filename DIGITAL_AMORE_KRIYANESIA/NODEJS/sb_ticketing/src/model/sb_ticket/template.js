const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_ticket_db } = require('../../database/sb_ticket')
const sql = require('sql-template-strings')
var df = require('../../utility/dateFormat')
var rf = require('../../utility/responseFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("support_auth")

var sequelize = sb_ticket_db;

class Template extends Model { }
Template.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    creator: DataTypes.STRING,
    status: DataTypes.STRING,
    title: DataTypes.STRING,
    message: DataTypes.TEXT,
}, { sequelize, modelName: 'template' });

(async function () {
    logger.info("[Template Initialize]");
    await sequelize.sync({ alter: true });
})();

exports.create = async function (creator, title, message) {
    try {
        const template = await Template.create({
            creator: creator,
            title: title,
            message: message,
            status: "ACTIVE"
        });
        return template;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAll = async function () {
    try {
        const template = await Template.findAll();
        return template;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findOne = async function (id) {
    try {
        const template = await Template.findOne({ where: { id: id } });
        return template;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (id) {
    try {
        const template = await Template.findOne({ where: { id: id } });
        template.destroy()
        return template;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.edit = async function (id, username, title, message) {
    try {
        const template = await Template.findOne({ where: { id: id } });
        template.creator = username
        template.title = title
        template.message = message
        template.save()
        return template;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}