const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_ticket_db } = require('../../database/sb_ticket')
const sql = require('sql-template-strings')
var df = require('../../utility/dateFormat')
var rf = require('../../utility/responseFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("support_auth")

var sequelize = sb_ticket_db;

class Document extends Model { }
Document.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    creator: DataTypes.STRING,
    status: DataTypes.STRING,
    category: DataTypes.STRING,
    title: DataTypes.STRING,
    text: DataTypes.TEXT,
}, { sequelize, modelName: 'document' });

(async function () {
    logger.info("[Document Initialize]");
    await sequelize.sync({ alter: true });
})();

exports.create = async function (creator, title, text, category) {
    try {
        const docResult = await Document.create({
            creator: creator,
            title: title,
            text: text,
            status: "ACTIVE",
            category: category
        });
        return docResult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAll = async function () {
    try {
        const docResult = await Document.findAll();
        return docResult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getCategory = async function () {
    try {
        const [docResult, meta] = await sequelize.query(`select d.category from "document" d where d.category is not null group by d.category`)
        return docResult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findOne = async function (id) {
    try {
        const docResult = await Document.findOne({ where: { id: id } });
        return docResult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (id) {
    try {
        const docResult = await Document.findOne({ where: { id: id } });
        docResult.destroy()
        return docResult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.edit = async function (id, username, title, text) {
    try {
        const docResult = await Document.findOne({ where: { id: id } });
        docResult.creator = username
        docResult.title = title
        docResult.text = text
        docResult.save()
        return docResult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}