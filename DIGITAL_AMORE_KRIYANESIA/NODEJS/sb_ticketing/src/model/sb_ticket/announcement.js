const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_ticket_db } = require('../../database/sb_ticket')
const sql = require('sql-template-strings')
var df = require('../../utility/dateFormat')
var rf = require('../../utility/responseFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("support_auth")

var sequelize = sb_ticket_db;

class Announcement extends Model { }
Announcement.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    expired: DataTypes.DATE,
    creator: DataTypes.STRING,
    type: DataTypes.STRING,
    status: DataTypes.STRING,
    title: DataTypes.STRING,
    text: DataTypes.TEXT,
}, { sequelize, modelName: 'announcement' });

(async function () {
    logger.info("[Announcement Initialize]");
    await sequelize.sync({ alter: true });
})();

exports.create = async function (expired, creator, type, status, title, text) {
    try {
        var anndata = await Announcement.create({
            expired: expired,
            creator: creator,
            type: type,
            status: status,
            title: title,
            text: text
        })
        return anndata
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAll = async function () {
    try {
        const anndata = await Announcement.findAll();
        return anndata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByStatus = async function (status) {
    try {
        const [anndata, meta] = await sequelize.query(`select * from announcement a where a.status = 'ACTIVE' and expired >= now() or a.status = 'PINNED'`)
        return anndata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByType = async function (type) {
    try {
        const anndata = await Announcement.findAll({ where: { type: type } });
        return anndata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findOne = async function (id) {
    try {
        const anndata = await Announcement.findOne({ where: { id: id } });
        return anndata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (id) {
    try {
        const anndata = await Announcement.findOne({ where: { id: id } });
        anndata.destroy()
        return anndata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}