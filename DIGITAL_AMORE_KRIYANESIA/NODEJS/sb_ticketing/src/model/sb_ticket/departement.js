const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_ticket_db } = require('../../database/sb_ticket')
const sql = require('sql-template-strings')
var df = require('../../utility/dateFormat')
var rf = require('../../utility/responseFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("support_auth")

var sequelize = sb_ticket_db;

class Depertement extends Model { }
Depertement.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    contributor: DataTypes.TEXT,
    status: DataTypes.STRING,

}, { sequelize, modelName: 'departement' });

(async function () {
    logger.info("[Depertement Initialize]");
    await sequelize.sync({ alter: true });
})();

exports.create = async function (name, description, contributor) {
    try {
        const depresult = await Depertement.create({
            name: name,
            description: description,
            contributor: contributor,
            status: "ACTIVE",
        });
        return depresult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAll = async function () {
    try {
        const depresult = await Depertement.findAll();
        return depresult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findByName = async function (name) {
    try {
        const depresult = await Depertement.findAll({ where: { name: name } });
        return depresult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findOne = async function (id) {
    try {
        const depresult = await Depertement.findOne({ where: { id: id } });
        return depresult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (id) {
    try {
        const depresult = await Depertement.findOne({ where: { id: id } });
        depresult.destroy()
        return depresult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.edit = async function (id, name, description, contributor, status) {
    try {
        const depresult = await Depertement.findOne({ where: { id: id } });
        depresult.name = name
        depresult.description = description
        depresult.contributor = JSON.stringify(contributor)
        depresult.status = status
        depresult.save()
        return depresult;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

