const { Sequelize, Model, DataTypes, QueryTypes, Op } = require('sequelize');
const { sb_ticket_db } = require('../../database/sb_ticket')
const sql = require('sql-template-strings')
var df = require('../../utility/dateFormat')
var rf = require('../../utility/responseFormat')

var { log4js } = require('../../utility/logger')
var logger = log4js.getLogger("support_auth")

var sequelize = sb_ticket_db;

class Ticket extends Model { }
Ticket.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    cr_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    mod_time: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        onUpdate: DataTypes.NOW
    },
    number: DataTypes.STRING,
    username: DataTypes.STRING,
    company: DataTypes.INTEGER,
    companyname: DataTypes.STRING,
    status: DataTypes.STRING,
    departement: DataTypes.STRING,
    title: DataTypes.STRING,
    message: DataTypes.TEXT,
    response: DataTypes.TEXT
}, { sequelize, modelName: 'ticket' });

(async function () {
    logger.info("[Ticket Initialize]");
    await sequelize.sync({ alter: true });
})();

exports.create = async function (username, title, message, company, companyname, departement) {
    try {
        const ticketdata = await Ticket.create({
            username: username,
            title: title,
            message: message,
            number: df.getTimeID(),
            company: company,
            companyname: companyname,
            departement: departement,
            status: "NOT_RESPONDED"
        });
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAllTicket = async function () {
    try {
        const ticketdata = await Ticket.findAll({ order: [["id", "DESC"]] })
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAllTicketByStatus = async function (status) {
    try {
        const ticketdata = await Ticket.findAll({ where: { status: status }, order: [["id", "DESC"]] })
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAllTicketByStatusAndUsernameAndCompany = async function (status, username, company) {
    try {
        const ticketdata = await Ticket.findAll({ where: { status: status, username: username, company: company }, order: [["id", "DESC"]] })
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findTicketByUsernameAndCompany = async function (username, company) {
    try {
        const ticketdata = await Ticket.findAll({ where: { username: username, company: company }, order: [["id", "DESC"]] })
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findTicketByNumber = async function (number) {
    try {
        const ticketdata = await Ticket.findOne({ where: { number: number } })
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findTicketByUsernameAndCompanyAndNumber = async function (username, company, number) {
    try {
        const ticketdata = await Ticket.findOne({ where: { username: username, company: company, number: number } })
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.selectAllTicketCounter = async function () {
    try {
        const [ticketdata, raw] = await sequelize.query(sql`
            select
                t.status,
                COUNT (t.status)
            from
                ticket t
            group by
                t.status
        `)
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.selectTicketCounterbyUsernameAndCompany = async function (username, company) {
    try {
        const [ticketdata, raw] = await sequelize.query(sql`
            select
                t.status,
                COUNT (t.status)
            from
                ticket t
            where 
                t.username = ${username}
                and t.company = ${company}
            group by
                t.status
        `)
        return ticketdata;
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}