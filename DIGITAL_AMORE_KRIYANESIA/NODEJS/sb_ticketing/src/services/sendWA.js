const WAsender = "6287720009216"

class WA {
    static convertNumber = async function (number) {
        try {
            if (number.length < 10) {
                return false
            }
            if (number.startsWith("+62")) {
                number = number.substring(1)
            }
            if (number.startsWith("0")) {
                number = "62" + number.substring(1)
            }
            if (!number.startsWith("628")) {
                return false
            }
            return number
        } catch (error) {
            return false;
        }
    }

    static send = async function (destination, message) {
        destination = await this.convertNumber(destination)
        logger.info("Destination WA: " + destination)
        if (destination) {

            var data = JSON.stringify({
                "device_number": WAsender,
                "message": message,
                "to": destination
            });

            var request = require('request');
            var options = {
                'method': 'POST',
                'url': 'https://wapi.srv.co.id:31012/sendMessage',
                'headers': {
                    'Content-Type': 'application/json',
                    'access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjAyODMyOTM2fQ.q_Wg-mKlV8rOPds9bLvkYWQ-5s5WYJGTul35yCjUCTE'
                },
                body: data
            };
            logger.info("sending WA", data)
            request(options, function (error, response) {
                if (error) throw new Error(error);
                logger.info("Result:")
                logger.info(response.body);
            });

        } else {
            logger.info("failed to send message, destinantion false")
        }
    }
}

exports.WA = WA