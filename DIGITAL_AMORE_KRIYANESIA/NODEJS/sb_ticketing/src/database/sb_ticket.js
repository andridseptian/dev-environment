const { log4js } = require('../utility/logger')
const expressSession = require('express-session');
const SessionStore = require('connect-session-sequelize')(expressSession.Store);


var logger = function (params) {
    var log = log4js.getLogger("sb_ticket_db")
    log.info(params)
}

const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');
const sb_ticket_db = new Sequelize('postgres://postgres:4k535r00t@10.81.20.87:5432/sb_ticketing', {
    // logging: logger,
    logging: false,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: false
    }
}) // Example for postgres

sb_ticket_db.define("sessions", {
    sid: {
        type: Sequelize.STRING,
        primaryKey: true,
    },
    authid: Sequelize.STRING,
    expires: Sequelize.DATE,
    data: Sequelize.TEXT,
});

function extendDefaultFields(defaults, session) {
    return {
        data: defaults.data,
        expires: defaults.expires,
        authid: session.auth_id,
    };
}

const sequelizeSessionStore = new SessionStore({
    db: sb_ticket_db,
    table: "sessions",
    extendDefaultFields: extendDefaultFields,
});

exports.sb_ticket_db = sb_ticket_db
exports.sequelizeSessionStore = sequelizeSessionStore