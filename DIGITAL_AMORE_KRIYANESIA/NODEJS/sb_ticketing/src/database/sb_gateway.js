const { log4js } = require('../utility/logger')

var logger = function (params) {
    var log = log4js.getLogger("sb_gateway")
    log.info(params)
}

const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');
exports.sb_gateway_db = new Sequelize('postgres://postgres:4k535r00t@10.81.20.87:5432/sb_gateway', {
    // logging: logger,
    logging: false,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: false
    }
}) // Example for postgres