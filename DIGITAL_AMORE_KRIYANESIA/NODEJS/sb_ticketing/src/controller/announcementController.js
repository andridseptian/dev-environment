var anndb = require('../model/sb_ticket/announcement')
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("announcementController")

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');

exports.createNew = async function (req, res) {
    try {
        logger.info('createNew announcement')

        var expired = req.body.expired
        var type = req.body.type
        var status = req.body.status
        var username = req.body.username
        var title = req.body.title
        var text = req.body.text

        var annres = await anndb.create(expired, username, type, status, title, text)
        return rf.dataWrap(annres)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getAll = async function (req, res) {
    try {
        logger.info('find All announcement')
        var annres = await anndb.findAll()
        return rf.dataWrap(annres)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getByStatus = async function (req, res) {
    try {
        logger.info('find status announcement')
        var status = req.body.status
        var annres = await anndb.findByStatus(status)
        return rf.dataWrap(annres)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getAnnouncementById = async function (req, res) {
    try {
        logger.info('find One announcement')
        var id = req.body.id
        var annres = await anndb.findOne(id)
        return rf.dataWrap(annres)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (req, res) {
    try {
        logger.info('delete announcement')
        var id = req.body.id
        var annres = await anndb.delete(id)
        return rf.dataWrap(annres)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}