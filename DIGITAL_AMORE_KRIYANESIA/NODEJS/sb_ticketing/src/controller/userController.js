var userapp = require('../model/sb_gateway/user_app')
var auth = require('../model/sb_ticket/support_auth')

var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("userController")

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat')
const tripleDes = require('../utility/tripleDes')
var td = new tripleDes('APLIKASISHARINGBANDWIDTHENKRIPSI')

exports.login = async function (req, res) {
    try {
        logger.info('login user')

        var username = td.encrypt3DES(req.body.username);
        var password = td.encrypt3DES(req.body.password);

        var userappdata = await userapp.findByUsernameAndPassword(username, password)

        if (await rf.isNull(userappdata)) {
            req.session.username = req.body.username;
            req.session.fullname = userappdata.fullname;
            req.session.bank_name = userappdata.bank_name;
            req.session.bank_id = userappdata.bank_id;
            req.session.phone_number = userappdata.phone_number;
            req.session.email = userappdata.email;
            req.session.ses_id = await df.getSessionID();
            req.session.user_type = userappdata.user_type;
            var logindata;
            if (userappdata.user_type === 1) {
                if (userappdata.bank_id === "2499") {
                    if (req.body.username === "Angga03") {
                        logindata = {
                            url: "/dashboard/member.html",
                            user_type: 0,
                            bank_id: userappdata.bank_id,
                            session: req.session
                        }
                        userappdata.logindata = logindata
                        return rf.dataWrap(userappdata)
                    } else {
                        logindata = {
                            url: "/dashboard/admin.html",
                            user_type: userappdata.user_type,
                            bank_id: userappdata.bank_id,
                            session: req.session
                        }
                        userappdata.logindata = logindata
                        return rf.dataWrap(userappdata)
                    }

                } else {
                    logindata = {
                        url: "/dashboard/member.html",
                        bank_id: userappdata.bank_id,
                        user_type: userappdata.user_type,
                        session: req.session
                    }
                    userappdata.logindata = logindata
                    return rf.dataWrap(userappdata)
                }
            } else {
                req.session.destroy(function (err) {
                    // cannot access session here
                })
                return rf.responseWrap("99", "Mohon maaf hanya user admin maing-masing BPR yang bisa melakukan login")
            }
        } else {
            return rf.responseWrap("99", "Username and Password did not match")
        }

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.checkSession = async function (req, res) {
    try {
        logger.info('logout user')

        if (req.session.ses_id) {
            return {
                rc: "00",
                rm: req.session
            }
        } else {
            return {
                rc: "05",
                rm: "Session Not Found"
            }
        }
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.logout = async function (req, res) {
    try {
        logger.info('user check session')
        req.session.destroy(function (err) {
            // cannot access session here
            if (err) {
                return rf.errorWrap(logger, err)
            }
        })
        return rf.dataWrap('Logout Success')
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getadminlist = async function (req, res) {
    try {
        logger.info('user get admin list')
        var userappdata = await userapp.findByBankID(2499)

        for (let index = 0; index < userappdata.length; index++) {
            var user = userappdata[index];
            user.username = td.decrypt3DES(user.username)
        }

        return rf.dataWrap(userappdata)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}