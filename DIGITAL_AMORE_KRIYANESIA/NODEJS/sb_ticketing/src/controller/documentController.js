var docDB = require('../model/sb_ticket/document')
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("documentController")

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');

exports.createNew = async function (req, res) {
    try {
        logger.info('createNew document')

        var username = req.body.username
        var title = req.body.title
        var message = req.body.text
        var category = req.body.category

        var docResult = await docDB.create(username, title, message, category)
        return rf.dataWrap(docResult)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getAll = async function (req, res) {
    try {
        logger.info('find All document')
        var docResult = await docDB.findAll()
        return rf.dataWrap(docResult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getCategory = async function (req, res) {
    try {
        logger.info('find Category document')
        var docResult = await docDB.getCategory()

        var categoryList = []
        for (let index = 0; index < docResult.length; index++) {
            const element = docResult[index];
            categoryList.push(element.category)
        }
        var data = {
            category: categoryList
        }
        return rf.dataWrap(data)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getTemplatebyId = async function (req, res) {
    try {
        logger.info('find One document')
        var id = req.body.id
        var docResult = await docDB.findOne(id)
        return rf.dataWrap(docResult)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (req, res) {
    try {
        logger.info('delete document')
        var id = req.body.id
        var docResult = await docDB.delete(id)
        return rf.dataWrap(docResult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.edit = async function (req, res) {
    try {
        logger.info('edit document')
        var id = req.body.id
        var username = req.body.username
        var title = req.body.title
        var message = req.body.text
        var docResult = await docDB.edit(id, username, title, message)
        return rf.dataWrap(docResult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}