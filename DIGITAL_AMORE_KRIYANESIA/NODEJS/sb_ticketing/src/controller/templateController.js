var templateDB = require('../model/sb_ticket/template')
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("templateController")

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');
const { converNumberToWA } = require("../utility/stringFormat");

exports.createNew = async function (req, res) {
    try {
        logger.info('createNew template')

        var username = req.body.username
        var title = req.body.title
        var message = req.body.message

        var templateResult = await templateDB.create(username, title, message)
        return rf.dataWrap(templateResult)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getAll = async function (req, res) {
    try {
        logger.info('find All template')
        var templateResult = await templateDB.findAll()
        return rf.dataWrap(templateResult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getTemplatebyId = async function (req, res) {
    try {
        logger.info('find One template')
        var id = req.body.id
        var templateResult = await templateDB.findOne(id)
        return rf.dataWrap(templateResult)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (req, res) {
    try {
        logger.info('delete template')
        var id = req.body.id
        var templateResult = await templateDB.delete(id)
        return rf.dataWrap(templateResult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.edit = async function (req, res) {
    try {
        logger.info('edit template')
        var id = req.body.id
        var username = req.body.username
        var title = req.body.title
        var message = req.body.message
        var templateResult = await templateDB.edit(id, username, title, message)
        return rf.dataWrap(templateResult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}