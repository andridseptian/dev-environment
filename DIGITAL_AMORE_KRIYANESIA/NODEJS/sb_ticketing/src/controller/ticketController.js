const multer = require("multer");
//untuk menambahkan path
const path = require("path");

var ticket = require('../model/sb_ticket/ticket')
var userapp = require('../model/sb_gateway/user_app')


var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("ticketController")

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');
const { converNumberToWA } = require("../utility/stringFormat");
const tripleDes = require('../utility/tripleDes');
const { WA } = require("../services/sendWA");
var td = new tripleDes('APLIKASISHARINGBANDWIDTHENKRIPSI')

exports.createNew = async function (req, res) {
    try {
        logger.info('createNew ticket')

        var company = req.body.company
        var companyname = req.session.bank_name
        var username = req.body.username
        var title = req.body.title
        var message = req.body.message
        var departement = req.body.departement

        var ticketData = await ticket.create(username, title, message, company, companyname, departement)
        var message = `[NOTIFIKASI - TIKET SB]\n${username} dari ${companyname}, membuat tiket baru dengan nomor ${ticketData.number}, berjudul \"${title}\", pada ${df.getNowDateAndTime()}`
        WA.send("082196740580", message)
        return rf.dataWrap(ticketData)

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findAllTicket = async function (req, res) {
    try {
        var ticketData = await ticket.findAllTicket();
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findUserTicket = async function (req, res) {
    try {
        var username = req.body.username
        var company = req.body.bank_id

        var ticketData = await ticket.findTicketByUsernameAndCompany(username, company);
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findTicketNumber = async function (req, res) {
    try {
        var username = req.body.username
        var company = req.body.bank_id
        var number = req.body.number

        var ticketData = await ticket.findTicketByUsernameAndCompanyAndNumber(username, company, number);
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.findTicketByNumber = async function (req, res) {
    try {
        var number = req.body.number
        var ticketData = await ticket.findTicketByNumber(number);
        ticketData = ticketData.toJSON()
        var username = td.encrypt3DES(ticketData.username);
        var ticketCount = await ticket.findTicketByUsernameAndCompany(ticketData.username, ticketData.company);
        var userData = await userapp.findByUsername(username);
        userData = userData.toJSON()
        logger.info("get user data")
        // var response = {
        //     tiket: ticketData,
        //     user: userData
        // }
        userData.ticketcount = ticketCount.length;
        ticketData.user = userData;
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.responseTicket = async function (req, res) {
    try {
        var username = req.body.username
        var company = req.body.bank_id
        var number = req.body.number
        var responder = req.body.responder
        var response = [new Date(), responder, req.body.response]

        var ticketData = await ticket.findTicketByUsernameAndCompanyAndNumber(username, company, number);

        console.log("dataaaa:");
        console.log(ticketData);

        if (ticketData.response !== null) {
            console.log("tidak null")
            var data = JSON.parse(ticketData.response)
            data.push(response)
            console.log("data: ", data);
            ticketData.response = JSON.stringify(data)
        } else {
            console.log("nulll wehhh");
            ticketData.response = JSON.stringify([response])
        }

        ticketData.save()

        // var message = `[NOTIFIKASI - TIKET SB]\n${username} dari ${companyname}, membuat tiket baru dengan nomor ${ticketData.number}, berjudul \"${title}\", pada ${df.getNowDateAndTime()}`

        // if(responder.startsWith('admin -')){

        // }

        // await sendWAMessage("082196740580", message)

        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.setTicketStatus = async function (req, res) {
    try {
        var number = req.body.number
        var status = req.body.status
        var ticketData = await ticket.findTicketByNumber(number);
        ticketData.status = status
        ticketData.save()
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}
exports.findTicketByStatusForAdmin = async function (req, res) {
    try {
        var status = req.body.status
        var ticketData = await ticket.findAllTicketByStatus(status);
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}
exports.findTicketByStatusForUser = async function (req, res) {
    try {
        var status = req.body.status
        var username = req.body.username
        var company = req.body.bank_id
        var ticketData = await ticket.findAllTicketByStatusAndUsernameAndCompany(status, username, company);
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}
exports.getTicketCounter = async function (req, res) {
    try {
        var ticketData = await ticket.selectAllTicketCounter();
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getTicketCounterByUsername = async function (req, res) {
    try {
        var username = req.body.username
        var company = req.body.bank_id
        var ticketData = await ticket.selectTicketCounterbyUsernameAndCompany(username, company);
        return rf.dataWrap(ticketData)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

async function sendWAMessage(destination, message) {
    destination = await converNumberToWA(destination)
    logger.info("Destination WA: " + destination)
    if (destination) {

        var data = JSON.stringify({
            "device_number": "6287720009216",
            "message": message,
            "to": destination
        });

        var request = require('request');
        var options = {
            'method': 'POST',
            'url': 'https://wapi.srv.co.id:31012/sendMessage',
            'headers': {
                'Content-Type': 'application/json',
                'access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjAyODMyOTM2fQ.q_Wg-mKlV8rOPds9bLvkYWQ-5s5WYJGTul35yCjUCTE'
            },
            body: data
        };
        logger.info("sending WA", data)
        request(options, function (error, response) {
            if (error) throw new Error(error);
            logger.info("Result:")
            logger.info(response.body);
        });

    } else {
        logger.info("failed to send message, destinantion false")
    }
}