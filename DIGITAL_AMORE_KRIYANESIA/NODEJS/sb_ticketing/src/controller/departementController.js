var depdb = require('../model/sb_ticket/departement')
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("departementController")

var rf = require('../utility/responseFormat')
var df = require('../utility/dateFormat');

exports.createNew = async function (req, res) {
    try {
        logger.info('createNew departement')
        var name = req.body.name
        var description = req.body.description
        var contributor = JSON.stringify(req.body.contributor)

        var checkdep = await depdb.findByName(name);
        if (checkdep !== []) {
            var depresult = await depdb.create(name, description, contributor)
            return rf.dataWrap(depresult)
        } else {
            return rf.responseWrap("99", "Departemen sudah ada", checkdep)
        }

    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getAll = async function (req, res) {
    try {
        logger.info('get all departement')
        var depresult = await depdb.findAll()
        return rf.dataWrap(depresult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.getOne = async function (req, res) {
    try {
        logger.info('get departement')
        var id = req.body.id
        var depresult = await depdb.findOne(id)
        return rf.dataWrap(depresult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.edit = async function (req, res) {
    try {
        logger.info('edit departement')

        var id = req.body.id
        var name = req.body.name
        var description = req.body.description
        var contributor = req.body.contributor
        var status = req.body.status

        var depresult = await depdb.edit(id, name, description, contributor, status)
        return rf.dataWrap(depresult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}

exports.delete = async function (req, res) {
    try {
        logger.info('delete departement')
        var id = req.body.id
        var depresult = await depdb.delete(id)
        return rf.dataWrap(depresult)
    } catch (error) {
        return rf.errorWrap(logger, error)
    }
}