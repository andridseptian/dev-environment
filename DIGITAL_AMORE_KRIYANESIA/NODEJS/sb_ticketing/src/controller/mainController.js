var lembaga = require('../model/lembaga')
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("mainController")

var rf = require('../utility/responseFormat')


exports.selectAllLembaga = async function (req, res) {
    try {
        logger.info('select all lembaga')
        res.send(await lembaga.selectAll())
    } catch (error) {
        logger.error(error)
    }
}

// exports.insertUser = async function (req, res) {
//     try {
//         logger.info('insert user')
//         var fullname = req.body.fullname
//         var username = req.body.username
//         var password = req.body.password
//         logger.info('request body: ', req.body)
//         res.send(await user.insert(fullname, username, password))
//     } catch (error) {
//         logger.error(error)
//     }

// }

// exports.activateUser = async function (req, res) {
//     try {
//         logger.info('activate user')
//         logger.info('query: ', req.query)
//         var userid = req.query.userid
//         res.send(await user.activateUser(userid))
//     } catch (error) {
//         logger.error(error)
//     }
// }

// exports.deleteUser = async function (req, res) {
//     logger.info('delete user')
//     logger.info('query: ', req.query)
//     var userid = req.query.userid
//     res.send(await user.deleteUser(userid))
// }