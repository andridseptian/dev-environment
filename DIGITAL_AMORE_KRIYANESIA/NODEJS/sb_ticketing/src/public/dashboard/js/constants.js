export class constants {
    static announcement = {
        status: {
            ACTIVE: "ACTIVE",
            NOTACTIVE: "NOTACTIVE",
            PINNED: "PINNED"
        },
        type: {
            INFO: "INFO",
            UPDATE: "UPDATE",
            URGENT: "URGENT"
        }
    }

    static ticket = {
        status: {
            NOT_RESPONDED: "NOT_RESPONDED",
            PROCESS: "PROCESS",
            DONE: "DONE"
        }
    }
}