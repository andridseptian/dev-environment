import * as sidebar from './sidebar.js'
import * as topbar from './topbar.js'
import * as content from './content.js'
import { internalSession } from './session.js'

(async function () {
    sidebar.show()
    topbar.show()

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    await checkSession();
    console.log(internalSession.data)
    content.show(urlParams)
    //load template script
    $("#sidebarToggle, #sidebarToggleTop").on("click", function (e) { $("body").toggleClass("sidebar-toggled"), $(".sidebar").toggleClass("toggled"), $(".sidebar").hasClass("toggled") && $(".sidebar .collapse").collapse("hide") }), $(window).resize(function () { $(window).width() < 768 && $(".sidebar .collapse").collapse("hide"), $(window).width() < 480 && !$(".sidebar").hasClass("toggled") && ($("body").addClass("sidebar-toggled"), $(".sidebar").addClass("toggled"), $(".sidebar .collapse").collapse("hide")) }), $("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function (e) { if (768 < $(window).width()) { var o = e.originalEvent, l = o.wheelDelta || -o.detail; this.scrollTop += 30 * (l < 0 ? 1 : -1), e.preventDefault() } }), $(document).on("scroll", function () { 100 < $(this).scrollTop() ? $(".scroll-to-top").fadeIn() : $(".scroll-to-top").fadeOut() }), $(document).on("click", "a.scroll-to-top", function (e) { var o = $(this); $("html, body").stop().animate({ scrollTop: $(o.attr("href")).offset().top }, 1e3, "easeInOutExpo"), e.preventDefault() })
})()

async function checkSession() {
    try {
        var result = await $.ajax({
            "url": "/user/post/checksession",
            "method": "POST",
            "timeout": 0
        });

        console.log(result)
        if (result.rc !== "00") {
            window.location.href = "/dashboard/memberLogin.html";
        }

        if (result.rm.bank_id === "2499") {
            if (result.rm.username !== "Angga03") {
                window.location.href = "/dashboard/admin.html";
            }
        }
        internalSession.data = result.rm
        return result;
    } catch (error) {
        console.error(error);
        return error;
    }
}
