import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { unsafeHTML } from '../../vendor/lit-html/directives/unsafe-html.js?module';
import { constants } from 'https://support.perbarindo.org/dashboard/js/constants.js';

export class ann {
    static show = async function () {

        var settings = {
            "url": "/ann/post/getstatus",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "status": "ACTIVE"
            }),
        };

        var result = await $.ajax(settings)
        var data = result.rm

        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Pengumuman</h1>
                </div>
                
                ${data.map((item) => {
                
            var border = "primary"
            if (item.type === constants.announcement.type.UPDATE) border = "warning"
            if (item.type === constants.announcement.type.URGENT) border = "danger"
                
            return html`
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card border-left-${border} shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">${item.title}</h6>
                            </div>
                            <div class="card-body">
                                ${unsafeHTML(item.text)}
                            </div>
                        </div>
                    </div>
                </div>
                `})}
        `, document.getElementById('content-ann'))
    }
}