import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { unsafeHTML } from '../../vendor/lit-html/directives/unsafe-html.js?module';

import { internalSession } from './session.js';
import { converNumberToWA } from '../utility/stringFormat.js';
import { ann } from './ann.js';

export function show(urlParams) {
    if (urlParams.get('ticket')) {
        showTicketMessage(internalSession.data.username, internalSession.data.bank_id, urlParams.get('ticket'))
    } else {
        switch (urlParams.get('f')) {
            case 'unread':
                contentGenerate.showTicketListByStatus("NOT_RESPONDED")
                break;
            case 'process':
                contentGenerate.showTicketListByStatus("PROCESS")
                break;
            case 'done':
                contentGenerate.showTicketListByStatus("DONE")
                break;
            case 'new':
                contentGenerate.showCreateNewTicket()
                break;
            default:
                contentGenerate.showTicketList()
                ann.show()
                break;
        }
    }

}

export class contentGenerate {

    static cardCounting = async function () {

        var settings = {
            "url": "/ticket/post/getallticketcountbyusername",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": JSON.stringify({
                "username": internalSession.data.username,
                "bank_id": internalSession.data.bank_id
            })
        };

        const result = await $.ajax(settings)
        console.log(result)

        var data = result.rm;

        var all = 0;
        var unread = "0";
        var process = "0";
        var done = "0";

        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            if (element.status === "NOT_RESPONDED") {
                unread = element.count;
            }
            if (element.status === "PROCESS") {
                process = element.count;
            }
            if (element.status === "DONE") {
                done = element.count;
            }
            all += parseInt(element.count)
        }

        return html`
        <!-- Content Row -->
        <div class="row">
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=all'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                    Tiket Masuk
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${all}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=unread'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Belum Di Respon
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${unread}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=process'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Sedang Di Proses
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${process}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=done'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Selesai
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${done}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `
    }

    static showCreateNewTicket = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Buat Tiket Baru</h1>
                </div>
                
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Form Daftar Tiket Baru</h6>
                            </div>
                            <div class="card-body">
                
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Judul Tiket</label>
                                    <input id="ticket-title" type="text" class="form-control" id="create-title"
                                        placeholder="Apa topik yang ingin anda tanyakan?">
                                </div>
                
                                <div class="form-group">
                                    <label for="ticket-depart">Departemen Tujuan</label>
                                    <select class="form-control" id="ticket-depart">
                                        <option>INFORMASI</option>
                                        <option>PEMBERITAHUAN</option>
                                        <option>PENTING</option>
                                    </select>
                                    <small><i id="ticket-depart-desc" class="ml-3"></i></small>
                                </div>
                
                                <div id="ticket-message"></div>
                
                                <div class="btn-group mt-2 mb-2 float-right" role="group">
                                    <button id="reset-response" type="button" class="btn btn-danger">Hapus</button>
                                    <button id="ticket-create" type="button" class="btn btn-primary">Buat Tiket</button>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))

        var settings = {
            "url": "/dep/post/getall",
            "method": "POST",
            "timeout": 0,
        };

        $.ajax(settings).done(function (response) {
            var data = response.rm
            render(html`
            ${data.map(function (item) {
                return html`
                <option value="${item.id}" description="${item.description}">${item.name}</option>
                `   
            })}
            `, document.getElementById('ticket-depart'))

            $("#ticket-depart-desc").html("*"+$("#ticket-depart").children("option:selected").attr("description"));
            $("#ticket-depart").on("change", function () {
                $("#ticket-depart-desc").html("*"+$(this).children("option:selected").attr("description"));
                this.options[this.selectedIndex].getAttribute("isred")
            });
        });

        $('#ticket-message').summernote({
            placeholder: 'Jelaskan kendala yang anda alami dan disertakan screenshot jika ada',
            tabsize: 2,
            height: 100,
            maximumImageFileSize: 500 * 1024, // 500 KB
            callbacks: {
                onImageUploadError: function (msg) {
                    console.log(msg + ' (1 MB)');
                },
                onImageUpload: function (files) {
                    console.log("try to send data");
                    // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    sendFile(files[0], "/upload", $(this));
                }
            }
        });

        document.getElementById("reset-response").onclick = async function () {
            $('#ticket-message').summernote('code', "")
        }

        document.getElementById("ticket-create").onclick = async function () {
            var result = await $.ajax({
                "url": "/ticket/post/create",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                },
                "data": JSON.stringify({
                    "username": internalSession.data.username,
                    "title": $("#ticket-title").val(),
                    "message": $('#ticket-message').summernote('code'),
                    "company": internalSession.data.bank_id,
                    "departement": $("#ticket-depart").val()
                }),
            });
            console.log(result)
            if (result.rc === "00") {
                // showTicketMessage(result.rm.username, result.rm.company, result.rm.number)
                window.location = "?ticket=" + result.rm.number;
            }
        }

    }

    static showTicketList = async function () {

        var settings = {
            "url": "/ticket/post/finduserticket",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": JSON.stringify({ "username": internalSession.data.username, "bank_id": internalSession.data.bank_id }),
        };

        var result = await $.ajax(settings);

        var cardcount = await contentGenerate.cardCounting();

        render(html`
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Tiket Anda</h1>
            </div>
            
            ${cardcount}
            
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Daftar Tiket</h1>
            </div>
            
            <style>
                .clickableContent:hover{
                    cursor: pointer;
                    opacity: 0.5;
                }
            </style>
            
            ${result.rm.map(data => {

            var color = "warning"
            var STATUS = html`<i class="text-white bg-warning">BELUM DIRESPON</i>`
            if (data.status === "PROCESS") {
                STATUS = html`<i class="text-white bg-primary">SEDANG DIPROSES</i>`
                color = "primary"
            }
            if (data.status === "DONE") {
                STATUS = html`<i class="text-white bg-success">SUDAH SELESAI</i>`
                color = "success"
            }
            return html`
                        <div class="row">
                            <div class="col mb-4">
                                <!-- Illustrations -->
                                <div class="card border-left-${color} shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                <a>${data.title}</a>
                                            </div>
                                            <div class="mb-0 text-gray-800">
                                                <div class="row">
                                                    <div class="col-sm-2">Tanggal Dibuat</div>
                                                    <div class="col-sm-10">: ${data.cr_time}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2">ID Tiket</div>
                                                    <div class="col-sm-10">: ${data.number}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2">Status</div>
                                                    <div class="col-sm-10">: ${STATUS}</div>
                                                    <!-- ${STATUS} -->
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-auto clickableContent">
                                                <i id="${data.number}" href="?ticket=${data.number}" class="fas fa-arrow-alt-circle-right fa-5x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `})}
                
        `, document.getElementById('content-page'))

        result.rm.forEach(data => {
            document.getElementById(data.number).onclick = function () {
                // showTicketMessage(data.username, data.company, data.number)
                window.location = "?ticket=" + data.number
            }
        });
    }

    static showTicketListByStatus = async function (status) {

        var settings = {
            "url": "/ticket/post/finduserticketbystatus",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": JSON.stringify({ "username": internalSession.data.username, "bank_id": internalSession.data.bank_id, "status": status }),
        };

        var result = await $.ajax(settings);

        var cardcount = await contentGenerate.cardCounting();

        render(html`

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Tiket Anda</h1>
            </div>
            
            ${cardcount}
            
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Daftar Tiket : ${status}</h1>
            </div>
            
            <style>
                .clickableContent:hover{
                    cursor: pointer;
                    opacity: 0.5;
                }
            </style>
            
            ${result.rm.map(data => {

            var color = "warning"
            var STATUS = html`<i class="text-white bg-warning">BELUM DIRESPON</i>`
            if (data.status === "PROCESS") {
                STATUS = html`<i class="text-white bg-primary">SEDANG DIPROSES</i>`
                color = "primary"
            }
            if (data.status === "DONE") {
                STATUS = html`<i class="text-white bg-success">SUDAH SELESAI</i>`
                color = "success"
            }
            return html`
            <div class="row">
                <div class="col mb-4">
                    <!-- Illustrations -->
                    <div class="card border-left-${color} shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                        <a>${data.title}</a>
                                    </div>
                                    <div class="mb-0 text-gray-800">
                                        <div class="row">
                                            <div class="col-sm-2">Tanggal Dibuat</div>
                                            <div class="col-sm-10">: ${data.cr_time}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">ID Tiket</div>
                                            <div class="col-sm-10">: ${data.number}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">Status</div>
                                            <div class="col-sm-10">: ${STATUS}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto clickableContent">
                                    <i id="${data.number}" href="?ticket=${data.number}" class="fas fa-arrow-alt-circle-right fa-5x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `})}
                
        `, document.getElementById('content-page'))

        result.rm.forEach(data => {
            document.getElementById(data.number).onclick = function () {
                showTicketMessage(data.username, data.company, data.number)
            }
        });
    }
}

async function showTicketMessage(username, company, number) {

    var settings = {
        "url": "/ticket/post/findticket",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        },
        "data": JSON.stringify({
            "username": username,
            "bank_id": company,
            "number": number
        }),
    };

    var findticket = await $.ajax(settings);
    var data;
    if (findticket.rc === "00") {
        data = findticket.rm
    } else {
        alert(findticket.rm)
    }

    render(html`
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tiket Anda</h1>
        </div>
        
        <div class="row">
            <div class="col mb-4">
                <!-- Illustrations -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">ID: ${data.number}</h6>
                    </div>
                    <div class="card-body">
        
                        <h1 id="ticket-number" class="text-center">${data.title}</h1> <br><br>
        
                        <div class="row">
                            <div class="col-md-12 mx-auto d-block">
                                <div class="jumbotron">
                                    <div class="row">
                                        <div class="col-sm-2">Waktu Dibuka</div>
                                        <div class="col">: ${data.cr_time.replace("T", " ").replace("Z", " ")}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">Pembuat Tiket</div>
                                        <div class="col">: ${data.username}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">Bank ID</div>
                                        <div class="col">: ${data.company}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">Nama BPR</div>
                                        <div class="col">: ${data.companyname}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">Nomor Tiket</div>
                                        <div class="col">: ${data.number}</div>
                                    </div>
                                    <hr>
                                    ${unsafeHTML(data.message)}
                                </div>
                            </div>
                        </div>
        
                        <div id="response-data"></div>
        
                        <div id="response-message"></div>
                        <div id="button-response" class="btn-group mt-2 mb-2 float-right" role="group">
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    `, document.getElementById("content-page"))

    if (data.response !== null) {
        render(JSON.parse(data.response).map(item => {
            if (item[1] === data.username) {
                return html`
                <div class="row">
                    <div class="col-md-8">
                        <div class="container rounded pt-2 pb-2 mb-4 bg-primary text-white">
                            <small> <i class="fas fa-clock"></i> ${item[0].replace("T", " ").replace("Z", " ").substring(0, 16)}</small>
                            <small> <i class="fas fa-user"></i> ${item[1]}</small>
                            <hr>
                            ${unsafeHTML(item[2])}
                        </div>
                    </div>
                </div>
                `
            } else {
                return html`
                <div class="row justify-content-end">
                    <div class="col-md-8">
                        <div class="container rounded pt-2 pb-2 mb-4 bg-success text-white">
                            <small> <i class="fas fa-clock"></i> ${item[0].replace("T", " ").replace("Z", " ").substring(0, 16)}</small>
                            <small> <i class="fas fa-user"></i> ${item[1]}</small>
                            <hr>
                            ${unsafeHTML(item[2])}
                        </div>
                    </div>
                </div>
                `
            }
        }), document.getElementById("response-data"))
    }

    if (data.status !== "DONE") {
        $('#response-message').summernote({
            placeholder: '...',
            tabsize: 2,
            height: 100,
            maximumImageFileSize: 500 * 1024, // 500 KB
            callbacks: {
                onImageUploadError: function (msg) {
                    console.log(msg + ' (1 MB)');
                },
                onImageUpload: function (files) {
                    console.log("try to send data");
                    // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    sendFile(files[0], "/upload", $(this));
                }
            }
        });
        render(html`
        <button id="reset-response" type="button" class="btn btn-danger">Hapus</button>
        <button id="ticket-response" type="button" class="btn btn-primary">Kirim Respon</button>
        `, document.getElementById("button-response"))

        $('#response-message').summernote({
            placeholder: '...',
            tabsize: 2,
            height: 100,
            maximumImageFileSize: 500 * 1024, // 500 KB
            callbacks: {
                onImageUploadError: function (msg) {
                    console.log(msg + ' (1 MB)');
                },
                onImageUpload: function (files) {
                    console.log("try to send data");
                    // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    sendFile(files[0], "/upload", $(this));
                }
            }
        });

        document.getElementById("reset-response").onclick = async function () {
            $('#response-message').summernote('code', "")
        }

        document.getElementById("ticket-response").onclick = async function () {
            var settings = {
                "url": "/ticket/post/responseticket",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                },
                "data": JSON.stringify({
                    "username": data.username,
                    "bank_id": data.company,
                    "number": data.number,
                    "responder": data.username,
                    "response": $('#response-message').summernote('code')
                }),
            };

            var result = await $.ajax(settings);
            if (result.rc === "00") {
                console.log(result);
                showTicketMessage(data.username, data.company, data.number)
                $('#response-message').summernote('code', "")
            }
        }
    } else {
        render(html`
        <hr>
        <p class="text-center">Tiket ini sudah selesai</p>
        <hr>`, document.getElementById("response-message"))
    }

    // setTimeout(() => {
    //     showTicketMessage(username, company, number)
    // }, 5000);
}

function sendFile(file, url, editor) {
    console.log("sending file");
    var data = new FormData();
    data.append("file", file);
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            // Success!
            var resp = request.responseText;
            editor.summernote('insertImage', resp);
            console.log(resp);
        } else {
            // We reached our target server, but it returned an error
            var resp = request.responseText;
            console.log(resp);
        }
    };
    request.onerror = function (jqXHR, textStatus, errorThrown) {
        // There was a connection error of some sort
        console.log(jqXHR);
    };
    request.send(data);
}