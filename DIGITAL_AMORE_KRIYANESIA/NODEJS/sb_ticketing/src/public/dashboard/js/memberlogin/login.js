(async function () {
    await checkSession()
})()

async function checkSession() {
    try {
        var result = await $.ajax({
            "url": "/user/post/checksession",
            "method": "POST",
            "timeout": 0
        });

        console.log(result)
        if (result.rc === "00") {
            if (result.rm.bank_id === "2499") {
                window.location.href = "/dashboard/admin.html";
            } else {
                window.location.href = "/dashboard/member.html";
            }
        }
        return result;
    } catch (error) {
        console.error(error);
        return error;
    }
}

$('#submit-login').click(async function () {
    try {

        var username = $("#input-username").val()
        var password = $("#input-password").val()
        var data = JSON.stringify({
            username: username,
            password: password
        })

        console.log("request body: " + data)

        const result = await $.ajax({
            "url": "/user/post/login",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": data
        });

        console.log(result)
        if (result.rc === "00") {
            var data = result.rm;
            window.location.href = result.rm.logindata.url;
        } else {
            alert(result.rm)
        }
        return result;
    } catch (error) {
        console.error(error);
        return error;
    }
});