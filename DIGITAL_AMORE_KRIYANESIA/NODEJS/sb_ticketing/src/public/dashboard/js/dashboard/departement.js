import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { unsafeHTML } from '../../vendor/lit-html/directives/unsafe-html.js?module';
import { internalSession } from "./session.js";
import { constants } from "../constants.js";

export class dep {
    static showDepartementList = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">${status}</h1>
                </div>
                
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Daftar Dokumen</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- Confirm Modal-->
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                                <button id="confirm-modal" class="btn btn-primary">Iya</button>
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))
        getTableData("/dep/post/getall")
    }

    static showCreateDepartement = async function () {

        var settings = {
            "url": "/user/post/getadminlist",
            "method": "POST",
            "timeout": 0,
        };

        var result = await $.ajax(settings)


        var adminlist = []
        adminlist = result.rm
        var contributorList = []

        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Buat Document Baru</h1>
                </div>
                
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Form Document</h6>
                            </div>
                            <div class="card-body">
                
                                <div class="form-group">
                                    <label>Judul Dokumen</label>
                                    <input id="dep-title" type="text" class="form-control" placeholder="NAMA DEPARTEMEN">
                                </div>
                
                                <div class="form-group">
                                    <label>Deskripsi Departemen</label>
                                    <textarea class="form-control" id="dep-desc" rows="3"></textarea>
                                </div>
                
                                <div id="add-contrib">
                                    <div class="form-group">
                                        <label>Penanggung Jawab</label>
                
                                        <div id="contrib-list" class="mt-3 mb-3">
                                            <i>*)Belum ada penanggung jawab</i>
                                        </div>
                
                                        <div class="form-inline m-2">
                                            <select class="form-control" id="dep-contrib">
                                                ${adminlist.map(((item, index) => html`
                                                <option value="${index}">${item.fullname}
                                                </option>`))}
                                            </select>
                                            <button id="add-contrib-form" class="btn btn-primary ml-2">+ Tambah Penanggung
                                                Jawab</button>
                                            <button id="reset-contrib-form" class="btn btn-danger ml-2">Atur Ulang</button>
                                        </div>
                                        <i id="contrib-form-alert"></i>
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label for="dep-status">Status Departemen</label>
                                    <select class="form-control" id="dep-status">
                                        <option value="${constants.announcement.status.ACTIVE}">AKTIF</option>
                                        <option value="${constants.announcement.status.NOTACTIVE}">TIDAK AKTIF</option>
                                    </select>
                                </div>
                
                                <div class="btn-group mt-2 mb-2 float-right" role="group">
                                    <button id="dep-create" type="button" class="btn btn-primary">Tambah Departemen</button>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))

        $("#reset-contrib-form").on("click", function () {
            contributorList = []
            render(html`
                <i>*)Belum ada penanggung jawab</i>
            `, document.getElementById('contrib-list'))
        })

        $("#add-contrib-form").on("click", function () {
            var index = $("#dep-contrib").val()

            var check = contributorList.findIndex(obj => obj.username === adminlist[index].username);
            if (check > -1) {
                $("#contrib-form-alert").html("*Penanggung jawab sudah ditambahkan");
                return;
            }


            contributorList.push(adminlist[index])
            $("#contrib-form-alert").html("");

            render(html`
                ${contributorList.map(((item, index) => html`
                <li>
                    <i class="fa fa-user"></i> ${item.username} <i class="fa fa-phone"></i> ${item.phone_number}
                    <i class="fa fa-envelope"></i> ${item.email} <br>
                </li>
                `))}
            `, document.getElementById('contrib-list'))
        });

        $("#dep-create").on("click", function () {
            var settings = {
                "url": "/dep/post/create",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "name": $("#dep-title").val(),
                    "description": $("#dep-desc").val(),
                    "contributor": contributorList
                }),
            };

            $.ajax(settings).done(function (response) {
                if (response.rc === "00") {
                    window.location.href = "/dashboard/admin.html?f=deplist";
                } else {
                    alert(response.rm)
                }
            });
        });
    }

    static showEditDepartment = async function (id) {

        var adminlist = await getadmin()

        var settings = {
            "url": "/dep/post/getone",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "id": id
            }),
        };

        var result = await $.ajax(settings)

        var data = result.rm

        var contributorList = []
        contributorList = JSON.parse(data.contributor)

        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Buat Document Baru</h1>
                </div>
                
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Form Document</h6>
                            </div>
                            <div class="card-body">
                
                                <div class="form-group">
                                    <label>Nama Departemen</label>
                                    <input id="dep-title" type="text" class="form-control" value="${data.name}">
                                </div>
                
                                <div class="form-group">
                                    <label>Deskripsi Departemen</label>
                                    <textarea class="form-control" id="dep-desc" rows="3">${data.description}</textarea>
                                </div>
                
                                <div id="add-contrib">
                                    <div class="form-group">
                                        <label>Penanggung Jawab</label>
                
                                        <div id="contrib-list" class="mt-3 mb-3">
                                            <i>*)Belum ada penanggung jawab</i>
                                        </div>
                
                                        <div class="form-inline m-2">
                                            <select class="form-control" id="dep-contrib">
                                                ${adminlist.map(((item, index) => html`
                                                <option value="${index}">${item.fullname}
                                                </option>`))}
                                            </select>
                                            <button id="add-contrib-form" class="btn btn-primary ml-2">+ Tambah Penanggung
                                                Jawab</button>
                                            <button id="reset-contrib-form" class="btn btn-danger ml-2">Atur Ulang</button>
                                        </div>
                                        <i id="contrib-form-alert"></i>
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label for="dep-status">Status Departemen</label>
                                    <select class="form-control" id="dep-status" value="${data.status}">
                                        <option value="${constants.announcement.status.ACTIVE}">AKTIF</option>
                                        <option value="${constants.announcement.status.NOTACTIVE}">TIDAK AKTIF</option>
                                    </select>
                                </div>
                
                                <div class="btn-group mt-2 mb-2 float-right" role="group">
                                    <button id="dep-edit" type="button" class="btn btn-success">Simpan Perubahan</button>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))

        render(html`
        ${contributorList.map(((item, index) => html`
        <li>
            <i class="fa fa-user"></i> ${item.username} <i class="fa fa-phone"></i> ${item.phone_number}
            <i class="fa fa-envelope"></i> ${item.email} <br>
        </li>
        `))}
        `, document.getElementById('contrib-list'))

        $("#reset-contrib-form").on("click", function () {
            contributorList = []
            render(html`
        <i>*)Belum ada penanggung jawab</i>
        `, document.getElementById('contrib-list'))
        })

        $("#add-contrib-form").on("click", function () {
            var index = $("#dep-contrib").val()

            var check = contributorList.findIndex(obj => obj.username === adminlist[index].username);
            if (check > -1) {
                $("#contrib-form-alert").html("*Penanggung jawab sudah ditambahkan");
                return;
            }

            contributorList.push(adminlist[index])
            $("#contrib-form-alert").html("");

            render(html`
            ${contributorList.map(((item, index) => html`
            <li>
                <i class="fa fa-user"></i> ${item.username} <i class="fa fa-phone"></i> ${item.phone_number}
                <i class="fa fa-envelope"></i> ${item.email} <br>
            </li>
            `))}
            `, document.getElementById('contrib-list'))
        });

        $("#dep-edit").on("click", function () {
            var settings = {
                "url": "/dep/post/edit",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "id": id,
                    "name": $("#dep-title").val(),
                    "description": $("#dep-desc").val(),
                    "contributor": contributorList,
                    "status": $("#dep-status").val(),
                }),
            };

            $.ajax(settings).done(function (response) {
                if (response.rc === "00") {
                    window.location.href = "/dashboard/admin.html?f=deplist";
                } else {
                    alert(response.rm)
                }
            });
        });

    }
}

async function getadmin() {
    var settings = {
        "url": "/user/post/getadminlist",
        "method": "POST",
        "timeout": 0,
    };

    var result = await $.ajax(settings)
    var adminlist = []
    adminlist = result.rm
    return adminlist
}


async function getTableData(url) {
    const result = await $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        }
    });

    if (result.rc === "00") {
        var data = result.rm;
        render(html`
            <thead>
                <tr class="align-middle">
                    <th>Departement ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th style="min-width:300px">Contributor</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                ${data.map(item => html`
                <tr>
                    <td><a class="btn btn-primary" href="?dep=${item.id}">${item.id} | Lihat</a></td>
                    <td>${item.cr_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.mod_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.name}</td>
                    <td>${item.description}</td>
                    <td>${(function () {
                        var contrib = JSON.parse(item.contributor)
                        return html`
                        <ul>
                            ${contrib.map(item => html`
                            <li>
                                <small>
                                    <i class="fa fa-user"></i> ${item.username} <br>
                                    <i class="fa fa-phone"></i> ${item.phone_number} <br>
                                    <i class="fa fa-envelope"></i> ${item.email}
                                </small>
                            </li>
                            `)}
                        </ul>
                        `})()}
            
                    </td>
                    <td>${item.status}</td>
                    <td><a class="${item.id}-delete btn btn-danger" href="#">hapus</a></td>
                </tr>
                `)}
            </tbody>
        `, document.getElementById("dataTable"))

        $('#dataTable').DataTable();

        data.forEach(temp => {
            $(`.${temp.id}-delete`).on("click", async function () {
                callModal("Yakin ingin menghapus Departemen Ini?", "Anda akan menghapus departemen \"" + temp.name + "\"", () => {
                    var settings = {
                        "url": "/dep/post/delete",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "data": JSON.stringify({ "id": temp.id }),
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        window.location.reload();
                    });
                })
            });
        });

    }
}

async function callModal(header, message, callback) {
    $(".modal-title").html(header);
    $(".modal-body").html(message);

    $('#confirmModal').modal()
    $('#confirmModal').modal('toggle')

    $("#confirm-modal").on("click", async function () {
        await callback()
    });
}