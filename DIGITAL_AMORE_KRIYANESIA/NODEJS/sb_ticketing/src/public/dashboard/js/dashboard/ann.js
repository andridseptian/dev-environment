import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { unsafeHTML } from '../../vendor/lit-html/directives/unsafe-html.js?module';
import { internalSession } from "./session.js";
import { constants } from "../constants.js";

export class ann {
    static showCreateAnnouncement = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Buat Pengumuman Baru</h1>
                </div>
                
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Form Pengumuman</h6>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Judul Pengumuman</label>
                                    <input id="ann-title" type="text" class="form-control" placeholder="Apa judul pengumuman ini?">
                                </div>
                                <div class="form-group">
                                    <label for="ann-type">Tipe Pengumuman</label>
                                    <select class="form-control" id="ann-type">
                                        <option value="${constants.announcement.type.INFO}">INFORMASI</option>
                                        <option value="${constants.announcement.type.UPDATE}">PEMBERITAHUAN</option>
                                        <option value="${constants.announcement.type.URGENT}">PENTING</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="ann-status">Status Pengumuman</label>
                                    <select class="form-control" id="ann-status">
                                        <option value="${constants.announcement.status.ACTIVE}">AKTIF</option>
                                        <option value="${constants.announcement.status.NOTACTIVE}">TIDAK AKTIF</option>
                                        <option value="${constants.announcement.status.PINNED}">DITAMBATKAN/SELALU MUNCUL</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Batas Pengumuman</label>
                                    <input id="ann-expired" type="date" class="form-control">
                                </div>
                
                                <div id="ann-text"></div>
                
                                <div class="btn-group mt-2 mb-2 float-right" role="group">
                                    <button id="reset-doc" type="button" class="btn btn-danger">Hapus</button>
                                    <button id="ann-create" type="button" class="btn btn-primary">Buat Pengumuman</button>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))

        $("#ann-status").on("change", async function () {
            console.log("test");
            if ($(this).val() === constants.announcement.status.PINNED) {
                $("#ann-expired").attr("disabled", true);
            } else {
                $("#ann-expired").attr("disabled", false);
            }
        })

        $('#ann-text').summernote({
            placeholder: 'isi dokumen',
            tabsize: 2,
            height: 500,
            maximumImageFileSize: 500 * 1024, // 500 KB
            callbacks: {
                onImageUploadError: function (msg) {
                    console.log(msg + ' (1 MB)');
                },
                onImageUpload: function (files) {
                    console.log("try to send data");
                    // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    sendFile(files[0], "/upload", $(this));
                }
            }
        });

        $("#ann-create").on("click", async function () {




            var data = JSON.stringify({
                username: internalSession.data.username,
                expired: ($("#ann-expired").val() !== "") ? $("#ann-expired").val() : null,
                type: $("#ann-type").val(),
                status: $("#ann-status").val(),
                title: $("#ann-title").val(),
                text: $('#ann-text').summernote("code")
            })

            // alert(data)

            var settings = {
                "url": "/ann/post/create",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": data
            };

            var result = await $.ajax(settings)
            if (result.rc === "00") {
                window.location.href = "/dashboard/admin.html?f=annlist";
            } else {
                alert(JSON.stringify(result))
            }
        });
    }

    static showAnnouncementList = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">${status}</h1>
                </div>
                
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Daftar Pengumuman</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- Confirm Modal-->
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                                <button id="confirm-modal" class="btn btn-primary">Iya</button>
                            </div>
                        </div>
                    </div>
                </div>
                `, document.getElementById('content-page'))
        getTableData("/ann/post/getall")

    }
}

async function getTableData(url) {
    const result = await $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        }
    });

    if (result.rc === "00") {
        var data = result.rm;
        render(html`
            <thead>
                <tr>
                    <th>Announcement ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Expired</th>
                    <th>Creator</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Announcement ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Expired</th>
                    <th>Creator</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
            <tbody>
                ${data.map(item => html`
                <tr>
                    <td><a class="btn btn-primary" href="?doc=${item.id}">${item.id} | Lihat</a></td>
                    <td>${item.cr_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.mod_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${(item.expired ? item.expired.replace("T", " ").replace("Z", " ").substring(0, 16) : "SELALU AKTIF")}</td>
                    <td>${item.creator}</td>
                    <td>${item.title}</td>
                    <td>${item.status}</td>
                    <td><a class="${item.id}-delete btn btn-danger" href="#">hapus</a></td>
                </tr>
                `)}
            </tbody>
        `, document.getElementById("dataTable"))

        $('#dataTable').DataTable();

        data.forEach(temp => {
            $(`.${temp.id}-delete`).on("click", async function () {
                callModal("Yakin ingin menghapus Pengumuman Ini?", "Anda akan menghapus pengumuman dengan judul \"" + temp.title + "\"", () => {
                    var settings = {
                        "url": "/ann/post/delete",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "data": JSON.stringify({ "id": temp.id }),
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        window.location.reload();
                    });
                })
            });
        });

    }
}

async function callModal(header, message, callback) {
    $(".modal-title").html(header);
    $(".modal-body").html(message);

    $('#confirmModal').modal()
    $('#confirmModal').modal('toggle')

    $("#confirm-modal").on("click", async function () {
        await callback()
    });
}