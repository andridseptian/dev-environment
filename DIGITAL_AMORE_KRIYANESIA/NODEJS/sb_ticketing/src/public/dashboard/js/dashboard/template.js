import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { internalSession } from "./session.js";

export class template {
    static showCreateTemplate = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Buat Template Baru</h1>
                </div>
                
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Form Template</h6>
                            </div>
                            <div class="card-body">
                
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Judul Template</label>
                                    <input id="template-title" type="text" class="form-control" id="create-template"
                                        placeholder="Apa judul template ini?">
                                </div>
                
                                <div id="template-message"></div>
                
                                <div class="btn-group mt-2 mb-2 float-right" role="group">
                                    <button id="reset-template" type="button" class="btn btn-danger">Hapus</button>
                                    <button id="template-create" type="button" class="btn btn-primary">Buat Template</button>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))

        $('#template-message').summernote({
            placeholder: 'isi template',
            tabsize: 2,
            height: 100,
            maximumImageFileSize: 500 * 1024, // 500 KB
            callbacks: {
                onImageUploadError: function (msg) {
                    console.log(msg + ' (1 MB)');
                },
                onImageUpload: function (files) {
                    console.log("try to send data");
                    // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    sendFile(files[0], "/upload", $(this));
                }
            }
        });

        $("#template-create").on("click", async function () {
            var settings = {
                "url": "http://117.54.12.174:56/template/post/create",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "username": internalSession.data.username,
                    "title": $("#template-title").val(),
                    "message": $('#template-message').summernote('code')
                }),
            };

            var result = await $.ajax(settings)
            if (result.rc === "00") {
                window.location.href = "/dashboard/admin.html?f=templatelist";
            }
        });
    }

    static showTemplateList = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">${status}</h1>
                </div>
                
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Daftar Template</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- Confirm Modal-->
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                                <button id="confirm-modal" class="btn btn-primary">Iya</button>
                            </div>
                        </div>
                    </div>
                </div>
                `, document.getElementById('content-page'))
        getTableData("/template/post/getall")

    }

    static showEditTemplate = async function (id) {

        var settings = {
            "url": "http://117.54.12.174:56/template/post/getone",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({ "id": id }),
        };

        var result = await $.ajax(settings)

        if (result.rc === "00") {
            render(html`
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Edit Template</h1>
            </div>
            
            <!-- Content Row -->
            <div class="row">
                <div class="col mb-4">
                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Form Template</h6>
                        </div>
                        <div class="card-body">
            
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Judul Template</label>
                                <input id="template-title" type="text" class="form-control" id="create-template"
                                    placeholder="Apa judul template ini?" value="${result.rm.title}">
                            </div>
            
                            <div id="template-message-edit"></div>
            
                            <div class="btn-group mt-2 mb-2 float-right" role="group">
                                <button id="delete-template" type="button" class="btn btn-danger">Hapus</button>
                                <button id="template-edit" type="button" class="btn btn-primary">Edit Template</button>
                            </div>
            
                        </div>
                    </div>
                </div>
            </div>
            `, document.getElementById('content-page'))

            $('#template-message').summernote({
                placeholder: 'isi template',
                tabsize: 2,
                height: 100,
                maximumImageFileSize: 500 * 1024, // 500 KB
                callbacks: {
                    onImageUploadError: function (msg) {
                        console.log(msg + ' (1 MB)');
                    },
                    onImageUpload: function (files) {
                        console.log("try to send data");
                        // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                        sendFile(files[0], "/upload", $(this));
                    }
                }
            });

            $("#template-message-edit").summernote("code", result.rm.message);

            $("#template-edit").on("click", async function () {

                var message = $("#template-message-edit").summernote("code")
                var settings = {
                    "url": "/template/post/edit",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify({
                        "id": id,
                        "username": internalSession.data.username,
                        "title": $("#template-title").val(),
                        "message": message
                    }),
                };

                var result = await $.ajax(settings)

                if (result.rc === "00") {
                    window.location.href = "/dashboard/admin.html?f=templatelist"
                }

            });
        }
    }

    static getTemplateList = async function () {
        const result = await $.ajax({
            "url": "/template/post/getall",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            }
        });
        if (result.rc === "00") return result.rm
        else return false
    }

    static getTemplateID = async function (id) {
        const result = await $.ajax({
            "url": "/template/post/getone",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": JSON.stringify({
                "id": id
            }),
        });
        if (result.rc === "00") return result.rm
        else return false
    }

}

async function getTableData(url) {
    const result = await $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        }
    });

    if (result.rc === "00") {
        var data = result.rm;
        render(html`
            <thead>
                <tr>
                    <th>Template ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Creator</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Template ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Creator</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
            <tbody>
                ${data.map(item => html`
                <tr>
                    <td><a class="btn btn-primary" href="?template=${item.id}">${item.id} | Lihat</a></td>
                    <td>${item.cr_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.mod_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.creator}</td>
                    <td>${item.title}</td>
                    <td>${item.status}</td>
                    <td><a class="${item.id}-delete btn btn-danger" href="#">hapus</a></td>
                </tr>
                `)}
            </tbody>
        `, document.getElementById("dataTable"))

        $('#dataTable').DataTable();

        data.forEach(temp => {
            $(`.${temp.id}-delete`).on("click", async function () {
                callModal("Yakin ingin menghapus Template Ini?", "Anda akan menghapus template dengan judul \"" + temp.title + "\"", () => {
                    var settings = {
                        "url": "/template/post/delete",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "data": JSON.stringify({ "id": temp.id }),
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        window.location.reload();
                    });
                })
            });
        });

    }
}

async function callModal(header, message, callback) {
    $(".modal-title").html(header);
    $(".modal-body").html(message);

    $('#confirmModal').modal()
    $('#confirmModal').modal('toggle')

    $("#confirm-modal").on("click", function () {
        callback()
    });
}