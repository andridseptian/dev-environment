import * as sidebar from './sidebar.js'
import * as topbar from './topbar.js'
import * as content from './content.js'
import { internalSession } from './session.js'

(async function () {
    await checkSession()
    sidebar.show()
    topbar.show()
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    content.show(urlParams)

    !function (s) { "use strict"; s("#sidebarToggle, #sidebarToggleTop").on("click", function (e) { s("body").toggleClass("sidebar-toggled"), s(".sidebar").toggleClass("toggled"), s(".sidebar").hasClass("toggled") && s(".sidebar .collapse").collapse("hide") }), s(window).resize(function () { s(window).width() < 768 && s(".sidebar .collapse").collapse("hide"), s(window).width() < 480 && !s(".sidebar").hasClass("toggled") && (s("body").addClass("sidebar-toggled"), s(".sidebar").addClass("toggled"), s(".sidebar .collapse").collapse("hide")) }), s("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function (e) { if (768 < s(window).width()) { var o = e.originalEvent, l = o.wheelDelta || -o.detail; this.scrollTop += 30 * (l < 0 ? 1 : -1), e.preventDefault() } }), s(document).on("scroll", function () { 100 < s(this).scrollTop() ? s(".scroll-to-top").fadeIn() : s(".scroll-to-top").fadeOut() }), s(document).on("click", "a.scroll-to-top", function (e) { var o = s(this); s("html, body").stop().animate({ scrollTop: s(o.attr("href")).offset().top }, 1e3, "easeInOutExpo"), e.preventDefault() }) }(jQuery);
})()

async function checkSession() {
    try {
        var result = await $.ajax({
            "url": "/user/post/checksession",
            "method": "POST",
            "timeout": 0
        });

        console.log(result)
        if (result.rc !== "00") {
            window.location.href = "/dashboard/memberLogin.html";
        }

        if (result.rm.bank_id !== "2499") {
            window.location.href = "/dashboard/member.html";
        }
        internalSession.data = result.rm
        return result;
    } catch (error) {
        console.error(error);
        return error;
    }
}