import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { unsafeHTML } from '../../vendor/lit-html/directives/unsafe-html.js?module';
import { internalSession } from "./session.js";

export class doc {
    static showCreateDocument = async function () {

        var settings = {
            "url": "/doc/post/getcategory",
            "method": "POST",
            "timeout": 0,
        };

        var result = await $.ajax(settings)
        var categorylist = result.rm.category

        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Buat Document Baru</h1>
                </div>
                
                <!-- Content Row -->
                <div class="row">
                    <div class="col mb-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Form Document</h6>
                            </div>
                            <div class="card-body">
                
                                <div class="form-group">
                                    <label>Judul Dokumen</label>
                                    <input id="doc-title" type="text" class="form-control" placeholder="Apa judul dokumen ini?">
                                </div>
                
                                <div class="form-group">
                                    <label>Kategori</label>
                                    <input id="doc-category" list="browsers" type="text" class="form-control" placeholder="Kategori"
                                        style="text-transform:uppercase">
                                    <small class="ml-3">*Pilih kategori yang sudah ada atau ketikkan kategori baru</small>
                                    <datalist id="browsers">
                                        ${categorylist.map((item => html`<option value="${item}">`))}
                                    </datalist>
                                </div>
                
                                <div id="doc-message"></div>
                
                                <div class="btn-group mt-2 mb-2 float-right" role="group">
                                    <button id="reset-doc" type="button" class="btn btn-danger">Hapus</button>
                                    <button id="doc-create" type="button" class="btn btn-primary">Buat Dokumen</button>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
        `, document.getElementById('content-page'))

        $("#doc-category").on("keyup", function () {
            $(this).val($(this).val().toUpperCase());
        });

        $('#doc-message').summernote({
            placeholder: 'isi dokumen',
            tabsize: 2,
            height: 500,
            maximumImageFileSize: 500 * 1024, // 500 KB
            callbacks: {
                onImageUploadError: function (msg) {
                    console.log(msg + ' (1 MB)');
                },
                onImageUpload: function (files) {
                    console.log("try to send data");
                    // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    sendFile(files[0], "/upload", $(this));
                }
            }
        });

        $("#doc-create").on("click", async function () {
            var settings = {
                "url": "/doc/post/create",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "username": internalSession.data.username,
                    "title": $("#doc-title").val(),
                    "text": $('#doc-message').summernote('code'),
                    "category": $('#doc-category').val(),
                }),
            };

            var result = await $.ajax(settings)
            if (result.rc === "00") {
                window.location.href = "/dashboard/admin.html?f=doclist";
            }
        });
    }

    static showDocumentList = async function () {
        render(html`
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">${status}</h1>
                </div>
                
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Daftar Dokumen</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- Confirm Modal-->
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                                <button id="confirm-modal" class="btn btn-primary">Iya</button>
                            </div>
                        </div>
                    </div>
                </div>
                `, document.getElementById('content-page'))
        getTableData("/doc/post/getall")

    }

    static showEditDocument = async function (id) {

        var settings = {
            "url": "/doc/post/getone",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({ "id": id }),
        };

        var result = await $.ajax(settings)

        if (result.rc === "00") {
            render(html`
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Edit Dokumen</h1>
            </div>
            
            <!-- Content Row -->
            <div class="row">
                <div class="col mb-4">
                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Form Dokumen</h6>
                        </div>
                        <div class="card-body">
            
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Judul Dokumen</label>
                                <input id="doc-title" type="text" class="form-control" placeholder="Apa judul dokumen ini?"
                                    value="${result.rm.title}">
                            </div>
            
                            <div id="doc-message-edit">${unsafeHTML(result.rm.text)}</div>
            
                            <div id="doc-edit-button" class="btn-group mt-2 mb-2 float-right" role="group">
                                <button id="doc-edit" type="button" class="btn btn-primary">Edit Dokumen</button>
                            </div>
            
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Confirm Modal-->
            <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Ready to Leave?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                            <button id="confirm-modal" class="btn btn-primary">Iya</button>
                        </div>
                    </div>
                </div>
            </div>
            `, document.getElementById('content-page'))


            $("#doc-edit").on("click", async function () {
                $('#doc-message-edit').summernote({
                    placeholder: 'isi dokumen',
                    tabsize: 2,
                    height: 500,
                    maximumImageFileSize: 500 * 1024, // 500 KB
                    callbacks: {
                        onImageUploadError: function (msg) {
                            console.log(msg + ' (1 MB)');
                        },
                        onImageUpload: function (files) {
                            console.log("try to send data");
                            // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                            sendFile(files[0], "/upload", $(this));
                        }
                    }
                });

                $("#doc-message-edit").summernote("code", result.rm.text);

                render(html`
                    <button id="doc-edit-delete" type="button" class="btn btn-danger">Hapus</button>
                    <button id="doc-edit-close" type="button" class="btn btn-secondary">Batal</button>
                    <button id="doc-edit-save" type="button" class="btn btn-primary">Simpan</button>
                `, document.getElementById('doc-edit-button'))

                $("#doc-edit-close").on("click", function () {
                    location.reload();
                });
                $("#doc-edit-delete").on("click", function () {
                    $("#doc-message-edit").summernote("code", "");
                });

                $("#doc-edit-save").on("click", async function () {
                    callModal("Konfirmasi Edit",
                        "Anda akan merubah dokumen berjudul \"" + $("#doc-title").val() + "\"?",
                        async () => {
                            var message = $("#doc-message-edit").summernote("code")
                            var settings = {
                                "url": "/doc/post/edit",
                                "method": "POST",
                                "timeout": 0,
                                "headers": {
                                    "Content-Type": "application/json"
                                },
                                "data": JSON.stringify({
                                    "id": id,
                                    "username": internalSession.data.username,
                                    "title": $("#doc-title").val(),
                                    "text": message
                                }),
                            };

                            var result = await $.ajax(settings)

                            if (result.rc === "00") {
                                window.location.href = "/dashboard/admin.html?f=doclist"
                            }
                        }
                    );
                });
            });
        }
    }

    static getDocumentList = async function () {
        const result = await $.ajax({
            "url": "/doc/post/getall",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            }
        });
        if (result.rc === "00") return result.rm
        else return false
    }

    static getDocumentID = async function (id) {
        const result = await $.ajax({
            "url": "/doc/post/getone",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": JSON.stringify({
                "id": id
            }),
        });
        if (result.rc === "00") return result.rm
        else return false
    }

}

async function getTableData(url) {
    const result = await $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        }
    });

    if (result.rc === "00") {
        var data = result.rm;
        render(html`
            <thead>
                <tr>
                    <th>Document ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Creator</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Document ID</th>
                    <th>Create Time</th>
                    <th>Mod Time</th>
                    <th>Creator</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
            <tbody>
                ${data.map(item => html`
                <tr>
                    <td><a class="btn btn-primary" href="?doc=${item.id}">${item.id} | Lihat</a></td>
                    <td>${item.cr_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.mod_time.replace("T", " ").replace("Z", " ").substring(0, 16)}</td>
                    <td>${item.creator}</td>
                    <td>${item.title}</td>
                    <td>${item.status}</td>
                    <td><a class="${item.id}-delete btn btn-danger" href="#">hapus</a></td>
                </tr>
                `)}
            </tbody>
        `, document.getElementById("dataTable"))

        $('#dataTable').DataTable();

        data.forEach(temp => {
            $(`.${temp.id}-delete`).on("click", async function () {
                callModal("Yakin ingin menghapus Dokumen Ini?", "Anda akan menghapus dokumen dengan judul \"" + temp.title + "\"", () => {
                    var settings = {
                        "url": "/doc/post/delete",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "data": JSON.stringify({ "id": temp.id }),
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        window.location.reload();
                    });
                })
            });
        });

    }
}

async function callModal(header, message, callback) {
    $(".modal-title").html(header);
    $(".modal-body").html(message);

    $('#confirmModal').modal()
    $('#confirmModal').modal('toggle')

    $("#confirm-modal").on("click", async function () {
        await callback()
    });
}