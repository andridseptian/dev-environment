import { render, html, } from "../../vendor/lit-html/lit-html.js"
import { unsafeHTML } from '../../vendor/lit-html/directives/unsafe-html.js?module';
import { internalSession } from "./session.js";
import { template } from "./template.js";
import { doc } from "./document.js";
import { ann } from "./ann.js";
import { dep } from "./departement.js";

export function show(urlParams) {
    if (urlParams.get('ticket')) {
        Generate.showTicketByNumber(urlParams.get('ticket'))
    }
    else if (urlParams.get('template')) {
        template.showEditTemplate(urlParams.get('template'))
    }
    else if (urlParams.get('doc')) {
        doc.showEditDocument(urlParams.get('doc'))
    }
    else if (urlParams.get('dep')) {
        dep.showEditDepartment(urlParams.get('dep'))
    }
    else {
        switch (urlParams.get('f')) {
            case 'unread':
                Generate.showNotRespondedTicket()
                break;
            case 'process':
                Generate.showProcessTicket()
                break;
            case 'done':
                Generate.showDoneTicket()
                break;
            case 'newtemplate':
                template.showCreateTemplate()
                break;
            case 'templatelist':
                template.showTemplateList()
                break;
            case 'newdoc':
                doc.showCreateDocument()
                break;
            case 'doclist':
                doc.showDocumentList()
                break;
            case 'newann':
                ann.showCreateAnnouncement()
                break;
            case 'annlist':
                ann.showAnnouncementList()
                break;
            case 'deplist':
                dep.showDepartementList()
                break;
            case 'newdep':
                dep.showCreateDepartement()
                break;
            default:
                Generate.showAllTicket()
                break;
        }
    }
}

export class Generate {
    static cardCounting = async function () {

        var settings = {
            "url": "/ticket/post/getallticketcount",
            "method": "POST",
            "timeout": 0,
        };

        const result = await $.ajax(settings)
        console.log(result)

        var data = result.rm;

        var all = 0;
        var unread = "0";
        var process = "0";
        var done = "0";

        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            if (element.status === "NOT_RESPONDED") {
                unread = element.count;
            }
            if (element.status === "PROCESS") {
                process = element.count;
            }
            if (element.status === "DONE") {
                done = element.count;
            }
            all += parseInt(element.count)
        }

        return html`
        <!-- Content Row -->
        <div class="row">
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=all'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                    Tiket Masuk
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${all}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=unread'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Belum Di Respon
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${unread}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=process'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Sedang Di Proses
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${process}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Pending Requests Card Example -->
            <div onclick="window.location='?f=done'" class="col-xl-3 col-md-6 mb-4" style="cursor: pointer">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Selesai
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">${done}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `
    }

    static showAllTicket = async function () {
        try {
            GenerateTableRender("/ticket/post/findAllTicket")
        } catch (error) {
            alert(error)
        }
    }
    static showNotRespondedTicket = async function () {
        try {
            GenerateTableRender("/ticket/post/findallticketbystatus", "NOT_RESPONDED")
        } catch (error) {
            alert(error)
        }
    }
    static showProcessTicket = async function () {
        try {
            GenerateTableRender("/ticket/post/findallticketbystatus", "PROCESS")
        } catch (error) {
            alert(error)
        }
    }
    static showDoneTicket = async function () {
        try {
            GenerateTableRender("/ticket/post/findallticketbystatus", "DONE")
        } catch (error) {
            alert(error)
        }
    }
    static showTicketByNumber = async function (number) {
        var settings = {
            "url": "/ticket/post/findticketnumber",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({ "number": number }),
        };

        const result = await $.ajax(settings)

        if (result.rc === "00") {

            var data = result.rm

            render(html`
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Tiket ${data.username} dari ${data.companyname}</h1>
            </div>
            
            <div class="row">
                <div class="col mb-4">
                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">ID: ${data.number}</h6>
                        </div>
                        <div class="card-body">
            
                            <h1 id="ticket-number" class="text-center">${data.title}</h1> <br><br>
            
                            <div class="row">
                                <div class="col-md-12 mx-auto d-block">
                                    <div class="jumbotron">
                                        <div class="row">
                                            <div class="col-sm-2">Tiket</div>
                                            <div class="col">: ${data.number}
                                                <table class="ml-4">
                                                    <tbody class="small">
                                                        <tr>
                                                            <td><i class="fas fa-calendar"></i></td>
                                                            <td>${data.cr_time.replace("T", " ").replace("Z", " ").substring(0, 10)}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-clock"></i></td>
                                                            <td>${data.cr_time.replace("T", " ").replace("Z", " ").substring(10,
                16)}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">Oleh</div>
                                            <div class="col">: ${data.user.fullname}
                                                <table class="ml-4">
                                                    <tbody class="small">
                                                        <tr>
                                                            <td><i class="fas fa-user"></i></td>
                                                            <td>${data.username}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-phone"></td>
                                                            <td>${data.user.phone_number}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-envelope"></td>
                                                            <td>${data.user.email}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-book"></td>
                                                            <td>${data.user.ticketcount}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">Dari</div>
                                            <div class="col">: ${data.companyname}
                                                <table class="ml-4">
                                                    <tbody class="small">
                                                        <tr>
                                                            <td><i class="fas fa-building"></i></td>
                                                            <td>${data.company}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <hr>
                                        ${unsafeHTML(data.message)}
                                    </div>
                                </div>
                            </div>
            
                            <div id="response-data"></div>
                            <div id="response-message"></div>
                            <div id="button-template" class="input-group mb-3 float-left" role="group"></div>
                            <div id="button-response" class="btn-group mt-2 mb-2 float-right" role="group"></div>
            
                        </div>
                    </div>
                </div>
            </div>
            `, document.getElementById('content-page'))

            if (data.response !== null) {
                render(JSON.parse(data.response).map(item => {
                    if (item[1] === data.username) {
                        return html`
                        <div class="row">
                            <div class="col-md-8">
                                <div class="container rounded pt-2 pb-2 mb-4 bg-primary text-white">
                                    <small>${item[0].replace("T", " ").replace("Z", " ")}</small>|
                                    <small>${item[1]}</small><br>
                                    <hr>
                                    ${unsafeHTML(item[2])}
                                </div>
                            </div>
                        </div>
                        `
                    } else {
                        return html`
                        <div class="row justify-content-end">
                            <div class="col-md-8">
                                <div class="container rounded pt-2 pb-2 mb-4 bg-success text-white">
                                    <small> <i class="fas fa-clock"></i> ${item[0].replace("T", " ").replace("Z", " ").substring(0, 16)}</small>
                                    <small> <i class="fas fa-user"></i> ${item[1]}</small>
                                    <hr>
                                    ${unsafeHTML(item[2])}
                                </div>
                            </div>
                        </div>
                        `
                    }
                }), document.getElementById("response-data"))
            }


            if (data.status !== "DONE") {
                $('#response-message').summernote({
                    placeholder: '...',
                    tabsize: 2,
                    height: 100,
                    maximumImageFileSize: 500 * 1024, // 500 KB
                    callbacks: {
                        onImageUploadError: function (msg) {
                            console.log(msg + ' (1 MB)');
                        },
                        onImageUpload: function (files) {
                            console.log("try to send data");
                            // url = $(this).data('upload'); //path is defined as data attribute for  textarea
                            sendFile(files[0], "/upload", $(this));
                        }
                    }
                });
                render(html`
                <button id="ticket-setdone" type="button" class="btn btn-success">Tandai Tiket Sudah
                    Selesai</button>
                <button type="button" class="btn btn-danger">Hapus</button>
                <button id="ticket-response" type="button" class="btn btn-primary">Kirim Respon</button>
                `, document.getElementById("button-response"))

                var templateList = await template.getTemplateList()

                render(html`
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputTemplate">Template</label>
                    </div>
                    <select class="custom-select" id="inputTemplate">
                        <option value="0" selected>Kosong...</option>
                        ${templateList.map(temp => html`
                        <option value="${temp.id}">${temp.title}</option>
                        `)}
                    </select>
                `, document.getElementById("button-template"))

                $("#inputTemplate").on("change", async function () {
                    var templateID = $(this).val();
                    if (templateID === "0") $('#response-message').summernote('code', "")
                    var tempdata = await template.getTemplateID(templateID)
                    if (tempdata) {
                        $('#response-message').summernote('code', tempdata.message)
                    }
                });

                document.getElementById("ticket-setdone").onclick = async function () {
                    var settings = {
                        "url": "/ticket/post/setstatus",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json",
                        },
                        "data": JSON.stringify({
                            "number": data.number,
                            "status": "DONE"
                        }),
                    };

                    var result = await $.ajax(settings);
                    if (result.rc === "00") {
                        Generate.showTicketByNumber(number)
                        location.reload();
                    }

                }

                document.getElementById("ticket-response").onclick = async function () {
                    var settings = {
                        "url": "/ticket/post/responseticket",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json",
                        },
                        "data": JSON.stringify({
                            "username": data.username,
                            "bank_id": data.company,
                            "number": data.number,
                            "responder": "admin - " + internalSession.data.username,
                            "response": $('#response-message').summernote('code')
                        }),
                    };

                    var result = await $.ajax(settings);
                    if (result.rc === "00") {
                        var settings = {
                            "url": "/ticket/post/setstatus",
                            "method": "POST",
                            "timeout": 0,
                            "headers": {
                                "Content-Type": "application/json",
                            },
                            "data": JSON.stringify({
                                "number": data.number,
                                "status": "PROCESS"
                            }),
                        };

                        await $.ajax(settings);
                        Generate.showTicketByNumber(number)
                        $('#response-message').summernote('code', "")
                    }
                }

            } else {
                render(html`
                <hr>
                <p class="text-center">Tiket ini sudah selesai, <button id="activate-ticket" class="btn btn-sm btn-primary">Aktifkan
                        kembali tiket</button></p>
                <hr>`, document.getElementById("response-message"))

                document.getElementById("activate-ticket").onclick = async function () {
                    var settings = {
                        "url": "/ticket/post/setstatus",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json",
                        },
                        "data": JSON.stringify({
                            "number": data.number,
                            "status": "PROCESS"
                        }),
                    };

                    var result = await $.ajax(settings);
                    if (result.rc === "00") {
                        // Generate.showTicketByNumber(number)
                        location.reload();
                    }
                }

            }

            // setTimeout(() => {
            //     Generate.showTicketByNumber(number)
            //     // showTicketByNumber(number)
            // }, 5000);
        } else {
            render(html`
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Tiket Anda</h1>
            </div>
            
            <div class="card shadow mb-4 text-center">
                <hr> TIKET TIDAK DITEMUKAN
                <hr>
            </div>
            `, document.getElementById('content-page'))
        }
    }
}

async function GenerateTableRender(url, status) {
    try {
        var cardcount = await Generate.cardCounting();
        render(html``, document.getElementById('content-page'))
        render(html`
            ${cardcount}
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">${status}</h1>
            </div>
            
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">TICKET DATA</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            </thead>
                            <tfoot>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            `, document.getElementById('content-page'))
        getTableData(url, status)
    } catch (error) {
        console.error(error);
        alert(error)
    }
}

async function getTableData(url, status) {
    const result = await $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        },
        "data": JSON.stringify({ status: status })
    });

    if (result.rc === "00") {
        var data = result.rm;

        render(html`
            <thead>
                <tr>
                    <th>Ticket Number</th>
                    <th>create time</th>
                    <th>Username</th>
                    <th>Bank Id</th>
                    <th>Title</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Ticket Number</th>
                    <th>create time</th>
                    <th>Username</th>
                    <th>Bank Id</th>
                    <th>Title</th>
                    <th>Status</th>
                </tr>
            </tfoot>
            <tbody>
                ${data.map(item => html`
                <tr>
                    <td><a class="${item.number}" href="?ticket=${item.number}">${item.number}</td>
                    <td>${item.cr_time.replace("T", " ").replace("Z", " ")}</td>
                    <td>${item.username}</td>
                    <td>${item.company}</td>
                    <td>${item.title}</td>
                    <td>${statusParsing(item.status)}</td>
                </tr>
                `)}
            </tbody>
        `, document.getElementById("dataTable"))

        $('#dataTable').DataTable();

        data.forEach(element => {
            $("." + element.number).on("click", async function () {
                // alert("clicked")
                Generate.showTicketByNumber(element.number)
            })
        });

    }
}

function statusParsing(status) {
    if (status === "NOT_RESPONDED") return html`<i class="text-light bg-warning">BELUM DIRESPON</i>`
    if (status === "PROCESS") return html`<i class="text-light bg-primary">DIPROSES</i>`
    if (status === "DONE") return html`<i class="text-light bg-success">SELESAI</i>`
    return html`<i class="bg-info">${status}</i>`
}