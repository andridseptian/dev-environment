var annController = require('../controller/announcementController')
var express = require('express')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("announcementRoutes")

exports.init = async function (app) {
    try {
        logger.info("asd=>document")
        app.use('/ann', router)
        router.post('/post/:service', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)
                var response;
                switch (req.params.service) {
                    case 'create': response = await annController.createNew(req, res); break;
                    case 'getall': response = await annController.getAll(req, res); break;
                    case 'getstatus': response = await annController.getByStatus(req, res); break;
                    case 'getone': response = await annController.getAnnouncementById(req, res); break;
                    case 'delete': response = await annController.delete(req, res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }

                if (response !== "" || response !== undefined) {
                    res.send(response)
                } else {
                    res.send(JSON.stringify({
                        rc: "99",
                        rm: "RESPONSE NOT FOUND"
                    }))
                }

                logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })

    } catch (error) {
        logger.error(error)
    }
}