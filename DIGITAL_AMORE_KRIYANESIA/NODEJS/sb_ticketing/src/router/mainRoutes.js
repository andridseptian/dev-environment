var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("mainRoutes")

var userRouter = require('./userRoutes')
var ticketRouter = require('./ticketRoutes')
var templateRouter = require('./templateRoutes')
var documentRouter = require('./documentRoutes')
var annRouter = require('./announcementRoutes')
var depRouter = require('./departementRoutes')

exports.init = async function (app) {
    try {
        ticketRouter.init(app)
        userRouter.init(app)
        templateRouter.init(app)
        documentRouter.init(app)
        annRouter.init(app)
        depRouter.init(app)
    } catch (error) {
        logger.error(error)
    }
}