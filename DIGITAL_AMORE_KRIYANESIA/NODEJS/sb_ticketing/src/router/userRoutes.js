var userController = require('../controller/userController')
var express = require('express')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("userRoutes")

exports.init = async function (app) {
    try {
        logger.info("asd=>user")
        app.use('/user', router)
        router.post('/post/:service', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)

                var response;

                switch (req.params.service) {
                    case 'login': response = await userController.login(req, res); break;
                    case 'logout': response = await userController.logout(req, res); break;
                    case 'checksession': response = await userController.checkSession(req, res); break;
                    case 'getadminlist': response = await userController.getadminlist(req, res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }

                if (response !== "" || response !== undefined) {
                    res.send(response)
                } else {
                    res.send(JSON.stringify({
                        rc: "99",
                        rm: "RESPONSE NOT FOUND"
                    }))
                }

                logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })

    } catch (error) {
        logger.error(error)
    }
}