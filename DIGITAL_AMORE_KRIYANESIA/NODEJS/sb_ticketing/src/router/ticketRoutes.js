const multer = require("multer");
//untuk menambahkan path
const path = require("path");
const fs = require("fs");

var ticketController = require('../controller/ticketController')
var express = require('express')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("ticketRoutes")


// menentukan lokasi pengunggahan
const diskStorage = multer.diskStorage({
    destination: async function (req, file, cb) {
        await fs.promises.mkdir(path.join(__dirname, `../public/uploads/${req.session.username}`), { recursive: true })
        cb(null, path.join(__dirname, `../public/uploads/${req.session.username}`));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            file.fieldname + "-" + Date.now() + path.extname(file.originalname)
        );
    },
});

exports.init = async function (app) {
    try {
        logger.info("asd=>ticket")
        app.use('/ticket', router)
        router.post('/post/:service', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)

                var response;

                switch (req.params.service) {
                    case 'create': response = await ticketController.createNew(req, res); break;
                    case 'finduserticket': response = await ticketController.findUserTicket(req, res); break;
                    case 'responseticket': response = await ticketController.responseTicket(req, res); break;
                    case 'findticket': response = await ticketController.findTicketNumber(req, res); break;
                    case 'findticketnumber': response = await ticketController.findTicketByNumber(req, res); break;
                    case 'findAllTicket': response = await ticketController.findAllTicket(req, res); break;
                    case 'setstatus': response = await ticketController.setTicketStatus(req, res); break;
                    case 'findallticketbystatus': response = await ticketController.findTicketByStatusForAdmin(req, res); break;
                    case 'finduserticketbystatus': response = await ticketController.findTicketByStatusForUser(req, res); break;
                    case 'setstatus': response = await ticketController.setTicketStatus(req, res); break;
                    case 'getallticketcount': response = await ticketController.getTicketCounter(req, res); break;
                    case 'getallticketcountbyusername': response = await ticketController.getTicketCounterByUsername(req, res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }

                if (response !== "" || response !== undefined) {
                    res.send(response)
                } else {
                    res.send(JSON.stringify({
                        rc: "99",
                        rm: "RESPONSE NOT FOUND"
                    }))
                }

                logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })

        app.post('/upload', multer({ storage: diskStorage }).single("file"),
            (req, res) => {
                const file = req.file.path;
                console.log(req.file);
                console.log(file);
                if (!file) {
                    res.status(400).send({
                        status: false,
                        data: "No File is selected.",
                    });
                }
                res.send(`/uploads/${req.session.username}/${req.file.filename}`);
            }
        );

    } catch (error) {
        logger.error(error)
    }
}