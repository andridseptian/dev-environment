var departementController = require('../controller/departementController')
var express = require('express')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("depRoutes")

exports.init = async function (app) {
    try {
        logger.info("asd=>departement")
        app.use('/dep', router)
        router.post('/post/:service', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)

                var response;

                switch (req.params.service) {
                    case 'create': response = await departementController.createNew(req, res); break;
                    case 'getall': response = await departementController.getAll(req, res); break;
                    case 'getone': response = await departementController.getOne(req, res); break;
                    case 'delete': response = await departementController.delete(req, res); break;
                    case 'edit': response = await departementController.edit(req, res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }

                if (response !== "" || response !== undefined) {
                    res.send(response)
                } else {
                    res.send(JSON.stringify({
                        rc: "99",
                        rm: "RESPONSE NOT FOUND"
                    }))
                }

                logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })

    } catch (error) {
        logger.error(error)
    }
}