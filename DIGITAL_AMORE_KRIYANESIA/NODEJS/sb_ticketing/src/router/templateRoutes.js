var templateController = require('../controller/templateController')
var express = require('express')
var router = express.Router()
var { log4js } = require('../utility/logger')
var logger = log4js.getLogger("templateRoutes")

exports.init = async function (app) {
    try {
        logger.info("asd=>template")
        app.use('/template', router)
        router.post('/post/:service', async function (req, res, next) {
            try {
                logger.info('request path: ', req.path)
                logger.info('request body: ', req.body)

                var response;

                switch (req.params.service) {
                    case 'create': response = await templateController.createNew(req, res); break;
                    case 'getall': response = await templateController.getAll(req, res); break;
                    case 'getone': response = await templateController.getTemplatebyId(req, res); break;
                    case 'delete': response = await templateController.delete(req, res); break;
                    case 'edit': response = await templateController.edit(req, res); break;
                    default: res.send(JSON.stringify({
                        rc: "99",
                        rm: "Path Not Found"
                    }));
                }

                if (response !== "" || response !== undefined) {
                    res.send(response)
                } else {
                    res.send(JSON.stringify({
                        rc: "99",
                        rm: "RESPONSE NOT FOUND"
                    }))
                }

                logger.info('response : ', JSON.stringify(response).substring(0, 1000) + ' ...')
            } catch (error) {
                logger.error(error)
                res.send(JSON.stringify({
                    rc: "99",
                    rm: "INTERNAL SERVER ERROR"
                }))
            }
        })

    } catch (error) {
        logger.error(error)
    }
}