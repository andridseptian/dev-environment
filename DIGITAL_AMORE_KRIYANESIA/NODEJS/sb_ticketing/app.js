var express = require('express')
var app = express()
// var port = 16010
var port = 3000
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var routes = require('./src/router/mainRoutes')
var { log4js } = require('./src/utility/logger')
var logger = log4js.getLogger("express")
var sb_ticket = require('./src/database/sb_ticket')
const multer = require("multer")
var fs = require('fs')
var https = require('https')
var upload = multer();

// https.createServer({
//   key: fs.readFileSync('private.key'),
//   cert: fs.readFileSync('certificate.crt')
// }, app)

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json({ limit: '50mb' }))
// use log4j for logging request
// app.use(log4js.connectLogger(logger, { level: 'info' }));

// sessions init using cookie parser and Express session
app.use(cookieParser());
app.use(expressSession({
  secret: 'secretkeepsecret',
  store: sb_ticket.sequelizeSessionStore,
  resave: false,
  saveUninitialized: false,
}));

// initials static/public folder location
app.use('/', express.static('src/public'))

app.get('/', (req, res) => {
  res.redirect('/dashboard/memberLogin.html')
})

app.get('/support', (req, res) => {
  res.redirect('http://103.28.148.203:61006/support')
})

app.get('/download', (req, res) => {
  res.redirect('http://103.28.148.203:61006/')
})

// initials path/routes
routes.init(app)

// Statring services
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

// https.createServer({
//   key: fs.readFileSync('private.key'),
//   cert: fs.readFileSync('certificate.crt')
// }, app)
//   .listen(port, function () {
//     console.log('app listening on port 3000! Go to https://localhost:' + port);
//   })