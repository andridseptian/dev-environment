const express = require('express')
const cors = require('cors')
const app = express()
const port = 4040

app.use(cors());

app.use('/', express.static('src/public'))


app.get('/', (req, res) => {
    // res.send('Hello World!')
    res.redirect('/support/dashboard/')
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})