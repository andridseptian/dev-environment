import { html, render } from 'https://unpkg.com/lit-html?module';

import * as modal from '../modal.js'
import * as util from '../utility.js'

var headerTitle = "LAPORAN BPR AKTIF BULANAN"

export function show() {
    render(html`
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">${headerTitle}</h1>
    </div>
    
    <div>
        <h3>Jumlah BPR Aktif login selama 10 bulan kebelakang</h3>
    </div>
    
    <div style="width: 100%">
        <div class="row justify-content-center">
            <div id="loading-table" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div class="container ml-5 mr-5 mt-5 table-responsive">
            <canvas id="chart-container"></canvas>
        </div>
    </div>
    `, document.getElementById('main-view'))

    getChartData()
}


async function getChartData() {

    document.getElementById("loading-table").style.display = "block"

    try {
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://103.28.148.203:61006/lembaga/get/active/month",
            "method": "GET"
        };

        $.ajax(settings).done(function (response) {
            var resBody = response

            var chartlabels = [];
            var chartdata = [];
            resBody.forEach((element, index) => {
                chartlabels.push(element.month_name)
                chartdata.push(element.bprcount)
                if (index == resBody.length - 1) {
                    document.getElementById("loading-table").style.display = "none"
                    var ctx = document.getElementById('chart-container').getContext('2d');
                    var chart = new Chart(ctx, {
                        // The type of chart we want to create
                        type: 'line',

                        // The data for our dataset
                        data: {
                            labels: chartlabels,
                            datasets: [{
                                label: 'BPR Active Month',
                                backgroundColor: 'rgba(52, 152, 219,1.0)',
                                // borderColor: 'rgb(255, 99, 132)',
                                lineTension: 0,
                                data: chartdata
                            }]
                        },

                        // Configuration options go here
                        options: {
                            animation: {
                                duration: 1,
                                onComplete: function () {
                                    var chartInstance = this.chart,
                                        ctx = chartInstance.ctx;
                                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'bottom';

                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = dataset.data[index];
                                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                        });
                                    });
                                }
                            }
                        }
                    });
                }
            });
        });
    } catch (error) {
        alert(error);
    }
}