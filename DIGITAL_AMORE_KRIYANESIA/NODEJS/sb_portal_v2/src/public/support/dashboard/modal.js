import { html, render } from 'https://unpkg.com/lit-html?module';


export function modalInitialize() {
    render(html`
    <!-- Modal -->
    <div class="modal fade" id="modaldefault" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title-label">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="modal-body" class="modal-body">
    
                </div>
                <div id="modal-footer" class="modal-footer">
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                    <!-- <button type="button" class="btn btn-primary">Understood</button> -->
                </div>
            </div>
        </div>
    </div>
    `, document.getElementById("modal-container"))
}

export function showSimpleModal(modalbody) {
    render(html`Informasi`, document.getElementById('modal-title-label'))
    render(html`${modalbody}`, document.getElementById('modal-body'))
    render(html``, document.getElementById('modal-footer'))

    $('#modaldefault').modal('show')
}

export function showModal(modalTitle, modalBody, modalFooter) {
    render(html`${modalTitle}`, document.getElementById('modal-title-label'))
    render(html`${modalBody}`, document.getElementById('modal-body'))
    render(html`${modalFooter}`, document.getElementById('modal-footer'))
    $('#modaldefault').modal('show')
}

export function hide() {
    $('#modaldefault').modal('hide')
}