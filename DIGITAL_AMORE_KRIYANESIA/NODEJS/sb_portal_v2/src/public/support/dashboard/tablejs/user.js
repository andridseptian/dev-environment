import { html, render } from 'https://unpkg.com/lit-html?module';

import * as modal from '../modal.js'
import * as util from '../utility.js'

var tableData;
var jsonTableData;
var tempLastPath;
var headerTitle = "USER";
var headerFilter = "Data User: "
var tableName = "#table-user"
var filerDatas = [
    {
        tittle: "Semua User Lembaga",
        value: util.config.supporthost + "users/get/all"
    },
    {
        tittle: "Semua Admin Lembaga",
        value: util.config.supporthost + "users/get/all/admin"
    },
    {
        tittle: "Semua User Lembaga Belum Bayar",
        value: util.config.supporthost + "users/get/all/notpaid"
    },
    {
        tittle: "Semua Admin Lembaga Belum Bayar",
        value: util.config.supporthost + "users/get/admin/notpaid"
    },
    {
        tittle: "Semua User Belum Aktif",
        value: util.config.supporthost + "users/get/notactive"
    },
    {
        tittle: "Semua User Terblokir",
        value: util.config.supporthost + "users/get/blocked"
    },
]
var tableColumns = [
    { "data": "Edit", "title": "Edit", "width": "50px", "defaultContent": "<a href=\"#\" class=\"edit-data\">Edit</a>" },
    { "data": "user_id", "title": "User ID", "width": "100px", "defaultContent": "-" },
    { "data": "fullname", "title": "Full Name", "width": "200px", "defaultContent": "-" },
    { "data": "username.dec", "title": "Username", "width": "200px", "defaultContent": "-" },
    { "data": "username.enc", "title": "Username (encrypted)", "width": "200px", "defaultContent": "-" },
    { "data": "password.enc", "title": "Password", "width": "200px", "defaultContent": "-" },
    { "data": "user_type", "title": "Privilege", "width": "100px", "defaultContent": "-" },
    { "data": "nik.dec", "title": "NIK", "width": "150px", "defaultContent": "-" },
    { "data": "email", "title": "Email", "width": "200px", "defaultContent": "-" },
    { "data": "phone_number", "title": "Phone Number", "width": "150px", "defaultContent": "-" },
    { "data": "expired_password", "title": "Expired Date", "width": "200px", "defaultContent": "-" },
    { "data": "status", "title": "Status", "width": "100px", "defaultContent": "-" },
    { "data": "status_login", "title": "Login Status", "width": "100px", "defaultContent": "-" },
    { "data": "terminal_login", "title": "Terminal Login", "width": "150px", "defaultContent": "-" },
    { "data": "fail_login", "title": "Fail Login", "width": "100px", "defaultContent": "-" },
    { "data": "invoice", "title": "Invoice", "width": "100px", "defaultContent": "-" },
    { "data": "bank_id", "title": "Bank ID", "width": "100px", "defaultContent": "-" },
    { "data": "bank_name", "title": "Bank Name", "width": "200px", "defaultContent": "-" },
    { "data": "due_date", "title": "Due Date", "width": "200px", "defaultContent": "-" },
]

export async function show() {
    await render(html``, document.getElementById('main-view'))
    render(html`
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">${headerTitle}</h1>
    </div>
    
    <div class="row">
        <div class="col-sm-3">
            <h4>${headerFilter}</h4>
        </div>
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control" id="filter-data">
                </select>
            </div>
        </div>
    </div>
    <div class="mb-5">
        <div class="row justify-content-center">
            <div id="loading-table" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div class="table-responsive">
        </div>
        <table id="${tableName.replace('#', '')}" class="table table-striped table-bordered table-hover"
            style="width: 100%">
        </table>
    </div>
    `, document.getElementById('main-view'))

    var loadingTable = document.getElementById("loading-table");
    loadingTable.style.display = "none"

    generateFilter()
    generateTable(filerDatas[0].value)
    tempLastPath = filerDatas[0].value

}

function generateFilter() {
    render(filerDatas.map((data) => html`
        <option value="${data.value}">${data.tittle}</option>
    `), document.getElementById('filter-data'))

    document.getElementById('filter-data').addEventListener("change", function () {
        var selected = document.getElementById('filter-data').value;
        generateTable(selected)
        tempLastPath = selected
    });
}

function generateTable(tableUrl) {
    console.log(tableUrl)
    if ($.fn.DataTable.isDataTable(tableName)) {
        tableData.clear().rows.add([]).draw();
    }
    var loadingTable = document.getElementById("loading-table");
    loadingTable.style.display = "block"

    const settings = {
        "async": true,
        "crossDomain": true,
        "url": tableUrl,
        "method": "GET",
        "error": function (xhr, ajaxOptions, thrownError) {
            alert("Failed to Get Response");
            loadingTable.style.display = "none"
        }
    };

    $.ajax(settings).done(function (response) {
        // console.log(response);
        jsonTableData = response
        jsonTableData.forEach((element, index) => {
            // do edit table
            element.due_date = util.fullDateToDate(element.due_date)
            element.cr_time = util.fullDateToDateAndTime(element.cr_time)
            element.mod_time = util.fullDateToDateAndTime(element.mod_time)
            element.last_login_time = util.fullDateToDateAndTime(element.last_login_time)

            // when edit table finish
            if (jsonTableData.length === index + 1) {

                // create footer
                var footer = $("<tfoot></tfoot>").appendTo(tableName);
                var footertr = $("<tr></tr>").appendTo(footer);
                for (var i = 0; i < tableColumns.length; i++) {
                    $('<td><input type="text" placeholder="Search ' + tableColumns[i].data + '" /></td>').appendTo(footertr);
                }

                // initialize datatables
                if (!$.fn.DataTable.isDataTable(tableName)) {

                    tableData = $(tableName).DataTable({
                        scrollX: true,
                        autoWidth: true,
                        paging: true,
                        fixedHeader: {
                            header: true,
                            footer: false
                        },
                        data: jsonTableData,
                        columns: tableColumns,
                        // dom: '<"top"<"left-col"B><"center-col"l><"right-col"f>>rtlip',
                        dom: '<"top"<"left-col"B><"right-col"f>>rtlip',
                        buttons: [
                            {
                                text: '<i class="fa fa-print"></i>',
                                extend: 'print',
                                titleAttr: 'Print'
                            },
                            {
                                text: '<i class="fa fa-file-excel-o"></i>',
                                extend: 'excelHtml5',
                                titleAttr: 'Download to Excel',
                                title: function () {
                                    return `SB User - ${$("#filter-data option:selected").text()} - ${util.fullDateToDate(new Date().toISOString())}`
                                },
                                exportOptions: {
                                    columns: [1, 2, 3, 7, 8, 9, 10, 16, 17, 18]
                                }
                            },
                            {
                                text: '<i class="fa fa-refresh"></i>',
                                titleAttr: 'Refresh Table',
                                action: function (e, dt, node, config) {
                                    generateTable(tempLastPath)
                                }
                            }
                        ],
                        initComplete: function (settings, json) {
                            feather.replace()

                            // apply column search
                            this.api().columns().every(function () {
                                var that = this;

                                $('input', this.footer()).on('keyup change clear', function () {
                                    if (that.search() !== this.value) {
                                        that
                                            .search(this.value)
                                            .draw();
                                    }
                                });
                            });
                        }
                    });

                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

                    setEditTableView(tableData)


                } else {
                    tableData.clear().rows.add(jsonTableData).draw();
                }

                loadingTable.style.display = "none"
            }
        });
    });
}

function setEditTableView(table) {
    $(tableName + ' tbody').on('click', '.edit-data', function () {
        var data = table.row($(this).parents('tr')).data();
        // alert(JSON.stringify(data))

        var columns = [
            ["User ID", "number", data.user_id, "userid", true],
            ["Username", "text", data.username.dec, "username", false,],
            ["Password", "text", data.password.dec, "password", false],
            ["Nama Lengkap", "text", data.fullname, "full_name", false],
            ["NIK", "text", data.nik.dec, "nik", false],
            ["Email", "text", data.email, "email", false],
            ["No Telp", "text", data.phone_number, "no_hp", false],
            ["Kadaluarsa Password", "text", util.fullDateToDate(data.expired_password), "expired_date", false],
            ["Status", "number", data.status, "status", false],
            ["Tipe User", "number", data.user_type, "user_type", false],
            ["Status Login", "number", data.status_login, "status_login", false],
            ["Gagal Login", "number", data.fail_login, "fail_login", false],
        ]

        modal.showModal("Edit User Data", html`
        <form>
            ${columns.map((col) => html`
            <div class="form-group row">
                <label class="col-sm-3 col-form-label col-form-label-sm">${col[0]}</label>
                <div class="col-sm-9">
                    <input id=${col[3]} type="${col[1]}" class="form-control form-control-sm" ?disabled="${col[4]}"
                        value="${col[2]}">
                </div>
            </div>
            `)}
        </form>
        `, html`
            <button id="updateData" type="button" class="btn btn-primary">Update</button>
        `)

        document.getElementById("updateData").onclick = function () {

            var updatedColums = {}
            columns.forEach(element => {
                updatedColums[element[3]] = document.getElementById(element[3]).value
            });

            modal.showModal("Konfirmasi Perubahan", html`
            ${columns.map((item) => html`
            <div class="row">
                <div class="col-sm-4"><b>${item[0]}</b></div>
                <div class="col-sm-8">${document.getElementById(item[3]).value}</div>
            </div>
            `)}
            <br>
            Apakah anda yakin melakukan perubahan ini?
            `, html`
            <button id="confirmYes" type="button" class="btn btn-primary">Ya</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
            `)

            document.getElementById("confirmYes").onclick = function () {
                // alert(JSON.stringify(updatedColums))
                const data = JSON.stringify(updatedColums);

                const xhr = new XMLHttpRequest();

                xhr.addEventListener("readystatechange", function () {
                    if (this.readyState === this.DONE) {
                        console.log(this.responseText);
                        // alert(this.responseText)
                        modal.hide()
                        generateTable(tempLastPath)
                    }
                });
                xhr.open("POST", util.config.supporthost + "users/insert");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.send(data);
            }
        }
    });
}