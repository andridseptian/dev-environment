import { html, render } from 'https://unpkg.com/lit-html?module';

import * as modal from '../modal.js'
import * as util from '../utility.js'

var tableData;
var jsonTableData;
var tempLastPath;

// configurable value
var headerTitle = "SESSIONS";
var headerFilter = "Data Sessions: "
var tableName = "#table-sessions"
var tableColumns = [
    { "data": "session_id", "title": "Sessions ID", "width": "100px" },
    { "data": "info", "title": "Info", "width": "100px" },
    { "data": "req_time", "title": "Req Time", "width": "100px" },
    { "data": "res_time", "title": "Res Time", "width": "100px" },
    { "data": "path", "title": "Path", "width": "200px" },
    { "data": "rc", "title": "response code", "width": "100px" },
    { "data": "rm", "title": "response message", "width": "400px" },
    { "data": "req_body", "title": "req body", "width": "800px" },
    { "data": "res_body", "title": "res body", "width": "800px" },
    { "data": "exception", "title": "exception", "width": "200px" }
]
var filerDatas = [
    {
        tittle: "Sessions hari ini",
        value: util.config.supporthost + "sessions/get/today"
    },
    {
        tittle: "Failed hari ini",
        value: util.config.supporthost + "sessions/get/today/failed"
    },
    {
        tittle: "Exception hari ini",
        value: util.config.supporthost + "sessions/get/today/exception"
    },

]

export async function show() {
    await render(html``, document.getElementById('main-view'))
    render(html`
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">${headerTitle}</h1>
    </div>
    
    <div class="row">
        <div class="col-sm-3">
            <h4>${headerFilter}</h4>
        </div>
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control" id="filter-data">
                </select>
            </div>
        </div>
    </div>
    
    <div class="mb-5" style="width: 100%">
        <div class="row justify-content-center">
            <div id="loading-table" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>


        <div class="table-responsive">
            <table id="${tableName.replace('#', '')}" class="table table-striped table-bordered table-hover">
            </table>
        </div>
    </div>
    `, document.getElementById('main-view'))

    var loadingTable = document.getElementById("loading-table");
    loadingTable.style.display = "none"

    generateFilter()
    generateTable(filerDatas[0].value)
    tempLastPath = filerDatas[0].value
}

function generateFilter() {
    render(filerDatas.map((data) => html`
        <option value="${data.value}">${data.tittle}</option>
    `), document.getElementById('filter-data'))

    document.getElementById('filter-data').addEventListener("change", function () {
        var selected = document.getElementById('filter-data').value;
        generateTable(selected)
        tempLastPath = selected;
    });
}

function generateTable(tableUrl) {
    console.log(tableUrl)
    if ($.fn.DataTable.isDataTable(tableName)) {
        tableData.clear().rows.add([]).draw();
    }
    var loadingTable = document.getElementById("loading-table");
    loadingTable.style.display = "block"

    const settings = {
        "async": true,
        "crossDomain": true,
        "url": tableUrl,
        "method": "GET",
        "error": function (xhr, ajaxOptions, thrownError) {
            alert("Failed to Get Response");
            loadingTable.style.display = "none"
        }
    };

    $.ajax(settings).done(function (response) {
        // console.log(response);
        jsonTableData = response
        jsonTableData.forEach((element, index) => {
            // do edit table

            // when edit table finish
            if (jsonTableData.length === index + 1) {

                // create footer
                var footer = $("<tfoot></tfoot>").appendTo(tableName);
                var footertr = $("<tr></tr>").appendTo(footer);
                for (var i = 0; i < tableColumns.length; i++) {
                    $('<td><input type="text" placeholder="Search ' + tableColumns[i].data + '" /></td>').appendTo(footertr);
                }

                // initialize datatables
                if (!$.fn.DataTable.isDataTable(tableName)) {

                    tableData = $(tableName).DataTable({
                        scrollX: true,
                        autoWidth: true,
                        paging: true,
                        fixedHeader: {
                            header: true,
                            footer: false
                        },
                        data: jsonTableData,
                        columns: tableColumns,
                        // dom: '<"top"<"left-col"B><"center-col"l><"right-col"f>>rtlip',
                        dom: '<"top"<"left-col"B><"right-col"f>>rtlip',
                        buttons: [
                            {
                                text: '<i class="fa fa-print"></i>',
                                extend: 'print',
                                titleAttr: 'Print'
                            },
                            {
                                text: '<i class="fa fa-file-excel-o"></i>',
                                extend: 'excelHtml5',
                                titleAttr: 'Download to Excel',
                                title: function () {
                                    return `SB Session - ${$("#filter-data option:selected").text()} - ${util.fullDateToDate(new Date().toISOString())}`
                                }
                            },
                            {
                                text: '<i class="fa fa-refresh"></i>',
                                titleAttr: 'Refresh Table',
                                action: function (e, dt, node, config) {
                                    generateTable(tempLastPath)
                                }
                            }
                        ],
                        initComplete: function (settings, json) {
                            feather.replace()

                            // apply column search
                            this.api().columns().every(function () {
                                var that = this;

                                $('input', this.footer()).on('keyup change clear', function () {
                                    if (that.search() !== this.value) {
                                        that
                                            .search(this.value)
                                            .draw();
                                    }
                                });
                            });

                            // $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

                        }
                    });

                    // $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

                } else {
                    tableData.clear().rows.add(jsonTableData).draw();
                }

            }
        });
        loadingTable.style.display = "none"
    });
}