export function fullDateToDateAndTime(dateinput) {
    try {
        return dateinput.replace(/T/g, ' ').replace(/Z/g, ' ')
    } catch (error) {
        return dateinput
    }
}

export function fullDateToDate(dateinput) {
    try {
        return dateinput.replace(/T/g, ' ').replace(/Z/g, ' ').substring(0, 10)
    } catch (error) {
        return dateinput
    }
}

export var config = {
    supporthost: 'http://103.28.148.203:61006/',
    authhost: 'http://localhost:2000/',
}