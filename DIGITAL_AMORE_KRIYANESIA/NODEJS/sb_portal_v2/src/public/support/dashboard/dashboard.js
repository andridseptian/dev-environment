/* globals Chart:false, feather:false */
import { html, render } from 'https://unpkg.com/lit-html?module';

import * as lembaga from './tablejs/lembaga.js'
import * as user from './tablejs/user.js'
import * as serial from './tablejs/serialNumber.js'
import * as terminal from './tablejs/terminal.js'
import * as sessions from './tablejs/sessions.js'
import * as reportdaysactive from './reportjs/bprAktifHarian.js'
import * as reportmonthsactive from './reportjs/bprAktifBulanan.js'
import * as insertbpr from './additional/insertBPR.js'
import * as duedatesync from './additional/duedatesync.js'
import * as namesync from './additional/namesync.js'

import * as modal from './modal.js'


(function () {
  'use strict'

  // checkSession()

  feather.replace()

  modal.modalInitialize()

  lembaga.show()

  $('#sidebarMenu').collapse({
    toggle: false
  })
  // $('#sidebarMenu').collapse('show')

  initMenu()


})()

function initMenu() {
  var dataMenu = [
    {
      object: lembaga,
      id: "menu-lembaga"
    },
    {
      object: user,
      id: "menu-user"
    },
    {
      object: serial,
      id: "menu-serial"
    },
    {
      object: terminal,
      id: "menu-terminal"
    },
    {
      object: sessions,
      id: "menu-sessions"
    }
  ]

  var dataReport = [
    {
      object: reportdaysactive,
      id: "report-bpraktifharian"
    },
    {
      object: reportmonthsactive,
      id: "report-bpraktifbulanan"
    },
  ]

  var dataTambahan = [
    {
      object: insertbpr,
      id: "add-insertbpr"
    },
    {
      object: duedatesync,
      id: "add-duedatesync"
    },
    {
      object: namesync,
      id: "add-namesync"
    },
  ]

  dataMenu.forEach(element => {
    document.getElementById(element.id).onclick = function () {
      element.object.show()
      $('#sidebarMenu').collapse('hide')
    }
  });
  dataReport.forEach(element => {
    document.getElementById(element.id).onclick = function () {
      element.object.show()
      $('#sidebarMenu').collapse('hide')
    }
  });
  dataTambahan.forEach(element => {
    document.getElementById(element.id).onclick = function () {
      element.object.show()
      $('#sidebarMenu').collapse('hide')
    }
  });
}


function checkSession() {
  const settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://103.28.148.203:61006/support/check/session",
    "method": "GET"
  };

  $.ajax(settings).done(function (response) {
    if (response != 'success') {
      window.location.href = response.redirect;
    }
  });
}
