import { html, render } from 'https://unpkg.com/lit-html?module';

import * as modal from '../modal.js'
import * as util from '../utility.js'

export function show() {
    modal.showSimpleModal("Sinkronisasi due date dilakukan secara otomatis oleh service support tiap 5 menit sekali, dan sinkronisasi due date tiap BPR dilakukan setiap kali BPR tersebut melakukan login pada aplikasi Sharing Bandwidth")
}