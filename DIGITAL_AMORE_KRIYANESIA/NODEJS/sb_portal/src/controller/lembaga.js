const db_sb_gateway = require('../database/db_sb_gateway')
const dtFormat = require('../utility/dateformat')
const http = require("http")

exports.selectAll = (req, res) => {
    console.log('lembaga select all queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    a.*
                from
                    lembaga a
                order by
                    a.bank_id
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.selectActive = (req, res) => {
    console.log('lembaga select notactive queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    a.*
                from
                    lembaga a
                where
                    a.status = 1
                order by
                    a.cr_time desc
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.selectNotActive = (req, res) => {
    console.log('lembaga select notactive queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    a.*
                from
                    lembaga a
                where
                    a.status = 0
                order by
                    a.cr_time desc
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.selectNotPaid = (req, res) => {
    console.log('lembaga select not paid queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    a.*
                from
                    lembaga a
                where
                    a.due_date <= '${dtFormat.currentDueDateBilling()}'
                order by
                    a.bank_id
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.checkBankIDExist = (req, res) => {
    console.log('checkBankIDExist queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)
    db = db_sb_gateway.con;
    db.query(`
            select
                *
            from
                lembaga l
            where
                l.bank_id = ${req.body.bank_id}
        `,
        function (err, result, fields) {

            if (result.rows.length >= 1) {
                res.send({
                    rc: "00",
                    rm: result.rows
                });
            } else {
                res.send({
                    rc: "99",
                    rm: "bank id not exist"
                });
            }
        }
    );
};

exports.delete = (req, res) => {
    console.log('lembaga select all queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)
    db = db_sb_gateway.con;
    db.query(`
            delete
            from
                public.sessions
            where
                bank_id = ${req.body.bank_id};

            delete
            from
                public.lembaga
            where
                bank_id = ${req.body.bank_id};    
        `,
        function (err, result, fields) {
            console.log(result);
            if (err) {
                console.log(err)
                res.send('failed: ' + err.message)
            } else {
                res.send('deleted');
            }
        }
    );
};

exports.updateData = (req, res) => {
    console.log('lembaga update queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)
    db = db_sb_gateway.con;
    db.query(`
            update
                public.lembaga
            set
                bank_name = '${req.body.bank_name}',
                due_date = '${req.body.due_date}',
                status = ${req.body.status}
            where
                bank_id = ${req.body.bank_id};    
        `,
        function (err, result, fields) {
            console.log(result);
            if (err) {
                console.log(err)
                res.send('failed: ' + err.message)
            } else {
                res.send('Updated!');
            }
        }
    );
};

exports.insertLembaga = (req, res) => {
    console.log('lembaga select all queriying')
    var bankName = req.body.bank_name;
    var resp = { rc: "", rm: "" };

    var bankId = req.body.bank_id;
    var info = req.body.registrar;
    var now = new Date();
    var dueDate = new Date();
    var statusLogin = 0;
    var status = 0;
    db = db_sb_gateway.con;

    if (dueDate.getDay > 20) {
        dueDate.setMonth(dueDate.getMonth + 1);
    }

    var length = 10;
    var bool = true;
    next(bankName, bankId, info, now, dueDate, info).then(function (result) {
        console.log(result);
        // if (result == 'error') {
        //     resp.rc = "99";
        //     resp.rm = "Pendaftaran Gagal";
        //     res.send(JSON.stringify(resp));
        // } else {
        res.send(result);
        // }
        console.log('success');
    });
};

exports.insertLembaga2 = (req, res) => {
    console.log('lembaga select all queriying')
    var bankName = req.body.bank_name;
    var resp = { rc: "", rm: "" };
    var bankId = req.body.bank_id;
    var info = req.body.registrar;
    var now = new Date();
    var dueDate = new Date();
    var statusLogin = 0;
    var status = 0;
    db = db_sb_gateway.con;

    if (dueDate.getDay > 20) {
        dueDate.setMonth(dueDate.getMonth + 1);
    }

    var length = 10;
    var bool = true;
    next2(bankName, bankId, info, now, dueDate, info).then(function (result) {
        console.log(result);
        // if (result == 'error') {
        //     resp.rc = "99";
        //     resp.rm = "Pendaftaran Gagal";
        //     res.send(JSON.stringify(resp));
        // } else {
        res.send(result);
        // }
        console.log('success');
    });
};

exports.selectDaysActiveBPR = (req, res) => {
    console.log('lembaga select 5 Days active BPR queriying')
    db = db_sb_gateway.con;

    var custQuery = ``;
    for (let index = 0; index < 10; index++) {
        if (index !== 0) {
            custQuery += `union all`
        }
        custQuery += `
            select
                count(*) as bprcount,
                (now()::date - interval '${index} day')::date as dateactive
            from
                (
                select
                    s.bank_id
                from
                    sessions s
                where
                    s.rc = '00'
                    and s."path" = 'login'
                    and s.req_time::date = now()::date - interval '${index} day'
                group by
                    s.bank_id ) as data_active
        `
    }
    console.log(custQuery);
    db.query(custQuery,
        function (err, result, fields) {
            // generateData(err, result, fields);
            result.rows.forEach((element, index) => {
                var date = new Date(element.dateactive)
                element.dateactive = `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}`
                if (index === result.rows.length - 1) {
                    res.send(result.rows);
                }
            });
        }
    );
}

exports.selectMonthActiveBPR = (req, res) => {
    console.log('lembaga select Month active BPR queriying')
    db = db_sb_gateway.con;

    var currentMonth = new Date().getMonth() + 1;

    var custQuery = ``;
    for (let index = 10; index >= 0; index--) {
        if (index !== 10) {
            custQuery += `union all`
        }
        custQuery += `
                            select
                            count(*) as bprcount,
                            extract(month from now()::timestamp - '${index} month'::interval) as month
                        from
                            (
                            select
                                s.bank_id
                            from
                                sessions s
                            where
                                s.rc = '00'
                                and s."path" = 'login'
                                and extract(month from s.req_time) = extract(month from now()::timestamp - '${index} month'::interval) 
                            group by
                                s.bank_id ) as data_active
        `
    }
    console.log(custQuery);
    db.query(custQuery,
        function (err, result, fields) {
            // generateData(err, result, fields);
            result.rows.forEach((element, index) => {
                if (element.month == 1) { element.month_name = 'Januari' }
                if (element.month == 2) { element.month_name = 'Februari' }
                if (element.month == 3) { element.month_name = 'Maret' }
                if (element.month == 4) { element.month_name = 'April' }
                if (element.month == 5) { element.month_name = 'Mei' }
                if (element.month == 6) { element.month_name = 'Juni' }
                if (element.month == 7) { element.month_name = 'Juli' }
                if (element.month == 8) { element.month_name = 'Agustus' }
                if (element.month == 9) { element.month_name = 'September' }
                if (element.month == 10) { element.month_name = 'Oktober' }
                if (element.month == 11) { element.month_name = 'November' }
                if (element.month == 12) { element.month_name = 'Desember' }

                if (index === result.rows.length - 1) {
                    res.send(result.rows);
                }
            });
        }
    );
}

exports.selectLastLoginAMonthLater = (req, res) => {
    console.log('selectLastLoginAMonthLater')
    db = db_sb_gateway.con;
    db.query(`
                select d.*, l.*
                from (
                    select u.bank_id, u.date_part as month, count(u.bank_id) as num
                    from (
                            select
                                s.bank_id,
                                extract(month from req_time)
                            from
                                sessions s
                            where
                                s."path" = 'login'
                                and s.rc = '00'
                                and extract(month from req_time) = extract(month from now())
                            group by
                                s.bank_id,
                                extract(month from req_time)
                            union all
                            select
                                s.bank_id,
                                extract(month from req_time)
                            from
                                sessions s
                            where
                                s."path" = 'login'
                                and s.rc = '00'
                                and extract(month from req_time) = extract(month from now()) - 1
                            group by
                                s.bank_id,
                                extract(month from req_time)
                        ) u
                    inner join (
                            select
                                s.bank_id,
                                extract(month from req_time)
                            from
                                sessions s
                            where
                                s."path" = 'login'
                                and s.rc = '00'
                                and extract(month from req_time) = extract(month from now())
                            group by
                                s.bank_id,
                                extract(month from req_time)
                            union all
                            select
                                s.bank_id,
                                extract(month from req_time)
                            from
                                sessions s
                            where
                                s."path" = 'login'
                                and s.rc = '00'
                                and extract(month from req_time) = extract(month from now()) - 1
                            group by
                                s.bank_id,
                                extract(month from req_time)
                        ) j on u.bank_id = j.bank_id
                    group by u.bank_id, u.date_part
                ) d
                inner join lembaga l on l.bank_id = d.bank_id
                where d.month = 10 and num = 1
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

function between(min, max) {
    return Math.floor(
        Math.random() * (max - min + 1) + min
    )
}

function next(bankName, bankId, info, now, dueDate, info) {
    return new Promise(function (resolve, reject) {
        console.log(info);
        db.query(`
        INSERT INTO public.lembaga (bank_id, bank_name,status,status_login,info,cr_time,due_date) 
                        VALUES (   ${bankId},'${bankName}',1,0,'${JSON.stringify(info)}','${dtFormat.formatDateAndTime(now)}','${dtFormat.formatDateAndTime(dueDate)}' )              
        `,
            function (err, result, fields) {
                if (err) {
                    console.log(err);
                    // res.send(err);
                    // resolve('error');
                    console.log('tampil');
                }
                console.log(result);
                resolve(info);
                // console(data);
                //generateData(err, result, fields);
                //     resp.rc = "00";
                //     resp.rm = "Success";
                //     res.send(resp);
            }
        );
        // generateData(err, result, fields)
        // res.send(result.rows);
    }).then(function (val) {

        // let body = JSON.stringify({
        val.bank_id = '' + bankId;
        val.bank_name = '' + bankName;
        console.log(val);
        // })
        var promise2 = new Promise(function (resolve, reject) {
            // if (val == 'error') {
            //     resolve('error');
            // } else {

            let body = JSON.stringify(val);
            let data = "";
            var count;
            let options = {
                hostname: "117.54.12.173",
                port: 9980,
                path: "/vpn/broadcast",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Content-Length": Buffer.byteLength(body)
                }
            }

            http
                .request(options, res2 => {
                    res2.on("data", d => {
                        data += d
                    })
                    res2.on("end", () => {
                        // console.log(res2.rc);
                        // count = 1;
                        // console.log(data);
                        // data2.push(data);
                        // console.log(data2.rc);
                        // if(data2.rc == 99){
                        //     console.log('failed');
                        //     var respon = {rc : 99 , rm: "failed"}
                        //     res.send(respon);
                        resolve(data);

                        // }

                    })
                })
                .on("error", console.error)
                .end(body)
            // }

        });
        return promise2;

    })
}


function next2(bankName, bankId, info, now, dueDate, info) {
    return new Promise(function (resolve, reject) {
        console.log(info);
        db.query(`
        INSERT INTO public.lembaga (bank_id, bank_name,status,status_login,cr_time,due_date) 
                        VALUES (   ${bankId},'${bankName}',1,0,'${dtFormat.formatDateAndTime(now)}','${dtFormat.formatDateAndTime(dueDate)}' )              
        `,
            function (err, result, fields) {
                if (err) {
                    console.log(err);
                    // res.send(err);
                    // resolve('error');
                    console.log('tampil');
                }
                console.log(result);
                resolve(info);

                // console(data);
                //generateData(err, result, fields);
                //     resp.rc = "00";
                //     resp.rm = "Success";
                //     res.send(resp);
            }
        );


        // generateData(err, result, fields)
        // res.send(result.rows);
    }).then(function (val) {

        // let body = JSON.stringify({
        val.bank_id = '' + bankId;
        val.bank_name = '' + bankName;
        console.log(val);
        // })
        var promise2 = new Promise(function (resolve, reject) {

            // if (val == 'error') {

            //     resolve('error');
            // } else {

            let body = JSON.stringify(val);
            let data = "";
            var count;
            let options = {
                hostname: "117.54.12.173",
                port: 9980,
                path: "/vpn/broadcast",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Content-Length": Buffer.byteLength(body)
                }
            }

            http
                .request(options, res2 => {
                    res2.on("data", d => {
                        data += d
                    })
                    res2.on("end", () => {
                        // console.log(res2.rc);
                        // count = 1;
                        // console.log(data);
                        // data2.push(data);
                        // console.log(data2.rc);
                        // if(data2.rc == 99){
                        //     console.log('failed');
                        //     var respon = {rc : 99 , rm: "failed"}
                        //     res.send(respon);
                        resolve(data);

                        // }

                    })
                })
                .on("error", console.error)
                .end(body)
            // }
        });
        return promise2;

    })
}

function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        var btn = `<img style="width:12px"
                 src="/assets/img/icon/edit.png" />`
        element.button = `<button type="button" class="lembaga-edit-btn btn btn-primary btn-sm">${btn}</button>`;
        element.invoice = `<a href="https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=${element.bank_id}" target="_blank">Open Invoice</button>`;
    });
}