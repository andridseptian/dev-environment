const db_sb = require('../database/db_sb_gateway')
const dt = require('../utility/dateformat')

exports.selectAll = (req, res) => {
    console.log('App Serial Number select all queriying')
    db = db_sb.con;
    db.query(`
                select
                    a.*
                from
                    configuration a
                    
                order by 
                    a.config_name
                
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

exports.selectByBankId = (req, res) => {
    console.log('Serial Number select all by BankId queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)


    db = db_sb.con;
    db.query(`
            select
                
                a.*
            from
            service_serial_numbers a
            where a.bpr = ${req.body.bpr}
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

exports.getSSHConfig = (req, res) => {
    console.log('Get SSH Config')
    res.send(JSON.stringify({
        "ssh": {
            "ssh_host": "117.54.12.173",
            "ssh_user": "root",
            "ssh_pass": "qawsed1477",
            "ssh_port": "7819",
        },
        "forward": {
            "web_portal_nik_host": {
                "ip": "172.16.160.52",
                "path": "/webportal/login"
            },
            "web_databalikan_host": {
                "ip": "172.16.160.31",
                "path": "/databalikan/login"
            },
        }
    }))
};

exports.getVPNConfig = (req, res) => {
    console.log('Get SSH Config')
    res.send(JSON.stringify({
        vpn: "117.54.12.173"
    }))
};

function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        var btnimage = `<img style="width:12px"
                 src="/assets/img/icon/edit.png" />`
        element.button = `<button type="button" class="terminal-edit-btn btn btn-primary btn-sm">${btnimage}</button>`;
    });
}