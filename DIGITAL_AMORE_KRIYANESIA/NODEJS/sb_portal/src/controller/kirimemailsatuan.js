var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

exports.kirim = (req, res) => {

    var bankid = req.body.bankid;
    var bankname = req.body.bankname;
    var email = req.body.email;
    var serialnumber = req.body.serialnumber;

    var format = `       
                    {
                        "subject": "PESERTA UJI COBA SB 3 GELOMBANG 2",
                        "from": {
                            "name": "PERBARINDO",
                            "email": "noreply@perbarindo.org"
                        },
                        "textpart": "",
                        "html": "PESERTA UJI COBA SB 3 GELOMBANG 2<br><br>Bank ID: ${bankid}<br>Nama BPR: BPR KLATEN SEJAHTERA<br>Serial Number: ${serialnumber}<br>Silahkan menggunakan serial number diatas untuk mengaktifkan terminal anda. Setelah terminal berhasil diaktifkan, silahkan untuk melakukan pendaftaran admin untuk BPR Anda. Ikuti tahap-tahapnya di link berikut <br> http://103.28.148.203/<br>",
                        "to": {
                            "name": "${bankname}",
                            "email": "${email}"
                    }
            `;

    console.log(format);

    const data = JSON.stringify({
        format
    });

    const xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === this.DONE) {
            console.log(this.responseText);
            res.send(this.responseText);
        }
    });

    xhr.open("POST", "https://yunna-service.now.sh//v1/send_mail");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("api-key", "WIBUDEV-YUNNA2020");
    xhr.setRequestHeader("mailjet_user", "4b490dee1673c324e82a232d591627d8");
    xhr.setRequestHeader("mailjet_pass", "4511bce5ec2f3f6ea93e317f81d128f1");

    xhr.send(data);

}