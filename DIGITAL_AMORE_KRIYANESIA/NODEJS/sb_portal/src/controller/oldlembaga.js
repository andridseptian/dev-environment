const db_sb_gateway = require('../database/db_sb_old')
const dtFormat = require('../utility/dateformat')
const http = require("http")

exports.selectAll = (req, res) => {
    console.log('old lembaga select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                m.*,
                u.*
            from
                m_url_mapping m
            inner join m_user_app u on
                m.map_id = u.url_mapping
            where
                u.privilege = 1
        `,
        function (err, result, fields) {
            result.rows.forEach((element, index) => {
                element.invoice = `<a href="https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=${element.bank_id}" target="_blank">Open Invoice</button>`;
                if (element.no_hp.startsWith("08")) {
                    element.no_hp = element.no_hp.replace('0', '+62')
                }
                
                if (result.rows.length - 1 == index) {
                    res.send(result.rows);
                }
            });
        }
    );
};
exports.selectNotPaid = (req, res) => {
    console.log('old lembaga select notpaid queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                m.*,
                u.*
            from
                m_url_mapping m
            inner join m_user_app u on
                m.map_id = u.url_mapping
            where
                u.privilege = 1 and
                m.duedate_time <= now()::date
        `,
        function (err, result, fields) {
            result.rows.forEach((element, index) => {
                element.invoice = `<a href="https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=${element.bank_id}" target="_blank">Open Invoice</button>`;
                
                if (element.no_hp.startsWith("08")) {
                    element.no_hp = element.no_hp.replace('0', '+62')
                }
                
                if (result.rows.length - 1 == index) {
                    res.send(result.rows);
                }
            });
        }
    );
};