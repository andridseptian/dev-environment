const db_sip = require('../database/db_sip')
// const db_sb_gateway = require('../database/db_sb_gateway')
const db_sb_gateway = require('../database/db_sb_gateway')
const dtFormat = require('../utility/dateformat')


exports.selectAll = (req, res) => {
    console.log('sessions select all queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    *
                from
                    sessions s
                where
                    s.req_time >= now()::date
                order by
                    s.session_id desc
                limit 1000
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.selectTodayFailed = (req, res) => {
    console.log('sessions select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                *
            from
                sessions s
            where
                s.req_time >= now()::date
                and s.rc != '00'
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.selectException = (req, res) => {
    console.log('sessions select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select * 
            from sessions s
            where s.exception is not null and s.req_time >= now()::date
            order by s.req_time desc
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};


function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        element.req_time = dtFormat.formatDateAndTime(element.req_time)
        element.res_time = dtFormat.formatDateAndTime(element.res_time)
    });
}