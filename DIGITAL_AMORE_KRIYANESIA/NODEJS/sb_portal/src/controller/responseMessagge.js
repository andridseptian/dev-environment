const db_sb = require('../database/db_sb_gateway')
const dt = require('../utility/dateformat')

exports.selectAll = (req, res) => {
    console.log('Response message select all queriying')
    db = db_sb.con;
    db.query(`
                select
                    a.*
                from
                    response_message a
                    
                order by 
                    a.id desc

                    limit
                    100
 
                
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

exports.selectByBankId = (req, res) => {
    console.log('Serial Number select all by BankId queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)


    db = db_sb.con;
    db.query(`
            select
                
                a.*
            from
            service_serial_numbers a
            where a.bpr = ${req.body.bpr}
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        var btnimage = `<img style="width:12px"
                 src="/assets/img/icon/edit.png" />`
        element.button = `<button type="button" class="terminal-edit-btn btn btn-primary btn-sm">${btnimage}</button>`;
    });
}