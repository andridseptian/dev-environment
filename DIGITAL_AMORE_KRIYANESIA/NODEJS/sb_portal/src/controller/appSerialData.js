const db_sb_gateway = require('../database/db_sb_gateway')
const dt = require('../utility/dateformat')

exports.selectAll = (req, res) => {
    console.log('App Serial Number select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select asn.*, l.bank_id, l.bank_name 
            from app_serial_number asn 
            inner join lembaga l 
            on asn.bank_id = l.bank_id 
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

exports.selectByBankId = (req, res) => {
    console.log('Serial Number select all by BankId queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)

    db = db_sb_gateway.con;
    db.query(`
            select
                
                a.*
            from
            app_serial_number a
            where a.bpr = ${req.body.bpr}
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};



exports.resetSerialNumberBySerial = (req, res) => {
    console.log('Serial Number select all by BankId queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)

    db = db_sb_gateway.con;
    db.query(`
            update
                app_serial_number
            set
                mac_address = null,
                terminal_id = null,
                status = null
            where
                serial_number = '${req.body.serial_number}'
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            console.log(result);
            if (err) {
                console.log(err)
                res.send('failed')
            } else {
                res.send(result.rows);
            }
        }
    );
};

exports.deleteSerialNumber = (req, res) => {
    console.log('Delete Serial Number Queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)

    db = db_sb_gateway.con;
    db.query(`
            DELETE FROM public.app_serial_number
            WHERE serial_number='${req.body.serial_number}'; 
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            console.log(result);
            if (err) {
                console.log(err)
                res.send('failed')
            } else {
                res.send(result.rows);
            }
        }
    );
};


function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        var btnimage = `<img style="width:12px"
                 src="/assets/img/icon/edit.png" />`
        element.button = `<button type="button" class="serial-edit-btn btn btn-primary btn-sm">${btnimage}</button>`;
    });
}