const crypto = require("crypto");
const forge = require("node-forge");
const db_sip = require('../database/db_sip')
const db_sb = require('../database/db_sb')
const db_sb_gateway = require('../database/db_sb_gateway')

// const db_sb = require('../database/db_sb_old')
const dtFormat = require('../utility/dateformat')
const tdes = require('../utility/tdes')

// exports.selectAll = (req, res) => {
//     console.log('userapp select all queriying')
//     db = db_sb.con;
//     db.query(`
//             SELECT *
//             FROM m_user_app a
//             INNER JOIN m_url_mapping b
//             ON a.url_mapping = b.map_id;
//         `,
//         function (err, result, fields) {
//             console.log("select all users get result");
//             if (err) throw err;
//             result.rows.forEach(element => {
//                 var btnimage = `<img style="width:12px"
//                  src="/assets/img/icon/edit.png" />`
//                 element.button = `<button type="button" class="user-edit-btn btn btn-primary btn-sm">${btnimage}</button>`;
//                 element.username = {
//                     enc: element.username,
//                     dec: (decrypt3DES(element.username, 'APLIKASISHARINGBANDWIDTHENKRIPSI'))
//                 }
//                 element.password = {
//                     enc: element.password,
//                     dec: (decrypt3DES(element.password, 'APLIKASISHARINGBANDWIDTHENKRIPSI'))
//                 }
//                 element.nik = {
//                     enc: element.nik,
//                     dec: (decrypt3DES(element.nik, 'APLIKASISHARINGBANDWIDTHENKRIPSI'))
//                 }
//                 element.expired_date = dtFormat.formatDate(element.expired_date);
//                 element.last_login = dtFormat.formatDateAndTime(element.last_login);
//             });
//             res.send(result.rows);
//         }
//     );
// };

exports.selectAll = (req, res) => {
    console.log('userapp select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                b.bank_id,
                b.bank_name,
                b.due_date,
                a.*
            from
                user_app a
            inner join lembaga b on
                a.bank_id = b.bank_id
        `,
        function (err, result, fields) {
            console.log("select all users get result");
            if (err) throw err;
            generateUserTableData(req, res, err, result, fields)
        }
    );
};

exports.selectAllAdmin = (req, res) => {
    console.log('userapp select all Admin queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                b.bank_id,
                b.bank_name,
                b.due_date,
                a.*
            from
                user_app a
            inner join lembaga b on
                a.bank_id = b.bank_id
            where
                a.user_type = 1
        `,
        function (err, result, fields) {
            console.log("select all admin get result");
            if (err) throw err;
            generateUserTableData(req, res, err, result, fields)
        }
    );
};

exports.selectAllNotPaid = (req, res) => {
    console.log('userapp select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                b.bank_id,
                b.bank_name,
                b.due_date,
                a.*
            from
                user_app a
            inner join lembaga b on
                a.bank_id = b.bank_id
            where b.due_date <= '${dtFormat.currentDueDateBilling()}'
        `,
        function (err, result, fields) {
            console.log("select all users get result");
            if (err) throw err;
            generateUserTableData(req, res, err, result, fields)
        }
    );
};

exports.selectAllNotPaidAdmin = (req, res) => {
    console.log('userapp select all queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    b.bank_id,
                    b.bank_name,
                    b.due_date,
                    a.*
                from
                    user_app a
                inner join lembaga b on
                    a.bank_id = b.bank_id
                where
                    b.due_date <= '${dtFormat.currentDueDateBilling()}' 
                    and a.user_type = 1
        `,
        function (err, result, fields) {
            console.log("select all users get result");
            if (err) throw err;
            generateUserTableData(req, res, err, result, fields)
        }
    );
};

exports.selectAllBlocked = (req, res) => {
    console.log('userapp select blocked user')
    db = db_sb_gateway.con;
    db.query(`
                select
                    b.bank_id,
                    b.bank_name,
                    b.due_date,
                    a.*
                from
                    user_app a
                inner join lembaga b on
                    a.bank_id = b.bank_id
                where
                    a.status = 2;
        `,
        function (err, result, fields) {
            console.log("select blocked users get result");
            if (err) throw err;
            generateUserTableData(req, res, err, result, fields)
        }
    );
};

exports.selectAllNotActive = (req, res) => {
    console.log('userapp select notactive user')
    db = db_sb_gateway.con;
    db.query(`
                select
                    b.bank_id,
                    b.bank_name,
                    b.due_date,
                    a.*
                from
                    user_app a
                inner join lembaga b on
                    a.bank_id = b.bank_id
                where
                    a.status = 0;
        `,
        function (err, result, fields) {
            console.log("select not active users get result");
            if (err) throw err;
            generateUserTableData(req, res, err, result, fields)
        }
    );
};

exports.getAllAdminUser = (req, res) => {
    console.log('getAllAdminUser')
    db = db_sb_gateway.con;
    db.query(`
                select
                    ua.bank_id,
                    l.bank_name,
                    l.due_date,
                    ua.user_id,
                    ua.fullname,
                    ua.phone_number,
                    ua.email
                from
                    user_app ua
                inner join lembaga l on
                    ua.bank_id = l.bank_id
                where
                    ua.user_type = 1
                order by
                    ua.bank_id asc
        `,
        function (err, result, fields) {
            console.log("getAllAdminUser get result");
            if (err) throw err;
            result.rows.forEach((element, index) => {
                if (element.phone_number.startsWith("08")) {
                    element.phone_number = element.phone_number.replace('0', '+62')
                }
                if (index == result.rows.length - 1) {
                    res.send(result.rows);
                }
            })
        }
    );
};

exports.getAllUser = (req, res) => {
    console.log('getAllUser')
    db = db_sb_gateway.con;
    db.query(`
                select
                    ua.bank_id,
                    l.bank_name,
                    l.due_date,
                    ua.user_id,
                    ua.fullname,
                    ua.phone_number,
                    ua.email
                from
                    user_app ua
                inner join lembaga l on
                    ua.bank_id = l.bank_id
                order by
                    ua.bank_id asc
        `,
        function (err, result, fields) {
            console.log("getAllUser get result");
            if (err) throw err;
            result.rows.forEach((element, index) => {
                if (element.phone_number.startsWith("08")) {
                    element.phone_number = element.phone_number.replace('0', '+62')
                }
                if (index == result.rows.length - 1) {
                    res.send(result.rows);
                }
            })
        }
    );
};

exports.getAllAdminDueDatePass = (req, res) => {
    console.log('getAllUser')
    db = db_sb_gateway.con;
    db.query(`
                select
                    ua.bank_id,
                    l.bank_name,
                    l.due_date,
                    ua.user_id,
                    ua.fullname,
                    ua.phone_number,
                    ua.email
                from
                    user_app ua
                inner join lembaga l on
                    ua.bank_id = l.bank_id
                where
                    ua.user_type = 1
                    and l.due_date <= '${dtFormat.currentDueDateBilling()}'
                order by
                    ua.bank_id asc
        `,
        function (err, result, fields) {
            console.log("getAllUser get result");
            if (err) throw err;
            result.rows.forEach((element, index) => {
                if (element.phone_number.startsWith("08")) {
                    element.phone_number = element.phone_number.replace('0', '+62')
                }
                if (index == result.rows.length - 1) {
                    res.send(result.rows);
                }
            })
        }
    );
};

function generateUserTableData(req, res, err, result, fields) {
    result.rows.forEach((element, index) => {
        var btnimage = `<img style="width:12px"
         src="/assets/img/icon/edit.png" />`
        element.button = `<button type="button" class="user-edit-btn btn btn-primary btn-sm">${btnimage}</button>`;
        element.invoice = `<a href="https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=${element.bank_id}" target="_blank">Open Invoice</button>`;
        element.username = {
            enc: element.username,
            dec: (decrypt3DES(element.username, 'APLIKASISHARINGBANDWIDTHENKRIPSI'))
        }
        element.password = {
            enc: element.password,
            dec: (decrypt3DES(element.password, 'APLIKASISHARINGBANDWIDTHENKRIPSI'))
        }
        element.nik = {
            enc: element.nik,
            dec: (decrypt3DES(element.nik, 'APLIKASISHARINGBANDWIDTHENKRIPSI'))
        }
        element.expired_date = dtFormat.formatDate(element.expired_date);
        element.last_login = dtFormat.formatDateAndTime(element.last_login);

        if (element.phone_number.startsWith("08")) {
            element.phone_number = element.phone_number.replace('0', '+62')
        }

        element.due_date = dtFormat.formatDate(element.due_date)

        if (index == result.rows.length - 1) {
            res.send(result.rows);
        }
    });
}


// exports.update = (req, res) => {
//     console.log('userapp update queriying')
//     console.log(`this is body: ${JSON.stringify(req.body)}`)


//     /** 	"username":"",
//             "password":"",
//             "name":"",
//             "nik":"",
//             "email":"",
//             "no_hp":"",
//             "fail_login":"" 
//     */

//     db = db_sb.con;
//     db.query(`
//                UPDATE public.m_user_app
//                SET 
//                    dtm_updated = '${new Date().toUTCString()}' -- timestamp without time zone NULLABLE
//                    , username = '${encrypt3DES(req.body.username,'APLIKASISHARINGBANDWIDTHENKRIPSI')}' -- character varying NULLABLE
//                    , password = '${encrypt3DES(req.body.password,'APLIKASISHARINGBANDWIDTHENKRIPSI')}' -- character varying NULLABLE
//                    , name = '${req.body.full_name}' -- character varying NULLABLE
//                    , nik = '${encrypt3DES(req.body.nik,'APLIKASISHARINGBANDWIDTHENKRIPSI')}' -- character varying
//                    , email = '${req.body.email}' -- character varying NULLABLE
//                    , no_hp = '${req.body.no_hp}' -- character varying
//                    , fail_login = '${req.body.fail_login}' -- integer NULLABLE
//                WHERE id = ${req.body.userid}
//        `,
//         function (err, result, fields) {
//             console.log("update user data");
//             if (err) {
//                 console.log(err);
//                 res.send(err)
//             } else {
//                 console.log(result);
//                 res.send("success");
//             }
//         })
// };



exports.update = (req, res) => {
    console.log('userapp update queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)


    /** 	"username":"",
            "password":"",
            "name":"",
            "nik":"",
            "email":"",
            "no_hp":"",
            "fail_login":"" 
    */

    db = db_sb_gateway.con;
    db.query(`
               UPDATE public.user_app
               SET 
                   mod_time = '${new Date().toUTCString()}' -- timestamp without time zone NULLABLE
                   , username = '${encrypt3DES(req.body.username, 'APLIKASISHARINGBANDWIDTHENKRIPSI')}' -- character varying NULLABLE
                   , password = '${encrypt3DES(req.body.password, 'APLIKASISHARINGBANDWIDTHENKRIPSI')}' -- character varying NULLABLE
                   , fullname = '${req.body.full_name}' -- character varying NULLABLE
                   , nik = '${encrypt3DES(req.body.nik, 'APLIKASISHARINGBANDWIDTHENKRIPSI')}' -- character varying
                   , email = '${req.body.email}' -- character varying NULLABLE
                   , phone_number = '${req.body.no_hp}' -- character varying
                   , fail_login = '${req.body.fail_login}' -- integer NULLABLE
                   , expired_password = '${req.body.expired_date}' -- integer NULLABLE
                   , status = '${req.body.status}'
                   , user_type = '${req.body.user_type}'
                   , status_login = '${req.body.status_login}'
               WHERE user_id = ${req.body.userid}
       `,
        function (err, result, fields) {
            console.log("update user data");
            if (err) {
                console.log(err);
                res.send(JSON.stringify(err.message));
            } else {
                console.log(result);
                res.send("success");
            }
        })
};

exports.delete = (req, res) => {
    console.log('userapp delete queriying')
    console.log(`this is body: ${JSON.stringify(req.body)}`)

    db = db_sb_gateway.con;
    db.query(`
            DELETE FROM public.user_app
            WHERE user_id=${req.body.user_id}
       `,
        function (err, result, fields) {
            console.log("delete user data");
            if (err) {
                console.log(err);
                res.send(err)
            } else {
                console.log(result);
                res.send("success");
            }
        })
};


function decrypt3DES(input, key) {
    let md5Key = forge.md.md5.create();
    md5Key.update(key);
    md5Key = md5Key.digest().toHex();
    const decipher = forge.cipher.createDecipher('3DES-ECB', key.substring(0, 24));
    decipher.start();

    const inputEx = forge.util.createBuffer(Buffer.from(input, "base64").toString("binary"));
    decipher.update(inputEx);
    decipher.finish();
    const decrypted = decipher.output;
    return Buffer.from(decrypted.getBytes(), "binary").toString("utf8")
}

function encrypt3DES(input, key) {
    var md5Key = forge.md.md5.create();
    md5Key.update(key);
    md5Key = md5Key.digest().toHex();

    // var cipher = forge.cipher.createCipher('3DES-ECB', md5Key.substring(0, 24));
    var cipher = forge.cipher.createCipher('3DES-ECB', key.substring(0, 24));
    cipher.start();
    cipher.update(forge.util.createBuffer(Buffer.from(input, "utf8").toString("binary")));
    cipher.finish();
    var encrypted = cipher.output;

    return Buffer.from(encrypted.getBytes(), "binary").toString("base64")
}


