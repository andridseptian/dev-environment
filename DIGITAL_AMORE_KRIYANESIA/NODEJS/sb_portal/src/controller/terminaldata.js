const db_sb = require('../database/db_sb')
const db_sb_gateway = require('../database/db_sb_gateway')

const dt = require('../utility/dateformat')

exports.selectAll = (req, res) => {
    console.log('terminal select all queriying')
    db = db_sb.con;
    db.query(`
                select
                    b.bank_id,
                    b.bank_name,
                    a.*
                from
                    m_url_allowed_mapping a
                    inner join m_url_mapping b on a.mapping_id = b.map_id
                    and a.status = 1
                order by
                    a.cr_time
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

exports.delete = (req, res) => {
    console.log('delete terminal data')
    console.log(`this is body: ${JSON.stringify(req.body)}`)
    db = db_sb_gateway.con;
    db.query(`
            DELETE FROM terminal
            WHERE terminal_id=${req.body.terminal_id};
        `,
        function (err, result, fields) {
            console.log(result);
            if (err) {
                console.log(err)
                res.send('failed')
            } else {
                // res.send(result.rows);
                res.send("DELETED!!")
            }
        }
    );
};


exports.selectAll2 = (req, res) => {
    console.log('terminal select all queriying')
    db = db_sb_gateway.con;
    db.query(`
                select
                    b.bank_name,
                    a.*
                from
                    terminal a
                    inner join lembaga b on a.bank_id = b.bank_id
                    and a.status = 1
                order by
                    a.cr_time
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};



exports.selectActive = (req, res) => {
    console.log('terminal select all active queriying')
    db = db_sb_gateway.con;
    db.query(`
            select
                b.bank_name,
                a.*
            from
                terminal a
                inner join lembaga b on a.bank_id = b.bank_id
                and a.status = 1
        `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

exports.selectNotactive = (req, res) => {
    console.log('terminal select all active queriying')
    db = db_sb_gateway.con;
    db.query(`
        select
        b.bank_name,
        a.*
        from
        terminal a
        inner join lembaga b on a.bank_id = b.bank_id
        and a.status = 0
            `,
        function (err, result, fields) {
            generateData(err, result, fields)
            res.send(result.rows);
        }
    );
};

function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        var btnimage = `<img style="width:12px"
                 src="/assets/img/icon/edit.png" />`
        element.button = `<button type="button" class="terminal-edit-btn btn btn-primary btn-sm">${btnimage}</button>`;
    });
}