const db_sip = require('../database/db_sip')
const db_sb = require('../database/db_sb_gateway')
const db_sb_old = require('../database/db_sb_old')
const dtFormat = require('../utility/dateformat')

exports.syncduedate = (req, res) => {
    console.log('sync due date')
    db = db_sip.con;
    db.query(`
            SELECT *
            FROM service_orders so where so.due_date > now()
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            result.rows.forEach(element => {
                // console.log(element);
                var due_date = dtFormat.formatDateAndTime(element.due_date)

                sb = db_sb.con;
                sb.query(`
                            UPDATE public.lembaga
                            SET due_date = '${due_date}'
                            WHERE bank_id = ${element.bpr};
                        `,
                    function (err, result, fields) {
                        count++;
                        // console.log(`BPR: ${element.bpr} due date to ${due_date} data number ${count}`)
                        if (err) {
                            console.error(err);
                            res.send(`failed sync  at ${count} data`);
                        }
                        if (count >= length) {
                            console.log(`success sync ${count} data`)
                            res.send(`success sync ${count} data`);
                        }
                    });
            });

        });
}

exports.syncduedateold = (req, res) => {
    console.log('sync due date')
    db = db_sip.con;
    db.query(`
            SELECT *
            FROM service_orders so where so.due_date > now()
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            result.rows.forEach(element => {
                // console.log(element);
                // var due_date = dtFormat.formatDateAndTime(element.due_date)
                var due_date = new Date(element.due_date).toISOString();

                sb = db_sb_old.con;
                sb.query(`
                            UPDATE public.m_url_mapping
                            SET duedate_time = '${due_date}'
                            WHERE bank_id = ${element.bpr};
                        `,
                    function (err, result, fields) {
                        count++;
                        // console.log(`OLD DB BPR: ${element.bpr} due date to ${due_date} data number ${count}`)
                        if (err) {
                            console.error(err);
                            res.send(`failed sync  at ${count} data`);
                        }
                        if (count >= length) {
                            console.log(`success sync ${count} data`)
                            res.send(`success sync ${count} data`);
                        }
                    });
            });

        });
}

exports.getbprdata = (req, res) => {
    console.log('get all bpr data')
    db = db_sip.con;
    db.query(`
            select b.id, b.jenis_bank , b."name" 
            from bpr b
            order by b."name" 
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            // result.rows.forEach(element => {

            // });
            res.send(result.rows)
        });
}

exports.getbprbyname = (req, res) => {
    console.log('get bpr by name')
    db = db_sip.con;
    db.query(`
            select b.id, b.jenis_bank , b."name" 
            from bpr b where b."name" = '${req.body.name}' 
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            // result.rows.forEach(element => {

            // });
            res.send(result.rows)
        });
}

exports.getallbprsb = (req, res) => {
    console.log('get bpr by name')
    db = db_sip.con;
    db.query(`
            select
                so.bpr,
                b."name",
                b.jenis_bank,
                b.dpd,
                so.status,
                so.due_date
            from
                service_orders so
            inner join bpr b on
                so.bpr = b.id
            where
                so.status = 'ACTIVE'
                and due_date > now()::date
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            console.log('length: ' + result.rows.length);
            var sbsip = result.rows;
            res.send(sbsip)
        });
}

exports.getBprNotPaid = (req, res) => {
    console.log('get bpr active not paid')
    db = db_sip.con;
    db.query(`
                select
                    b.id,
                    b."name",
                    b.address,
                    b.telp,
                    b.fax,
                    b.email,
                    b.website,
                    b.jenis_bank,
                    so.bpr,
                    so.order_date,
                    so.due_date,
                    so.status,
                    si.status,
                    si.order_number,
                    si.payment_code,
                    count(b.id) 
                from
                    service_orders so
                inner join service_invoices si on
                    so.bpr = si.bpr
                inner join bpr b on
                    si.bpr = b.id
                where
                    si.status <> 'PAID'
                    and so.status = 'ACTIVE'
                group by
                    b.id,
                    b."name",
                    b.address,
                    b.telp,
                    b.fax,
                    b.email,
                    b.website,
                    b.jenis_bank,
                    so.bpr,
                    so.order_date,
                    so.due_date,
                    so.status,
                    si.status,
                    si.order_number,
                    si.payment_code
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) {
                res.send(JSON.stringify({
                    rc: 99,
                    rm: err.message
                }))
            } else {
                result.rows.forEach((element, index) => {
                    element.invoice = `<a href="https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=${element.bpr}" target="_blank">Open Invoice</button>`
                    if (index === result.rows.length - 1) {
                        res.send(result.rows)
                    }
                })
            }
        });
}

exports.getNotPaidOneMonth = (req, res) => {
    console.log('--Get Not Paid One Month--')
    db = db_sip.con;
    db.query(`
                select
                    b.id,
                    b."name",
                    b.address,
                    b.telp,
                    b.fax,
                    b.email,
                    b.website,
                    b.jenis_bank,
                    so.bpr,
                    so.order_date,
                    so.due_date,
                    so.status,
                    si.status,
                    si.order_number,
                    si.payment_code,
                    count(b.id)
                from
                    service_orders so
                inner join service_invoices si on
                    so.bpr = si.bpr
                inner join bpr b on
                    si.bpr = b.id
                where
                    si.status <> 'PAID'
                    and si."type" = 'extension' 
                    and extract(month from so.due_date) = extract(month from now())
                group by 
                    b.id,
                    b."name",
                    b.address,
                    b.telp,
                    b.fax,
                    b.email,
                    b.website,
                    b.jenis_bank,
                    so.bpr,
                    so.order_date,
                    so.due_date,
                    so.status,
                    si.status,
                    si.order_number,
                    si.payment_code
                having count(b.id) < 2
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) {
                res.send(JSON.stringify({
                    rc: 99,
                    rm: err.message
                }))
            } else {
                // res.send(result.rows)
                result.rows.forEach((element, index) => {
                    element.invoice = `<a href="https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=${element.bpr}" target="_blank">Open Invoice</button>`
                    if (index === result.rows.length - 1) {
                        res.send(result.rows)
                    }
                });
            }
        });
}