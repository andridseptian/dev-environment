const db_sb = require('../database/db_sb')
const db_sb_gateway = require('../database/db_sb_gateway')
const dt = require('../utility/dateformat')

exports.lembaga = (req, res) => {
    console.log('migrate data')
    db = db_sb.con;

    /** INSERT INTO public.m_url_mapping
        (bank_id, bank_name, cr_time, description, destination, mod_time, status, terminal_name, group_mapping, duedate_time, register_time, last_login_time, status_login, info)
        VALUES(0, '', '', '', '', '', 0, '', 0, '', '', '', 0, '');
    */

    db.query(`
            SELECT *
            FROM public.m_url_mapping;
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            result.rows.forEach(element => {
                console.log(element);

                sb = db_sb_gateway.con;
                sb.query(`
                            INSERT INTO public.lembaga
                            (bank_id, bank_name, cr_time, mod_time, status, due_date, last_login_time, status_login)
                            VALUES(
                                ${element.bank_id},
                                '${element.bank_name}', 
                                '${dt.full(element.cr_time)}', 
                                '${dt.full(element.mod_time)}', 
                                ${element.status}, 
                                '${dt.full(element.duedate_time)}', 
                                '${dt.full(element.last_login_time)}', 
                                ${element.status_login}
                            );
                        `,
                    function (err, result, fields) {
                        count++;
                        console.log(`Bank ID: ${element.bank_id} Bank Name: ${element.bank_name} Due Date: ${dt.full(element.duedate_time)}  data number ${count}`)
                        if (err) {
                            console.error(err);
                            res.send(err);
                        }
                        if (count >= length) {
                            res.send(`success migrate ${count} data`);
                        }
                    });
            });

        });
}

exports.terminal = (req, res) => {
    console.log('migrate data')
    db = db_sb.con;

    /** INSERT INTO public.m_url_allowed_mapping
        (allow_id, activation_time, cr_time, data_allowed, description, mod_time, paramter, registration_code, registration_time, status, terminal_id, mapping_id, host_name, processor_id, processor_name, sn_windows, uuid)
        VALUES('', '', '', '', '', 0, '', '', 0, 0, 0, '', '', '', '', '');

    */

    /** SELECT a.allow_id, a.activation_time, a.cr_time, a.data_allowed, a.description, a.mod_time, a.paramter, a.registration_code, a.registration_time, a.status, a.terminal_id, a.mapping_id, a.host_name, a.processor_id, a.processor_name, a.sn_windows, a.uuid
        FROM public.m_url_allowed_mapping;
    */

    db.query(`
            select a.allow_id, a.activation_time, a.cr_time, a.data_allowed, a.description, a.mod_time, a.paramter, a.registration_code, a.registration_time, a.status, a.terminal_id, a.mapping_id, a.host_name, a.processor_id, a.processor_name, a.sn_windows, a.uuid,b.bank_id
            from m_url_allowed_mapping a
            inner join m_url_mapping b 
            on a.mapping_id = b.map_id
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            result.rows.forEach(element => {
                console.log(element);
                sb = db_sb_gateway.con;
                sb.query(`
                            INSERT INTO public.terminal
                            (
                                terminal_id, 
                                bank_id, 
                                cr_time, 
                                mod_time, 
                                serial_number, 
                                terminal_number, 
                                mac_address, 
                                host_name, 
                                processor_id, 
                                processor_name, 
                                serial_windows,
                                uuid, 
                                status
                            )
                            VALUES(
                                ${element.allow_id}, 
                                ${element.bank_id}, 
                                '${dt.full(element.cr_time)}', 
                                '${dt.full(element.mod_time)}', 
                                '${element.registration_code}', 
                                ${element.terminal_id}, 
                                '${element.data_allowed}', 
                                '${element.host_name}', 
                                '${element.processor_id}', 
                                '${element.processor_name}', 
                                '${element.sn_windows}', 
                                '${element.uuid}', 
                                ${element.status}
                            );
                        `,
                    function (err, result, fields) {
                        count++;
                        console.log(`Terminal ID: ${element.allow_id} data number ${count}`)
                        if (err) {
                            console.error(err);
                            res.send(err);
                        }
                        if (count >= length) {
                            res.send(`success migrate ${count} data`);
                        }
                    });
            });

        });
}

exports.userapps = (req, res) => {
    console.log('migrate data')
    db = db_sb.con;

    /** INSERT INTO public.m_url_allowed_mapping
        (allow_id, activation_time, cr_time, data_allowed, description, mod_time, paramter, registration_code, registration_time, status, terminal_id, mapping_id, host_name, processor_id, processor_name, sn_windows, uuid)
        VALUES('', '', '', '', '', 0, '', '', 0, 0, 0, '', '', '', '', '');

    */

    /** SELECT a.allow_id, a.activation_time, a.cr_time, a.data_allowed, a.description, a.mod_time, a.paramter, a.registration_code, a.registration_time, a.status, a.terminal_id, a.mapping_id, a.host_name, a.processor_id, a.processor_name, a.sn_windows, a.uuid
        FROM public.m_url_allowed_mapping;
    */

    db.query(`
            SELECT
                a.*,
                b.bank_id
            FROM
                m_user_app a
                INNER JOIN m_url_mapping b ON a.url_mapping = b.map_id
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            result.rows.forEach(element => {
                console.log(element);
                sb = db_sb_gateway.con;
                sb.query(`
                            INSERT INTO
                                public.user_app (
                                    user_id,
                                    cr_time,
                                    mod_time,
                                    bank_id,
                                    username,
                                    "password",
                                    fullname,
                                    nik,
                                    email,
                                    phone_number,
                                    user_type,
                                    expired_password,
                                    fail_login,
                                    status,
                                    info,
                                    additional_data,
                                    status_login,
                                    last_login,
                                    last_logout,
                                    terminal_login,
                                    activate_date
                                )
                            VALUES(
                                '${element.id}',
                                '${new Date(element.dtm_created).toUTCString()}',
                                '${new Date(element.dtm_updated).toUTCString()}',
                                '${element.bank_id}',
                                '${element.username}',
                                '${element.password}',
                                '${element.name}',
                                '${element.nik}',
                                '${element.email}',
                                ${element.no_hp.tostring()},
                                '${element.privilege}',
                                '${new Date(element.expired_date).toUTCString()}',
                                ${element.fail_login},
                                ${element.status},
                                NULL,
                                NULL,
                                ${element.status_login},
                                '${new Date(element.last_login).toUTCString()}',
                                NULL,
                                ${element.terminal_login},
                                '${new Date(element.active_date).toUTCString()}'
                            );
                        `,
                    function (err, result, fields) {
                        count++;
                        console.log(`User ID: ${element.id} Email: ${element.email} Active: ${new Date(element.active_date).toUTCString()}  data number ${count}`)
                        if (err) {
                            console.error(err);
                            console.log(`failed at number ${count}`)
                            res.send(err);
                        }
                        if (count >= length) {
                            res.send(`success migrate ${count} data`);
                        }
                    });
            });

        });
}

exports.serialnumbers = (req, res) => {
    console.log('migrate data')
    db = db_sb.con;

    /** INSERT INTO public.m_url_allowed_mapping
        (allow_id, activation_time, cr_time, data_allowed, description, mod_time, paramter, registration_code, registration_time, status, terminal_id, mapping_id, host_name, processor_id, processor_name, sn_windows, uuid)
        VALUES('', '', '', '', '', 0, '', '', 0, 0, 0, '', '', '', '', '');

    */

    /** SELECT a.allow_id, a.activation_time, a.cr_time, a.data_allowed, a.description, a.mod_time, a.paramter, a.registration_code, a.registration_time, a.status, a.terminal_id, a.mapping_id, a.host_name, a.processor_id, a.processor_name, a.sn_windows, a.uuid
        FROM public.m_url_allowed_mapping;
    */

    db.query(`
            SELECT
                id,
                service_plan_id,
                serial_number,
                bpr,
                requested_at,
                mac_address
            FROM
                public.service_serial_numbers;
        `,
        function (err, result, fields) {
            console.log("querying...");
            if (err) throw err;
            var count = 0;
            var failCount = 0;
            console.log('length: ' + result.rows.length);
            var length = result.rows.length;
            result.rows.forEach(element => {
                console.log(element);
                sb = db_sb_gateway.con;
                sb.query(`
                            INSERT INTO
                                public.app_serial_number (
                                    serial_number,
                                    generate_number,
                                    bank_id,
                                    used_time,
                                    mac_address,
                                    status,
                                    info,
                                    additional_data
                                )
                            VALUES(
                                '${element.serial_number}',
                                NULL, 
                                '${element.bpr}',
                                NULL, 
                                '${element.mac_address}',
                                1,
                                NULL,
                                NULL
                            );
                        `,
                    function (err, result, fields) {
                        count++;
                        console.log(`Bank ID: ${element.id} Serial: ${element.serial_number} mac: ${element.mac_address}  data number ${count}`)
                        if (err) {
                            failCount++
                            console.error(err);
                            console.log(`failed at number ${count}`)
                            // res.send(err);
                        }
                        if (count >= length) {
                            res.send(`success migrate ${count} data, failed process ${failCount}`);
                        }
                    });
            });

        });
}