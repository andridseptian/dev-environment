const db_sb_gateway = require('../database/db_sb_gateway')
const dtFormat = require('../utility/dateformat')


exports.selectByReqTime = (req, res) => {
    console.log('sessions vpn select all queriying')
    db = db_sb_gateway.con;
    db.query(`
            select * 
            from sessions_vpn s 
            order by s.req_time desc
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};

exports.updateTest = (req, res) => {
    console.log('sessions select all queriying')
    db = db_sb_gateway.con;
    db.query(`
        UPDATE sessions_vpn SET rm = 'ada' WHERE session_id = 102 
        `,
        function (err, result, fields) {
            generateData(err, result, fields);
            res.send(result.rows);
        }
    );
};


function generateData(err, result, fields) {
    if (err) throw err;
    result.rows.forEach(element => {
        element.req_time = dtFormat.formatDateAndTime(element.req_time)
        element.res_time = dtFormat.formatDateAndTime(element.res_time)
    });
}