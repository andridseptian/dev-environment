exports.formatDate = function (date) {
    if (date != "") {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    } else {
        return "";
    }
}

exports.formatDateAndTime = function (date) {
    if (date != "") {
        try {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hours = d.getHours().toString(),
                minutes = d.getMinutes().toString(),
                seconds = d.getSeconds().toString();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            if (hours.length < 2)
                hours = '0' + hours;
            if (minutes.length < 2)
                minutes = '0' + minutes;
            if (seconds.length < 2)
                seconds = '0' + seconds;

            return [year, month, day].join('-') + " " + [hours, minutes, seconds].join(':');
        } catch (error) {
            console.error(error);
        }
    } else {
        return "";
    }
}

exports.full = function (date) {
    if (date != "") {
        try {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hours = d.getHours().toString(),
                minutes = d.getMinutes().toString(),
                seconds = d.getSeconds().toString();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            if (hours.length < 2)
                hours = '0' + hours;
            if (minutes.length < 2)
                minutes = '0' + minutes;
            if (seconds.length < 2)
                seconds = '0' + seconds;

            return [year, month, day].join('-') + " " + [hours, minutes, seconds].join(':');
        } catch (error) {
            console.error(error);
        }

    } else {
        return "";
    }
}

exports.half = function (date) {
    if (date != "") {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    } else {
        return "";
    }
}

exports.currentDueDateBilling = function () {

    var billingDate = 13;

    var year = new Date().getFullYear().toString();
    var month = (new Date().getMonth() + 2).toString().padStart(2, 0);

    if (new Date().getDate() < billingDate) {
        month = month - 1;
    }

    var date = year + '-' + month + '-' + billingDate.toString()
    return date;
}