/** init controller */
const users = require('../controller/usersapp')
const sip = require('../controller/sipdata')
const sessions = require('../controller/sessions')
const migrate = require('../controller/datamigrate')
const terminal = require('../controller/terminaldata')
const lembaga = require('../controller/lembaga')
const serialNumber = require('../controller/appSerialData')
const configuration = require('../controller/configuration')
const responseMessage = require('../controller/responseMessagge')
const vpnAccount = require('../controller/vpnAccount')
const vpnSesion = require('../controller/sessionsvpn')
const kirimemailsatuan = require('../controller/kirimemailsatuan')
const cekBankIdStatusLogin = require('../controller/cekBankIdStatusLogin')
const oldlembaga = require('../controller/oldlembaga')

module.exports = function (app, upload) {
    try {
        /** user apps */
        app.get('/users/get/all', users.selectAll);
        app.get('/users/get/all/admin', users.selectAllAdmin);
        app.get('/users/get/all/notpaid', users.selectAllNotPaid);
        app.get('/users/get/admin/notpaid', users.selectAllNotPaidAdmin);
        app.get('/users/get/blocked', users.selectAllBlocked);
        app.get('/users/get/notactive', users.selectAllNotActive);
        app.get('/users/plain/get/admin/all', users.getAllAdminUser);
        app.get('/users/plain/get/all', users.getAllUser);
        app.get('/users/plain/get/admin/duedatepass', users.getAllAdminDueDatePass);
        app.post('/users/insert', users.update);
        app.post('/users/delete', users.delete);
        // app.post('/users/insert2', users.update2);
        // app.get('/users/get/all2', users.selectAll2);

        /** sip routes */
        app.get('/get/sip/bpr/sb/sync/duedate', sip.syncduedate);
        app.get('/get/sip/bpr/sb/sync/duedate/old', sip.syncduedateold);
        app.get('/get/sip/bpr/sb/getnotlisted', sip.getallbprsb);

        /** app sessions */
        app.get('/sessions/get/today', sessions.selectAll);
        app.get('/sessions/get/today/exception', sessions.selectException);
        app.get('/sessions/get/today/failed', sessions.selectTodayFailed);

        /** terminals */
        app.get('/terminals/get/all', terminal.selectAll2)
        app.post('/terminals/delete', terminal.delete)
        app.get('/terminals/get/all/active', terminal.selectActive)
        app.get('/terminals/get/all/notactive', terminal.selectNotactive)

        /** lembaga */
        app.get('/lembaga/get/all', lembaga.selectAll)
        app.post('/lembaga/delete', lembaga.delete)
        app.post('/lembaga/update', lembaga.updateData)
        app.get('/lembaga/get/all/active', lembaga.selectActive)
        app.get('/lembaga/get/all/notactive', lembaga.selectNotActive)
        app.get('/lembaga/get/all/notpaid', lembaga.selectNotPaid)
        app.post('/lembaga/demo/insert', lembaga.insertLembaga)
        app.post('/lembaga/demo/insert/2', lembaga.insertLembaga2)
        app.post('/lembaga/check/bankid', lembaga.checkBankIDExist)
        app.get('/lembaga/get/activecounter', lembaga.selectDaysActiveBPR)
        app.get('/lembaga/get/active/month', lembaga.selectMonthActiveBPR)
        app.get('/lembaga/get/notactive/amonthlater', lembaga.selectLastLoginAMonthLater)

        /** lembaga Old */
        app.get('/oldlembaga/get/all', oldlembaga.selectAll)
        app.get('/oldlembaga/get/all/notpaid', oldlembaga.selectNotPaid)


        /** migrate database */
        app.get('/migrate/lembaga', migrate.lembaga);
        app.get('/migrate/terminal', migrate.terminal);
        app.get('/migrate/userapps', migrate.userapps);
        app.get('/migrate/serial', migrate.serialnumbers);


        /** Service Serial Number */
        app.get('/serialNumber/get/all', serialNumber.selectAll);
        app.get('/serialNumber/get/bankid', serialNumber.selectByBankId);
        app.post('/serialNumber/reset', serialNumber.resetSerialNumberBySerial);
        app.post('/serialNumber/delete', serialNumber.deleteSerialNumber);

        /** configuration */
        app.get('/configuration/get/all', configuration.selectAll);
        app.get('/configuration/get/sshconfig', configuration.getSSHConfig);
        app.get('/configuration/get/vpnconfig', configuration.getVPNConfig);

        /** response message */
        app.get('/responseMessage/get/all', responseMessage.selectAll);

        /** vpn account */
        app.get('/vpnAccount/get/all', vpnAccount.selectAll);
        app.get('/vpnAccount/get/last', vpnAccount.selectLast);

        /** vpn account */
        app.get('/sessionsvpn/get/reqtime', vpnSesion.selectByReqTime);
        app.get('/sessionsvpn/update', vpnSesion.updateTest);
        // app.get('/vpnAccount/get/last', vpnAccount.selectLast);


        /** kirim email satuan */
        app.post('/kirim/email/satuan', kirimemailsatuan.kirim);

        app.post('/cekBankIdStatus/bankId', cekBankIdStatusLogin.selectBankIdStatus);

        app.post('/cek', kirimemailsatuan.kirim);

        /** get data sip */
        app.get('/sip/get/bpr/data', sip.getbprdata);
        app.get('/sip/get/bpr/notpaid', sip.getBprNotPaid);
        app.get('/sip/get/bpr/notpaid/onemonth', sip.getNotPaidOneMonth);
        app.post('/sip/get/bpr/name', sip.getbprbyname);

        scheduler()

    } catch (error) {
        // log.error(error);
    }
}

function scheduler() {
    console.log("--init scheduler--")
    setInterval(() => {
        console.log(`----doing schedule at ${new Date()}----`)
        getSyncDueDate("/get/sip/bpr/sb/sync/duedate")
        // getSyncDueDate("/get/sip/bpr/sb/sync/duedate/old")
    }, 5 * 60 * 1000);
}

function getSyncDueDate(path) {
    const http = require("http");

    console.log('req path: ' + path)

    const options = {
        "method": "GET",
        "hostname": "localhost",
        "port": "16006",
        "path": path
    };

    const req = http.request(options, function (res) {
        const chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            console.log(path + ' Sync Due Date Resp: ')
            const body = Buffer.concat(chunks);
            console.log(body.toString());
            console.log('END: ' + path)
        });
    });
    req.end();
}