import * as button from './buttonListener.js'

$(document).ready(() => {
    $('#summernote').summernote({
        maximumImageFileSize: 500 * 1024, // 500 KB
        callbacks: {
            onImageUploadError: function (msg) {
                alert(msg + ' (1 MB)');
            }
        },
        placeholder: '',
        tabsize: 2,
        height: 100
    });

    button.buttonListener()
})