// export function show(message, footer) {
//     $('#modal-data').modal('show')
//     $('.modal-body').html(message)
//     $('.modal-footer').html(footer)
// }

export function show(message) {
    console.log(message)
    $('#modal-plain').modal('show')
    $('.modal-body').html(message)
    $('.modal-footer').html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>`)
}

export function showConfirm(message, footer) {
    console.log(message)
    $('#modal-plain').modal('show')
    $('.modal-body').html(message)
    $('.modal-footer').html(footer)
}

export function hide() {
    $('#modal-data').modal('hide')
}