export function terminals() {
    try {
        /** 
         *  terminals-get-all" 
            terminals-get-active" 
            terminals-get-notactive"
         */
        $('#terminal-btn-syncduedate').click(() => {

            $('#terminal-btn-syncduedate').html('...Sending Synchronize Due Date to SIP')
            $('#terminal-btn-syncduedate').prop('disabled', true);

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "/get/sip/bpr/sb/sync/duedate",
                "method": "GET",
                "headers": {}
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                $('#terminal-btn-syncduedate').html(response)
                setTimeout(() => {
                    $('#terminal-btn-syncduedate').html('Synchronize Due Date to SIP')
                    $('#terminal-btn-syncduedate').prop('disabled', false);
                }, 10000);
            });

        })

        $('#terminal-btn-syncduedate-old').click(() => {
            $('#terminal-btn-syncduedate-old').html('...Sending Synchronize Due Date to SIP | OLD')
            $('#terminal-btn-syncduedate-old').prop('disabled', true);

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "/get/sip/bpr/sb/sync/duedate/old",
                "method": "GET",
                "headers": {}
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                $('#terminal-btn-syncduedate-old').html(response)
                setTimeout(() => {
                    $('#terminal-btn-syncduedate-old').html('Synchronize Due Date to SIP | OLD')
                    $('#terminal-btn-syncduedate-old').prop('disabled', false);
                }, 10000);
            });
        })
    } catch (error) {
        alert(error)
    }
}

export function users() {
    try {
        $('')
    } catch (error) {
        alert(error);
    }
}

export function lembaga() {
    try {

    } catch (error) {
        alert(error);
    }
}

export function sessions() {
    try {
        /** sessions-get-today"
            sessions-get-exceptions" */

        $('#sessions-get-today').click(() => {

        })

        $('#sessions-get-exceptions').click(() => {

        })

    } catch (error) {
        alert(error);
    }
}