import { html, render } from 'https://unpkg.com/lit-html?module';
import * as buttonListeners from './buttonListeners.js';
import * as users from './table/users.js';
import * as sessions from './table/sessions.js';
import * as lembaga from './table/lembaga.js';
import * as terminals from './table/terminals.js';
import * as serials from './table/serials.js';
import * as oldlembaga from './table/oldlembaga.js';
import * as sip from './table/sip.js';
import { tableSip } from './table/sip.js';

$(document).ready(() => {
    // lock()

    checkSession();

    if ($(window).width() < 800) {
        $("#lembaga-list-menu").hide();
    } else {
        $("#lembaga-list-menu").show();
    }

    $(window).resize(function () {
        if ($(window).width() < 800) {
            $("#lembaga-list-menu").hide();
        } else {
            $("#lembaga-list-menu").show();
        }
    });

    users.generate_table('/users/get/all');
    sessions.generate_table('/sessions/get/today');
    terminals.generate_table('/terminals/get/all');
    lembaga.generate_table('/lembaga/get/all');
    serials.generate_table('/serialNumber/get/all');
    oldlembaga.generate_table('/oldlembaga/get/all');
    sip.generate_table('/sip/get/bpr/notpaid/onemonth');
    
    buttonListeners.lembaga();
    buttonListeners.sessions();
    buttonListeners.terminals();
    buttonListeners.users();
})

function checkSession() {
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "/support/check/session",
        "method": "GET"
    };

    $.ajax(settings).done(function (response) {
        if (response != 'success') {
            window.location.href = response.redirect;
        }
    });
}

function lock() {
    $('.tab-content').hide();
    $('#modal-data').modal({
        backdrop: 'static',
        keyboard: false
    });

    $('.modal-header').html(`Input Key`);
    $('.modal-body').html(`
    
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-default">KEY</span>
        </div>
        <input id="key-input" type="password" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
    </div>
    
    `);
    $('.modal-footer').html(`
        <button id="open-key" type="button" class="btn btn-outline-primary">Submit Key</button>
    `);

    $('#open-key').click(function () {
        if ($('#key-input').val() == 'passwordnyah') {
            $('.tab-content').show('slow');
            $('#modal-data').modal('hide');
        } else {
            alert('key not found')
        }
    });
}