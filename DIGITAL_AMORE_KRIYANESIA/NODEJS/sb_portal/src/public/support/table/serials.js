import { html, render } from 'https://unpkg.com/lit-html?module';
import * as modal from '../modal.js'

export var tableSerials;

export function generate_table(path) {

    //#region 
    /*
        "serial_number": "qwF90mTsxWpDIk6b5bC6FL5XhZ8hDqUqyFnn2EfjUc4=",
        "generate_number": null,
        "bank_id": "92",
        "used_time": null,
        "mac_address": "F4-4D-30-16-32-6A",
        "status": 1,
        "info": null,
        "additional_data": null,
        "terminal_id": "1623",
        "cr_time": null,
        "bank_name": "PT BPR MUSI ARTHA SURYA",
     */
    //#endregion

    try {
        tableSerials = $('#table-serial').DataTable({
            scrollX: true,
            scrollCollapse: true,
            autoWidth: false,
            paging: true,
            ajax: {
                "url": path,
                "dataSrc": ""
            },
            columns: [
                { "data": "button", "title": "", "width": "50px" },
                { "data": "bank_id", "title": "Bank ID", "width": "100px" },
                { "data": "bank_name", "title": "Bank Name", "width": "200px" },
                { "data": "serial_number", "title": "Serial Number", "width": "400px" },
                { "data": "mac_address", "title": "Mac Address", "width": "100px" },
                { "data": "terminal_id", "title": "Terminal ID", "width": "100px" },
                { "data": "status", "title": "Status", "width": "100px" }
            ],
            order: [[0, 'desc']],
            initComplete: function (settings, json) {
                tableSerials.ajax.reload();
            }
        });

        $('#table-serials').on('draw.dt', function () {
            // tableSessions.ajax.reload();
        });

        $('#a-menu-serials').click(function () {
            tableSerials.ajax.reload();
        });


        $('#table-serial tbody').on('click', 'button', function () {
            var data = tableSerials.row($(this).parents('tr')).data();
            var spanstyle = `style="width:200px"`
            var input_data = [
                ["bank_id", data.bank_id, "disabled"],
                ["bank_name", data.bank_name, "disabled"],
                ["serial_number", data.serial_number, "disabled"],
                ["mac_address", data.mac_address, "disabled"],
                ["terminal_id", data.terminal_id, "disabled"],
                ["status", data.status, "disabled"]
            ]

            modal.showCustom(
                html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                html`
                    <div class="container">
                        ${input_data.map(element => html`
                        <div class="form-group row">
                            <label for="terminal-input-${element[0].replace(" ", " _")}"
                                class="col-sm-3 col-form-label">${element[0].replace("_", " ")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="terminal-input-${element[0].replace(" ", " _")}"
                                    value="${element[1]}" ?disabled=${element[2]}>
                            </div>
                        </div>
                        `)}
                    </div>
                `,
                html`
                    <button id="serial-reset-confirm" type="button" class="btn btn-outline-primary">Reset</button>
                    <button id="serial-delete" type="button" class="btn btn-outline-danger">Delete</button>
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>`
            )

            $('#serial-reset-confirm').click(function () {
                var serial_number = $('#terminal-input-serial_number').val();

                modal.showCustom(
                    html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                    html`
                    <p>Anda yakin akan mereset serial number ini? <br>
                        ${$('#terminal-input-bank_name').val()}<br>
                        ${serial_number}
                    </p>`,
                    html`
                    <button id="serial-reset-yes" type="button" class="btn btn-outline-primary">Ya</button>
                    <button id="serial-reset-no" type="button" class="btn btn-outline-danger">Tidak</button>`
                )

                $('#serial-reset-yes').click(function () {
                    const settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "/serialNumber/reset",
                        "method": "POST",
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "processData": false,
                        "data": JSON.stringify({
                            serial_number: serial_number
                        })
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        tableSerials.ajax.reload();
                        modal.showCustom(
                            html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                            html`<p>Serial Reseted!</p>`,
                            html``
                        )
                    });
                })


            })

            $('#serial-delete').click(function () {
                var serial_number = $('#terminal-input-serial_number').val();

                modal.showCustom(
                    html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                    html`
                    <p>Anda yakin akan menghapus serial number ini? <br>
                        ${$('#terminal-input-bank_name').val()}<br>
                        ${serial_number}
                    </p>`,
                    html`
                    <button id="serial-delete-yes" type="button" class="btn btn-outline-primary">Ya</button>
                    <button id="serial-delete-no" type="button" class="btn btn-outline-danger">Tidak</button>`
                )

                $('#serial-delete-yes').click(function () {
                    const settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "/serialNumber/delete",
                        "method": "POST",
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "processData": false,
                        "data": JSON.stringify({
                            serial_number: serial_number
                        })
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        tableSerials.ajax.reload();
                        modal.showCustom(
                            html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                            html`<p>Serial Deleted</p>`,
                            html``
                        )
                    });
                })
            })
        });

    } catch (error) {
        alert(error);
    }
}