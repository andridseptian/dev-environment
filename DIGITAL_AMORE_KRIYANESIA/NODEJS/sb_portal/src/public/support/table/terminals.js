import { html, render } from 'https://unpkg.com/lit-html?module';
import * as modal from '../modal.js'

export var tableTerminals;

export function generate_table(path) {
    try {
        tableTerminals = $('#table-terminals').DataTable({
            scrollX: true,
            scrollCollapse: true,
            autoWidth: false,
            paging: true,
            ajax: {
                "url": path,
                "dataSrc": ""
            },
            columns: [
                { "data": "button", "title": "", "width": "50px" },
                { "data": "bank_id", "title": "Bank ID", "width": "50px" },
                { "data": "bank_name", "title": "Bank Name", "width": "300px" },
                { "data": "terminal_id", "title": "Terminal ID", "width": "100px" },
                { "data": "cr_time", "title": "Create Time", "width": "200px" },
                { "data": "mac_address", "title": "Mac Address", "width": "150px" },
                { "data": "mod_time", "title": "Modif Time", "width": "100px" },
                { "data": "status", "title": "Status", "width": "50px" },
                { "data": "host_name", "title": "Host Name", "width": "200px" },
                { "data": "processor_id", "title": "Processor ID", "width": "200px" },
                { "data": "processor_name", "title": "Processor Name", "width": "300px" },
                { "data": "serial_windows", "title": "Serial Windows", "width": "200px" },
                { "data": "uuid", "title": "UUID", "width": "300px" }
            ],
            order: [[0, 'desc']],
            initComplete: function (settings, json) {
                tableTerminals.ajax.reload();
                // alert('done');
                $('#terminal-get-all').click(() => { alert('terminals') })
                $('#terminal-get-active').click(() => { tableTerminals.ajax.url('/terminals/get/all/active').load() })
                $('#terminal-get-notactive').click(() => { tableTerminals.ajax.url('/terminals/get/all/notactive').load() })
            }
        });

        // <button type="button" class="terminal-edit-btn btn btn-primary btn-sm"><img style="width:12px" src="/assets/img/icon/edit.png"></button>

        $('#table-terminals').on('draw.dt', function () {
            // tableTerminals.ajax.reload();
        });

        $('#a-menu-terminals').click(function () {
            tableTerminals.ajax.reload();
        });

        $('#table-terminals tbody').on('click', 'button', function () {
            var data = tableTerminals.row($(this).parents('tr')).data();
            // console.log(JSON.stringify(data));

            var spanstyle = `style="width:200px"`
            var input_data = [
                ["bank_id", data.bank_id, "disabled"],
                ["bank_name", data.bank_name, "disabled"],
                ["terminal_id", data.terminal_id, "disabled"],
                ["cr_time", data.cr_time.replace("T", " ").replace("Z", " "), "disabled"],
                ["mac_address", data.mac_address],
                ["status", data.status],
                ["host_name", data.host_name],
                ["processor_id", data.processor_id],
                ["processor_name", data.processor_name],
                ["serial_windows", data.serial_windows],
                ["uuid", data.uuid]
            ]

            modal.showCustom(
                html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                html`
                    <div class="container">
                        ${input_data.map(element => html`
                        <div class="form-group row">
                            <label for="terminal-input-${element[0].replace(" ", " _")}"
                                class="col-sm-3 col-form-label">${element[0].replace("_", " ")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="terminal-input-${element[0].replace(" ", " _")}"
                                    value="${element[1]}" ?disabled=${element[2]}>
                            </div>
                        </div>
                        `)}
                    </div>
                `,
                html`
                    <button id="terminal-edit-confirm" type="button" class="btn btn-outline-primary" data-dismiss="modal">Edit</button>
                    <button id="terminal-delete" type="button" class="btn btn-outline-danger" data-dismiss="modal">Delete</button>
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>`
            )

            $('#terminal-edit-confirm').click(function () {
                alert('Not Available Right Now')
            })

            $('#terminal-delete').click(function () {

                var terminal_id = $('#terminal-input-terminal_id').val()

                modal.showCustom(
                    html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                    html`
                    <p>Anda yakin akan menghapus Terminal ini? <br>
                        Bank ID : ${$('#terminal-input-bank_id').val()}<br>
                        Bank Name : ${$('#terminal-input-bank_name').val()}<br>
                        Terminal ID : ${$('#terminal-input-terminal_id').val()}<br>
                        Mac Address : ${$('#terminal-input-mac_address').val()}<br>
                        Processir ID : ${$('#terminal-input-processor_id').val()}<br>
                        Processor Name : ${$('#terminal-input-processor_name').val()}<br>
                    </p>`,
                    html`
                    <button id="terminal-delete-yes" type="button" class="btn btn-outline-primary">Ya</button>
                    <button id="terminal-delete-no" type="button" data-dismiss="modal" class="btn btn-outline-danger">Tidak</button>`
                )

                $('#terminal-delete-yes').click(function () {
                    const settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "/terminals/delete",
                        "method": "POST",
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "processData": false,
                        "data": JSON.stringify({
                            terminal_id: terminal_id
                        })
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        tableTerminals.ajax.reload();
                        modal.showCustom(
                            html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                            html`<p>${response}</p>`,
                            html``
                        )
                    });
                })
            })

        })
    } catch (error) {
        alert(error);
    }
}