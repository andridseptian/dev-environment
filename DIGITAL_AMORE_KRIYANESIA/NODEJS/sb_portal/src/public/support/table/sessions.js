import { html, render } from 'https://unpkg.com/lit-html?module';

export var tableSessions;

export function generate_table(path) {
    try {
        setTimeout(() => {
            tableSessions = $('#table-sessions').DataTable({
                scrollX: true,
                scrollCollapse: true,
                autoWidth: false,
                paging: true,
                ajax: {
                    "url": path,
                    "dataSrc": ""
                },
                columns: [
                    { "data": "session_id", "title": "Sessions ID", "width": "10px" },
                    { "data": "info", "title": "Info", "width": "30px" },
                    { "data": "req_time", "title": "Req Time", "width": "100px" },
                    { "data": "res_time", "title": "Res Time", "width": "100px" },
                    { "data": "path", "title": "Path", "width": "200px" },
                    { "data": "rc", "title": "response code", "width": "30px" },
                    { "data": "rm", "title": "response message", "width": "400px" },
                    { "data": "req_body", "title": "req body", "width": "800px" },
                    { "data": "res_body", "title": "res body", "width": "800px" },
                    { "data": "exception", "title": "exception", "width": "200px" }
                ],
                order: [[0, 'desc']],
                initComplete: function (settings, json) {
                    tableSessions.ajax.reload();
                    render(html`
                        <button id="sessions-get-today" class="dropdown-item">Today Sessions</button>
                        <button id="sessions-get-failed" class="dropdown-item">Today Failed Sessoins</button>
                        <button id="sessions-get-exceptions" class="dropdown-item">Exception Sessoins</button>
                    `, document.getElementById('sessions-drop-button'))

                    $('#sessions-get-today').click(() => { tableSessions.clear().draw(); tableSessions.ajax.url('/sessions/get/today').load() })
                    $('#sessions-get-failed').click(() => { tableSessions.clear().draw(); tableSessions.ajax.url('/sessions/get/today/failed').load() })
                    $('#sessions-get-exceptions').click(() => { tableSessions.clear().draw(); tableSessions.ajax.url('/sessions/get/today/exception').load() })

                }
            });

            $('#table-sessions').on('draw.dt', function () {
                // tableSessions.ajax.reload();
            });

            $('#a-menu-sessions').click(function () {
                // tableSessions.ajax.reload();
            });

        }, 500);
    } catch (error) {
        alert(error);
    }
}