import { html, render } from 'https://unpkg.com/lit-html?module';
import * as modal from '../modal.js'

export var tableOldLembaga;

export function generate_table(path) {
    try {
        setTimeout(() => {
            tableOldLembaga = $('#table-oldlembaga').DataTable({
                scrollX: true,
                scrollCollapse: true,
                autoWidth: false,
                paging: true,
                ajax: {
                    "url": path,
                    "dataSrc": ""
                },
                columns: [
                    { "data": "invoice", "title": "Invoice", "width": "100px" },
                    { "data": "bank_id", "title": "Bank ID", "width": "50px" },
                    { "data": "bank_name", "title": "Bank Name", "width": "200px" },
                    { "data": "cr_time", "title": "Created Time", "width": "200px" },
                    { "data": "mod_time", "title": "Modif Time", "width": "200px" },
                    { "data": "duedate_time", "title": "Due Date Time", "width": "200px" },
                    { "data": "name", "title": "Admin Name", "width": "200px" },
                    { "data": "no_hp", "title": "Admin Number", "width": "200px" },
                ],
                order: [[0, 'desc']],
                initComplete: function (settings, json) {
                    tableOldLembaga.ajax.reload();
                    /** lembaga-get-all"
                        lembaga-get-active"
                        lembaga-get-notactive"
                    */
                    $('#oldlembaga-get-all').click(() => { tableOldLembaga.clear().draw(); tableOldLembaga.ajax.url('/oldlembaga/get/all').load() })
                    $('#oldlembaga-get-notpaid').click(() => { tableOldLembaga.clear().draw(); tableOldLembaga.ajax.url('/oldlembaga/get/all/notpaid').load() })
                },
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: 'refresh',
                        action: function (e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }, 'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

        }, 500);

    } catch (error) {
        alert(error);
    }
}

export function getBankIDDetail() {
    try {

    } catch (error) {
        alert(error);
    }
}