import { html, render } from 'https://unpkg.com/lit-html?module';
import * as modal from '../modal.js'

export var tableSip;

export function generate_table(path) {

    /* 
     {
        "id": "429",
        "name": "BANK KLATEN",
        "address": "Jl.VETERAN NO 140 KLATEN",
        "telp": "0272322161",
        "fax": "0272321531",
        "email": "bankklaten@ymail.com",
        "website": "http://bankklaten.co.id",
        "jenis_bank": "BPR",
        "bpr": "429",
        "order_date": "2020-05-19T02:26:40.000Z",
        "due_date": "2020-11-19T17:00:00.000Z",
        "status": "UNPAID",
        "order_number": "20200519092640429",
        "payment_code": null,
        "count": "1",
        "invoice": "<a href=\"https://perbarindo.org/services/invoice/invoice_bypass?key=61d90eb6c09b48cf5802b823eac08980&id_bpr=429\" target=\"_blank\">Open Invoice</button>"
    }
    */


    try {
        setTimeout(() => {
            tableSip = $('#table-sip').DataTable({
                scrollX: true,
                scrollCollapse: true,
                autoWidth: false,
                paging: true,
                ajax: {
                    "url": path,
                    "dataSrc": ""
                },
                columns: [
                    { "data": "invoice", "title": "Invoice", "width": "100px" },
                    { "data": "id", "title": "Bank ID", "width": "50px" },
                    { "data": "name", "title": "Bank Name", "width": "200px" },
                    { "data": "address", "title": "Alamat", "width": "200px" },
                    { "data": "telp", "title": "Telp", "width": "150px" },
                    { "data": "fax", "title": "Fax", "width": "150px" },
                    { "data": "email", "title": "Email", "width": "200px" },
                    { "data": "website", "title": "Website", "width": "200px" },
                    { "data": "jenis_bank", "title": "Jenis Bank", "width": "100px" },
                    { "data": "bpr", "title": "BPR", "width": "50px" },
                    { "data": "order_date", "title": "Order Date", "width": "200px" },
                    { "data": "due_date", "title": "Due Date", "width": "200px" },
                    { "data": "status", "title": "Status", "width": "150px" },
                    { "data": "order_number", "title": "Order Number", "width": "150px" },
                    { "data": "payment_code", "title": "Payment Code", "width": "150px" },
                    { "data": "count", "title": "Jumlah Tagihan", "width": "100px" }
                ],
                order: [[1, 'desc']],
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: 'refresh',
                        action: function (e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }, 'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                initComplete: function (settings, json) {
                    tableSip.ajax.reload();

                    var dropdown = [
                        ["sip-get-notpaid-onemonth", "BPR NOT PAID (ONE MONTH)", "/sip/get/bpr/notpaid/onemonth"],
                        ["sip-get-notpaid", "BPR NOT PAID", "/sip/get/bpr/notpaid"],
                    ]

                    render(html`
                    ${dropdown.map((item) => html`
                    <button id="${item[0]}" class="dropdown-item">${item[1]}</button>
                    `)}
                    `, $('#dropdown-sip-list')[0])

                    dropdown.forEach(item => {
                        $(`#${item[0]}`).click(() => {
                            tableSip.clear()
                            tableSip.ajax.url(`${item[2]}`).load()
                            $('#sip-table-tittle').html(`${item[1]}`)
                        })
                    });

              }
            });
        }, 500);


    } catch (error) {
        alert(error);
    }
}