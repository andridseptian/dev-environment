import { html, render } from 'https://unpkg.com/lit-html?module';
import * as modal from '../modal.js'

export var tableUsers;

export function generate_table(path) {
    try {
        tableUsers = $('#table-users').DataTable({
            scrollX: true,
            scrollCollapse: true,
            autoWidth: false,
            paging: true,
            ajax: {
                "url": path,
                "dataSrc": ""
            },
            columns: [
                { "data": "button", "title": " ", "width": "50px" },
                { "data": "invoice", "title": "Invoice", "width": "50px" },
                { "data": "bank_id", "title": "Bank ID", "width": "50px" },
                { "data": "bank_name", "title": "Bank Name", "width": "200px" },
                { "data": "due_date", "title": "Due Date", "width": "200px" },
                { "data": "user_id", "title": "User ID", "width": "80px" },
                { "data": "username.dec", "title": "Username", "width": "200px" },
                { "data": "password.enc", "title": "Password", "width": "200px" },
                { "data": "user_type", "title": "Privilege", "width": "50px" },
                { "data": "nik.dec", "title": "NIK", "width": "100px" },
                { "data": "email", "title": "Email", "width": "200px" },
                { "data": "phone_number", "title": "Phone Number", "width": "100px" },
                { "data": "expired_password", "title": "Expired Date", "width": "200px" },
                { "data": "status", "title": "Status", "width": "50px" },
                { "data": "status_login", "title": "Login Status", "width": "50px" },
                { "data": "terminal_login", "title": "Terminal Login", "width": "50px" },
                { "data": "fail_login", "title": "Fail Login", "width": "50px" }
            ],
            order: [[0, 'desc']],
            initComplete: function (settings, json) {
                tableUsers.ajax.reload();

                var dropdown = [
                    ["user-get-all", "All Users", "/users/get/all"],
                    ["user-get-all-admin", "All Admin", "/users/get/all/admin"],
                    ["user-get-not-paid-all", "Not Paid User", "/users/get/all/notpaid"],
                    ["user-get-not-paid-admin", "Not Paid Admin", "/users/get/admin/notpaid"],
                    ["user-get-not-paid-admin", "Test Aja", "/users/get/admin/notpaid"],
                ]

                render(html`
                ${dropdown.map((item) => html`
                <button id="${item[0]}" class="dropdown-item">${item[1]}</button>
                `)}
                `, $('#dropdown-user-list')[0])

                dropdown.forEach(item => {
                    $(`#${item[0]}`).click(() => { tableUsers.ajax.url(`${item[2]}`).load() })
                });

                /* 
                <button id="users-get-all" class="dropdown-item">All Users</button>
                <button id="users-get-all-admin" class="dropdown-item">All Admin</button>
                <button id="users-get-not-paid-all" class="dropdown-item">Not Paid User</button>
                <button id="users-get-not-paid-admin" class="dropdown-item">Not Paid Admin</button>
                */

                // $('#users-get-all').click(() => { tableUsers.ajax.url('/users/get/all').load() })
                // $('#users-get-all-admin').click(() => { tableUsers.ajax.url('/users/get/all/admin').load() })
                // $('#users-get-not-paid-all').click(() => { tableUsers.ajax.url('/users/get/all/notpaid').load() })
                // $('#users-get-not-paid-admin').click(() => { tableUsers.ajax.url('/users/get/admin/notpaid').load() })

            },
            dom: 'Bfrtip',
            buttons: [
                {
                    text: 'refresh',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }, 'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        tableUsers.buttons().container()
            .appendTo('#example_wrapper .col-md-6:eq(0)');

        $('#table-users').on('draw.dt', function () {
            // tableUsers.ajax.reload();
        });

        $('#table-users tbody').on('click', 'button', function () {
            var data = tableUsers.row($(this).parents('tr')).data();
            console.log(JSON.stringify(data));

            var spanstyle = `style="width:200px"`
            var input_data = [
                ["userid", data.user_id, "disabled"],
                ["username", data.username.dec],
                ["password", data.password.dec],
                ["full name", data.fullname],
                ["nik", data.nik.dec],
                ["email", data.email],
                ["no hp", data.phone_number],
                ["status", data.status],
                ["expired date", data.expired_password.replace("T", " ").replace("Z", " ")],
                ["user type", data.user_type],
                ["status login", data.status_login],
                ["fail login", data.fail_login]
            ]
            var fulldata = ``;
            input_data.forEach(element => {
                fulldata += `<div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" ${spanstyle}>${element[0]}</span>
                                </div>
                                <input id="user-input-${element[0].replace(" ", "_")}" type="text" class="form-control" value="${element[1]}" ${element[2]}>
                            </div>`
            });

            $('#modal-data').modal();

            $('.modal-header').html(`
                <h4 class="modal-title">Edit User</h4>
            `);

            $('.modal-body').html(fulldata);

            $('.modal-footer').html(`
                <button id="user-edit-submit" type="button" class="btn btn-outline-warning">edit</button>
                <button id="user-delete-submit" type="button" class="btn btn-outline-danger">delete</button>
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">cancel</button>
            `);

            $('#user-edit-submit').click(function () {
                // table.ajax.reload();

                var reqbody = () => {
                    var data = {};
                    input_data.forEach(element => {
                        data[element[0].replace(" ", "_")] = $(`#user-input-${element[0].replace(" ", "_")}`).val()
                    });
                    return JSON.stringify(data)
                }
                console.log('req body : ' + reqbody())

                try {
                    const settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "/users/insert",
                        "method": "POST",
                        "headers": {
                            "content-type": "application/json"
                        },
                        "processData": false,
                        "data": reqbody()
                    };

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        $('.modal-body').html(response);
                        $('.modal-footer').html(``);
                        $('.modal-header').html(`<button type="button" class="close" data-dismiss="modal">&times;</button>`);
                        tableUsers.ajax.reload();
                    });
                } catch (error) {
                    alert(error)
                }
            });

            $('#user-delete-submit').click(function () {
                // table.ajax.reload();

                modal.showCustom(
                    html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                    html`
                        User ID: ${data.user_id} <br>
                        Username: ${data.username.dec} <br>
                        Fullname: ${data.fullname} <br>
                        Anda yakin akan menghapus user ini?`,
                    html`
                        <button id="user-delete-confirm" type="button" class="btn btn-outline-danger">Ya</button>
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tidak</button>`
                )


                $('#user-delete-confirm').click(function () {
                    try {
                        const settings = {
                            "async": true,
                            "crossDomain": true,
                            "url": "/users/delete",
                            "method": "POST",
                            "headers": {
                                "content-type": "application/json"
                            },
                            "processData": false,
                            "data": JSON.stringify({
                                user_id: data.user_id
                            }),
                            "error": function (error) {
                                // alert(error.statusText);
                                modal.showCustom(
                                    html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                                    html`Failed: ${error.statusText}`,
                                    html``
                                )
                            }
                        };

                        $.ajax(settings).done(function (response) {
                            console.log(response);
                            modal.showCustom(
                                html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                                html`<p>${response}</p>`,
                                html``
                            )
                            tableUsers.ajax.reload();
                        });
                    } catch (error) {
                        alert(error)
                    }
                })

            });

        });

        $('#a-menu-users').click(function () {
            tableUsers.ajax.reload();
        });
    } catch (error) {
        alert(error);
    }
}