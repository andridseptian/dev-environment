import { html, render } from 'https://unpkg.com/lit-html?module';
import * as modal from '../modal.js'

export var tableLembaga;

export function generate_table(path) {
    try {
        setTimeout(() => {
            tableLembaga = $('#table-lembaga').DataTable({
                scrollX: true,
                scrollCollapse: true,
                autoWidth: false,
                paging: true,
                ajax: {
                    "url": path,
                    "dataSrc": ""
                },
                columns: [
                    { "data": "button", "title": "", "width": "50px" },
                    { "data": "invoice", "title": "Invoice", "width": "100px" },
                    { "data": "bank_id", "title": "Bank ID", "width": "50px" },
                    { "data": "bank_name", "title": "Bank Name", "width": "200px" },
                    { "data": "cr_time", "title": "Created Time", "width": "200px" },
                    { "data": "mod_time", "title": "Modif Time", "width": "200px" },
                    { "data": "status", "title": "Status", "width": "50px" },
                    { "data": "due_date", "title": "Due Date Time", "width": "200px" },
                    { "data": "last_login_time", "title": "Last Login Time", "width": "200px" },
                    { "data": "status_login", "title": "Status Login", "width": "100px" },
                    { "data": "info", "title": "Info", "width": "500px" }
                ],
                order: [[0, 'desc']],
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: 'refresh',
                        action: function (e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }, 'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                initComplete: function (settings, json) {
                    tableLembaga.ajax.reload();
                    /** lembaga-get-all"
                        lembaga-get-active"
                        lembaga-get-notactive"
                    */

                    var dropdown = [
                        ["lembaga-get-all", "All Lembaga", "/lembaga/get/all"],
                        ["lembaga-get-active", "Active Lembaga", "/lembaga/get/all/active"],
                        ["lembaga-get-notactive", "Not Active Lembaga", "/lembaga/get/all/notactive"],
                        ["lembaga-get-notpaid", "Not Paid Lembaga", "/lembaga/get/all/notpaid"],
                        ["lembaga-get-notactive-amonth", "Not Active Lembaga (A Month)", "/lembaga/get/notactive/amonthlater"],
                    ]

                    render(html`
                    ${dropdown.map((item) => html`
                    <button id="${item[0]}" class="dropdown-item">${item[1]}</button>
                    `)}
                    `, $('#dropdown-lembaga-list')[0])

                    dropdown.forEach(item => {
                        $(`#${item[0]}`).click(() => {
                            tableLembaga.clear()
                            tableLembaga.ajax.url(`${item[2]}`).load()
                            $('#lembaga-table-tittle').html(`${item[1]}`)
                        })
                    });

                    // $('#lembaga-get-all').click(() => { tableLembaga.clear().draw(); tableLembaga.ajax.url('/lembaga/get/all').load() })
                    // $('#lembaga-get-active').click(() => { tableLembaga.clear().draw(); tableLembaga.ajax.url('/lembaga/get/all/active').load() })
                    // $('#lembaga-get-notactive').click(() => { tableLembaga.clear().draw(); tableLembaga.ajax.url('/lembaga/get/all/notactive').load() })
                    // $('#lembaga-get-notpaid').click(() => { tableLembaga.clear().draw(); tableLembaga.ajax.url('/lembaga/get/all/notpaid').load() })
                }
            });

            $('#table-lembaga tbody').on('click', 'button', function () {
                var data = tableLembaga.row($(this).parents('tr')).data();
                var input_data = [
                    ["bank_id", "text", data.bank_id, "disabled"],
                    ["bank_name", "text", data.bank_name],
                    ["status", "text", data.status],
                    ["cr_time", "text", (data.cr_time ? data.cr_time : "NOT AVAILABLE").replace("T", " ").replace("Z", " "), "disabled"],
                    ["due_date", "date", (data.due_date ? data.due_date : "NOT AVAILABLE").substring(0, 10)],
                    ["last_login_time", "text", (data.last_login_time ? data.last_login_time : "NOT AVAILABLE").replace("T", " ").replace("Z", " "), "disabled"],
                    ["status_login", "text", data.status_login]
                ]

                modal.showCustom(
                    html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                    html`
                        <div class="container">
                            ${input_data.map(element => html`
                            <div class="form-group row">
                                <label for="terminal-input-${element[0].replace(" ", " _")}"
                                    class="col-sm-3 col-form-label">${element[0].replace("_", " ")}</label>
                                <div class="col-sm-9">
                                    <input type="${element[1]}" class="form-control" id="lembaga-input-${element[0].replace(" ", " _")}"
                                        value="${element[2]}" ?disabled=${element[3]}>
                                </div>
                            </div>
                            `)}
                        </div>
                    `,
                    html`
                        <button id="lembaga-edit-confirm" type="button" class="btn btn-outline-primary">Edit</button>
                        <button id="lembaga-delete" type="button" class="btn btn-outline-danger">Delete</button>
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>`
                )

                $('#lembaga-delete').click(function () {
                    var req_body = JSON.stringify({
                        bank_id: $('#lembaga-input-bank_id').val()
                    });
                    modal.showCustom(
                        html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                        html`
                        <p>Anda yakin akan menghapus Lembaga Ini ini? <br>
                            Bank ID : ${$('#lembaga-input-bank_id').val()}<br>
                            Bank Name: ${$('#lembaga-input-bank_name').val()}<br>
                            Due Date : ${$('#lembaga-input-due_date').val()}<br>
                        </p>`,
                        html`
                        <button id="lembaga-delete-yes" type="button" class="btn btn-outline-primary">Ya</button>
                        <button id="lembaga-delete-no" type="button" data-dismiss="modal" class="btn btn-outline-danger">Tidak</button>`
                    )

                    $('#lembaga-delete-yes').click(function () {
                        console.log("req body: " + req_body)
                        const settings = {
                            "async": true,
                            "crossDomain": true,
                            "url": "/lembaga/delete",
                            "method": "POST",
                            "headers": {
                                "Content-Type": "application/json"
                            },
                            "processData": false,
                            "data": req_body
                        };

                        $.ajax(settings).done(function (response) {
                            console.log(response);
                            tableLembaga.ajax.reload();
                            modal.showCustom(
                                html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                                html`<p>Deleted!</p>`,
                                html``
                            )
                        });
                    })
                })

                $('#lembaga-edit-confirm').click(function () {
                    var req_body = JSON.stringify({
                        bank_id: $('#lembaga-input-bank_id').val(),
                        bank_name: $('#lembaga-input-bank_name').val(),
                        due_date: $('#lembaga-input-due_date').val(),
                        status: $('#lembaga-input-status').val(),
                        status_login: $('#lembaga-input-status_login').val()
                    });
                    modal.showCustom(
                        html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                        html`
                        <p>Anda yakin akan mengedit Lembaga Ini ini? <br>
                            Bank ID : ${$('#lembaga-input-bank_id').val()}<br>
                            Bank Name: ${$('#lembaga-input-bank_name').val()}<br>
                            Due Date : ${$('#lembaga-input-due_date').val()}<br>
                            Status : ${$('#lembaga-input-status').val()}<br>
                            Status Login : ${$('#lembaga-input-status_login').val()}<br>
                        </p>`,
                        html`
                        <button id="lembaga-edit-yes" type="button" class="btn btn-outline-primary">Ya</button>
                        <button id="lembaga-edit-no" type="button" data-dismiss="modal" class="btn btn-outline-danger">Tidak</button>`
                    )

                    $('#lembaga-edit-yes').click(function () {
                        console.log("req body: " + req_body)

                        const settings = {
                            "async": true,
                            "crossDomain": true,
                            "url": "/lembaga/update",
                            "method": "POST",
                            "headers": {
                                "Content-Type": "application/json"
                            },
                            "processData": false,
                            "data": req_body
                        };

                        $.ajax(settings).done(function (response) {
                            console.log(response);
                            tableLembaga.ajax.reload();
                            modal.showCustom(
                                html`<button type="button" class="close" data-dismiss="modal">&times;</button>`,
                                html`<p>Updated!</p>`,
                                html``
                            )
                        });
                    })
                })

            })


        }, 500);

        setTimeout(() => {
            var tableLembagaAktif = $('#table-lembaga-active').DataTable({
                scrollX: true,
                scrollCollapse: true,
                autoWidth: false,
                paging: true,
                ajax: {
                    "url": '/lembaga/get/activecounter',
                    "dataSrc": ""
                },
                columns: [
                    { "data": "bprcount", "title": "Jumlah BPR", "width": "50%" },
                    { "data": "dateactive", "title": "Tanggal Aktif", "width": "50%" }
                ],
                initComplete: function (settings, json) {
                    tableLembagaAktif.ajax.reload();
                    setLembagaActiveChart();
                    setLembagaActiveMonthChart();
                }
            });
        }, 500);


    } catch (error) {
        alert(error);
    }
}

export function getBankIDDetail() {
    try {

    } catch (error) {
        alert(error);
    }
}

export function setLembagaActiveChart() {
    try {
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": "/lembaga/get/activecounter",
            "method": "GET"
        };

        $.ajax(settings).done(function (response) {
            var resBody = response

            var chartlabels = [];
            var chartdata = [];
            resBody.forEach((element, index) => {
                chartlabels.push(element.dateactive)
                chartdata.push(element.bprcount)
                if (index == resBody.length - 1) {
                    var ctx = document.getElementById('lembaga-login-barchart').getContext('2d');
                    var chart = new Chart(ctx, {
                        // The type of chart we want to create
                        type: 'horizontalBar',

                        // The data for our dataset
                        data: {
                            labels: chartlabels,
                            datasets: [{
                                label: 'BPR Active Count',
                                backgroundColor: 'rgba(52, 152, 219,1.0)',
                                // borderColor: 'rgb(255, 99, 132)',
                                lineTension: 0,
                                data: chartdata
                            }]
                        },

                        // Configuration options go here
                        options: {
                            animation: {
                                duration: 1,
                                onComplete: function () {
                                    var chartInstance = this.chart,
                                        ctx = chartInstance.ctx;
                                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'bottom';

                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = dataset.data[index];
                                            ctx.fillText(data, bar._model.x + 15, bar._model.y + 5);
                                        });
                                    });
                                }
                            }
                        }
                    });
                }
            });
        });
    } catch (error) {
        alert(error);
    }
}

export function setLembagaActiveMonthChart() {
    try {
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": "/lembaga/get/active/month",
            "method": "GET"
        };

        $.ajax(settings).done(function (response) {
            var resBody = response

            var chartlabels = [];
            var chartdata = [];
            resBody.forEach((element, index) => {
                chartlabels.push(element.month_name)
                chartdata.push(element.bprcount)
                if (index == resBody.length - 1) {
                    var ctx = document.getElementById('lembaga-login-month-barchart').getContext('2d');
                    var chart = new Chart(ctx, {
                        // The type of chart we want to create
                        type: 'line',

                        // The data for our dataset
                        data: {
                            labels: chartlabels,
                            datasets: [{
                                label: 'BPR Active Month',
                                backgroundColor: 'rgba(52, 152, 219,1.0)',
                                // borderColor: 'rgb(255, 99, 132)',
                                lineTension: 0,
                                data: chartdata
                            }]
                        },

                        // Configuration options go here
                        options: {
                            animation: {
                                duration: 1,
                                onComplete: function () {
                                    var chartInstance = this.chart,
                                        ctx = chartInstance.ctx;
                                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'bottom';

                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = dataset.data[index];
                                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                        });
                                    });
                                }
                            }
                        }
                    });
                }
            });
        });
    } catch (error) {
        alert(error);
    }
}