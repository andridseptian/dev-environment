import { show } from './modal.js'
import { showConfirm } from './modal.js'
import { hide } from './modal.js'

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];

function getAutocomplete(params) {

}

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
$(document).ready(() => {

    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "/sip/get/bpr/data",
        "method": "GET",
        "headers": {}
    };

    $.ajax(settings).done(function (response) {
        var array = response
        var data = [];
        array.forEach(element => {
            data.push(element.name)
        });

        autocomplete(document.getElementById("form-input-bprname"), data);
        // autocomplete(document.getElementById("form-input-bprname-detail"), data);
    });

    $('#check-bank-name').click(() => {

        if ($('#form-input-bprname').val() != "") {
            var reqBody = JSON.stringify({
                name: $('#form-input-bprname').val().replace("BPR ", "").replace("BPRS ", "")
            })

            console.log("request body: " + reqBody);

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "/sip/get/bpr/name",
                "method": "POST",
                "headers": {
                    "content-type": "application/json"
                },
                "processData": false,
                "data": reqBody
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                var array = response;
                if (array.length != 0) {
                    array.forEach(element => {
                        $('#form-input-bprname').val(element.jenis_bank + " " + element.name);
                        $('#form-input-bankid').val(element.id)
                    });
                } else {
                    $('#form-input-bprname').val("BANK ID TIDAK DITEMUKAN");
                }
            });
        } else {
            show("Isi Kolom Nama BPR Terlebih dahulu untuk mengecek data BPR anda")

        }
    })

    $('#check-bank-name-detail').click(() => {

        // var bankname = 

       if ($('#form-input-bprname-detail').val() != "") {
            var reqBody = JSON.stringify({
                name: $('#form-input-bprname-detail').val().replace("BPR ", "").replace("BPRS ", "")
            })

            console.log("request body: " + reqBody);

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "/sip/get/bpr/name",
                "method": "POST",
                "headers": {
                    "content-type": "application/json"
                },
                "processData": false,
                "data": reqBody
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                var array = response;
                if (array.length != 0) {
                    array.forEach(element => {
                        $('#form-input-bprname-detail').val(element.jenis_bank + " " + element.name);
                        $('#form-input-bankid-detail').val(element.id)
                    });
                } else {
                    $('#form-input-bprname-detail').val("BANK ID TIDAK DITEMUKAN");
                }
            });
        } else {
            show("Isi Kolom Nama BPR Terlebih dahulu untuk mengecek data BPR anda")

        }
    })

    $('#form-regist-submit').click(() => {
        //#region data
        /* 
            bank_id
            bank_name
            cr_time
            mod_time
            status
            terminal_count
            due_date
            last_login_time
            info
            additional_data
            status_login
        */
        //#endregion

        if (
            $('#form-input-bankid').val() != "" &&
            $('#form-input-bprname').val() != "" &&
            $('#form-input-registname').val() != "" &&
            $('#form-input-registnik').val() != "" &&
            $('#form-input-registwa').val() != "" &&
            $('#form-input-registemail').val() != ""
        ) {

            var data = `
                --Lembaga--<br>
                Bank ID: <a>${$('#form-input-bankid').val()}</a><br>
                Nama BPR: <a>${$('#form-input-bprname').val()}</a><br>
                --Pendaftar--<br>
                Nama: <a>${$('#form-input-registname').val()}</a><br>
                NIK: <a>${$('#form-input-registnik').val()}</a><br>
                WA: <a>${$('#form-input-registwa').val()}</a><br>
                Email: <a>${$('#form-input-registemail').val()}</a><br>

                <p>Apakah data berikut sudah sesuai?</p>
            `

            var footer = `        
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="submit-form" type="button" class="btn btn-primary">Submit</button>
            `

            showConfirm(data, footer)

            $('#submit-form').click(function () {
                var reqBody = JSON.stringify({
                    bank_id: $('#form-input-bankid').val(),
                    bank_name: $('#form-input-bprname').val(),
                    registrar: {
                        name: 'DEVELOPER',
                        nik: 'DEVELOPER',
                        wa: 'DEVELOPER',
                        email: $('#form-input-registemail').val(),
                    }
                })
                console.log(reqBody);
                const settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "/lembaga/demo/insert/2",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "processData": false,
                    "data": reqBody
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);
                    try {
                        var resBody = JSON.parse(response);
                        show(resBody.rm);
                    } catch (error) {
                        alert(error)
                    }
                });
            })

        } else {
            show("Semua kolom harus diisi, jangan lupa untuk klik 'Cek Nama BPR' untuk mendapatkan data Bank ID anda")
        }
    })

    $('#form-data-get').click(() => {
        alert('Form Data Get Feature is Not Ready right now')
    })
})



