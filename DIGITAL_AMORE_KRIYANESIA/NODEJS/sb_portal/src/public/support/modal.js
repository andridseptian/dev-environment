import { html, render } from 'https://unpkg.com/lit-html?module';
// export function show(message, footer) {
//     $('#modal-data').modal('show')
//     $('.modal-body').html(message)
//     $('.modal-footer').html(footer)
// }

export function showCustom(header, body, footer) {
    $('#modal-data').modal('show')
    render(body, document.getElementById('modal-body'))
    render(footer, document.getElementById('modal-footer'))
    render(header, document.getElementById('modal-header'))
}

export function show(message) {
    console.log(message)
    $('#modal-data').modal('show')
    $('.modal-body').html(message)
    $('.modal-footer').html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>`)
}

export function showConfirm(message, footer) {
    console.log(message)
    $('#modal-data').modal('show')
    $('.modal-body').html(message)
    $('.modal-footer').html(footer)
}

export function hide() {
    $('#modal-data').modal('hide')
}