const express = require('express')
const app = express()
const port = 16006
const routes = require('./src/routes/mainRoutes.js')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const cors = require('cors')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

app.use(cookieParser());
app.use(session({ secret: "PERBARINDOXDAKXSUPPORT" }));

// parse application/json
app.use(bodyParser.json())

const corsOptions = {
  origin: '*',
  credentials: true,
}
app.use(cors());


app.use('/', express.static('./src/public'));

app.get('/', (req, res) => {
  res.redirect('/index.html')
});

app.post('/support/login', (req, res) => {
  if (req.body.username === 'admin') {
    if (req.body.password === 'qawsed1477') {
      req.session.username = 'admin';
      res.send('success');
    } else {
      res.send('failed')
    }
  } else {
    res.send('failed')
  }
});

app.post('/support/session_destroy', (req, res) => {
  req.session.destroy();
  req.session.username = null
  res.redirect('/support');
});

app.get('/support/check/session', (req, res) => {
  if (req.session.username) {
    res.send('success')
  } else {
    res.redirect('/support/login.html')
  }
});

app.get(['/support', '/support/*'], (req, res) => {
  if (req.session.username) {
    // res.redirect('/support/dashboard.html');
    res.redirect('/support2/dashboard');
  } else {
    res.redirect('/support/login.html')
  }
});

routes(app);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})