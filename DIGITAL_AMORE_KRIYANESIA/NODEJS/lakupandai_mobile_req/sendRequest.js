var unirest = require('unirest');
const tripleDes = require('./tripleDes');
var td = new tripleDes("0102030405060708090A0B0C")

var reqbody = JSON.stringify(
    { "msisdn_agen": "andre1234", "msisdn_nasabah": "082196740588", "nominal": "4523", "no_rekening": "001221000580", "id_bank": "007", "action": "request_transfer_off_us" }
)

// var reqbody = JSON.stringify(
//     { "msisdn_agen": "andre1234", "token": "123276", "pin": "123456", "action": "request_transfer_off_us_pin" }
// )

// var reqbody = JSON.stringify(
//     {"msisdn_agen":"andre1234","token":"218036","action":"send_token_transfer_off_us"}
// )

// var reqbody = JSON.stringify(
//     {"msisdn_agen":"andre1234","token":"123276","pin":"123456","action":"posting_transfer_off_us"}
// )

var reqbodyenc = td.encrypt3DES(reqbody)

console.log("request dec : ", reqbody);
console.log("request enc : ", reqbodyenc);

var req = unirest('POST', 'https://51.79.219.4:10010/lakupandaidiy/')
    .headers({
        'Authorization': 'YW5kcm9pZHwxNjE4OTc0Njc0MjU4',
        'Content-Type': 'text/plain'
    }).strictSSL(false)
    .send(reqbodyenc)
    .end(function (res) {
        if (res.error) throw new Error(res.error);
        console.log("resopnse enc: ", res.raw_body, "\n\n");
        console.log("response dec: ",td.decrypt3DES(res.raw_body));
    });
