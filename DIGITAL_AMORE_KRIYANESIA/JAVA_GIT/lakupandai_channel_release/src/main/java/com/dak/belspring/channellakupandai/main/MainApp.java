/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.main;


import com.dak.belspring.channellakupandai.spring.SpringInitializer;
import javax.naming.ConfigurationException;
import org.jpos.q2.Q2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Andri D Septian
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.dak.belspring.channellakupandai.controller"})
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
public class MainApp {
    public static void main(String[] args) {
        SpringApplication.run(MainApp.class, args);
        
        try{
       //     Q2 q2 = new Q2("src/main/resources/deploy");
          Q2 q2 = new Q2("deploy");
            q2.start();
            SpringInitializer springInitializer = new SpringInitializer();
            springInitializer.initService(); 
        } 
        catch(ConfigurationException c) {
            c.printStackTrace();
        }
    }
}
