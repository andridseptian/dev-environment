/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.spring;

import com.dak.belspring.channellakupandai.dao.LogActivityDao;
import com.dak.belspring.channellakupandai.dao.RegistrationDao;
import com.dak.belspring.channellakupandai.dao.SessionAppDao;
import com.dak.belspring.channellakupandai.dao.TrxDao;
import static com.dak.belspring.channellakupandai.spring.SpringInitializer.logActivityDao;
import javax.naming.ConfigurationException;
import org.jpos.util.Log;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Andri D Septian
 */
public class SpringInitializer {

    Log log = Log.getLog("Q2", getClass().getName());
    public static SessionAppDao sessionDao;
    public static TrxDao trxDao;
    public static LogActivityDao logActivityDao;
    public static RegistrationDao registrationDao;

    public static LogActivityDao getLogActivityDao() {
        return logActivityDao;
    }

    public static void setLogActivityDao(LogActivityDao logActivityDao) {
        SpringInitializer.logActivityDao = logActivityDao;
    }

    public static TrxDao getTrxDao() {
        return trxDao;
    }

    public static void setTrxDao(TrxDao trxDao) {
        SpringInitializer.trxDao = trxDao;
    }

    public static SessionAppDao getSessionAppDao() {
        return sessionDao;
    }

    public static void setSessionAppDao(SessionAppDao sessionDao) {
        SpringInitializer.sessionDao = sessionDao;
    }

    public static RegistrationDao getRegistrationDao() {
        return registrationDao;
    }

    public static void setRegistrationDao(RegistrationDao registrationDao) {
        SpringInitializer.registrationDao = registrationDao;
    }

    public void initService() throws ConfigurationException {

        //   ApplicationContext context = new FileSystemXmlApplicationContext("/src/main/resources/ApplicationContext.xml");
        ApplicationContext context = new FileSystemXmlApplicationContext("ApplicationContext.xml");

//        setmUserDao(context.getBean("MUserDao", MUserDao.class));
        setTrxDao(context.getBean("TrxDao", TrxDao.class));
        setLogActivityDao(context.getBean("LogActivityDao", LogActivityDao.class));
        setSessionAppDao(context.getBean("SessionAppDao", SessionAppDao.class));
        setRegistrationDao(context.getBean("RegistrationDao", RegistrationDao.class));
        log.info("Init DB has Started");
        log.info("Service Started");
    }
}
