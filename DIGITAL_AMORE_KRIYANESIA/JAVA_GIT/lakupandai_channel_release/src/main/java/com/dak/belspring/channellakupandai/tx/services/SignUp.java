/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.tx.services;


import com.dak.belspring.channellakupandai.spring.SpringInitializer;
import com.dak.belspring.channellakupandai.utility.Constants;
import com.dak.belspring.channellakupandai.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class SignUp implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
          //  Agent regDat = new Agent();
            // UserDetail regDetail =  new UserDetail();
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);
            System.out.println(json.toString());
//           
//            String username = json.getString("username");
//            log.info(username);
//            String password = json.getString("password");
//            log.info(password);
//
//            //String fullname = json.getString("fullname");
//            //String contact = json.getString("contact");
//            String nik = json.getString("NIK");
//
//   Agent temp= SpringInitializer.getAgentDao().findUserByNIK(username);
//            
//           Agent checkAgent = SpringInitializer.getAgentDao().findUserByUsername(username);
//            
//            NIKData checkNIK = SpringInitializer.getNIKDataDao().findUserByUsername(nik);
//           // System.out.println(temp);
//            temp = SpringInitializer.agentDao.findUserByNIK(nik);
//            if ((checkAgent == null) && (checkNIK != null) && (temp == null)) {
//
//                String Birthday = checkNIK.getNIKBirthday();
//                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(Birthday);
//
//                LocalDate localDate = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//
//                Random rand = new Random();
//                int random = rand.nextInt(1000);
//                //     System.out.println(month+"aaaaaaaaaaaaaaaaaaaaaaaaaaaa");
//                regDat.setAgentRek(Integer.toString(localDate.getMonthValue()) + Integer.toString(localDate.getDayOfMonth()) + Integer.toString(random) + Integer.toString(localDate.getYear()));
//
//                regDat.setAgentUsername(username);
//                regDat.setAgentPass(password);
//                regDat.setAgentAddress(checkNIK.getAddress());
//                regDat.setAgentFullname(checkNIK.getFullname());
//                regDat.setAgentBirthday(checkNIK.getNIKBirthday());
//                regDat.setAgentBrithplace(checkNIK.getNIKBirthplace());
//                regDat.setAgentNIK(nik);

                log.info("__DEBUG " + json.getString("pin"));
//                regDat.setDateCreated(new Date());
//                regDat.setDateLastLogin(new Date());
//                regDat.setStatus("0");

            //    SpringInitializer.getAgentDao().saveOrUpdate(regDat);
                //SpringInitializer.getUserDetailDao().saveOrUpdate(regDetail);

                jso.put("Status", "Registered");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);

//            } else {
//                jso.put("Status", "Username already exist");
//                rwsc = new ResponseWebServiceContainer(jso);
//                ctx.put(Constants.BODY_RESPONSE, rwsc);
//            }

        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Sign Up ERROR");
        } 
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }

}
