/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.tx.participant;


import com.dak.belspring.channellakupandai.utility.Constants;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;
import org.jpos.util.Log;

/**
 *
 * @author Andri D Septian
 */
public class ControllerSwitch implements GroupSelector, Configurable {
    
    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public String select(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        return cfg.get(ctx.getString(Constants.TRANSACTIONAL_MANAGER.GROUP), "NoRoute");
    }
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String path = ctx.getString(Constants.PATH);
        if(path == null || "".equals(path)) {
            ctx.put(Constants.TRANSACTIONAL_MANAGER.GROUP, "NoRoute");
        } else {
            ctx.put(Constants.TRANSACTIONAL_MANAGER.GROUP, path);
        }
        return PREPARED | NO_JOIN;
    }
        
    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
    
    @Override
    public void commit(long id, Serializable context) {
    }
        
    @Override
    public void abort(long id, Serializable context) {
    }
    
}
