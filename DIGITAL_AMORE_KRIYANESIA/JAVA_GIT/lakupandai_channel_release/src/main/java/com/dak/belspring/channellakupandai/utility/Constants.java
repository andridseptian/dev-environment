/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.utility;

/**
 *
 * @author Andri D Septian
 */
public class Constants {
    public static final String REQ_BODY = "REQ_BODY";
    public static final String BODY_RESPONSE = "BODY_RESPONSE";
    public static final String PATH = "PATH";
    public static final String LOG = "LOG";
    public static final String ABORTED_RESPONSE = "ABORTED_RESPONSE";
    public static final String STATUS = "STATUS";
    
        //    http entity constants
    public static final String HTTP_SERVLET_ENTITY_KEY = "x-api-key";
    public static final String HTTP_SERVLET_ENTITY_PARTNER_IDENTIFIER = "user-agent";
    public static final String HTTP_SERVLET_ENTITY_PARTNER_AUTH = "authorization";
    
    public static class TRANSACTIONAL_MANAGER {
        
        public static final String GROUP = "GROUP";
    
    }
    
    public static class DATA {
        
        public static final String USER = "USER";
    
    }
}
