/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.tx.services;

import com.dak.belspring.channellakupandai.controller.MainController;
import com.dak.belspring.channellakupandai.dao.TrxDao;
import com.dak.belspring.channellakupandai.model.SessionApp;
import com.dak.belspring.channellakupandai.model.Trx;
import com.dak.belspring.channellakupandai.spring.SpringInitializer;
import com.dak.belspring.channellakupandai.utility.Constants;
import com.dak.belspring.channellakupandai.utility.ResponseWebServiceContainer;
import com.dak.belspring.channellakupandai.utility.Utility;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HTTP;
import org.apache.tomcat.util.bcel.classfile.Constant;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class SendIso implements TransactionParticipant, Configurable {

    Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    ResponseWebServiceContainer msResponse = new ResponseWebServiceContainer();

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jso = new JSONObject();

        jso = (JSONObject) ctx.get("res");
        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        String stan = s.substring(s.length() - 6);
        String time = sdf.format(new Date());
        String dc = cfg.get("dc", "6027");
        String action = jso.getString("action");
        String fcm_key = "";
        String destination = cfg.get("host");

        int pathRc = cfg.getInt("path_rc", 39);
        int pathClientReff = cfg.getInt("path_reff", 37);
        int pathMessage = cfg.getInt("path_message", 61);
        Trx trx = new Trx();
        SessionApp session = new SessionApp();
        TrxDao trxDao = new TrxDao();
        JSONObject res = (JSONObject) ctx.get("res");
        JSONObject reqBody = new JSONObject();
        fcm_key = res.getString("fcm_key");
        String rrn = res.getString("rrn");

        
        ISOMsg r = new ISOMsg();

        log.info("----MASUK CLASS SENDISO----\n   ACTION : " + action);

        //---- Metode Pengiriman Sesuai Value Dari ACtion----//
        if (action.equals("initialize")) {

            log.info("----Masuk Initialize----");

            String userId = jso.getString("msisdn");
            String info = jso.getString("info");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("info", info);
            String url = "http://localhost:10020/service/web/initialize";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());
                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCESS" : "FAILED");
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                log.info("REQUEST SUCCESS");

            } catch (IOException ex) {
                log.info("-----ERROR IN SEND ISO CLASS-----");
                log.error(ex);
                session.setRequestGateaway("Failed di Send Iso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else if (action.equals("history_trx")) {

            log.info("----Masuk History TRX----");
            String eDate = res.getString("end_date");
            String sDate = res.getString("start_date");

            String userId = res.getString("msisdn");
            String page = res.getString("page");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("startDate", sDate);
            content.put("endDate", eDate);
            content.put("page", page);
            String url = "http://localhost:10020/service/web/mutasiAgent";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());
                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCESS" : "FAILED");
                log.info("REQUEST SUCCESS");
            } catch (IOException ex) {
                log.info("-----ERROR IN SEND ISO CLASS-----" );
                log.error(ex);
                session.setStatus("99");
                session.setRequestGateaway("Failed di SendIso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else if (action.equals("notifikasi")) {

            log.info("----masuk notifikasi----");
            System.out.println("masuk notifikaso");
            String userId = jso.getString("msisdn");
            String page = ctx.getString("page");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("page", page);
            String url = "http://localhost:10020/service/web/notificationAgent";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());
                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCESS" : "FAILED");
                log.info("REQUEST SUCCESS");
            } catch (IOException ex) {
                log.info("-----ERROR IN SEND ISO CLASS-----");
                log.error(ex);
                session.setStatus("99");
                session.setRequestGateaway("Failed di SendIso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else if (action.equals("notifikasi_mobile")) {

            log.info("----Masuk Notifikasi Mobile----");
            System.out.println("masuk notifikasi mobile");
            String userId = jso.getString("msisdn");
            String page = jso.getString("page");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("page", page);
            String url = "http://localhost:10020/service/web/notifikasi_mobile";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());

                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCES" : "FAILED");
                log.info("REQUEST SUCCESS");
            } catch (IOException ex) {
                log.info("-----ERROR IN SEND ISO CLASS-----");
                log.error(ex);
                session.setStatus("99");
                session.setRequestGateaway("Failed di SendIso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message :" + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else if (action.equals("detail_notifikasi_mobile_xx")) {

            log.info("----Masuk Detail Notifikasi Mobile----");
            System.out.println("masuk detail notofikasi mobile xx");
            String userId = jso.getString("msisdn");
            String page = jso.getString("message");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            content.put("resi", page);
            String url = "http://localhost:10020/service/web/detail_notifikasi_mobile";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);

                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());
                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCES" : "FAILED");
                log.info("REQUEST SUCCESS");
            } catch (IOException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                log.info("-----ERROR IN SEND ISO CLASS-----");
                log.error(ex);
                session.setStatus("99");
                session.setRequestGateaway("Failed di SendIso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else if (action.equals("list_bank")) {

            log.info("----Masuk List Bank----");
            System.out.println("masuk list bank");
            String userId = jso.getString("msisdn");
            JSONObject content = new JSONObject();
            content.put("userId", userId);
            String url = "http://localhost:10020/service/web/bank";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());
                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCES" : "FAILED");
                log.info("REQUEST SUCCESS");
            } catch (IOException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                log.info("-----ERROR IN SEND ISO CLASS-----");
                log.error(ex);
                session.setStatus("99");
                session.setRequestGateaway("Failed di SendIso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else if (action.equals("get_menu")) {

            log.info("----Masuk Get Menu----");

            String tipe = jso.getString("message");
            trx.setCustomerType(tipe);
            JSONObject content = new JSONObject();
            content.put("type", tipe);
            String url = "http://localhost:10020/service/web/product";

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            StringEntity body = new StringEntity(content.toString(), "UTF-8");
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
            postRequest.setEntity(body);

            HttpResponse response;
            try {
                trx.setReqTime(new Date());
                session.setRequestGateaway(content.toString());
                session.setRequestGateawayTime(new Date());
                response = httpClient.execute(postRequest);
                int RC = response.getStatusLine().getStatusCode();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
                String output = br.readLine();

                log.info("Send request to " + url + "\n"
                        + "    Body : " + content.toString() + "\n"
                        + "    Resp : " + output + "\n"
                        + "    Status : " + RC);

                trx.setMsgContent(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);

                session.setResponGateaway(RC != HttpStatus.SC_OK ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : output);
                session.setResponGateawayTime(new Date());
                if (RC != HttpStatus.SC_OK) {
                    trx.setClientRc("99");
                } else {
                    trx.setClientRc("00");
                }
                JSONObject a = new JSONObject(output);

                a.put("RM", output);
                a.put("RC", RC);
                session.setStatus(Integer.toString(RC));
                ctx.put(Constants.BODY_RESPONSE, a.toString());
                trx.setClientReff(rrn);
                trx.setStatus(RC == HttpStatus.SC_OK ? "SUCCESS" : "FAILED");
                log.info("REQUEST SUCCESS");
            } catch (IOException ex) {

                log.info("-----ERROR IN SEND ISO CLASS-----");
                log.error(ex);
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);

                session.setStatus("99");
                session.setRequestGateaway("Failed di SendIso HTTP");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                ctx.put(Constants.BODY_RESPONSE, "FAILED");
            } finally {
//               

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);
            }
        } else {

            try {

                log.info("----Masuk ISO Sending----");

                String host = "lpchn";

                BaseChannel chn = (BaseChannel) NameRegistrar.get("channel." + host);
                MUX mux = (MUX) NameRegistrar.get("mux." + host + "-mux");

                String ada = "";
                trx.setStan(Utility.generateStan());
                r.setPackager(new ISO87APackager());
                r.setMTI("0200");
                r.set(2, res.getString("msisdn"));
                r.set(3, res.getString("pc"));
                r.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
                r.set(11, trx.getStan());
                r.set(32, dc);
                r.set(37, rrn);
                r.set(41, "MOBI-APP");
                r.set(42, "DATA");
                r.set(46, rrn);
                r.set(48, res.getString("message"));
                r.set(61, res.getString("token"));
                r.set(62, fcm_key);
                log.info("Request Message : " + res.getString("message"));
                log.info("Sending request to " + host, r);
                log.info("Get Package : " + r.getPackager().getDescription());
                log.info("Get pack : " + r.pack());
                trx.setSendTime(new Date());

                session.setRequestGateaway(r.toString());
                session.setRequestGateawayTime(new Date());
                ISOMsg dataIso = mux.request(r, 60000);

                JSONObject respon = new JSONObject();
                if (dataIso != null && dataIso.hasField(pathMessage) && dataIso.hasField(pathRc) && dataIso.hasField(pathClientReff)) {
                    String rsp = dataIso.getString(pathMessage);
                    rsp = rsp.replace("<", "&lt;");
                    rsp = rsp.replace(">", "&gt;");
                    session.setResponGateaway(rsp);
                    session.setResponGateawayTime(new Date());
                    if (action.equals("request_setor_tunai") || action.equals("inquiry_payment")) {
                        respon.put("token", dataIso.getString("61"));
                        log.info("ini TOken " + respon.getString("token"));
                    }
                    if (action.equals("request_transfer_on_us") || action.equals("request_transfer_on_us_lakupandai")) {
                        respon.put("token", dataIso.getString("61"));

                        log.info("ini TOken " + respon.getString("token"));
                    }
                    if (action.equals("inquiry_prepaid")) {
                        respon.put("token", dataIso.getString("61"));

                        log.info("ini TOken " + respon.getString("token"));
                    }

                    respon.put("RM", rsp);
                    String rc = dataIso.getString(pathRc);
                    log.info("RESPON : " + rsp + "\n    RC     : " + rc);

                    trx.setMsgContent(rc.equals("50") ? "Kesalahan jaringan : Transaksi tidak dapat dilakukan" : rsp);
                    trx.setClientRc(rc);
                    trx.setClientReff(dataIso.getString(pathClientReff));
                    trx.setStatus(rc.equals("00") || rc.equals("50") ? "SUCCESS" : "FAILED");
                    session.setStatus(rc);
                    respon.put("RC", rc);
                    ctx.put(Constants.BODY_RESPONSE, respon.toString());
                    log.info("REQUEST SUCCESS");

                } else {
                    trx.setTrxInfo(pathMessage + "," + pathRc + "," + pathClientReff + " not found in response");
                    trx.setStatus("FAILED");
                    trx.setClientRc("60");
                }

            } catch (Exception e) {

                log.info("-----ERROR IN SEND ISO CLASS-----");

                log.error(e);
                reqBody.put("data", "tida");
                reqBody.put("rc", "99");
                reqBody.put("rm ", "Internal Error");
                session.setStatus("99");

                session.setRequestGateaway("di Class SendIso Kirim Denagan ISO");
                session.setRequestGateawayTime(new Date());
                session.setResponGateaway("Failed");
                session.setResponGateawayTime(new Date());
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                ctx.put("Session_App", session);
                ctx.put(Constants.BODY_RESPONSE, "FAILED");

                ctx.put(Constants.BODY_RESPONSE, reqBody);
                return ABORTED | NO_JOIN;
            } finally {

                trx.setProvider("DATA");
                session.setPc(res.getString("pc"));
                session.setMsisdnAgen(res.getString("msisdn"));
                session.setDc(dc);
                trx.setMsisdn(res.getString("msisdn"));
                trx.setPc(res.getString("pc"));
                trx.setReqMsg(res.getString("message"));

                System.out.println("Request Message " + trx.getReqMsg());
                trx.setAction(res.getString("action"));
                trx.setCrTime(new Date());

                SpringInitializer.trxDao.saveOrUpdate(trx);
                log.info("SUKSES");
                ctx.put("Session_App", session);

            }
        }

        return PREPARED | NO_JOIN;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {

    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        cfg = c;
    }

}
