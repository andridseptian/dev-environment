/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;

/**
 *
 * @author DAK-Haikal
 */
public class Utility {
 
    
     public static String generateRrn() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
        String dateString = sdf.format(new Date());
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return dateString + s.substring(s.length() - 6);
    }

    public static String generateEdcRrn() throws ISOException {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return ISOUtil.zeropad(s.substring(s.length() - 6), 12);
    }

    public static String generateStan() {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return s.substring(s.length() - 6);
    }

    public static String generateRandomNumber(int length) {
        UUID gen = UUID.randomUUID();
        long randomNumber = Math.abs(gen.getLeastSignificantBits());
        String stringRep = String.valueOf(randomNumber);
        String reff1 = stringRep.substring(0, length);
        return reff1;
    }
    
}
