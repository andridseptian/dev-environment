/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.tx.services;

import com.dak.belspring.channellakupandai.model.Registration;
import com.dak.belspring.channellakupandai.model.SessionApp;
import com.dak.belspring.channellakupandai.model.Trx;
import com.dak.belspring.channellakupandai.spring.SpringInitializer;
import com.dak.belspring.channellakupandai.utility.Constants;
import com.dak.belspring.channellakupandai.utility.EncryptUtility;
import com.dak.belspring.channellakupandai.utility.Utility;
import java.io.Serializable;
import java.util.Date;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author DAK
 */
public class ParseRequest implements TransactionParticipant {

    @Override
    public int prepare(long l, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        String pc = "";
        String rc_sms = "";
        String imei = "";
        String msg = "";
        String token = "";
        String msisdnNasabah = "";
        SessionApp session = new SessionApp();
        String rrn = Utility.generateRrn();
        String fcm = "";
        JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);
        JSONObject jso = new JSONObject();
        Trx trx = new Trx();
        Log log = Log.getLog("Q2", getClass().getName());
        log.info("----MASUK CLASS PARSE REQUEST----");

        try {

            String Action = json.getString("action"); // Pengambilan data Action
            String pinAgen = "";
            JSONObject res = new JSONObject();

            //---- CEK APAKAH REQUEST DARI MOBILE MENGANDUNG ATRIBUTE PIN ----//
            if (json.has("pin")) {

                String pass = json.getString("pin");
                log.info("Pin ADA ");
                EncryptUtility EU = null;
                try {
                    EU = new EncryptUtility("0404040404040404");
                } catch (Exception ex) {
                    Logger.getLogger(ParseRequest.class.getName()).log(Level.SEVERE, null, ex);
                }
                String padding = "FFFFFFFFFFFFFFFF";
                pinAgen = EU.encrypt(StringUtils.substring(pass + padding, 0, 16)).toLowerCase();
            }

            //---- Masuk Pilihan Menu Untuk Action Sesuai denga Action yang didapat dari Json ------//
            if (Action.equals("login")) {

                log.info("----Masuk Login----");
                pc = "900000";
                rc_sms = "00";
                if (json.has("token")) {
                    token = json.getString("token");
                    fcm = json.getString("token");
                }

                jso.put("rc", rc_sms);
                jso.put("rm", "INI LOGIN");

                imei = json.getString("imei");
                msg = imei + "|" + pinAgen;

            } else if (Action.equals("check_nik")) {
                pc = "920000";

                log.info("----Masuk Check NIK----");
                jso.put("rc", "00");
                jso.put("rm", "CHECK NIK");

                String nik = json.getString("nik");
                msg = nik;
                log.info("sadaasd =====  " + nik);
            } else if (Action.equals("reset_pwd")) {

                log.info(" Masuk Reset Password ");
                if ("".equals(pinAgen) || pinAgen == null) {

                } else {
                    pc = "930000";
                    jso.put("rc", "00");
                    jso.put("rm", "RESET PASSWORD");

                    imei = json.getString("imei");
                    msg = imei + "|" + pinAgen;
                }

            } else if (Action.equals("generate_token_activate_account")) {
                Registration AR = new Registration();
                log.info("----Masuk Generate Token Activate Acoouynt----");

                pc = "910000";
                rc_sms = "01";
                jso.put("rc", rc_sms);
                jso.put("rm", "Generate token Activasi Account");

                String noKtp = json.getString("nik");
                String nama = json.getString("nama");
                String tgl_lahir = json.getString("tanggal");
                String msisdn_nasabah = json.getString("msisdn_nasabah");
                String msisdn_agen = json.getString("msisdn_agen");
                msg = noKtp + "|" + nama + "|" + tgl_lahir + "|" + msisdn_nasabah;
                AR.setDtm(new Date());
                AR.setMsisdnNasabah(msisdn_nasabah);
                AR.setMsisdnAgen(msisdn_agen);
                AR.setDc("APP-LAKU-PANDAI");
                AR.setBankCode("114");
                AR.setNoIdentitas(noKtp);
                AR.setTanggalLahir(tgl_lahir);
                AR.setReff(rrn);
                AR.setProvider("DATA");
                AR.setNama(nama);
                AR.setFreeText(msg);

                SpringInitializer.getRegistrationDao().saveOrUpdate(AR);

            } else if (Action.equals("pendaftaran_rekening")) {

                log.info("----Masuk Pendaftaran Rekening----");
                pc = "940000";

                jso.put("rc", "00");
                jso.put("rm", "PENDAFTARAN REKENING");

                token = json.getString("token");
                msg = token + "|" + pinAgen;

            } else if (Action.equals("resend_token")) {

                log.info("----Masuk Resend Token----");
                msisdnNasabah = json.getString("msisdn_nasabah");
                pc = "950000";
                jso.put("rc", "00");
                jso.put("rm", "RESEND TOKEN");

                msg = msisdnNasabah;
            } else if (Action.equals("request_tarik_tunai")) {

                log.info("----Masuk Request Tarik Tunai----");
                pc = "210000";
                rc_sms = "02";
                jso.put("rc", rc_sms);
                jso.put("rm", "REQUEST TARIK TUNAI");

                String nomorHp = json.getString("msisdn_nasabah");
                String amount = json.getString("nominal");
                res.put("amount", amount);
                msg = nomorHp + "|" + amount + "|" + pinAgen;

            } else if (Action.equals("send_token_tarik_tunai")) {

                log.info("----Masuk Send Token Tarik Tunai----");
                pc = "220000";
                token = json.getString("token");
                msg = token;
                rc_sms = "03";
                jso.put("rc", rc_sms);
                jso.put("rm", "SEND TOKEN TARIK TUNAI");

            } else if (Action.equals("posting_tarik_tunai")) {

                log.info("----Masuk Posting Tarik Tunai----");
                pc = "230000";
                token = json.getString("token");
                rc_sms = "04";
                msg = pinAgen;
                jso.put("rc", rc_sms);
                jso.put("rm", "POSTING TARIK TUNAI");

            } else if (Action.equals("request_setor_tunai")) {

                log.info("----Masuk Request Setor Tunai----");
                pc = "310000";
                rc_sms = "05";
                String nomorHp = json.getString("msisdn_nasabah");

                String amount = json.getString("nominal");
                res.put("amount", amount);

                msg = nomorHp + "|" + amount;
                jso.put("rc", rc_sms);
                jso.put("rm", "REQUEST SETOR TUNAI");

            } else if (Action.equals("posting_setor_tunai")) {

                log.info("----Posting Setor Tunai----");
                pc = "320000";
                rc_sms = "06";
                token = json.getString("token");
                msg = pinAgen;
                jso.put("rc", rc_sms);
                jso.put("rm", "POSTING SETOR TUNAI");

            } else if (Action.equals("request_transfer_on_us_lakupandai")) {

                log.info("----Masuk Request Transfer on us Lakupandai----");
                pc = "421100";
                String nomorHp = json.getString("msisdn_nasabah");
                String noRek = json.getString("msisdn_tujuan");
                String amount = json.getString("nominal");
                msg = nomorHp + "|" + noRek + "|" + amount;
                res.put("amount", amount);
                jso.put("rc", "00");
                jso.put("rm", "REQUEST TRANSFER ON US LAKUPANDAI");

            } else if (Action.equals("request_transfer_on_us_pin_lakupandai")) {

                log.info("----Request Transfer On Us Pin Lakupandai----");
                pc = "421200";
                token = json.getString("token");
                msg = pinAgen;
                jso.put("rc", "00");
                jso.put("rm", "REQUEST TRANSFER ON US PIN LAKUPANDAI");

            } else if (Action.equals("send_token_transfer_on_us_lakupandai")) {
                log.info("----Masuk Send Token Transfer On Us Lakupandai----");
                pc = "421300";
                token = json.getString("token");
                msg = token;
                jso.put("rc", "00");
                jso.put("rm", "SEND TOKEN TRANSFER ON US LAKUPANDAI");

            } else if (Action.equals("posting_transfer_on_us_lakupandai")) {

                log.info("----Masuk Posting Transfer on Us Lakupandai----");
                pc = "421400";
                token = json.getString("token");
                msg = pinAgen;
                jso.put("rc", "00");
                jso.put("rm", "POSTING TRANSFER ON US LAKUPANDAI");

            } else if (Action.equals("request_transfer_on_us")) {

                log.info("----Masuk Request Transfer On Us----");
                pc = "411100";
                rc_sms = "07";
                String nomorHp = json.getString("msisdn_nasabah");
                String noRek = json.getString("no_rekening");
                String amount = json.getString("nominal");
                msg = nomorHp + "|" + noRek + "|" + amount;
                res.put("amount", amount);

                jso.put("rc", rc_sms);
                jso.put("rm", "REQUEST TRANSFER ON US");

            } else if (Action.equals("request_transfer_on_us_pin")) {

                log.info("----Masuk Request Transfer On us Pin----");
                pc = "411200";
                token = json.getString("token");
                msg = pinAgen;
                jso.put("rc", "00");
                jso.put("rm", "REQUEST TRANSFER ON US PIN");

                rc_sms = "08";
            } else if (Action.equals("send_token_transfer_on_us")) {

                log.info("----Masuk Send Token Transfer On Us----");
                pc = "411300";
                token = json.getString("token");
                rc_sms = "09";
                msg = token;
                jso.put("rc", rc_sms);
                jso.put("rm", "SEND TOKEN TRANSFER ON US");

            } else if (Action.equals("posting_transfer_on_us")) {

                log.info("----Masuk Posting Transfer On Us----");
                pc = "411400";
                token = json.getString("token");
                msg = pinAgen;
                rc_sms = "10";
                jso.put("rc", rc_sms);
                jso.put("rm", "POSTING TRANSFER ON US");

            } else if (Action.equals("request_beli_pulsa")) {

                log.info("----Masuk Request Beli Pulsa----");
                pc = "511100";
                rc_sms = "12";

                String nomorHpNas = json.getString("msisdn_nasabah");
                String nomorHpTuj = json.getString("msisdn_tujuan");
                String telco = json.getString("provider");
                String denom = json.getString("denom");
                String key = "BELI PULSA|";
                String data = nomorHpTuj + "|" + telco + "|" + denom + "|";
                String keyword = "|BELI " + nomorHpTuj + " " + denom + "|";
                msg = key + data + nomorHpNas + keyword + pinAgen;
                jso.put("rc", rc_sms);
                jso.put("rm", "REQUEST BELI PULSA");

            } else if (Action.equals("send_token_beli_pulsa")) {

                log.info("----Masuk Send Token Beli Pulsa----");
                pc = "511200";
                rc_sms = "13";

                token = json.getString("token");
                msg = token;
                jso.put("rc", rc_sms);
                jso.put("rm", "SEND TOKEN BELI PULSA");

            } else if (Action.equals("posting_beli_pulsa")) {

                log.info("----Masuk Posting Beli Pulsa----");
                pc = "511300";
                rc_sms = "14";

                token = json.getString("token");
                msg = pinAgen;
                jso.put("rc", rc_sms);
                jso.put("rm", "POSTING BELI PULSA");

            } else if (Action.equals("last_trx")) {

                log.info("----Masuk Last TRX----");
                pc = "610000";
                rc_sms = "15";

                String nomorHp = json.getString("msisdn_nasabah");
                msg = nomorHp + "|" + pinAgen;
                jso.put("rc", rc_sms);
                jso.put("rm", "LAST TRX");

                rc_sms = "15";
            } else if (Action.equals("history_trx")) {

                int a = 1;
                if (a == 1) {
                    throw new Exception("TESTING");
                }

                log.info("----Masuk History TRX----");
                pc = "620000";
                String startDate = json.getString("start_date");

                String endDate = json.getString("end_date");

                String page = json.getString("page");
                res.put("page", page);
                res.put("start_date", startDate);
                res.put("end_date", endDate);
                res.put("andre", 2);

                msg = startDate + "|" + endDate;

                jso.put("rc", "00");
                jso.put("rm", "HISTORY TRX");

            } else if (Action.equals("notifikasi")) {

                log.info("----Masuk Notifikasi----");
                String page = json.getString("page");
                res.put("page", page);

                jso.put("rc", "00");
                jso.put("rm", "NOTIFIKASI");

            } else if (Action.equals("notifikasi_mobile")) {

                log.info("----Masuk Notifikasi Mobile----");
                String page = json.getString("page");
                res.put("page", page);

                jso.put("rc", "00");
                jso.put("rm", "NOTIFIKASI MOBILE");

            } else if (Action.equals("detail_notifikasi_mobile")) {

                log.info("----Masuk Detail Notifikasi Mobile----");
                pc = "505000";
                String resi = json.getString("resi");

                msg = resi;

                jso.put("rc", "00");
                jso.put("rm", "DETAIL NOTIFIKASI MOBILE");

            } else if (Action.equals("list_bank")) {

                log.info("----Masuk List Bank----");
                jso.put("rc", "00");
                jso.put("rm", "LIST BANK");

            } else if (Action.equals("get_menu")) {

                log.info("----Masuk Get Menu----");
                pc = "630000";
                String tipe = json.getString("tipe");
                msg = tipe;

                jso.put("rc", "00");
                jso.put("rm", "GET MENU");

            } else if (Action.equals("saldo_agen")) {

                log.info(" Masuk Saldo Agen ");
                pc = "710000";
                rc_sms = "16";

                msg = pinAgen;
                jso.put("rc", rc_sms);
                jso.put("rm", "SALDO AGEN");

            } else if (Action.equals("saldo_nasabah")) {

                log.info("----Masuk Saldo Nasabah----");
                pc = "720000";
                String nomorHp = json.getString("msisdn_nasabah");
                msg = nomorHp + "|" + pinAgen;

                jso.put("rc", "00");
                jso.put("rm", "SALDO NASABAH");

            } else if (Action.equals("inquiry_payment")) { //inquiry telkomsel postpaid 0 nasabah 5 agen 7 posting nasabah 8 posting agen

                log.info("----Masuk Inquiry Payment---- ");
                String product_code = json.getString("product_code");
                String nomor_pelanggan = json.getString("id_pelanggan");
                msg = nomor_pelanggan;
                if (!json.has("hp_nasabah")) {
                    product_code = "5" + product_code.substring(1);
                } else {
                    msg = json.getString("hp_nasabah") + "|" + nomor_pelanggan;
                }
                pc = product_code;

                jso.put("rc", "00");
                jso.put("rm", "INQUIRY PAYMENT");

            } else if (Action.equals("inquiry_prepaid")) { //inquiry telkomsel postpaid 0 nasabah 5 agen 7 posting nasabah 8 posting agen

                log.info("----Masuk Inquiry Prepaid---- ");
                String product_code = json.getString("product_code");
                String nomor_pelanggan = json.getString("id_pelanggan");
                String denom = json.getString("denom");
                msg = nomor_pelanggan + "|" + denom;
                if (!json.has("hp_nasabah")) {
                    product_code = "5" + product_code.substring(1);
                } else {
                    msg = json.getString("hp_nasabah") + "|" + nomor_pelanggan + "|" + denom;
                }
                pc = product_code;

                jso.put("rc", "00");
                jso.put("rm", "INQUIRY PREPAID");

            } else if (Action.equals("posting_payment") || Action.equals("posting_prepaid")) { //inquiry telkomsel postpaid 0 nasabah 5 agen 7 posting nasabah 8 posting agen

                log.info("-----Masuk Posting Payment or Posting Prepaid----- ");
                String product_code = json.getString("product_code");
                String tipe = json.getString("tipe");
                res.put("tipe", tipe);
                token = json.getString("token");
                msg = pinAgen;
                if (tipe.equals("debit")) {
                    product_code = "7" + product_code.substring(1);
                    jso.put("rc", "00");
                    jso.put("rm", "POSTING PAYMENT");
                } else {
                    product_code = "8" + product_code.substring(1);
                    jso.put("rc", "00");
                    jso.put("rm", "POSTING PREPAID");
                }
                pc = product_code;

            } else if (Action.equals("initialize")) {

                log.info("----Masuk Intiialize---- ");

                String info = json.getString("info");
                jso.put("rc", "00");
                jso.put("rm", "POSTING INTIALIZE");
                res.put("info", info);
            } else {

                log.info(" No Route ! ");
                jso.put("rc", "99");
                jso.put("rm", "GAGAl");

            }

            ctx.put(Constants.BODY_RESPONSE, jso);

            res.put("msisdn", json.getString("msisdn_agen"));
            res.put("message", msg);
            res.put("action", Action);
            res.put("pc", pc);
            res.put("token", token);
            res.put("fcm_key", fcm);
            res.put("rc_sms", rc_sms);
            res.put("rrn", rrn);
            ctx.put("res", res);
            return PREPARED;
        } catch (Exception e) {
            log.info("-----ERROR IN PARSE REQUEST CLASS----- ");
           log.error(e);

            session.setStatus("99");

            session.setRequestGateaway("Failed");
            session.setRequestGateawayTime(new Date());
            session.setResponGateaway("Failed di ParseRequest");
            session.setResponGateawayTime(new Date());
            session.setPc(" ");
            session.setMsisdnAgen(" ");
            ctx.put("Session_App", session);

            session.setDc("dc");
            ctx.put(Constants.BODY_RESPONSE, "Internal Error");
            return ABORTED;
        }

    }

    @Override
    public void commit(long l, Serializable srlzbl) {

    }

    @Override
    public void abort(long l, Serializable srlzbl
    ) {

    }

}
