/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author DAK-Haikal
 */
@Entity
@Table(name = "trx")
public class Trx {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_trx_seq")
    @SequenceGenerator(name = "id_trx_seq", sequenceName = "id_trx_seq")
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "action", nullable = false)
    private String Action;

    @Column(name = "pc", nullable = false)
    private String pc;

    @Column(name = "cr_time", nullable = false)
    private Date crTime;

    @Column(name = "msg_content", nullable = false)
    private String msgContent;

    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @Column(name = "req_msg", nullable = false)
    private String reqMsg;

//    @Column(name = "response_msg", nullable = false)
//    private String responseMsg;

//    @Column(name = "response_time", nullable = false)
//    private String responseTime;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "request_time", nullable = false)
    private Date reqTime;
    
    @Column(name = "sending_time", nullable = false)
    private Date sendTime;

    @Column(name = "client_rc", nullable = false)
    private String ClientRc;

    @Column(name = "client_reff", nullable = false)
    private String ClientReff;

    @Column(name = "stan", nullable = false)
    private String stan;

    @Column(name = "provider", nullable = false)
    private String provider;
    
    @Column(name = "amount", nullable = false)
    private int amount;
    
    @Column(name = "trx_info", nullable = false)
    private String trxInfo;
    
    @Column(name = "admin_fee", nullable = false)
    private int adminFee;
    
    @Column(name = "fee", nullable = false)
    private int fee;
    
    @Column(name = "customer_type", nullable = false)
    private String customerType;

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Date getCrTime() {
        return crTime;
    }

    public void setCrTime(Date crTime) {
        this.crTime = crTime;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public Date getReqTime() {
        return reqTime;
    }

    public void setReqTime(Date reqTime) {
        this.reqTime = reqTime;
    }

//    public String getResponseTime() {
//        return responseTime;
//    }
//
//    public void setResponseTime(String responseTime) {
//        this.responseTime = responseTime;
//    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getReqMsg() {
        return reqMsg;
    }

    public void setReqMsg(String reqMsg) {
        this.reqMsg = reqMsg;
    }

//    public String getResponseMsg() {
//        return responseMsg;
//    }
//
//    public void setResponseMsg(String responseMsg) {
//        this.responseMsg = responseMsg;
//    }
//
    public String getAction() {
        return Action;
    }

    public void setAction(String Action) {
        this.Action = Action;
    }

    /**
     * @return the ClientRc
     */
    public String getClientRc() {
        return ClientRc;
    }

    /**
     * @param ClientRc the ClientRc to set
     */
    public void setClientRc(String ClientRc) {
        this.ClientRc = ClientRc;
    }

    /**
     * @return the ClientReff
     */
    public String getClientReff() {
        return ClientReff;
    }

    /**
     * @param ClientReff the ClientReff to set
     */
    public void setClientReff(String ClientReff) {
        this.ClientReff = ClientReff;
    }

    /**
     * @return the stan
     */
    public String getStan() {
        return stan;
    }

    /**
     * @param stan the stan to set
     */
    public void setStan(String stan) {
        this.stan = stan;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return the trxInfo
     */
    public String getTrxInfo() {
        return trxInfo;
    }

    /**
     * @param trxInfo the trxInfo to set
     */
    public void setTrxInfo(String trxInfo) {
        this.trxInfo = trxInfo;
    }

    

  

    /**
     * @return the customerType
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * @param customerType the customerType to set
     */
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    /**
     * @return the adminFee
     */
    public int getAdminFee() {
        return adminFee;
    }

    /**
     * @param adminFee the adminFee to set
     */
    public void setAdminFee(int adminFee) {
        this.adminFee = adminFee;
    }

    /**
     * @return the fee
     */
    public int getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(int fee) {
        this.fee = fee;
    }

    /**
     * @return the sendTime
     */
    public Date getSendTime() {
        return sendTime;
    }

    /**
     * @param sendTime the sendTime to set
     */
    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

}
