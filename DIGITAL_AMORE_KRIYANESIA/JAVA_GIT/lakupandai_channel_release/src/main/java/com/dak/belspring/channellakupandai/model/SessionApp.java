/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DAK
 */
@Entity(name = "SessionApp")
@Table( name = "session_app")
public class SessionApp {
    
    
      @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
      
    @Column(name = "cr_time",nullable = false)
    private Date crTime;
    
    @Column(name = "action",nullable = false)
    private String action;
    
    @Column(name = "bank_code", nullable = false)
    private String bankCode;
    
    @Column(name = "msisdn_agen",nullable = false)
    private String msisdnAgen;
    
    @Column(name = "reff", nullable = false)
    private String reff;
    
    @Column(name = "status", nullable = false)
    private String status;
    
    @Column(name = "dc",nullable = false)
    private String dc;
    
    @Column(name = "pc",nullable = false)
    private String pc;
    
    @Column(name = "req_input",nullable = false)
    private String reqInput;
    
    @Column(name = "req_input_time",nullable = false)
    private Date reqInputTime ;
    
    @Column(name = "respon_mobile",nullable = false)
    private String responMobile;
    
    @Column(name = "respon_mobile_time",nullable =false)
    private Date responMobileTime;
    
    @Column(name = "respon_gateaway",nullable = false)
    private String responGateaway;
    
    @Column(name = "respon_gateaway_time",nullable = false)
    private Date responGateawayTime;
    
      
    @Column(name = "request_gateaway",nullable = false)
    private String requestGateaway;
    
    @Column(name = "request_gateaway_time",nullable = false)
    private Date requestGateawayTime;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the crTime
     */
    public Date getCrTime() {
        return crTime;
    }

    /**
     * @param crTime the crTime to set
     */
    public void setCrTime(Date crTime) {
        this.crTime = crTime;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the msisdnAgen
     */
    public String getMsisdnAgen() {
        return msisdnAgen;
    }

    /**
     * @param msisdnAgen the msisdnAgen to set
     */
    public void setMsisdnAgen(String msisdnAgen) {
        this.msisdnAgen = msisdnAgen;
    }

    /**
     * @return the reff
     */
    public String getReff() {
        return reff;
    }

    /**
     * @param reff the reff to set
     */
    public void setReff(String reff) {
        this.reff = reff;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the dc
     */
    public String getDc() {
        return dc;
    }

    /**
     * @param dc the dc to set
     */
    public void setDc(String dc) {
        this.dc = dc;
    }

    /**
     * @return the pc
     */
    public String getPc() {
        return pc;
    }

    /**
     * @param pc the pc to set
     */
    public void setPc(String pc) {
        this.pc = pc;
    }

    /**
     * @return the reqInput
     */
    public String
         getReqInput() {
        return reqInput;
    }

    /**
     * @param reqInput the reqInput to set
     */
    public void setReqInput(String reqInput) {
        this.reqInput= reqInput;
    }

    /**
     * @return the reqInputTime
     */
    public Date getReqInputTime() {
        return reqInputTime;
    }

    /**
     * @param reqInputTime the reqInputTime to set
     */
    public void setReqInputTime(Date reqInputTime) {
        this.reqInputTime = reqInputTime;
    }

    /**
     * @return the responMobile
     */
    public String getResponMobile() {
        return responMobile;
    }

    /**
     * @param responMobile the responMobilr to set
     */
    public void setResponMobile(String responMobile) {
        this.responMobile = responMobile;
    }

    /**
     * @return the responMobileTime
     */
    public Date getResponMobileTime() {
        return responMobileTime;
    }

    /**
     * @param responMobileTime the responMobileTime to set
     */
    public void setResponMobileTime(Date responMobileTime) {
        this.responMobileTime = responMobileTime;
    }

    /**
     * @return the responGateaway
     */
    public String getResponGateaway() {
        return responGateaway;
    }

    /**
     * @param responGateaway the responGateaway to set
     */
    public void setResponGateaway(String responGateaway) {
        this.responGateaway = responGateaway;
    }

    /**
     * @return the responGateawayTime
     */
    public Date getResponGateawayTime() {
        return responGateawayTime;
    }

    /**
     * @param responGateawayTime the responGateawayTime to set
     */
    public void setResponGateawayTime(Date responGateawayTime) {
        this.responGateawayTime = responGateawayTime;
    }

    /**
     * @return the requestGateaway
     */
    public String getRequestGateaway() {
        return requestGateaway;
    }

    /**
     * @param requestGateaway the requestGateaway to set
     */
    public void setRequestGateaway(String requestGateaway) {
        this.requestGateaway = requestGateaway;
    }

    /**
     * @return the requestGateawayTime
     */
    public Date getRequestGateawayTime() {
        return requestGateawayTime;
    }

    /**
     * @param requestGateawayTime the requestGateawayTime to set
     */
    public void setRequestGateawayTime(Date requestGateawayTime) {
        this.requestGateawayTime = requestGateawayTime;
    }

    /**
     * @param reqInput the reqInput to set
     */
   
    
    
    
    
    
    
    
    
    
}
