/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.dao;

import com.dak.belspring.channellakupandai.model.Trx;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author DAK-Haikal
 */
@Repository(value = "TrxDao")
@Transactional
public class TrxDao extends Dao{
    
      
    public Trx findUserByUsername(String username){
        try {
            Trx trxdat = (Trx) em.createQuery("SELECT a FROM Trx a WHERE a.Username = :username")
                    .setParameter("username", username)
                    .getSingleResult();
            return trxdat;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public Trx saveOrUpdate(Trx trxdat){
        if (trxdat.getId() == null){
            em.persist(trxdat);
            System.out.println("persist / saved");
        }
        else{
            em.merge(trxdat);
            System.out.println("Merge / updated");
        }
        return trxdat;
    }
}
