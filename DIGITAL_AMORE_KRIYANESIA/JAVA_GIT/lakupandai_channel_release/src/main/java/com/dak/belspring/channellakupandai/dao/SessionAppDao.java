/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.dak.belspring.channellakupandai.model.SessionApp;

/**
 *
 * @author DAK-Haikal
 */
@Repository(value = "SessionAppDao")
@Transactional
public class SessionAppDao extends Dao{
    
    
     public SessionApp saveOrUpdate(SessionApp logact) {
        
//         if (logact.getId() == null) {
            em.persist(logact);
            System.out.println("persist / saved");
//        } else {
//            em.merge(logact);
//            System.out.println("Merge / updated");
//        }
        return logact;
    }
    
}
