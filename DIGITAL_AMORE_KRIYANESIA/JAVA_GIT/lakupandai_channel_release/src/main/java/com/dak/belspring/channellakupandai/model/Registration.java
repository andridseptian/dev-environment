/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dak.belspring.channellakupandai.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

/**
 *
 * @author Erwin
 */
@Entity
@Table(name = "account_register")
public class Registration implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "account_register_seq")
    @SequenceGenerator(name = "account_register_seq", sequenceName = "account_register_seq")
    @Column(name="id")
    private Long id;
    
    @Column(name = "msisdn_nasabah", length = 255)
    private String msisdnNasabah;
    
    @Column(name = "no_identitas", length = 255)
    private String noIdentitas;
    
    @Column(name = "nama", length = 255)
    private String nama;
    
    @Column(name = "kota_lahir", length = 255)
    private String kotaLahir;
    
    @Column(name = "tgl_lahir", length = 255)
    private String tanggalLahir;
    
    @Column(name = "jenis_kelamin", length = 255)
    private String jenisKelamin;
    
    @Column(name = "msisdn_agen", length = 255)
    private String msisdnAgen;
    
    @Column(name = "reff", length = 255)
    private String reff;
    
    @Column(name = "provider", length = 255)
    private String provider;
    
    @Column(name = "bank_code", length = 255)
    private String bankCode;
    
    @Column(name = "dc", length = 255)
    private String dc;
    
    @Column(name = "rc", length = 255)
    private String rc;
    
    @Column(name = "rm")
    @Type(type = "text")
    private String rm;
    
    @Column(name = "dtm_created", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtm;
    
    @Column(name = "free_text")
    @Type(type = "text")
    private String freeText;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the msisdnNasabah
     */
    public String getMsisdnNasabah() {
        return msisdnNasabah;
    }

    /**
     * @param msisdnNasabah the msisdnNasabah to set
     */
    public void setMsisdnNasabah(String msisdnNasabah) {
        this.msisdnNasabah = msisdnNasabah;
    }

    /**
     * @return the noIdentitas
     */
    public String getNoIdentitas() {
        return noIdentitas;
    }

    /**
     * @param noIdentitas the noIdentitas to set
     */
    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the kotaLahir
     */
    public String getKotaLahir() {
        return kotaLahir;
    }

    /**
     * @param kotaLahir the kotaLahir to set
     */
    public void setKotaLahir(String kotaLahir) {
        this.kotaLahir = kotaLahir;
    }

    /**
     * @return the tanggalLahir
     */
    public String getTanggalLahir() {
        return tanggalLahir;
    }

    /**
     * @param tanggalLahir the tanggalLahir to set
     */
    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    /**
     * @return the jenisKelamin
     */
    public String getJenisKelamin() {
        return jenisKelamin;
    }

    /**
     * @param jenisKelamin the jenisKelamin to set
     */
    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    /**
     * @return the msisdnAgen
     */
    public String getMsisdnAgen() {
        return msisdnAgen;
    }

    /**
     * @param msisdnAgen the msisdnAgen to set
     */
    public void setMsisdnAgen(String msisdnAgen) {
        this.msisdnAgen = msisdnAgen;
    }

    /**
     * @return the reff
     */
    public String getReff() {
        return reff;
    }

    /**
     * @param reff the reff to set
     */
    public void setReff(String reff) {
        this.reff = reff;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the dc
     */
    public String getDc() {
        return dc;
    }

    /**
     * @param dc the dc to set
     */
    public void setDc(String dc) {
        this.dc = dc;
    }

    /**
     * @return the rc
     */
    public String getRc() {
        return rc;
    }

    /**
     * @param rc the rc to set
     */
    public void setRc(String rc) {
        this.rc = rc;
    }

    /**
     * @return the rm
     */
    public String getRm() {
        return rm;
    }

    /**
     * @param rm the rm to set
     */
    public void setRm(String rm) {
        this.rm = rm;
    }

    /**
     * @return the dtm
     */
    public Date getDtm() {
        return dtm;
    }

    /**
     * @param dtm the dtm to set
     */
    public void setDtm(Date dtm) {
        this.dtm = dtm;
    }

    /**
     * @return the freeText
     */
    public String getFreeText() {
        return freeText;
    }

    /**
     * @param freeText the freeText to set
     */
    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    
}
