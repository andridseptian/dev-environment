/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.utility;

import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import org.jpos.iso.ISOUtil;

/**
 *
 * @author aksesnusantara
 */
public class EncryptUtility {

    public static final String DES_ENCRYPTION_SCHEME = "DES";
    private KeySpec myKeySpec;
    private SecretKeyFactory mySecretKeyFactory;
    private Cipher cipher;
    byte[] keyAsBytes;
    private byte[] myEncryptionKey;
    SecretKey key;

    public EncryptUtility(String param) throws Exception {

        myEncryptionKey = ISOUtil.hex2byte(param);
        myKeySpec = new DESKeySpec(myEncryptionKey);
        mySecretKeyFactory = SecretKeyFactory.getInstance(DES_ENCRYPTION_SCHEME);
        cipher = Cipher.getInstance("DES/ECB/NoPadding");
        key = mySecretKeyFactory.generateSecret(myKeySpec);
    }

    /**
     * Method To Encrypt The String
     */
    public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = ISOUtil.hex2byte(unencryptedString);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = ISOUtil.hexString(encryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }

    /**
     * Method To Decrypt An Ecrypted String
     */
    public String decrypt(String encryptedString) {
        String decryptedText = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = cipher.doFinal(ISOUtil.hex2byte(encryptedString));
            decryptedText = ISOUtil.hexString(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }
}
