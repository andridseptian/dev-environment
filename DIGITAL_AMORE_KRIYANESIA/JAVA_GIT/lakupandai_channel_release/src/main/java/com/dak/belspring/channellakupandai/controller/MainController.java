/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.belspring.channellakupandai.controller;

import com.dak.belspring.channellakupandai.model.LogActivity;
import com.dak.belspring.channellakupandai.model.SessionApp;
import com.dak.belspring.channellakupandai.spring.SpringInitializer;
import com.dak.belspring.channellakupandai.utility.Constants;
import com.dak.belspring.channellakupandai.utility.ResponseWebServiceContainer;
import com.dak.belspring.channellakupandai.utility.TripleDes;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andri D Septian
 */
@RestController
public class MainController {

    //Controller Class
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @RequestMapping(value = "/{service_type}", method = RequestMethod.POST)
    public String allProcess(@RequestBody(required = false) String body, @PathVariable("service_type") String path,
            HttpServletRequest request) throws ParseException {

        log.info("----Masuk Controller----");

        //Function inidigunakan Untuk Handling Request dalam bentuk Json 
        LogActivity logActivity = new LogActivity();
        SessionApp session = new SessionApp();
        try {

            TripleDes dec = new TripleDes(); // decript requeest dengan tripledes
            Context ctx = new Context();
            Space space = SpaceFactory.getSpace();

            JSONObject json = new JSONObject(dec.decrypt(body));

            session.setCrTime(new Date());
            session.setReqInput(json.toString());
            session.setReqInputTime(new Date());
            session.setAction(json.getString("action"));
            session.setMsisdnAgen(json.getString("msisdn_agen"));
            logActivity.setRequest(json.toString());
            logActivity.setDtm_created_msg(new Date());

            log.info("BODY   : " + json.toString() + " \n    ACTION : " + json.getString("action"));

            logActivity = SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);

            ctx.put(Constants.REQ_BODY, json);
            ctx.put(Constants.PATH, path);
            ctx.put(Constants.LOG, logActivity);

            space.out("channel-trxmgr", ctx, 60000);
            Context ctx_response = (Context) space.in(logActivity.getId(), 60000);

            if (true) {

                String respon = (String) ctx_response.getString(Constants.BODY_RESPONSE); // respon dari gateaway
                SessionApp session2 = (SessionApp) ctx_response.get("Session_App");
                String encripRespon = dec.encrypt(respon.toString()); //Encript Respon dari gateAway dengan TripleDEs

                session.setRequestGateaway(session2.getRequestGateaway());
                session.setRequestGateawayTime(session2.getRequestGateawayTime());
                session.setResponGateaway(session2.getResponGateaway());
                session.setResponGateawayTime(session2.getResponGateawayTime());
                session.setPc(session2.getPc());

                session.setDc(session2.getDc());
                session.setStatus(session2.getStatus());

                session.setResponMobile(encripRespon);
                session.setResponMobileTime(new Date());
                SpringInitializer.getSessionAppDao().saveOrUpdate(session);
                log.info("RESPON : " + encripRespon);
                return encripRespon;
            } else {
                logActivity.setResponse("TIMEOUT");
                SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);
                jso.put("response", "TIMEOUT");
                rwsc = new ResponseWebServiceContainer(jso);
                return rwsc.jsonToString();
            }

        } catch (Exception e) {

            log.info("-----ERROR IN MAIN CONTROLLER CLASS----- ");
            log.error(e);

            e.printStackTrace();
            session.setStatus("99");
            session.setRequestGateaway("Failed");
            session.setRequestGateawayTime(new Date());
            session.setResponGateaway("Failed di Main Controller");
            session.setResponGateawayTime(new Date());
            session.setPc(" ");
            session.setMsisdnAgen(" ");
            session.setCrTime(new Date());
            session.setReqInput(" ");
            session.setReqInputTime(new Date());
            session.setAction(" ");

            SpringInitializer.getSessionAppDao().saveOrUpdate(session);

            logActivity.setResponse("Error");
            return logActivity.getResponse();
        }
    }

}
