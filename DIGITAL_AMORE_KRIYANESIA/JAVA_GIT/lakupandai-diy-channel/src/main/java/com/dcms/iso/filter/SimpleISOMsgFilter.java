package com.dcms.iso.filter;

import com.acs.ussd.entity.MLogger;
import com.acs.ussd.service.LoggerService;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.util.LogEvent;
import org.jpos.util.NameRegistrar;
import org.jpos.util.NameRegistrar.NotFoundException;
import static org.jpos.util.ProtectedLogListener.BINARY_WIPED;
import static org.jpos.util.ProtectedLogListener.WIPED;
import org.springframework.context.ApplicationContext;

public class SimpleISOMsgFilter implements ISOFilter, Configurable {

    Configuration cfg;
    boolean logMsg;
    String origin;
    String[] protectFields = null;
    String[] wipeFields = null;

    public SimpleISOMsgFilter() {
        super();
    }

    public SimpleISOMsgFilter(boolean logMsg, String origin) {
        super();
        this.logMsg = logMsg;
    }

    @Override
    public void setConfiguration(Configuration cfg) {
        this.cfg = cfg;
        origin = cfg.get("origin");
        logMsg = cfg.getBoolean("log-msg");
        protectFields = ISOUtil.toStringArray(cfg.get("protect", ""));
        wipeFields = ISOUtil.toStringArray(cfg.get("wipe", ""));
    }

    @Override
    public ISOMsg filter(ISOChannel channel, ISOMsg m, LogEvent evt) {
        if (logMsg) {
            MLogger ml = null;
            LoggerService ls = null;
            try {
                if ("0800".equals(m.getMTI()) || "0810".equals(m.getMTI())) {
                    evt.addMessage("Not Save Network Management Message to Database");
                } else {
                    evt.addMessage("Logging message to Database...");
                    ml = new MLogger();
                    ml.setLogTime(new Date());
                    if (origin == null) {
                        ml.setHostAddress(channel.getName());
                    } else {
                        ml.setHostAddress(origin);
                    }
                    if (m.isIncoming()) {
                        ml.setIncoming(m.isIncoming());
                    } else {
                        ml.setIncoming(false);
                    }

                    try {
                        ISOMsg savedMsg = (ISOMsg) m.clone();
                        checkProtected(savedMsg);
                        checkHidden(savedMsg);
                        ml.setMsg(new String(savedMsg.pack()));
                    } catch (ISOException ex) {
                        ml.setMsg("MESSAGE CANNOT BE SAVED...");
                        evt.addMessage(ml.getMsg());
                    }

                    try {
                        ApplicationContext ac = (ApplicationContext) NameRegistrar.get("spring");
                        ls = (LoggerService) ac.getBean("loggerService");
                        ls.logMsg(ml);
                    } catch (NotFoundException nfe) {
                        evt.addMessage(nfe.getMessage());
                    }
                }
            } catch (ISOException ex) {
                evt.addMessage(ex.getMessage());
            }
        }
        return m;
    }

    private void checkProtected(ISOMsg m) throws ISOException {
        for (String f : protectFields) {
            Object v = null;
            try {
                v = m.getValue(f);
            } catch (ISOException e) {
                // ignore error
            }
            if (v != null) {
                if (v instanceof String) {
                    m.set(f, ISOUtil.protect((String) v));
                } else {
                    m.set(f, BINARY_WIPED);
                }
            }
        }
    }

    private void checkHidden(ISOMsg m) throws ISOException {
        for (String f : wipeFields) {
            Object v = null;
            try {
                v = m.getValue(f);
            } catch (ISOException e) {
                // ignore error
            }
            if (v != null) {
                if (v instanceof String) {
                    m.set(f, WIPED);
                } else {
                    m.set(f, BINARY_WIPED);
                }
            }
        }
    }
}
