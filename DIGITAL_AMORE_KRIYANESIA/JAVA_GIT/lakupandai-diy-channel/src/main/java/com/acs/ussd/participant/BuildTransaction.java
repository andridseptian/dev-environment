/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.spring.SpringImpl;
import java.io.Serializable;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author yogha
 */
public class BuildTransaction implements TransactionParticipant, Configurable {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        if (trx == null
                && ctx.get(Constant.PROVIDER) != null
                && ctx.get(Constant.MSISDN) != null
                && ctx.get(Constant.SMS) != null
                && ctx.get(Constant.SESSION) != null) {
            // mobile app
            String provider = ctx.getString(Constant.PROVIDER);
            String msisdn = ctx.getString(Constant.MSISDN);
            String sms = ctx.getString(Constant.SMS);
            String session = ctx.getString(Constant.SESSION);
            String clientCode = cfg.get("clientCode", "not-supported");
//            String clientCode = "112";
            trx = new Trx();
//            String groups = cfg.get("url", "Not-Supported");
            RequestMapping req = SpringImpl.getRequestMappingDao().findByClientCode(clientCode);
            if (req != null) {
                trx.setRequestTime(new Date());
                trx.setQueueKey(msisdn+session);
                trx.setClient(req.getClient());
                trx.setMsisdn(msisdn);
                trx.setProvider(provider);
                trx.setProviderReff(ctx.getString("CHNNL"));
                trx.setReqMessage(sms);
                ctx.put(Constant.TRX, trx);
                ctx.put(Constant.ORIGIN, req.getClient().getDisplayName());
                return PREPARED;
            } else {
                trx.setMsisdn(msisdn);
                trx.setProvider(provider);
                trx.setReqMessage(sms);
                ctx.put(Constant.TRX, trx);
                ctx.put(Constant.ORIGIN, "not-supported");
                return ABORTED | NO_JOIN;
            }
        } else {
            // USSD
            ctx.put(Constant.TRX, trx);
            return PREPARED;
        }
    }

    @Override
    public void commit(long l, Serializable srlzbl) {

    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
