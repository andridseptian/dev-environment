/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.entity;

import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.spring.SpringImpl;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Akses Nusantara
 */
public class InputLogger {

    private JSONObject json;

    public InputLogger(Session session, String input) {
        json = new JSONObject();
        if (session != null) {
            Menu m = SpringImpl.getMenuDao().findById(session.getLastMenuId());
            if (m != null) {
                if (session.getFreeText() == null) {
                    json.put(String.valueOf(m.getMenuName()), input);
                } else {
                    json = new JSONObject(session.getFreeText());
                    json.put(String.valueOf(m.getMenuName()), input);
                }
            }
            session.setUserInput(input);
            session.setFreeText(json.toString());
        }
    }

    public JSONObject getJson() {
        return json;
    }

    public void setJson(JSONObject json) {
        this.json = json;
    }

    public String getValue(String key) {
        return (String) json.get(key);
    }

    public static void setSelectedMenu(Session session, Menu selectedMenu) {
        JSONObject json = new JSONObject(session.getFreeText());
        json.put(Constant.SELECTED_MENU, selectedMenu.getMenuName());
        session.setFreeText(json.toString());
    }

    public static JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
        JSONObject mergedJSON = new JSONObject();
        try {
            mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
            for (String crunchifyKey : JSONObject.getNames(json2)) {
                mergedJSON.put(crunchifyKey, json2.get(crunchifyKey));
            }

        } catch (JSONException e) {
            throw new JSONException("JSON Exception" + e);
        }
        return mergedJSON;
    }
}
