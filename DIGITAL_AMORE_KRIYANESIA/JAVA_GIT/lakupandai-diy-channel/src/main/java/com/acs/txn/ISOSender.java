/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.txn;


import com.acs.ussd.entity.enums.Constant;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.q2.iso.QMUX;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.jpos.util.NameRegistrar.NotFoundException;

/**
 *
 * @author EliteBook
 */
public class ISOSender {

    public static Log log = Log.getLog("Q2", "ISO8583-Socket-Handler");

    public static ISOMsg request(String destination, ISOMsg req) throws ISOException, NotFoundException {
        ISOMsg rsp;
        ISOChannel chn = (ISOChannel) NameRegistrar.get("channel." + destination);
        if (!chn.isConnected()) {
            rsp = (ISOMsg) req.clone();
//            rsp.unset(new int[]{2, 35});
            rsp.set(Constant.ISO_FIELD_RC, Constant.RC_UNABLE_TO_ROUTE_TRANSACTION);
            log.error("Not connected to module '" + destination + "'");
        } else {
            QMUX qmux = (QMUX) NameRegistrar.get("mux." + destination + "-mux");
            rsp = qmux.request(req, 60000);
            if (rsp == null) {
                rsp = (ISOMsg) req.clone();
                rsp.set(Constant.ISO_FIELD_RC, Constant.RC_TIME_OUT);
                rsp.set(Constant.ISO_FIELD_PRIVATE_DATA, "Transaksi Timeout");
                log.error("No response from module '" + destination + "'");
            } else {
                String destinationAddress = ((BaseChannel) rsp.getSource()).getSocket().getInetAddress().getHostAddress();
                if (!rsp.hasField(Constant.ISO_FIELD_RC)) {
                    rsp.set(Constant.ISO_FIELD_RC, Constant.RC_TIME_OUT);
                    log.error("Missing response code from module '" + destination + "' [" + destinationAddress + "]");
                }
            }
        }
        return rsp;
    }
}
