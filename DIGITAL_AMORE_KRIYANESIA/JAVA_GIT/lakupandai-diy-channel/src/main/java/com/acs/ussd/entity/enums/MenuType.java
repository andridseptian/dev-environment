/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.entity.enums;

/**
 *
 * @author Akses Nusantara
 */
public enum MenuType {
     MENU,                              //MENU
     MENU_HEADER,                       //MENU HEADER
     INPUT,                             //ONLY VIEW INPUT FORM (max 160 char)
     INPUT_VIEW,
     INFO,                              //ONLY VIEW INFO FORM (max 160 char)
     BACK,                              //BACK TO PARENT MENU
     CHECK_PREFIX,                      //CHECK PREFIX
     SEND_VIEW_INPUT_OR_INFO_BY_RC,     //SEND TO TRXMGR, SHOW INPUT FORM IF RC IN FREETEXT
     SEND_VIEW_INFO_FORM,               //SEND TO TRXMGR, SHOW INFO FORM FOR FINAL RESULT
     SEND_VIEW_INPUT_FORM,              //SEND TO TRXMGR, SHOW INPUT FORM FOR NEXT
     SEND_VIEW_INFO_MULTIPLE_PAGE,      //SEND TO TRXMGR, SHOW MULTIPLE PAGE FORM FROM INPUT / 150
     SEND_VIEW_INPUT_MULTIPLE_PAGE,     //SEND TO TRXMGR, SHOW MULTIPLE PAGE FORM WITH INPUT TYPE
     SEND_VIEW_INFO_MENU_FORM;         
}
