/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.SessionApp;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.spring.SpringImpl;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;

/**
 *
 * @author Akses Nusantara
 */
public class SendResponse implements AbortParticipant, Configurable {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        String action = ctx.getString(Constant.SMS);
        ctx.put(Constant.TRX, trx);
        ctx.put(Constant.RC, trx.getClientRc());
        ctx.put(Constant.RM, trx.getMsgContent());
        if("login".equals(action) && "00".equals(trx.getClientRc())) {
            SessionApp ambilWaktu = SpringImpl.getSessionAppDao().findLastLogin(trx.getMsisdn());
            if(ambilWaktu != null) {
                ctx.put("LAST_LOGIN", ambilWaktu.getDtm().toString());
            }            
        }
//        ctx.put("token", ctx.get("token"));
        String queueKey = trx.getQueueKey();
        if (ctx.get(Constant.PROVIDER) != null
                && ctx.getString(Constant.PROVIDER).equals("XL")) {
            String session = ctx.getString(Constant.SESSION);
            String msisdn = ctx.getString(Constant.MSISDN);
            queueKey = msisdn + session;
        }
        String response = Constant.RSP + queueKey;
        log.info("Trying put to : " + response);
        sp.out(response, ctx, 60000);
//        ctx.put(Constant.TRX, trx);
//        String queueKey = trx.getQueueKey();    
//        log.info("Trying put to : " + Constant.RSP + queueKey);
//        sp.out(Constant.RSP + queueKey, ctx, 60000); 
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        
        ctx.put(Constant.TRX, trx);
        String queueKey = trx.getQueueKey();    
        String response = Constant.RSP + queueKey;
        log.info("Trying put to : " + response);
        sp.out(response, ctx, 60000);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;   
        Trx trx = (Trx) ctx.get(Constant.TRX);
        LogEvent le = (LogEvent) ctx.get(Constant.LOGEVENT);
        le.addMessage(ctx.getString(Constant.TRX_INFO));
        String message = cfg.get("error_message", "Maaf, saat ini menu sedang tidak dapat diakses");     
        trx.setMsgContent(message);
        return ABORTED;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
