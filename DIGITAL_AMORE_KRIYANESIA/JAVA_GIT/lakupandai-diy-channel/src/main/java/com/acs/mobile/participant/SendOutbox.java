/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.mobile.participant;

import com.acs.sms.entity.Outbox;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.spring.SpringImpl;
import java.io.Serializable;
import org.apache.commons.codec.binary.Base64;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author riwan
 */
public class SendOutbox implements TransactionParticipant, Configurable {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        String action = ctx.getString("SMS");
        String rc_sms = "";
        if (trx.getClientRc().equals("00")) {
//            rc_sms = (String) ctx.get("rc_sms");
            rc_sms = "00";
        } else {
//            rc_sms = "99";
            rc_sms = trx.getClientRc();
        }

        Outbox sendMessage = new Outbox();
        sendMessage.setDestinationNumber(ctx.getString("RECIPIENT"));
        if (action.equals("request_setor_tunai") || action.equals("request_transfer_on_us")) {
            String token = Base64.encodeBase64String(ctx.getString("TOKEN_BE").getBytes());
            sendMessage.setTextDecoded("JTM " + rc_sms + " " + token + " " + trx.getMsgContent());
        } else {
            sendMessage.setTextDecoded("JTM " + rc_sms + " " + trx.getMsgContent());
        }
//        sendMessage.setTextDecoded("JTM " + rc_sms + " " + trx.getMsgContent());
        sendMessage.setCreatorId("SMS");
        log.info("Kirim sms ke HP");
//        SpringImpl.getInboxDao().saveOutbox(sendMessage);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
