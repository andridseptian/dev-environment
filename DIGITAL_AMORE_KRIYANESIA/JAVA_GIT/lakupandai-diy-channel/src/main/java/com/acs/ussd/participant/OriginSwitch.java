/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.enums.Constant;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;

/**
 *
 * @author aksesciptasolusi
 */
public class OriginSwitch implements GroupSelector, Configurable {

//    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    Log log = Log.getLog("Q2", getClass().getName());
    Configuration cfg;

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ctx.put("GRP", ctx.get(Constant.ORIGIN));
        return PREPARED | NO_JOIN;
    }

    @Override
    public String select(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String key = (String) ctx.getString("GRP");
        String groups = cfg.get(key, "Not-Supported");
        return groups;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        cfg = c;
    }
}
