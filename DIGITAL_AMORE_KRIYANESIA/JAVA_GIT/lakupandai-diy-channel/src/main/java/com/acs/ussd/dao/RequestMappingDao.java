/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.dao;

import com.acs.ussd.entity.RequestMapping;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Akses Nusantara
 */
@Repository(value = "requestMappingDao")
public class RequestMappingDao {

    @Resource(name = "sharedEntityManager")
    private EntityManager em;

    public RequestMapping find(String pathInfo) {
        try {
            if (pathInfo != null) {
                return (RequestMapping) em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.pathInfo = :pathInfo")
                        .setParameter("pathInfo", pathInfo)
                        .setMaxResults(1)
                        .getSingleResult();
            } else {
                return null;
            }
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public RequestMapping findByClientCode(String clientCode) {
        try {
            if (clientCode != null) {
                return (RequestMapping) em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.client.clientCode = :clientCode")
//                return (RequestMapping) em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.client = :clientCode")
                        .setParameter("clientCode", clientCode)
                        .setMaxResults(1)
                        .getSingleResult();
            } else {
                return null;
            }
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public RequestMapping findByClientCodeUrl(String clientCode, String url) {
        try {
            if (clientCode != null) {
                return (RequestMapping) em.createQuery("SELECT r FROM RequestMapping AS r WHERE r.client.clientCode = :clientCode AND r.pathInfo = :path")
                        .setParameter("clientCode", clientCode)
                        .setParameter("path", url)
                        .setMaxResults(1)
                        .getSingleResult();
            } else {
                return null;
            }
        } catch (NoResultException nre) {
            return null;
        }
    }
}
