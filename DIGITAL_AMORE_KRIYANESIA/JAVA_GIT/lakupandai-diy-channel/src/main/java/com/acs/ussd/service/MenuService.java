/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.service;

import com.acs.ussd.dao.MenuDao;
import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Akses Nusantara
 */
@Service(value = "menuService")
public class MenuService {
    
    
    @Autowired
    private MenuDao menuDao;
    
    public List<Menu> findDefaultMenu(RequestMapping req) {
        return menuDao.findByClientAndGroup(req.getClient().getClientCode(), req.getDefaultGroup());
    }
}
