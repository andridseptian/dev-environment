/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.util.Utility;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author yogha
 */
public class SendSMS implements TransactionParticipant, Configurable {

    Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        switch (trx.getProvider()) {
            case "SAT":
            case "TSEL":
            case "XL":
                return PREPARED;
            default:
                return ABORTED;
        }

    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        String output = "";
        Space sp = SpaceFactory.getSpace();
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        LogEvent le = new LogEvent("");
        String telco = trx.getProvider();
        String msisdn = trx.getMsisdn();
        String sms = trx.getMsgContent();
        String transId = "UMB" + Utility.generateStan();
        String transDate = "0";
        String host = cfg.get("host");
        String bankCode = cfg.get("bankcode");
        String NoSID = cfg.get("noSID");
        Boolean sendSms = Boolean.TRUE;
        Boolean multipleSms = Boolean.FALSE;
        try {
            if (trx.getFreeText() != null) {
                JSONObject dataCheck = new JSONObject(trx.getFreeText());
                if (dataCheck.has(Constant.TELCO)) {
                    telco = dataCheck.getString(Constant.TELCO);
                }
                if (dataCheck.has(Constant.HOST)) {
                    host = dataCheck.getString(Constant.HOST);
                }
                if (dataCheck.has(Constant.BANKCODE)) {
                    bankCode = dataCheck.getString(Constant.BANKCODE);
                }
                if (dataCheck.has(Constant.SID)) {
                    NoSID = dataCheck.getString(Constant.SID);
                }
                if (dataCheck.has(telco.toLowerCase() + "_" + Constant.SID)) {
                    NoSID = dataCheck.getString(telco.toLowerCase() + "_" + Constant.SID);
                }
                if (dataCheck.has(Constant.SMS_STATUS)) {
                    sendSms = dataCheck.getBoolean(Constant.SMS_STATUS);
                }
                if (dataCheck.has(telco.toLowerCase() + "_" + Constant.SMS_STATUS)) {
                    sendSms = dataCheck.getBoolean(telco.toLowerCase() + "_" + Constant.SMS_STATUS);
                }
                if (dataCheck.has(Constant.MULTIPLE_SMS)) {
                    multipleSms = dataCheck.getBoolean(Constant.MULTIPLE_SMS);
                }
                if (dataCheck.has(telco.toLowerCase() + "_" + Constant.MULTIPLE_SMS)) {
                    multipleSms = dataCheck.getBoolean(telco.toLowerCase() + "_" + Constant.MULTIPLE_SMS);
                }
            }
        } catch (JSONException je) {
            log.error("JSONException :" + trx.getFreeText());
        }
        if (sendSms && !trx.getClientRc().equals(Constant.RC_MISSING_MANDATORY_PARAMETER) && !trx.getClientRc().equals(Constant.RC_TIME_OUT)) {
            log.info("Send SMS by request");
            try {
                log.info("Multiple SMS : "+multipleSms);
                if (multipleSms && sms.length() > 159) {
                    int totalSms = (int) Math.ceil((double) sms.length() / 160);
                    log.info("Send Total SMS :" + totalSms);
                    log.info("Text : "+ sms);
//                    int x = 0;
                    String temp[] = sms.split("\n\n");
                    String text;
                    log.info("Temp Length: "+ temp.length);
                    for (int loop = 0; loop < temp.length; loop++) {
                        log.info("Loop : "+ loop); 
                        text = temp[loop];
//                        if (loop == totalSms - 1) {
//                            text = sms.substring(x, sms.length());
//                        } else {
//                            text = sms.substring(x, x + 159);
//                        }
//                        x += 159;
                        String req = "";
                        try {
                            req = "telco=" + URLEncoder.encode(telco, "UTF-8")
                                    + "&msisdn=" + URLEncoder.encode(msisdn, "UTF-8")
                                    + "&sms=" + URLEncoder.encode(text, "UTF-8")
                                    + "&bank_code=" + URLEncoder.encode(bankCode, "UTF-8")
                                    + "&trans_id=" + URLEncoder.encode(transId, "UTF-8")
                                    + "&trans_date=" + URLEncoder.encode(transDate, "UTF-8")
                                    + "&no_sid=" + URLEncoder.encode(NoSID, "UTF-8");

                            URL url = new URL(host + "?" + req);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("GET");
                            conn.setReadTimeout(40000);
                            conn.setConnectTimeout(40000);

                            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                            String hasil = "";
                            while ((output = br.readLine()) != null) {
                                hasil = hasil + output;
                            }
                            log.info("Response : "+hasil);
                        } catch (UnsupportedEncodingException ex) {
                            log.info("Error Send SMS" + ex.getMessage());
                        }
                    }
                } else {
                    String req = "telco=" + URLEncoder.encode(telco, "UTF-8")
                            + "&msisdn=" + URLEncoder.encode(msisdn, "UTF-8")
                            + "&sms=" + URLEncoder.encode(sms, "UTF-8")
                            + "&bank_code=" + URLEncoder.encode(bankCode, "UTF-8")
                            + "&trans_id=" + URLEncoder.encode(transId, "UTF-8")
                            + "&trans_date=" + URLEncoder.encode(transDate, "UTF-8")
                            + "&no_sid=" + URLEncoder.encode(NoSID, "UTF-8");
                    URL url = new URL(host + "?" + req);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setReadTimeout(40000);
                    conn.setConnectTimeout(40000);

                    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                    String hasil = "";
                    while ((output = br.readLine()) != null) {
                        System.out.println(output);
                        hasil = hasil + output;
                    }
                }
            } catch (UnsupportedEncodingException ex) {
                log.error(ex.getMessage());
            } catch (MalformedURLException ex) {
                log.error(ex.getMessage());
            } catch (IOException ex) {
                log.info("MASUK SINI ERROR");
                log.error(ex.getMessage());
            } finally {
                ctx.put(Constant.TRX, trx);
            }
        } else {
            log.info("Not Send SMS by request");
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
