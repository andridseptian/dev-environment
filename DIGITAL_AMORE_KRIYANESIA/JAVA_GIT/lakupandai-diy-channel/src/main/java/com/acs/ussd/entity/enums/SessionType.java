/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.entity.enums;

/**
 *
 * @author Akses Nusantara
 */
public enum SessionType {
    TIME_LIMIT,             //Setting a time limit to all transaction, transaction will be aborted when times up
    REFRESH_TIME_LIMIT,     //Setting a time limit to every one transaction, reset expired time when triggered before times up
    NO_LIMIT;               //No Limit to all transaction
}
