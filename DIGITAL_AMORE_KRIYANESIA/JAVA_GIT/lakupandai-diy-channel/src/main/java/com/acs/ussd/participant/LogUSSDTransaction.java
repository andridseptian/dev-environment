package com.acs.ussd.participant;

import com.acs.ussd.entity.Registration;
import com.acs.ussd.entity.Session;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.spring.SpringImpl;
import com.acs.ussd.entity.enums.Constant;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.json.JSONObject;

/**
 *
 * @author Erwin
 */
public class LogUSSDTransaction implements AbortParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    /**
     *
     * @param l
     * @param srlzbl
     * @context TXLOG
     *
     */
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        Session sesi = (Session) ctx.get(Constant.SESSION);
        String freeText = sesi.getFreeText();
        JSONObject ft = new JSONObject(freeText);
        if(ft.has("pc")){
            if(ft.getString("pc").equals("100000")){
                Registration reg = new Registration();
                reg.setMsisdnNasabah(ft.getString("lakupandai_jatim_aktivasi_nohp"));
                reg.setNama(ft.getString("lakupandai_jatim_aktivasi_nama"));
                reg.setMsisdnAgen(ft.getString("msisdn"));
                reg.setDc("USSD-LAKU-PANDAI");
                reg.setBankCode(trx.getClient().getClientCode());
                reg.setProvider(trx.getProvider());
                reg.setReff(trx.getClientReff());
                reg.setRc(trx.getClientRc());
                reg.setRm(trx.getMsgContent());
                reg.setFreeText(trx.getReqMessage());
                
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    reg.setDtm(df.parse(df.format(new Date())));
                } catch (ParseException ex) {
                    Logger.getLogger(LogUSSDTransaction.class.getName()).log(Level.SEVERE, null, ex);
                }
                SpringImpl.getRegDao().save(reg);
            }
        }
        trx.setTrxInfo(ctx.getString(Constant.TRX_INFO));
        if (trx.getSendingTime() != null && trx.getRequestTime() != null) {
            trx.setElapsed(trx.getSendingTime().getTime() - trx.getRequestTime().getTime());
        }
        try {
//            log.info(trx.getStatus().toString());
////            log.info(trx.getId().toString());
//            log.info(trx.getTrxInfo());
//            log.info(trx.getProvider());
//            log.info(trx.getFreeText());
//            log.info(trx.getClient().getClientCode());
//            log.info(trx.getMsgContent());
            SpringImpl.getTransactionDao().save(trx);
        } catch (NullPointerException npe) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            npe.printStackTrace(printWriter);
            log.error(stringWriter.toString());
        }
        LogEvent le = (LogEvent) ctx.get(Constant.LOGEVENT);
        log.info(le);

    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        trx.setTrxInfo(ctx.getString(Constant.TRX_INFO));
        if (trx.getSendingTime() != null && trx.getRequestTime() != null) {
            trx.setElapsed(trx.getSendingTime().getTime() - trx.getRequestTime().getTime());
        }
        SpringImpl.getTransactionDao().save(trx);
        LogEvent le = (LogEvent) ctx.get(Constant.LOGEVENT);
        log.info(le);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

}
