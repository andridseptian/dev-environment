package com.acs.mobile.participant;

import com.acs.ussd.entity.Registration;
import com.acs.ussd.entity.SessionApp;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.spring.SpringImpl;
import com.acs.ussd.entity.enums.Constant;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;

/**
 *
 * @author Erwin
 */
public class LogMobileTransaction implements AbortParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    /**
     *
     * @param l
     * @param srlzbl
     * @context TXLOG
     *
     */
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        trx.setTrxInfo(ctx.getString(Constant.TRX_INFO));
        trx.setPc(ctx.getString("pc"));
        String action = ctx.getString(Constant.SMS);
        SessionApp sessionApp = new SessionApp();
        sessionApp.setAction(action);
        sessionApp.setBankCode(trx.getClient().getClientCode());
        sessionApp.setDtm(new Date());
        sessionApp.setMsisdnAgen(trx.getMsisdn());
        sessionApp.setReff(trx.getClientReff());
        sessionApp.setDc(ctx.getString("CHNNL"));
        sessionApp.setPc(ctx.getString("pc"));
        sessionApp.setReq_msg(ctx.getString("BODY_RAW"));
        if (trx.getSendingTime() != null && trx.getRequestTime() != null) {
            trx.setElapsed(trx.getSendingTime().getTime() - trx.getRequestTime().getTime());
        }
        
        try {
            if(action.equals("generate_token_activate_account")){
                Registration reg = (Registration) ctx.get("ACC_REGISTER");
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                reg.setDtm(df.parse(df.format(new Date())));
                reg.setDtm(new Date());
                reg.setRc(trx.getClientRc());
                reg.setRm(trx.getMsgContent());
                reg.setFreeText(trx.getReqMessage());
                reg.setReff(trx.getClientReff());
                SpringImpl.getRegDao().save(reg);
                SpringImpl.getTransactionDao().save(trx);
            }else if(action.equals("login") || action.equals("check_nik")){
                sessionApp.setStatus(trx.getMsgContent());
            }else{
                SpringImpl.getTransactionDao().save(trx);
            }
            SpringImpl.getSessionAppDao().save(sessionApp);
        } catch (NullPointerException npe) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            npe.printStackTrace(printWriter);
            log.error(stringWriter.toString());
        } 
//        catch (ParseException ex) {
//            Logger.getLogger(LogMobileTransaction.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        LogEvent le = (LogEvent) ctx.get(Constant.LOGEVENT);
//        log.info(le);

    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        trx.setTrxInfo(ctx.getString(Constant.TRX_INFO));
        if (trx.getSendingTime() != null && trx.getRequestTime() != null) {
            trx.setElapsed(trx.getSendingTime().getTime() - trx.getRequestTime().getTime());
        }
        SpringImpl.getTransactionDao().save(trx);
        LogEvent le = (LogEvent) ctx.get(Constant.LOGEVENT);
        log.info(le);
    }

    @Override
    public int prepareForAbort(long l, Serializable srlzbl) {
        return ABORTED;
    }

}
