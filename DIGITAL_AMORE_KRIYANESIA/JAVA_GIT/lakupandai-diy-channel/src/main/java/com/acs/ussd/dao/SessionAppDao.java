/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.dao;

import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.SessionApp;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Akses Nusantara
 */
@Repository(value = "sessionAppDao")
@Transactional
public class SessionAppDao {
    
    @Resource(name = "sharedEntityManager")
    private EntityManager em1;
       
    public void save(SessionApp sessionApp) {
        em1.persist(sessionApp);
    }
    
    public SessionApp findLastLogin(String msisdn) {
        try {
            return (SessionApp) em1.createQuery("SELECT m FROM SessionApp AS m WHERE m.msisdnAgen =:msisdnAgen AND m.status =:status ORDER BY m.dtm desc")
                    .setParameter("msisdnAgen", msisdn)
                    .setParameter("status", "LOGIN SUCCESS")
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
