package com.acs.ussd.spring;

import com.acs.sms.dao.InboxDao;
import com.acs.spring.Spring;
import com.acs.ussd.dao.ClientDao;
import com.acs.ussd.dao.MenuDao;
import com.acs.ussd.dao.RequestMappingDao;
import com.acs.ussd.dao.SessionDao;
import com.acs.ussd.dao.TransactionDao;
import com.acs.ussd.service.LoggerService;
import com.acs.ussd.dao.ParseXMLDao;
import com.acs.ussd.dao.PrefixDao;
import com.acs.ussd.dao.RegistrationDao;
import com.acs.ussd.dao.SessionAppDao;
import org.jpos.core.ConfigurationException;

/**
 * Spring Implementation of SMS Broadcast
 *
 * @author Erwin
 */
public class SpringImpl extends Spring {

    private static MenuDao menuDao;
    private static ClientDao clientDao;
    private static SessionDao sessionDao;
    private static RequestMappingDao requestMappingDao;
    private static LoggerService loggerService;
    private static TransactionDao transactionDao;
    private static ParseXMLDao parseXMLDao;
    private static RegistrationDao regDao;
    private static SessionAppDao sessionAppDao;
    private static PrefixDao prefixDao;
   // private static InboxDao inboxDao;

    /**
     * @return the broadcastDao
     */
    public static RequestMappingDao getRequestMappingDao() {
        return requestMappingDao;
    }

    public static void setRequestMappingDao(RequestMappingDao requestMappingDao) {
        SpringImpl.requestMappingDao = requestMappingDao;
    }

    public static MenuDao getMenuDao() {
        return menuDao;
    }

    public static void setMenuDao(MenuDao menuDao) {
        SpringImpl.menuDao = menuDao;
    }

    public static SessionDao getSessionDao() {
        return sessionDao;
    }

    public static void setSessionDao(SessionDao sessionDao) {
        SpringImpl.sessionDao = sessionDao;
    }

    public static ClientDao getClientDao() {
        return clientDao;
    }

    public static void setClientDao(ClientDao clientDao) {
        SpringImpl.clientDao = clientDao;
    }

    public static LoggerService getLoggerService() {
        return loggerService;
    }

    public static void setLoggerService(LoggerService loggerService) {
        SpringImpl.loggerService = loggerService;
    }

    public static TransactionDao getTransactionDao() {
        return transactionDao;
    }

    public static void setTransactionDao(TransactionDao transactionDao) {
        SpringImpl.transactionDao = transactionDao;
    }

    public static ParseXMLDao getParseXMLDao() {
        return parseXMLDao;
    }

    public static void setParseXMLDao(ParseXMLDao parseXMLDao) {
        SpringImpl.parseXMLDao = parseXMLDao;
    }

    public static RegistrationDao getRegDao() {
        return regDao;
    }

    public static void setRegDao(RegistrationDao regDao) {
        SpringImpl.regDao = regDao;
    }

    public static SessionAppDao getSessionAppDao() {
        return sessionAppDao;
    }

    public static void setSessionAppDao(SessionAppDao sessionAppDao) {
        SpringImpl.sessionAppDao = sessionAppDao;
    }

    public static PrefixDao getPrefixDao() {
        return prefixDao;
    }

    public static void setPrefixDao(PrefixDao prefixDao) {
        SpringImpl.prefixDao = prefixDao;
    }

//    public static InboxDao getInboxDao() {
//        return inboxDao;
//    }
//
//    public static void setInboxDao(InboxDao inboxDao) {
//        SpringImpl.inboxDao = inboxDao;
//    }
//    
    
    
    @Override
    public void initService() throws ConfigurationException {
        super.initService();
        setMenuDao((MenuDao) context.getBean("menuDao"));
        setSessionDao((SessionDao) context.getBean("sessionDao"));
        setClientDao((ClientDao) context.getBean("clientDao"));
        setRequestMappingDao((RequestMappingDao) context.getBean("requestMappingDao"));
        setLoggerService((LoggerService) context.getBean("loggerService"));
        setTransactionDao((TransactionDao)context.getBean("transactionDao"));
        setParseXMLDao((ParseXMLDao) context.getBean("parseXMLDao"));
        setRegDao((RegistrationDao) context.getBean("regDao"));
        setSessionAppDao((SessionAppDao) context.getBean("sessionAppDao"));
        setPrefixDao((PrefixDao) context.getBean("prefixDao"));
       // setInboxDao((InboxDao) context.getBean("inboxDao"));
    }
}
