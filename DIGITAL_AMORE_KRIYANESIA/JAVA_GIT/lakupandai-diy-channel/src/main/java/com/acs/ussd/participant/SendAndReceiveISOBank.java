/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.txn.ISOSender;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.TransactionStatus;
import com.acs.util.Utility;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author Akses Nusantara
 */
public class SendAndReceiveISOBank implements TransactionParticipant, Configurable {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        ISOMsg req = new ISOMsg(Constant.MTI_POSTING);
        String pc = cfg.get("pc", "800000");
        String dc = cfg.get("dc", "6027");
        String destination = cfg.get("host");
        ISOMsg hasil = new ISOMsg();
        try {
            trx.setSendingTime(new Date());
            req.setPackager(new ISO87APackager());
            req.set(2, trx.getMsisdn());
            req.set(3, pc);
            req.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
            req.set(11, trx.getStan());
            req.set(32, dc);
            req.set(37, Utility.generateRrn());
            req.set(41, ISOUtil.strpad("USSD", 4));
            req.set(42, trx.getProvider());
            req.set(46, trx.getReqMessage());
            req.set(48, trx.getReqMessage());
            try {
                String[] isoParam = ISOUtil.toStringArray(cfg.get("isomsg"));
                for (String dataParam : isoParam) {
                    if (dataParam.contains(":")) {
                        String[] param = dataParam.split(":");
                        req.set(Integer.parseInt(param[0]), param[1].contains("~") ? ctx.getString(param[1]) : param[1]);
                    }
                }
                hasil = ISOSender.request(destination, req);
                String spaceName = cfg.get("space_name", "jatim-space");
                Space sp = SpaceFactory.getSpace(spaceName); //edit
                long timeout = cfg.getLong("timeout", 30000);
                String[] bitkey = ISOUtil.toStringArray(cfg.get("keyspace", "2 11"));
                String key = "";
                for (String bit : bitkey) {
                    log.info("BITkey " + bit);
                    if (req.hasField(bit)) {
                        key += req.getString(bit);
                    }
                }
                Context newCtx = (Context) sp.in(key, timeout);
                ISOMsg isos = (ISOMsg) newCtx.get(Constant.IN);
                log.info("ISINA" + isos.getValue(48));

                trx.setMsgContent((String) isos.getValue(48));
                String rc = (String) isos.getValue(39);
                trx.setClientRc(rc);
                trx.setClientReff((String) isos.getValue(37));
                trx.setStatus(rc.equals(Constant.RC_APPROVED) || rc.equals(Constant.RC_TIME_OUT) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } catch (ISOException | NumberFormatException ex) {
                log.info("cfg isoparam is not supported : " + cfg.get("isomsg"));
            }
        } catch (NameRegistrar.NotFoundException ex) {
            ctx.put(Constant.TRX_INFO, "UNABLE_TO_ROUTE_TRANSACTION");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_UNABLE_TO_ROUTE_TRANSACTION);
            log.error(ex);
        } catch (ISOException isoe) {
            ctx.put(Constant.TRX_INFO, "FORMAT_ERROR");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_FORMAT_ERROR);
            log.error(isoe);
        } catch (NullPointerException noe) {
            ctx.put(Constant.TRX_INFO, "NULL");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_UNKNOWN_ERROR);
            log.error(noe);
        } finally {
            ctx.put(Constant.IN, hasil);
            ctx.put(Constant.OUT, hasil);
            ctx.put(Constant.TRX, trx);
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
