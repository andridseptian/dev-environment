/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.entity;

import com.acs.ussd.entity.enums.MenuType;
import com.acs.ussd.entity.enums.SessionType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Akses Nusantara
 */
@Entity
@Table(name = "req_mapping")
public class RequestMapping {
    
    @Id
    @Column(name = "path_info")
    private String pathInfo;
    
    @ManyToOne
    @JoinColumn(name = "client", nullable = false)
    private Client client;
    
    @Column(name = "provider", nullable = false)
    private String provider;
    
    @Column(name = "url_proxy", nullable = true)
    private String url_proxy;
    
    @Column(name="xml_path_menu")
    private String xmlPathMenu;
    
    @Column(name="xml_path_info")
    private String xmlPathInfo;
    
    @Column(name="xml_path_input")
    private String xmlPathInput;
    
    @Column(name="default_group_menu")
    private String defaultGroup;
       
    @Column(name = "session_type")
    @Enumerated(EnumType.STRING)
    private SessionType sessionType;
        
    @Column(name = "exp_session_in_milis")
    private Long expTime;
    
    @Column(name = "parameter_msisdn")
    private String parameterMSISDN;
    
    @Column(name = "parameter_input")
    private String parameterInput;
    
    @Column(name = "parameter_session")
    private String parameterSession;

    public String getParameterMSISDN() {
        return parameterMSISDN;
    }

    public void setParameterMSISDN(String parameterMSISDN) {
        this.parameterMSISDN = parameterMSISDN;
    }

    public String getParameterInput() {
        return parameterInput;
    }

    public void setParameterInput(String parameterInput) {
        this.parameterInput = parameterInput;
    }

    public String getParameterSession() {
        return parameterSession;
    }

    public void setParameterSession(String parameterSession) {
        this.parameterSession = parameterSession;
    }
    
    public String getDefaultGroup() {
        return defaultGroup;
    }

    public void setDefaultGroup(String defaultGroup) {
        this.defaultGroup = defaultGroup;
    }

    public String getPathInfo() {
        return pathInfo;
    }

    public void setPathInfo(String pathInfo) {
        this.pathInfo = pathInfo;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getUrl_proxy() {
        return url_proxy;
    }

    public void setUrl_proxy(String url_proxy) {
        this.url_proxy = url_proxy;
    }

    public String getXmlPathMenu() {
        return xmlPathMenu;
    }

    public void setXmlPathMenu(String xmlPathMenu) {
        this.xmlPathMenu = xmlPathMenu;
    }

    public String getXmlPathInfo() {
        return xmlPathInfo;
    }

    public void setXmlPathInfo(String xmlPathInfo) {
        this.xmlPathInfo = xmlPathInfo;
    }

    public String getXmlPathInput() {
        return xmlPathInput;
    }

    public void setXmlPathInput(String xmlPathInput) {
        this.xmlPathInput = xmlPathInput;
    }

    public SessionType getSessionType() {
        return sessionType;
    }

    public void setSessionType(SessionType sessionType) {
        this.sessionType = sessionType;
    }

    public Long getExpTime() {
        return expTime;
    }

    public void setExpTime(Long expTime) {
        this.expTime = expTime;
    }
}
