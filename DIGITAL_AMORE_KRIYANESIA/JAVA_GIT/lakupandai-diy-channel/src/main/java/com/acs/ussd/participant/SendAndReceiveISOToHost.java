/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.txn.ISOSender;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.TransactionStatus;
import com.acs.util.Utility;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author Akses Nusantara
 */
public class SendAndReceiveISOToHost implements TransactionParticipant, Configurable {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        ISOMsg req = new ISOMsg(Constant.MTI_POSTING);
        String pc = cfg.get("pc", "800000");
        String dc = cfg.get("dc", "6027");
        String destination = cfg.get("host");
        int pathRc = cfg.getInt("path_rc", 39);
        int pathClientReff = cfg.getInt("path_reff", 37);
        int pathMessage = cfg.getInt("path_message", 61);
        ISOMsg hasil = new ISOMsg();
        try {
            if(trx.getMsisdn().equals("6281214026017")){
                req.set(2, "62816812946");
            }else{
                req.set(2, trx.getMsisdn());
            }
            trx.setSendingTime(new Date());
            req.setPackager(new ISO87APackager());
//            req.set(2, trx.getMsisdn());
            req.set(3, pc);
            req.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
            req.set(11, trx.getStan());
            req.set(32, dc);
            req.set(37, Utility.generateRrn());
            req.set(41, ISOUtil.strpad("USSD", 4));
            req.set(42, trx.getProvider());
            req.set(46, trx.getReqMessage());
            req.set(48, trx.getReqMessage());
            hasil = ISOSender.request(destination, req);
            if (hasil != null && hasil.hasField(pathMessage) && hasil.hasField(pathRc) && hasil.hasField(pathClientReff)) {
                String rsp = hasil.getString(pathMessage);
                rsp = rsp.replace("<", "&lt;");
                rsp = rsp.replace(">", "&gt;");
                
                trx.setMsgContent(rsp);
                String rc = hasil.getString(pathRc);
                trx.setClientRc(rc);
                trx.setClientReff(hasil.getString(pathClientReff));
                trx.setStatus(rc.equals(Constant.RC_APPROVED) || rc.equals(Constant.RC_TIME_OUT) ? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
            } else {
                ctx.put(Constant.TRX_INFO, pathMessage + "," + pathRc + "," + pathClientReff + " not found in response");
                trx.setStatus(TransactionStatus.FAILED);
                trx.setClientRc(Constant.RC_MISSING_MANDATORY_PARAMETER);
            }
        } catch (NameRegistrar.NotFoundException ex) {
            ctx.put(Constant.TRX_INFO, "UNABLE_TO_ROUTE_TRANSACTION");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_UNABLE_TO_ROUTE_TRANSACTION);
            log.error(ex);
        } catch (ISOException isoe) {
            ctx.put(Constant.TRX_INFO, "FORMAT_ERROR");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_FORMAT_ERROR);
            log.error(isoe);
        } catch (NullPointerException noe) {
            ctx.put(Constant.TRX_INFO, "NULL");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_UNKNOWN_ERROR);
            log.error(noe);
        } finally {
            ctx.put(Constant.OUT, hasil);
            ctx.put(Constant.TRX, trx);
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

}
