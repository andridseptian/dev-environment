/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.sms.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author Erwin
 */
@Entity
@Table(name = "inbox")
public class Inbox implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "UpdatedInDB")
    private Date updatedInDB;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "ReceivingDateTime")
    private Date receivingDateTime;
    
    @Column(name = "Text")
    @Type(type = "text")
    private String text;

    @Column(name = "SenderNumber", length = 20)
    private String senderNumber;

    @Column(name = "UDH")
    @Type(type = "text")
    private String udh;

    @Column(name = "SMSCNumber", length = 20)
    private String smscNumber;
    
    @Column(name = "TextDecoded")
    @Type(type = "text")
    private String textDecoded;
    
    @Column(name = "RecipientID")
    @Type(type = "text")
    private String recipientID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdatedInDB() {
        return updatedInDB;
    }

    public void setUpdatedInDB(Date updatedInDB) {
        this.updatedInDB = updatedInDB;
    }

    public Date getReceivingDateTime() {
        return receivingDateTime;
    }

    public void setReceivingDateTime(Date receivingDateTime) {
        this.receivingDateTime = receivingDateTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getUdh() {
        return udh;
    }

    public void setUdh(String udh) {
        this.udh = udh;
    }

    public String getSmscNumber() {
        return smscNumber;
    }

    public void setSmscNumber(String smscNumber) {
        this.smscNumber = smscNumber;
    }

    public String getTextDecoded() {
        return textDecoded;
    }

    public void setTextDecoded(String textDecoded) {
        this.textDecoded = textDecoded;
    }

    public String getRecipientID() {
        return recipientID;
    }

    public void setRecipientID(String recipientID) {
        this.recipientID = recipientID;
    }
}
