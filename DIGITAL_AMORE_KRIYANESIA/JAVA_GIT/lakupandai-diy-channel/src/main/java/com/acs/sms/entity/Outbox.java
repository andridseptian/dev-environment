/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.sms.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.annotations.Type;

/**
 *
 * @author Erwin
 */
@Entity
@Table(name = "outbox")
public class Outbox implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "UpdatedInDB")
    private Date updatedInDB;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "InsertIntoDB")
    private Date insertIntoDB;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "SendingDateTime")
    private Date sendingDateTime;

    @Temporal(javax.persistence.TemporalType.TIME)
    @Column(name = "SendBefore")
    private Date sendBefore;

    @Temporal(javax.persistence.TemporalType.TIME)
    @Column(name = "SendAfter")
    private Date sendAfter;

    @Column(name = "Text")
    @Type(type = "text")
    private String text;

    @Column(name = "DestinationNumber", length = 20)
    private String destinationNumber;

    @Column(name = "Coding", length = 50)
    private String coding;

    @Column(name = "UDH")
    @Type(type = "text")
    private String udh;

    @Column(name = "Class")
    private Integer clazz;

    @Column(name = "TextDecoded")
    @Type(type = "text")
    private String textDecoded;

//    @Column(name = "MultiPart")
//    private Boolean multiPart;
//    @Column(name = "MultiPart")
//    @Type(type = "org.hibernate.type.NumericBooleanType")
//    private boolean multiPart;

    @Column(name = "RelativeValidity")
    private Integer relativeValidity;

    @Column(name = "SenderID", length = 255)
    private String senderId;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "SendingTimeOut")
    private Date sendingTimeOut;

    @Column(name = "DeliveryReport", length = 10)
    private String deliveryReport;

    @Column(name = "CreatorID")
    @Type(type = "text")
    private String creatorId;

    public Outbox() {
        Date currentTimestamp = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String sendBeforeTime = "23:59:59";
        String sendAfterTime = "00:00:00";
        updatedInDB = currentTimestamp;
        insertIntoDB = currentTimestamp;
        sendingDateTime = currentTimestamp;
        try {
            sendBefore = sdf.parse(sendBeforeTime);
            sendAfter = sdf.parse(sendAfterTime);
        } catch (ParseException ex) {
            Logger.getLogger(Outbox.class.getName()).log(Level.ERROR, null, ex);
        }
        coding = "Default_No_Compression";
        //multiPart = false;
        deliveryReport = "default";
        clazz = -1;
        relativeValidity = -1;
        sendingTimeOut = currentTimestamp;
        textDecoded = "";
        creatorId = "default-sender";
    }

    /**
     * @return the updatedInDB
     */
    public Date getUpdatedInDB() {
        return updatedInDB;
    }

    /**
     * @param updatedInDB the updatedInDB to set
     */
    public void setUpdatedInDB(Date updatedInDB) {
        this.updatedInDB = updatedInDB;
    }

    /**
     * @return the insertIntoDB
     */
    public Date getInsertIntoDB() {
        return insertIntoDB;
    }

    /**
     * @param insertIntoDB the insertIntoDB to set
     */
    public void setInsertIntoDB(Date insertIntoDB) {
        this.insertIntoDB = insertIntoDB;
    }

    /**
     * @return the sendingDateTime
     */
    public Date getSendingDateTime() {
        return sendingDateTime;
    }

    /**
     * @param sendingDateTime the sendingDateTime to set
     */
    public void setSendingDateTime(Date sendingDateTime) {
        this.sendingDateTime = sendingDateTime;
    }

    /**
     * @return the sendBefore
     */
    public Date getSendBefore() {
        return sendBefore;
    }

    /**
     * @param sendBefore the sendBefore to set
     */
    public void setSendBefore(Date sendBefore) {
        this.sendBefore = sendBefore;
    }

    /**
     * @return the sendAfter
     */
    public Date getSendAfter() {
        return sendAfter;
    }

    /**
     * @param sendAfter the sendAfter to set
     */
    public void setSendAfter(Date sendAfter) {
        this.sendAfter = sendAfter;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the destinationNumber
     */
    public String getDestinationNumber() {
        return destinationNumber;
    }

    /**
     * @param destinationNumber the destinationNumber to set
     */
    public void setDestinationNumber(String destinationNumber) {
        this.destinationNumber = destinationNumber;
    }

    /**
     * @return the coding
     */
    public String getCoding() {
        return coding;
    }

    /**
     * @param coding the coding to set
     */
    public void setCoding(String coding) {
        this.coding = coding;
    }

    /**
     * @return the udh
     */
    public String getUdh() {
        return udh;
    }

    /**
     * @param udh the udh to set
     */
    public void setUdh(String udh) {
        this.udh = udh;
    }

    /**
     * @return the clazz
     */
    public int getClazz() {
        return clazz;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(int clazz) {
        this.clazz = clazz;
    }

    /**
     * @return the textDecoded
     */
    public String getTextDecoded() {
        return textDecoded;
    }

    /**
     * @param textDecoded the textDecoded to set
     */
    public void setTextDecoded(String textDecoded) {
        this.textDecoded = textDecoded;
    }

    /**
     * @return the relativeValidity
     */
    public Integer getRelativeValidity() {
        return relativeValidity;
    }

    /**
     * @param relativeValidity the relativeValidity to set
     */
    public void setRelativeValidity(Integer relativeValidity) {
        this.relativeValidity = relativeValidity;
    }

    /**
     * @return the senderId
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     * @param senderId the senderId to set
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     * @return the sendingTimeOut
     */
    public Date getSendingTimeOut() {
        return sendingTimeOut;
    }

    /**
     * @param sendingTimeOut the sendingTimeOut to set
     */
    public void setSendingTimeOut(Date sendingTimeOut) {
        this.sendingTimeOut = sendingTimeOut;
    }

    /**
     * @return the deliveryReport
     */
    public String getDeliveryReport() {
        return deliveryReport;
    }

    /**
     * @param deliveryReport the deliveryReport to set
     */
    public void setDeliveryReport(String deliveryReport) {
        this.deliveryReport = deliveryReport;
    }

    /**
     * @return the creatorId
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * @param creatorId the creatorId to set
     */
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    /**
     * @return the multiPart
     */
//    public Boolean getMultiPart() {
//        return multiPart;
//    }

    /**
     * @param multiPart the multiPart to set
     */
//    public void setMultiPart(Boolean multiPart) {
//        this.multiPart = multiPart;
//    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

}
