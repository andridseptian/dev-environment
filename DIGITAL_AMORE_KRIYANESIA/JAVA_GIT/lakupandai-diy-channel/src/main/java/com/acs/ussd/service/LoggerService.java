/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.service;

import com.acs.ussd.dao.MLoggerDao;
import com.acs.ussd.entity.MLogger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author EliteBook
 */
@Service(value = "loggerService")
public class LoggerService {
    
    @Autowired
    private MLoggerDao mLoggerDao;
    
    @Transactional
    public void logMsg(MLogger ml) {
        mLoggerDao.save(ml);
    }
    
    public List<MLogger> find(String keyword, int first, int max) {
        return mLoggerDao.find(keyword, first, max);
    }
    
    public Integer count(String keyword) {
        return mLoggerDao.count(keyword);
    }
    
}
