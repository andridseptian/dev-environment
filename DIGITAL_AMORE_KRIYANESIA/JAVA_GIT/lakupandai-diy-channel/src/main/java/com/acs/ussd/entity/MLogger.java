/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author EliteBook
 */
@Entity
@Table(name = "m_logger")
public class MLogger implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "dcms.m_logger_id_seq")
    @SequenceGenerator(name = "dcms.m_logger_id_seq", sequenceName = "dcms.m_logger_id_seq")
    @Column(name="logger_id", nullable = false)
    private Long id;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "log_time")
    private Date logTime;
    
    @Column(name = "incoming", length = 50)
    private Boolean incoming;
    
    @Column(name = "msg")
    @Type(type = "text")
    private String msg;
    
    @Column(name = "host_address", length = 20)
    private String hostAddress;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the logTime
     */
    public Date getLogTime() {
        return logTime;
    }

    /**
     * @param logTime the logTime to set
     */
    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the incoming
     */
    public Boolean getIncoming() {
        return incoming;
    }

    /**
     * @param incoming the incoming to set
     */
    public void setIncoming(Boolean incoming) {
        this.incoming = incoming;
    }

    /**
     * @return the hostAddress
     */
    public String getHostAddress() {
        return hostAddress;
    }

    /**
     * @param hostAddress the hostAddress to set
     */
    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }
    
}
