/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.entity.enums;

/**
 *
 * @author Erwin
 */
public enum TransactionStatus {
    
    SUCCESS("1"), FAILED("0"), PENDING("2");
    
    private String status;
    
    private TransactionStatus(String status) {
        this.status = status;
    }
    
    @Override
    public String toString() {
        return getStatus();
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
}
