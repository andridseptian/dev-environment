// Copyright (c) Micro Pay Nusantara 2010

package com.acs.ussd.channel;

import com.acs.util.Utility;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.q2.iso.QMUX;
import org.jpos.util.NameRegistrar;
import org.jpos.util.NameRegistrar.NotFoundException;

/**
 *
 * @author Erwin
 */
public class AutoSignonASCIIChannel extends ASCIIChannel {
    
    public AutoSignonASCIIChannel() {
        super();
    }
    
    @Override
    public void connect() {
        try {
            super.connect();
            doLogOn();
        } catch (NotFoundException ex) {
            Logger.getLogger(AutoSignonASCIIChannel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void doLogOn() throws NotFoundException {
        try {
            ISOMsg r = createMsg();
            r.setMTI("0800");
            r.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
            r.set(11, Utility.generateStan());
            r.set(70, "001");
            if (this.isConnected()) {
                QMUX mux = (QMUX) NameRegistrar.get("mux." + this.getName() + "-mux");
                ISOMsg rcv;
                rcv = mux.request(r, 5000);
                if (rcv == null) {
                } else {
                    if (rcv.getString(39).equals("00")) {
                    } else {
                    }
                }
            }
        } catch (ISOException ex) {
            ex.printStackTrace();
        }
    }

}
