/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;


import com.acs.http.HttpChannelMsg;
import com.acs.http.HttpSender;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author yogha
 */
public class SendMessageToPortal implements TransactionParticipant, Configurable  {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        String telco = trx.getProvider();
        String msisdn = trx.getMsisdn();
        String sms = trx.getMsgContent();
        String bankCode = trx.getClient().getClientCode();
        String transId = trx.getStan();
        String transDate = Constant.FORMAT_BANK_DATE.format(new Date());
        String noSid = cfg.get("sid");
        String destination = cfg.get("destination", "telcoportal");
        String additionalUrl = cfg.get("additionalUrl", "/MTRequestUSSDBPRKS");
        int timeout = cfg.getInt("timeout",30000);
        try {
            if (msisdn.startsWith("620")) {
                msisdn = "62" + msisdn.substring(3);
            } else if (msisdn.startsWith("0")) {
                msisdn = "62" + msisdn.substring(1);
            } 
            String reqParam = "telco=" + URLEncoder.encode(telco, "UTF-8")
                    + "&msisdn=" + URLEncoder.encode(msisdn, "UTF-8")
                    + "&sms=" + URLEncoder.encode(sms, "UTF-8")
                    + "&bank_code=" + URLEncoder.encode(bankCode, "UTF-8")
                    + "&trans_id=" + URLEncoder.encode(transId, "UTF-8")
                    + "&trans_date=" + URLEncoder.encode(transDate, "UTF-8")
                    + "&no_sid=" + URLEncoder.encode(noSid, "UTF-8");
            HttpChannelMsg req = new HttpChannelMsg();
            req.setUrlParam(reqParam);
            HttpChannelMsg rsp = HttpSender.request(destination, req, additionalUrl, "get",timeout);
            if (rsp.getTxStatusCode() == Constant.SUCCESS) {
                
                log.info(rsp.getContent());
            }else{
                log.info(rsp.getContent());
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SendMessageToPortal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NameRegistrar.NotFoundException ex) {
            Logger.getLogger(SendMessageToPortal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SendMessageToPortal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
    
}
