/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.entity.enums;

import java.text.SimpleDateFormat;

/**
 *
 * @author Akses Nusantara
 */
public class Constant {

    public static long SESSION_LIMIT_INDOSAT = 60000;
    public static long SESSION_LIMIT_TELKOMSEL = 40000;
    public static long JATIM_DEFAULT_MENU_INDOSAT = 3366;
    public static String CHECK_PREFIX_IP = "http://172.168.100.6";
    public static int CHECK_PREFIX_PORT = 14022;
    public static String DEFAULT_BACK_CODE = "9";
    public static int DEFAULT_MAX_MULTIPLE_PAGE = 145;

    //Provider
    public static final String INDOSAT = "SAT";
    public static final String TELKOMSEL = "TSEL";
    public static final String XL = "XL";

    public static final String JATIM_URL_USSD_INDOSAT = "http://10.138.101.4:3889/umb/isat/bpdjatim";
    public static final String BPRKS_URL_USSD_INDOSAT = "http://10.138.101.4:3889/umb/isat/bprks";

    public static final String TRX = "TRX";
    public static final String SESSION = "SESSION";
    public static final String SELECTED_MENU = "selected_menu";
    public static final String RESPONSE_MENU = "response_menu";
    public static final String MSG = "MSG";
    public static final String LOGEVENT = "LE";
    public static final String TRX_INFO = "INFOTRX";
    public static final String PROVIDER = "PROVIDER";
    public static final String MSISDN = "MSISDN";
    public static final String SMS = "SMS";

    public static int ISO_FIELD_PAN = 2;
    public static int ISO_FIELD_PROCESSING_CODE = 3;
    public static int ISO_FIELD_TX_AMOUNT = 4;
    public static int ISO_FIELD_SETTLE_AMOUNT = 5;
    public static int ISO_FIELD_DATETIME = 7;
    public static int ISO_FIELD_STAN = 11;
    public static int ISO_FIELD_TIME = 12;
    public static int ISO_FIELD_DATE = 13;
    public static int ISO_FIELD_SETTLE_DATE = 15;
    public static int ISO_FIELD_CHANNEL_TYPE = 18;
    public static int ISO_FIELD_TID = 41;
    public static int ISO_FIELD_MID = 42;
    public static int ISO_FIELD_TRACK2 = 35;
    public static int ISO_FIELD_RRN = 37;
    public static int ISO_FIELD_RC = 39;
    public static int ISO_FIELD_PRIVATE_DATA = 48;
    public static int ISO_FIELD_CURRENCY = 49;
    public static int ISO_FIELD_ACC_INFO = 54;
    public static int ISO_FIELD_IGATE_FEATURE_CODE = 56;
    public static int ISO_FIELD_AMS_FEATURE_CODE = 63;
    public static int ISO_FIELD_NETWORK_CODE = 70;
    public static int ISO_FIELD_DESTINATION_BANK_CODE = 100;
    public static int ISO_FIELD_ACC_IDENTIFICATION1 = 102;
    public static int ISO_FIELD_ACC_IDENTIFICATION2 = 103;
    public static int ISO_FIELD_TX_INFO = 104;
    public static int ISO_FIELD_ACC_NAME1 = 105;
    public static int ISO_FIELD_ACC_NAME2 = 106;
    public static int ISO_FIELD_CUST_REFF = 126;
    public static int ISO_FIELD_ACQUIRING_INSTITUTION_CODE = 32;
    public static int ISO_FIELD_FORWARDING_INSTITUTION_CODE = 33;

    public static String RC_APPROVED = "00";
    public static String RC_INTERNAL_ERROR = "06";
    public static String RC_TRANSACTION_AUTHENTICATION_FAILED = "07";
    public static String RC_INSUFFICIENT_BALANCE = "11";
    public static String RC_ACCOUNT_NOT_FOUND = "12";
    public static String RC_ACCOUNT_NOT_ACTIVE = "13";
    public static String RC_SECONDARY_ACCOUNT_NOT_FOUND = "14";
    public static String RC_SECONDARY_ACCOUNT_NOT_ACTIVE = "15";
    public static String RC_ACCOUNT_BLOCKED = "16";
    public static String RC_PARTNER_NOT_FOUND = "17";
    public static String RC_INVALID_HOST = "18";
    public static String RC_TRANSACTION_NOT_FOUND = "21";
    public static String RC_TRANSACTION_ALREADY_PAID = "22";
    public static String RC_CARD_OVERLIMIT = "23";
    public static String RC_INVALID_PRODUCT = "31";
    public static String RC_INVALID_TRANSACTION_CHANNEL = "32";
    public static String RC_TIME_OUT = "50";
    public static String RC_UNKNOWN_ERROR = "53";
    public static String RC_DB_ERROR = "56";
    public static String RC_MISSING_MANDATORY_PARAMETER = "60";
    public static String RC_CONN_DOWN = "69";
    public static String RC_FORMAT_ERROR = "70";
    public static String RC_UNABLE_TO_ROUTE_TRANSACTION = "73";
    public static String RC_REPRINT_NOT_AVAILABLE = "80";
    public static String RC_TRANSACTION_IS_FAILED = "81";

    //ISO8583 1987 MTI
    public static String MTI_POSTING = "0200";
    public static String MTI_INQUIRY = "0100";
    public static String MTI_REVERSAL = "0400";

    // Context
    public static String RC = "RC";
    public static String RSP = "RSP";
    public static String SRC = "SRC";
    public static String IN = "IN";
    public static String OUT = "OUT";
    public static String ORIGIN = "ORIGIN";
    public static String DESTINATION = "DEST";
    public static String RM = "RM";
    public static final String TELCO = "telco";
    public static final String SID = "sid";
    public static final String HOST = "host";
    public static final String BANKCODE = "bankcode";
    public static final String SMS_STATUS = "sms";
    public static final String MULTIPLE_SMS = "multiple_sms";
    public static final String SPLIT_VALUE = "split_value";

    // menu default error
    public static final String ERROR_DATA = "9990";

    public static final SimpleDateFormat FORMAT_BANK_DATE = new SimpleDateFormat("MMddHHmmss");

    // Transaction Status
    public static int SUCCESS = 1;
    public static int FAILED = 0;
    public static int PENDING = 2;
    public static int REVERSED = 3;
    public static int SYNC_REQUIRED = 5;
}
