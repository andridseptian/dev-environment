/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.web;

import com.acs.ussd.entity.Client;
import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.Session;
import com.acs.ussd.entity.InputLogger;
import com.acs.ussd.entity.ParseXML;
import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.MenuType;
import com.acs.ussd.parser.XMLParser;
import com.acs.ussd.spring.SpringImpl;
import com.dcms.iso.filter.XmlFormatter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jpos.q2.QBeanSupport;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.LogEvent;
import org.json.JSONException;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import static spark.Spark.get;

/**
 *
 * @author Akses Nusantara
 */
public class USSDController extends QBeanSupport {

    private Route ussdGetRoute;
//            , ussdPostRoute;

    @Override
    protected void initService() throws Exception {
        final int port = cfg.getInt("port", 8889);
        Spark.setPort(port); //Set port default 8889
        ussdGetRoute = new Route("/*") {
            @Override
            public Object handle(Request request, Response response) {
                String path = request.pathInfo();
                RequestMapping req = SpringImpl.getRequestMappingDao().find(path);
                if (req != null) {
                    switch (req.getProvider()) {
                        case "TSEL":
                            return telkomselRequest(req, port, request, response);
                        case "SAT":
                            return indosatRequest(req, request, response);
                        default:
                            return "provider not supported";
                    }
                } else {
                    log.info("Get Request on : " + request.url() + "?" + request.queryString()
                        +"\n Response : Request not found");
                    return "request not found";
                }
            }
        };
    }

    public String indosatRequest(RequestMapping req, Request request, Response response) {
        if (req != null) {
            String rsp = "";
            XMLParser xml = new SATParser(req);
            LogEvent le = new LogEvent();
            Session session = new Session();
            Client client = req.getClient();
            String msisdn = request.queryParams(req.getParameterMSISDN());
            String input = request.queryParams(req.getParameterInput());
            String messageid = request.queryParams(req.getParameterSession());
            if (msisdn != null && input != null && messageid != null) {
                //parsing number
                if (msisdn.startsWith("0")) {
                    msisdn = "62" + msisdn.substring(1);
                }
                try {
                    le.addMessage("Get Request on : " + request.url() + "?" + request.queryString());
                    session = SpringImpl.getSessionDao().findSession(msisdn, client.getClientCode(), messageid, req.getProvider());
                    InputLogger inputLogger = new InputLogger(session, input);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    //check new session
                    if (session != null && session.isActive()) {
                        session.setActive(session.getExpDate().after(new Date()));
                        session.setClientReff(messageid);
                        SpringImpl.getSessionDao().save(session, req);
                    }
                    // generate default menu
                    if (session == null || !session.isActive()) {
                        session = new Session(msisdn, client.getClientCode(), req.getDefaultGroup(), req.getProvider(), messageid);
                        List<Menu> menuUtama = SpringImpl.getMenuDao().findByClientAndGroup(client.getClientCode(), req.getDefaultGroup());
                        session.setLastMenuId(Menu.getHeader(menuUtama).getId());
                        rsp = xml.generateMenu(menuUtama);
                        le.addMessage("Send Response : " + rsp != null
                                ? new XmlFormatter().format(rsp)
                                : rsp);
                        return rsp;
                        // generate menu by input
                    } else if (session.isActive()) {
                        log.info("session active");
                        Menu lastMenu = SpringImpl.getMenuDao().findById(session.getLastMenuId() == null ? Long.valueOf(1) : session.getLastMenuId());
                        le.addMessage("LAST MENU : " + lastMenu.getId() + " | " + lastMenu.getDisplayName());
                        rsp = generateResponse(req, inputLogger, lastMenu, session, xml);
                        le.addMessage("Send Response : " + rsp != null
                                ? new XmlFormatter().format(rsp)
                                : rsp);
                        return rsp;
                    } else {
                        return xml.generateDefaultResponse();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                    SpringImpl.getSessionDao().setInactive(session);
                    return xml.generateDefaultResponse();
                } finally {
                    log.info(le);
                    SpringImpl.getSessionDao().save(session, req);
                }
            } else {
                return "parameter not supported";
            }
        } else {
            return request.pathInfo() + " not supported";
        }
    }

    public String telkomselRequest(RequestMapping req, int port, Request request, Response response) {
        if (req != null) {
            String rsp = "";
            XMLParser xml = new TSELParser(req);
            Session session = new Session();
            Client client = req.getClient();
            String msisdn = request.queryParams(req.getParameterMSISDN());
            String input = request.queryParams(req.getParameterInput());
            String messageid = request.queryParams(req.getParameterSession());
            LogEvent le = new LogEvent();
            try {
                //LONG DIAL
                String array[] = input.split("\\*");
                int count = array.length;
                //tambah case input != *806*5#
                if (count > 2 && !"*141*500#".equals(input) && !"*806*5#".equals(input)) {
                    log.info("Get longdial-request on : " + request.url() + "?" + request.queryString());
                    String output = "";
                    String hasil = "";
                    String data = "";
                    String host = "";
                    host = "http://localhost:" + port + req.getPathInfo();

                    for (int i = 2; i < count; i++) {
                        if (i == (count - 1)) {
                            array[i] = array[i].substring(0, array[i].length() - 1);
                        }
                        data = "";
                        data += "&msisdn=" + msisdn;
                        data += "&command=" + array[i];
                        data += "&umb_session_id=" + messageid;

                        URL url = new URL(host + "?" + data);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        hasil = "";
                        output = "";
                        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                        while ((output = br.readLine()) != null) {
                            hasil = hasil + output;
                        }
                        conn.disconnect();
                    }
                    return hasil;
                }
                //END LONG DIAL
            } catch (IOException ioe) {
                log.error("ERROR LONG DIAL " + req.getProvider() + " : " + ioe.getMessage());
            }
            //message id (TSEL) not found, diset dari MSISDN
            if (messageid == null) {
                messageid = msisdn;
            }
            if (msisdn != null && input != null && messageid != null) {
                //parsing number
                if (msisdn.startsWith("0")) {
                    msisdn = "62" + msisdn.substring(1);
                }
                try {
                    le.addMessage("Get Request on : " + request.url() + "?" + request.queryString());
                    session = SpringImpl.getSessionDao().findSession(msisdn, client.getClientCode(), messageid, req.getProvider());
                    if (session != null && input.equals("0")) {
                        session.setActive(Boolean.FALSE);
                        SpringImpl.getSessionDao().save(session, req);
                        return "reset accepted";
                    }
                    InputLogger inputLogger = new InputLogger(session, input);

                    //check new session
                    if (session != null && session.isActive()) {
                        session.setActive(session.getExpDate().after(new Date()));
                        session.setClientReff(messageid);
                        SpringImpl.getSessionDao().save(session, req);
                    }

                    // generate default menu
                    if (session == null || !session.isActive()) {
                        session = new Session(msisdn, client.getClientCode(), req.getDefaultGroup(), req.getProvider(), messageid);
                        List<Menu> menuUtama = SpringImpl.getMenuDao().findByClientAndGroup(client.getClientCode(), req.getDefaultGroup());
                        session.setLastMenuId(Menu.getHeader(menuUtama).getId());
                        rsp = xml.generateMenu(menuUtama);
                        le.addMessage("Send Response : " + rsp != null
                                ? new XmlFormatter().format(rsp)
                                : rsp);
                        return rsp;

                        // generate menu by input
                    } else if (session.isActive()) {
                        // last menu not found, change id to default menu 
                        Menu lastMenu = SpringImpl.getMenuDao().findById(session.getLastMenuId() == null
                                ? Menu.getHeader(SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), req.getDefaultGroup())).getId()
                                : session.getLastMenuId());
                        le.addMessage("LAST MENU : " + lastMenu.getId() + " | " + lastMenu.getDisplayName() + " | Group : " + lastMenu.getGroupMenu());
                        rsp = generateResponse(req, inputLogger, lastMenu, session, xml);
                        le.addMessage("Send Response : " + rsp != null
                                ? new XmlFormatter().format(rsp)
                                : rsp);
                        return rsp;
                    } else {
                        return xml.generateDefaultResponse();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                    SpringImpl.getSessionDao().setInactive(session);
                    return xml.generateDefaultResponse();
                } finally {
                    log.info(le);
                    SpringImpl.getSessionDao().save(session, req);
                }
            } else {
                return "Parameter salah / tidak lengkap";
            }
        } else {
            return request.pathInfo() + " tidak tersedia";
        }
    }

    @Override
    protected void startService() throws Exception {
        get(ussdGetRoute);
//        post(ussdPostRoute);
    }

    @Override
    protected void stopService() throws Exception {

    }

    public String generateResponse(RequestMapping req, InputLogger inputLogger, Menu lastMenu, Session session, XMLParser xml) {
        switch (lastMenu.getType()) {
            case MENU_HEADER:
            case MENU:
                Menu selectedMenu = SpringImpl.getMenuDao().findByGroupAndIndex(req.getClient().getClientCode(), lastMenu.getGroupMenu(), session.getUserInput());
                if (selectedMenu != null) {
                    if (selectedMenu.getAddToSession() != null && selectedMenu.getAddToSession().contains("{")) {
                        try {
                            inputLogger.setJson(InputLogger.mergeJSONObjects(inputLogger.getJson(), new JSONObject(selectedMenu.getAddToSession())));
                            session.setFreeText(inputLogger.getJson().toString());
                            JSONObject jsonData = inputLogger.getJson();
                        } catch (JSONException json) {
                            log.error("Can't add [" + selectedMenu.getAddToSession() + "] to session");
                        }
                        try {
                            if (inputLogger.getJson().has(Constant.SPLIT_VALUE)) {
                                inputLogger.getJson().put(inputLogger.getJson().getString(Constant.SPLIT_VALUE), inputLogger.getJson().getString(selectedMenu.getId().toString()));
                                session.setFreeText(inputLogger.getJson().toString());
                            }
                        } catch (JSONException json) {
                            log.error("Can't add split value[" + lastMenu.getId() + "] to session");
                        }
                        if (inputLogger.getJson().has("next")) {
                            Menu select = SpringImpl.getMenuDao().findByKey(session.getClientCode(), inputLogger.getJson().getString("next"));
                            if (inputLogger.getJson().has("display")) {
                                select.setDisplayName(inputLogger.getJson().getString("display"));
                                inputLogger.getJson().remove("display");
                            }
                            inputLogger.getJson().remove("next");
                            session.setFreeText(inputLogger.getJson().toString());
                            return generateXML(req, inputLogger, select, session, xml);
                        }
                    }
                    switch (selectedMenu.getType()) {
                        case MENU_HEADER:
                        case MENU:
                            Menu childMenu = SpringImpl.getMenuDao().findChild(req.getClient().getClientCode(), selectedMenu);
                            return generateXML(req, inputLogger, childMenu, session, xml);
                        case SEND_VIEW_INFO_FORM:
                        case SEND_VIEW_INPUT_FORM:
                        case SEND_VIEW_INPUT_OR_INFO_BY_RC:
                        case SEND_VIEW_INPUT_MULTIPLE_PAGE:
                        case SEND_VIEW_INFO_MULTIPLE_PAGE:
                        case CHECK_PREFIX:
                            return generateXML(req, inputLogger, selectedMenu, session, xml);
                        case BACK:
                            Menu parentMenu = SpringImpl.getMenuDao().findParent(req.getClient().getClientCode(), selectedMenu);
                            return generateXML(req, inputLogger, parentMenu, session, xml);
                        default:
                            SpringImpl.getSessionDao().setInactive(session);
                            return xml.generateDefaultResponse();
                    }
                } else {
                    SpringImpl.getSessionDao().setInactive(session);
                    return xml.generateDefaultResponse();
                }
            case SEND_VIEW_INPUT_OR_INFO_BY_RC:
                // Check khusus indosat (saldo pulsa tidak ada)
//                if ("SAT".equals(req.getProvider())) {
//                    try {
//                        String request = "MSISDN=" + URLEncoder.encode(session.getMsisdn(), "UTF-8");
//
//                        URL url = new URL("http://172.168.100.6:14048/?" + request);
//                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                        conn.setRequestMethod("GET");
//                        conn.setReadTimeout(40000);
//                        conn.setConnectTimeout(40000);
//
//                        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//                        String hasil = "";
//                        String output = "";
//                        while ((output = br.readLine()) != null) {
//                            hasil = hasil + output;
//                        }
//                        log.info("Response : " + hasil);
//
//                        if (!"1".equals(hasil)) {
//                            Menu m = new Menu();
//                            m.setType(MenuType.INFO);
//                            m.setDisplayName("Insufficient Balance");
//                            session.setActive(Boolean.FALSE);
//                            SpringImpl.getSessionDao().save(session, req);
//                            return xml.generateInfo(m);
//                        }
//                    } catch (UnsupportedEncodingException ex) {
//                        log.info("Error Send SMS" + ex.getMessage());
//                    } catch (MalformedURLException ex) {
//                        Logger.getLogger(USSDController.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (IOException ex) {
//                        Logger.getLogger(USSDController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
            case INPUT:
            case SEND_VIEW_INFO_FORM:
            case SEND_VIEW_INPUT_FORM:
                if (lastMenu.getAddToSession() != null && lastMenu.getAddToSession().contains("{")) {
                    JSONObject jsonData = new JSONObject(lastMenu.getAddToSession());
                    try {
                        //check input Minimal digit 
                        if (jsonData.has("min") && (jsonData.has("if_min") || jsonData.has("if_min_text"))) {
                            int minLength = jsonData.getInt("min");
                            jsonData.remove("min");
                            if (session.getUserInput().length() < minLength) {
                                Menu next = new Menu();
                                if (jsonData.has("if_min")) {
                                    next = SpringImpl.getMenuDao().findByKey(session.getClientCode(), jsonData.getString("if_min"));
                                    jsonData.remove("if_min");
                                } else {
                                    next.setType(MenuType.INFO);
                                    next.setDisplayName(jsonData.getString("if_min_text"));
                                    jsonData.remove("if_min_text");
                                }
                                if (next != null) {
                                    session.setFreeText(jsonData.toString());
                                    return generateXML(req, inputLogger, next, session, xml);
                                }
                            }
                        }
                        //check input Maximal digit
                        if (jsonData.has("max") && (jsonData.has("if_max") || jsonData.has("if_max_text"))) {
                            int maxLength = jsonData.getInt("max");
                            jsonData.remove("max");
                            if (session.getUserInput().length() > maxLength) {
                                Menu next = new Menu();
                                if (jsonData.has("if_max")) {
                                    next = SpringImpl.getMenuDao().findByKey(session.getClientCode(), jsonData.getString("if_max"));
                                    jsonData.remove("if_max");
                                } else {
                                    next.setType(MenuType.INFO);
                                    next.setDisplayName(jsonData.getString("if_max_text"));
                                    jsonData.remove("if_max_text");
                                }
                                if (next != null) {
                                    session.setFreeText(jsonData.toString());
                                    return generateXML(req, inputLogger, next, session, xml);
                                }
                            }
                        }
                    } catch (JSONException jse) {
                        log.error("Can't load [" + lastMenu.getAddToSession() + "] to session, caused by " + jse.getMessage());
                    } catch (Exception ex) {
                        log.error("Can't load [" + lastMenu.getAddToSession() + "] to session, caused by " + ex.getMessage());
                    }
                }
                Menu childMenu = SpringImpl.getMenuDao().findChild(req.getClient().getClientCode(), lastMenu);
                return generateXML(req, inputLogger, childMenu, session, xml);
//            case SEND_VIEW_INFO_MULTIPLE_PAGE:
//            case SEND_VIEW_INPUT_MULTIPLE_PAGE:
//                try {
//                    JSONObject dataMultipage = new JSONObject(session.getFreeText());
//                    if (dataMultipage.has("totalpage_" + lastMenu.getMenuName()) && dataMultipage.has("lastdisplay_" + lastMenu.getMenuName())) {
//                        int totalPage = dataMultipage.getInt("totalpage_" + lastMenu.getMenuName());
//                        int selectedPage = dataMultipage.getInt("lastdisplay_" + lastMenu.getMenuName());
//                        return generateXML(selectedMenu, session);
//                    } else {
//                        Menu childMenuParameterNotFound = SpringImpl.getMenuDao().findChild(req.getClient().getClientCode(), lastMenu);
//                        return generateXML(childMenuParameterNotFound, session);
//                    }
//
//                } catch (JSONException jse) {
//                    log.error("Can't load [" + lastMenu.getAddToSession() + "] to multipage, caused by " + jse.getMessage());
//                }
//                Menu childMenuMultiple = SpringImpl.getMenuDao().findChild(req.getClient().getClientCode(), lastMenu);
//                return generateXML(childMenuMultiple, session);
            case CHECK_PREFIX:
                Menu menuProvider = checkPrefix(inputLogger, lastMenu, session);
                return generateXML(req, inputLogger, menuProvider, session, xml);
            case BACK:
                Menu parentMenu = SpringImpl.getMenuDao().findParent(req.getClient().getClientCode(), lastMenu);
                return generateXML(req, inputLogger, parentMenu, session, xml);
            default:
                session.setLastMenuId(lastMenu.getId());
                SpringImpl.getSessionDao().setInactive(session);
                return xml.generateDefaultResponse();
        }

    }

    private String generateXML(RequestMapping req, InputLogger inputLogger, Menu selectedMenu, Session session, XMLParser xml) {
        if (selectedMenu.getAddToSession() != null && selectedMenu.getAddToSession().startsWith("{")) {
            try {
                JSONObject jsonData = new JSONObject(selectedMenu.getAddToSession());
                inputLogger.setJson(InputLogger.mergeJSONObjects(inputLogger.getJson(), new JSONObject(selectedMenu.getAddToSession())));
                session.setFreeText(inputLogger.getJson().toString());
                if (jsonData.has("next")) {
                    Menu next = SpringImpl.getMenuDao().findByKey(session.getClientCode(), jsonData.getString("next"));
                    if (next != null) {
                        selectedMenu = next;
                    }
                }
            } catch (JSONException json) {
                log.error("Can't load [" + selectedMenu.getAddToSession() + "] to session, check parameter (min/max must be integer) caused by " + json.getMessage());
            } catch (Exception ex) {
                log.error("Can't load [" + selectedMenu.getAddToSession() + "] to session, caused by " + ex.getMessage());
            }
        }
        try {
            switch (selectedMenu.getType()) {
                case MENU_HEADER:
                case MENU:
                    String[] hasil = selectedMenu.getDisplayName().split("[{}]");
                    if (hasil.length > 1) {
                        List<Menu> listMenu = SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), selectedMenu.getGroupMenu());
                        JSONObject jsonStep = new JSONObject(session.getFreeText());
                        for (int i = 0; i < listMenu.size(); i++) {
                            log.info(listMenu.get(i).getType());
                            if (MenuType.MENU_HEADER.equals(listMenu.get(i).getType())) {
                                String[] hasil2 = listMenu.get(i).getDisplayName().split("[{}]");
                                String message = "";
                                for (String x : hasil2) {
                                    if (jsonStep.has(x)) {
                                        x = jsonStep.getString(x);
                                    }
                                    message += x;
                                }
                                listMenu.get(i).setDisplayName(message);
                            }
                            log.info(listMenu.get(i).getDisplayName());
                        }
                        session.setLastMenuId(Menu.getHeader(listMenu).getId());
                        return xml.generateMenu(listMenu);
                    } else {
                        List<Menu> listMenu = SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), selectedMenu.getGroupMenu());
                        session.setLastMenuId(Menu.getHeader(listMenu).getId());
                        return xml.generateMenu(listMenu);
                    }
                case INPUT:
                case CHECK_PREFIX:
                    session.setLastMenuId(selectedMenu.getId());
                    return xml.generateInput(selectedMenu);
                case INFO:
                    session.setLastMenuId(selectedMenu.getId());
                    return xml.generateInfo(selectedMenu);
                case SEND_VIEW_INFO_FORM:
                case SEND_VIEW_INPUT_FORM:
                case SEND_VIEW_INPUT_OR_INFO_BY_RC:
                case SEND_VIEW_INPUT_MULTIPLE_PAGE:
                case SEND_VIEW_INFO_MULTIPLE_PAGE:
                case SEND_VIEW_INFO_MENU_FORM:
                    session.setLastMenuId(selectedMenu.getId());
                    InputLogger.setSelectedMenu(session, selectedMenu);
                    Space sp = SpaceFactory.getSpace();
                    Context ctx = new Context();

                    Trx trx = new Trx();
                    trx.setMsisdn(session.getMsisdn());
                    trx.setClient(req.getClient());
                    trx.setFreeText(session.getFreeText());
                    trx.setProvider(req.getProvider());
                    trx.setReqMessage(selectedMenu.getTrxMessage());

                    ctx.put(Constant.TRX, trx);
                    ctx.put(Constant.SESSION, session);
                    ctx.put(Constant.ORIGIN, req.getClient().getDisplayName());

                    String processor = trx.getClient().getName();
                    trx.setRequestTime(new Date());
                    String reffNumber = trx.getStan();
                    String queueKey = trx.getMsisdn() + reffNumber;
                    trx.setQueueKey(queueKey);
                    log.info("TRX " + trx.getMsisdn() + "[" + trx.getStan() + "] process using : " + processor);
                    sp.out(processor, ctx, cfg.getInt("timeout", 60000));

                    log.info("Trying fetch from : " + queueKey);
                    Context rspCtx = (Context) sp.in(Constant.RSP + queueKey, cfg.getInt("timeout", 60000));
                    Trx rspTrx = (Trx) rspCtx.get(Constant.TRX);
                    selectedMenu.setDisplayName(parseToXML(rspTrx.getMsgContent(), req.getClient().getClientCode(), req.getProvider()));
                    session.setLastMenuId(selectedMenu.getId());
                    switch (selectedMenu.getType()) {
                        case SEND_VIEW_INPUT_FORM:
                            return xml.generateInput(selectedMenu);
                        case SEND_VIEW_INFO_FORM:
                            SpringImpl.getSessionDao().setInactive(session);
                            return xml.generateInfo(selectedMenu);
                        case SEND_VIEW_INPUT_OR_INFO_BY_RC:
                            if (rspTrx.getClient().getClientCode().equals("114")) {
                                if (rspTrx.getMsgContent().toLowerCase().contains("ditolak")) {
                                    SpringImpl.getSessionDao().setInactive(session);
                                    return xml.generateInfo(selectedMenu);
                                } else {
                                    return xml.generateInput(selectedMenu);
                                }
                            } else {
                                if (rspTrx.getClientRc().equals(Constant.RC_APPROVED)) {
                                    return xml.generateInput(selectedMenu);
                                } else {
                                    SpringImpl.getSessionDao().setInactive(session);
                                    return xml.generateInfo(selectedMenu);
                                }
                            }
                        case SEND_VIEW_INFO_MENU_FORM:
                            if (rspTrx.getClientRc().equals(Constant.RC_APPROVED)) {
                                String[] splitDisplayRM = rspTrx.getMsgContent().split("#");
                                List<Menu> listMenu1 = SpringImpl.getMenuDao().findByParentIdByLength(selectedMenu.getId(), req.getClient().getClientCode(), splitDisplayRM.length);
                                for (int i = 0; i < splitDisplayRM.length; i++) {
                                    listMenu1.get(i).setDisplayName(listMenu1.get(i).getDisplayName().replace("$split" + i, parseToXML(splitDisplayRM[i], req.getClient().getClientCode(), req.getProvider())));
                                    if (i > 1) {
                                        inputLogger.setJson(InputLogger.mergeJSONObjects(inputLogger.getJson(), new JSONObject().put(listMenu1.get(i).getId().toString(), splitDisplayRM[i])));
                                        session.setFreeText(inputLogger.getJson().toString());
                                    }
                                }
                                session.setLastMenuId(Menu.getHeader(listMenu1).getId());
                                SpringImpl.getSessionDao().save(session, req);
                                return xml.generateMenu(listMenu1);
                            } else {
                                SpringImpl.getSessionDao().setInactive(session);
                                return xml.generateInfo(selectedMenu);
                            }
//                        case SEND_VIEW_INFO_MULTIPLE_PAGE:
//                            if (selectedMenu.getDisplayName().length() > Constant.DEFAULT_MAX_MULTIPLE_PAGE) {
//                                String[] splitDisplayText = selectedMenu.getDisplayName().split("?<={150}");
//                                session.setFreeText(InputLogger.mergeJSONObjects(new JSONObject(session.getFreeText()), new JSONObject().put("totalpage_" + selectedMenu.getMenuName(), splitDisplayText.length).put("lastdisplay_" + selectedMenu.getMenuName(), 0)).toString());
//                                selectedMenu.setDisplayName(splitDisplayText[0]);
//                                selectedMenu.setType(MenuType.MENU_HEADER);
//                                List<Menu> listMenuMultipage = SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), selectedMenu.getGroupMenu());
//                                listMenuMultipage.add(selectedMenu);
//                                session.setLastMenuId(selectedMenu.getId());
//                                return xml.generateMenu(listMenuMultipage);
//                            } else {
//                                return xml.generateInput(selectedMenu);
//                            }
//                        case SEND_VIEW_INPUT_MULTIPLE_PAGE:
//                            if (selectedMenu.getDisplayName().length() > Constant.DEFAULT_MAX_MULTIPLE_PAGE) {
//                                String[] splitDisplayText = selectedMenu.getDisplayName().split("?<={150}");
//                                session.setFreeText(InputLogger.mergeJSONObjects(new JSONObject(session.getFreeText()), new JSONObject().put("totalpage_" + selectedMenu.getMenuName(), splitDisplayText.length).put("lastdisplay_" + selectedMenu.getMenuName(), 0)).toString());
//                                selectedMenu.setDisplayName(splitDisplayText[0]);
//                                selectedMenu.setType(MenuType.MENU_HEADER);
//                                List<Menu> listMenuMultipage = SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), selectedMenu.getGroupMenu());
//                                listMenuMultipage.add(selectedMenu);
//                                session.setLastMenuId(selectedMenu.getId());
//                                return xml.generateMenu(listMenuMultipage);
//                            } else {
//                                return xml.generateInput(selectedMenu);
//                            }
                        default:
                            session.setLastMenuId(selectedMenu.getId());
                            SpringImpl.getSessionDao().setInactive(session);
                            return xml.generateDefaultResponse();
                    }
                case BACK:
                    Menu parentMenu = SpringImpl.getMenuDao().findParent(req.getClient().getClientCode(), selectedMenu);
                    session.setLastMenuId(parentMenu.getId());
                    return xml.generateMenu(SpringImpl.getMenuDao().findByClientAndGroup(req.getClient().getClientCode(), parentMenu.getGroupMenu()));
                default:
                    session.setLastMenuId(selectedMenu.getId());
                    SpringImpl.getSessionDao().setInactive(session);
                    return xml.generateDefaultResponse();
            }
        } catch (Exception ex) {
            log.error(ex);
            SpringImpl.getSessionDao().setInactive(session);
            return xml.generateDefaultResponse();
        }
    }

    private Menu checkPrefix(InputLogger inputLogger, Menu menu, Session session) {
        try {
            String nomorHandphone = inputLogger.getValue(menu.getMenuName());
            URI uri = new URIBuilder(Constant.CHECK_PREFIX_IP)
                    .setPort(Constant.CHECK_PREFIX_PORT)
                    .setPath("/prefix")
                    .setParameter("no", nomorHandphone)
                    .build();
            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            final HttpGet httpGet = new HttpGet(uri);
            HttpResponse response = httpClient.execute(httpGet);
            StringWriter writer = new StringWriter();
            IOUtils.copy(response.getEntity().getContent(), writer);
            JSONObject rsp = new JSONObject(writer.toString());
            JSONObject parameter = new JSONObject(menu.getTrxMessage());
            if (nomorHandphone.length() < 8) {
                Menu m = new Menu();
                m.setType(MenuType.INFO);
                m.setDisplayName("Panjang No HP tidak boleh kurang dari 8 Digit");
                return m;
            }

            if (nomorHandphone.length() > 14) {
                Menu m = new Menu();
                m.setType(MenuType.INFO);
                m.setDisplayName("Panjang No HP tidak boleh lebih dari 14 Digit");
                return m;
            }

            if (rsp.has("product") && rsp.has("biller") && parameter.has("check")) {
                String check = parameter.getString("check");
                if (check != null && rsp.has(check) && parameter.has(rsp.getString(check))) {
                    Menu targetMenu = SpringImpl.getMenuDao().findByKey(menu.getClientCode(), parameter.getString(rsp.getString(check)));
                    JSONObject ft = new JSONObject(session.getFreeText());
                    ft.put("product", rsp.getString("product"));
                    session.setFreeText(ft.toString());
                    // log.info("rsp:" + rsp.toString());
                    // log.info("parameter:" + parameter.toString());
                    // log.info("param:" + parameter.getString(rsp.getString(check)));
                    // log.info("target" + menu.getClientCode());
                    return targetMenu;
                } else {
                    if (parameter.has("else")) {
                        return SpringImpl.getMenuDao().findByKey("0000", parameter.getString("else"));
                    } else {
                        return SpringImpl.getMenuDao().findById(Long.valueOf(Constant.ERROR_DATA));
                    }
                }
            } else {
                Menu m = new Menu();
                m.setType(MenuType.INFO);
                m.setDisplayName("Product tidak tersedia");
                return m;
            }
        } catch (IOException ex) {
            log.error(ex);
            return null;
        } catch (URISyntaxException ex) {
            log.error(ex);
            return null;
        }
    }

    public static Map<String, String> splitQuery(String parameter) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String[] pairs = parameter.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;

    }

    public String parseToXML(String text, String bankCode, String provider) {
        String request = text;
        //log.info("hasil awal : " + request);
        if (!request.isEmpty()) {
            List<ParseXML> listData = SpringImpl.getParseXMLDao().find(bankCode, provider);
            if (listData != null) {
                for (ParseXML parseXML : listData) {
                    //log.info("parse this : " + parseXML.getTargetContent() + " to " + parseXML.getReplaceContent());
                    //if (request.contains(parseXML.getTargetContent())) {
                    //    log.info("masuk : " + parseXML.getTargetContent());
                    // if(StringEscapeUtils.unescapeJava(parseXML.getTargetContent()).equals("\n")){
                    //   log.info("asupp ");
                    //    request = request.replace("\n", parseXML.getReplaceContent());
                    // }else{
                    request = request.replace(StringEscapeUtils.unescapeJava(parseXML.getTargetContent()), StringEscapeUtils.unescapeJava(parseXML.getReplaceContent()));
                    // }
                    //}
                }
            }
        }
        //log.info("hasil parse : " + request);
        return request;
//            if ("889".equals(bankCode)) {
//                text = text
//                        .replace("%", " ");
////                    .replace("\n", "&#10;");
//            } else if ("600".equals(bankCode)) {
//                text = text
//                        //                .replace("<", "&lt;")
//                        //                .replace(">", "&gt;")
//                        //                .replace("[", "&#40;")
//                        //                .replace("]", "&#41;")
//                        //                .replace("&#10;", "&#xA;")
//                        .replace("%", " ")
//                        .replace("\n", "&#xA;");
//            } else {
//                text = text
//                        //                .replace("<", "&lt;")
//                        //                .replace(">", "&gt;")
//                        //                .replace("[", "&#40;")
//                        //                .replace("]", "&#41;")
//                        //                .replace("&#10;", "&#xA;")
//                        .replace("%", " ");
////                    .replace("\n", "&#xA;");
//            }
//            return text;
//        } else {
//            return text;
//        }
    }

}
