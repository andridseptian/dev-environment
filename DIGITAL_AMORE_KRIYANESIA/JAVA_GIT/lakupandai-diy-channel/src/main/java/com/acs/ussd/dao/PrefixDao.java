/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.dao;

import com.acs.ussd.entity.Prefix;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Akses Nusantara
 */
@Repository(value = "prefixDao")
@Transactional
public class PrefixDao {
    
    @Resource(name = "sharedEntityManager")
    private EntityManager em1;
       
    public Prefix find(String prefix) {
        try {
            return (Prefix) em1.createQuery("SELECT p FROM Prefix AS p WHERE p.prefix = :prefix")
                    .setParameter("prefix", prefix)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
