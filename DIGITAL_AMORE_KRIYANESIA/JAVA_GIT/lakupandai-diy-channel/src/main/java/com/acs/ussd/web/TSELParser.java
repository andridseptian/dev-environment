/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.web;

import com.acs.ussd.entity.Menu;
import com.acs.ussd.entity.RequestMapping;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.MenuType;
import com.acs.ussd.parser.XMLParser;
import java.util.List;
import org.jpos.util.Log;

/**
 *
 * @author Akses Nusantara
 */
public class TSELParser extends XMLParser {

    Log log = Log.getLog("Q2", getClass().getName());

    final String mark_header = "[HEADER]";
    final String mark_startLooping = "[START_LOOPING]";
    final String mark_endLooping = "[END_LOOPING]";
    final String mark_index = "[INDEX]";
    final String mark_displayName = "[DISPLAY_NAME]";
    final String mark_info = "[INFO]";
    final String mark_backCode = "[BACK_CODE]";
    final String mark_totalMenu = "[TOTAL_MENU]";
    final String PATH_INFO;
    final String PATH_MENU;
    final String PATH_INPUT;
    final String DefaultResponse = "Maaf session telah habis / menu tidak tersedia";
    private String response;

    public TSELParser(RequestMapping req) {
        this.PATH_INFO = req.getXmlPathInfo();
        this.PATH_INPUT = req.getXmlPathInput();
        this.PATH_MENU = req.getXmlPathMenu();
    }

    @Override
    public String generateMenu(List<Menu> listMenu) {
        response = XMLParser.getXMLByLocation(PATH_MENU);
        if (response.contains(mark_header)) {
            response = XMLParser.replace(response, mark_header, Menu.getHeader(listMenu).getDisplayName());
        }
        if(response.contains(mark_totalMenu)){
            response = XMLParser.replace(response, mark_totalMenu, String.valueOf(listMenu.size()));
        }
//        if (response.contains(mark_backCode)) {
//            response = XMLParser.replace(response, mark_backCode, Constant.DEFAULT_BACK_CODE);
//            for (Menu m : listMenu) {
//                if (m.getType().equals(MenuType.BACK)) {
//                    response = XMLParser.replace(response, mark_backCode, m.getIndex());
//                    break;
//                }
//            }
//        }
        response = (loopingProcess(listMenu, response, mark_startLooping, mark_endLooping));
        return response;
    }

    @Override
    public String generateInfo(Menu menu) {
        response = XMLParser.getXMLByLocation(PATH_INFO);
        if (response.contains(mark_info)) {
            response = XMLParser.replace(response, mark_info, menu.getDisplayName());
        }
        if (response.contains(mark_backCode)) {
            response = XMLParser.replace(response, mark_backCode, Constant.DEFAULT_BACK_CODE);
        }
        return response;
    }

    @Override
    public String generateInput(Menu menu) {
        response = XMLParser.getXMLByLocation(PATH_INPUT);
        if (response.contains(mark_info)) {
            response = XMLParser.replace(response, mark_info, menu.getDisplayName());
        }
        if (response.contains(mark_backCode)) {
            response = XMLParser.replace(response, mark_backCode, Constant.DEFAULT_BACK_CODE);
        }
        return response;
    }

    @Override
    public String generateDefaultResponse() {
        response = XMLParser.getXMLByLocation(PATH_INFO);
        if (response.contains(mark_info)) {
            response = XMLParser.replace(response, mark_info, DefaultResponse);
        }
        if (response.contains(mark_backCode)) {
            response = XMLParser.replace(response, mark_backCode, Constant.DEFAULT_BACK_CODE);
        }
        return response;
    }

    @Override
    public String loopingProcess(List<Menu> listMenu, String messageLooping, String mark_start, String mark_end) {
        String messageText = messageLooping;
        String loopingText = getLoopingMessage(messageLooping, mark_start, mark_end);
        String responseText = "";
        String messageTotalLooping = "";
        for (Menu m : listMenu) {
            if (m.getIndex() != null) {
                responseText = loopingText;
                if (responseText.contains(mark_displayName)) {
                    responseText = XMLParser.replace(responseText, mark_displayName, m.getDisplayName());
                }
                if (responseText.contains(mark_index)) {
                    responseText = XMLParser.replace(responseText, mark_index, m.getIndex());
                }
            } else {
                responseText = responseText.trim();
            }
            messageTotalLooping += responseText;
        }
        responseText = messageText.replace(loopingText, messageTotalLooping).replace(mark_start, "").replace(mark_end, "");
        return responseText;
    }

}
