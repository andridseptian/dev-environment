/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.dao;

import com.acs.ussd.entity.InboxGammu;
import com.acs.ussd.entity.Registration;
import com.acs.ussd.entity.SpamGammu;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Akses Nusantara
 */
@Repository(value = "regDao")
@Transactional
public class RegistrationDao {
    
    @Resource(name = "sharedEntityManager")
    private EntityManager em1;
    
    public void save(Registration registration) {
        em1.persist(registration);
    }
    
    public void save(InboxGammu inboxGammu) {
        em1.persist(inboxGammu);
    }
    
    public void save(SpamGammu spamGammu) {
        em1.persist(spamGammu);
    }
}
