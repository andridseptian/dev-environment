/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.dao;

import com.acs.ussd.entity.MLogger;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author EliteBook
 */
@Repository("mLoggerDao")
@Transactional
public class MLoggerDao {

    @Resource(name = "sharedEntityManager")
    private EntityManager entityManager;

    public void save(MLogger mlogger) {
        entityManager.persist(mlogger);
    }

    public List<MLogger> find(String keyword, int first, int max) {
        List<MLogger> logs = new ArrayList<MLogger>();
        try {
            Query q = entityManager.createQuery("SELECT log FROM MsgLog log WHERE log.msg LIKE :keyword ORDER BY log.logTime DESC");
            q.setParameter("keyword", "%" + keyword + "%");
            if (first > 0) {
                q.setFirstResult(first);
            }
            if (max > 0) {
                q.setMaxResults(max);
            }
            logs = q.getResultList();
        } catch (NoResultException nre) {
            logs.clear();
        }
        return logs;
    }
    
    public Integer count(String keyword) {
        Integer count;
        try {
            Query q = entityManager.createQuery("SELECT COUNT(log) FROM MsgLog log WHERE log.msg LIKE :keyword");
            q.setParameter("keyword", "%" + keyword + "%");           
            count = (Integer) q.getSingleResult();
        } catch (NoResultException nre) {
            return 0;
        }
        return count;
    }

}
