/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.dao;

import com.acs.ussd.entity.Trx;
import java.util.Date;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import org.jpos.util.Log;
import org.jpos.util.LogEvent;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Akses Nusantara
 */
@Repository(value = "transactionDao")
@Transactional
public class TransactionDao {

    Log log = Log.getLog("Q2", getClass().getName());
    LogEvent le;

    @Resource(name = "sharedEntityManager")
    private EntityManager em;
    
//    @Resource(name = "sharedEntityManagerDcms")
//    private EntityManager emDcms;

    @Transactional
    public void save(Trx trx) {
        try {
//            if (trx.getId() == null) {
                em.persist(trx);
//            } else {
//                em.merge(trx);
//            }
        } catch (Exception ex) {
            le = new LogEvent(getClass().getName());
            le.addMessage("Cannot save : " + trx != null ? trx.getMsisdn() : "trx at " + new Date());
            le.addMessage("===================");
            le.addMessage(ex);
            le.addMessage("========END========");
            log.error(le);
        }
    }
    
//    public TxLog findTrx(String messageId,String msisdn){
//        try {
//            return (TxLog) emDcms.createQuery("SELECT t FROM TxLog AS t WHERE t.providerReff =:messageId AND t.mobilePhone =:msisdn ORDER BY t.txDatetime DESC")
//                    .getSingleResult();
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
    

}
