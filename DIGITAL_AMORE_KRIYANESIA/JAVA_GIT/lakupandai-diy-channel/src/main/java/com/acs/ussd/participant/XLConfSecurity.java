/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import java.io.Serializable;
import org.apache.commons.codec.binary.Base64;
import org.jpos.core.Configurable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;

/**
 *
 * @author yogha
 */
public class XLConfSecurity implements TransactionParticipant {

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        String reqMessage = trx.getReqMessage();
        if (reqMessage.contains("conf#")) {
            String[] data = reqMessage.split("#");
            String encPin = new String(Base64.encodeBase64(data[1].getBytes()));
            trx.setReqMessage(data[0] + "#" + encPin);
        }
         ctx.put(Constant.TRX, trx);
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
