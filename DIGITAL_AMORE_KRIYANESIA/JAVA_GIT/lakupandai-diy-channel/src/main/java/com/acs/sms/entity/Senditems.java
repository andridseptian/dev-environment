/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.sms.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author Akses Nusantara
 */

@Entity
@Table(name = "sentitems")
public class Senditems implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "UpdatedInDB")
    private Date updatedInDB;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "InsertIntoDB")
    private Date insertIntoDB;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "SendingDateTime")
    private Date sendingDateTime;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "DeliveryDateTime")
    private Date deliveryDateTime;
    
    @Column(name = "Text")
    @Type(type = "text")
    private String text;
    
    @Column(name = "DestinationNumber", length = 20)
    private String destinationNumber;

    @Column(name = "Coding")
    private String coding;
    
    @Column(name = "UDH")
    @Type(type = "text")
    private String udh;

    @Column(name = "Class")
    private Integer clazz;
    
    @Column(name = "TextDecoded")
    @Type(type = "text")
    private String textDecoded;
    
    @Column(name = "SenderID", length = 255)
    private String senderId;
    
    @Column(name = "SequencePosition")
    private Integer sequencePosition;
    
    @Column(name = "Status")
    private Status status;
    
    @Column(name = "StatusError")
    private Integer statusError;
    
    @Column(name = "TPMR")
    private Integer TPMR;
    
    @Column(name = "RelativeValidity")
    private Integer relativeValidity;
    
    @Column(name = "CreatorID")
    @Type(type = "text")
    private String creatorId;
    
    public enum Status{
        SendingOK,
        SendingOKNoReport,
        SendingError,
        DeliveryOK,
        DeliveryFailed,
        DeliveryPending,
        DeliveryUnknown,
        Error;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdatedInDB() {
        return updatedInDB;
    }

    public void setUpdatedInDB(Date updatedInDB) {
        this.updatedInDB = updatedInDB;
    }

    public Date getInsertIntoDB() {
        return insertIntoDB;
    }

    public void setInsertIntoDB(Date insertIntoDB) {
        this.insertIntoDB = insertIntoDB;
    }

    public Date getSendingDateTime() {
        return sendingDateTime;
    }

    public void setSendingDateTime(Date sendingDateTime) {
        this.sendingDateTime = sendingDateTime;
    }

    public Date getDeliveryDateTime() {
        return deliveryDateTime;
    }

    public void setDeliveryDateTime(Date deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDestinationNumber() {
        return destinationNumber;
    }

    public void setDestinationNumber(String destinationNumber) {
        this.destinationNumber = destinationNumber;
    }

    public String getCoding() {
        return coding;
    }

    public void setCoding(String coding) {
        this.coding = coding;
    }

    public String getUdh() {
        return udh;
    }

    public void setUdh(String udh) {
        this.udh = udh;
    }

    public Integer getClazz() {
        return clazz;
    }

    public void setClazz(Integer clazz) {
        this.clazz = clazz;
    }

    public String getTextDecoded() {
        return textDecoded;
    }

    public void setTextDecoded(String textDecoded) {
        this.textDecoded = textDecoded;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public Integer getSequencePosition() {
        return sequencePosition;
    }

    public void setSequencePosition(Integer sequencePosition) {
        this.sequencePosition = sequencePosition;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getStatusError() {
        return statusError;
    }

    public void setStatusError(Integer statusError) {
        this.statusError = statusError;
    }

    public Integer getTPMR() {
        return TPMR;
    }

    public void setTPMR(Integer TPMR) {
        this.TPMR = TPMR;
    }

    public Integer getRelativeValidity() {
        return relativeValidity;
    }

    public void setRelativeValidity(Integer relativeValidity) {
        this.relativeValidity = relativeValidity;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }
}
