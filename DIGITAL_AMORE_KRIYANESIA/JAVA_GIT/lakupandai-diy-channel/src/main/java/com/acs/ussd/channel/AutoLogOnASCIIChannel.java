// Copyright (c) Micro Pay Nusantara 2010

package com.acs.ussd.channel;

import com.acs.util.Utility;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.ISO87APackager;

/**
 *
 * @author Erwin
 */
public class AutoLogOnASCIIChannel extends ASCIIChannel {
    
    public AutoLogOnASCIIChannel() {
        super();
    }
    
    @Override
    public void connect() {
        try {
            super.connect();
            doLogOn();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void doLogOn() {
        try {
            ISOMsg r = new ISOMsg();
            r.setPackager(new ISO87APackager());
            r.setMTI("0800");
            r.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
            r.set(11, Utility.generateStan());
            r.set(70, "001");
            if (this.isConnected()) {
                send(r);
                ISOMsg rcv = receive();
                if (rcv == null) {
                    System.out.println("No Logon response from host, disconnecting...");
                    disconnect();
                } else {
                    if (rcv.getString(39).equals("00")) {
                        System.out.println("Logon Success");
                    } else {
                        System.out.println("Logon rejected from host, disconnecting...");
                        disconnect();
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ISOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
