/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.enums.Constant;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author Akses Nusantara
 */
public class SendToSpace implements TransactionParticipant, Configurable {
    
    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }
    
    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        ISOMsg in = (ISOMsg) ctx.get(Constant.IN);
        Long timeout = cfg.getLong("timeout", 30000);
        String spaceName = cfg.get("space_name", "jatim-space");
        try {
            String[] bitkey = ISOUtil.toStringArray(cfg.get("key_space"));
            String key = "";
            for (String bit : bitkey) {
                if (in.hasField(bit)) {
                    key += in.getString(bit);
                }
            }
            log.info("KEY SEND TO SPACE : "+key);
            Space sp = SpaceFactory.getSpace(spaceName);
            sp.out(key, ctx, timeout);
        } catch (ISOException isos) {
            log.error(isos);
        }
    }
    
    @Override
    public void abort(long l, Serializable srlzbl) {
        
    }
    
    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }
    
}
