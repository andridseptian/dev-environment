/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.entity;

import com.acs.ussd.entity.enums.TransactionStatus;
import com.acs.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

/**
 *
 * @author Akses Nusantara
 */
@Entity
@Table(name = "trx")
public class Trx implements Serializable {

    public Trx() {
        this.crTime = new Date();
        this.stan = Utility.generateStan();
        this.status = TransactionStatus.PENDING;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "trx_seq")
    @SequenceGenerator(name = "trx_seq", sequenceName = "trx_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "cr_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date crTime;

    @Column(name = "request_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestTime;

    @Column(name = "sending_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendingTime;

    @Column(name = "log_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime;

    @Column(name = "elapsed")
    private Long elapsed;

    @Column(name = "req_msg")
    private String reqMessage;

    @Column(name = "msg_content")
    @Type(type = "text")
    private String msgContent;

    @ManyToOne
    @JoinColumn(name = "client")
    private Client client;

    @Column(name = "provider")
    private String provider;

    @Column(name = "msisdn", length = 20)
    private String msisdn;

    @Column(name = "stan", nullable = true, length = 20)
    private String stan;

    @Column(name = "provider_reff", nullable = true, length = 100)
    private String providerReff;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @Column(name = "queue_key", nullable = true)
    private String queueKey;

    @Column(name = "client_reff", nullable = true)
    private String clientReff;

    @Column(name = "client_rc", nullable = true)
    private String clientRc;

    @Column(name = "free_text")
    @Type(type = "text")
    private String freeText;

    @Column(name = "trx_info")
    @Type(type = "text")
    private String trxInfo;
    
    @Column(name = "pc", length = 10)
    private String pc;

    //Call this when pre-save data
    @PrePersist
    protected void onCreate() {
        logTime = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLogTime() {
        return logTime;
    }

    public Date getCrTime() {
        return crTime;
    }

    public void setCrTime(Date crTime) {
        this.crTime = crTime;
    }

    public Long getElapsed() {
        return elapsed;
    }

    public void setElapsed(Long elapsed) {
        this.elapsed = elapsed;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getProviderReff() {
        return providerReff;
    }

    public void setProviderReff(String providerReff) {
        this.providerReff = providerReff;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public String getClientReff() {
        return clientReff;
    }

    public void setClientReff(String clientReff) {
        this.clientReff = clientReff;
    }

    public String getClientRc() {
        return clientRc;
    }

    public void setClientRc(String clientRc) {
        this.clientRc = clientRc;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getReqMessage() {
        return reqMessage;
    }

    public void setReqMessage(String reqMessage) {
        this.reqMessage = reqMessage;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(Date sendingTime) {
        this.sendingTime = sendingTime;
    }

    public String getQueueKey() {
        return queueKey;
    }

    public void setQueueKey(String queueKey) {
        this.queueKey = queueKey;
    }

    public String getTrxInfo() {
        return trxInfo;
    }

    public void setTrxInfo(String trxInfo) {
        this.trxInfo = trxInfo;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    
}