/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd.participant;

import com.acs.ussd.entity.Trx;
import com.acs.ussd.entity.enums.Constant;
import com.acs.ussd.entity.enums.TransactionStatus;
import java.io.Serializable;
import org.jpos.core.Configuration;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;

/**
 *
 * @author Akses Nusantara
 */
public class ISOResponseParser implements TransactionParticipant {

    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        Trx trx = (Trx) ctx.get(Constant.TRX);
        ISOMsg hasil = (ISOMsg) ctx.get(Constant.IN);
        try {
            String[] arrayCheck = ISOUtil.toStringArray(cfg.get("check"));
            for (String check : arrayCheck) {
                if (check.contains(":")) {
                    String[] param = check.split(":");
                    if (hasil != null && hasil.hasField(param[0])) {
                        System.out.println("PARAM 0: "+param[0]+" ,1:"+param[1]);
                        if (param[1].equals(hasil.getValue(param[0]))) {
                            trx.setStatus(TransactionStatus.SUCCESS);
                            trx.setClientRc(Constant.RC_APPROVED);
                        } else {
                            ctx.put(Constant.TRX_INFO, param[0]+" not found in response");
                            trx.setStatus(TransactionStatus.FAILED);
                            trx.setClientRc(Constant.RC_MISSING_MANDATORY_PARAMETER);
                            ctx.put(Constant.TRX, trx);
                            return;
                        }
                    } else {
                        ctx.put(Constant.TRX_INFO, arrayCheck.toString() + " not found in response");
                        trx.setStatus(TransactionStatus.FAILED);
                        trx.setClientRc(Constant.RC_MISSING_MANDATORY_PARAMETER);
                        ctx.put(Constant.TRX, trx);
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            log.info(ex);
            ctx.put(Constant.TRX_INFO, "IN null");
            trx.setStatus(TransactionStatus.FAILED);
            trx.setClientRc(Constant.RC_INTERNAL_ERROR);
        } finally {
            ctx.put(Constant.TRX, trx);
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

}
