/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.acs.ussd.entity.enums;

/**
 *
 * @author Akses Nusantara
 */
public enum TransactionType {
    ONE_STEP,       // one step transaction, 0200 -> 0210 for response text
    TWO_STEP;       // two step transaction, 0200 -> 0210 only for commit the message
}
