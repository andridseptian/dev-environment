/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd;

import org.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Akses Nusantara
 */
public class testClass {

    public static void main(String[] args) {
        JSONObject json = new JSONObject();
        json.put("selected_menu", "trf_on_us_konfirmasi");
        json.put("menu_bprks", 2);
        json.put("trf_on_us_no_rekening", "021423342");
        json.put("trf_on_us_konfirmasi", "1");
        json.put("trf_on_us_berita", "test");
        json.put("header_transfer_dana", "1");
        json.put("trf_on_us_nominal", "50000");
        String text = "transfer_inq#{trf_on_us_nominal}#{trf_on_us_no_rekening}#{trf_on_us_berita}";
        String[] hasil = text.split("[{}]");
        String req = "";
        for (String x : hasil) {
            if (json.has(x)) {
                x = json.getString(x);
            }
            req += x;
        }
        byte[] encodedBytes = Base64.encodeBase64("Test".getBytes());
        System.out.println("encodedBytes " + new String(encodedBytes));

        System.out.println(req);

    }
}
