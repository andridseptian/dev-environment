/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acs.ussd;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

/**
 *
 * @author Akses Nusantara
 */
public class TestCasee {

    public static void main(String[] args) {
        String message = "transfer_conf#Base64({trf_on_us_konfirmasi})#Base64({trf_on_the})";
        JSONObject json = new JSONObject();
        json.put("trf_on_us_konfirmasi", "123456");
        json.put("trf_on_the", "12345");
        String[] hasil = message.split("(?<=[{}])");
        String req = "";
        for (String x : hasil) {
            System.out.println("X :" + x);
            if (json.has(x)) {
                x = json.getString(x);
            }
            req += x;
        }
        
        
        
        
        System.out.println(req);
        System.out.println("================");
        String reqBase64  = "";
        if (req.indexOf("Base64") > 0) {
            req = req.replace("Base64", "~");
            String[] replace = req.split("(?<=[~])");
            for (String r : replace) {
                if (r.startsWith("(") && r.contains(")")) {
                    String replaceThis = r.substring((r.indexOf('(')+1), r.indexOf(')'));
                    String messageAll = r.substring((r.indexOf('(')), r.indexOf(')')+1);            
                    byte[] encodedBytes = Base64.encodeBase64(replaceThis.getBytes());
                    String h = new String(encodedBytes);      
                    r = r.replaceAll(messageAll, h);
                }
                reqBase64 += r;
            }
        }
        System.out.println(reqBase64.replace("~","").replaceAll("\\(", "").replaceAll("\\)", ""));

    }
}
